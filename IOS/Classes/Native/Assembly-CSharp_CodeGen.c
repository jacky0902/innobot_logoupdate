﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* MediaRecorderiOS_OnRecording_mE56D0FE0CEB3ADDB92F9346A3DCDAAF7F7F1C82B_RuntimeMethod_var;
extern const RuntimeMethod* RenderDispatcher_DequeueRender_mEFFD3475E3F871614C267EA217B6342B76758068_RuntimeMethod_var;



// 0x00000001 System.Void AboutScene::UStart()
extern void AboutScene_UStart_m07EAD6D287CA38326A087A3D92885B209996E8E4 (void);
// 0x00000002 System.Void AboutScene::.ctor()
extern void AboutScene__ctor_mA36347B427AAEED5705DAF8385BF6A7E9E8F4FAC (void);
// 0x00000003 System.Void ArrowsRealTime::Start()
extern void ArrowsRealTime_Start_mB9548DBED1AEC1098BA2D54E0326C2CC7AC6FC67 (void);
// 0x00000004 System.Void ArrowsRealTime::.ctor()
extern void ArrowsRealTime__ctor_m27CA6EF245D048841AFFF448F83237AF3C177BF2 (void);
// 0x00000005 System.Void ArrowsRealTime::<Start>b__5_0()
extern void ArrowsRealTime_U3CStartU3Eb__5_0_mA80DEB38BB5337B827A156426C8BF9047B9E6F38 (void);
// 0x00000006 System.Void ArrowsRealTime::<Start>b__5_1()
extern void ArrowsRealTime_U3CStartU3Eb__5_1_mBE23A2430EA93A8C14E8A962E31E1A38E88AAA0B (void);
// 0x00000007 System.Void BarProgress::Start()
extern void BarProgress_Start_mD2A21A1C0F8551453B254A87FBB12D5C4E6FE5D5 (void);
// 0x00000008 System.Void BarProgress::StartSong(System.Single)
extern void BarProgress_StartSong_mD41DE672D75646B2A57ABB8EA21D5B96BC37089A (void);
// 0x00000009 System.Collections.IEnumerator BarProgress::BarCoroutine()
extern void BarProgress_BarCoroutine_m42E33E823A5AF5B084074B7E02A6D456B101C14F (void);
// 0x0000000A System.Void BarProgress::StopSong()
extern void BarProgress_StopSong_mDC7A7E69D1DF2524637BD44BD1A864DD725AB961 (void);
// 0x0000000B System.Void BarProgress::.ctor()
extern void BarProgress__ctor_m10E71C8CECD7C75B9D8A4C1527CC315C7DE754E7 (void);
// 0x0000000C System.Void BarProgress/<BarCoroutine>d__7::.ctor(System.Int32)
extern void U3CBarCoroutineU3Ed__7__ctor_m23C1539725D2119616718EF7BA05B3F0EB414D5F (void);
// 0x0000000D System.Void BarProgress/<BarCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CBarCoroutineU3Ed__7_System_IDisposable_Dispose_m9835E294522F630BC99CD44E8332590317EB0EDB (void);
// 0x0000000E System.Boolean BarProgress/<BarCoroutine>d__7::MoveNext()
extern void U3CBarCoroutineU3Ed__7_MoveNext_mECD7CF9F84B385B7CD5A727DB6E9FE5ABAA6BB51 (void);
// 0x0000000F System.Object BarProgress/<BarCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBarCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72FCE8FE471CB3AA7EE811EE21D35DDF31B50B99 (void);
// 0x00000010 System.Void BarProgress/<BarCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CBarCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m67BDED7D58737BAB2559AE1D44E2D7E4E64AA71F (void);
// 0x00000011 System.Object BarProgress/<BarCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CBarCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_m2653EC05BEBFE9545C2D0DBBA592AAA317DBA6D4 (void);
// 0x00000012 System.Void Flash::Start()
extern void Flash_Start_mB3F283392DE2C490DC1EF9F48CFE865CE9791F0A (void);
// 0x00000013 System.Void Flash::DoFlash()
extern void Flash_DoFlash_m6CA10AA41E15D586EB0B8366C56EFE2C8D5454FD (void);
// 0x00000014 System.Collections.IEnumerator Flash::FlashRoutine(System.Single)
extern void Flash_FlashRoutine_m2757A18F9D0FB2C6E2F6E208D73232394561F624 (void);
// 0x00000015 System.Void Flash::.ctor()
extern void Flash__ctor_mD42700F6BDFE87A28A3301EAB53DF4A8B562D10C (void);
// 0x00000016 System.Void Flash/<FlashRoutine>d__3::.ctor(System.Int32)
extern void U3CFlashRoutineU3Ed__3__ctor_mB26267773BF6336204DB3EDAB396AEE24B7AF3DA (void);
// 0x00000017 System.Void Flash/<FlashRoutine>d__3::System.IDisposable.Dispose()
extern void U3CFlashRoutineU3Ed__3_System_IDisposable_Dispose_mE34F87C5B58FEBF1C71A7899F4AEC7FEFB579571 (void);
// 0x00000018 System.Boolean Flash/<FlashRoutine>d__3::MoveNext()
extern void U3CFlashRoutineU3Ed__3_MoveNext_mCF33647F38E3E1D34A992BE86894906833B0F01A (void);
// 0x00000019 System.Object Flash/<FlashRoutine>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlashRoutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35F71B21278725AE070D651D3AB7693BF5D83CD1 (void);
// 0x0000001A System.Void Flash/<FlashRoutine>d__3::System.Collections.IEnumerator.Reset()
extern void U3CFlashRoutineU3Ed__3_System_Collections_IEnumerator_Reset_m50A65F73FD1C18C207AF05302780FB95E5A66C2C (void);
// 0x0000001B System.Object Flash/<FlashRoutine>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CFlashRoutineU3Ed__3_System_Collections_IEnumerator_get_Current_m1A07A88FDD81376B68505EE64E5B5D8724B86705 (void);
// 0x0000001C System.Void GreenPlay::.ctor()
extern void GreenPlay__ctor_mCA8E371BF41D2B16C1227F7154637123C0A43BB3 (void);
// 0x0000001D System.Void GyrosScript::Update()
extern void GyrosScript_Update_mDC58AAE8503976546FEF4F01098D570E8537CCC3 (void);
// 0x0000001E System.Void GyrosScript::.ctor()
extern void GyrosScript__ctor_mF7672C14C4D57BD6B867480E0BD4956D02FC77ED (void);
// 0x0000001F System.Void OpenScene::Start()
extern void OpenScene_Start_mF143B95937024F6DDE3BC6821D773A056E1E9821 (void);
// 0x00000020 System.Void OpenScene::OnClick()
extern void OpenScene_OnClick_mD80DBCD7A44CAC345CA646F3BC1D12ED09971799 (void);
// 0x00000021 System.Void OpenScene::.ctor()
extern void OpenScene__ctor_m2AC575BDAA09D5BE6DED2B619860AA0C44A96513 (void);
// 0x00000022 System.Void MainMenuScene::UStart()
extern void MainMenuScene_UStart_m19E4D53A5EAFD9C5EE30D04C6DA91720EBBC4E19 (void);
// 0x00000023 System.Void MainMenuScene::Update()
extern void MainMenuScene_Update_mBEB4B608FF11A182883494FB88A8EC7B23E4D94B (void);
// 0x00000024 System.Void MainMenuScene::.ctor()
extern void MainMenuScene__ctor_mF74AC5A080C085EE614238A09C55287F48B9B412 (void);
// 0x00000025 System.Void MainMenuScene/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mCE1250F1C47436877E6F952107411E47E9F2F710 (void);
// 0x00000026 System.Void MainMenuScene/<>c__DisplayClass2_0::<UStart>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CUStartU3Eb__0_m390A93E25BD1CFC783A3747CD43DA86D8784C52A (void);
// 0x00000027 System.Void MainMenuScene/<>c__DisplayClass2_0::<UStart>b__2()
extern void U3CU3Ec__DisplayClass2_0_U3CUStartU3Eb__2_mF51EFA624AE4CBA0C9773FACEFD8039E3F0972CE (void);
// 0x00000028 System.Void MainMenuScene/<>c::.cctor()
extern void U3CU3Ec__cctor_mF52C849060A83B7B7628D604ABD7271EA532B1EA (void);
// 0x00000029 System.Void MainMenuScene/<>c::.ctor()
extern void U3CU3Ec__ctor_mD81BC8F1170D0F752DFEFAFC11BD7DA3D0FA0319 (void);
// 0x0000002A System.Void MainMenuScene/<>c::<UStart>b__2_1()
extern void U3CU3Ec_U3CUStartU3Eb__2_1_m8EC299EBD4DE60775E80945A8B95B6A2A5D5DD10 (void);
// 0x0000002B System.Void MouseOverViewPort::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MouseOverViewPort_OnPointerEnter_mEF8185C27622FDDF6919D7DC59BF341BCC61EF83 (void);
// 0x0000002C System.Void MouseOverViewPort::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MouseOverViewPort_OnPointerExit_m617578E9394B24E4BE3CD982C185A51C28CE045B (void);
// 0x0000002D System.Void MouseOverViewPort::.ctor()
extern void MouseOverViewPort__ctor_m14E192911CEDB0C316174B2AC8941F7A66A335C3 (void);
// 0x0000002E System.Void MusicButton::Start()
extern void MusicButton_Start_m470B3D841B003920A256A3F875DB8C10A0D2C5F0 (void);
// 0x0000002F System.Void MusicButton::OnClick()
extern void MusicButton_OnClick_m27695C33321ABC30A26E27BC49FE5254CBBFA217 (void);
// 0x00000030 System.Void MusicButton::StartSong()
extern void MusicButton_StartSong_mECD707F068E435705FCC1630326A48D613BA7386 (void);
// 0x00000031 System.Void MusicButton::StopSong()
extern void MusicButton_StopSong_m11ED8F987F0078EAB52B77B0E83AC14C33CE5B94 (void);
// 0x00000032 System.Void MusicButton::.ctor()
extern void MusicButton__ctor_m1ED0930034301A38D16858A9FCB40E326405CA2D (void);
// 0x00000033 System.Void MusicButtonPhotoMode::Start()
extern void MusicButtonPhotoMode_Start_m20DF21D9FB26A0E7323D47D1A1C0BA10793DBAD4 (void);
// 0x00000034 System.Void MusicButtonPhotoMode::OnClick()
extern void MusicButtonPhotoMode_OnClick_m299F7BD9E873825365D98AF2951D636B28C14807 (void);
// 0x00000035 System.Void MusicButtonPhotoMode::StartSong()
extern void MusicButtonPhotoMode_StartSong_m6A261BC914AB1645125A96A055923956C2BC9080 (void);
// 0x00000036 System.Void MusicButtonPhotoMode::StopSong()
extern void MusicButtonPhotoMode_StopSong_m94DFD03F3DBD7B5F7E9169ACDCAE6549DE152323 (void);
// 0x00000037 System.Void MusicButtonPhotoMode::.ctor()
extern void MusicButtonPhotoMode__ctor_m03FC52C6C38B35EB76D24DE4C9971ED08085F6A3 (void);
// 0x00000038 System.Void OnClickGoToWeb::Start()
extern void OnClickGoToWeb_Start_m174D187C041D182F9A265BCCA56CCFE4E978D209 (void);
// 0x00000039 System.Void OnClickGoToWeb::Update()
extern void OnClickGoToWeb_Update_m577AAB559AE048D1BE6E8C4073EF36BEF8958504 (void);
// 0x0000003A System.Void OnClickGoToWeb::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void OnClickGoToWeb_OnPointerClick_mD9C248A602AB1B763074A950E07A74B6CCDAB0C8 (void);
// 0x0000003B System.Void OnClickGoToWeb::.ctor()
extern void OnClickGoToWeb__ctor_mBE13D538381B0E5649E8550B7680FA27C6C48DA3 (void);
// 0x0000003C System.Void OnClickSendEmail::Start()
extern void OnClickSendEmail_Start_mBC579B612C620883494369D23A529BD83A6E2370 (void);
// 0x0000003D System.Void OnClickSendEmail::Update()
extern void OnClickSendEmail_Update_mA0F3C5A2EA651FECF6578B1EEE1A128DB14165C2 (void);
// 0x0000003E System.Void OnClickSendEmail::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void OnClickSendEmail_OnPointerClick_m14E688D118ADF29A1339C3E53300E1F73F79131B (void);
// 0x0000003F System.Void OnClickSendEmail::.ctor()
extern void OnClickSendEmail__ctor_m3E1B305A1C14899C6789A7688D237AF6A89050FD (void);
// 0x00000040 System.Void ArrowToShowButtons::Start()
extern void ArrowToShowButtons_Start_m57582BD827D7EC83A9781EFF79ADF3E8EA0D87A5 (void);
// 0x00000041 System.Void ArrowToShowButtons::Hide()
extern void ArrowToShowButtons_Hide_mA706197DB653A4A0D753BEED44273CBADD28AE37 (void);
// 0x00000042 System.Void ArrowToShowButtons::.ctor()
extern void ArrowToShowButtons__ctor_m35CAE886B7B185F782C0CFFD8D33FD97773B7FF5 (void);
// 0x00000043 System.Void EmptyScript::.ctor()
extern void EmptyScript__ctor_mAF769F24203F563BD14920402063E1AEFE02A1B4 (void);
// 0x00000044 System.Void HomeButton::Start()
extern void HomeButton_Start_mDE53CD878E625747C9DDF9F38984CA7DDFCAF3FE (void);
// 0x00000045 System.Void HomeButton::OnClick()
extern void HomeButton_OnClick_m99A3EC7748F9358AE581F45F0517F16A67FFA201 (void);
// 0x00000046 System.Void HomeButton::.ctor()
extern void HomeButton__ctor_m3F8B7A08E900FBEA261080C2BB164BDCCB902107 (void);
// 0x00000047 System.Void PlusButton::Start()
extern void PlusButton_Start_mA070D7E6690C70DFC0150DAADDB4AE58CE115CD8 (void);
// 0x00000048 System.Void PlusButton::OnClick()
extern void PlusButton_OnClick_m7A385547D3C6CF71D71DE9C06997F70D1C13035E (void);
// 0x00000049 System.Void PlusButton::.ctor()
extern void PlusButton__ctor_m5FFB7FE1FFCC8CE10DDB483A12494CAB87FE79F9 (void);
// 0x0000004A System.Void ResetButton::UStart()
extern void ResetButton_UStart_m3849863D6C0CF9AB1004879FC0EEB23EC9247CF5 (void);
// 0x0000004B System.Void ResetButton::OnClick()
extern void ResetButton_OnClick_m1E0A7953522631143E3429F15F79D19F51C68F21 (void);
// 0x0000004C System.Void ResetButton::.ctor()
extern void ResetButton__ctor_m6F0DF3952E54AA88046E41814EE352B5A6CBE3FD (void);
// 0x0000004D System.Int32 RowColumn::get_Row()
extern void RowColumn_get_Row_m333F000183C44948345F44B2FCF6366DD4455E0B (void);
// 0x0000004E System.Void RowColumn::set_Row(System.Int32)
extern void RowColumn_set_Row_m3690A6E67C35653D3B2DF253C835DA41E2604D21 (void);
// 0x0000004F System.Int32 RowColumn::get_Column()
extern void RowColumn_get_Column_m96B3038B0A009E52A89C9D5796F81500E9772B12 (void);
// 0x00000050 System.Void RowColumn::set_Column(System.Int32)
extern void RowColumn_set_Column_m2F4103415D215EAA830F16995C1702AD68B36231 (void);
// 0x00000051 System.Void RowColumn::.ctor()
extern void RowColumn__ctor_mF06489A8F9F9A52FB754B4506ADD5EE68FEEF444 (void);
// 0x00000052 System.Void TransparentButton::Start()
extern void TransparentButton_Start_m422B0B73FD677F32CDBE8EC3E775289C503EA59E (void);
// 0x00000053 System.Void TransparentButton::OnClick()
extern void TransparentButton_OnClick_m1388453B6E17B86892B731DD8B38FBA0094172DB (void);
// 0x00000054 System.Void TransparentButton::.ctor()
extern void TransparentButton__ctor_m4606C7B677E25F517DB3DC23C97ADDA43FB70019 (void);
// 0x00000055 UnityEngine.Color RobotAction::get_Color()
extern void RobotAction_get_Color_mC51F6EABBA7092392357773E63849F4AA8C4B543 (void);
// 0x00000056 System.Void RobotAction::set_Color(UnityEngine.Color)
extern void RobotAction_set_Color_mCBA8760E0F8ABD373953E1084041A0979A0E4F0F (void);
// 0x00000057 RobotActionCategory RobotAction::get_Category()
extern void RobotAction_get_Category_mC5450A085B339918E7CCDD3AC4FB670E188D1014 (void);
// 0x00000058 System.Void RobotAction::set_Category(RobotActionCategory)
extern void RobotAction_set_Category_m2B9BBEFC1F72F0FF101F7CD590576487CF0077DD (void);
// 0x00000059 System.Void RobotAction::.ctor(System.String,UnityEngine.Sprite,System.Int32)
extern void RobotAction__ctor_m93C27B58B1F2BA0D36D97C57EFD552F4EE093A31 (void);
// 0x0000005A System.String RobotAction::get_Name()
extern void RobotAction_get_Name_m37F3BC13A9D8988162BF6D1AE29222FDAD647196 (void);
// 0x0000005B UnityEngine.Sprite RobotAction::get_Icon()
extern void RobotAction_get_Icon_m76C8E8FD48A93732B77B710940FCC39F41E19A68 (void);
// 0x0000005C System.Int32 RobotAction::get_Duration()
extern void RobotAction_get_Duration_m379F6085A8241B2AB319C6ABE8950548E471AEA4 (void);
// 0x0000005D RobotAction/FaceAction[] RobotAction::GetFaceActions(RobotAction/EmotionAction)
extern void RobotAction_GetFaceActions_m2FACB2D2C318958BDFB7FDAAD9148F27C04AA62B (void);
// 0x0000005E System.Void RobotActionCategory::.ctor(System.String,UnityEngine.Color)
extern void RobotActionCategory__ctor_m6F4F2CB2642D56D0731D432563C06E454F61AB57 (void);
// 0x0000005F System.String RobotActionCategory::get_Name()
extern void RobotActionCategory_get_Name_m1A0781878E3F63996AD8746522C6F46C722B512C (void);
// 0x00000060 UnityEngine.Color RobotActionCategory::get_Color()
extern void RobotActionCategory_get_Color_mC94763CD0F499A064F1130CCD0F87D2BE1BDD6C8 (void);
// 0x00000061 System.Collections.Generic.List`1<RobotAction> RobotActionCategory::get_RobotActions()
extern void RobotActionCategory_get_RobotActions_mC331CE64E2FA269FAA7813A47A87310B8717D041 (void);
// 0x00000062 System.Void RobotActionCategory::AddAction(RobotAction)
extern void RobotActionCategory_AddAction_m43C55CFDF7D0259FFE111E4A6B5EBD22B5D204D0 (void);
// 0x00000063 RobotAction RobotActionCategory::GetAction(System.String)
extern void RobotActionCategory_GetAction_m1124F155D27176CAAFCE9FE05DE98F079778B889 (void);
// 0x00000064 System.Void RotateItSelf::Start()
extern void RotateItSelf_Start_mF85CD71BB0EB1B404C18A262D725798592B071DF (void);
// 0x00000065 System.Void RotateItSelf::Update()
extern void RotateItSelf_Update_m29163586C1FB849926C7675FC7ED3E64FAD90307 (void);
// 0x00000066 System.Void RotateItSelf::.ctor()
extern void RotateItSelf__ctor_mF84D89DF31E7A7BD181F27108251F29828688E80 (void);
// 0x00000067 System.Void ScaleFloor::Start()
extern void ScaleFloor_Start_mA3F617C8B98ED4E16A3283E3C68367A8890E782C (void);
// 0x00000068 System.Void ScaleFloor::Update()
extern void ScaleFloor_Update_mE6644DE891E5A769CBD9F91A1F2053173874B92E (void);
// 0x00000069 System.Void ScaleFloor::.ctor()
extern void ScaleFloor__ctor_mC271939B84F8E8A186751056210F595D88FB8DBE (void);
// 0x0000006A System.Void SquareScript::Start()
extern void SquareScript_Start_m77FE50CB869C7C5531F5F0E0D8B5D86918A03859 (void);
// 0x0000006B System.Void SquareScript::StartSong()
extern void SquareScript_StartSong_mC59987E0AC6A8FCC2E5875A680CE6470A7D8B147 (void);
// 0x0000006C System.Void SquareScript::StopSong()
extern void SquareScript_StopSong_mE058CA63F28869C94494A9D48B573A52760A33A5 (void);
// 0x0000006D System.Collections.IEnumerator SquareScript::SquaresCoroutine()
extern void SquareScript_SquaresCoroutine_mBCCDFC602667926201146A9A3655270B10F5D9B5 (void);
// 0x0000006E System.Void SquareScript::SetColumnHight(System.Int32,System.Int32)
extern void SquareScript_SetColumnHight_m5A2F20C440158A25262AAE6119FAD54BDC4EB35B (void);
// 0x0000006F System.Void SquareScript::.ctor()
extern void SquareScript__ctor_m08A51BD8536D3082AB04900120943485D6EE90C2 (void);
// 0x00000070 System.Void SquareScript/<SquaresCoroutine>d__7::.ctor(System.Int32)
extern void U3CSquaresCoroutineU3Ed__7__ctor_m6563D6EE6FE3EAEC4F954344013782F3D71A49FC (void);
// 0x00000071 System.Void SquareScript/<SquaresCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CSquaresCoroutineU3Ed__7_System_IDisposable_Dispose_mC37CF4764B37CB91917AAEE28E2279BACDA52977 (void);
// 0x00000072 System.Boolean SquareScript/<SquaresCoroutine>d__7::MoveNext()
extern void U3CSquaresCoroutineU3Ed__7_MoveNext_m41313DB756AF8A08B4DEA6D51E1453AAE18C1E2C (void);
// 0x00000073 System.Object SquareScript/<SquaresCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSquaresCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10C7A5FDD125FCCAC5F7CD5DEB1EB84845B97D65 (void);
// 0x00000074 System.Void SquareScript/<SquaresCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSquaresCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m1EBD89DD246CA868F9C1E044493B42EE67130A58 (void);
// 0x00000075 System.Object SquareScript/<SquaresCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSquaresCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mAF01FC8EC09AC387231FB31A681EA86537C8D9B7 (void);
// 0x00000076 System.Void WebCamPhotoCamera::Start()
extern void WebCamPhotoCamera_Start_m246A1152801FFAA8843BF5D4775B462B24295A47 (void);
// 0x00000077 System.Collections.Generic.IEnumerable`1<UnityEngine.WaitForEndOfFrame> WebCamPhotoCamera::TakePhoto(System.String)
extern void WebCamPhotoCamera_TakePhoto_m3F0BEC7432FE1BE320B4A4CAD230C4C702C82DD9 (void);
// 0x00000078 System.Void WebCamPhotoCamera::.ctor()
extern void WebCamPhotoCamera__ctor_mC37C8FC9D1B2CD7E1C5319D10B619E92C02DE3E1 (void);
// 0x00000079 System.Void WebCamPhotoCamera/<TakePhoto>d__2::.ctor(System.Int32)
extern void U3CTakePhotoU3Ed__2__ctor_m18715F20CEE37803C8CF9CA87B7E9D5744A52DC1 (void);
// 0x0000007A System.Void WebCamPhotoCamera/<TakePhoto>d__2::System.IDisposable.Dispose()
extern void U3CTakePhotoU3Ed__2_System_IDisposable_Dispose_m305CD191A679B1656C60627758C88E791E3471F3 (void);
// 0x0000007B System.Boolean WebCamPhotoCamera/<TakePhoto>d__2::MoveNext()
extern void U3CTakePhotoU3Ed__2_MoveNext_mC84B534F4AFDE5B142285A83496054D3CBBC6FD3 (void);
// 0x0000007C UnityEngine.WaitForEndOfFrame WebCamPhotoCamera/<TakePhoto>d__2::System.Collections.Generic.IEnumerator<UnityEngine.WaitForEndOfFrame>.get_Current()
extern void U3CTakePhotoU3Ed__2_System_Collections_Generic_IEnumeratorU3CUnityEngine_WaitForEndOfFrameU3E_get_Current_mEE7C5280D66E7661C26759A3E79EFAF152D3C3B9 (void);
// 0x0000007D System.Void WebCamPhotoCamera/<TakePhoto>d__2::System.Collections.IEnumerator.Reset()
extern void U3CTakePhotoU3Ed__2_System_Collections_IEnumerator_Reset_m1128DB9726EC4E7F87E93AEEAAAC242948282A43 (void);
// 0x0000007E System.Object WebCamPhotoCamera/<TakePhoto>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CTakePhotoU3Ed__2_System_Collections_IEnumerator_get_Current_m4F13E74A055BC74585057C2D23B618212DE9D2CB (void);
// 0x0000007F System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForEndOfFrame> WebCamPhotoCamera/<TakePhoto>d__2::System.Collections.Generic.IEnumerable<UnityEngine.WaitForEndOfFrame>.GetEnumerator()
extern void U3CTakePhotoU3Ed__2_System_Collections_Generic_IEnumerableU3CUnityEngine_WaitForEndOfFrameU3E_GetEnumerator_mE83D842777CEE388E8AD5ED2B8CA3D8A74850AEB (void);
// 0x00000080 System.Collections.IEnumerator WebCamPhotoCamera/<TakePhoto>d__2::System.Collections.IEnumerable.GetEnumerator()
extern void U3CTakePhotoU3Ed__2_System_Collections_IEnumerable_GetEnumerator_mF16F8DD01D18A6813DB5F6C82DCC4A8037D8AA63 (void);
// 0x00000081 System.String SavedRoutine::get_Name()
extern void SavedRoutine_get_Name_m34FA47460187AAFAB4CCA8079500748AC3BC6CA5 (void);
// 0x00000082 System.String SavedRoutine::get_Id()
extern void SavedRoutine_get_Id_m81229AF45B9C0E933C25924FC29CBC2783DA8157 (void);
// 0x00000083 System.Void SavedRoutine::.ctor(System.String,System.String)
extern void SavedRoutine__ctor_m03585835C5841F999BD1A09DDDE97BE86F17F876 (void);
// 0x00000084 System.String ArRobotLogicMod::get_ServiceUUID()
extern void ArRobotLogicMod_get_ServiceUUID_m407DA635A23E9CFA69D206651790985AEF4A0046 (void);
// 0x00000085 System.Void ArRobotLogicMod::set_ServiceUUID(System.String)
extern void ArRobotLogicMod_set_ServiceUUID_m2417B641F869D08F81904CB3D2CBD0DA1283BE55 (void);
// 0x00000086 System.String ArRobotLogicMod::get_CharacteristicUUID()
extern void ArRobotLogicMod_get_CharacteristicUUID_mFDADCA916B90CF056CE2D17F2332427F82472967 (void);
// 0x00000087 System.Void ArRobotLogicMod::set_CharacteristicUUID(System.String)
extern void ArRobotLogicMod_set_CharacteristicUUID_m150B6520F8AB6CEB7052B8BD719BE95431398119 (void);
// 0x00000088 System.String ArRobotLogicMod::get_DeviceName()
extern void ArRobotLogicMod_get_DeviceName_m169B250ED427BF5E3326531317FE66750BC30D25 (void);
// 0x00000089 System.Void ArRobotLogicMod::set_DeviceName(System.String)
extern void ArRobotLogicMod_set_DeviceName_mFF8413F510B6E8F5624B740DB0FC4E310CB053BC (void);
// 0x0000008A System.Void ArRobotLogicMod::UStart()
extern void ArRobotLogicMod_UStart_m3831A293859F9B585688989C48F2E2E923B6A0A1 (void);
// 0x0000008B System.Void ArRobotLogicMod::OnListenersAndModRegistration()
extern void ArRobotLogicMod_OnListenersAndModRegistration_mBE4869C2027160C20FCC596032306B79D2276F81 (void);
// 0x0000008C System.Void ArRobotLogicMod::OnDestroyListenersAndModUnregistration()
extern void ArRobotLogicMod_OnDestroyListenersAndModUnregistration_m12323DA3D816B30F1F245E02A5ABDC9C7E674260 (void);
// 0x0000008D System.Void ArRobotLogicMod::StartGettingBluetoothDeviceListAsync()
extern void ArRobotLogicMod_StartGettingBluetoothDeviceListAsync_mC73B3469538698A01184472AB12105AD597EB348 (void);
// 0x0000008E System.Void ArRobotLogicMod::StopGettingBluetoothDeviceList()
extern void ArRobotLogicMod_StopGettingBluetoothDeviceList_m604385E5AFFF3249911C35A93EAD3FE086580CB4 (void);
// 0x0000008F System.Void ArRobotLogicMod::ConnectToBluetoothDeviceAsync(System.String)
extern void ArRobotLogicMod_ConnectToBluetoothDeviceAsync_mDFC1EFE3DD745DA6EE5C32F238C5D5E69629F964 (void);
// 0x00000090 System.Void ArRobotLogicMod::DisconnectFromBluetoothDeviceAsync()
extern void ArRobotLogicMod_DisconnectFromBluetoothDeviceAsync_m671F93CBBF099BC27DEAD3D0028986BCFA268FFA (void);
// 0x00000091 System.Void ArRobotLogicMod::ScanAndConnectToCompatibleDeviceAsync()
extern void ArRobotLogicMod_ScanAndConnectToCompatibleDeviceAsync_mBDE2F96F961AECBDF0C3A2F3973502EE76A88E54 (void);
// 0x00000092 System.Collections.IEnumerator ArRobotLogicMod::_ScanningAndConnectingTimeOut()
extern void ArRobotLogicMod__ScanningAndConnectingTimeOut_m08BB0B209A845F676397FF0423E553E7A485511B (void);
// 0x00000093 System.Void ArRobotLogicMod::StopScanningAndConnectingToCompatibleDevice()
extern void ArRobotLogicMod_StopScanningAndConnectingToCompatibleDevice_m68247679079D2C185A878B9BCD5F39626D3CF685 (void);
// 0x00000094 ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice ArRobotLogicMod::GetBluetoothDevice(System.String)
extern void ArRobotLogicMod_GetBluetoothDevice_mAEDF468CB0361A03499227C3912F2D90F9B1D454 (void);
// 0x00000095 ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice ArRobotLogicMod::GetConnectedBluetoothDevice()
extern void ArRobotLogicMod_GetConnectedBluetoothDevice_m3ECA9F5AF158AD0ADA9D00A5F1F3E94A1428DFE3 (void);
// 0x00000096 System.Boolean ArRobotLogicMod::IsBluetoothConnected()
extern void ArRobotLogicMod_IsBluetoothConnected_mE45A57D27023E09BBC4209100FD0212CDD87FB18 (void);
// 0x00000097 System.Boolean ArRobotLogicMod::IsScanningBluetoothDevices()
extern void ArRobotLogicMod_IsScanningBluetoothDevices_m723635D72B9743F900A0236753B1F4C8BD054BF3 (void);
// 0x00000098 System.Void ArRobotLogicMod::_SendMessage(RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction,System.Int32)
extern void ArRobotLogicMod__SendMessage_mFA0CB5A382E63715EBE090BA2AE4757AB2E5E838 (void);
// 0x00000099 SavedRoutine[] ArRobotLogicMod::GetSavedRoutinesList()
extern void ArRobotLogicMod_GetSavedRoutinesList_mD2632743D6701CE2B1BC5EEB5947C32E491D876C (void);
// 0x0000009A System.Void ArRobotLogicMod::LoadRoutineAsync(System.String)
extern void ArRobotLogicMod_LoadRoutineAsync_mA4AE0887F79421E4D701FA854E981CE88586454C (void);
// 0x0000009B System.Void ArRobotLogicMod::SaveRoutineAsync(System.String,RobotAction/MotionAction[],RobotAction/EmotionAction[],RobotAction/ArmsAction[],RobotAction/SoundAction[])
extern void ArRobotLogicMod_SaveRoutineAsync_m8E80DF5AAD872F35F4198AED395138878E3875C5 (void);
// 0x0000009C System.Boolean ArRobotLogicMod::PlayRoutine(RobotAction/MotionAction[],RobotAction/EmotionAction[],RobotAction/ArmsAction[],RobotAction/SoundAction[],System.Boolean,System.Boolean)
extern void ArRobotLogicMod_PlayRoutine_mEEBD57FE151A7E32DED5F7718F5587D3B4485EC6 (void);
// 0x0000009D System.Collections.IEnumerator ArRobotLogicMod::_PlayingRoutineCoroutine()
extern void ArRobotLogicMod__PlayingRoutineCoroutine_mC47460CB44B872022722548BCED178A271EC090D (void);
// 0x0000009E System.Collections.IEnumerator ArRobotLogicMod::_PlayingRoutineCoroutineWithPartial(System.Int32,System.Single)
extern void ArRobotLogicMod__PlayingRoutineCoroutineWithPartial_mAB241A53E3ECF6597FACE1EA09D81C5834DF33C1 (void);
// 0x0000009F System.Void ArRobotLogicMod::_PlayActionsOnRobot(RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction,System.Int32)
extern void ArRobotLogicMod__PlayActionsOnRobot_m2997A8A1F3A06E678F87B49272D404575421669B (void);
// 0x000000A0 System.Void ArRobotLogicMod::PauseRoutine()
extern void ArRobotLogicMod_PauseRoutine_mDB494E58ACB2F46D0DCA0B4F8303C8DCF5414796 (void);
// 0x000000A1 System.Void ArRobotLogicMod::_StopRobot(System.Single)
extern void ArRobotLogicMod__StopRobot_m425E423E35233A1BB1D364834ED9CAA0440786E1 (void);
// 0x000000A2 System.Void ArRobotLogicMod::_StopFace(System.Single)
extern void ArRobotLogicMod__StopFace_mBB65C61E5D55F375EF4762444E638960CC2607BA (void);
// 0x000000A3 System.Void ArRobotLogicMod::UnpauseRoutine()
extern void ArRobotLogicMod_UnpauseRoutine_m877DA837D14523273F0B3E70905344B68ED4E2C9 (void);
// 0x000000A4 System.Void ArRobotLogicMod::StopRoutine()
extern void ArRobotLogicMod_StopRoutine_mE58B0A1FA3B5C9EFE4D7068A718F3DA42930B3CD (void);
// 0x000000A5 System.Boolean ArRobotLogicMod::SendRealtimeAction(RobotAction/ArmsAction)
extern void ArRobotLogicMod_SendRealtimeAction_m436D2A6E19C11B1590432356FA439E9A459C0DC3 (void);
// 0x000000A6 System.Boolean ArRobotLogicMod::SendRealtimeAction(RobotAction/EmotionAction)
extern void ArRobotLogicMod_SendRealtimeAction_m1275F481D2CF6B9272309F44B08EBC006269B823 (void);
// 0x000000A7 System.Boolean ArRobotLogicMod::SendRealtimeAction(RobotAction/SoundAction)
extern void ArRobotLogicMod_SendRealtimeAction_m59986D575902CCF4E1AF6D90D984D89881334715 (void);
// 0x000000A8 System.Boolean ArRobotLogicMod::_SendRealTimeAction(RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction)
extern void ArRobotLogicMod__SendRealTimeAction_mD8E6B0A3D2EF683E362A5A557A4BE4F2DB51739D (void);
// 0x000000A9 System.Void ArRobotLogicMod::StartSendingRealtimeAction(RobotAction/MotionAction)
extern void ArRobotLogicMod_StartSendingRealtimeAction_m7366977948488BD11C63A0FA00CF637136710412 (void);
// 0x000000AA System.Collections.IEnumerator ArRobotLogicMod::_SendContinuousRealTimeAction(RobotAction/MotionAction)
extern void ArRobotLogicMod__SendContinuousRealTimeAction_m24A2FB5F19586A97632067D70E8B6028C0D657CC (void);
// 0x000000AB System.Void ArRobotLogicMod::StopSendingRealtimeAction(RobotAction/MotionAction)
extern void ArRobotLogicMod_StopSendingRealtimeAction_m45B148B0046C3DD127E2711423C2201248D3FEBF (void);
// 0x000000AC System.Boolean ArRobotLogicMod::StartMotionWithGyroscope(System.Single)
extern void ArRobotLogicMod_StartMotionWithGyroscope_mE8B28F36C5AFF3F1AF78FE874C4100602D789A76 (void);
// 0x000000AD System.Void ArRobotLogicMod::StopMotionWithGyroscope()
extern void ArRobotLogicMod_StopMotionWithGyroscope_mA65EC353B870424150BDB501C55BEEB864CE2B6B (void);
// 0x000000AE System.Void ArRobotLogicMod::StartDancing(ArRobot.Modules.Common.DancingMode/DancingMusic)
extern void ArRobotLogicMod_StartDancing_m0A840581C0917F4E003E9729CCC3436F370BADE1 (void);
// 0x000000AF System.Collections.IEnumerator ArRobotLogicMod::DancingCoroutine(ArRobot.Modules.Common.DancingMode/DancingMusic)
extern void ArRobotLogicMod_DancingCoroutine_m8F0B70B41DDF8037D6066CA7AA3203A017DEC1EB (void);
// 0x000000B0 System.Void ArRobotLogicMod::PauseDancing()
extern void ArRobotLogicMod_PauseDancing_m2DA5AAFC2AFC15737671FB3CF5D3469D47FA9275 (void);
// 0x000000B1 System.Void ArRobotLogicMod::StopDancing()
extern void ArRobotLogicMod_StopDancing_m2C44BCF026FA6CC9009C41BD409FA2B5068E715B (void);
// 0x000000B2 System.Void ArRobotLogicMod::ShowCameraOnTexture(UnityEngine.UI.RawImage,UnityEngine.UI.AspectRatioFitter)
extern void ArRobotLogicMod_ShowCameraOnTexture_mC9D667762C7B8DC8203D28D7955F68D89381276D (void);
// 0x000000B3 System.Void ArRobotLogicMod::StopShowingCameraOnTexture()
extern void ArRobotLogicMod_StopShowingCameraOnTexture_mFAA5BA9684AD5F108A7A3ABF2298EE101FC80B28 (void);
// 0x000000B4 System.Void ArRobotLogicMod::TakePhotoAsync()
extern void ArRobotLogicMod_TakePhotoAsync_mBC81D9FF80F01BB0763CA923CB7D0B298C7E6B3E (void);
// 0x000000B5 System.Void ArRobotLogicMod::StartMakingVideo()
extern void ArRobotLogicMod_StartMakingVideo_m7C6579924A67D9CA1F379F9486C8B2BE884DFCC9 (void);
// 0x000000B6 System.Void ArRobotLogicMod::StopAndSaveVideoAsync()
extern void ArRobotLogicMod_StopAndSaveVideoAsync_mF714030D08538D00F0F36F70D1FE500897E9C6B6 (void);
// 0x000000B7 System.Void ArRobotLogicMod::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice>)
extern void ArRobotLogicMod_OnBluetoothDeviceListUpdated_m6BBFAFD60DFF55B1048E07B34B89F3F23100FDBB (void);
// 0x000000B8 System.Void ArRobotLogicMod::OnBluetoothConnected(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice,Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
extern void ArRobotLogicMod_OnBluetoothConnected_mCA2624614B9E8CDFD81F1BF1026AE5C31EAED3F2 (void);
// 0x000000B9 System.Void ArRobotLogicMod::OnBluetoothDisconnected(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice,Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
extern void ArRobotLogicMod_OnBluetoothDisconnected_mBEFA4659C4DFADBE5D595E3F1DC79BA6B329A41E (void);
// 0x000000BA System.Void ArRobotLogicMod::OnMessageSent(System.String)
extern void ArRobotLogicMod_OnMessageSent_m728FD7C82A939657F250692BFDB3A0FD5F8F158D (void);
// 0x000000BB System.Void ArRobotLogicMod::OnLoadCompleted(System.Object)
extern void ArRobotLogicMod_OnLoadCompleted_mC5C8B75BC3F37612E1CDE126D14B658C41CEA123 (void);
// 0x000000BC System.Void ArRobotLogicMod::OnStorageCompleted(Umods.ExternalMods.Storage.Mods.StorageMod.Common.StorageVariables/StoragingState)
extern void ArRobotLogicMod_OnStorageCompleted_m3123B252BEB1AA49256EA60124023A5FA7B63A70 (void);
// 0x000000BD System.Byte[] ArRobotLogicMod::_TransformActionsToMessage(RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction,System.Int32)
extern void ArRobotLogicMod__TransformActionsToMessage_m1782A584E40573EF976E9BCA4E0818701F6A8F7C (void);
// 0x000000BE System.Void ArRobotLogicMod::OnNewGyroscopeStatus(UnityEngine.Quaternion)
extern void ArRobotLogicMod_OnNewGyroscopeStatus_mB9042421429EE21BBB000F9764A0F2ED060EA5ED (void);
// 0x000000BF System.Void ArRobotLogicMod::StabilizeGyroscospe()
extern void ArRobotLogicMod_StabilizeGyroscospe_m7025B5CBC02C8119E4205A5497B17B077974AFA5 (void);
// 0x000000C0 System.Void ArRobotLogicMod::.ctor()
extern void ArRobotLogicMod__ctor_mF22B7BA1C2B2B3341D8F59F4CEB047B04EAA5608 (void);
// 0x000000C1 System.Void ArRobotLogicMod::.cctor()
extern void ArRobotLogicMod__cctor_m4E511158D61B1AD09C4B90DBF6634463AA734C51 (void);
// 0x000000C2 System.Void ArRobotLogicMod/DanceRoutine::.ctor(RobotAction/MotionAction[],RobotAction/ArmsAction[],RobotAction/EmotionAction[],RobotAction/SoundAction[])
extern void DanceRoutine__ctor_mB7B75820AE220161C3CD1857D0B6A339AEAF8263 (void);
// 0x000000C3 System.Void ArRobotLogicMod/<_ScanningAndConnectingTimeOut>d__63::.ctor(System.Int32)
extern void U3C_ScanningAndConnectingTimeOutU3Ed__63__ctor_mFE121B757E547AB140F7F35BFD6131C2344D20B2 (void);
// 0x000000C4 System.Void ArRobotLogicMod/<_ScanningAndConnectingTimeOut>d__63::System.IDisposable.Dispose()
extern void U3C_ScanningAndConnectingTimeOutU3Ed__63_System_IDisposable_Dispose_m556B56249CAF17311F95DC8C967757CB710EF37F (void);
// 0x000000C5 System.Boolean ArRobotLogicMod/<_ScanningAndConnectingTimeOut>d__63::MoveNext()
extern void U3C_ScanningAndConnectingTimeOutU3Ed__63_MoveNext_mC6BAA00F6A9F4F6D7542D58DA542AA9B28AF1795 (void);
// 0x000000C6 System.Object ArRobotLogicMod/<_ScanningAndConnectingTimeOut>d__63::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_ScanningAndConnectingTimeOutU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5837FD6B1017425588F98AE19B82011EBF5CC287 (void);
// 0x000000C7 System.Void ArRobotLogicMod/<_ScanningAndConnectingTimeOut>d__63::System.Collections.IEnumerator.Reset()
extern void U3C_ScanningAndConnectingTimeOutU3Ed__63_System_Collections_IEnumerator_Reset_m152C0FA1A11B0D092093AFD72E81EA19689E3D9B (void);
// 0x000000C8 System.Object ArRobotLogicMod/<_ScanningAndConnectingTimeOut>d__63::System.Collections.IEnumerator.get_Current()
extern void U3C_ScanningAndConnectingTimeOutU3Ed__63_System_Collections_IEnumerator_get_Current_m6CBAB3122A5A7B626AB237B51C9F4DF57A6D9C32 (void);
// 0x000000C9 System.Void ArRobotLogicMod/<_PlayingRoutineCoroutine>d__75::.ctor(System.Int32)
extern void U3C_PlayingRoutineCoroutineU3Ed__75__ctor_m15EE7DD2A43EFBDCCDE1AA10549BB47762151728 (void);
// 0x000000CA System.Void ArRobotLogicMod/<_PlayingRoutineCoroutine>d__75::System.IDisposable.Dispose()
extern void U3C_PlayingRoutineCoroutineU3Ed__75_System_IDisposable_Dispose_m856048495F47D845B3CD613D91B5B58BDFD53AFE (void);
// 0x000000CB System.Boolean ArRobotLogicMod/<_PlayingRoutineCoroutine>d__75::MoveNext()
extern void U3C_PlayingRoutineCoroutineU3Ed__75_MoveNext_mB4416C0E5662EFEBEC11788770ECE482C457C76A (void);
// 0x000000CC System.Object ArRobotLogicMod/<_PlayingRoutineCoroutine>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_PlayingRoutineCoroutineU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7B2152A44501419E0E94508D5B24E01AC41D9D6 (void);
// 0x000000CD System.Void ArRobotLogicMod/<_PlayingRoutineCoroutine>d__75::System.Collections.IEnumerator.Reset()
extern void U3C_PlayingRoutineCoroutineU3Ed__75_System_Collections_IEnumerator_Reset_m4616D580F2781DD99851861BC8492EC07CC2B180 (void);
// 0x000000CE System.Object ArRobotLogicMod/<_PlayingRoutineCoroutine>d__75::System.Collections.IEnumerator.get_Current()
extern void U3C_PlayingRoutineCoroutineU3Ed__75_System_Collections_IEnumerator_get_Current_mAA9899C8C3C86B116CA8F9197DE4740048D3986B (void);
// 0x000000CF System.Void ArRobotLogicMod/<_PlayingRoutineCoroutineWithPartial>d__76::.ctor(System.Int32)
extern void U3C_PlayingRoutineCoroutineWithPartialU3Ed__76__ctor_mD427AEF03961EA698C17CEC424ECCE21A1558404 (void);
// 0x000000D0 System.Void ArRobotLogicMod/<_PlayingRoutineCoroutineWithPartial>d__76::System.IDisposable.Dispose()
extern void U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_IDisposable_Dispose_m4BB9B29062C83C2747659EC95C69DC7395068775 (void);
// 0x000000D1 System.Boolean ArRobotLogicMod/<_PlayingRoutineCoroutineWithPartial>d__76::MoveNext()
extern void U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_MoveNext_mEC07B9910C7E6584B2707CA33DC18DF7DBFD93B9 (void);
// 0x000000D2 System.Object ArRobotLogicMod/<_PlayingRoutineCoroutineWithPartial>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FDA8D962017E023C2B630311D2B9A449942E2A5 (void);
// 0x000000D3 System.Void ArRobotLogicMod/<_PlayingRoutineCoroutineWithPartial>d__76::System.Collections.IEnumerator.Reset()
extern void U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_Collections_IEnumerator_Reset_m8F03BA586EE7181F57BE5A9D688DAB7C7FD120A1 (void);
// 0x000000D4 System.Object ArRobotLogicMod/<_PlayingRoutineCoroutineWithPartial>d__76::System.Collections.IEnumerator.get_Current()
extern void U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_Collections_IEnumerator_get_Current_mC4FAD2740C095B7C7CC0B623168A8D353A64A7A0 (void);
// 0x000000D5 System.Void ArRobotLogicMod/<_SendContinuousRealTimeAction>d__90::.ctor(System.Int32)
extern void U3C_SendContinuousRealTimeActionU3Ed__90__ctor_mC4C6FFA9C977CD42F5CB106B2723D67E5D5E4AE7 (void);
// 0x000000D6 System.Void ArRobotLogicMod/<_SendContinuousRealTimeAction>d__90::System.IDisposable.Dispose()
extern void U3C_SendContinuousRealTimeActionU3Ed__90_System_IDisposable_Dispose_mAF1FFA5B3A4DF8A4788A17ABCA059DAF34AAFB08 (void);
// 0x000000D7 System.Boolean ArRobotLogicMod/<_SendContinuousRealTimeAction>d__90::MoveNext()
extern void U3C_SendContinuousRealTimeActionU3Ed__90_MoveNext_m09E5C8F035B2F655E7E000DCEB1C4A992C61263A (void);
// 0x000000D8 System.Object ArRobotLogicMod/<_SendContinuousRealTimeAction>d__90::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_SendContinuousRealTimeActionU3Ed__90_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m136BF5268335439F97CBB3C3E0FE22A2E87DEE91 (void);
// 0x000000D9 System.Void ArRobotLogicMod/<_SendContinuousRealTimeAction>d__90::System.Collections.IEnumerator.Reset()
extern void U3C_SendContinuousRealTimeActionU3Ed__90_System_Collections_IEnumerator_Reset_m706D25D38C718115446748CD847E79FA8C04DD74 (void);
// 0x000000DA System.Object ArRobotLogicMod/<_SendContinuousRealTimeAction>d__90::System.Collections.IEnumerator.get_Current()
extern void U3C_SendContinuousRealTimeActionU3Ed__90_System_Collections_IEnumerator_get_Current_m5208934A5D375B33B930DB480A37718DFCF4D4E8 (void);
// 0x000000DB System.Void ArRobotLogicMod/<DancingCoroutine>d__96::.ctor(System.Int32)
extern void U3CDancingCoroutineU3Ed__96__ctor_mBB91F6F21C9FEF3B6C856E53A76E29425C56AEB3 (void);
// 0x000000DC System.Void ArRobotLogicMod/<DancingCoroutine>d__96::System.IDisposable.Dispose()
extern void U3CDancingCoroutineU3Ed__96_System_IDisposable_Dispose_m7D8BBD747E6D80DD2FE8924E33B124E74D1D99A4 (void);
// 0x000000DD System.Boolean ArRobotLogicMod/<DancingCoroutine>d__96::MoveNext()
extern void U3CDancingCoroutineU3Ed__96_MoveNext_m083D7AFAE0BE3BF953174EAD8ADD31F2F8C42E83 (void);
// 0x000000DE System.Object ArRobotLogicMod/<DancingCoroutine>d__96::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDancingCoroutineU3Ed__96_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55444C112E81CC65A33ECC887012496147411479 (void);
// 0x000000DF System.Void ArRobotLogicMod/<DancingCoroutine>d__96::System.Collections.IEnumerator.Reset()
extern void U3CDancingCoroutineU3Ed__96_System_Collections_IEnumerator_Reset_mCF32A1E12F57A310D5457D3666AE56672122FAF8 (void);
// 0x000000E0 System.Object ArRobotLogicMod/<DancingCoroutine>d__96::System.Collections.IEnumerator.get_Current()
extern void U3CDancingCoroutineU3Ed__96_System_Collections_IEnumerator_get_Current_mBC74819129F44BE34F6A2F14E54D4DA09C18F283 (void);
// 0x000000E1 SavedRoutine[] IArRobotLogicMod::GetSavedRoutinesList()
// 0x000000E2 System.Void IArRobotLogicMod::LoadRoutineAsync(System.String)
// 0x000000E3 System.Void IArRobotLogicMod::SaveRoutineAsync(System.String,RobotAction/MotionAction[],RobotAction/EmotionAction[],RobotAction/ArmsAction[],RobotAction/SoundAction[])
// 0x000000E4 System.Boolean IArRobotLogicMod::PlayRoutine(RobotAction/MotionAction[],RobotAction/EmotionAction[],RobotAction/ArmsAction[],RobotAction/SoundAction[],System.Boolean,System.Boolean)
// 0x000000E5 System.Void IArRobotLogicMod::PauseRoutine()
// 0x000000E6 System.Void IArRobotLogicMod::UnpauseRoutine()
// 0x000000E7 System.Void IArRobotLogicMod::StopRoutine()
// 0x000000E8 System.Boolean IArRobotLogicMod::SendRealtimeAction(RobotAction/ArmsAction)
// 0x000000E9 System.Boolean IArRobotLogicMod::SendRealtimeAction(RobotAction/EmotionAction)
// 0x000000EA System.Boolean IArRobotLogicMod::SendRealtimeAction(RobotAction/SoundAction)
// 0x000000EB System.Void IArRobotLogicMod::StartSendingRealtimeAction(RobotAction/MotionAction)
// 0x000000EC System.Void IArRobotLogicMod::StopSendingRealtimeAction(RobotAction/MotionAction)
// 0x000000ED System.Boolean IArRobotLogicMod::StartMotionWithGyroscope(System.Single)
// 0x000000EE System.Void IArRobotLogicMod::StopMotionWithGyroscope()
// 0x000000EF System.Void IArRobotLogicMod::StabilizeGyroscospe()
// 0x000000F0 System.Void IArRobotLogicMod::StartDancing(ArRobot.Modules.Common.DancingMode/DancingMusic)
// 0x000000F1 System.Void IArRobotLogicMod::PauseDancing()
// 0x000000F2 System.Void IArRobotLogicMod::StopDancing()
// 0x000000F3 System.Void IArRobotLogicMod::ShowCameraOnTexture(UnityEngine.UI.RawImage,UnityEngine.UI.AspectRatioFitter)
// 0x000000F4 System.Void IArRobotLogicMod::StopShowingCameraOnTexture()
// 0x000000F5 System.Void IArRobotLogicMod::TakePhotoAsync()
// 0x000000F6 System.Void IArRobotLogicMod::StartMakingVideo()
// 0x000000F7 System.Void IArRobotLogicMod::StopAndSaveVideoAsync()
// 0x000000F8 System.Void ILogicActionsPlayerHandler::OnNewStepPlayed(System.Int32,RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction)
// 0x000000F9 System.Void ILogicBluetoothConnectivityHandler::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
// 0x000000FA System.Void ILogicBluetoothConnectivityHandler::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
// 0x000000FB System.Void ILogicBluetoothConnectivityHandler::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
// 0x000000FC System.Void ILogicRoutinesStorageHandler::OnRoutineLoaded(System.Boolean,SavedRoutine,RobotAction/MotionAction[],RobotAction/ArmsAction[],RobotAction/EmotionAction[],RobotAction/SoundAction[])
// 0x000000FD System.Void ILogicRoutinesStorageHandler::OnRoutineSaved(System.Boolean,SavedRoutine)
// 0x000000FE System.Void ActionConnectivityTestingScene::UStart()
extern void ActionConnectivityTestingScene_UStart_m4C27107991CF4E96B8C0CC5273391B2B0843CE27 (void);
// 0x000000FF System.Void ActionConnectivityTestingScene::SendNewButton()
extern void ActionConnectivityTestingScene_SendNewButton_m5E247128720B4EA37688BF177A772A914B3287B2 (void);
// 0x00000100 System.Void ActionConnectivityTestingScene::_SendActions()
extern void ActionConnectivityTestingScene__SendActions_m946DC7EBC4202D3F7039FC0E95582720AA9B9028 (void);
// 0x00000101 System.Void ActionConnectivityTestingScene::_ConnectToAddress()
extern void ActionConnectivityTestingScene__ConnectToAddress_mA937241E8E927D9A3A3962119DC99741B6D7C41E (void);
// 0x00000102 System.Void ActionConnectivityTestingScene::ShowWarning(System.String)
extern void ActionConnectivityTestingScene_ShowWarning_mBA6FDA9101094329FFD163178E7159879A516E34 (void);
// 0x00000103 System.Void ActionConnectivityTestingScene::HideWarning()
extern void ActionConnectivityTestingScene_HideWarning_m8659A6747669C2153E1DE768F968FE6E1FDAB9AB (void);
// 0x00000104 System.Void ActionConnectivityTestingScene::_ButtonPressed(RobotAction/MotionAction,UnityEngine.UI.Button)
extern void ActionConnectivityTestingScene__ButtonPressed_m5CF5A4A46F26AE165BEAE632D8C5FC52001BE6B9 (void);
// 0x00000105 System.Void ActionConnectivityTestingScene::_ButtonPressed(RobotAction/ArmsAction,UnityEngine.UI.Button)
extern void ActionConnectivityTestingScene__ButtonPressed_mD36E7C87DE2900FCC751810DA6EC3FAD4A9B757F (void);
// 0x00000106 System.Void ActionConnectivityTestingScene::_ButtonPressed(RobotAction/EmotionAction,UnityEngine.UI.Button)
extern void ActionConnectivityTestingScene__ButtonPressed_m69A6485C154E28F1DFAE75EE5AB2983963B1B5B8 (void);
// 0x00000107 System.Void ActionConnectivityTestingScene::_ButtonPressed(RobotAction/SoundAction,UnityEngine.UI.Button)
extern void ActionConnectivityTestingScene__ButtonPressed_mD6769AEDD589566EC0F721003C545D82AF38FA1D (void);
// 0x00000108 System.Void ActionConnectivityTestingScene::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
extern void ActionConnectivityTestingScene_OnBluetoothDeviceListUpdated_m84AD6E07E04F48FCA8DEC6AE39FF188344D4A213 (void);
// 0x00000109 System.Void ActionConnectivityTestingScene::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void ActionConnectivityTestingScene_OnBluetoothConnected_mE58509F53E0C7CB7C49D1475D0665AC5F78A2E87 (void);
// 0x0000010A System.Void ActionConnectivityTestingScene::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void ActionConnectivityTestingScene_OnBluetoothDisconnected_m6DAD62364EA5BE63899089E4D8D5949F55BB7B6B (void);
// 0x0000010B System.Void ActionConnectivityTestingScene::.ctor()
extern void ActionConnectivityTestingScene__ctor_m031088F99CCDAB27A5009A0C58297B96E81AE3A5 (void);
// 0x0000010C System.Void ActionConnectivityTestingScene::<UStart>b__54_0()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_0_m390524D5D3CF843D826963FA75ECBACAC8AAE78A (void);
// 0x0000010D System.Void ActionConnectivityTestingScene::<UStart>b__54_1()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_1_mDE1F6CD4606AE71B09E6E9E0858E40D5A6931D20 (void);
// 0x0000010E System.Void ActionConnectivityTestingScene::<UStart>b__54_2()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_2_mE21A32AA0BB7ACA548D146979774D7D79407044C (void);
// 0x0000010F System.Void ActionConnectivityTestingScene::<UStart>b__54_3()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_3_mD53564B28566B96868E8D7CA612ED8720B908387 (void);
// 0x00000110 System.Void ActionConnectivityTestingScene::<UStart>b__54_4()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_4_m0B8FA85FD4554F32989546AE05289D7DFEB8C674 (void);
// 0x00000111 System.Void ActionConnectivityTestingScene::<UStart>b__54_5()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_5_mCDAAB46A622265754DAECD2A1CF218FBC1B4B11A (void);
// 0x00000112 System.Void ActionConnectivityTestingScene::<UStart>b__54_6()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_6_mF3C1ADA1CDBE331B70211BD0D86506A4F939D14B (void);
// 0x00000113 System.Void ActionConnectivityTestingScene::<UStart>b__54_7()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_7_m95E632F07D5AC64258EFFE30BEE8842A11513A54 (void);
// 0x00000114 System.Void ActionConnectivityTestingScene::<UStart>b__54_8()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_8_m83FAEAF90AD367CB3917A77A13D07E6EABDDF2B3 (void);
// 0x00000115 System.Void ActionConnectivityTestingScene::<UStart>b__54_9()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_9_m5BD82AA5407C1B1E028943EA2CE16E3DFC4B1C26 (void);
// 0x00000116 System.Void ActionConnectivityTestingScene::<UStart>b__54_10()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_10_m5721E8DA95C149186072C891D138489FECFB7F84 (void);
// 0x00000117 System.Void ActionConnectivityTestingScene::<UStart>b__54_11()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_11_m0F7DFDA9CC4D4600E70B5CC39FB7CE883F06BD68 (void);
// 0x00000118 System.Void ActionConnectivityTestingScene::<UStart>b__54_12()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_12_m1D81AF41EB55C8DEB3B5808928A347827AB45AAD (void);
// 0x00000119 System.Void ActionConnectivityTestingScene::<UStart>b__54_13()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_13_mCB45D92AD96CA650C52BA5076B4967CDA5D65297 (void);
// 0x0000011A System.Void ActionConnectivityTestingScene::<UStart>b__54_14(System.String)
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_14_m398F6C8C5D5E6673137EC0FCBAB636239E87253B (void);
// 0x0000011B System.Void ActionConnectivityTestingScene::<UStart>b__54_15(System.String)
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_15_mBC0B47724E8345CBDC2E8ABA4554676B18152FE0 (void);
// 0x0000011C System.Void ActionConnectivityTestingScene::<UStart>b__54_16(System.String)
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_16_mBC9B10E379B14186D7C0590094F9967D2A497AAF (void);
// 0x0000011D System.Void ActionConnectivityTestingScene::<UStart>b__54_17(System.String)
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_17_mBFD559418E61C4EBCBD3137580E4DDC21F6AC846 (void);
// 0x0000011E System.Void ActionConnectivityTestingScene::<UStart>b__54_18()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_18_m04E5CB22703C4B4D3CECA506132733DC497DEFC2 (void);
// 0x0000011F System.Void ActionConnectivityTestingScene::<UStart>b__54_19()
extern void ActionConnectivityTestingScene_U3CUStartU3Eb__54_19_m1A74AE701CDE1F0CE2750C4F2B2B4FACB9D9E8B6 (void);
// 0x00000120 System.Void AutomaticConnection::UStart()
extern void AutomaticConnection_UStart_m7AAE51172F1AD1FC57845F9842F158F6D13ADA66 (void);
// 0x00000121 System.Void AutomaticConnection::_SendActions()
extern void AutomaticConnection__SendActions_m1E9B6750869427C39CA6954F36EC12B427C8F8E5 (void);
// 0x00000122 System.Void AutomaticConnection::_ConnectToAddress()
extern void AutomaticConnection__ConnectToAddress_m83132FC7925FA137CE6B61F22E27DB0AEB06929C (void);
// 0x00000123 System.Void AutomaticConnection::ShowWarning(System.String)
extern void AutomaticConnection_ShowWarning_mD9B8F04D8C16069EEA231E55A9687878AF5B0BFE (void);
// 0x00000124 System.Void AutomaticConnection::HideWarning()
extern void AutomaticConnection_HideWarning_mB6A018368C561F9F257E04BD59399EF0F6515D8D (void);
// 0x00000125 System.Void AutomaticConnection::_ButtonPressed(RobotAction/MotionAction,UnityEngine.UI.Button)
extern void AutomaticConnection__ButtonPressed_mE0F3423216A50AD279E33AF2C83E7555C634C661 (void);
// 0x00000126 System.Void AutomaticConnection::_ButtonPressed(RobotAction/ArmsAction,UnityEngine.UI.Button)
extern void AutomaticConnection__ButtonPressed_m82FF526DB35CFC61130083408F7A926FD2BD5C7E (void);
// 0x00000127 System.Void AutomaticConnection::_ButtonPressed(RobotAction/EmotionAction,UnityEngine.UI.Button)
extern void AutomaticConnection__ButtonPressed_m497F20F54D85D52445EB28096D2AF4800C41D408 (void);
// 0x00000128 System.Void AutomaticConnection::_ButtonPressed(RobotAction/SoundAction,UnityEngine.UI.Button)
extern void AutomaticConnection__ButtonPressed_m7EEBDFBF8CF7E3EE34B781C9145DCE8C5E4A7C36 (void);
// 0x00000129 System.Void AutomaticConnection::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
extern void AutomaticConnection_OnBluetoothDeviceListUpdated_m71682EA1F409371FFFCB227194F50D24C5E89C48 (void);
// 0x0000012A System.Void AutomaticConnection::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void AutomaticConnection_OnBluetoothConnected_mED6845F11E89716085766F9B7DC173B9A798D3D0 (void);
// 0x0000012B System.Void AutomaticConnection::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void AutomaticConnection_OnBluetoothDisconnected_mA56BF756CAFC5FE06749D0E00EFC94FEBB628F11 (void);
// 0x0000012C System.Void AutomaticConnection::.ctor()
extern void AutomaticConnection__ctor_m44D1B22BA97E5A38E0341CEB67D9DE7E9B71B03B (void);
// 0x0000012D System.Void AutomaticConnection::<UStart>b__43_0()
extern void AutomaticConnection_U3CUStartU3Eb__43_0_mF765595BC70D9AB5C7C03D6A8681D2C0C32E06D8 (void);
// 0x0000012E System.Void AutomaticConnection::<UStart>b__43_1()
extern void AutomaticConnection_U3CUStartU3Eb__43_1_m0D38D92FAAEA2D59DE40B37129245BCC9AF1D2FD (void);
// 0x0000012F System.Void AutomaticConnection::<UStart>b__43_2()
extern void AutomaticConnection_U3CUStartU3Eb__43_2_m6EB30D7F6A9D8461CB61736E37B83AECA92FF14A (void);
// 0x00000130 System.Void AutomaticConnection::<UStart>b__43_3()
extern void AutomaticConnection_U3CUStartU3Eb__43_3_mB36784BB77AA7F318DB5F51974499A5237678E27 (void);
// 0x00000131 System.Void AutomaticConnection::<UStart>b__43_4()
extern void AutomaticConnection_U3CUStartU3Eb__43_4_mFD73030E15B39CDB5121F6AF69FCBF88997EA7B0 (void);
// 0x00000132 System.Void AutomaticConnection::<UStart>b__43_5()
extern void AutomaticConnection_U3CUStartU3Eb__43_5_m5B3548488ECE0739212CE3E22FBB31C1DC459004 (void);
// 0x00000133 System.Void AutomaticConnection::<UStart>b__43_6()
extern void AutomaticConnection_U3CUStartU3Eb__43_6_mA47D36D860E942BAEDA27D6AC40FEA4D9355D460 (void);
// 0x00000134 System.Void AutomaticConnection::<UStart>b__43_7()
extern void AutomaticConnection_U3CUStartU3Eb__43_7_m80697AAA5F82D662613F43ADB6C98E16E5470BFE (void);
// 0x00000135 System.Void AutomaticConnection::<UStart>b__43_8()
extern void AutomaticConnection_U3CUStartU3Eb__43_8_m48DBAF739BA5E10313DA7DE8A9C8A34987B17483 (void);
// 0x00000136 System.Void AutomaticConnection::<UStart>b__43_9()
extern void AutomaticConnection_U3CUStartU3Eb__43_9_mE9233D6FBC3F3863F27B3307C95412B8DFF9E6CE (void);
// 0x00000137 System.Void AutomaticConnection::<UStart>b__43_10()
extern void AutomaticConnection_U3CUStartU3Eb__43_10_m7D54EFC7C83EAEA77F519E62751FE4793BBEC2F8 (void);
// 0x00000138 System.Void AutomaticConnection::<UStart>b__43_11()
extern void AutomaticConnection_U3CUStartU3Eb__43_11_m6FA76EEA22B437115FE7BC6D27A164FA72FB7129 (void);
// 0x00000139 System.Void AutomaticConnection::<UStart>b__43_12()
extern void AutomaticConnection_U3CUStartU3Eb__43_12_mC101F7834F4EDAE0A853D75A9F95F9A5895994A6 (void);
// 0x0000013A System.Void AutomaticConnection::<UStart>b__43_13()
extern void AutomaticConnection_U3CUStartU3Eb__43_13_mBEDAAA76CE5DC7ACAD3C3E73D2395E07C66DFBD4 (void);
// 0x0000013B System.Void AutomaticConnection::<UStart>b__43_14(System.String)
extern void AutomaticConnection_U3CUStartU3Eb__43_14_m6587C607FA38D1B19B0216EC9EA696323736579C (void);
// 0x0000013C System.Void AutomaticConnection::<UStart>b__43_15(System.String)
extern void AutomaticConnection_U3CUStartU3Eb__43_15_m4317D9CD25D5EA7761CBA23B15CC8DC954F3D15C (void);
// 0x0000013D System.Void AutomaticConnection::<UStart>b__43_16(System.String)
extern void AutomaticConnection_U3CUStartU3Eb__43_16_m311283953885D7ADD516FD5EAE490C494282F03F (void);
// 0x0000013E System.Void AutomaticConnection::<UStart>b__43_17(System.String)
extern void AutomaticConnection_U3CUStartU3Eb__43_17_m9C0C06F8766024C8639A446D869730FC9928518D (void);
// 0x0000013F System.Void GyroTestingScene::UStart()
extern void GyroTestingScene_UStart_m396F7BBD9250B27275E0579EFA14E956ED89BA6B (void);
// 0x00000140 System.Void GyroTestingScene::_PlayButton()
extern void GyroTestingScene__PlayButton_m89ED6BF30E6E28EA7FCD96DAA229DF55DD980E99 (void);
// 0x00000141 System.Void GyroTestingScene::_StopButton()
extern void GyroTestingScene__StopButton_mDA899F7EE46C92C208427A89F51D4C3828A7ACE1 (void);
// 0x00000142 System.Void GyroTestingScene::OnNewGyroscopeStatus(System.Single,System.Single)
extern void GyroTestingScene_OnNewGyroscopeStatus_m891DDA7E7346DDDCED6D029AE6003FBDED673D03 (void);
// 0x00000143 System.Void GyroTestingScene::.ctor()
extern void GyroTestingScene__ctor_m056A62668679A2A27A96952EC35EC338D7F5CEF8 (void);
// 0x00000144 System.Void PlayingRoutineTestingScene::UStart()
extern void PlayingRoutineTestingScene_UStart_m38E264340404821113B2EF1B032B12CFFAF9C99A (void);
// 0x00000145 System.Void PlayingRoutineTestingScene::_PlayButton()
extern void PlayingRoutineTestingScene__PlayButton_m3939D6CC4DE476DE8C50A19BB41C4A682F6EEF73 (void);
// 0x00000146 System.Void PlayingRoutineTestingScene::OnNewStepPlayed(System.Int32,RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction)
extern void PlayingRoutineTestingScene_OnNewStepPlayed_mBB086569ADA31C71B1A21A322378A8CC0C6FDDAD (void);
// 0x00000147 System.Void PlayingRoutineTestingScene::.ctor()
extern void PlayingRoutineTestingScene__ctor_m11D258AA28ED5505EA8B20D7469F28ACC4761D5A (void);
// 0x00000148 System.Void StorageActionsTestingScene::UStart()
extern void StorageActionsTestingScene_UStart_m9ACB433ACE733D29DF5962C7784AA2951C4CCCAB (void);
// 0x00000149 System.Void StorageActionsTestingScene::OnRoutineLoaded(System.Boolean,SavedRoutine,RobotAction/MotionAction[],RobotAction/ArmsAction[],RobotAction/EmotionAction[],RobotAction/SoundAction[])
extern void StorageActionsTestingScene_OnRoutineLoaded_mC8EADD17BE7F18CF42F87EDC8565BD20B9057C52 (void);
// 0x0000014A System.Void StorageActionsTestingScene::OnRoutineSaved(System.Boolean,SavedRoutine)
extern void StorageActionsTestingScene_OnRoutineSaved_m585AFCA66AA698A148FF1DDC35B0ED2B5FA33B9C (void);
// 0x0000014B System.Void StorageActionsTestingScene::.ctor()
extern void StorageActionsTestingScene__ctor_m991B1647A9A28E44886FC16664B472A69800A18D (void);
// 0x0000014C System.Void TestArRobotLogicConnectivityScene::UStart()
extern void TestArRobotLogicConnectivityScene_UStart_m9E08D191A05B874080AD80BBA1A321EC5AE3EEB5 (void);
// 0x0000014D System.Void TestArRobotLogicConnectivityScene::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
extern void TestArRobotLogicConnectivityScene_OnBluetoothDeviceListUpdated_m471528E81052DBBA63AFA21EB5D17999D08426DC (void);
// 0x0000014E System.Void TestArRobotLogicConnectivityScene::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void TestArRobotLogicConnectivityScene_OnBluetoothConnected_m29A53A35411CCC655E3018FB4C52A3936DF98426 (void);
// 0x0000014F System.Void TestArRobotLogicConnectivityScene::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void TestArRobotLogicConnectivityScene_OnBluetoothDisconnected_mF6A7BC471B732FC0483C574458380DA1AE36CA74 (void);
// 0x00000150 System.Void TestArRobotLogicConnectivityScene::_OnConnectButtonPressed()
extern void TestArRobotLogicConnectivityScene__OnConnectButtonPressed_mA8F6E4508B658622144357D879BFB7C3E49AE0C4 (void);
// 0x00000151 System.Void TestArRobotLogicConnectivityScene::_SendMessage()
extern void TestArRobotLogicConnectivityScene__SendMessage_m6EEA66E853C9DE178FFC22B26C8431FCD58DABC9 (void);
// 0x00000152 System.Void TestArRobotLogicConnectivityScene::.ctor()
extern void TestArRobotLogicConnectivityScene__ctor_m43488CDE0FE99AD8FAF316DE6C209DFDACD76B64 (void);
// 0x00000153 System.Void TestArRobotLogicConnectivityScene::<UStart>b__9_0()
extern void TestArRobotLogicConnectivityScene_U3CUStartU3Eb__9_0_mF06F70BF9F06DE13A687E91531C1A1AFF8B4C07E (void);
// 0x00000154 System.Void TestArRobotLogicConnectivityScene::<UStart>b__9_1()
extern void TestArRobotLogicConnectivityScene_U3CUStartU3Eb__9_1_m90189EC68363249E9BDDEC170FF8871133065497 (void);
// 0x00000155 System.Void TestArRobotLogicConnectivityScene::<UStart>b__9_2()
extern void TestArRobotLogicConnectivityScene_U3CUStartU3Eb__9_2_mC80B5635C8230A4DB52BE390F79DA4E4F921685F (void);
// 0x00000156 System.Void ArRobotUIMod::UStart()
extern void ArRobotUIMod_UStart_mEF519500F791847FF11D2A6EBA97B0EDAD8E404C (void);
// 0x00000157 System.Void ArRobotUIMod::UUpdate()
extern void ArRobotUIMod_UUpdate_m7D73F19E5EB5F95F38433F0B80AF6006C72B5C19 (void);
// 0x00000158 System.Void ArRobotUIMod::OnListenersAndModRegistration()
extern void ArRobotUIMod_OnListenersAndModRegistration_mE9B7B437BA7963A558A4B1A365CE562E1ED70B50 (void);
// 0x00000159 System.Void ArRobotUIMod::OnDestroyListenersAndModUnregistration()
extern void ArRobotUIMod_OnDestroyListenersAndModUnregistration_m9654CE1C2C95E89AB7698F7FEFD3840F127A406A (void);
// 0x0000015A System.Void ArRobotUIMod::OnRoutinesListReceived(System.Collections.Generic.List`1<SavedRoutine>)
extern void ArRobotUIMod_OnRoutinesListReceived_mDADF51C0E8926AA0A6AB6FCAE0C3EA5D7F54695F (void);
// 0x0000015B System.Void ArRobotUIMod::OnRoutineLoaded(System.Boolean,SavedRoutine,RobotAction/MotionAction[],RobotAction/ArmsAction[],RobotAction/EmotionAction[],RobotAction/SoundAction[])
extern void ArRobotUIMod_OnRoutineLoaded_m47177F2123FB79E3C117E996FF025354697D5861 (void);
// 0x0000015C System.Void ArRobotUIMod::OnRoutineSaved(System.Boolean,SavedRoutine)
extern void ArRobotUIMod_OnRoutineSaved_m27891E846AF6EFF5C833A99EB75E51FF487DD291 (void);
// 0x0000015D System.Void ArRobotUIMod::.ctor()
extern void ArRobotUIMod__ctor_mF7411B84ABB0A7259FDA5CC594AFE4FB38DD0781 (void);
// 0x0000015E System.Void PreloadScene::UStart()
extern void PreloadScene_UStart_mAB547A62766E02D3DA96ADE25372B25131D5A0F2 (void);
// 0x0000015F System.Void PreloadScene::.ctor()
extern void PreloadScene__ctor_m672134C8D3ECBC944DC5162A507FF29605A5DE29 (void);
// 0x00000160 System.Void DancingModeButtonScript::Start()
extern void DancingModeButtonScript_Start_m21256A0ED1A36F8E0FEB70D33EFE32B941D7F7FB (void);
// 0x00000161 System.Void DancingModeButtonScript::ToState(System.Boolean)
extern void DancingModeButtonScript_ToState_mDBABC1C1C6D1BC0622CB5ECE6D13EC10C5CBE972 (void);
// 0x00000162 System.Void DancingModeButtonScript::.ctor()
extern void DancingModeButtonScript__ctor_m177B14B4AEEB334A8E7BBB79BD6B0ABEA9EC6BFC (void);
// 0x00000163 System.Void UIAboutAppScene::UITopButtonAction(SceneActionType)
extern void UIAboutAppScene_UITopButtonAction_m40F1B64FA6ADCD1DFE52A8FB774564795A39BDA6 (void);
// 0x00000164 System.Void UIAboutAppScene::.ctor()
extern void UIAboutAppScene__ctor_mF5BB81ED979046B5E7763817CE6B58B8DAD353FE (void);
// 0x00000165 System.Void UIDanceModeScene::UITopButtonAction(SceneActionType)
extern void UIDanceModeScene_UITopButtonAction_m9D40F6B7593B9F392EF563B7E3AD6954B5B77B95 (void);
// 0x00000166 System.Void UIDanceModeScene::.ctor()
extern void UIDanceModeScene__ctor_m21ADBF7D1674D093895A2A8EA6152C7EFC59EF57 (void);
// 0x00000167 System.Void UIDancingModePhotoScene::UAwake()
extern void UIDancingModePhotoScene_UAwake_mD227190894148CD5B985F334EBA9E16F0A9E474F (void);
// 0x00000168 System.Void UIDancingModePhotoScene::UStart()
extern void UIDancingModePhotoScene_UStart_mCC2FF574C2313739A90FA0BD32A413419A02BED9 (void);
// 0x00000169 System.Void UIDancingModePhotoScene::_PhotoButtonPressed()
extern void UIDancingModePhotoScene__PhotoButtonPressed_m6E24590A30480FABD356FB8AA019171E40F9985C (void);
// 0x0000016A System.Void UIDancingModePhotoScene::_VideoButtonPressed()
extern void UIDancingModePhotoScene__VideoButtonPressed_mD2BADA17FD4D5576B0FCFDF2D7980EFFE5F5F268 (void);
// 0x0000016B System.Void UIDancingModePhotoScene::OnButtonPressed(DancingModeButtonScript)
extern void UIDancingModePhotoScene_OnButtonPressed_m8D7C70FFD2E6EC6EE3479D0F0D460F4ECA886151 (void);
// 0x0000016C System.Void UIDancingModePhotoScene::StopDancing()
extern void UIDancingModePhotoScene_StopDancing_m87EBB982B1E005796C37CDB3E38B3F89825B128D (void);
// 0x0000016D System.Void UIDancingModePhotoScene::UITopButtonAction(SceneActionType)
extern void UIDancingModePhotoScene_UITopButtonAction_mDAC3E0ACE66AED4627E5813DEDC81FA9BABAD11E (void);
// 0x0000016E System.Collections.IEnumerator UIDancingModePhotoScene::OnEndSongCoroutine(System.Single)
extern void UIDancingModePhotoScene_OnEndSongCoroutine_m89F290715955EB9FA611697CB83DA48E49580E7E (void);
// 0x0000016F System.Void UIDancingModePhotoScene::.ctor()
extern void UIDancingModePhotoScene__ctor_m0E148E32C4399D5DED1C286566D347F2932B9DBC (void);
// 0x00000170 System.Void UIDancingModePhotoScene::<UStart>b__7_0()
extern void UIDancingModePhotoScene_U3CUStartU3Eb__7_0_m375ED372FBF72A93993944759D422B8050086BB7 (void);
// 0x00000171 System.Void UIDancingModePhotoScene::<UStart>b__7_1()
extern void UIDancingModePhotoScene_U3CUStartU3Eb__7_1_m3A0D131E331ABCC4569A4988CA42005C1439D3DB (void);
// 0x00000172 System.Void UIDancingModePhotoScene/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mDE9A66AAAA140145FA9F822B9144C7E2512C5BAE (void);
// 0x00000173 System.Void UIDancingModePhotoScene/<>c__DisplayClass7_0::<UStart>b__2()
extern void U3CU3Ec__DisplayClass7_0_U3CUStartU3Eb__2_mEE2FE10D0A4D801F5172BD1BA07823E53D0DA54F (void);
// 0x00000174 System.Void UIDancingModePhotoScene/<OnEndSongCoroutine>d__17::.ctor(System.Int32)
extern void U3COnEndSongCoroutineU3Ed__17__ctor_mB70D8A90057205572BE3F7D5E9FECA6EAFBE7493 (void);
// 0x00000175 System.Void UIDancingModePhotoScene/<OnEndSongCoroutine>d__17::System.IDisposable.Dispose()
extern void U3COnEndSongCoroutineU3Ed__17_System_IDisposable_Dispose_m0204E67F0D0C67B569D6BD92E85E0963C9597F7E (void);
// 0x00000176 System.Boolean UIDancingModePhotoScene/<OnEndSongCoroutine>d__17::MoveNext()
extern void U3COnEndSongCoroutineU3Ed__17_MoveNext_m9D349CFF4726015BCDAB4AB267366E005DC7E075 (void);
// 0x00000177 System.Object UIDancingModePhotoScene/<OnEndSongCoroutine>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnEndSongCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29CFCDFB1EDEE5A805D444A042BD2ADECECF7C86 (void);
// 0x00000178 System.Void UIDancingModePhotoScene/<OnEndSongCoroutine>d__17::System.Collections.IEnumerator.Reset()
extern void U3COnEndSongCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m7C669CA62E616A1AE62E560ABD72A8322DB9682D (void);
// 0x00000179 System.Object UIDancingModePhotoScene/<OnEndSongCoroutine>d__17::System.Collections.IEnumerator.get_Current()
extern void U3COnEndSongCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m98D62F2AA5A03863FFE34C373FA39E664E84B5DE (void);
// 0x0000017A System.Void UIDancingModeScene::UStart()
extern void UIDancingModeScene_UStart_m2018258783A1D1972B9DB41E4632C28CE1EC77D9 (void);
// 0x0000017B System.Void UIDancingModeScene::OnButtonPressed(DancingModeButtonScript)
extern void UIDancingModeScene_OnButtonPressed_mB9361184BBFE1E579EA07A9440CAE05D8EECE9B4 (void);
// 0x0000017C System.Void UIDancingModeScene::OnEnable()
extern void UIDancingModeScene_OnEnable_m169D5C6F6106DA9826269DD24DCB7A6D62875C51 (void);
// 0x0000017D System.Void UIDancingModeScene::StopDancing()
extern void UIDancingModeScene_StopDancing_m8239DA899AFCFDED45BF1774CDF261AA5424251B (void);
// 0x0000017E System.Void UIDancingModeScene::UITopButtonAction(SceneActionType)
extern void UIDancingModeScene_UITopButtonAction_m631A9FD1973005DC88516F12F4B7DA51E50B384D (void);
// 0x0000017F System.Collections.IEnumerator UIDancingModeScene::OnEndSongCoroutine(System.Single)
extern void UIDancingModeScene_OnEndSongCoroutine_m8340878C6F8464ABCF1F114ACC192B84B5F5CD40 (void);
// 0x00000180 System.Void UIDancingModeScene::.ctor()
extern void UIDancingModeScene__ctor_m56E1A9362F99540A3F40D35C8DAEF077CBF05DEB (void);
// 0x00000181 System.Void UIDancingModeScene/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m9CF705BA026E90FFBCCE02BE0AADD2EC2ACD78CE (void);
// 0x00000182 System.Void UIDancingModeScene/<>c__DisplayClass7_0::<UStart>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CUStartU3Eb__0_m9F6D0565A7B74A48936D4E168117C5DDB817D0E2 (void);
// 0x00000183 System.Void UIDancingModeScene/<OnEndSongCoroutine>d__12::.ctor(System.Int32)
extern void U3COnEndSongCoroutineU3Ed__12__ctor_mD8486A36A5BC9054806B48AAA7EA84F97B18F87A (void);
// 0x00000184 System.Void UIDancingModeScene/<OnEndSongCoroutine>d__12::System.IDisposable.Dispose()
extern void U3COnEndSongCoroutineU3Ed__12_System_IDisposable_Dispose_m726CCD8F8B06BD56E629EF882F79C010C59F8A42 (void);
// 0x00000185 System.Boolean UIDancingModeScene/<OnEndSongCoroutine>d__12::MoveNext()
extern void U3COnEndSongCoroutineU3Ed__12_MoveNext_mFB04A6B16A970FE4E2FA3E640C93C4AA7CE55F2F (void);
// 0x00000186 System.Object UIDancingModeScene/<OnEndSongCoroutine>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnEndSongCoroutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D905BC0896E98EAB9DFB23CC59A390BB9BC169E (void);
// 0x00000187 System.Void UIDancingModeScene/<OnEndSongCoroutine>d__12::System.Collections.IEnumerator.Reset()
extern void U3COnEndSongCoroutineU3Ed__12_System_Collections_IEnumerator_Reset_m47A62F444AC70D86A665237F2782BDCE2E558499 (void);
// 0x00000188 System.Object UIDancingModeScene/<OnEndSongCoroutine>d__12::System.Collections.IEnumerator.get_Current()
extern void U3COnEndSongCoroutineU3Ed__12_System_Collections_IEnumerator_get_Current_m646069AD90BE12F77928402EC30EDA2ED2502765 (void);
// 0x00000189 System.Void UIGyroscopeModeScene::UStart()
extern void UIGyroscopeModeScene_UStart_m141E18F7C4DDC6C6C50FACB95F7138433DA69CAD (void);
// 0x0000018A System.Void UIGyroscopeModeScene::AddActionToButton(UnityEngine.UI.Button,UnityEngine.EventSystems.EventTriggerType,System.Action)
extern void UIGyroscopeModeScene_AddActionToButton_mDB0723318B41EA0E27FE5D8CA33A27D2839B9B0F (void);
// 0x0000018B System.Void UIGyroscopeModeScene::UITopButtonAction(SceneActionType)
extern void UIGyroscopeModeScene_UITopButtonAction_mC913F8D63C74FDFE05059A49104074F246ED198E (void);
// 0x0000018C System.Void UIGyroscopeModeScene::OnNewGyroscopeStatus(System.Single,System.Single)
extern void UIGyroscopeModeScene_OnNewGyroscopeStatus_m69C748815BC7EDB97C261558D6A63A436BDC863B (void);
// 0x0000018D System.Void UIGyroscopeModeScene::PlaySound(System.Int32)
extern void UIGyroscopeModeScene_PlaySound_m13AD3F46645AA7CA99CFB9379304680753151BEC (void);
// 0x0000018E System.Collections.IEnumerator UIGyroscopeModeScene::SoundCoroutine()
extern void UIGyroscopeModeScene_SoundCoroutine_mAA185400DB8131AC9FFDCDC44AB734DADD799EA9 (void);
// 0x0000018F System.Void UIGyroscopeModeScene::StopSound()
extern void UIGyroscopeModeScene_StopSound_m516A9528471D3CDF59AF05588F87C763753A3AD1 (void);
// 0x00000190 System.Collections.IEnumerator UIGyroscopeModeScene::StabilizeCoroutine(System.Single)
extern void UIGyroscopeModeScene_StabilizeCoroutine_m36476B9CCDFE1316FEAAD31D180373CC35898089 (void);
// 0x00000191 System.Void UIGyroscopeModeScene::.ctor()
extern void UIGyroscopeModeScene__ctor_m9CB896303BEB61EFE5F1EC50594590C20F626398 (void);
// 0x00000192 System.Void UIGyroscopeModeScene::<UStart>b__23_0()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_0_mF36264461536DBBFD6DE7ECD9D1E34CAAB0E929F (void);
// 0x00000193 System.Void UIGyroscopeModeScene::<UStart>b__23_1()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_1_mB873D32976C6FD25E7E58602B2952434CF6701C1 (void);
// 0x00000194 System.Void UIGyroscopeModeScene::<UStart>b__23_2()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_2_m760EF6D69B373302935ECF25E0B9A3A54A506063 (void);
// 0x00000195 System.Void UIGyroscopeModeScene::<UStart>b__23_3()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_3_mBA768F734127A4854707420560ECD39D6F6106A4 (void);
// 0x00000196 System.Void UIGyroscopeModeScene::<UStart>b__23_4()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_4_mB65E6D27EDC5EFA2B12BB10F19791955BBFAD347 (void);
// 0x00000197 System.Void UIGyroscopeModeScene::<UStart>b__23_5()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_5_mC075B9F03AE9E611D0CB19026C01DB0398069837 (void);
// 0x00000198 System.Void UIGyroscopeModeScene::<UStart>b__23_6()
extern void UIGyroscopeModeScene_U3CUStartU3Eb__23_6_m9DBEF4130BE5DAFE7F8223803D32597592F1AA31 (void);
// 0x00000199 System.Void UIGyroscopeModeScene/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mB69F80D478CD2025F11C5C039B9326A23C463E81 (void);
// 0x0000019A System.Void UIGyroscopeModeScene/<>c__DisplayClass23_0::<UStart>b__7()
extern void U3CU3Ec__DisplayClass23_0_U3CUStartU3Eb__7_m72A128EAE3013D40C49507BCDE1D24EE87E3BC27 (void);
// 0x0000019B System.Void UIGyroscopeModeScene/<>c__DisplayClass23_1::.ctor()
extern void U3CU3Ec__DisplayClass23_1__ctor_m66B5600E52FCBC18B851A7681A54C42CBD25C4B7 (void);
// 0x0000019C System.Void UIGyroscopeModeScene/<>c__DisplayClass23_1::<UStart>b__8()
extern void U3CU3Ec__DisplayClass23_1_U3CUStartU3Eb__8_m22323C02BEB1FB96687AD0BF1B06A973E533CE7E (void);
// 0x0000019D System.Void UIGyroscopeModeScene/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mB3520F95D5D7C040F26E1EE68BBF4A4E1D2080B6 (void);
// 0x0000019E System.Void UIGyroscopeModeScene/<>c__DisplayClass24_0::<AddActionToButton>b__0(UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec__DisplayClass24_0_U3CAddActionToButtonU3Eb__0_mD6BBDF719F3E635D85C7FD7837DA129B35E3CE44 (void);
// 0x0000019F System.Void UIGyroscopeModeScene/<SoundCoroutine>d__34::.ctor(System.Int32)
extern void U3CSoundCoroutineU3Ed__34__ctor_m3DD5DD5C7E038C247B87D17A23FAE279CDA28A6B (void);
// 0x000001A0 System.Void UIGyroscopeModeScene/<SoundCoroutine>d__34::System.IDisposable.Dispose()
extern void U3CSoundCoroutineU3Ed__34_System_IDisposable_Dispose_m1D4387776066F998CDF90927917B7DCF7234B66B (void);
// 0x000001A1 System.Boolean UIGyroscopeModeScene/<SoundCoroutine>d__34::MoveNext()
extern void U3CSoundCoroutineU3Ed__34_MoveNext_m162CA23B3CD8E6FB39F36C1AD2C1E4C4C08581CD (void);
// 0x000001A2 System.Object UIGyroscopeModeScene/<SoundCoroutine>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSoundCoroutineU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m440B3DE2257AC59C3AEB36729A8BFDB018339206 (void);
// 0x000001A3 System.Void UIGyroscopeModeScene/<SoundCoroutine>d__34::System.Collections.IEnumerator.Reset()
extern void U3CSoundCoroutineU3Ed__34_System_Collections_IEnumerator_Reset_m956302EE2A28A6248C6351636C9FB598CF071010 (void);
// 0x000001A4 System.Object UIGyroscopeModeScene/<SoundCoroutine>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CSoundCoroutineU3Ed__34_System_Collections_IEnumerator_get_Current_m73611655B954D5226F1209F5B492DF8B4ED6F4B1 (void);
// 0x000001A5 System.Void UIGyroscopeModeScene/<StabilizeCoroutine>d__36::.ctor(System.Int32)
extern void U3CStabilizeCoroutineU3Ed__36__ctor_mA29524B1BDFC05A3F9EC1239D66DCC9B693F32C7 (void);
// 0x000001A6 System.Void UIGyroscopeModeScene/<StabilizeCoroutine>d__36::System.IDisposable.Dispose()
extern void U3CStabilizeCoroutineU3Ed__36_System_IDisposable_Dispose_m3CE2330EFF0DF2A8154E04C138E80626271CC858 (void);
// 0x000001A7 System.Boolean UIGyroscopeModeScene/<StabilizeCoroutine>d__36::MoveNext()
extern void U3CStabilizeCoroutineU3Ed__36_MoveNext_m736BCADF61F146881D5A6C97644133D5D6165E42 (void);
// 0x000001A8 System.Object UIGyroscopeModeScene/<StabilizeCoroutine>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStabilizeCoroutineU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE1BA0E4C8497B7586AB00AB8D8648787E11BD22 (void);
// 0x000001A9 System.Void UIGyroscopeModeScene/<StabilizeCoroutine>d__36::System.Collections.IEnumerator.Reset()
extern void U3CStabilizeCoroutineU3Ed__36_System_Collections_IEnumerator_Reset_mAE912BA59414010E9D3605F33DD64DBB50792560 (void);
// 0x000001AA System.Object UIGyroscopeModeScene/<StabilizeCoroutine>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CStabilizeCoroutineU3Ed__36_System_Collections_IEnumerator_get_Current_m52C25F38575633C085680BBAF0D9074C6B2993DA (void);
// 0x000001AB System.Void UIMainMenuScene::UStart()
extern void UIMainMenuScene_UStart_m96B69A9F1B60338E16539BD62975A25F1EFD4E13 (void);
// 0x000001AC System.Void UIMainMenuScene::AskAndroidPerssions()
extern void UIMainMenuScene_AskAndroidPerssions_m523BEEF3F6691967439571D694BDA3AA271CB919 (void);
// 0x000001AD System.Void UIMainMenuScene::_OnProgrammingModeClicked()
extern void UIMainMenuScene__OnProgrammingModeClicked_m7CA146E31C11DE55382BE971F4695D2A5856BC46 (void);
// 0x000001AE System.Void UIMainMenuScene::_OnSettingsClicked()
extern void UIMainMenuScene__OnSettingsClicked_m07E785F58BF8C56F861FE044C2241766380E2271 (void);
// 0x000001AF System.Void UIMainMenuScene::UITopButtonAction(SceneActionType)
extern void UIMainMenuScene_UITopButtonAction_m70F4439FF306C42D5BC44E36CE9A27799D38F067 (void);
// 0x000001B0 System.Void UIMainMenuScene::_OnDancingModeClicked()
extern void UIMainMenuScene__OnDancingModeClicked_mF8977A72013DB80B3B67B133011A1BFB11A0F051 (void);
// 0x000001B1 System.Void UIMainMenuScene::.ctor()
extern void UIMainMenuScene__ctor_mF8934EDDF01C9ECD3BC3B5DCD25C041B2BEF6D24 (void);
// 0x000001B2 System.Void UIMainMenuScene::<UStart>b__11_0()
extern void UIMainMenuScene_U3CUStartU3Eb__11_0_mB98FC33B713F5BCAECF17FD9DF03A0CA33154C01 (void);
// 0x000001B3 System.Void UIMainMenuScene::<UStart>b__11_1()
extern void UIMainMenuScene_U3CUStartU3Eb__11_1_m4EBF693F78C4B05E36B9C0F8C962635687EC1E64 (void);
// 0x000001B4 System.Void UIMainMenuScene::<AskAndroidPerssions>b__14_0(System.String)
extern void UIMainMenuScene_U3CAskAndroidPerssionsU3Eb__14_0_m6A92B97C824432FA02AE967702F3E6C0CFAD24BD (void);
// 0x000001B5 System.Void UIMainMenuScene::<AskAndroidPerssions>b__14_1(System.String)
extern void UIMainMenuScene_U3CAskAndroidPerssionsU3Eb__14_1_m8B2B0FF7845258FF4736F0820FE8A83183EA0245 (void);
// 0x000001B6 System.Void UIMainMenuScene::<AskAndroidPerssions>b__14_2(System.String)
extern void UIMainMenuScene_U3CAskAndroidPerssionsU3Eb__14_2_mB629A0DD26F0765563942FE0508078C9A49F7212 (void);
// 0x000001B7 System.Void UIProgrammingModePhotoScene::UAwake()
extern void UIProgrammingModePhotoScene_UAwake_m2BF4E651D7C6D933E895233765DEAA312EF1647F (void);
// 0x000001B8 System.Void UIProgrammingModePhotoScene::UStart()
extern void UIProgrammingModePhotoScene_UStart_m72052516C36DD24EE8E43F1DCFECA72E862F4556 (void);
// 0x000001B9 System.Void UIProgrammingModePhotoScene::_PowerButtonPressed()
extern void UIProgrammingModePhotoScene__PowerButtonPressed_mEB5D207D4BE1ABB5EC7DD2395D678BAD3D37261B (void);
// 0x000001BA System.Void UIProgrammingModePhotoScene::_PhotoButtonPressed()
extern void UIProgrammingModePhotoScene__PhotoButtonPressed_m36AFC0D8DF08D6BF940951E9D4F76C0A738F6C0D (void);
// 0x000001BB System.Void UIProgrammingModePhotoScene::_VideoButtonPressed()
extern void UIProgrammingModePhotoScene__VideoButtonPressed_mB313A1AC1D048DD07D201EC7EE0D31C9B44FF007 (void);
// 0x000001BC System.Void UIProgrammingModePhotoScene::_SetRestButtonPlaying(System.Boolean)
extern void UIProgrammingModePhotoScene__SetRestButtonPlaying_m05C1B6A54CDEE38E2BA1F1DC2345DE45AE768536 (void);
// 0x000001BD System.Void UIProgrammingModePhotoScene::OnNewStepPlayed(System.Int32,RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction)
extern void UIProgrammingModePhotoScene_OnNewStepPlayed_m1EC4CD104724DFCA06AC3641442D7EA8D9F06ED8 (void);
// 0x000001BE System.Void UIProgrammingModePhotoScene::StopPlaying()
extern void UIProgrammingModePhotoScene_StopPlaying_m7302A09432612E21044CF3A300E6311DB0A39875 (void);
// 0x000001BF System.Void UIProgrammingModePhotoScene::UITopButtonAction(SceneActionType)
extern void UIProgrammingModePhotoScene_UITopButtonAction_mA8D582E0585A0F5641CF97BC752127F8096FCAB9 (void);
// 0x000001C0 System.Void UIProgrammingModePhotoScene::PlaySound(RobotAction/SoundAction)
extern void UIProgrammingModePhotoScene_PlaySound_mE0FD01B9D14646390558FC84C65D3FC6BD181F1C (void);
// 0x000001C1 System.Void UIProgrammingModePhotoScene::StopSound()
extern void UIProgrammingModePhotoScene_StopSound_m0B1FA961B8783E2EB07620F5708710A8C70E9486 (void);
// 0x000001C2 System.Void UIProgrammingModePhotoScene::.ctor()
extern void UIProgrammingModePhotoScene__ctor_m880EEDE20EE757C84B2E7F0CECCA624EBA852D0D (void);
// 0x000001C3 System.Void UIProgrammingModePhotoScene::<UStart>b__13_0()
extern void UIProgrammingModePhotoScene_U3CUStartU3Eb__13_0_m5E8B00DA3A247F8C5854BB4264969DBDF68A6465 (void);
// 0x000001C4 System.Void UIProgrammingModePhotoScene::<UStart>b__13_1()
extern void UIProgrammingModePhotoScene_U3CUStartU3Eb__13_1_m72181D1A9797F3153BA827953A89FA7CB528AB41 (void);
// 0x000001C5 System.Void UIProgrammingModePhotoScene::<UStart>b__13_2()
extern void UIProgrammingModePhotoScene_U3CUStartU3Eb__13_2_m0F236F06639AC1A9374E76060AA85D76096B8081 (void);
// 0x000001C6 System.Void UIProgrammingModePhotoScene::<_SetRestButtonPlaying>b__18_0()
extern void UIProgrammingModePhotoScene_U3C_SetRestButtonPlayingU3Eb__18_0_mEA66FE49FF17B7E8CDF8C920801470B32AE6CC73 (void);
// 0x000001C7 System.Void UIProgrammingModeScene::UAwake()
extern void UIProgrammingModeScene_UAwake_mE058E577EB8970BA6DE03BEC50EEAA89789BCDBA (void);
// 0x000001C8 System.Void UIProgrammingModeScene::UStart()
extern void UIProgrammingModeScene_UStart_m36433A02E644AB9C129E21F2C569B7086681C4F3 (void);
// 0x000001C9 System.Void UIProgrammingModeScene::_TestButtonPressed()
extern void UIProgrammingModeScene__TestButtonPressed_m8D161CA2C4297C97225E8DDDE8A1863326D4534D (void);
// 0x000001CA System.Void UIProgrammingModeScene::_PowerButtonPressed()
extern void UIProgrammingModeScene__PowerButtonPressed_m508917C62E53D9B23F25B33836CEF4D585236820 (void);
// 0x000001CB System.Void UIProgrammingModeScene::_SetRestButtonPlaying(System.Boolean)
extern void UIProgrammingModeScene__SetRestButtonPlaying_mA02FAB777945A63502B59549A95819E8DEB4F22C (void);
// 0x000001CC System.Void UIProgrammingModeScene::StopPlaying()
extern void UIProgrammingModeScene_StopPlaying_m206C1620641F0D8920072EB9C87359E95124BA48 (void);
// 0x000001CD System.Void UIProgrammingModeScene::OnRoutineLoaded(System.Boolean,SavedRoutine,RobotAction/MotionAction[],RobotAction/ArmsAction[],RobotAction/EmotionAction[],RobotAction/SoundAction[])
extern void UIProgrammingModeScene_OnRoutineLoaded_m650AC391C2B8ED9F8615CCC743D0785E52060CEA (void);
// 0x000001CE System.Void UIProgrammingModeScene::OnRoutineSaved(System.Boolean,SavedRoutine)
extern void UIProgrammingModeScene_OnRoutineSaved_mB165CAF51B02059A2E59D7F5D37FC244A803E8BD (void);
// 0x000001CF System.Void UIProgrammingModeScene::OnNewStepPlayed(System.Int32,RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction)
extern void UIProgrammingModeScene_OnNewStepPlayed_mFA9648FB2EA701EFE6A08F3F7CAA2E12C7135028 (void);
// 0x000001D0 System.Void UIProgrammingModeScene::UITopButtonAction(SceneActionType)
extern void UIProgrammingModeScene_UITopButtonAction_m59BAD238119C4BCA1176A4E9C1AF7748304FA326 (void);
// 0x000001D1 System.Void UIProgrammingModeScene::PlaySound(RobotAction/SoundAction)
extern void UIProgrammingModeScene_PlaySound_m1E58793FA0BE0DE14B0B99535CDBA9F04F3E442F (void);
// 0x000001D2 System.Void UIProgrammingModeScene::StopSound()
extern void UIProgrammingModeScene_StopSound_mD38F1D6E8641037A90A02A9FAC719CE7C33854DF (void);
// 0x000001D3 System.Void UIProgrammingModeScene::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
extern void UIProgrammingModeScene_OnBluetoothDeviceListUpdated_m317AE40D13883D060EF18656A94D3ECA05BDB230 (void);
// 0x000001D4 System.Void UIProgrammingModeScene::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void UIProgrammingModeScene_OnBluetoothConnected_mB214535479BA69A43F5DC296AC93D6111835B06B (void);
// 0x000001D5 System.Void UIProgrammingModeScene::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void UIProgrammingModeScene_OnBluetoothDisconnected_mFD97406461F12A2F707ECF83DCAF2CE06E50DE62 (void);
// 0x000001D6 System.Void UIProgrammingModeScene::.ctor()
extern void UIProgrammingModeScene__ctor_m692F908D72CD96A48AE3557F36FF73E12C844C54 (void);
// 0x000001D7 System.Void UIProgrammingModeScene::<UStart>b__41_0()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_0_m600136CED779E8E594251A09E00EDF7F7CF753C6 (void);
// 0x000001D8 System.Void UIProgrammingModeScene::<UStart>b__41_1()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_1_mDDFEBE2D6F81A828B17C08B7B1E691B9DF5A2CF5 (void);
// 0x000001D9 System.Void UIProgrammingModeScene::<UStart>b__41_2()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_2_mE4089A085E5A30833821F9970617A9F7B2587353 (void);
// 0x000001DA System.Void UIProgrammingModeScene::<UStart>b__41_3()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_3_m411E4C27F70072D80B65A9D7E32C796C47DE2C84 (void);
// 0x000001DB System.Void UIProgrammingModeScene::<UStart>b__41_4()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_4_m7F0E39407B1607F70CEB06406BDA370E50EAC8BE (void);
// 0x000001DC System.Void UIProgrammingModeScene::<UStart>b__41_5()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_5_mD5369B2F6E299DA277F9D57FE18853466EBD4DAC (void);
// 0x000001DD System.Void UIProgrammingModeScene::<UStart>b__41_6()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_6_m9680FF86966C77E7C17DF5C017DF43F3060C2845 (void);
// 0x000001DE System.Void UIProgrammingModeScene::<UStart>b__41_7(System.String)
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_7_m78DC5083D24AD32C526E9C170F834C1348679709 (void);
// 0x000001DF System.Void UIProgrammingModeScene::<UStart>b__41_8()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_8_m0AF6CB82BA4C2BDD50883657C467D9616F1C8BE8 (void);
// 0x000001E0 System.Void UIProgrammingModeScene::<UStart>b__41_9()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_9_mB8C2703026ED2313785658A4397928B3391C177C (void);
// 0x000001E1 System.Void UIProgrammingModeScene::<UStart>b__41_13(System.Int32)
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_13_mF315FE20249D26287121660A87A92AAFB605D450 (void);
// 0x000001E2 System.Void UIProgrammingModeScene::<UStart>b__41_10()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_10_m83C5ADF7C04BDC979E7FE0387C60F33C84D4BAD4 (void);
// 0x000001E3 System.Void UIProgrammingModeScene::<UStart>b__41_11()
extern void UIProgrammingModeScene_U3CUStartU3Eb__41_11_m571DBFB860E41846E456F5CB6480ED6F73FB0815 (void);
// 0x000001E4 System.Void UIProgrammingModeScene::<OnRoutineLoaded>b__46_0()
extern void UIProgrammingModeScene_U3COnRoutineLoadedU3Eb__46_0_m95B66EA934FDEB64562A0A9D4DA8E28F8D303BE0 (void);
// 0x000001E5 System.Void UIProgrammingModeScene::<OnRoutineLoaded>b__46_1()
extern void UIProgrammingModeScene_U3COnRoutineLoadedU3Eb__46_1_m696DD93517C87421153CCFDCDBE10020C06E3473 (void);
// 0x000001E6 System.Void UIProgrammingModeScene::<OnRoutineSaved>b__47_0()
extern void UIProgrammingModeScene_U3COnRoutineSavedU3Eb__47_0_m1E842B54248C213875E28C34797B345CA62E24DF (void);
// 0x000001E7 System.Void UIProgrammingModeScene::<OnRoutineSaved>b__47_1()
extern void UIProgrammingModeScene_U3COnRoutineSavedU3Eb__47_1_m811EC9FE3D7B0D3B7F6595625B6396F770C0D519 (void);
// 0x000001E8 System.Void UIProgrammingModeScene/<>c::.cctor()
extern void U3CU3Ec__cctor_m710C86201C67A33B24C7BEAB162FEA8B9627553C (void);
// 0x000001E9 System.Void UIProgrammingModeScene/<>c::.ctor()
extern void U3CU3Ec__ctor_mCDEF9D3AFFF4F179E7F8244F44107AC69E360C80 (void);
// 0x000001EA System.String UIProgrammingModeScene/<>c::<UStart>b__41_12(SavedRoutine)
extern void U3CU3Ec_U3CUStartU3Eb__41_12_m97674252F813DC99AD94015FEAFD47741C6065C4 (void);
// 0x000001EB System.Void UIProgrammingModeSimulationScene::UStart()
extern void UIProgrammingModeSimulationScene_UStart_m706537D6AEFBEFE8B639049CC58791EA174AC1F3 (void);
// 0x000001EC System.Void UIProgrammingModeSimulationScene::_PowerButtonPressed()
extern void UIProgrammingModeSimulationScene__PowerButtonPressed_mCCCCADCC71C7285DE1042D2534C2D77552A3EBF9 (void);
// 0x000001ED System.Void UIProgrammingModeSimulationScene::StopPlaying()
extern void UIProgrammingModeSimulationScene_StopPlaying_m6BE60499869931004A95F09C893D806E7F61F26F (void);
// 0x000001EE System.Void UIProgrammingModeSimulationScene::_SetRestButtonPlaying(System.Boolean)
extern void UIProgrammingModeSimulationScene__SetRestButtonPlaying_m72662523AF23FEA53A4B4559A23CC15BBAD7825C (void);
// 0x000001EF System.Void UIProgrammingModeSimulationScene::OnNewStepPlayed(System.Int32,RobotAction/MotionAction,RobotAction/ArmsAction,RobotAction/EmotionAction,RobotAction/SoundAction)
extern void UIProgrammingModeSimulationScene_OnNewStepPlayed_mA99E5C79AB8A332EF55B0B60744DCC8B94D69318 (void);
// 0x000001F0 System.Void UIProgrammingModeSimulationScene::UITopButtonAction(SceneActionType)
extern void UIProgrammingModeSimulationScene_UITopButtonAction_mA5467E250B948D6E9F9F8867646F2726D88FC5EC (void);
// 0x000001F1 System.Void UIProgrammingModeSimulationScene::PlaySound(RobotAction/SoundAction)
extern void UIProgrammingModeSimulationScene_PlaySound_m638363D19C5B3208D57990C5EDEB747D85ED4DA8 (void);
// 0x000001F2 System.Void UIProgrammingModeSimulationScene::StopSound()
extern void UIProgrammingModeSimulationScene_StopSound_m806EB915C75A9FC64B07B863213CDE41EAAA3FD5 (void);
// 0x000001F3 System.Void UIProgrammingModeSimulationScene::.ctor()
extern void UIProgrammingModeSimulationScene__ctor_m338F35FE67A4A839FEA09670EB71E192E5DE5D62 (void);
// 0x000001F4 System.Void UIProgrammingModeSimulationScene::<UStart>b__11_0()
extern void UIProgrammingModeSimulationScene_U3CUStartU3Eb__11_0_mDA19CE37FD735D0831F4F812DDA0B09732DDB19E (void);
// 0x000001F5 System.Void UIRealTimeModePhotoScene::UAwake()
extern void UIRealTimeModePhotoScene_UAwake_m86B1B6147C759D13443C73B3D7BA307902DDEA50 (void);
// 0x000001F6 System.Void UIRealTimeModePhotoScene::UStart()
extern void UIRealTimeModePhotoScene_UStart_mF1DB008391597EB207369CA1DEC1AECB10B10B4E (void);
// 0x000001F7 System.Void UIRealTimeModePhotoScene::AddActionToButton(UnityEngine.UI.Button,UnityEngine.EventSystems.EventTriggerType,System.Action)
extern void UIRealTimeModePhotoScene_AddActionToButton_m6E0F1B15EDD7597A611D6B548BA63C4EA41BED4B (void);
// 0x000001F8 System.Void UIRealTimeModePhotoScene::_PhotoButtonPressed()
extern void UIRealTimeModePhotoScene__PhotoButtonPressed_mAF78AC70D7B731109D2EB7FCBCC763976D5B72BC (void);
// 0x000001F9 System.Void UIRealTimeModePhotoScene::_VideoButtonPressed()
extern void UIRealTimeModePhotoScene__VideoButtonPressed_mA48B79A8327DF118CC5F612172B94A005A650963 (void);
// 0x000001FA System.Void UIRealTimeModePhotoScene::UITopButtonAction(SceneActionType)
extern void UIRealTimeModePhotoScene_UITopButtonAction_m9AEC3AC971F95CB6EFD528D71F9D6EFA3C309528 (void);
// 0x000001FB System.Void UIRealTimeModePhotoScene::PlaySound(RobotAction/SoundAction)
extern void UIRealTimeModePhotoScene_PlaySound_mA3B3579C1E44516406F412F5C3AD1699993A8CEE (void);
// 0x000001FC System.Collections.IEnumerator UIRealTimeModePhotoScene::SoundCoroutine()
extern void UIRealTimeModePhotoScene_SoundCoroutine_m37627DEACB6EBA3EB7E19A471775B67A49ABDD28 (void);
// 0x000001FD System.Void UIRealTimeModePhotoScene::StopSound()
extern void UIRealTimeModePhotoScene_StopSound_m77FF05F61C1CB29DF37AA2BE37BF9BC1F2896B5D (void);
// 0x000001FE System.Void UIRealTimeModePhotoScene::.ctor()
extern void UIRealTimeModePhotoScene__ctor_m342C609EB2EEE7D301E69370D303940FCF937C8B (void);
// 0x000001FF System.Void UIRealTimeModePhotoScene::<UStart>b__22_0()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_0_m2B8E329C37BEC924D8C9C902875D54401AE51B00 (void);
// 0x00000200 System.Void UIRealTimeModePhotoScene::<UStart>b__22_1()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_1_mD5812571B327F29A68A74A676FBFF8DDEEB46B60 (void);
// 0x00000201 System.Void UIRealTimeModePhotoScene::<UStart>b__22_2()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_2_m097B5542446DA48904F8C2E497ED68B948AD16F3 (void);
// 0x00000202 System.Void UIRealTimeModePhotoScene::<UStart>b__22_3()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_3_m9289E416544E889DC57E00B4F9A6F503C7DDEE56 (void);
// 0x00000203 System.Void UIRealTimeModePhotoScene::<UStart>b__22_4()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_4_m4A5C477555043CA6C7A1F6B92A6102500CD9A38B (void);
// 0x00000204 System.Void UIRealTimeModePhotoScene::<UStart>b__22_5()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_5_m8C170E92E62B68D6AC8F373CDB452DDF769EA848 (void);
// 0x00000205 System.Void UIRealTimeModePhotoScene::<UStart>b__22_6()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_6_mC51FA53E29B33DCE246F146CAD2ED600CF0F18AC (void);
// 0x00000206 System.Void UIRealTimeModePhotoScene::<UStart>b__22_7()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_7_mF101B1ACBA4EB8AA743C5AF982D42C57CA326FB4 (void);
// 0x00000207 System.Void UIRealTimeModePhotoScene::<UStart>b__22_8()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_8_m8D7BB5D5F60EC8658A3458C2E3F750FC2A4C2CB8 (void);
// 0x00000208 System.Void UIRealTimeModePhotoScene::<UStart>b__22_9()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_9_mB60D9F6944E13800595AC1C0DC5EA58B2E1C7888 (void);
// 0x00000209 System.Void UIRealTimeModePhotoScene::<UStart>b__22_10()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_10_m9DF2BD115902B1706A5FB4BDBD15C8EA8E5376A8 (void);
// 0x0000020A System.Void UIRealTimeModePhotoScene::<UStart>b__22_11()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_11_m1FF8441869B29A35B660DAF9901762CAEEFC0BE8 (void);
// 0x0000020B System.Void UIRealTimeModePhotoScene::<UStart>b__22_12()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_12_m74F0FAD79C33B5B4FC92A3581373FAB26EEFE86F (void);
// 0x0000020C System.Void UIRealTimeModePhotoScene::<UStart>b__22_13()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_13_m2D11193C5D103A4E3B6F8B8DB6CBEF931F46198F (void);
// 0x0000020D System.Void UIRealTimeModePhotoScene::<UStart>b__22_14()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_14_m2B306A8B72A5D8FA67D6EC2D66416ED404719C76 (void);
// 0x0000020E System.Void UIRealTimeModePhotoScene::<UStart>b__22_15()
extern void UIRealTimeModePhotoScene_U3CUStartU3Eb__22_15_mEC2DEC2AD742AC6B08710D5A95B31848F8AE9026 (void);
// 0x0000020F System.Void UIRealTimeModePhotoScene/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m8556457FE80981E9224B670DA2D508AE72D8CCC3 (void);
// 0x00000210 System.Void UIRealTimeModePhotoScene/<>c__DisplayClass22_0::<UStart>b__16()
extern void U3CU3Ec__DisplayClass22_0_U3CUStartU3Eb__16_m357E715B17117AB043045E720936675BF742F096 (void);
// 0x00000211 System.Void UIRealTimeModePhotoScene/<>c__DisplayClass22_1::.ctor()
extern void U3CU3Ec__DisplayClass22_1__ctor_m9FCA61FE32BE64B21A3C23C94F2ACDDE86596C13 (void);
// 0x00000212 System.Void UIRealTimeModePhotoScene/<>c__DisplayClass22_1::<UStart>b__17()
extern void U3CU3Ec__DisplayClass22_1_U3CUStartU3Eb__17_m1236D45D2564DA2DA2CB94616C435F3E80E5454D (void);
// 0x00000213 System.Void UIRealTimeModePhotoScene/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m11F131070385918ACE097BBB998E7D8EA1A6B10D (void);
// 0x00000214 System.Void UIRealTimeModePhotoScene/<>c__DisplayClass23_0::<AddActionToButton>b__0(UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec__DisplayClass23_0_U3CAddActionToButtonU3Eb__0_mF0D73F7E2C1BEDDF620E959C5C9989BA1B6A4616 (void);
// 0x00000215 System.Void UIRealTimeModePhotoScene/<SoundCoroutine>d__33::.ctor(System.Int32)
extern void U3CSoundCoroutineU3Ed__33__ctor_mA29129E7915E0ED663D17579F701BD363B32E7A8 (void);
// 0x00000216 System.Void UIRealTimeModePhotoScene/<SoundCoroutine>d__33::System.IDisposable.Dispose()
extern void U3CSoundCoroutineU3Ed__33_System_IDisposable_Dispose_m9EDE82CD37F4E9ACF310B054DDABE3444FE5334E (void);
// 0x00000217 System.Boolean UIRealTimeModePhotoScene/<SoundCoroutine>d__33::MoveNext()
extern void U3CSoundCoroutineU3Ed__33_MoveNext_m86BD48AF3FBE793A633A6375099BC73FD26B70E5 (void);
// 0x00000218 System.Object UIRealTimeModePhotoScene/<SoundCoroutine>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSoundCoroutineU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m580B3FF31CEF007BB00E3C9E93C0D07B7183DBE4 (void);
// 0x00000219 System.Void UIRealTimeModePhotoScene/<SoundCoroutine>d__33::System.Collections.IEnumerator.Reset()
extern void U3CSoundCoroutineU3Ed__33_System_Collections_IEnumerator_Reset_m0ED918FA48C48DF1140BE4AB461345D7E888EF50 (void);
// 0x0000021A System.Object UIRealTimeModePhotoScene/<SoundCoroutine>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CSoundCoroutineU3Ed__33_System_Collections_IEnumerator_get_Current_m6091FBF00CF9C8FA3F1E981D82852128A54F97CF (void);
// 0x0000021B System.Void UIRealTimeModeScene::UStart()
extern void UIRealTimeModeScene_UStart_mB45AD01A7A40C4C58B3591C5E902C632D1F565F4 (void);
// 0x0000021C System.Void UIRealTimeModeScene::AddActionToButton(UnityEngine.UI.Button,UnityEngine.EventSystems.EventTriggerType,System.Action)
extern void UIRealTimeModeScene_AddActionToButton_mDAF9E3DBEEA9EA14C0021433B4ECE423617D14FC (void);
// 0x0000021D System.Void UIRealTimeModeScene::UITopButtonAction(SceneActionType)
extern void UIRealTimeModeScene_UITopButtonAction_m14C1D2AC07DD9E90C4629BE35C6DEA1E88ACC83A (void);
// 0x0000021E System.Void UIRealTimeModeScene::PlaySound(System.Int32)
extern void UIRealTimeModeScene_PlaySound_m61DB7BC6E12B32BCE1B3F7C630EDE3EF9A930685 (void);
// 0x0000021F System.Collections.IEnumerator UIRealTimeModeScene::SoundCoroutine()
extern void UIRealTimeModeScene_SoundCoroutine_m2AD9F74A79AE3BC337C9D0EDBC6A59B7E94C3EE9 (void);
// 0x00000220 System.Void UIRealTimeModeScene::StopSound()
extern void UIRealTimeModeScene_StopSound_mA64ADA64454A8AD26BF5D4861A2E1FD019C1F90C (void);
// 0x00000221 System.Void UIRealTimeModeScene::.ctor()
extern void UIRealTimeModeScene__ctor_mA4AC992A5D576677F057634D74FD445ED122FB62 (void);
// 0x00000222 System.Void UIRealTimeModeScene::<UStart>b__18_0()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_0_mA0C46BBE42151146CC6BAC44ABADD495B6C7DCFD (void);
// 0x00000223 System.Void UIRealTimeModeScene::<UStart>b__18_1()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_1_mBF69B2A0CAD81FB1B590193FB5C8B597E5458086 (void);
// 0x00000224 System.Void UIRealTimeModeScene::<UStart>b__18_2()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_2_m88EC9C1999E2B34D95BC424AA81C9F9EA3F9BE24 (void);
// 0x00000225 System.Void UIRealTimeModeScene::<UStart>b__18_3()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_3_m4D68BB6BE63EC3F64FF924702AAF7437D8717556 (void);
// 0x00000226 System.Void UIRealTimeModeScene::<UStart>b__18_4()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_4_m9D62DBECBFC71F98240C87FE78839AE158689C0E (void);
// 0x00000227 System.Void UIRealTimeModeScene::<UStart>b__18_5()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_5_mFFF148B84B7346BF1074821DC8929709DCFDA284 (void);
// 0x00000228 System.Void UIRealTimeModeScene::<UStart>b__18_6()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_6_mDA2E26B30E3EA4D0F1C41CBA86F5557B50215BD7 (void);
// 0x00000229 System.Void UIRealTimeModeScene::<UStart>b__18_7()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_7_mB58A68E48FC7A71F9B9CE304F63DC7D0857C4D73 (void);
// 0x0000022A System.Void UIRealTimeModeScene::<UStart>b__18_8()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_8_mDB5626CC47695EABF8C03C0D72D0E738EB89F5B2 (void);
// 0x0000022B System.Void UIRealTimeModeScene::<UStart>b__18_9()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_9_m8BA942BA4A2EE6ACCF975B7EA17EA96A7784FF4C (void);
// 0x0000022C System.Void UIRealTimeModeScene::<UStart>b__18_10()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_10_m5EE476A0864809739E0112572D32088E793522B6 (void);
// 0x0000022D System.Void UIRealTimeModeScene::<UStart>b__18_11()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_11_m57A571E7FCE5CEEA04F0AB29042AA14A9974E7DE (void);
// 0x0000022E System.Void UIRealTimeModeScene::<UStart>b__18_12()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_12_mEF69AACD3E1F768102A50E3C78ADE53EF8B677BF (void);
// 0x0000022F System.Void UIRealTimeModeScene::<UStart>b__18_13()
extern void UIRealTimeModeScene_U3CUStartU3Eb__18_13_m1E52460F4F7F6131DD752020D8DF0F47F7D24DC7 (void);
// 0x00000230 System.Void UIRealTimeModeScene/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m0D697401A49F8ABA0BE5AAC0F6895C27D5E96FE5 (void);
// 0x00000231 System.Void UIRealTimeModeScene/<>c__DisplayClass18_0::<UStart>b__14()
extern void U3CU3Ec__DisplayClass18_0_U3CUStartU3Eb__14_m65AA5028EE53B0A4A5EB332CEE04A630751280D3 (void);
// 0x00000232 System.Void UIRealTimeModeScene/<>c__DisplayClass18_1::.ctor()
extern void U3CU3Ec__DisplayClass18_1__ctor_mAEA09BD62A20C4B5A42A2FE857AF7B70A94735AA (void);
// 0x00000233 System.Void UIRealTimeModeScene/<>c__DisplayClass18_1::<UStart>b__15()
extern void U3CU3Ec__DisplayClass18_1_U3CUStartU3Eb__15_mCC2A097CE6FCA5FFBB90927CA15C5D8783839538 (void);
// 0x00000234 System.Void UIRealTimeModeScene/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m1DB0FE2629FFAB15F36921F10C41E907A6591F56 (void);
// 0x00000235 System.Void UIRealTimeModeScene/<>c__DisplayClass19_0::<AddActionToButton>b__0(UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec__DisplayClass19_0_U3CAddActionToButtonU3Eb__0_mFD0286BF88AABFCC4DE7423A6D39172B117E9CFE (void);
// 0x00000236 System.Void UIRealTimeModeScene/<SoundCoroutine>d__26::.ctor(System.Int32)
extern void U3CSoundCoroutineU3Ed__26__ctor_mBEA94C617C4AD6FC6D0DAB14688871E77F7EAE16 (void);
// 0x00000237 System.Void UIRealTimeModeScene/<SoundCoroutine>d__26::System.IDisposable.Dispose()
extern void U3CSoundCoroutineU3Ed__26_System_IDisposable_Dispose_mE08FEC327BFBE8EADFD7F3A7E9622DFF17FD2D90 (void);
// 0x00000238 System.Boolean UIRealTimeModeScene/<SoundCoroutine>d__26::MoveNext()
extern void U3CSoundCoroutineU3Ed__26_MoveNext_m29863AD2C100C40FED5A3B8D442ACFACCEE76F4D (void);
// 0x00000239 System.Object UIRealTimeModeScene/<SoundCoroutine>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSoundCoroutineU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BDD797107163AF497EE1C5368E2A60B94F93D26 (void);
// 0x0000023A System.Void UIRealTimeModeScene/<SoundCoroutine>d__26::System.Collections.IEnumerator.Reset()
extern void U3CSoundCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m72FFAC3D0ADEFB06DDA3BE5C06A82BCAB10AFE47 (void);
// 0x0000023B System.Object UIRealTimeModeScene/<SoundCoroutine>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CSoundCoroutineU3Ed__26_System_Collections_IEnumerator_get_Current_m6DF4F75E2D852B563239D4A906BCED4EF2E0F956 (void);
// 0x0000023C System.Void UISettingsScene::UStart()
extern void UISettingsScene_UStart_mF5B7DAD9A464A1CC173796208C4B68F8082EF0F4 (void);
// 0x0000023D System.Void UISettingsScene::_OnSynchornizeClicked()
extern void UISettingsScene__OnSynchornizeClicked_mE5FD26F996FE5AEDEEBCCEBED418F9BC78313FC2 (void);
// 0x0000023E System.Void UISettingsScene::_OnAboutAppClicked()
extern void UISettingsScene__OnAboutAppClicked_mD34541E1DE124DC1EC31A1BBDB451BC5C0C8D4D5 (void);
// 0x0000023F System.Void UISettingsScene::UITopButtonAction(SceneActionType)
extern void UISettingsScene_UITopButtonAction_m62DD0BAB473F7D5FA1734296A76C8650CB988CA3 (void);
// 0x00000240 System.Void UISettingsScene::.ctor()
extern void UISettingsScene__ctor_m34CACF920879CB586B52AA4E3309AE82E8D773F8 (void);
// 0x00000241 System.Void UISyncSetupScene::UAwake()
extern void UISyncSetupScene_UAwake_m040ED1B3866A4A06CBB15729D1D9CD622E7079E0 (void);
// 0x00000242 System.Void UISyncSetupScene::UStart()
extern void UISyncSetupScene_UStart_m8171A64F544ECE10AD813B5BD89E2B4CC1CD6DB2 (void);
// 0x00000243 System.Void UISyncSetupScene::_ContinueOfflineButton()
extern void UISyncSetupScene__ContinueOfflineButton_m990A9C9965A038F7A1A98E9B7459432FE9D162FA (void);
// 0x00000244 System.Void UISyncSetupScene::_SynchronizeButton()
extern void UISyncSetupScene__SynchronizeButton_m04DB7CF464DF69782DC14721484104D446521D56 (void);
// 0x00000245 System.Void UISyncSetupScene::_DisconnectButton()
extern void UISyncSetupScene__DisconnectButton_mB86B246ADCE68D2D011C66795ACC57B2EFC6B92D (void);
// 0x00000246 System.Void UISyncSetupScene::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
extern void UISyncSetupScene_OnBluetoothDeviceListUpdated_mD40E119DEA5543495827EE340C62A72C45AE0069 (void);
// 0x00000247 System.Void UISyncSetupScene::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void UISyncSetupScene_OnBluetoothConnected_m54B5F9016A6CB939B48953FE71C69C5E879471C8 (void);
// 0x00000248 System.Void UISyncSetupScene::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void UISyncSetupScene_OnBluetoothDisconnected_m29A937CF28967D254DA4EE4C4767F72ADEF2F9BA (void);
// 0x00000249 System.Void UISyncSetupScene::UITopButtonAction(SceneActionType)
extern void UISyncSetupScene_UITopButtonAction_m0BE6AC4F8F0E90EDE38EE06CF8A0A245825A166D (void);
// 0x0000024A System.Void UISyncSetupScene::.ctor()
extern void UISyncSetupScene__ctor_m19C5F143CC13F1D90E0E59CCDAF5F07A526C5038 (void);
// 0x0000024B System.Void UISyncSetupScene::<OnBluetoothConnected>b__20_0()
extern void UISyncSetupScene_U3COnBluetoothConnectedU3Eb__20_0_m217DF1852C52BAB37AFB32CA2A66F9252F8AF226 (void);
// 0x0000024C System.Void UISyncSetupScene::<OnBluetoothConnected>b__20_1()
extern void UISyncSetupScene_U3COnBluetoothConnectedU3Eb__20_1_mB79AE6756303EB3A1CE4FDFD6377D385FEB18BC6 (void);
// 0x0000024D System.Void UISyncSetupScene/<>c::.cctor()
extern void U3CU3Ec__cctor_m8E95854478F00AF4E51DBBC91AECF62850CAF0AB (void);
// 0x0000024E System.Void UISyncSetupScene/<>c::.ctor()
extern void U3CU3Ec__ctor_mF6548E4432B1AD3D8712824343031B0C3E7C6704 (void);
// 0x0000024F System.Void UISyncSetupScene/<>c::<UAwake>b__14_0()
extern void U3CU3Ec_U3CUAwakeU3Eb__14_0_m2AAADCA42CE0CC67314DBDF04C47D2A36722919E (void);
// 0x00000250 System.Void UISyncSetupScene/<>c::<UAwake>b__14_1()
extern void U3CU3Ec_U3CUAwakeU3Eb__14_1_mEADE934E140CBC52F8EF41143F79466DB0BF9A35 (void);
// 0x00000251 System.Void UIBluetoothButton::UStart()
extern void UIBluetoothButton_UStart_m9F258870EC1D1CB264435FF629839EF82039B946 (void);
// 0x00000252 System.Void UIBluetoothButton::UUpdate()
extern void UIBluetoothButton_UUpdate_m9BD196624FD301CF2B4025003837FFC569ADA7FF (void);
// 0x00000253 System.Void UIBluetoothButton::OpenSyncScene()
extern void UIBluetoothButton_OpenSyncScene_mA92B945A4BEDBE9AB82764D7BD80A17C77266F2C (void);
// 0x00000254 System.Void UIBluetoothButton::InitWithData(UISceneData)
extern void UIBluetoothButton_InitWithData_m51CB469545FCCFD25382458877B78E79FD9EFBF2 (void);
// 0x00000255 System.Void UIBluetoothButton::.ctor()
extern void UIBluetoothButton__ctor_mD528AC622117F537E86D310C5944378ECF181517 (void);
// 0x00000256 System.Void UICustomButton::Awake()
extern void UICustomButton_Awake_mF5ED792172D0312415C75FD1A2F31615E1BE3749 (void);
// 0x00000257 System.Void UICustomButton::ExecuteAction()
extern void UICustomButton_ExecuteAction_mC102F537754B90F88EBA5F9402CDB9DD3E9B0D35 (void);
// 0x00000258 System.Void UICustomButton::.ctor()
extern void UICustomButton__ctor_mFD2FC56C30D7A37D1BF2E74387BDD2CA950C6973 (void);
// 0x00000259 System.Void UIMenuNavigationButton::ExecuteAction()
extern void UIMenuNavigationButton_ExecuteAction_m4FC18D4C7B4D32C5CD3ED66EC8E9C2037C351B86 (void);
// 0x0000025A System.Void UIMenuNavigationButton::.ctor()
extern void UIMenuNavigationButton__ctor_mEF815BEEA7FB40ABDFD479F707F49466CDB28898 (void);
// 0x0000025B System.Void UIProgrammingPanel::Awake()
extern void UIProgrammingPanel_Awake_m488BFCD2B7A2588C0552BA35F9E153F2BB7A3BBD (void);
// 0x0000025C System.Void UIProgrammingPanel::Start()
extern void UIProgrammingPanel_Start_m40D9AA76193686C1A598BA4CBF0580F4701FD77B (void);
// 0x0000025D System.Void UIProgrammingPanel::ShowPanel(System.Int32)
extern void UIProgrammingPanel_ShowPanel_mD31BB1BE319A835E5FDFEEB917C97F784A470CF2 (void);
// 0x0000025E System.Void UIProgrammingPanel::_OnButtonPressed(UIProgrammingPanelButton)
extern void UIProgrammingPanel__OnButtonPressed_mBD4C08CDE9AB4D68BFEAB1D2F854D912B3F34BB4 (void);
// 0x0000025F UnityEngine.GameObject UIProgrammingPanel::GetButtonPrefab(RobotAction/MotionAction)
extern void UIProgrammingPanel_GetButtonPrefab_m8D587B51758BAD48EC82CC4DEEEB3E8E9A4A0C81 (void);
// 0x00000260 UnityEngine.GameObject UIProgrammingPanel::GetButtonPrefab(RobotAction/EmotionAction)
extern void UIProgrammingPanel_GetButtonPrefab_mCA24AF61820DDAA232F74C0603086BCA77CCCB60 (void);
// 0x00000261 UnityEngine.GameObject UIProgrammingPanel::GetButtonPrefab(RobotAction/ArmsAction)
extern void UIProgrammingPanel_GetButtonPrefab_mB79D3D8F98A530ED788ABC813024FCCAAB44A695 (void);
// 0x00000262 UnityEngine.GameObject UIProgrammingPanel::GetButtonPrefab(RobotAction/SoundAction)
extern void UIProgrammingPanel_GetButtonPrefab_m41BAFD6BAE20EB6EA455B504EA102FADD267C20F (void);
// 0x00000263 System.Void UIProgrammingPanel::.ctor()
extern void UIProgrammingPanel__ctor_m0148261C5A1B5B47B30F930C01C6D79BE9E864AE (void);
// 0x00000264 System.Void UIProgrammingPanel/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m8A12614BA8A880DF83E9508CB5C50300ECBBF7FF (void);
// 0x00000265 System.Void UIProgrammingPanel/<>c__DisplayClass6_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CStartU3Eb__0_m67501FFFD47D778EFFC6ABDACBBC9F5FE3B73735 (void);
// 0x00000266 System.Void UIProgrammingPanelButton::Awake()
extern void UIProgrammingPanelButton_Awake_mDF896DC3185821E99DADBBB18CAACA32AD697806 (void);
// 0x00000267 System.Void UIProgrammingPanelButton::.ctor()
extern void UIProgrammingPanelButton__ctor_m9E75DE0112FF7141773E6A44A4318432370B01DA (void);
// 0x00000268 System.Void UIRobotAnimation::UStart()
extern void UIRobotAnimation_UStart_m7C17A9E3DFF1B917D027FF77C966AB155F9CE7F7 (void);
// 0x00000269 System.Void UIRobotAnimation::StopAnimation()
extern void UIRobotAnimation_StopAnimation_mCE616DC8938E4B2D84A34309AF780B4867C4A49A (void);
// 0x0000026A System.Void UIRobotAnimation::SetAction(RobotAction/MotionAction)
extern void UIRobotAnimation_SetAction_m2E24A0BD7F7673379D62A4E267A13868DCED4878 (void);
// 0x0000026B System.Collections.IEnumerator UIRobotAnimation::StartMoving(System.Boolean)
extern void UIRobotAnimation_StartMoving_m95D0ADE252ECDC9CBB144DF31189D78609181E33 (void);
// 0x0000026C System.Collections.IEnumerator UIRobotAnimation::StartRotating(System.Boolean)
extern void UIRobotAnimation_StartRotating_m9A149A8FB55868C29B0C9FE51209A4C83114E5AF (void);
// 0x0000026D System.Void UIRobotAnimation::SetAction(RobotAction/EmotionAction)
extern void UIRobotAnimation_SetAction_mD7AA4D627C3EFD783359026F79FB206B54A0879C (void);
// 0x0000026E System.Void UIRobotAnimation::SetAction(RobotAction/ArmsAction)
extern void UIRobotAnimation_SetAction_mABCE45C6BFE1FFEA892358AC2B5BABBE1ABFFBD8 (void);
// 0x0000026F System.Void UIRobotAnimation::.ctor()
extern void UIRobotAnimation__ctor_mC811CA51AD713E408F3C0CCB2A559398C1B0DD7E (void);
// 0x00000270 System.Void UIRobotAnimation/<StartMoving>d__20::.ctor(System.Int32)
extern void U3CStartMovingU3Ed__20__ctor_mF8751983B2BF9B97EEF59E4525E78FEDA6164979 (void);
// 0x00000271 System.Void UIRobotAnimation/<StartMoving>d__20::System.IDisposable.Dispose()
extern void U3CStartMovingU3Ed__20_System_IDisposable_Dispose_m738174DD9920FDE512B08742B86B28073C0B09B1 (void);
// 0x00000272 System.Boolean UIRobotAnimation/<StartMoving>d__20::MoveNext()
extern void U3CStartMovingU3Ed__20_MoveNext_mC7E156EBD4466ECB69AD5FF478F4B0ADE875F92D (void);
// 0x00000273 System.Object UIRobotAnimation/<StartMoving>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartMovingU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77C738DFD77C76E6D8F7C2ABEEA51DBB4F6280E2 (void);
// 0x00000274 System.Void UIRobotAnimation/<StartMoving>d__20::System.Collections.IEnumerator.Reset()
extern void U3CStartMovingU3Ed__20_System_Collections_IEnumerator_Reset_m0DA38B4C519C9EE2DEB0B0B1DE847D3373A32F4C (void);
// 0x00000275 System.Object UIRobotAnimation/<StartMoving>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CStartMovingU3Ed__20_System_Collections_IEnumerator_get_Current_mA7AC08E32F6E70890F1623B31261BB8793255931 (void);
// 0x00000276 System.Void UIRobotAnimation/<StartRotating>d__21::.ctor(System.Int32)
extern void U3CStartRotatingU3Ed__21__ctor_m21F90FE00C8DB0168B075A9A19ACABFC4FD716D8 (void);
// 0x00000277 System.Void UIRobotAnimation/<StartRotating>d__21::System.IDisposable.Dispose()
extern void U3CStartRotatingU3Ed__21_System_IDisposable_Dispose_m811767697723ED47240D70273B0B0767FE7FA2E6 (void);
// 0x00000278 System.Boolean UIRobotAnimation/<StartRotating>d__21::MoveNext()
extern void U3CStartRotatingU3Ed__21_MoveNext_m5D4425EAF60D4830B61C54AB773C4511D9C9E1C8 (void);
// 0x00000279 System.Object UIRobotAnimation/<StartRotating>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartRotatingU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4DD8247F9B2C063D1AE21AFA5E9387DF731136C (void);
// 0x0000027A System.Void UIRobotAnimation/<StartRotating>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartRotatingU3Ed__21_System_Collections_IEnumerator_Reset_m3FBB4FE304908E60065853B9F782AF5AAD7A64C7 (void);
// 0x0000027B System.Object UIRobotAnimation/<StartRotating>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartRotatingU3Ed__21_System_Collections_IEnumerator_get_Current_m4C75AD00B16975C0075650479384070116E27061 (void);
// 0x0000027C System.Void UIRoutineSelector::Populate(System.String[])
extern void UIRoutineSelector_Populate_m6C29C1CEE867F84F6E674C8AD64977A0C672E21A (void);
// 0x0000027D System.Int32 UIRoutineSelector::GetChoice()
extern void UIRoutineSelector_GetChoice_m7D129357AD5D6EA4CDD3AE6924879DCE4B311E72 (void);
// 0x0000027E System.Void UIRoutineSelector::_OnButtonPressed(System.Int32)
extern void UIRoutineSelector__OnButtonPressed_mB6B862B808812C18C2FBF7142323BD89702699D7 (void);
// 0x0000027F System.Void UIRoutineSelector::.ctor()
extern void UIRoutineSelector__ctor_mAD587121E3C86BFAD77629E83D943471D97F2746 (void);
// 0x00000280 System.Void UIRoutineSelector/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m3B4C62A76B31762F989716AD911573343AEB19E0 (void);
// 0x00000281 System.Void UIRoutineSelector/<>c__DisplayClass5_0::<Populate>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass5_0_U3CPopulateU3Eb__0_m96ACD101F45F24C2BEAA5FF0A4182FD314B2FF7B (void);
// 0x00000282 System.Void SceneActionVisualData::.ctor()
extern void SceneActionVisualData__ctor_m699F55AADE320AF748113D748FDCC0FEAEE8878E (void);
// 0x00000283 System.Void UISceneActionBarInitialiser::InitWithData(UISceneData)
extern void UISceneActionBarInitialiser_InitWithData_mBBE379C8BA6DF984B854D15A4B4EFC4AEA66BE44 (void);
// 0x00000284 System.Void UISceneActionBarInitialiser::UnInit()
extern void UISceneActionBarInitialiser_UnInit_mA7B276AE408DE9503774FABC068D5140CD0353D2 (void);
// 0x00000285 SceneActionVisualData UISceneActionBarInitialiser::GetVisualDataByType(SceneActionType)
extern void UISceneActionBarInitialiser_GetVisualDataByType_m52908A46BF230C5D06F50B3B45BADF68BFF69A87 (void);
// 0x00000286 System.Void UISceneActionBarInitialiser::.ctor()
extern void UISceneActionBarInitialiser__ctor_mDC01032C4610647201E2490AB80F492CC715AE16 (void);
// 0x00000287 System.Void UISceneActionBarInitialiser/<>c::.cctor()
extern void U3CU3Ec__cctor_mB5411FAFE955809747DA427ED922A634B52ADCEA (void);
// 0x00000288 System.Void UISceneActionBarInitialiser/<>c::.ctor()
extern void U3CU3Ec__ctor_mC559AB49B0A771DAF3A442A5C51139C28E1CD1F3 (void);
// 0x00000289 System.Void UISceneActionBarInitialiser/<>c::<InitWithData>b__5_0(SceneActionType)
extern void U3CU3Ec_U3CInitWithDataU3Eb__5_0_m43E8E9C6C316FDDFECC6A4A65D85EA2B973241DC (void);
// 0x0000028A System.Void UISceneActionBarInitialiser/<>c::<InitWithData>b__5_1(SceneActionType)
extern void U3CU3Ec_U3CInitWithDataU3Eb__5_1_m5A5C9A29E8261B985ED3E14395AC273E844DEC96 (void);
// 0x0000028B System.Void UISceneActionBarInitialiser/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m2347059AFDE9DB21B222491DC8401E58443263FA (void);
// 0x0000028C System.Boolean UISceneActionBarInitialiser/<>c__DisplayClass7_0::<GetVisualDataByType>b__0(SceneActionVisualData)
extern void U3CU3Ec__DisplayClass7_0_U3CGetVisualDataByTypeU3Eb__0_mFEAF094133D0C011D240CFA50563CCD19FD45E2A (void);
// 0x0000028D System.Void UISceneActionButton::SetSceneActionType(SceneActionType)
extern void UISceneActionButton_SetSceneActionType_m2B5BB559466248AD92E29C75DCC00BE1ADBD8DF3 (void);
// 0x0000028E System.Void UISceneActionButton::InitialiseVisualData(SceneActionVisualData,System.Action`1<SceneActionType>)
extern void UISceneActionButton_InitialiseVisualData_m7BEEBD22FC02242EB21B90540C03D3E323F1BF85 (void);
// 0x0000028F System.Void UISceneActionButton::DeinitialiseVisualData()
extern void UISceneActionButton_DeinitialiseVisualData_mCFC1C6A62F63F1757FEBB701DB979C871FF5C30B (void);
// 0x00000290 System.Void UISceneActionButton::ExecuteAction()
extern void UISceneActionButton_ExecuteAction_mAF84889418DFF114850B87DDBC5CEEE7733377DB (void);
// 0x00000291 System.Void UISceneActionButton::.ctor()
extern void UISceneActionButton__ctor_mF9F4814D0A812B55EC484EB4A1AC660AECD63748 (void);
// 0x00000292 System.Void UISceneBase::.ctor()
extern void UISceneBase__ctor_mD4F168ED4C224CBD57CF3C0AB34EE7AF433124A6 (void);
// 0x00000293 System.Void UISceneInitialiser::Start()
extern void UISceneInitialiser_Start_m46792CFF88603632C69EB250E2E97FA9EAC484B1 (void);
// 0x00000294 System.Void UISceneInitialiser::InitWithData(UISceneData)
extern void UISceneInitialiser_InitWithData_m527DEFEE57D196CC4E9750AC8FAEADE10A731E10 (void);
// 0x00000295 System.Void UISceneInitialiser::UnInit()
extern void UISceneInitialiser_UnInit_m213E4C05BD7486367279509F546BACE72018458E (void);
// 0x00000296 System.Void UISceneInitialiser::.ctor()
extern void UISceneInitialiser__ctor_m69C162CD06917447448DD2CB8F2757FD20BFC724 (void);
// 0x00000297 System.Void OnSceneOpened::.ctor()
extern void OnSceneOpened__ctor_mA2A69CD371825FF3DDA5F1BE66BBA7394A205A9A (void);
// 0x00000298 System.Void OnSceneClosed::.ctor()
extern void OnSceneClosed__ctor_mC9A627C64D695B3E863A7624C1D7A6E7B37013FA (void);
// 0x00000299 System.Void UISceneManager::Awake()
extern void UISceneManager_Awake_mA21498548D3D73C6EFFDCA3EA08B95C01DD3D44B (void);
// 0x0000029A System.Void UISceneManager::Start()
extern void UISceneManager_Start_m4D6A3BCD743F0DC427CDF50E63FD109CA8049177 (void);
// 0x0000029B System.Void UISceneManager::OpenHomeScene()
extern void UISceneManager_OpenHomeScene_mDBB76E668F578E0A2449F4D1B19A76922A55BEA1 (void);
// 0x0000029C System.Void UISceneManager::OpenScene(UISceneBase,System.Boolean)
extern void UISceneManager_OpenScene_m86A7A5C2C4C4EE8D377120F444155FECC43ADB45 (void);
// 0x0000029D System.Void UISceneManager::SetSceneToFullScreen(UnityEngine.RectTransform)
extern void UISceneManager_SetSceneToFullScreen_mD4D1E886E05CCBA14647EEC1E2A4ED45D91B0372 (void);
// 0x0000029E System.Void UISceneManager::DisableCurrentScene()
extern void UISceneManager_DisableCurrentScene_m1915766D9A092C47B1B70CBA8EC7794DCFA5AB38 (void);
// 0x0000029F System.Void UISceneManager::CloseCurrentScene()
extern void UISceneManager_CloseCurrentScene_m5B1EB54A44DFD44C25AB32E33B824EE515E9E460 (void);
// 0x000002A0 System.Void UISceneManager::RestorePreviousScene()
extern void UISceneManager_RestorePreviousScene_m443099568928A8E51374932137D429E029B84992 (void);
// 0x000002A1 System.Void UISceneManager::.ctor()
extern void UISceneManager__ctor_mCC24A77F740109C0DC8BE616A88FC0D632AAAD4A (void);
// 0x000002A2 System.Void UISceneTitleInitialiser::InitWithData(UISceneData)
extern void UISceneTitleInitialiser_InitWithData_m410A78567505DFAA725DF3873948AB9EFB7635E6 (void);
// 0x000002A3 System.Void UISceneTitleInitialiser::.ctor()
extern void UISceneTitleInitialiser__ctor_m67961492CF30E1263C9E295AC4B56024DDDA1672 (void);
// 0x000002A4 System.Void UITopButtonActionContainer::UITopButtonAction(SceneActionType)
// 0x000002A5 System.Void UITopButtonActionContainer::.ctor()
extern void UITopButtonActionContainer__ctor_mC1E2F607E20320604A1C3CF405BE4EDABA566119 (void);
// 0x000002A6 System.Void UIScene::UStart()
extern void UIScene_UStart_m64FE30A98842D02F8E53D22E2A59890BB600CE26 (void);
// 0x000002A7 System.Void UIScene::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice>)
extern void UIScene_OnBluetoothDeviceListUpdated_mC901A53E6428A78BFC09BD296F281CD36A6A5BC5 (void);
// 0x000002A8 System.Void UIScene::OnBluetoothConnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void UIScene_OnBluetoothConnected_mA7BA30BFA2259E253462D71E70A8EDC19ED04BDF (void);
// 0x000002A9 System.Void UIScene::OnBluetoothDisconnected(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice,ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState)
extern void UIScene_OnBluetoothDisconnected_mE5E1FF4E697177F4D8F41419C7FB30E5152B3427 (void);
// 0x000002AA System.Void UIScene::.ctor()
extern void UIScene__ctor_mB379C8871BC01D4DDAB3CDB0BB56F41B1C9F4F05 (void);
// 0x000002AB System.Void ArduinoHM10Test::OnButton(UnityEngine.UI.Button)
extern void ArduinoHM10Test_OnButton_m35132D3B3E892E51EED9C75E9F763856B7C55C6D (void);
// 0x000002AC System.Void ArduinoHM10Test::Reset()
extern void ArduinoHM10Test_Reset_m6E0CA937E454CFBDF6D26D732E1A20A5D6671403 (void);
// 0x000002AD System.Void ArduinoHM10Test::SetState(ArduinoHM10Test/States,System.Single)
extern void ArduinoHM10Test_SetState_mC716B9182D126EBEAFADC91E89D772E0263DA979 (void);
// 0x000002AE System.Void ArduinoHM10Test::StartProcess()
extern void ArduinoHM10Test_StartProcess_m8491400CDC3D27D5E77233FDA82DDA499BF6B4C3 (void);
// 0x000002AF System.Void ArduinoHM10Test::Start()
extern void ArduinoHM10Test_Start_m8C159A224C2185497E2212005A98902FE70D3FED (void);
// 0x000002B0 System.Void ArduinoHM10Test::Update()
extern void ArduinoHM10Test_Update_mA0011AB86A4471ABF01812FDB5DD86D2493EA08D (void);
// 0x000002B1 System.String ArduinoHM10Test::FullUUID(System.String)
extern void ArduinoHM10Test_FullUUID_mB6D69584D381072539E770EE3FC8FFC283C95CCB (void);
// 0x000002B2 System.Boolean ArduinoHM10Test::IsEqual(System.String,System.String)
extern void ArduinoHM10Test_IsEqual_mA3F8EC92DDD423AFF8A4218B375BA75EDB693563 (void);
// 0x000002B3 System.Void ArduinoHM10Test::SendString(System.String)
extern void ArduinoHM10Test_SendString_mCA8D3274B8A4BD2D8EDB41E8E8FA8066D4CC5BBD (void);
// 0x000002B4 System.Void ArduinoHM10Test::SendByte(System.Byte)
extern void ArduinoHM10Test_SendByte_mC95545DE61432BF6017660BAE71E095B438D4F89 (void);
// 0x000002B5 System.Void ArduinoHM10Test::.ctor()
extern void ArduinoHM10Test__ctor_m8F33BF8AC9D83124CBB478434DA80E1B84449E2E (void);
// 0x000002B6 System.Void ArduinoHM10Test::<StartProcess>b__17_0()
extern void ArduinoHM10Test_U3CStartProcessU3Eb__17_0_m36CE56CE68D3AA9F193AD27C9A20F7D0860CBB30 (void);
// 0x000002B7 System.Void ArduinoHM10Test::<Update>b__19_0(System.String,System.String)
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_0_m7FFD8CF46039C98FE844A9A0AC854141D23C92FD (void);
// 0x000002B8 System.Void ArduinoHM10Test::<Update>b__19_1(System.String,System.String,System.String)
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_1_mA821486F22C40F40195F9D7983307E099B33739B (void);
// 0x000002B9 System.Void ArduinoHM10Test::<Update>b__19_2(System.String)
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_2_m7BD33C7816181A95720FE6D6CAF234486F3A2660 (void);
// 0x000002BA System.Void ArduinoHM10Test::<Update>b__19_3(System.String,System.String,System.Byte[])
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_3_mDFEC7D0368D8AC4367C39631B6B632C16FA197E6 (void);
// 0x000002BB System.Void ArduinoHM10Test::<Update>b__19_4(System.String)
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_4_m8AE840EE065907AD031106B5E9B8B69C4F8C4BB6 (void);
// 0x000002BC System.Void ArduinoHM10Test::<Update>b__19_6()
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_6_m315891E662945ABB620C6EE79F18C36222424399 (void);
// 0x000002BD System.Void ArduinoHM10Test::<Update>b__19_5()
extern void ArduinoHM10Test_U3CUpdateU3Eb__19_5_mEADDC51187F60DE571AF2921260C9A99CE1CDD9A (void);
// 0x000002BE System.Void ArduinoHM10Test/<>c::.cctor()
extern void U3CU3Ec__cctor_mE78EC7643CD81ECAF4E3E99514C8697FB4230D57 (void);
// 0x000002BF System.Void ArduinoHM10Test/<>c::.ctor()
extern void U3CU3Ec__ctor_mD8E130AE3B755B4CB7ED3DCD893EF5A8DB3E9CA2 (void);
// 0x000002C0 System.Void ArduinoHM10Test/<>c::<StartProcess>b__17_1(System.String)
extern void U3CU3Ec_U3CStartProcessU3Eb__17_1_mFFAB43C4B031B65171782557077EBC8F910B9133 (void);
// 0x000002C1 System.Void ArduinoHM10Test/<>c::<SendString>b__22_0(System.String)
extern void U3CU3Ec_U3CSendStringU3Eb__22_0_m92A9ED888216707680C3A9B18B0968C799BEC000 (void);
// 0x000002C2 System.Void ArduinoHM10Test/<>c::<SendByte>b__23_0(System.String)
extern void U3CU3Ec_U3CSendByteU3Eb__23_0_m3A6C8566016F763A639E28466D8A419842DCCFFF (void);
// 0x000002C3 System.Void BLETestScript::Show(UnityEngine.Transform)
extern void BLETestScript_Show_m44DAE6593ABC230177BA83EE5D1CED8200FF95D0 (void);
// 0x000002C4 System.Void BLETestScript::Start()
extern void BLETestScript_Start_m419B6CAA6B3923D8889B8B76F03E9C902558A3C5 (void);
// 0x000002C5 System.Void BLETestScript::Update()
extern void BLETestScript_Update_mD4EDD5F8DFEDE83C780222749932C3B5DE8D8ECB (void);
// 0x000002C6 System.Void BLETestScript::.ctor()
extern void BLETestScript__ctor_mF8B5330ED0A1C41A3172E30110E01685379479BC (void);
// 0x000002C7 System.Boolean BusyScript::get_IsBusy()
extern void BusyScript_get_IsBusy_m053AAB00709798F01F635E48D416B14559FB1228 (void);
// 0x000002C8 System.Void BusyScript::set_IsBusy(System.Boolean)
extern void BusyScript_set_IsBusy_m8F3F55E507FC7B112EE9DD0E27C0AB513BC4E46E (void);
// 0x000002C9 System.Void BusyScript::Start()
extern void BusyScript_Start_mEDAE9C7175C8EC120FA22D47D36AFED0C0CBE5EC (void);
// 0x000002CA System.Void BusyScript::Update()
extern void BusyScript_Update_m14406B0FB0CE4092E25AF6241C2562EC1FBEE5F7 (void);
// 0x000002CB System.Void BusyScript::.ctor()
extern void BusyScript__ctor_m1BB43F81288D9AF44EB71F0DBE0549B009844928 (void);
// 0x000002CC System.Boolean CentralNordicScript::get_Connected()
extern void CentralNordicScript_get_Connected_mB4D10D0EE38ABB0FDA88001D9AE864EC1958655E (void);
// 0x000002CD System.Void CentralNordicScript::set_Connected(System.Boolean)
extern void CentralNordicScript_set_Connected_m0B78ED9E8EDB2820B0A3DEB5FD229C1F7C074E2D (void);
// 0x000002CE System.Void CentralNordicScript::Initialize(CentralPeripheralButtonScript)
extern void CentralNordicScript_Initialize_m72E1E64D62EED7EAFB982D6B42A6A424770E8D91 (void);
// 0x000002CF System.Void CentralNordicScript::disconnect(System.Action`1<System.String>)
extern void CentralNordicScript_disconnect_m783495C03AB660377C4A20AA5012D63C52F3DB0D (void);
// 0x000002D0 System.Void CentralNordicScript::OnSend()
extern void CentralNordicScript_OnSend_m54F39835F62A92CE61010819B694DC05CA64FDD7 (void);
// 0x000002D1 System.Void CentralNordicScript::OnBack()
extern void CentralNordicScript_OnBack_m0BFD74C76F8FE7058AEEB405F982560A69A202F4 (void);
// 0x000002D2 System.Void CentralNordicScript::OnConnect()
extern void CentralNordicScript_OnConnect_m7BE0E5AECB0FCB7180AFA2542C547AED0EEA2CC7 (void);
// 0x000002D3 System.Void CentralNordicScript::Start()
extern void CentralNordicScript_Start_m3E4FE04FD15692F00CFFCF694EBFE21E6E04BAD5 (void);
// 0x000002D4 System.Void CentralNordicScript::Update()
extern void CentralNordicScript_Update_mE0B45FC1AB613823FE2FD2586507D4AE447C55E1 (void);
// 0x000002D5 System.String CentralNordicScript::FullUUID(System.String)
extern void CentralNordicScript_FullUUID_mE618DF60A11E3BFE6C46269DCE44682562671436 (void);
// 0x000002D6 System.Boolean CentralNordicScript::IsEqual(System.String,System.String)
extern void CentralNordicScript_IsEqual_m475E8BCA5BB1AD13CD223FDBD2BE93E332332116 (void);
// 0x000002D7 System.Void CentralNordicScript::SendByte(System.Byte)
extern void CentralNordicScript_SendByte_mAC095DFD459FD629271F9422755FDE7DCD708007 (void);
// 0x000002D8 System.Void CentralNordicScript::SendBytes(System.Byte[])
extern void CentralNordicScript_SendBytes_m331D5E4F3A4F5DAB03BE2C20C5008A77FBC50C00 (void);
// 0x000002D9 System.Void CentralNordicScript::.ctor()
extern void CentralNordicScript__ctor_m79F1FCE65DE23FAA6626CD29879D985DDC524689 (void);
// 0x000002DA System.Void CentralNordicScript::<OnBack>b__22_0(System.String)
extern void CentralNordicScript_U3COnBackU3Eb__22_0_mEDFA1DEEEA1FAF80692CA440831CDD267E013342 (void);
// 0x000002DB System.Void CentralNordicScript::<OnConnect>b__23_0(System.String)
extern void CentralNordicScript_U3COnConnectU3Eb__23_0_m2142E584DF8C7AFAD4C54A4EEB30F68BE139E30D (void);
// 0x000002DC System.Void CentralNordicScript::<OnConnect>b__23_3(System.String,System.String,System.String)
extern void CentralNordicScript_U3COnConnectU3Eb__23_3_m2665CE99F6C11A6795173C21CC3E88D29CEA50BD (void);
// 0x000002DD System.Void CentralNordicScript::<OnConnect>b__23_4(System.String)
extern void CentralNordicScript_U3COnConnectU3Eb__23_4_mE2B4E2AAB01E40F8084C8DB006E3EC51377ED262 (void);
// 0x000002DE System.Void CentralNordicScript::<Update>b__25_1(System.String,System.String,System.Byte[])
extern void CentralNordicScript_U3CUpdateU3Eb__25_1_mFE8663531EC31E743D78C1348FB27199873EE008 (void);
// 0x000002DF System.Void CentralNordicScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m6A8E460F4929AB0ADB3D7DD2841B886FAFAD6C20 (void);
// 0x000002E0 System.Void CentralNordicScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m75944093E3490CCAEA7B560225D751F00C517993 (void);
// 0x000002E1 System.Void CentralNordicScript/<>c::<OnConnect>b__23_1(System.String)
extern void U3CU3Ec_U3COnConnectU3Eb__23_1_m560B54A3757F044B002ECE4E28001498991CA902 (void);
// 0x000002E2 System.Void CentralNordicScript/<>c::<OnConnect>b__23_2(System.String,System.String)
extern void U3CU3Ec_U3COnConnectU3Eb__23_2_mEBB8CAE69D2C0842B0AB2852EC058AB18F3BDC70 (void);
// 0x000002E3 System.Void CentralNordicScript/<>c::<Update>b__25_0(System.String,System.String)
extern void U3CU3Ec_U3CUpdateU3Eb__25_0_m015EAD2038EE0AC6F5E4C5AB5341CB777F1E7045 (void);
// 0x000002E4 System.Void CentralNordicScript/<>c::<SendByte>b__28_0(System.String)
extern void U3CU3Ec_U3CSendByteU3Eb__28_0_m66A16B3E3332FF1519C560B45E3AFF382785A038 (void);
// 0x000002E5 System.Void CentralNordicScript/<>c::<SendBytes>b__29_0(System.String)
extern void U3CU3Ec_U3CSendBytesU3Eb__29_0_mD9A4B4857C1925C052CDD2FECAF95FAF3FC67D6F (void);
// 0x000002E6 System.Void CentralPeripheralButtonScript::OnPeripheralSelected()
extern void CentralPeripheralButtonScript_OnPeripheralSelected_mFF58F44D562A4FABE685A636388DECCC1FFF307A (void);
// 0x000002E7 System.Void CentralPeripheralButtonScript::Start()
extern void CentralPeripheralButtonScript_Start_mC040BEE6056BF2DDBAD87DDB01B92C5CE6B47AB8 (void);
// 0x000002E8 System.Void CentralPeripheralButtonScript::Update()
extern void CentralPeripheralButtonScript_Update_m14BD22785B9A9A5ED812F0EE209C616BB8B1E2D5 (void);
// 0x000002E9 System.Void CentralPeripheralButtonScript::.ctor()
extern void CentralPeripheralButtonScript__ctor_mD0E46BF64008D537A7E0CDF8A6CC8DB4DA64D195 (void);
// 0x000002EA System.Boolean CentralRFduinoScript::get_Connected()
extern void CentralRFduinoScript_get_Connected_m20510ED622A2DE87D5FEDA43D1DA9102DA080786 (void);
// 0x000002EB System.Void CentralRFduinoScript::set_Connected(System.Boolean)
extern void CentralRFduinoScript_set_Connected_m72B6D235967E98F1138524B406D5447831891AD4 (void);
// 0x000002EC System.Void CentralRFduinoScript::Initialize(CentralPeripheralButtonScript)
extern void CentralRFduinoScript_Initialize_mA8704F8353E1EF1E90BB24C61FBCF8355E3AFA95 (void);
// 0x000002ED System.Void CentralRFduinoScript::disconnect(System.Action`1<System.String>)
extern void CentralRFduinoScript_disconnect_m2F236AA8C44FBB3EB841AD70FE227C11AC7BC31F (void);
// 0x000002EE System.Void CentralRFduinoScript::OnBack()
extern void CentralRFduinoScript_OnBack_mB1DCBC5552F9DCFEAC74F4E2AEB232CF2950A02D (void);
// 0x000002EF System.Void CentralRFduinoScript::OnConnect()
extern void CentralRFduinoScript_OnConnect_m832DE752BFB7C1E3B051A4A1BA904D4FA6FCC8E8 (void);
// 0x000002F0 System.Void CentralRFduinoScript::OnLED()
extern void CentralRFduinoScript_OnLED_mD3AA209F374D10BA0F6DDC07AC1A1ED31A5EF79F (void);
// 0x000002F1 System.Void CentralRFduinoScript::Start()
extern void CentralRFduinoScript_Start_m7D632025823CA9084FB5CF1E54DBA531A78C7FFE (void);
// 0x000002F2 System.Void CentralRFduinoScript::Update()
extern void CentralRFduinoScript_Update_m697B777AF8954658BFB3DB150B2F07EC5B33E707 (void);
// 0x000002F3 System.String CentralRFduinoScript::FullUUID(System.String)
extern void CentralRFduinoScript_FullUUID_mC5A378CCEA5E95F81E6E9D9480899F9B8928D387 (void);
// 0x000002F4 System.Boolean CentralRFduinoScript::IsEqual(System.String,System.String)
extern void CentralRFduinoScript_IsEqual_mB57FD5C1E73B16F6A7D49934D019B7068AF02A7A (void);
// 0x000002F5 System.Void CentralRFduinoScript::SendByte(System.Byte)
extern void CentralRFduinoScript_SendByte_m718437E0D5C987EAE18D8CFC84E0E253B6E32F09 (void);
// 0x000002F6 System.Void CentralRFduinoScript::SendBytes(System.Byte[])
extern void CentralRFduinoScript_SendBytes_m5E5E454978C05AA38D26253E0381C037C89828E2 (void);
// 0x000002F7 System.Void CentralRFduinoScript::.ctor()
extern void CentralRFduinoScript__ctor_m1F59B804BFE09506CB71D14B2FC929AABC790348 (void);
// 0x000002F8 System.Void CentralRFduinoScript::<OnBack>b__19_0(System.String)
extern void CentralRFduinoScript_U3COnBackU3Eb__19_0_m7087F3E0F6A52F3768C1F138CD245107D307A156 (void);
// 0x000002F9 System.Void CentralRFduinoScript::<OnConnect>b__20_0(System.String)
extern void CentralRFduinoScript_U3COnConnectU3Eb__20_0_mCC9D7769D92795CAAC3D3CE6C0F2E26D3948F007 (void);
// 0x000002FA System.Void CentralRFduinoScript::<OnConnect>b__20_3(System.String,System.String,System.String)
extern void CentralRFduinoScript_U3COnConnectU3Eb__20_3_mEE735F883A8B38CDC665CDA1F5F4CDBD3C5FD5A2 (void);
// 0x000002FB System.Void CentralRFduinoScript::<OnConnect>b__20_4(System.String)
extern void CentralRFduinoScript_U3COnConnectU3Eb__20_4_mD335DD02D3CC6E4BDBF40858B4D4B5E2B37AC662 (void);
// 0x000002FC System.Void CentralRFduinoScript/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mDC8ADB47D0DA675CE609B2ACC9FCDE46A6662F08 (void);
// 0x000002FD System.Void CentralRFduinoScript/<>c__DisplayClass20_0::<OnConnect>b__6(System.String,System.String,System.Byte[])
extern void U3CU3Ec__DisplayClass20_0_U3COnConnectU3Eb__6_m3B2CFBE21A384D3B2C21A10027EFF7D58747DB5E (void);
// 0x000002FE System.Void CentralRFduinoScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m69377EAA0FD4291F00042C5AA39964104BE01519 (void);
// 0x000002FF System.Void CentralRFduinoScript/<>c::.ctor()
extern void U3CU3Ec__ctor_mC27DDF998C3AD274FA1988D8A900BB1564928467 (void);
// 0x00000300 System.Void CentralRFduinoScript/<>c::<OnConnect>b__20_1(System.String)
extern void U3CU3Ec_U3COnConnectU3Eb__20_1_m82A41C0804885F65F6ABC3EB1EB356BEFBE0390C (void);
// 0x00000301 System.Void CentralRFduinoScript/<>c::<OnConnect>b__20_2(System.String,System.String)
extern void U3CU3Ec_U3COnConnectU3Eb__20_2_m8E5A417D6B76C4292AAD52334470EFC329F8997F (void);
// 0x00000302 System.Void CentralRFduinoScript/<>c::<OnConnect>b__20_5(System.String,System.String)
extern void U3CU3Ec_U3COnConnectU3Eb__20_5_mD91A062BD332C9D3ED2512089C941286A3D3F225 (void);
// 0x00000303 System.Void CentralRFduinoScript/<>c::<SendByte>b__27_0(System.String)
extern void U3CU3Ec_U3CSendByteU3Eb__27_0_mB8EDBFE3CEA981AAFBBF49501BEEBFC29D77B914 (void);
// 0x00000304 System.Void CentralScript::Initialize()
extern void CentralScript_Initialize_m0978E4A9B16C8BFCE2225E6E9D5C46461641B8C2 (void);
// 0x00000305 System.Void CentralScript::OnBack()
extern void CentralScript_OnBack_m9D19A090DBAD93723C422BADECB2017FE395E80B (void);
// 0x00000306 System.String CentralScript::BytesToString(System.Byte[])
extern void CentralScript_BytesToString_m5D9B7D139F549EC76B12BE240AD63154AEF4D959 (void);
// 0x00000307 System.Void CentralScript::OnScan()
extern void CentralScript_OnScan_mC39BC4344D05729F2DE42866ECE7FC63BF22CCEE (void);
// 0x00000308 System.Void CentralScript::RemovePeripherals()
extern void CentralScript_RemovePeripherals_mCA781D0EAE2A33774A0FB9D173FF71E2BF249625 (void);
// 0x00000309 System.Void CentralScript::AddPeripheral(System.String,System.String)
extern void CentralScript_AddPeripheral_m6C757EF948943858604AE2DD5E9D0BB864157E92 (void);
// 0x0000030A System.Void CentralScript::Start()
extern void CentralScript_Start_m8753BD4B9B6DAAF1CD0B28B46B1CC8E1A0E069DF (void);
// 0x0000030B System.Void CentralScript::Update()
extern void CentralScript_Update_mFAE4CDCB729EA6C21E3719B18E0EE1B6C5413AD3 (void);
// 0x0000030C System.Void CentralScript::.ctor()
extern void CentralScript__ctor_mA48C8F08550695BADCE5FDAEE879E3B2A58E34E3 (void);
// 0x0000030D System.Void CentralScript::<OnBack>b__10_0()
extern void CentralScript_U3COnBackU3Eb__10_0_m7ED365E89B5222E657B395005A18BFEB265B5B42 (void);
// 0x0000030E System.Void CentralScript::<OnScan>b__12_0(System.String,System.String)
extern void CentralScript_U3COnScanU3Eb__12_0_mA8C989B99223D88541E6928784E5EFAD0FDC790F (void);
// 0x0000030F System.Void CentralScript::<OnScan>b__12_1(System.String,System.String,System.Int32,System.Byte[])
extern void CentralScript_U3COnScanU3Eb__12_1_mD2ABEB0178A64FBAE05078028B298018092C6F95 (void);
// 0x00000310 System.Void CentralScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m41EEF78877B3DE37C2CB4E6DC659803C81148A4B (void);
// 0x00000311 System.Void CentralScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m2DD03A7AED33D9960ACCDEA1E333919A5D599AB7 (void);
// 0x00000312 System.Void CentralScript/<>c::<Initialize>b__9_0()
extern void U3CU3Ec_U3CInitializeU3Eb__9_0_mCFF0125B90918A91EB3495AE2ADCACCAFB83FEBA (void);
// 0x00000313 System.Void CentralScript/<>c::<Initialize>b__9_1(System.String)
extern void U3CU3Ec_U3CInitializeU3Eb__9_1_mAD2797F80B8CBD58E485F903AAF7BE0D3A8AC75E (void);
// 0x00000314 System.Boolean CentralTISensorTagScript::get_Connected()
extern void CentralTISensorTagScript_get_Connected_mB7F2C137CDF052907A725053DA9C662F77296AAE (void);
// 0x00000315 System.Void CentralTISensorTagScript::set_Connected(System.Boolean)
extern void CentralTISensorTagScript_set_Connected_mB695AEF960E96E06D78165C8A32CEF65D1735852 (void);
// 0x00000316 System.Void CentralTISensorTagScript::Initialize(CentralPeripheralButtonScript)
extern void CentralTISensorTagScript_Initialize_m067928A42BA9ACC712562D23CBBB4BC9CC519728 (void);
// 0x00000317 System.Void CentralTISensorTagScript::disconnect(System.Action`1<System.String>)
extern void CentralTISensorTagScript_disconnect_m2C941948BCF094242A83A1E706B08E7F5C2B732D (void);
// 0x00000318 System.Void CentralTISensorTagScript::OnBack()
extern void CentralTISensorTagScript_OnBack_mCC21259E7191B1CEE4B4EB012FD943CB3D675779 (void);
// 0x00000319 System.Void CentralTISensorTagScript::OnCharacteristicNotification(System.String,System.String,System.Byte[])
extern void CentralTISensorTagScript_OnCharacteristicNotification_m76A339033B8243BD51D8980527FC1CA74CF0F72C (void);
// 0x0000031A System.Boolean CentralTISensorTagScript::get_TemperatureEnabled()
extern void CentralTISensorTagScript_get_TemperatureEnabled_mEEE95C38C687641D6F8FCD417BDAA037C2E8F2E7 (void);
// 0x0000031B System.Void CentralTISensorTagScript::set_TemperatureEnabled(System.Boolean)
extern void CentralTISensorTagScript_set_TemperatureEnabled_m502C3B0745C87A39D7F4AC07F7FCCB35DDDDF379 (void);
// 0x0000031C System.Void CentralTISensorTagScript::OnTemperatureEnable()
extern void CentralTISensorTagScript_OnTemperatureEnable_m26752055984FB52FA829ED7257EA526CA758E030 (void);
// 0x0000031D System.Boolean CentralTISensorTagScript::get_AccelerometerEnabled()
extern void CentralTISensorTagScript_get_AccelerometerEnabled_m0BEEBE4E1765B941368545C830B84C6C827EF58D (void);
// 0x0000031E System.Void CentralTISensorTagScript::set_AccelerometerEnabled(System.Boolean)
extern void CentralTISensorTagScript_set_AccelerometerEnabled_m981B7DA07BDE9FC2EC121CB8D5E5E6EC87967E24 (void);
// 0x0000031F System.Void CentralTISensorTagScript::OnAccelerometerEnable()
extern void CentralTISensorTagScript_OnAccelerometerEnable_m75CB038A90EE2168ED9FC2B063EABCEE6D8D124B (void);
// 0x00000320 System.Void CentralTISensorTagScript::OnConnect()
extern void CentralTISensorTagScript_OnConnect_m5A9791F233463B0B969324313E1E3F23E9E6C4CF (void);
// 0x00000321 System.Void CentralTISensorTagScript::Start()
extern void CentralTISensorTagScript_Start_m2696A94C8F59B2B6AF1FC9CA3E3652482F6A2AF4 (void);
// 0x00000322 System.Void CentralTISensorTagScript::Update()
extern void CentralTISensorTagScript_Update_mA74B1D6133F0CF08228DFCB26A5F3C1ECB7F2414 (void);
// 0x00000323 System.Single CentralTISensorTagScript::AmbientTemperature(System.Byte[])
extern void CentralTISensorTagScript_AmbientTemperature_mF58E0CFC5E974CFA83393A48FA160445FD1525F8 (void);
// 0x00000324 System.Single CentralTISensorTagScript::TargetTemperature(System.Byte[],System.Single)
extern void CentralTISensorTagScript_TargetTemperature_mC39D04D4FF69587E96FC2558D1890DCF601318FC (void);
// 0x00000325 System.Int32 CentralTISensorTagScript::CharSignedAtOffset(System.Byte[],System.Int32)
extern void CentralTISensorTagScript_CharSignedAtOffset_mD8CFF14A1350C74475E5F38BEF4A235370CF13C5 (void);
// 0x00000326 System.Int32 CentralTISensorTagScript::ShortSignedAtOffset(System.Byte[],System.Int32)
extern void CentralTISensorTagScript_ShortSignedAtOffset_m056676E8BA2D767FE98489E1FEDA98193071843B (void);
// 0x00000327 System.Int32 CentralTISensorTagScript::ShortUnsignedAtOffset(System.Byte[],System.Int32)
extern void CentralTISensorTagScript_ShortUnsignedAtOffset_m997A0962166040ABFE0EC576060222439D7B7051 (void);
// 0x00000328 System.Void CentralTISensorTagScript::EnableFeature(System.String,System.String,System.Byte,System.Action`1<System.String>)
extern void CentralTISensorTagScript_EnableFeature_m6E917D612E797BD6EA107733D8167F2BD2EF1F65 (void);
// 0x00000329 System.String CentralTISensorTagScript::FullUUID(System.String)
extern void CentralTISensorTagScript_FullUUID_m8F5C96109EF513FD218B0E89269A47413722ACB6 (void);
// 0x0000032A System.Boolean CentralTISensorTagScript::IsEqual(System.String,System.String)
extern void CentralTISensorTagScript_IsEqual_m23C63B8C5A306BF79D10D5C12A7FF26C50E96E03 (void);
// 0x0000032B System.Void CentralTISensorTagScript::SendByte(System.String,System.String,System.Byte,System.Action`1<System.String>)
extern void CentralTISensorTagScript_SendByte_mB218754C562B5C857D7173B68BDC35E7A801E8AA (void);
// 0x0000032C System.Void CentralTISensorTagScript::SendBytes(System.String,System.String,System.Byte[],System.Action`1<System.String>)
extern void CentralTISensorTagScript_SendBytes_mA82CA88B7A85782CC73FB5A22F9188739C8E946C (void);
// 0x0000032D System.Void CentralTISensorTagScript::.ctor()
extern void CentralTISensorTagScript__ctor_m4591A737A97550438303C43A2395ED1F118EA421 (void);
// 0x0000032E System.Void CentralTISensorTagScript::<OnBack>b__27_0(System.String)
extern void CentralTISensorTagScript_U3COnBackU3Eb__27_0_mE21F73030B2AC0EDDCB78B0653AFA7F454C8A48A (void);
// 0x0000032F System.Void CentralTISensorTagScript::<OnConnect>b__39_0(System.String)
extern void CentralTISensorTagScript_U3COnConnectU3Eb__39_0_mEE9F19B743FC658B7FF6E42BF5A4353839C54F7F (void);
// 0x00000330 System.Void CentralTISensorTagScript::<OnConnect>b__39_1(System.String)
extern void CentralTISensorTagScript_U3COnConnectU3Eb__39_1_mE48ECDED84C58A11E9C5969ED8989561CF311502 (void);
// 0x00000331 System.Void CentralTISensorTagScript::<OnConnect>b__39_3(System.String,System.String,System.String)
extern void CentralTISensorTagScript_U3COnConnectU3Eb__39_3_m1C283F12E9B7E1F32A11FC6539E8E8E066C8AEF0 (void);
// 0x00000332 System.Void CentralTISensorTagScript::<OnConnect>b__39_4(System.String)
extern void CentralTISensorTagScript_U3COnConnectU3Eb__39_4_m985D75FEC053B42DEC8FDB2CBDAA7879BD82ADEA (void);
// 0x00000333 System.Void CentralTISensorTagScript/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m94E6FBE0A52BAD4780A01C53BD786A7D9D38AAB6 (void);
// 0x00000334 System.Void CentralTISensorTagScript/<>c__DisplayClass32_0::<set_TemperatureEnabled>b__0(System.String)
extern void U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__0_m131177BFC4483F7FE6C11EDE5228F3DE455FA96B (void);
// 0x00000335 System.Void CentralTISensorTagScript/<>c__DisplayClass32_0::<set_TemperatureEnabled>b__2(System.String)
extern void U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__2_m2BD0B640987509255E2FC540A30FAC85B9E3715C (void);
// 0x00000336 System.Void CentralTISensorTagScript/<>c__DisplayClass32_0::<set_TemperatureEnabled>b__1(System.String)
extern void U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__1_m11592206803A91127CA6AE24EDE32FD151D2CAB4 (void);
// 0x00000337 System.Void CentralTISensorTagScript/<>c__DisplayClass32_0::<set_TemperatureEnabled>b__3(System.String,System.String)
extern void U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__3_mA5F8098F407C003C6BADD666BA0AA01143272865 (void);
// 0x00000338 System.Void CentralTISensorTagScript/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mB3744DC6286098C5BF004E9291D4F18B20E4010A (void);
// 0x00000339 System.Void CentralTISensorTagScript/<>c__DisplayClass37_0::<set_AccelerometerEnabled>b__0(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__0_m96C60E416F26FB6F34659190FD194F5B9D4D6884 (void);
// 0x0000033A System.Void CentralTISensorTagScript/<>c__DisplayClass37_0::<set_AccelerometerEnabled>b__2(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__2_mC2A59E4BC6412BA1B63D4D10C0A74FF54D385B92 (void);
// 0x0000033B System.Void CentralTISensorTagScript/<>c__DisplayClass37_0::<set_AccelerometerEnabled>b__1(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__1_mADF2E99AFB4F28204138D9C78E5A6B4D93754662 (void);
// 0x0000033C System.Void CentralTISensorTagScript/<>c__DisplayClass37_0::<set_AccelerometerEnabled>b__3(System.String,System.String)
extern void U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__3_mFA3820B130C07B301F23DF610BF9875FEFD2EFA0 (void);
// 0x0000033D System.Void CentralTISensorTagScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m3B0E7CED6B7E3072A62C0BDA596D77B3A9A26122 (void);
// 0x0000033E System.Void CentralTISensorTagScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m733791A8C5123BF114A63E6E734BDF66FE47C15F (void);
// 0x0000033F System.Void CentralTISensorTagScript/<>c::<OnConnect>b__39_2(System.String,System.String)
extern void U3CU3Ec_U3COnConnectU3Eb__39_2_mC2B5E9CAD312DF5F8FF7D9D2869D2D11D78A4397 (void);
// 0x00000340 System.Boolean PeripheralScript::get_IsAdvertising()
extern void PeripheralScript_get_IsAdvertising_mBE94A46583148D318161B8FA08746F11B44120EE (void);
// 0x00000341 System.Void PeripheralScript::set_IsAdvertising(System.Boolean)
extern void PeripheralScript_set_IsAdvertising_mCE8CA30098CC427AA0266BEE74FF3D806B47A970 (void);
// 0x00000342 System.Void PeripheralScript::Initialize()
extern void PeripheralScript_Initialize_m093F1CDC1A67F4A0682EA2F341E370441C314DC4 (void);
// 0x00000343 System.Void PeripheralScript::OnBack()
extern void PeripheralScript_OnBack_m3F79CD4B20AF8B827304A44347C42CC1D022E885 (void);
// 0x00000344 System.Void PeripheralScript::OnStartAdvertising()
extern void PeripheralScript_OnStartAdvertising_m9D39DB3462F4C3A9FCAA0E824BBFCF029BE958AB (void);
// 0x00000345 System.Void PeripheralScript::OnButton1()
extern void PeripheralScript_OnButton1_m53A14D555369954DB73F3CF9D9F4EEBFDC7B14FF (void);
// 0x00000346 System.Void PeripheralScript::Start()
extern void PeripheralScript_Start_m45DBB471F29CB0ECD212B12A3792BF25A23BF076 (void);
// 0x00000347 System.Void PeripheralScript::Update()
extern void PeripheralScript_Update_m17CD3DE2DF754B770A1B5E2FE8247D9DF75D0741 (void);
// 0x00000348 System.Void PeripheralScript::.ctor()
extern void PeripheralScript__ctor_mB2122C6CD3B43BACFE1A478ADC8D08162B9D9C47 (void);
// 0x00000349 System.Void PeripheralScript::<Initialize>b__11_0()
extern void PeripheralScript_U3CInitializeU3Eb__11_0_m34A5A132CE918EDB975268E83B9AA6AFB0B1373E (void);
// 0x0000034A System.Void PeripheralScript::<Initialize>b__11_2(System.String,System.Byte[])
extern void PeripheralScript_U3CInitializeU3Eb__11_2_m39B8669653FAC17847340087FEDB5A69FE2915C5 (void);
// 0x0000034B System.Void PeripheralScript::<Initialize>b__11_3(System.String)
extern void PeripheralScript_U3CInitializeU3Eb__11_3_m0C09DF069961ACBCB9008C934646CC3361E43E03 (void);
// 0x0000034C System.Void PeripheralScript::<OnBack>b__12_0()
extern void PeripheralScript_U3COnBackU3Eb__12_0_mF4DC435670714AD4819ACDC4A97FD1936088260A (void);
// 0x0000034D System.Void PeripheralScript::<OnStartAdvertising>b__13_0()
extern void PeripheralScript_U3COnStartAdvertisingU3Eb__13_0_mE0D49D91F4470D4B81737597F5A62FDABE2834E5 (void);
// 0x0000034E System.Void PeripheralScript::<OnStartAdvertising>b__13_1()
extern void PeripheralScript_U3COnStartAdvertisingU3Eb__13_1_mC4CDF009EFB10744DA26A115D4AE635716C2CF80 (void);
// 0x0000034F System.Void PeripheralScript/<>c::.cctor()
extern void U3CU3Ec__cctor_mDFB60C35C9DF473D1025C6DA6C4B967E778D3695 (void);
// 0x00000350 System.Void PeripheralScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m33E34EF634E8B745E21A0600C03A7C9B7F778EA8 (void);
// 0x00000351 System.Void PeripheralScript/<>c::<Initialize>b__11_1(System.String)
extern void U3CU3Ec_U3CInitializeU3Eb__11_1_mDE0A2D427F95E39F34D735922FA613A53D3E9D32 (void);
// 0x00000352 System.Void TypeSelectionScript::OnCentral()
extern void TypeSelectionScript_OnCentral_m7FD1E97097B5A048EBEF7DBE77CACE2A8EB3FD8A (void);
// 0x00000353 System.Void TypeSelectionScript::OnPeripheral()
extern void TypeSelectionScript_OnPeripheral_mC81264BAE3333F1C513BAABDC7C554568D89B434 (void);
// 0x00000354 System.Void TypeSelectionScript::Start()
extern void TypeSelectionScript_Start_m2D3A1E8144C78329439A3B04892F6F014DCAC1A0 (void);
// 0x00000355 System.Void TypeSelectionScript::.ctor()
extern void TypeSelectionScript__ctor_mF0B6024CFA2EFB3D34CC310B2386EBA9F037C90E (void);
// 0x00000356 System.Void iBeaconExampleScript::Start()
extern void iBeaconExampleScript_Start_mE12533AC500E0A0EEBC4976EC40171E3A656BC4A (void);
// 0x00000357 System.Single iBeaconExampleScript::Distance(System.Single,System.Single,System.Single)
extern void iBeaconExampleScript_Distance_mDEF0A10D0D1AC4B1A416DC0332078F1420BABDF9 (void);
// 0x00000358 System.Void iBeaconExampleScript::Update()
extern void iBeaconExampleScript_Update_m344CB6059B26C94FBAD76DD5E6D2F923B8CBE735 (void);
// 0x00000359 System.Void iBeaconExampleScript::.ctor()
extern void iBeaconExampleScript__ctor_m11B3ABE05E3549CCEDB1CFEC8D3BB12B21C1B96A (void);
// 0x0000035A System.Void iBeaconExampleScript::<Start>b__6_0()
extern void iBeaconExampleScript_U3CStartU3Eb__6_0_m5A04CD1F43207010FAD424E6145119ED20014BD9 (void);
// 0x0000035B System.Void iBeaconExampleScript::<Update>b__8_0(BluetoothLEHardwareInterface/iBeaconData)
extern void iBeaconExampleScript_U3CUpdateU3Eb__8_0_m19806E1BBBDC1C425677FD1C8DE3D8CB4F140206 (void);
// 0x0000035C System.Void iBeaconExampleScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m4993D19C65869E19D658DED647919196B52B1584 (void);
// 0x0000035D System.Void iBeaconExampleScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m22878EA6C45B2A8A4615F4D981C7D76A7AA8497B (void);
// 0x0000035E System.Void iBeaconExampleScript/<>c::<Start>b__6_1(System.String)
extern void U3CU3Ec_U3CStartU3Eb__6_1_m5133AC403E9880F614B3088C42E41FEE1B559416 (void);
// 0x0000035F System.Void iBeaconItemScript::.ctor()
extern void iBeaconItemScript__ctor_m0AF4014B443A8AB590D0F56A80FA37A9CE7EDE54 (void);
// 0x00000360 System.Void DeviceObject::.ctor()
extern void DeviceObject__ctor_mE090EDCDD1FB691D9EACF3522E91C067102D6F74 (void);
// 0x00000361 System.Void DeviceObject::.ctor(System.String,System.String)
extern void DeviceObject__ctor_m6A6654880F94D7D8A7900543BCBF737656A94DBF (void);
// 0x00000362 System.Void FoundDeviceListScript::Start()
extern void FoundDeviceListScript_Start_m3F1C07D5694A849CEC04532FAE1BF5DA9B9AE086 (void);
// 0x00000363 System.Void FoundDeviceListScript::.ctor()
extern void FoundDeviceListScript__ctor_m11A65DDA40FD186744AB950BF15E9A9D1FBA4CB2 (void);
// 0x00000364 System.Void Level1Script::OnScanClick()
extern void Level1Script_OnScanClick_m22231BA949C2D38047E56957DFCA0ADFDF578835 (void);
// 0x00000365 System.Void Level1Script::OnStartLevel2()
extern void Level1Script_OnStartLevel2_mF2C3A93EB77EFCFB69549CA43C4096211BB932A0 (void);
// 0x00000366 System.Void Level1Script::.ctor()
extern void Level1Script__ctor_m965D1608582E72436468DB0406A73314ADEE2EEE (void);
// 0x00000367 System.Void Level1Script/<>c::.cctor()
extern void U3CU3Ec__cctor_m853EDED29231D330FD822AF7642579B39C494528 (void);
// 0x00000368 System.Void Level1Script/<>c::.ctor()
extern void U3CU3Ec__ctor_mB35F80B1CBCC080A78FEE3C6A7F6313EB1C36CF7 (void);
// 0x00000369 System.Void Level1Script/<>c::<OnScanClick>b__0_0()
extern void U3CU3Ec_U3COnScanClickU3Eb__0_0_m05364810185D819E1296523100444D8535D2DECF (void);
// 0x0000036A System.Void Level1Script/<>c::<OnScanClick>b__0_2(System.String,System.String)
extern void U3CU3Ec_U3COnScanClickU3Eb__0_2_mA84D57366C68FF13646F16E786B460BD0A7CEB2C (void);
// 0x0000036B System.Void Level1Script/<>c::<OnScanClick>b__0_1(System.String)
extern void U3CU3Ec_U3COnScanClickU3Eb__0_1_m5A1AACBBF71106FA49977250CDCD9F997A993C9F (void);
// 0x0000036C System.Void Level2Script::Start()
extern void Level2Script_Start_m222ECCEE69D8F0F4DE2F8FCE40228242AFD1560F (void);
// 0x0000036D System.Void Level2Script::OnCharacteristic(System.String,System.Byte[])
extern void Level2Script_OnCharacteristic_m676D517DCEEA51872E8CB5E6A8A9B39742CD4707 (void);
// 0x0000036E System.Void Level2Script::OnSubscribeClick(System.Int32)
extern void Level2Script_OnSubscribeClick_mF1BFBCAED3E17B14C473EA655D19ED75161FC796 (void);
// 0x0000036F System.Void Level2Script::OnButtonClick(System.Int32)
extern void Level2Script_OnButtonClick_m5B19F725271C5E0E4CC9A8A7072603C90D267306 (void);
// 0x00000370 System.String Level2Script::FullUUID(System.String)
extern void Level2Script_FullUUID_m84FE10A19C25D264DED67BCF0FEF51777E6C2353 (void);
// 0x00000371 System.Void Level2Script::.ctor()
extern void Level2Script__ctor_mD3E1FC4F5E33B58AE34EE1C1B61C5D7152EDF0EC (void);
// 0x00000372 System.Void Level2Script/<>c::.cctor()
extern void U3CU3Ec__cctor_mA6154BDEDA26CF2335EA3AF750C54ED0DD3ABB3A (void);
// 0x00000373 System.Void Level2Script/<>c::.ctor()
extern void U3CU3Ec__ctor_mD36CA45D8E35F45299DBA81A924AE72FC5007BA1 (void);
// 0x00000374 System.Void Level2Script/<>c::<OnSubscribeClick>b__5_0(System.String,System.Byte[])
extern void U3CU3Ec_U3COnSubscribeClickU3Eb__5_0_m8C5D3BE4EA21484F4EA7D3D2B9AF799575FD898F (void);
// 0x00000375 System.Void Level2Script/<>c::<OnButtonClick>b__6_2(System.String)
extern void U3CU3Ec_U3COnButtonClickU3Eb__6_2_mF61D472EB0362B48AA39A5559877306486E5F784 (void);
// 0x00000376 System.Void Level2Script/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m2C0C20881BE1D52FA31F0217AC977398DB9D9FC0 (void);
// 0x00000377 System.Void Level2Script/<>c__DisplayClass6_0::<OnButtonClick>b__0(System.String)
extern void U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__0_mE13DBFF3AA5C8107A793E9140351DACD17B87502 (void);
// 0x00000378 System.Void Level2Script/<>c__DisplayClass6_0::<OnButtonClick>b__4(System.String)
extern void U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__4_m74661962373AA309E09591FE93CFD444539D2B3C (void);
// 0x00000379 System.Void Level2Script/<>c__DisplayClass6_0::<OnButtonClick>b__1(System.String)
extern void U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__1_m0F84027C9D91ECE90F5B5A9469CDA8B6A64EDF91 (void);
// 0x0000037A System.Void Level2Script/<>c__DisplayClass6_0::<OnButtonClick>b__3(System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__3_mB7D08797F6D47F61601D4566FF7A971B938B3B00 (void);
// 0x0000037B System.Boolean NetworkExampleTest::get_AllCharacteristicsFound()
extern void NetworkExampleTest_get_AllCharacteristicsFound_mA5DC9422D6E455BC2504FFBFC70551C83DF7A38F (void);
// 0x0000037C NetworkExampleTest/Characteristic NetworkExampleTest::GetCharacteristic(System.String,System.String)
extern void NetworkExampleTest_GetCharacteristic_mFA15AADD72536F934283D20BD2DBA4E164483C0A (void);
// 0x0000037D System.Void NetworkExampleTest::set_StatusMessage(System.String)
extern void NetworkExampleTest_set_StatusMessage_mFF8F8F1A9B1123947E7CB4A9C10EFB02D61035CA (void);
// 0x0000037E System.Void NetworkExampleTest::OnButton(UnityEngine.UI.Button)
extern void NetworkExampleTest_OnButton_m0A03681FC8D18625BC90FA9A860F4271477868F5 (void);
// 0x0000037F System.Void NetworkExampleTest::Reset()
extern void NetworkExampleTest_Reset_m3C63CF541DDEE63A714E195BFB95A403709E6F9B (void);
// 0x00000380 System.Void NetworkExampleTest::SetState(NetworkExampleTest/States,System.Single)
extern void NetworkExampleTest_SetState_m04548AD181831D96BFAD2DA1D24A6D6290A93E10 (void);
// 0x00000381 System.Void NetworkExampleTest::StartProcess()
extern void NetworkExampleTest_StartProcess_mC7D4ADC4757F9296C7EAE11AB17DBD3C07B76627 (void);
// 0x00000382 System.Void NetworkExampleTest::Start()
extern void NetworkExampleTest_Start_mAC97BE5F3CDC86B84A6551DA44328DC135C06B52 (void);
// 0x00000383 System.Void NetworkExampleTest::Update()
extern void NetworkExampleTest_Update_m43895EA27FAB0F6FC239CB158233846927AD062A (void);
// 0x00000384 System.Boolean NetworkExampleTest::IsEqual(System.String,System.String)
extern void NetworkExampleTest_IsEqual_m950B533BA38B0C814F5AB8FC52390FD688A0AD90 (void);
// 0x00000385 System.Void NetworkExampleTest::.ctor()
extern void NetworkExampleTest__ctor_mB4EF9134B3A6FE5E022E355B0F82C4985D89A4EC (void);
// 0x00000386 System.Void NetworkExampleTest::.cctor()
extern void NetworkExampleTest__cctor_m2E372A1C57E6E8A93A414FE33C75C400CCC82868 (void);
// 0x00000387 System.Void NetworkExampleTest::<OnButton>b__20_2(System.String,System.Byte[])
extern void NetworkExampleTest_U3COnButtonU3Eb__20_2_m37F1B0C7042F33D4158305C65BB5BF7F5010F5D5 (void);
// 0x00000388 System.Void NetworkExampleTest::<OnButton>b__20_3(System.String)
extern void NetworkExampleTest_U3COnButtonU3Eb__20_3_mB2EB84E726EFD7533C11AE4C86FFF50D964F7288 (void);
// 0x00000389 System.Void NetworkExampleTest::<OnButton>b__20_4()
extern void NetworkExampleTest_U3COnButtonU3Eb__20_4_mAA214E57BF35B422A9C21FC17F8C851D71ADE6B3 (void);
// 0x0000038A System.Void NetworkExampleTest::<OnButton>b__20_1()
extern void NetworkExampleTest_U3COnButtonU3Eb__20_1_m37F3A199B6261BC58CF28CA6DD8950AF7199958B (void);
// 0x0000038B System.Void NetworkExampleTest::<StartProcess>b__23_1(System.String)
extern void NetworkExampleTest_U3CStartProcessU3Eb__23_1_mC3CCC037117F008D48F2E156BE979D112A11A37B (void);
// 0x0000038C System.Void NetworkExampleTest::<Update>b__25_0(System.String,System.String)
extern void NetworkExampleTest_U3CUpdateU3Eb__25_0_mFEB49F11F15755663C83F39340D89285FCCF4B95 (void);
// 0x0000038D System.Void NetworkExampleTest::<Update>b__25_1(System.String,System.String,System.String)
extern void NetworkExampleTest_U3CUpdateU3Eb__25_1_m95A9A2A9E0052B54483A0FE70966F148B8BF2DC9 (void);
// 0x0000038E System.Void NetworkExampleTest::<Update>b__25_2(System.String)
extern void NetworkExampleTest_U3CUpdateU3Eb__25_2_m5626B2246956E2E3DD88DD150786658D187A7E67 (void);
// 0x0000038F System.Void NetworkExampleTest::<Update>b__25_3(System.String,System.String,System.Byte[])
extern void NetworkExampleTest_U3CUpdateU3Eb__25_3_m9835E71C44C1BF1090809677FDFAD7C9ABC089B5 (void);
// 0x00000390 System.Void NetworkExampleTest/Characteristic::.ctor()
extern void Characteristic__ctor_m420E5DC74EF033AE15161A7EDCB9CE08A5FDCA2F (void);
// 0x00000391 System.Void NetworkExampleTest/<>c::.cctor()
extern void U3CU3Ec__cctor_mEF08672377086230E16E84410DCCBB053CE02E5C (void);
// 0x00000392 System.Void NetworkExampleTest/<>c::.ctor()
extern void U3CU3Ec__ctor_mD2DE66AF526BB6DD578852404D06040798FC0B28 (void);
// 0x00000393 System.Boolean NetworkExampleTest/<>c::<get_AllCharacteristicsFound>b__10_0(NetworkExampleTest/Characteristic)
extern void U3CU3Ec_U3Cget_AllCharacteristicsFoundU3Eb__10_0_m08D35E079583C95E540B7F278BB6B66669BF4B12 (void);
// 0x00000394 System.Void NetworkExampleTest/<>c::<OnButton>b__20_0(System.String)
extern void U3CU3Ec_U3COnButtonU3Eb__20_0_m776E530755002F4564E3A9475BA3861EF08941B5 (void);
// 0x00000395 System.Void NetworkExampleTest/<>c::<StartProcess>b__23_0()
extern void U3CU3Ec_U3CStartProcessU3Eb__23_0_mDAF160B65CC95D8E48CE310891900967E790C804 (void);
// 0x00000396 System.Void NetworkExampleTest/<>c::<Update>b__25_4(System.String)
extern void U3CU3Ec_U3CUpdateU3Eb__25_4_mC008716C143E64AEACD193C2D16F51C0A766ECB9 (void);
// 0x00000397 System.Void NetworkExampleTest/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m7D220B79A9FB9717E7CD0F38A14EA442B184758E (void);
// 0x00000398 System.Boolean NetworkExampleTest/<>c__DisplayClass11_0::<GetCharacteristic>b__0(NetworkExampleTest/Characteristic)
extern void U3CU3Ec__DisplayClass11_0_U3CGetCharacteristicU3Eb__0_mFB7660D53AEF6E8B5C12B643BDFA8B90011AC943 (void);
// 0x00000399 System.Void ScannedItemScript::.ctor()
extern void ScannedItemScript__ctor_mCB4CA033D4B1B39258584BC06BC7B25196497AA9 (void);
// 0x0000039A System.Void ScannerTestScript::OnButton()
extern void ScannerTestScript_OnButton_mC9E7979EFE3346A12E9A76D3BF09BAA4D9A904E3 (void);
// 0x0000039B System.Void ScannerTestScript::OnStopScanning()
extern void ScannerTestScript_OnStopScanning_m9A966894A1AADD160B4AF393609255CBBEA26769 (void);
// 0x0000039C System.Void ScannerTestScript::Start()
extern void ScannerTestScript_Start_m481E51D8E43FBFD9A261C5E732F525CF034C21F6 (void);
// 0x0000039D System.Void ScannerTestScript::Update()
extern void ScannerTestScript_Update_m07240E7DF1152543BD37823CEE1BE959C0C7CC66 (void);
// 0x0000039E System.Void ScannerTestScript::.ctor()
extern void ScannerTestScript__ctor_m495FC70CB6DCF75540B96469159A8C0F90CB01E7 (void);
// 0x0000039F System.Void ScannerTestScript::<Start>b__8_0()
extern void ScannerTestScript_U3CStartU3Eb__8_0_m1E4AF5A07F2C5026F3D042D50C0A1EA49EAE144E (void);
// 0x000003A0 System.Void ScannerTestScript::<Update>b__9_0(System.String,System.String,System.Int32,System.Byte[])
extern void ScannerTestScript_U3CUpdateU3Eb__9_0_m9974D219746A29786BDCF7A01827BAD1F82D7700 (void);
// 0x000003A1 System.Void ScannerTestScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m307241419024A810E34D0E111CFAAAFB9334D54B (void);
// 0x000003A2 System.Void ScannerTestScript/<>c::.ctor()
extern void U3CU3Ec__ctor_mBBAFE31356C8A6D31EF45095F68CF38DAC4293D4 (void);
// 0x000003A3 System.Void ScannerTestScript/<>c::<OnButton>b__6_0()
extern void U3CU3Ec_U3COnButtonU3Eb__6_0_mED4215E85EB44D0BA7507CDE4313FF6E46D7579F (void);
// 0x000003A4 System.Void ScannerTestScript/<>c::<Start>b__8_1(System.String)
extern void U3CU3Ec_U3CStartU3Eb__8_1_m57973128606DDD35C40CA66B93F0925C24E0995A (void);
// 0x000003A5 System.Boolean SensorBugTest::get_AllCharacteristicsFound()
extern void SensorBugTest_get_AllCharacteristicsFound_m33075EF2FEB9E60BB4FA738EC4DA0DD190CADFA8 (void);
// 0x000003A6 SensorBugTest/Characteristic SensorBugTest::GetCharacteristic(System.String,System.String)
extern void SensorBugTest_GetCharacteristic_m6D9FFBC8655AA66847C84406AC83DA37AFABBE64 (void);
// 0x000003A7 System.Void SensorBugTest::set_SensorBugStatusMessage(System.String)
extern void SensorBugTest_set_SensorBugStatusMessage_mD3A5202E28E5A3D6A635261F2A45A69742F78587 (void);
// 0x000003A8 System.Void SensorBugTest::Reset()
extern void SensorBugTest_Reset_mDE8104994D7B69CF76B259282877739C8EE88C1A (void);
// 0x000003A9 System.Void SensorBugTest::SetState(SensorBugTest/States,System.Single)
extern void SensorBugTest_SetState_m0C9CC41A7EF281487488FD95F5AEEE31A3B72E84 (void);
// 0x000003AA System.Void SensorBugTest::StartProcess()
extern void SensorBugTest_StartProcess_m3CE0B838BB42459744DFABB1B6B474231B68127B (void);
// 0x000003AB System.Void SensorBugTest::Start()
extern void SensorBugTest_Start_m31A103A54C1C078E403A39EDC15A81723E45C1EE (void);
// 0x000003AC System.Void SensorBugTest::Update()
extern void SensorBugTest_Update_m71B88BDD303887AB954EDFFA8D03CF2EA32AE770 (void);
// 0x000003AD System.Boolean SensorBugTest::IsEqual(System.String,System.String)
extern void SensorBugTest_IsEqual_m7628CEC6FC6F70B8C87D1D9D896820C7697DA45C (void);
// 0x000003AE System.Void SensorBugTest::.ctor()
extern void SensorBugTest__ctor_m63BE02B1B067751A5EDD743C6DC746C2359BE503 (void);
// 0x000003AF System.Void SensorBugTest::.cctor()
extern void SensorBugTest__cctor_m00B411B6529224A74BC1041517C7BF820C0697B7 (void);
// 0x000003B0 System.Void SensorBugTest::<StartProcess>b__25_0()
extern void SensorBugTest_U3CStartProcessU3Eb__25_0_m85E0CAA6F2459A40E04B1F01E37045FDF9153532 (void);
// 0x000003B1 System.Void SensorBugTest::<StartProcess>b__25_1(System.String)
extern void SensorBugTest_U3CStartProcessU3Eb__25_1_mFA6596ED773F248128D73D331506D68499816011 (void);
// 0x000003B2 System.Void SensorBugTest::<Update>b__27_0(System.String,System.String)
extern void SensorBugTest_U3CUpdateU3Eb__27_0_mA8469B4049030722819FF53D8BCF81D7F63A3DA7 (void);
// 0x000003B3 System.Void SensorBugTest::<Update>b__27_1(System.String,System.String,System.String)
extern void SensorBugTest_U3CUpdateU3Eb__27_1_mDECE6B9EF3E702D27E9FDC72876239098E1F33B2 (void);
// 0x000003B4 System.Void SensorBugTest::<Update>b__27_2(System.String)
extern void SensorBugTest_U3CUpdateU3Eb__27_2_m7AA858938C103785CB5FB16F528AAAC16F300AD1 (void);
// 0x000003B5 System.Void SensorBugTest::<Update>b__27_3(System.String,System.Byte[])
extern void SensorBugTest_U3CUpdateU3Eb__27_3_mF0323734218D7C6F9CD0C7CA7044422EBA99DB21 (void);
// 0x000003B6 System.Void SensorBugTest::<Update>b__27_7(System.String)
extern void SensorBugTest_U3CUpdateU3Eb__27_7_m253B16D5D7242C0C06E077221D46EF864CAF9021 (void);
// 0x000003B7 System.Void SensorBugTest::<Update>b__27_4(System.String)
extern void SensorBugTest_U3CUpdateU3Eb__27_4_m1E70AD0D9A91B4E83542C5C0FA89E21C84014A98 (void);
// 0x000003B8 System.Void SensorBugTest::<Update>b__27_5(System.String,System.String,System.Byte[])
extern void SensorBugTest_U3CUpdateU3Eb__27_5_m87A2C9210740362D86216EABDB4DCA168A075727 (void);
// 0x000003B9 System.Void SensorBugTest/Characteristic::.ctor()
extern void Characteristic__ctor_m266178646E61F88863548B588E2AD3B7A68F73CE (void);
// 0x000003BA System.Void SensorBugTest/<>c::.cctor()
extern void U3CU3Ec__cctor_m18A0A1758406011AB3CB42882123DB5F59B4D21C (void);
// 0x000003BB System.Void SensorBugTest/<>c::.ctor()
extern void U3CU3Ec__ctor_mDAF968BB3AF2BDD5F6A7F651178705CBDD0BE5BD (void);
// 0x000003BC System.Boolean SensorBugTest/<>c::<get_AllCharacteristicsFound>b__12_0(SensorBugTest/Characteristic)
extern void U3CU3Ec_U3Cget_AllCharacteristicsFoundU3Eb__12_0_m2E1ACFC5B850F4A5B50E3489B4058B43499506C6 (void);
// 0x000003BD System.Void SensorBugTest/<>c::<Update>b__27_6(System.String)
extern void U3CU3Ec_U3CUpdateU3Eb__27_6_m7FFB4EF6D637E6F96DB518C9A3F67C4E3E9A0928 (void);
// 0x000003BE System.Void SensorBugTest/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m36BE155F1C26F2675DAAC6CFB14FC264DF958C2A (void);
// 0x000003BF System.Boolean SensorBugTest/<>c__DisplayClass13_0::<GetCharacteristic>b__0(SensorBugTest/Characteristic)
extern void U3CU3Ec__DisplayClass13_0_U3CGetCharacteristicU3Eb__0_m79608C0B32BA8CA93ED708FE350305918FEF39C7 (void);
// 0x000003C0 System.Void SimpleTest::Reset()
extern void SimpleTest_Reset_m80E513ECB92683264167AF85DED868621137E410 (void);
// 0x000003C1 System.Void SimpleTest::SetState(SimpleTest/States,System.Single)
extern void SimpleTest_SetState_mF962EB279E59AEE870D2A5331FE96F7EE7192A7C (void);
// 0x000003C2 System.Void SimpleTest::StartProcess()
extern void SimpleTest_StartProcess_m12581D538084A6CDAE1B9512300021570B8CFDE5 (void);
// 0x000003C3 System.Void SimpleTest::Start()
extern void SimpleTest_Start_mDB6704185764EDC0B2D7562DFA1F7A85E9B359BC (void);
// 0x000003C4 System.Void SimpleTest::Update()
extern void SimpleTest_Update_mFBC1EA9EA621A9B65A227AC4E3C937F6E83DC659 (void);
// 0x000003C5 System.Void SimpleTest::OnLED()
extern void SimpleTest_OnLED_m6EE42B4201AB3758DCE645F8CD0BD15D40E232AF (void);
// 0x000003C6 System.String SimpleTest::FullUUID(System.String)
extern void SimpleTest_FullUUID_m5AA6C63647B01186CF35C5B3C381FF354A1749A9 (void);
// 0x000003C7 System.Boolean SimpleTest::IsEqual(System.String,System.String)
extern void SimpleTest_IsEqual_m613C6F3963C31C1A8EEB00E190975B25DE356B6F (void);
// 0x000003C8 System.Void SimpleTest::SendByte(System.Byte)
extern void SimpleTest_SendByte_m9929428B66E5530CF6D574C060AC2F26630E8B84 (void);
// 0x000003C9 System.Void SimpleTest::OnGUI()
extern void SimpleTest_OnGUI_m412A3AB144275112E89F3BB9FEB970070954464C (void);
// 0x000003CA System.Void SimpleTest::.ctor()
extern void SimpleTest__ctor_m7F3C8BBDD1C82B8681B1175815F1510C006BDF43 (void);
// 0x000003CB System.Void SimpleTest::<StartProcess>b__16_0()
extern void SimpleTest_U3CStartProcessU3Eb__16_0_m54B2036E98E536D90E6169FB03BABA34DC793E17 (void);
// 0x000003CC System.Void SimpleTest::<Update>b__18_0(System.String,System.String)
extern void SimpleTest_U3CUpdateU3Eb__18_0_mB3B3E6D1C3FEE48C0EE43BBFDC0322FB404983F4 (void);
// 0x000003CD System.Void SimpleTest::<Update>b__18_1(System.String,System.String,System.Int32,System.Byte[])
extern void SimpleTest_U3CUpdateU3Eb__18_1_mFFEE5D5435DBF0B9B5B121BC25EDE630118473FF (void);
// 0x000003CE System.Void SimpleTest::<Update>b__18_2(System.String,System.String,System.String)
extern void SimpleTest_U3CUpdateU3Eb__18_2_m55C97653760F39773959B494C311F1252B9AF21F (void);
// 0x000003CF System.Void SimpleTest::<Update>b__18_3(System.String,System.String,System.Byte[])
extern void SimpleTest_U3CUpdateU3Eb__18_3_m2A086E84FB55476DF776730E58A34532948BDB8D (void);
// 0x000003D0 System.Void SimpleTest::<Update>b__18_4(System.String)
extern void SimpleTest_U3CUpdateU3Eb__18_4_m94DE1498A0B6B8898A4C73C216B485EF2402C6C6 (void);
// 0x000003D1 System.Void SimpleTest::<Update>b__18_6()
extern void SimpleTest_U3CUpdateU3Eb__18_6_mA60AF5965D710D30B8DE67C2F345EE0657842691 (void);
// 0x000003D2 System.Void SimpleTest::<Update>b__18_5()
extern void SimpleTest_U3CUpdateU3Eb__18_5_m81398BFEB07BCD92500D1D93DA9394B1A5BA80A4 (void);
// 0x000003D3 System.Void SimpleTest/<>c::.cctor()
extern void U3CU3Ec__cctor_m263DA1BF0CC71ABDB88951746576429D7C97D47D (void);
// 0x000003D4 System.Void SimpleTest/<>c::.ctor()
extern void U3CU3Ec__ctor_m55938074A36BD2D27F9B0B8F99F843DFD9451C34 (void);
// 0x000003D5 System.Void SimpleTest/<>c::<StartProcess>b__16_1(System.String)
extern void U3CU3Ec_U3CStartProcessU3Eb__16_1_mE13AB464770AAAACE72297D4ABA7D8BA5E962511 (void);
// 0x000003D6 System.Void SimpleTest/<>c::<SendByte>b__23_0(System.String)
extern void U3CU3Ec_U3CSendByteU3Eb__23_0_m498F24D6DDBB2FCCC41AB49ABE4891988681F5FD (void);
// 0x000003D7 System.Void StartingExample::set_StatusMessage(System.String)
extern void StartingExample_set_StatusMessage_mCF27E70635BFE9BBD40D8FED1B636582FBDC3000 (void);
// 0x000003D8 System.Void StartingExample::OnDeinitializeButton()
extern void StartingExample_OnDeinitializeButton_m70E689BCDBC46139AED9A0E724EB6398EF7AF793 (void);
// 0x000003D9 System.Void StartingExample::Reset()
extern void StartingExample_Reset_mD3EB0C8AF064F5080C41942BB1262FC13A9F4FD7 (void);
// 0x000003DA System.Void StartingExample::SetState(StartingExample/States,System.Single)
extern void StartingExample_SetState_mF70F17891E022D7EA6273482893945676571BFCC (void);
// 0x000003DB System.Void StartingExample::StartProcess()
extern void StartingExample_StartProcess_m195D4CE68A4462079E1B969D2D177A72400E00C1 (void);
// 0x000003DC System.Void StartingExample::Start()
extern void StartingExample_Start_mE95C824F385E4E8B5E914F7762FA1F99F69DCC36 (void);
// 0x000003DD System.Void StartingExample::ProcessButton(System.Byte[])
extern void StartingExample_ProcessButton_m1A4CAD411D83AFA27A492F4C8E8A9F8FD85CA5BD (void);
// 0x000003DE System.Void StartingExample::Update()
extern void StartingExample_Update_mA4FCCB1DA38170D98E86A0D524430418530D89D1 (void);
// 0x000003DF System.Void StartingExample::OnLED()
extern void StartingExample_OnLED_m991AFF2485892CDBC85E014243489E261EDAE849 (void);
// 0x000003E0 System.String StartingExample::FullUUID(System.String)
extern void StartingExample_FullUUID_m2476FACD1766F07AF4189FDCDC3806D1CDFDF3DE (void);
// 0x000003E1 System.Boolean StartingExample::IsEqual(System.String,System.String)
extern void StartingExample_IsEqual_m694468A3EDCFFAAB91E2DCD364D8EB2952EE0BDF (void);
// 0x000003E2 System.Void StartingExample::SendByte(System.Byte)
extern void StartingExample_SendByte_m975A02F2E57A955DD824C27962BAA91EEFA6BB7B (void);
// 0x000003E3 System.Void StartingExample::.ctor()
extern void StartingExample__ctor_mBF3B2454334B5DA59E6491145E48E8369AFF935C (void);
// 0x000003E4 System.Void StartingExample::<OnDeinitializeButton>b__18_0()
extern void StartingExample_U3COnDeinitializeButtonU3Eb__18_0_m88BF2909375A7E96CC6FEBAF7EF33838C282454F (void);
// 0x000003E5 System.Void StartingExample::<StartProcess>b__21_0()
extern void StartingExample_U3CStartProcessU3Eb__21_0_m8218934FDDCAF92B8F7AA61FE15A46C3767D36CE (void);
// 0x000003E6 System.Void StartingExample::<StartProcess>b__21_1(System.String)
extern void StartingExample_U3CStartProcessU3Eb__21_1_mF15049C1F70CBB04E82C11D6C4D5FCDCFB3F507C (void);
// 0x000003E7 System.Void StartingExample::<Update>b__24_0(System.String,System.String)
extern void StartingExample_U3CUpdateU3Eb__24_0_m5B6A3B28BF17FB6B55C23D8E2E45A16282049752 (void);
// 0x000003E8 System.Void StartingExample::<Update>b__24_1(System.String,System.String,System.Int32,System.Byte[])
extern void StartingExample_U3CUpdateU3Eb__24_1_mF8AC0930A3ECC9BD459023D5E34DAF5142CB3491 (void);
// 0x000003E9 System.Void StartingExample::<Update>b__24_2(System.String,System.String,System.String)
extern void StartingExample_U3CUpdateU3Eb__24_2_mDAB43D31AA5CD1F4BBF3929570744249454EE396 (void);
// 0x000003EA System.Void StartingExample::<Update>b__24_3(System.String,System.String)
extern void StartingExample_U3CUpdateU3Eb__24_3_m9083A004B1BC7DF50E688B3D40023FA19141B00B (void);
// 0x000003EB System.Void StartingExample::<Update>b__24_7(System.String,System.Byte[])
extern void StartingExample_U3CUpdateU3Eb__24_7_mEC427A7282221075DA4A600BA3A2B2CC9A5837F8 (void);
// 0x000003EC System.Void StartingExample::<Update>b__24_4(System.String,System.String,System.Byte[])
extern void StartingExample_U3CUpdateU3Eb__24_4_m3C0E94C3647EF816DAC2639B87B65CD8A875E2BC (void);
// 0x000003ED System.Void StartingExample::<Update>b__24_5(System.String)
extern void StartingExample_U3CUpdateU3Eb__24_5_m3C214ADE95D1C9E90899A06CA0E365F73BEACD1E (void);
// 0x000003EE System.Void StartingExample::<Update>b__24_8()
extern void StartingExample_U3CUpdateU3Eb__24_8_m278798714171FAFC93C746690A0A85618AEEF398 (void);
// 0x000003EF System.Void StartingExample::<Update>b__24_6()
extern void StartingExample_U3CUpdateU3Eb__24_6_m96A0BC0D103E7A896FED234E6AD8C61865CFAA57 (void);
// 0x000003F0 System.Void StartingExample/<>c::.cctor()
extern void U3CU3Ec__cctor_mF0C0530FB59791CE409C167F44210CB68985F606 (void);
// 0x000003F1 System.Void StartingExample/<>c::.ctor()
extern void U3CU3Ec__ctor_mD5A931D8103C0C7E273BE47487ADDB16BA4549D8 (void);
// 0x000003F2 System.Void StartingExample/<>c::<SendByte>b__29_0(System.String)
extern void U3CU3Ec_U3CSendByteU3Eb__29_0_mC3753EBC6652A03B64169F270F0B090FE08F7B3A (void);
// 0x000003F3 System.Void MSACC_CameraType::.ctor()
extern void MSACC_CameraType__ctor_mDB7AFD2E5107B108781A4D05B0FFE0FA878E89D4 (void);
// 0x000003F4 System.Void MSACC_CameraSetting::.ctor()
extern void MSACC_CameraSetting__ctor_m72368248876D21F9B170A4A9640304456FFC8B3F (void);
// 0x000003F5 System.Void MSACC_SettingsCameraFirstPerson::.ctor()
extern void MSACC_SettingsCameraFirstPerson__ctor_mE2DE8258E795985C28D33552563261D1C3B7D705 (void);
// 0x000003F6 System.Void MSACC_SettingsCameraFollow::.ctor()
extern void MSACC_SettingsCameraFollow__ctor_m3DEC64866088069A87DFABDEFC1757D8E3A1D65E (void);
// 0x000003F7 System.Void MSACC_SettingsCameraOrbital::.ctor()
extern void MSACC_SettingsCameraOrbital__ctor_mFCCA1A72E73925036018E8F2A2AE2BCB45E34718 (void);
// 0x000003F8 System.Void MSACC_SettingsCameraOrbitalThatFollows::.ctor()
extern void MSACC_SettingsCameraOrbitalThatFollows__ctor_m40DEBD895CC9E377ACF56F40ACBE4E6727AC8499 (void);
// 0x000003F9 System.Void MSACC_SettingsCameraETS_StyleCamera::.ctor()
extern void MSACC_SettingsCameraETS_StyleCamera__ctor_mDAB2AC9191B18103DB25B350D15016DBF6DC60DA (void);
// 0x000003FA System.Void MSACC_SettingsFlyCamera::.ctor()
extern void MSACC_SettingsFlyCamera__ctor_m57D08ABE98311A69BD68B0C2542360EFC52ECC2D (void);
// 0x000003FB System.Void MSCameraController::OnValidate()
extern void MSCameraController_OnValidate_m6D119F54E733251A60B269E6D484E0DF96CB060B (void);
// 0x000003FC System.Void MSCameraController::Awake()
extern void MSCameraController_Awake_m1BC31F7837B878E3EFDF3849470AC4E346482DD2 (void);
// 0x000003FD System.Void MSCameraController::Start()
extern void MSCameraController_Start_m80BCFE915AF73AFF61096C37DD494D1AB6DA9698 (void);
// 0x000003FE System.Void MSCameraController::EnableCameras(System.Int32)
extern void MSCameraController_EnableCameras_m1F566605667BC223A30145AD5A8A0156D39EF9BB (void);
// 0x000003FF System.Void MSCameraController::ManageCameras()
extern void MSCameraController_ManageCameras_m8B723097FA4F0F2BE1A414CF45DB4B5B7439BF51 (void);
// 0x00000400 System.Single MSCameraController::ClampAngle(System.Single,System.Single,System.Single)
extern void MSCameraController_ClampAngle_m9A1F11FEF832ACAC930DBB2116819B1B24118680 (void);
// 0x00000401 System.Void MSCameraController::MSADCCChangeCameras()
extern void MSCameraController_MSADCCChangeCameras_mB6493F1660815E8F9F2CD705F29B7357D08C5A7E (void);
// 0x00000402 System.Void MSCameraController::Update()
extern void MSCameraController_Update_mAF405C540FFF02C40678807CBA1F702B38427135 (void);
// 0x00000403 System.Void MSCameraController::LateUpdate()
extern void MSCameraController_LateUpdate_m70B84369EDDF916AC62E10E917C69496A9699FE6 (void);
// 0x00000404 System.Void MSCameraController::FixedUpdate()
extern void MSCameraController_FixedUpdate_m9937C518488367D93E1655BDDE26A89B72B66437 (void);
// 0x00000405 System.Void MSCameraController::.ctor()
extern void MSCameraController__ctor_mA407D6DF47C960B2568D4016B616EFC03C203A56 (void);
// 0x00000406 System.Void ExampleScene::OnReceiveReply(System.String)
extern void ExampleScene_OnReceiveReply_m4578DE21D5486FB98800305628605AFED0CC99BA (void);
// 0x00000407 System.Void ExampleScene::UStart()
extern void ExampleScene_UStart_m5BB5CA6042DC9DEA5FD40BFCAB5A3D1D4B3358B1 (void);
// 0x00000408 System.Void ExampleScene::.ctor()
extern void ExampleScene__ctor_mE7C97B051BC893F36C80844A97B831FE9D995446 (void);
// 0x00000409 System.Void ExampleMod::SendMsg(System.String)
extern void ExampleMod_SendMsg_m107DE94914AAEDFFF6FCFC67ED2E111A08D91EDA (void);
// 0x0000040A System.Void ExampleMod::OnListenersAndModRegistration()
extern void ExampleMod_OnListenersAndModRegistration_m2B686875831DF39E665606FE9C9935ABA84FC346 (void);
// 0x0000040B System.Void ExampleMod::OnDestroyListenersAndModUnregistration()
extern void ExampleMod_OnDestroyListenersAndModUnregistration_m4C048561BA2E8830AF2495BE2D52BE8974F990D0 (void);
// 0x0000040C System.Void ExampleMod::.ctor()
extern void ExampleMod__ctor_m1E70AF07EE96009EA10BC8A7E768A57DF23B43E8 (void);
// 0x0000040D System.Void IExampleMod::SendMsg(System.String)
// 0x0000040E System.Void IExampleHandler::OnReceiveReply(System.String)
// 0x0000040F System.Void IProgrammingGridMod::RemoveButton(System.Int32,System.Int32)
// 0x00000410 System.Void IProgrammingGridMod::ClearGrid()
// 0x00000411 System.Void IProgrammingGridMod::CreateGrid()
// 0x00000412 System.Void IProgrammingGridMod::InitializeGrid(UnityEngine.GameObject,UnityEngine.UI.ScrollRect,UIProgrammingPanel[],UnityEngine.UI.Button,UnityEngine.UI.Button)
// 0x00000413 System.Void IProgrammingGridMod::InitializeColumn(UnityEngine.GameObject)
// 0x00000414 System.Void IProgrammingGridMod::CreateColumn()
// 0x00000415 System.Void IProgrammingGridMod::AddButton(RobotAction/MotionAction,System.Int32)
// 0x00000416 System.Void IProgrammingGridMod::AddButton(RobotAction/EmotionAction,System.Int32)
// 0x00000417 System.Void IProgrammingGridMod::AddButton(RobotAction/ArmsAction,System.Int32)
// 0x00000418 System.Void IProgrammingGridMod::AddButton(RobotAction/SoundAction,System.Int32)
// 0x00000419 System.Void IProgrammingGridMod::GetGridActions(RobotAction/MotionAction[]&,RobotAction/EmotionAction[]&,RobotAction/ArmsAction[]&,RobotAction/SoundAction[]&)
// 0x0000041A System.Void IProgrammingGridMod::FillGrid(RobotAction/MotionAction[],RobotAction/EmotionAction[],RobotAction/ArmsAction[],RobotAction/SoundAction[])
// 0x0000041B System.Void IProgrammingGridMod::FillColumn(System.Int32)
// 0x0000041C System.Void IProgrammingGridMod::SetColumnActivation(System.Int32,System.Boolean)
// 0x0000041D System.Void IProgrammingGridMod::SetEditable(System.Boolean)
// 0x0000041E System.Void ProgrammingGridMod::CreateGrid()
extern void ProgrammingGridMod_CreateGrid_m88773DA9A0BC8C201EDA7A1AB08A60F1196E9EB6 (void);
// 0x0000041F System.Void ProgrammingGridMod::InitializeGrid(UnityEngine.GameObject,UnityEngine.UI.ScrollRect,UIProgrammingPanel[],UnityEngine.UI.Button,UnityEngine.UI.Button)
extern void ProgrammingGridMod_InitializeGrid_m7560E9BF05FA9844EB16221D939D8C69AFB1F68F (void);
// 0x00000420 System.Void ProgrammingGridMod::InitializeColumn(UnityEngine.GameObject)
extern void ProgrammingGridMod_InitializeColumn_m50F7D01F09181A0310F4A961B4991EAE0184FA0E (void);
// 0x00000421 System.Void ProgrammingGridMod::_CreateColumn(System.Int32)
extern void ProgrammingGridMod__CreateColumn_m9D2DB1BEF0FD623B8C1555E2B540101E5A668C08 (void);
// 0x00000422 System.Collections.IEnumerator ProgrammingGridMod::_CreateGrid(System.Int32,System.Int32)
extern void ProgrammingGridMod__CreateGrid_m27682F9B29F8A2D188AFBE505198A3A2F0335DE9 (void);
// 0x00000423 System.Void ProgrammingGridMod::RemoveButton(System.Int32,System.Int32)
extern void ProgrammingGridMod_RemoveButton_m0E7D74568730F2D419B1BBD2363A8E7168FCE9B9 (void);
// 0x00000424 System.Void ProgrammingGridMod::RemoveButtonFromColumn(System.Int32)
extern void ProgrammingGridMod_RemoveButtonFromColumn_m0A962BB22700568F68876E4D527511C738174EB8 (void);
// 0x00000425 System.Void ProgrammingGridMod::ClearGrid()
extern void ProgrammingGridMod_ClearGrid_mB567CCEF9EE7FB4AD210800F2FD545F5686B2517 (void);
// 0x00000426 System.Void ProgrammingGridMod::CreateColumn()
extern void ProgrammingGridMod_CreateColumn_mC3F9DC420F6E9D285D5B9CB13DB0B6C42A46A2EE (void);
// 0x00000427 System.Void ProgrammingGridMod::FillColumn(System.Int32)
extern void ProgrammingGridMod_FillColumn_mC51AF4A9CFE143C65EDA5B6CBB47064198572B16 (void);
// 0x00000428 System.Void ProgrammingGridMod::AddButton(RobotAction/MotionAction,System.Int32)
extern void ProgrammingGridMod_AddButton_mFF71C452DFD7B5E1F8E1385EC9B0E8D31260F209 (void);
// 0x00000429 System.Void ProgrammingGridMod::AddButton(RobotAction/EmotionAction,System.Int32)
extern void ProgrammingGridMod_AddButton_m3F1076150F83152271941DD898030F32F7C150C7 (void);
// 0x0000042A System.Void ProgrammingGridMod::AddButton(RobotAction/ArmsAction,System.Int32)
extern void ProgrammingGridMod_AddButton_mDD5881647ECC5BA5F6027D5289A0FFE612F8B0EC (void);
// 0x0000042B System.Void ProgrammingGridMod::AddButton(RobotAction/SoundAction,System.Int32)
extern void ProgrammingGridMod_AddButton_mD9E3EA365D8E661B202C163EE27900BE519B7CCF (void);
// 0x0000042C System.Void ProgrammingGridMod::GetGridActions(RobotAction/MotionAction[]&,RobotAction/EmotionAction[]&,RobotAction/ArmsAction[]&,RobotAction/SoundAction[]&)
extern void ProgrammingGridMod_GetGridActions_mA7C319E0D3B50589FEA66EF1B52E2E56810AE690 (void);
// 0x0000042D System.Void ProgrammingGridMod::FillGrid(RobotAction/MotionAction[],RobotAction/EmotionAction[],RobotAction/ArmsAction[],RobotAction/SoundAction[])
extern void ProgrammingGridMod_FillGrid_mD8B4CE320469ADF055AE0D4592F96AFB7A999043 (void);
// 0x0000042E System.Void ProgrammingGridMod::SetColumnActivation(System.Int32,System.Boolean)
extern void ProgrammingGridMod_SetColumnActivation_m87A921762A20E41D2516AD7D3CD550B602F03012 (void);
// 0x0000042F System.Void ProgrammingGridMod::SetEditable(System.Boolean)
extern void ProgrammingGridMod_SetEditable_m6DFA1B6C60318CAA6152D16170800E2EB306F3C9 (void);
// 0x00000430 System.Void ProgrammingGridMod::UAwake()
extern void ProgrammingGridMod_UAwake_m16AC4F43D45B525F1E7E6FF81A0EEA2342C52D85 (void);
// 0x00000431 System.Void ProgrammingGridMod::PlusButtonPressed(System.Int32,System.Int32)
extern void ProgrammingGridMod_PlusButtonPressed_mFB3B5A78BCEDFBA5F5647FC33094F14C04784DA4 (void);
// 0x00000432 System.Void ProgrammingGridMod::OnPanelPressed(System.Int32,UIProgrammingPanelButton)
extern void ProgrammingGridMod_OnPanelPressed_mEEB89F0475176E4E214ADAD52C370608AB686193 (void);
// 0x00000433 System.Void ProgrammingGridMod::ExpandRect(UnityEngine.RectTransform)
extern void ProgrammingGridMod_ExpandRect_m01B9BA3626C68B51810B52BF3652239788AB4760 (void);
// 0x00000434 System.Void ProgrammingGridMod::_AddButton(System.Int32,System.Int32,UnityEngine.GameObject)
extern void ProgrammingGridMod__AddButton_m8EB0E914B71BCF7565641BB2385972357BD2F7FC (void);
// 0x00000435 System.Void ProgrammingGridMod::_AddButtonToColumn(System.Int32,UnityEngine.GameObject)
extern void ProgrammingGridMod__AddButtonToColumn_mB551F7FF176D70F831579A4FF63D34AF7E474396 (void);
// 0x00000436 System.Void ProgrammingGridMod::OnListenersAndModRegistration()
extern void ProgrammingGridMod_OnListenersAndModRegistration_m4784F5F84BDDEF2F224FD6A2936D772D353B4F64 (void);
// 0x00000437 System.Void ProgrammingGridMod::OnDestroyListenersAndModUnregistration()
extern void ProgrammingGridMod_OnDestroyListenersAndModUnregistration_m77000B3A9CC1727962A0EE42B3C2527B8A4FB3ED (void);
// 0x00000438 System.Void ProgrammingGridMod::_ScrollGrill(System.Boolean)
extern void ProgrammingGridMod__ScrollGrill_m063FD1BB9503B954A07B16CE5CF0FEBAD79A48CD (void);
// 0x00000439 System.Collections.IEnumerator ProgrammingGridMod::ScrollAnimation(System.Single,System.Single,System.Single)
extern void ProgrammingGridMod_ScrollAnimation_m137D845FA9663D3D5DC9625AFE491365F644168B (void);
// 0x0000043A System.Void ProgrammingGridMod::.ctor()
extern void ProgrammingGridMod__ctor_m6A740CE39C3AD6CEBABFFFF8AB338D8C349103EE (void);
// 0x0000043B System.Void ProgrammingGridMod::<InitializeGrid>b__38_0()
extern void ProgrammingGridMod_U3CInitializeGridU3Eb__38_0_mCE10DFF0D0ED426BF7A70E77DE342A4009FD26C6 (void);
// 0x0000043C System.Void ProgrammingGridMod::<InitializeGrid>b__38_1()
extern void ProgrammingGridMod_U3CInitializeGridU3Eb__38_1_m71E62CD1EBA405CBCBC0E782DE2F41DFBC92998A (void);
// 0x0000043D System.Void ProgrammingGridMod/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m0F36DF38C22D086D862027174908A784F9C38ADF (void);
// 0x0000043E System.Void ProgrammingGridMod/<>c__DisplayClass41_0::<_CreateGrid>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3C_CreateGridU3Eb__0_mA5F8DE8D01036334EDD8D2F2423CE0983E70BD2C (void);
// 0x0000043F System.Void ProgrammingGridMod/<_CreateGrid>d__41::.ctor(System.Int32)
extern void U3C_CreateGridU3Ed__41__ctor_mF5C74F212896F8994B8EA3A41EAC0859A103CF9E (void);
// 0x00000440 System.Void ProgrammingGridMod/<_CreateGrid>d__41::System.IDisposable.Dispose()
extern void U3C_CreateGridU3Ed__41_System_IDisposable_Dispose_m73D732B957AE810C6492643D8195D29CC803761D (void);
// 0x00000441 System.Boolean ProgrammingGridMod/<_CreateGrid>d__41::MoveNext()
extern void U3C_CreateGridU3Ed__41_MoveNext_m7E92DE61D2613D9B4FE2F7E99020FE53EF01EE57 (void);
// 0x00000442 System.Object ProgrammingGridMod/<_CreateGrid>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_CreateGridU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1175994B3A50A861572978E0A4C40AE5FFCD33F7 (void);
// 0x00000443 System.Void ProgrammingGridMod/<_CreateGrid>d__41::System.Collections.IEnumerator.Reset()
extern void U3C_CreateGridU3Ed__41_System_Collections_IEnumerator_Reset_m513E1AB08612B590760B76A1B4600414E5F4838D (void);
// 0x00000444 System.Object ProgrammingGridMod/<_CreateGrid>d__41::System.Collections.IEnumerator.get_Current()
extern void U3C_CreateGridU3Ed__41_System_Collections_IEnumerator_get_Current_m5D059BA4129F0E1B23A3EB37ADB052EBFD22790F (void);
// 0x00000445 System.Void ProgrammingGridMod/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mFD81F5A6965DAF4447CB147CF50523B5817E9EA5 (void);
// 0x00000446 System.Void ProgrammingGridMod/<>c__DisplayClass59_0::<_AddButton>b__0()
extern void U3CU3Ec__DisplayClass59_0_U3C_AddButtonU3Eb__0_m2701327FEC66751EC8D4F544C689295279541536 (void);
// 0x00000447 System.Void ProgrammingGridMod/<ScrollAnimation>d__64::.ctor(System.Int32)
extern void U3CScrollAnimationU3Ed__64__ctor_mA0B824A208DD7EAA21D8DB2E42912DC2D45C4139 (void);
// 0x00000448 System.Void ProgrammingGridMod/<ScrollAnimation>d__64::System.IDisposable.Dispose()
extern void U3CScrollAnimationU3Ed__64_System_IDisposable_Dispose_m88468C273FE8161A2DB428286B62D73A6968D5E7 (void);
// 0x00000449 System.Boolean ProgrammingGridMod/<ScrollAnimation>d__64::MoveNext()
extern void U3CScrollAnimationU3Ed__64_MoveNext_m7643FF774B04826B287525C8FE6A1A246DADDD1B (void);
// 0x0000044A System.Object ProgrammingGridMod/<ScrollAnimation>d__64::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScrollAnimationU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC115A161A44F5FF6E3B5CF394569DE31C5E3260 (void);
// 0x0000044B System.Void ProgrammingGridMod/<ScrollAnimation>d__64::System.Collections.IEnumerator.Reset()
extern void U3CScrollAnimationU3Ed__64_System_Collections_IEnumerator_Reset_m91E3BE8EEAC930248C2FD1FA3C64F8A01A2B5D13 (void);
// 0x0000044C System.Object ProgrammingGridMod/<ScrollAnimation>d__64::System.Collections.IEnumerator.get_Current()
extern void U3CScrollAnimationU3Ed__64_System_Collections_IEnumerator_get_Current_mD308AE5AFC5394AB0F090821AB87B281E949418F (void);
// 0x0000044D System.Void CameraMediaMakerMod::UStart()
extern void CameraMediaMakerMod_UStart_m70486CC84C0A9B368F07CC24EB61B5FB3B654FF1 (void);
// 0x0000044E System.Void CameraMediaMakerMod::Configuration()
extern void CameraMediaMakerMod_Configuration_m3E60B07D5534139012E74BE6D9AAFFF44BF9BF94 (void);
// 0x0000044F System.Void CameraMediaMakerMod::UUpdate()
extern void CameraMediaMakerMod_UUpdate_mDDE9A03AE61E85764587CA791B0A4D527F2B67A1 (void);
// 0x00000450 System.Void CameraMediaMakerMod::ShowCameraOnTexture(UnityEngine.UI.RawImage,UnityEngine.UI.AspectRatioFitter)
extern void CameraMediaMakerMod_ShowCameraOnTexture_m0B0C4A208C7AB11E1989316BD09BAA08802AA0F8 (void);
// 0x00000451 System.Void CameraMediaMakerMod::ShowCameraOnTexture()
extern void CameraMediaMakerMod_ShowCameraOnTexture_m466F9275B5341D4AFA29B00CC8A80A70320064F8 (void);
// 0x00000452 System.Void CameraMediaMakerMod::StopShowingCameraOnTexture()
extern void CameraMediaMakerMod_StopShowingCameraOnTexture_mCEAAF658EBD5C9B849011B66D502132253452991 (void);
// 0x00000453 UnityEngine.Texture2D CameraMediaMakerMod::TextureToTexture2D(UnityEngine.Texture)
extern void CameraMediaMakerMod_TextureToTexture2D_m2E38432F233F09C8068E5AED0F9F4BA4D7B22833 (void);
// 0x00000454 System.Void CameraMediaMakerMod::TakePhotoAsync(System.String,System.String)
extern void CameraMediaMakerMod_TakePhotoAsync_m5C8919A30E4AF7028D9C47C1DD5E1209D56B53D0 (void);
// 0x00000455 System.Void CameraMediaMakerMod::StartRecordingVideo()
extern void CameraMediaMakerMod_StartRecordingVideo_mBAE746EB310EA59AA62DF923FAF798FBE2E982F1 (void);
// 0x00000456 System.Void CameraMediaMakerMod::OnRecorded(System.String)
extern void CameraMediaMakerMod_OnRecorded_m4539CF1D83CF7CE2BA5C1C8E02DC55C2467A1814 (void);
// 0x00000457 System.Void CameraMediaMakerMod::StopAndSaveRecordedVideoAsync(System.String,System.String)
extern void CameraMediaMakerMod_StopAndSaveRecordedVideoAsync_mC4E4641E25C04D7CEBF08E2C3C7A0353C6CC5B33 (void);
// 0x00000458 System.Void CameraMediaMakerMod::OnListenersAndModRegistration()
extern void CameraMediaMakerMod_OnListenersAndModRegistration_m88537F3E5F4552502E1F71B15B40E48338F10BB4 (void);
// 0x00000459 System.Void CameraMediaMakerMod::OnDestroyListenersAndModUnregistration()
extern void CameraMediaMakerMod_OnDestroyListenersAndModUnregistration_m8190DFE324F9C43978E9C19FC5D51C92D2440F2A (void);
// 0x0000045A System.Void CameraMediaMakerMod::OnMediaTaken(System.String)
extern void CameraMediaMakerMod_OnMediaTaken_m90BE49DA51A8429064ED8A5F8D7E35D1C9655DF8 (void);
// 0x0000045B System.Void CameraMediaMakerMod::.ctor()
extern void CameraMediaMakerMod__ctor_m9D77BFD038A78A250E49840D20B6D399C061302A (void);
// 0x0000045C System.Void VideoCameraMediaMakerScene::UStart()
extern void VideoCameraMediaMakerScene_UStart_m236A7BAE47C30BC576A5E71B33F23FA22FE06376 (void);
// 0x0000045D System.Void VideoCameraMediaMakerScene::_TakePhoto()
extern void VideoCameraMediaMakerScene__TakePhoto_mB3643B1A011D00D06000CA677678AE7C901CBEEB (void);
// 0x0000045E System.Void VideoCameraMediaMakerScene::_TakeVideo()
extern void VideoCameraMediaMakerScene__TakeVideo_mCBF21311606EEC791BC6FAD78B2D3F9BEDC8678D (void);
// 0x0000045F System.Void VideoCameraMediaMakerScene::_SaveVideo()
extern void VideoCameraMediaMakerScene__SaveVideo_m342D6D2AE1967B022A7B8D23093E36D5E8F671BC (void);
// 0x00000460 System.Void VideoCameraMediaMakerScene::OnPhotoSaved(System.Boolean)
extern void VideoCameraMediaMakerScene_OnPhotoSaved_mC0509903D33525E3D085053C8FAA63E68F88B225 (void);
// 0x00000461 System.Void VideoCameraMediaMakerScene::OnVideoSaved(System.Boolean)
extern void VideoCameraMediaMakerScene_OnVideoSaved_mE48D4CE0514EE71C0DB56BFD8758ABF81E208776 (void);
// 0x00000462 System.Void VideoCameraMediaMakerScene::.ctor()
extern void VideoCameraMediaMakerScene__ctor_m83A908B45FFEEB148510C2698832FB8D15A90AFE (void);
// 0x00000463 System.Void CellStruct::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void CellStruct__ctor_mFE61576AF832638D4D8080AD8D82E5B76DC823B0 (void);
// 0x00000464 System.Void StorageTemplate::.ctor()
extern void StorageTemplate__ctor_mA9B4DE796F6FF19CA475D0F9A36088E0C8359C78 (void);
// 0x00000465 System.Void TestingScene::UStart()
extern void TestingScene_UStart_m6C5E3BB3E24D7C9C3002A32F9158A74179847717 (void);
// 0x00000466 System.Void TestingScene::OnStorageCompleted(Umods.ExternalMods.Storage.Mods.StorageMod.Common.StorageVariables/StoragingState)
extern void TestingScene_OnStorageCompleted_mDBE921C38EBB6981F1B358CECB8681901AD903DE (void);
// 0x00000467 System.Void TestingScene::OnLoadCompleted(System.Object)
extern void TestingScene_OnLoadCompleted_m4E45948116977C7AD92E9E284A1FE268672FA34D (void);
// 0x00000468 System.Void TestingScene::.ctor()
extern void TestingScene__ctor_mEA9FFBACF83304F30AB2F7156D8734A41FD139B4 (void);
// 0x00000469 System.Void ScopeScreenManager::.ctor()
extern void ScopeScreenManager__ctor_m82D1678C469F0C4F30824A75C633C855B311E4C1 (void);
// 0x0000046A System.Void ScreenManagerModTestScene::Start()
extern void ScreenManagerModTestScene_Start_mEAE27A8D32FE7CF72EDB98EE15F20935777F5CDD (void);
// 0x0000046B System.Void ScreenManagerModTestScene::Update()
extern void ScreenManagerModTestScene_Update_mD23532C3BB3D2DAAAAA4EABE0021CFD88F794D1B (void);
// 0x0000046C System.Void ScreenManagerModTestScene::.ctor()
extern void ScreenManagerModTestScene__ctor_m3F8D1E408EA8FCC84A3F7997B378902E62DB764F (void);
// 0x0000046D System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3::get_Item(T1)
// 0x0000046E System.Boolean MultiKeyDictionary`3::ContainsKey(T1,T2)
// 0x0000046F System.Void MultiKeyDictionary`3::.ctor()
// 0x00000470 System.Void Images::.ctor()
extern void Images__ctor_m6D3C3358F518CB4C2D7868641CD0F85BA58BCAF2 (void);
// 0x00000471 System.Single Reporter::get_TotalMemUsage()
extern void Reporter_get_TotalMemUsage_m10C1968BA6C2F34F7FFE70B152DA436016B2C9E9 (void);
// 0x00000472 System.Void Reporter::Awake()
extern void Reporter_Awake_mA05F12CC643D84A43D7BE3C98E4A4BCAB2CAFA84 (void);
// 0x00000473 System.Void Reporter::OnDestroy()
extern void Reporter_OnDestroy_mE98F92395C2BE83BF582ADB802E170E1A826BA1C (void);
// 0x00000474 System.Void Reporter::OnEnable()
extern void Reporter_OnEnable_mE1F5751BD00633D30038260E71CC6C5C27929194 (void);
// 0x00000475 System.Void Reporter::OnDisable()
extern void Reporter_OnDisable_mB6B693AA18CA1AB35300F1703FCDD8BFDCD93C05 (void);
// 0x00000476 System.Void Reporter::addSample()
extern void Reporter_addSample_mAD4EFAF6C2D5568AD4A2012EB64D1053C12E213B (void);
// 0x00000477 System.Void Reporter::Initialize()
extern void Reporter_Initialize_mAA682DFB4BC0F75FD5A155177C0CE29AA911FDDA (void);
// 0x00000478 System.Void Reporter::initializeStyle()
extern void Reporter_initializeStyle_mFA953E046D71C4283D69BCA59FFE004A43375594 (void);
// 0x00000479 System.Void Reporter::Start()
extern void Reporter_Start_m446802F56F56E3160BD26A917B18D06FB3EB775F (void);
// 0x0000047A System.Void Reporter::clear()
extern void Reporter_clear_m7D610B4FDEE7966EDB5949114B95081B0FF2AF3E (void);
// 0x0000047B System.Void Reporter::calculateCurrentLog()
extern void Reporter_calculateCurrentLog_mFB0B224FD7E17B906D6D67EF1CB6A9CEC729CA13 (void);
// 0x0000047C System.Void Reporter::DrawInfo()
extern void Reporter_DrawInfo_m86A5EB0BF0B5F7F46432490808B6F123AB01FC6F (void);
// 0x0000047D System.Void Reporter::drawInfo_enableDisableToolBarButtons()
extern void Reporter_drawInfo_enableDisableToolBarButtons_m52E96CBE8C62CF30F324857974B5ECF60F7087BD (void);
// 0x0000047E System.Void Reporter::DrawReport()
extern void Reporter_DrawReport_mD476D5C0ED66B1AB1795C38505BFC474B0C6B738 (void);
// 0x0000047F System.Void Reporter::drawToolBar()
extern void Reporter_drawToolBar_m9AE16E4DD9B3987C5344D7FB7B7B237E618216FA (void);
// 0x00000480 System.Void Reporter::DrawLogs()
extern void Reporter_DrawLogs_m543EA7CD2688AD9F23D2938F9D2A0902239B8E05 (void);
// 0x00000481 System.Void Reporter::drawGraph()
extern void Reporter_drawGraph_m0FB0706C34D21D3EACED1DBFC3C885D762E04CAB (void);
// 0x00000482 System.Void Reporter::drawStack()
extern void Reporter_drawStack_mDE34775477887CCEE8AB1EDBB8936B443B1445A8 (void);
// 0x00000483 System.Void Reporter::OnGUIDraw()
extern void Reporter_OnGUIDraw_m989E3903ABD3D4172D03E6B2F70C882A519E2207 (void);
// 0x00000484 System.Boolean Reporter::isGestureDone()
extern void Reporter_isGestureDone_m3CC6FADB0F178A730DA4D461FFAA307688FD9D56 (void);
// 0x00000485 System.Boolean Reporter::isDoubleClickDone()
extern void Reporter_isDoubleClickDone_mBD57415E98A74C5D88ED962973EE0562A9AB6098 (void);
// 0x00000486 UnityEngine.Vector2 Reporter::getDownPos()
extern void Reporter_getDownPos_m386F88542FDA509FA8D52F99F14BBC6F9040F5E7 (void);
// 0x00000487 UnityEngine.Vector2 Reporter::getDrag()
extern void Reporter_getDrag_mDB7C7B3DDAB95B33DCE693ABB90CE35706475E00 (void);
// 0x00000488 System.Void Reporter::calculateStartIndex()
extern void Reporter_calculateStartIndex_mF006CA3233D72E188852B5017D2DA609ED526D61 (void);
// 0x00000489 System.Void Reporter::doShow()
extern void Reporter_doShow_m48F015370C942A304F060D53AC6B631789B5C49C (void);
// 0x0000048A System.Void Reporter::Update()
extern void Reporter_Update_m10DDBD3CE80D61FED5928E7F4A72D0FBA0688DE9 (void);
// 0x0000048B System.Void Reporter::CaptureLog(System.String,System.String,UnityEngine.LogType)
extern void Reporter_CaptureLog_m2B074EDFEB14E8DFFE601BE8408C79DF1D2A0828 (void);
// 0x0000048C System.Void Reporter::AddLog(System.String,System.String,UnityEngine.LogType)
extern void Reporter_AddLog_m1DC979DC17934CF2AB2C463F4AC325B3C8DAEEDE (void);
// 0x0000048D System.Void Reporter::CaptureLogThread(System.String,System.String,UnityEngine.LogType)
extern void Reporter_CaptureLogThread_m10D6873709D9988908720E92DCECFEE9CEF7B063 (void);
// 0x0000048E System.Void Reporter::_OnLevelWasLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void Reporter__OnLevelWasLoaded_m6299EA2B588C6F405E264FADAA0A619C9B4F072E (void);
// 0x0000048F System.Void Reporter::OnApplicationQuit()
extern void Reporter_OnApplicationQuit_mBC8D59AAABE9EDEA29DCC34744D8F69B4DC3BDC9 (void);
// 0x00000490 System.Collections.IEnumerator Reporter::readInfo()
extern void Reporter_readInfo_m5CEA679FCEF5D1855FAA34D0AB171AE781F52DB0 (void);
// 0x00000491 System.Void Reporter::SaveLogsToDevice()
extern void Reporter_SaveLogsToDevice_mE722E0FDDF5444E1FE4EE9C26F6AD87272D51A58 (void);
// 0x00000492 System.Void Reporter::.ctor()
extern void Reporter__ctor_mA165B1C3F3534A08DFE14C785290E70564217326 (void);
// 0x00000493 System.Single Reporter/Sample::MemSize()
extern void Sample_MemSize_m58B3E52C74A47040F2BEDE76EEEBA0313591A6F8 (void);
// 0x00000494 System.String Reporter/Sample::GetSceneName()
extern void Sample_GetSceneName_m39205B31355C11E75F737F341C3E00B95F418BCF (void);
// 0x00000495 System.Void Reporter/Sample::.ctor()
extern void Sample__ctor_m91CD0ECF2B9B33DD9391FD3A89AD51805C40210C (void);
// 0x00000496 Reporter/Log Reporter/Log::CreateCopy()
extern void Log_CreateCopy_m6F7B2A38CEF5B721E70636B252CECD58153B7960 (void);
// 0x00000497 System.Single Reporter/Log::GetMemoryUsage()
extern void Log_GetMemoryUsage_mBEF407ED7EC6FEAF56316D4139096FE8E10851B0 (void);
// 0x00000498 System.Void Reporter/Log::.ctor()
extern void Log__ctor_m20C60492A00E977718447AF5C9A51F95BF9F84E2 (void);
// 0x00000499 System.Void Reporter/<readInfo>d__188::.ctor(System.Int32)
extern void U3CreadInfoU3Ed__188__ctor_m254978808992314EA07A7CC5E70AF72E6C7A9AAB (void);
// 0x0000049A System.Void Reporter/<readInfo>d__188::System.IDisposable.Dispose()
extern void U3CreadInfoU3Ed__188_System_IDisposable_Dispose_m5249C6D7C6927C5700525A86D05890776541F96D (void);
// 0x0000049B System.Boolean Reporter/<readInfo>d__188::MoveNext()
extern void U3CreadInfoU3Ed__188_MoveNext_m11006FBF4C7E561FD9EE8D51D0952211FD856351 (void);
// 0x0000049C System.Object Reporter/<readInfo>d__188::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CreadInfoU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFFD2E9F27F9240543634E6731841BDD5EE9D8881 (void);
// 0x0000049D System.Void Reporter/<readInfo>d__188::System.Collections.IEnumerator.Reset()
extern void U3CreadInfoU3Ed__188_System_Collections_IEnumerator_Reset_mEB09690D6F41312D7EED5B9FE4BF1320E88099D6 (void);
// 0x0000049E System.Object Reporter/<readInfo>d__188::System.Collections.IEnumerator.get_Current()
extern void U3CreadInfoU3Ed__188_System_Collections_IEnumerator_get_Current_m93150D71A6E5064FC2A2F530E798404D25ED2F8B (void);
// 0x0000049F System.Void ReporterGUI::Awake()
extern void ReporterGUI_Awake_m3A87B0F7A2669A4A492555A0DEC5FFB9815C96EE (void);
// 0x000004A0 System.Void ReporterGUI::OnGUI()
extern void ReporterGUI_OnGUI_m0259C45E54B81A6EF065DF865B40DEFD58995475 (void);
// 0x000004A1 System.Void ReporterGUI::.ctor()
extern void ReporterGUI__ctor_m86C037AF5B2F644D5ADE38485AFBECB75A05F220 (void);
// 0x000004A2 System.Void ReporterMessageReceiver::Start()
extern void ReporterMessageReceiver_Start_m9A80C850878106B725E2B7B331AAF9A48923B772 (void);
// 0x000004A3 System.Void ReporterMessageReceiver::OnPreStart()
extern void ReporterMessageReceiver_OnPreStart_m5CF7C808C27165116648227A6BB3135F5B36585C (void);
// 0x000004A4 System.Void ReporterMessageReceiver::OnHideReporter()
extern void ReporterMessageReceiver_OnHideReporter_m73097CEC01550AB1EECB974FF2A3E09B5F8EE0F4 (void);
// 0x000004A5 System.Void ReporterMessageReceiver::OnShowReporter()
extern void ReporterMessageReceiver_OnShowReporter_mD63CC8AF909A9ADC2EF833AF08C9F73CE54F53D1 (void);
// 0x000004A6 System.Void ReporterMessageReceiver::OnLog(Reporter/Log)
extern void ReporterMessageReceiver_OnLog_m9A92C7CEB82867BDC79311A912A93D02D7617ADB (void);
// 0x000004A7 System.Void ReporterMessageReceiver::.ctor()
extern void ReporterMessageReceiver__ctor_m7923B38EE7568AA0D472AACB2EA61B52D0EF0F66 (void);
// 0x000004A8 System.Void Rotate::Start()
extern void Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB (void);
// 0x000004A9 System.Void Rotate::Update()
extern void Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7 (void);
// 0x000004AA System.Void Rotate::.ctor()
extern void Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4 (void);
// 0x000004AB System.Void TestReporter::Start()
extern void TestReporter_Start_m21A1734784671210AAF7510311A801A87D7938FE (void);
// 0x000004AC System.Void TestReporter::OnDestroy()
extern void TestReporter_OnDestroy_m51622CA5CD9F3AD7DCE7CE9F5332101863128096 (void);
// 0x000004AD System.Void TestReporter::threadLogTest()
extern void TestReporter_threadLogTest_m770ED19CDCF1906DA5B44F3E6D0E1C54644BDBE3 (void);
// 0x000004AE System.Void TestReporter::Update()
extern void TestReporter_Update_mFAB961D73F7C686FC07F73D3C15953DD49AF0BD5 (void);
// 0x000004AF System.Void TestReporter::OnGUI()
extern void TestReporter_OnGUI_mFEA69290A684C9CC25CCB3E5D1BAAD93C500889E (void);
// 0x000004B0 System.Void TestReporter::.ctor()
extern void TestReporter__ctor_mA5EDF9AD6D70A294B65B0AB838E6DB753F7C0710 (void);
// 0x000004B1 System.Void UnityEngine.ScreenScope::UStart()
extern void ScreenScope_UStart_m93B383192637F79E49D70EA73CFCF7421778AD13 (void);
// 0x000004B2 Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen UnityEngine.ScreenScope::GetRootScreen()
extern void ScreenScope_GetRootScreen_m1F1A9C9E1070DB19BED7307BF6ABA0BE25A89E7E (void);
// 0x000004B3 System.Void UnityEngine.ScreenScope::_LoadScreen(Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen)
extern void ScreenScope__LoadScreen_m41BAAC33DD5186920964C38EE10C8AC006AD637E (void);
// 0x000004B4 System.Void UnityEngine.ScreenScope::.ctor()
extern void ScreenScope__ctor_mC04E7F7C883B423F1BF453BAD080137BCC81DAB3 (void);
// 0x000004B5 System.Void DefaultNamespace.IGyroscopeControllerHandler::OnNewGyroscopeStatus(UnityEngine.Quaternion)
// 0x000004B6 System.Void DefaultNamespace.IPhotoCameraMediaMakerHandler::OnPhotoSaved(System.Boolean)
// 0x000004B7 System.Void DefaultNamespace.IVideoCameraMediaMakerHandler::OnVideoSaved(System.Boolean)
// 0x000004B8 Umods.Scripts.Interfaces.IUmodsEventListener Umods.IUmodsEventListenerManager::RegisterListener()
// 0x000004B9 Umods.Scripts.Interfaces.IUmodsEventListener Umods.IUmodsEventListenerManager::GetListener()
// 0x000004BA System.Boolean Umods.IUmodsEventListenerManager::UnregisterListener()
// 0x000004BB System.Void Umods.IUmodsEventListenerManager::ClearListeners()
// 0x000004BC System.Void Umods.Scripts.InternalMods.SceneManager.Handlers.IUmodsSceneLoadingProgressHandler::OnUmodsLoadingSceneProgress(System.Single)
// 0x000004BD System.Single Umods.Scripts.InternalMods.SceneManager.Core.IUmodsSceneManager::get_LoadingProgress()
// 0x000004BE System.Void Umods.Scripts.InternalMods.SceneManager.Core.IUmodsSceneManager::LoadScene(System.String,System.Boolean)
// 0x000004BF System.Void Umods.Scripts.InternalMods.SceneManager.Core.IUmodsSceneManager::LoadPreviousScene(System.Boolean)
// 0x000004C0 System.Void Umods.Scripts.InternalMods.SceneManager.Core.IUmodsSceneManager::HideLoadingScreen()
// 0x000004C1 System.Void Umods.Scripts.InternalMods.SceneManager.Core.IUmodsSceneManager::LoadRootScene(System.Boolean)
// 0x000004C2 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneBehaviour::OnInitializeSceneBehaviour()
extern void UmodsSceneBehaviour_OnInitializeSceneBehaviour_mF18EDF847827049BDE5C1866DED7D984A77CD24B (void);
// 0x000004C3 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneBehaviour::HideLoadingScreen()
extern void UmodsSceneBehaviour_HideLoadingScreen_m84CAE20931558BCA52ED0D1E57C39065A5A6812F (void);
// 0x000004C4 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneBehaviour::LoadScene(System.String,System.Boolean)
extern void UmodsSceneBehaviour_LoadScene_mA25A4DD4C9BA16B6B4E49B529401D2D20C80D2FC (void);
// 0x000004C5 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneBehaviour::LoadPreviousScene(System.Boolean)
extern void UmodsSceneBehaviour_LoadPreviousScene_m39069DDF01EB848D506A2B4806A754B130F3EF65 (void);
// 0x000004C6 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneBehaviour::LoadRootScene(System.Boolean)
extern void UmodsSceneBehaviour_LoadRootScene_mC32A0553C322B31CBAB8D45CF7C12C7CA23357AC (void);
// 0x000004C7 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneBehaviour::.ctor()
extern void UmodsSceneBehaviour__ctor_mDCD0ED2A4E6F737B776770EE1AE04017B8B43C4D (void);
// 0x000004C8 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::LoadScene(System.String,System.Boolean)
extern void UmodsSceneManager_LoadScene_m36B3818CCF2345DCE381AF986817E19A80603AF4 (void);
// 0x000004C9 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::LoadPreviousScene(System.Boolean)
extern void UmodsSceneManager_LoadPreviousScene_mD731528C4F37F99B459808740FE61A687EFF934D (void);
// 0x000004CA System.Single Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::get_LoadingProgress()
extern void UmodsSceneManager_get_LoadingProgress_mB29A8FFE652E2F58DC0C23FA8A29EDEF6441A65D (void);
// 0x000004CB System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::set_LoadingProgress(System.Single)
extern void UmodsSceneManager_set_LoadingProgress_m82FBFB92B81FAF41AF37B570CA84A5425EFD9290 (void);
// 0x000004CC System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::LoadRootScene(System.Boolean)
extern void UmodsSceneManager_LoadRootScene_m6E90EB81C0B1B5DD2BDD7F048DF2B2F812C6C1EC (void);
// 0x000004CD System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::HideLoadingScreen()
extern void UmodsSceneManager_HideLoadingScreen_mD3A1081588369D78A9375B5F351CE75133877EEB (void);
// 0x000004CE System.Collections.IEnumerator Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::_LoadSceneWithLoadingScene(System.String)
extern void UmodsSceneManager__LoadSceneWithLoadingScene_mEE6B81E041BD064814084510639E8AE1FBB050E7 (void);
// 0x000004CF System.Collections.IEnumerator Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::_LoadSceneWLSCoroutine(System.String)
extern void UmodsSceneManager__LoadSceneWLSCoroutine_m3FF11DF925CCBE1728AC7510F67B90D724171CD3 (void);
// 0x000004D0 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::OnListenersAndModRegistration()
extern void UmodsSceneManager_OnListenersAndModRegistration_m11368CE2F11083E26653C677FBD210B482B49F0F (void);
// 0x000004D1 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::OnDestroyListenersAndModUnregistration()
extern void UmodsSceneManager_OnDestroyListenersAndModUnregistration_mB6419CA71BE6BCD515292D34C48911E28A3A1A55 (void);
// 0x000004D2 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager::.ctor()
extern void UmodsSceneManager__ctor_mD8D9F8212B79B1A277160016BFE256F5101A6F6D (void);
// 0x000004D3 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWithLoadingScene>d__14::.ctor(System.Int32)
extern void U3C_LoadSceneWithLoadingSceneU3Ed__14__ctor_m81C467AE102CF9E496C3707C5000ADFDA5D0B8C7 (void);
// 0x000004D4 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWithLoadingScene>d__14::System.IDisposable.Dispose()
extern void U3C_LoadSceneWithLoadingSceneU3Ed__14_System_IDisposable_Dispose_m66E2D552B1F3F76AC0461C003462563EFFC02E10 (void);
// 0x000004D5 System.Boolean Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWithLoadingScene>d__14::MoveNext()
extern void U3C_LoadSceneWithLoadingSceneU3Ed__14_MoveNext_mD494D0CD5A7CB5E0AD493E65A4F30639A9F7E416 (void);
// 0x000004D6 System.Object Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWithLoadingScene>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_LoadSceneWithLoadingSceneU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7B19E9FFEA9516935FF621847C946A770F5EC8F (void);
// 0x000004D7 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWithLoadingScene>d__14::System.Collections.IEnumerator.Reset()
extern void U3C_LoadSceneWithLoadingSceneU3Ed__14_System_Collections_IEnumerator_Reset_mC48B1FB57434D2BEA8AF056F76984F0005A05314 (void);
// 0x000004D8 System.Object Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWithLoadingScene>d__14::System.Collections.IEnumerator.get_Current()
extern void U3C_LoadSceneWithLoadingSceneU3Ed__14_System_Collections_IEnumerator_get_Current_mED8E7732E5D421FB99858C3646CF7DCA6C835A4D (void);
// 0x000004D9 System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWLSCoroutine>d__15::.ctor(System.Int32)
extern void U3C_LoadSceneWLSCoroutineU3Ed__15__ctor_m50F6845A118FD81E306CD942C66AB0D2BAB42762 (void);
// 0x000004DA System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWLSCoroutine>d__15::System.IDisposable.Dispose()
extern void U3C_LoadSceneWLSCoroutineU3Ed__15_System_IDisposable_Dispose_mC81A36BD9503D2FCC03969CEF2FA076DBC412A3F (void);
// 0x000004DB System.Boolean Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWLSCoroutine>d__15::MoveNext()
extern void U3C_LoadSceneWLSCoroutineU3Ed__15_MoveNext_m86C0215F6C7EBEFAB402CA2595AC86B33ACD95F2 (void);
// 0x000004DC System.Object Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWLSCoroutine>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_LoadSceneWLSCoroutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE0EFD83CFF38637105FF67D2F71BDA3391BEB77 (void);
// 0x000004DD System.Void Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWLSCoroutine>d__15::System.Collections.IEnumerator.Reset()
extern void U3C_LoadSceneWLSCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_mE9BA8B61286881D809B25880AE8D8682A5851FFD (void);
// 0x000004DE System.Object Umods.Scripts.InternalMods.SceneManager.Core.UmodsSceneManager/<_LoadSceneWLSCoroutine>d__15::System.Collections.IEnumerator.get_Current()
extern void U3C_LoadSceneWLSCoroutineU3Ed__15_System_Collections_IEnumerator_get_Current_m2328167C9476A1E5937BFC8DAA24801FAF02B050 (void);
// 0x000004DF System.Void Umods.Scripts.InternalMods.ExceptionsManager.Handlers.IUmodsExceptionEventHandler::OnExceptionEvent(Umods.Scripts.Common.UmodsException)
// 0x000004E0 System.Void Umods.Scripts.InternalMods.ExceptionsManager.Core.IUmodsExceptionEventManager::SendExceptionEvent(Umods.Scripts.Common.UmodsException)
// 0x000004E1 System.Void Umods.Scripts.InternalMods.ExceptionsManager.Core.UmodsExceptionEventManager::SendExceptionEvent(Umods.Scripts.Common.UmodsException)
extern void UmodsExceptionEventManager_SendExceptionEvent_m385ACF9D893A5D9C2A383078DCE79C9364B59A3F (void);
// 0x000004E2 System.Void Umods.Scripts.InternalMods.ExceptionsManager.Core.UmodsExceptionEventManager::OnListenersAndModRegistration()
extern void UmodsExceptionEventManager_OnListenersAndModRegistration_mC8EBD41255F96C7C5D7C7805BAB58E890933D4AE (void);
// 0x000004E3 System.Void Umods.Scripts.InternalMods.ExceptionsManager.Core.UmodsExceptionEventManager::OnDestroyListenersAndModUnregistration()
extern void UmodsExceptionEventManager_OnDestroyListenersAndModUnregistration_m419F79635E141557205F12005E6233DD0147F175 (void);
// 0x000004E4 System.Void Umods.Scripts.InternalMods.ExceptionsManager.Core.UmodsExceptionEventManager::.ctor()
extern void UmodsExceptionEventManager__ctor_m4B0A0E3446796490F99F061C7FEC8910F18580DA (void);
// 0x000004E5 System.Void Umods.Scripts.Interfaces.IUmodsEventHandlerManager::RegisterHandler(System.Object)
// 0x000004E6 System.Boolean Umods.Scripts.Interfaces.IUmodsEventHandlerManager::UnregisterHandler(System.Object)
// 0x000004E7 System.Boolean Umods.Scripts.Interfaces.IUmodsEventListener::PullEvent(System.String,System.Object[])
// 0x000004E8 System.Void Umods.Scripts.Interfaces.IUmodsLateLifeCycle::OnULateUpdate()
// 0x000004E9 System.Void Umods.Scripts.Core.UmodsAppConfiguration::Start()
extern void UmodsAppConfiguration_Start_m756A31A3818FE3575454278CA9FA099D54DC4BAC (void);
// 0x000004EA System.Void Umods.Scripts.Core.UmodsAppConfiguration::OnInitialializeConfiguration()
// 0x000004EB System.Void Umods.Scripts.Core.UmodsAppConfiguration::.ctor()
extern void UmodsAppConfiguration__ctor_m6D68E5A6CDCDB98F18B38142113F277142C65358 (void);
// 0x000004EC System.Boolean Umods.Scripts.Core.UmodsApplication::get_DebugInstance()
extern void UmodsApplication_get_DebugInstance_mF3BEB5D0E0B7B1182A88D6A75A409E0AFBAAF1F8 (void);
// 0x000004ED System.Void Umods.Scripts.Core.UmodsApplication::set_DebugInstance(System.Boolean)
extern void UmodsApplication_set_DebugInstance_m6BC90B09FD280FB7212AC4F4002881053AEAF9DB (void);
// 0x000004EE T Umods.Scripts.Core.UmodsApplication::GetUmodsAppConfiguration()
// 0x000004EF Umods.IUmodsEventListenerManager Umods.Scripts.Core.UmodsApplication::get_EventListenerManager()
extern void UmodsApplication_get_EventListenerManager_m195F930F81CE11BC82E9C02D797A66224570E342 (void);
// 0x000004F0 Umods.Scripts.Interfaces.IUmodsEventHandlerManager Umods.Scripts.Core.UmodsApplication::get_EventHandlerManager()
extern void UmodsApplication_get_EventHandlerManager_mA27BB145410EC52464E968C75D0B1D5F58315E70 (void);
// 0x000004F1 System.Void Umods.Scripts.Core.UmodsApplication::Awake()
extern void UmodsApplication_Awake_m692697763EE0E14295E5647A9DF9C9F0C3DFC35F (void);
// 0x000004F2 System.Void Umods.Scripts.Core.UmodsApplication::UStart()
extern void UmodsApplication_UStart_m32523CA197AD3D54339AE5DF8939608E9895288C (void);
// 0x000004F3 System.Void Umods.Scripts.Core.UmodsApplication::OnDestroy()
extern void UmodsApplication_OnDestroy_mCF386BB411D50D847AE84E9E50B4243A90B405E0 (void);
// 0x000004F4 System.Void Umods.Scripts.Core.UmodsApplication::InitializeLifeCycleListenerAndEventRegistrationManager()
extern void UmodsApplication_InitializeLifeCycleListenerAndEventRegistrationManager_m3BDF1D4446389C1C7A44C949AE4AF08A1A3C5C47 (void);
// 0x000004F5 System.Void Umods.Scripts.Core.UmodsApplication::InitializeInternalMods()
extern void UmodsApplication_InitializeInternalMods_m4A0C7A994723B2EED279F4ECCD19F8F3A51C3053 (void);
// 0x000004F6 System.Void Umods.Scripts.Core.UmodsApplication::RegisterInLifeCycle(System.Object)
extern void UmodsApplication_RegisterInLifeCycle_m49CEBA4BA567776C5EA4EA84288628A932573716 (void);
// 0x000004F7 System.Void Umods.Scripts.Core.UmodsApplication::UnregisterInLifeCycle(System.Object)
extern void UmodsApplication_UnregisterInLifeCycle_mD27DDE9B9198A750E1C89555CCD67594F264A487 (void);
// 0x000004F8 System.Void Umods.Scripts.Core.UmodsApplication::RegisterAppConfiguration(Umods.Scripts.Core.UmodsAppConfiguration)
extern void UmodsApplication_RegisterAppConfiguration_m976537DFB920FEFA97122915CA98F9D9DE1E73C5 (void);
// 0x000004F9 System.Void Umods.Scripts.Core.UmodsApplication::UnregisterAppConfiguration()
extern void UmodsApplication_UnregisterAppConfiguration_m6BD2EB56E5CDD62049F9A3EB65EC7E9525E3AB07 (void);
// 0x000004FA System.Void Umods.Scripts.Core.UmodsApplication::RegisterInternalMod(T)
// 0x000004FB System.Void Umods.Scripts.Core.UmodsApplication::UnregisterInternalMod()
// 0x000004FC T Umods.Scripts.Core.UmodsApplication::GetInternalMod()
// 0x000004FD System.Void Umods.Scripts.Core.UmodsApplication::RegisterMod(T)
// 0x000004FE System.Void Umods.Scripts.Core.UmodsApplication::UnregisterMod()
// 0x000004FF T Umods.Scripts.Core.UmodsApplication::GetMod()
// 0x00000500 System.Void Umods.Scripts.Core.UmodsApplication::.ctor()
extern void UmodsApplication__ctor_m23381177433EE98ABA788B4AFA10C64EC1025DDF (void);
// 0x00000501 Umods.Scripts.Core.UmodsApplication Umods.Scripts.Core.UmodsBehaviour::get_UmodsApplication()
extern void UmodsBehaviour_get_UmodsApplication_m96F40DAD3183272A19075FEC6EF8F1CDB01C6CBD (void);
// 0x00000502 System.Void Umods.Scripts.Core.UmodsBehaviour::Start()
extern void UmodsBehaviour_Start_mE3C02481C4BAD0BFF16CD1A560DA5DE6611A6EDE (void);
// 0x00000503 System.Void Umods.Scripts.Core.UmodsBehaviour::OnRegisterHandler()
extern void UmodsBehaviour_OnRegisterHandler_m41C5C9E5AD5C19B385A4A528C994171487E7044F (void);
// 0x00000504 System.Void Umods.Scripts.Core.UmodsBehaviour::OnDestroy()
extern void UmodsBehaviour_OnDestroy_m90AC2450B8E797D32B52873BC1539ABB0C4BFDD8 (void);
// 0x00000505 System.Void Umods.Scripts.Core.UmodsBehaviour::InternalUOnDestroy()
extern void UmodsBehaviour_InternalUOnDestroy_m3907A5E90BA1E2618BD279A364C3161BF5485F76 (void);
// 0x00000506 System.Void Umods.Scripts.Core.UmodsBehaviour::.ctor()
extern void UmodsBehaviour__ctor_m9B23039613C214337707F448630A167E8293DD83 (void);
// 0x00000507 System.Void Umods.Scripts.Core.UmodsEventListener::.ctor(System.Type)
extern void UmodsEventListener__ctor_mD5518F4FF6657E76D28857DDE299CF0819A6535B (void);
// 0x00000508 System.Boolean Umods.Scripts.Core.UmodsEventListener::AddObject(System.Object)
extern void UmodsEventListener_AddObject_mD057299D76C01C9310D8BCA3AF0AD50062BE28E0 (void);
// 0x00000509 System.Boolean Umods.Scripts.Core.UmodsEventListener::RemoveObject(System.Object)
extern void UmodsEventListener_RemoveObject_mDDC41C774A5AF11A0A2A2EE69208BC3D550569AB (void);
// 0x0000050A System.Boolean Umods.Scripts.Core.UmodsEventListener::PullEvent(System.String,System.Object[])
extern void UmodsEventListener_PullEvent_m80CD5D0AF7A142A6F9FD3CFD357D6913EFB1899C (void);
// 0x0000050B System.Void Umods.Scripts.Core.UmodsEventListenerHandlerManager::RegisterHandler(System.Object)
extern void UmodsEventListenerHandlerManager_RegisterHandler_m3CB3CD4F526005C8971C8BCA726CCDC640573CF0 (void);
// 0x0000050C System.Boolean Umods.Scripts.Core.UmodsEventListenerHandlerManager::UnregisterHandler(System.Object)
extern void UmodsEventListenerHandlerManager_UnregisterHandler_m370565EC288C3B6E0ED949D80FF6A69F21CB42B3 (void);
// 0x0000050D Umods.Scripts.Interfaces.IUmodsEventListener Umods.Scripts.Core.UmodsEventListenerHandlerManager::RegisterListener()
// 0x0000050E Umods.Scripts.Interfaces.IUmodsEventListener Umods.Scripts.Core.UmodsEventListenerHandlerManager::GetListener()
// 0x0000050F System.Boolean Umods.Scripts.Core.UmodsEventListenerHandlerManager::UnregisterListener()
// 0x00000510 System.Void Umods.Scripts.Core.UmodsEventListenerHandlerManager::ClearListeners()
extern void UmodsEventListenerHandlerManager_ClearListeners_mEA4B1FC88D1F872F75E4D7C36213BC890F0355D2 (void);
// 0x00000511 System.Void Umods.Scripts.Core.UmodsEventListenerHandlerManager::.ctor()
extern void UmodsEventListenerHandlerManager__ctor_mB551AD2C5AFDAB0B50135AEF6981B47C636F24CB (void);
// 0x00000512 System.Void Umods.Scripts.Core.UmodsInternalModBehaviour::.ctor()
extern void UmodsInternalModBehaviour__ctor_m7987CB1FC12147238876E420629A5CBF829BA80F (void);
// 0x00000513 System.Void Umods.Scripts.Core.UmodsLifeCycleListener::RegisterHandler(System.Object)
extern void UmodsLifeCycleListener_RegisterHandler_m733C0C190D4A1C6C4A00CC6D7942FBF2EC963BE1 (void);
// 0x00000514 System.Void Umods.Scripts.Core.UmodsLifeCycleListener::UnregisterHandler(System.Object)
extern void UmodsLifeCycleListener_UnregisterHandler_m076C0466BB7F9E6B8C7C6D8A604B64B19A37FD8F (void);
// 0x00000515 System.Void Umods.Scripts.Core.UmodsLifeCycleListener::UnregisterAll()
extern void UmodsLifeCycleListener_UnregisterAll_m1E8992AAB50679C9A0C1C888BFB5A60182A9182E (void);
// 0x00000516 System.Boolean Umods.Scripts.Core.UmodsLifeCycleListener::_RegisterUmodsBehaviour(System.Boolean,System.Object,System.String,Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler&,Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler&,Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler&,Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler&,Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler&)
extern void UmodsLifeCycleListener__RegisterUmodsBehaviour_m7CE22E8325F0C97C9A215FFA71E23915CD4FE0E3 (void);
// 0x00000517 System.Void Umods.Scripts.Core.UmodsLifeCycleListener::_RegistrationChange(System.Object,System.Boolean)
extern void UmodsLifeCycleListener__RegistrationChange_m2165F468436681F1437BF857A55A4C119EE9F1E8 (void);
// 0x00000518 System.Void Umods.Scripts.Core.UmodsLifeCycleListener::Update()
extern void UmodsLifeCycleListener_Update_m15C92ECF3099EC42E882440E1064453214C95608 (void);
// 0x00000519 System.Void Umods.Scripts.Core.UmodsLifeCycleListener::OnDestroyCore()
extern void UmodsLifeCycleListener_OnDestroyCore_m31AE6A78DC000BA99078EDEE2448EFBBF61FDD62 (void);
// 0x0000051A System.Void Umods.Scripts.Core.UmodsLifeCycleListener::.ctor()
extern void UmodsLifeCycleListener__ctor_m7C9CC9BB3B07718FFCE8C36B42FA673F16A40717 (void);
// 0x0000051B System.Void Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler::.ctor(System.Object,System.IntPtr)
extern void OnUmodsLifeCycleEventHandler__ctor_m8B7784EAC8EEEF49CB3C7793A095FE1088874074 (void);
// 0x0000051C System.Void Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler::Invoke()
extern void OnUmodsLifeCycleEventHandler_Invoke_m95B7632F30205D9AE8DDF249DCAA2F50DE90EFEC (void);
// 0x0000051D System.IAsyncResult Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnUmodsLifeCycleEventHandler_BeginInvoke_m763DF62BF687D64EE7FFF6000D97DD419739BAC1 (void);
// 0x0000051E System.Void Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler::EndInvoke(System.IAsyncResult)
extern void OnUmodsLifeCycleEventHandler_EndInvoke_m76FE5D48B0EA6E8C5D3090F1B112F7955348F322 (void);
// 0x0000051F System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m9E6466B364C702711AD7B76359C3481D91627208 (void);
// 0x00000520 System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass32_0::<_RegisterUmodsBehaviour>b__0(Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler)
extern void U3CU3Ec__DisplayClass32_0_U3C_RegisterUmodsBehaviourU3Eb__0_m3A21958093DAD21CCD4798F8048EC52AA65974F3 (void);
// 0x00000521 System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mE900DF73CBF2801C37D11EA78C012DE9986C5D29 (void);
// 0x00000522 System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass33_0::<_RegistrationChange>b__0(Umods.Scripts.Core.UmodsModBehaviour)
extern void U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__0_mC478BB26B1F987CB5854B8AB268343F741BBA493 (void);
// 0x00000523 System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass33_0::<_RegistrationChange>b__1(Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler)
extern void U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__1_m13252784B3E6D057BF29CBCA302EA721282B0735 (void);
// 0x00000524 System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass33_0::<_RegistrationChange>b__2(Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler)
extern void U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__2_m9F4247B9FF77082B0868ADA13CE2CBF4076DEA1B (void);
// 0x00000525 System.Void Umods.Scripts.Core.UmodsLifeCycleListener/<>c__DisplayClass33_0::<_RegistrationChange>b__3(Umods.Scripts.Core.UmodsLifeCycleListener/OnUmodsLifeCycleEventHandler)
extern void U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__3_mE7594930A6AACF9179CD882B5A616299369B3712 (void);
// 0x00000526 System.Boolean Umods.Scripts.Core.UmodsModBehaviour::get_DebugLog()
extern void UmodsModBehaviour_get_DebugLog_mAE099FC40A8DE6CE6D53E42562940302CEE60E29 (void);
// 0x00000527 System.Void Umods.Scripts.Core.UmodsModBehaviour::set_DebugLog(System.Boolean)
extern void UmodsModBehaviour_set_DebugLog_m79793141656BEEF92682E2EF95E7EBC3EB8EDE9D (void);
// 0x00000528 System.Void Umods.Scripts.Core.UmodsModBehaviour::OnInitializeModBehaviour()
extern void UmodsModBehaviour_OnInitializeModBehaviour_m9589E49E2B7AE63FB9C8A1A3F1FB2AD7D40BB929 (void);
// 0x00000529 System.Void Umods.Scripts.Core.UmodsModBehaviour::OnListenersAndModRegistration()
// 0x0000052A System.Void Umods.Scripts.Core.UmodsModBehaviour::OnDestroyListenersAndModUnregistration()
// 0x0000052B System.Void Umods.Scripts.Core.UmodsModBehaviour::UOnModDestroy()
extern void UmodsModBehaviour_UOnModDestroy_m4E10F2F54573C95502E9F66C779267D5328A9898 (void);
// 0x0000052C System.Void Umods.Scripts.Core.UmodsModBehaviour::.ctor()
extern void UmodsModBehaviour__ctor_m24D793B7A7F5BBD8A38F16207EF91F92BBD65E2A (void);
// 0x0000052D System.Void Umods.Scripts.Core.UmodsModBehaviour/DebugObject::.ctor(Umods.Scripts.Core.UmodsModBehaviour)
extern void DebugObject__ctor_mDEE53DE83E87C16DD5200C7CE23E601AAC6115CF (void);
// 0x0000052E System.Void Umods.Scripts.Core.UmodsModBehaviour/DebugObject::Log(System.String)
extern void DebugObject_Log_mAE6B3DE529532CC9E1E890376DFB0F5335873514 (void);
// 0x0000052F System.Void Umods.Scripts.Core.UmodsSceneBehaviour::OnInitializeSceneBehaviour()
extern void UmodsSceneBehaviour_OnInitializeSceneBehaviour_m3BF0E5E8FABD3CB42E457005F460E0896C086BA8 (void);
// 0x00000530 System.Void Umods.Scripts.Core.UmodsSceneBehaviour::.ctor()
extern void UmodsSceneBehaviour__ctor_mECEEBA6FA289E8E745B831BDDAF787F8DA067FBA (void);
// 0x00000531 System.Boolean Umods.Scripts.Common.UmodsExtensions::SafeConvert(System.Object,System.Action`1<T>)
// 0x00000532 System.Boolean Umods.Scripts.Common.UmodsExtensions::SafeGetDelegate(System.Object,System.String,System.Action`1<T>)
// 0x00000533 System.Boolean Umods.Scripts.Common.UmodsExtensions::SafeCallMethod(System.Object,System.String)
extern void UmodsExtensions_SafeCallMethod_m744E1D91A0AEF2C7FD6F6738951E06529CC5FB49 (void);
// 0x00000534 UnityEngine.EventSystems.EventTrigger/Entry Umods.Scripts.Common.UmodsExtensions::AddListenerToEventTrigger(UnityEngine.GameObject,UnityEngine.EventSystems.EventTriggerType,UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>)
extern void UmodsExtensions_AddListenerToEventTrigger_m9FBFF45C7CEE2736786E92B84136B203FF0F7AB8 (void);
// 0x00000535 System.Boolean Umods.Scripts.Common.UmodsExtensions::RemoveListenerToEventTrigger(UnityEngine.GameObject,UnityEngine.EventSystems.EventTrigger/Entry)
extern void UmodsExtensions_RemoveListenerToEventTrigger_mA515624ACB8C2586DC50107E30780B890AA2F26D (void);
// 0x00000536 System.Void Umods.Scripts.Common.UmodsException::.ctor(Umods.Scripts.Common.UmodsEnums/UmodsExceptionType,System.String,System.String)
extern void UmodsException__ctor_m92FFCD7FC71D5ACA6683853D5AEB9E67D3F90FBF (void);
// 0x00000537 System.Void Umods.Scripts.Common.UmodsEvent::.ctor(Umods.Scripts.Common.UmodsEnums/UmodsEventType,System.String,System.String)
extern void UmodsEvent__ctor_m2F58B1A195AF6F704719694E1817412186D30D2A (void);
// 0x00000538 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::OnListenersAndModRegistration()
extern void ScreenManagerMod_OnListenersAndModRegistration_m49095AA6767AE2CA646699898BE49F95114E928A (void);
// 0x00000539 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::OnDestroyListenersAndModUnregistration()
extern void ScreenManagerMod_OnDestroyListenersAndModUnregistration_m62E8906B8238EFF0B3184AFC0852D7C73DB6CE96 (void);
// 0x0000053A System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::UStart()
extern void ScreenManagerMod_UStart_m31C82B93298132DF45D6C7340E5D241360D4A25C (void);
// 0x0000053B System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::OnBackScreenButtonPressed()
extern void ScreenManagerMod_OnBackScreenButtonPressed_mBD7DCFE0E394927BE757693E5619710EF4E48275 (void);
// 0x0000053C System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::OnNextScreenButtonPressed(System.String)
extern void ScreenManagerMod_OnNextScreenButtonPressed_mB692A1EC12E9B558D6A8CA9FACEDFA16B47E79FF (void);
// 0x0000053D System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::OnBackScopeButtonPressed()
extern void ScreenManagerMod_OnBackScopeButtonPressed_mF01FFC11CA2DFE71A083E3975BE5B9BD3814270E (void);
// 0x0000053E System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::OnNextScopeButtonPressed(System.String)
extern void ScreenManagerMod_OnNextScopeButtonPressed_m8CC7D4E49E65EFCE9AACC0152511D17C4E5A4829 (void);
// 0x0000053F System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::ChangeScope(UnityEngine.ScreenScope)
extern void ScreenManagerMod_ChangeScope_m33593BE6F00CC2D87BCEFC9EEFAF845362987C9C (void);
// 0x00000540 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core.ScreenManagerMod::.ctor()
extern void ScreenManagerMod__ctor_m2DB508868617F70D9A2E1FECF9843FF6C9B694B3 (void);
// 0x00000541 System.Action Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::get_OnBackScreenButtonPressed()
extern void Button_get_OnBackScreenButtonPressed_m609BE5D9024DB96AF14E7AB48784954853352132 (void);
// 0x00000542 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::set_OnBackScreenButtonPressed(System.Action)
extern void Button_set_OnBackScreenButtonPressed_mA362E4510D5A37494C70C4B381560738EA1BD81B (void);
// 0x00000543 System.Action`1<System.String> Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::get_OnNextScreenButtonPressed()
extern void Button_get_OnNextScreenButtonPressed_m1FDD0BEC3151CD74F2CC59A01F0493E253BC7885 (void);
// 0x00000544 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::set_OnNextScreenButtonPressed(System.Action`1<System.String>)
extern void Button_set_OnNextScreenButtonPressed_mE52744DF31765A99EB18C023DEB3BFBBAFFC0D47 (void);
// 0x00000545 System.Action Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::get_OnBackScopeButtonPressed()
extern void Button_get_OnBackScopeButtonPressed_mBE8EA96606E44492996D9940B73E23C873018C4D (void);
// 0x00000546 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::set_OnBackScopeButtonPressed(System.Action)
extern void Button_set_OnBackScopeButtonPressed_mF6683A24AF42B2E9DEB85A03A23540C62A1B2088 (void);
// 0x00000547 System.Action`1<System.String> Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::get_OnNextScopeButtonPressed()
extern void Button_get_OnNextScopeButtonPressed_m4DBD3F41902CB89636DD1A404FFAC42912AB0EDA (void);
// 0x00000548 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::set_OnNextScopeButtonPressed(System.Action`1<System.String>)
extern void Button_set_OnNextScopeButtonPressed_mFEBE8FDCDE00876EFA6A9A1907D3649AC79345CD (void);
// 0x00000549 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::UStart()
extern void Button_UStart_m4B852EBF9ED7E204ED9665D439E01B8EFB7041CF (void);
// 0x0000054A System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::.ctor()
extern void Button__ctor_m123DAA14105BD1A16B6D3F62CF6B9D5FE303DED2 (void);
// 0x0000054B System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::<UStart>b__21_0()
extern void Button_U3CUStartU3Eb__21_0_mB25A26CF57A1A4A5132E983FB09DA07BE2BB5E8F (void);
// 0x0000054C System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Button::<UStart>b__21_1()
extern void Button_U3CUStartU3Eb__21_1_mD5F99A0DC8A1EF0188A5052662B013B2797F1F95 (void);
// 0x0000054D System.Action Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::get_OnBackScreenButtonPressed()
extern void Screen_get_OnBackScreenButtonPressed_m416ADFCFEB9CE02B307E8D5A2F99015310A8289B (void);
// 0x0000054E System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::set_OnBackScreenButtonPressed(System.Action)
extern void Screen_set_OnBackScreenButtonPressed_mE7457A9B5CDE69822AC99CDAF872354363518D71 (void);
// 0x0000054F System.Action`1<System.String> Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::get_OnNextScreenButtonPressed()
extern void Screen_get_OnNextScreenButtonPressed_m75EB01E0B09CB84EB7844319D295B2A5F73A221C (void);
// 0x00000550 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::set_OnNextScreenButtonPressed(System.Action`1<System.String>)
extern void Screen_set_OnNextScreenButtonPressed_m083B7E28C7643FFB2311ACAB0D73C5A18C48AA67 (void);
// 0x00000551 System.Action Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::get_OnBackScopeButtonPressed()
extern void Screen_get_OnBackScopeButtonPressed_mE420686898F625E539812F5677E47AC7C95561E1 (void);
// 0x00000552 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::set_OnBackScopeButtonPressed(System.Action)
extern void Screen_set_OnBackScopeButtonPressed_mC7B8C39A2EBE52FF6DA55EFCD4CD774E8D6E1E4A (void);
// 0x00000553 System.Action`1<System.String> Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::get_OnNextScopeButtonPressed()
extern void Screen_get_OnNextScopeButtonPressed_mE8A6296D3B83A8C7A6EE5037D9ABB05BAB0A7D04 (void);
// 0x00000554 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::set_OnNextScopeButtonPressed(System.Action`1<System.String>)
extern void Screen_set_OnNextScopeButtonPressed_m0832AE6B1BCAB7BCB4E90D77703929DBF1F84020 (void);
// 0x00000555 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::UStart()
extern void Screen_UStart_m47F7D69C04A82F3F1DC8DFA85880E26ABE1927E3 (void);
// 0x00000556 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::_OnBackButtonPressed()
extern void Screen__OnBackButtonPressed_m6A7394F5D6FC55B9275C70ABD81878110682F54E (void);
// 0x00000557 System.Void Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen::.ctor()
extern void Screen__ctor_m819E5C490D6859F1161DCC0A2B60444DC18A9BE9 (void);
// 0x00000558 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Testing.TestingObjectTemplate::.ctor()
extern void TestingObjectTemplate__ctor_m47973D8EF1DDA3DFABF6F17E47D402B85A2E05EA (void);
// 0x00000559 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Handlers.ILoadFileHandler::OnLoadFileCompleted(System.Byte[])
// 0x0000055A System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Handlers.ILoadObjectTemplateHandler::OnLoadCompleted(System.Object)
// 0x0000055B System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Handlers.IStorageCompletedHandler::OnStorageCompleted(Umods.ExternalMods.Storage.Mods.StorageMod.Common.StorageVariables/StoragingState)
// 0x0000055C System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.IStorageMod::StorageFileAsync(System.Byte[],System.String)
// 0x0000055D System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.IStorageMod::StorageObjectTemplateAsync(T,System.String)
// 0x0000055E System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.IStorageMod::LoadObjectTemplateAsync(System.String)
// 0x0000055F System.String[] Umods.ExternalMods.Storage.Mods.StorageMod.Core.IStorageMod::GetFilesInDirectory(System.String)
// 0x00000560 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::OnListenersAndModRegistration()
extern void StorageMod_OnListenersAndModRegistration_mB18E95189E806D24D5D795041EA6DA81621068A7 (void);
// 0x00000561 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::OnDestroyListenersAndModUnregistration()
extern void StorageMod_OnDestroyListenersAndModUnregistration_m05087236084D37A5681BC6CD840C092186C12EA3 (void);
// 0x00000562 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::StorageFileAsync(System.Byte[],System.String)
extern void StorageMod_StorageFileAsync_m7A6FF8FE2E35092C07808AF31420B0B123ED687A (void);
// 0x00000563 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::LoadFileAsync(System.String)
extern void StorageMod_LoadFileAsync_mEA22A8A562D4947EB508BEFF8BACF42033034283 (void);
// 0x00000564 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::StorageObjectTemplateAsync(T,System.String)
// 0x00000565 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::_WriteTextAsync(System.Byte[],System.String)
extern void StorageMod__WriteTextAsync_m8AB6A9591235A1950E6DB2A086C1020993ABF53A (void);
// 0x00000566 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::LoadObjectTemplateAsync(System.String)
// 0x00000567 System.String[] Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::GetFilesInDirectory(System.String)
extern void StorageMod_GetFilesInDirectory_m7FCAEEB844A746DB75CB3D72EAB2F4727E8D33AB (void);
// 0x00000568 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::_ReadTextAsync(System.String)
// 0x00000569 System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod::.ctor()
extern void StorageMod__ctor_m27CAF7DE6FA2B0B1A4FCB8924DD6D36CFD53A9E8 (void);
// 0x0000056A System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod/<_WriteTextAsync>d__10::MoveNext()
extern void U3C_WriteTextAsyncU3Ed__10_MoveNext_mB047908016279CE15714F3FA197BEF9447B2C19F (void);
// 0x0000056B System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod/<_WriteTextAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3C_WriteTextAsyncU3Ed__10_SetStateMachine_m796A7F9239E531781DEFA9126F8DE85368105461 (void);
// 0x0000056C System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod/<_ReadTextAsync>d__14`1::MoveNext()
// 0x0000056D System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Core.StorageMod/<_ReadTextAsync>d__14`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x0000056E System.Void Umods.ExternalMods.Storage.Mods.StorageMod.Common.StorageVariables::.ctor()
extern void StorageVariables__ctor_mD87338FE7ADD19ED1FC8B3D45A0A4562AAC4EDD0 (void);
// 0x0000056F System.Void Umods.ExternalMods.Media.Scripts.Core.ICameraMediaMakerMod::ShowCameraOnTexture(UnityEngine.UI.RawImage,UnityEngine.UI.AspectRatioFitter)
// 0x00000570 System.Void Umods.ExternalMods.Media.Scripts.Core.ICameraMediaMakerMod::ShowCameraOnTexture()
// 0x00000571 System.Void Umods.ExternalMods.Media.Scripts.Core.ICameraMediaMakerMod::StopShowingCameraOnTexture()
// 0x00000572 System.Void Umods.ExternalMods.Media.Scripts.Core.ICameraMediaMakerMod::TakePhotoAsync(System.String,System.String)
// 0x00000573 System.Void Umods.ExternalMods.Media.Scripts.Core.ICameraMediaMakerMod::StartRecordingVideo()
// 0x00000574 System.Void Umods.ExternalMods.Media.Scripts.Core.ICameraMediaMakerMod::StopAndSaveRecordedVideoAsync(System.String,System.String)
// 0x00000575 System.Boolean Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::StartReceivingGyroscopeStatus(System.Single)
extern void GyroscopeControllerMod_StartReceivingGyroscopeStatus_m922E31AE36E62B59BD197565A604EB5B757FDC6E (void);
// 0x00000576 System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::StopReceivingGyroscopeStatus()
extern void GyroscopeControllerMod_StopReceivingGyroscopeStatus_mA1EED369367269455988FBD65DD31CDB0BC5D848 (void);
// 0x00000577 System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::OnListenersAndModRegistration()
extern void GyroscopeControllerMod_OnListenersAndModRegistration_mBF9A501D66F3102F7F0FAD4A1924E9C52DB77FB0 (void);
// 0x00000578 System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::OnDestroyListenersAndModUnregistration()
extern void GyroscopeControllerMod_OnDestroyListenersAndModUnregistration_mD070ED5A7687E2D8D413FC83DBAA96950EE17350 (void);
// 0x00000579 System.Collections.IEnumerator Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::_UpdateGyroscopeData(System.Single)
extern void GyroscopeControllerMod__UpdateGyroscopeData_m0600F9A96FDEA94FC3702F9C032B581C2666F950 (void);
// 0x0000057A UnityEngine.Quaternion Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::GyroToUnity(UnityEngine.Quaternion)
extern void GyroscopeControllerMod_GyroToUnity_mF1CE12C5653D791341CAAA2DAA3FCB2A0528C533 (void);
// 0x0000057B System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod::.ctor()
extern void GyroscopeControllerMod__ctor_m25C6FC32EC6CEE08CC3E09D16AD553598807603E (void);
// 0x0000057C System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod/<_UpdateGyroscopeData>d__7::.ctor(System.Int32)
extern void U3C_UpdateGyroscopeDataU3Ed__7__ctor_mDFD95E244D6F408CFC29BEC5906DC0A00188029C (void);
// 0x0000057D System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod/<_UpdateGyroscopeData>d__7::System.IDisposable.Dispose()
extern void U3C_UpdateGyroscopeDataU3Ed__7_System_IDisposable_Dispose_m44573EF4EE073167E47E135AED33EDE468119ED9 (void);
// 0x0000057E System.Boolean Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod/<_UpdateGyroscopeData>d__7::MoveNext()
extern void U3C_UpdateGyroscopeDataU3Ed__7_MoveNext_m386B68E7765872F261712E61429803F7FC4EC77C (void);
// 0x0000057F System.Object Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod/<_UpdateGyroscopeData>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_UpdateGyroscopeDataU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D059DBFBF4649D156F14D5D5D992F073255ECF1 (void);
// 0x00000580 System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod/<_UpdateGyroscopeData>d__7::System.Collections.IEnumerator.Reset()
extern void U3C_UpdateGyroscopeDataU3Ed__7_System_Collections_IEnumerator_Reset_mB79A8FCC7850F965BEF869458CB983B58D8296E4 (void);
// 0x00000581 System.Object Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.GyroscopeControllerMod/<_UpdateGyroscopeData>d__7::System.Collections.IEnumerator.get_Current()
extern void U3C_UpdateGyroscopeDataU3Ed__7_System_Collections_IEnumerator_get_Current_m4FE3B44ED999C89B3E9DC1304126B33CE28811D9 (void);
// 0x00000582 System.Boolean Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.IGyroscopeControllerMod::StartReceivingGyroscopeStatus(System.Single)
// 0x00000583 System.Void Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core.IGyroscopeControllerMod::StopReceivingGyroscopeStatus()
// 0x00000584 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::OnListenersAndModRegistration()
extern void SimulationConnectivityBluetoothModMod_OnListenersAndModRegistration_m0D9E2A3FABD92AAF51D1EAE72BA6D1F7EC42B422 (void);
// 0x00000585 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::OnDestroyListenersAndModUnregistration()
extern void SimulationConnectivityBluetoothModMod_OnDestroyListenersAndModUnregistration_mE1EC35681D1B9B03C5F4762F034816E217AC796A (void);
// 0x00000586 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::StartScanningBluetoothDeviceListAsync(System.String)
extern void SimulationConnectivityBluetoothModMod_StartScanningBluetoothDeviceListAsync_mDE326AA90A37832F9907367E19F90AEA6A7A4B63 (void);
// 0x00000587 System.Collections.IEnumerator Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::_SimulateGettingBluetoothDeviceList()
extern void SimulationConnectivityBluetoothModMod__SimulateGettingBluetoothDeviceList_m19B8F4ED3653F39997198431E6B784BD4A852165 (void);
// 0x00000588 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::StopScanningBluetoothDeviceList()
extern void SimulationConnectivityBluetoothModMod_StopScanningBluetoothDeviceList_mDF37C964655C6716461E60D6DF79941BB4FACEDE (void);
// 0x00000589 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::ConnectToBluetoothDeviceAsync(System.String)
extern void SimulationConnectivityBluetoothModMod_ConnectToBluetoothDeviceAsync_m1B1B995069A6DA0A58283FD817CB4190C61F2C89 (void);
// 0x0000058A System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::DisconnectFromBluetoothDeviceAsync()
extern void SimulationConnectivityBluetoothModMod_DisconnectFromBluetoothDeviceAsync_m746261DEA30AEE1EE5EC5824F9BEDCE1F369A5D4 (void);
// 0x0000058B System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::SendMessageToConnectedBluetooth(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.String,System.Boolean)
extern void SimulationConnectivityBluetoothModMod_SendMessageToConnectedBluetooth_m731BF742913F41DD13249D18CE1A8D0A42A9C702 (void);
// 0x0000058C System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::SendMessageToConnectedBluetooth(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.Byte[],System.Boolean)
extern void SimulationConnectivityBluetoothModMod_SendMessageToConnectedBluetooth_m1CC8EBD3856D3EF28B39063C20B89D58BAC95B1A (void);
// 0x0000058D System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::IsConnectedToAnyBluetoothDevice()
extern void SimulationConnectivityBluetoothModMod_IsConnectedToAnyBluetoothDevice_m4EE2287AE8DB577F3C25C0E1C32E05A7BE04CB5F (void);
// 0x0000058E System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::IsConnectedToBluetoothDevice(System.String)
extern void SimulationConnectivityBluetoothModMod_IsConnectedToBluetoothDevice_mA025A222EA5EADD7EC9E8DCDA5C3FE2F606ED204 (void);
// 0x0000058F Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::GetConnectedBluetoothDevice()
extern void SimulationConnectivityBluetoothModMod_GetConnectedBluetoothDevice_mF541C5AFCE73F1C3799709D1AC1A7AE87DE3E653 (void);
// 0x00000590 Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::GetBluetoothDevice(System.String)
extern void SimulationConnectivityBluetoothModMod_GetBluetoothDevice_mE09AFD52EC26ECAB5AFD8F5F7A1C975F488872CD (void);
// 0x00000591 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::IsScanningBluetoothDevices()
extern void SimulationConnectivityBluetoothModMod_IsScanningBluetoothDevices_m758233A97527A56B47E72F141061B1AE0E43D3DA (void);
// 0x00000592 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::_SimulateBluetoothConection()
extern void SimulationConnectivityBluetoothModMod__SimulateBluetoothConection_mCABA65A36D851FD462C4D893A241323B298E52B1 (void);
// 0x00000593 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::_SimulateBluetoothDisconection()
extern void SimulationConnectivityBluetoothModMod__SimulateBluetoothDisconection_m135B7F00F9C3210E96649005DB7163C81806D557 (void);
// 0x00000594 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice>)
extern void SimulationConnectivityBluetoothModMod_OnBluetoothDeviceListUpdated_m9A2777DCFF584732728D422ACE9C7804002F016E (void);
// 0x00000595 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::OnBluetoothConnected(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice,Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
extern void SimulationConnectivityBluetoothModMod_OnBluetoothConnected_mD4766D2A08A7DBA1E07D40663D9484B45A0E4A94 (void);
// 0x00000596 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::OnBluetoothDisconnected(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice,Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
extern void SimulationConnectivityBluetoothModMod_OnBluetoothDisconnected_mAEB0751732561566D1CE1997982C9EF4E8534398 (void);
// 0x00000597 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod::.ctor()
extern void SimulationConnectivityBluetoothModMod__ctor_m422B3A214E6994CDEBB612356FA889254CB5994C (void);
// 0x00000598 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod/<_SimulateGettingBluetoothDeviceList>d__14::.ctor(System.Int32)
extern void U3C_SimulateGettingBluetoothDeviceListU3Ed__14__ctor_mA36A2DDC811386A2D396148928E0AACD12A4A5D6 (void);
// 0x00000599 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod/<_SimulateGettingBluetoothDeviceList>d__14::System.IDisposable.Dispose()
extern void U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_IDisposable_Dispose_mF1C60528FDD9D435DD268C418D681634D5AE2AAD (void);
// 0x0000059A System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod/<_SimulateGettingBluetoothDeviceList>d__14::MoveNext()
extern void U3C_SimulateGettingBluetoothDeviceListU3Ed__14_MoveNext_mB4AB6BA6BA7BB2D6BF4F66ED6D239E08D3890C1E (void);
// 0x0000059B System.Object Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod/<_SimulateGettingBluetoothDeviceList>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FBADF02C7E7828D0384895172F9E921AE4B6365 (void);
// 0x0000059C System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod/<_SimulateGettingBluetoothDeviceList>d__14::System.Collections.IEnumerator.Reset()
extern void U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_Collections_IEnumerator_Reset_m8A76CDC2DD87648EBE8B6CE2A406B85A2549C9C8 (void);
// 0x0000059D System.Object Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core.SimulationConnectivityBluetoothModMod/<_SimulateGettingBluetoothDeviceList>d__14::System.Collections.IEnumerator.get_Current()
extern void U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_Collections_IEnumerator_get_Current_mBC5FF564E0FC3B0B0EE0719BCA9184291D9B71A6 (void);
// 0x0000059E System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers.IConnectivityBluetoothHandler::OnBluetoothDeviceListUpdated(System.Collections.Generic.List`1<Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice>)
// 0x0000059F System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers.IConnectivityBluetoothHandler::OnBluetoothConnected(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice,Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
// 0x000005A0 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers.IConnectivityBluetoothHandler::OnBluetoothDisconnected(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice,Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
// 0x000005A1 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers.IConnectivityBluetoothHandler::OnMessageSent(System.String)
// 0x000005A2 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::OnListenersAndModRegistration()
extern void ConnectivityBluetoothMod_OnListenersAndModRegistration_m7F534B438B6DC3AA591D98AB2092D90DCC400D46 (void);
// 0x000005A3 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::OnDestroyListenersAndModUnregistration()
extern void ConnectivityBluetoothMod_OnDestroyListenersAndModUnregistration_mD651EC19300F1168FDB7D365812EAA7E563A7A64 (void);
// 0x000005A4 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::UStart()
extern void ConnectivityBluetoothMod_UStart_m67BE4D88D1761FDE67A9088C115C45111B1E1101 (void);
// 0x000005A5 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::StartScanningBluetoothDeviceListAsync(System.String)
extern void ConnectivityBluetoothMod_StartScanningBluetoothDeviceListAsync_mB601FA85622EF602C7E83C6E27DE39E7361073F1 (void);
// 0x000005A6 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::StopScanningBluetoothDeviceList()
extern void ConnectivityBluetoothMod_StopScanningBluetoothDeviceList_mD9F1844ECC08D1DB82A15F811F72C45AA3CE3A95 (void);
// 0x000005A7 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::IsScanningBluetoothDevices()
extern void ConnectivityBluetoothMod_IsScanningBluetoothDevices_mFCB03978912BE0E1F9D62854A48263A366013960 (void);
// 0x000005A8 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::ConnectToBluetoothDeviceAsync(System.String)
extern void ConnectivityBluetoothMod_ConnectToBluetoothDeviceAsync_mE9574FD7CA35A7CC6EA06584DD211FBAC5E63AF6 (void);
// 0x000005A9 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::DisconnectFromBluetoothDeviceAsync()
extern void ConnectivityBluetoothMod_DisconnectFromBluetoothDeviceAsync_m0CF6773E6E4940935BACC7AA33B39C7C4821949E (void);
// 0x000005AA System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::SendMessageToConnectedBluetooth(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.String,System.Boolean)
extern void ConnectivityBluetoothMod_SendMessageToConnectedBluetooth_m6A7FC5172EB6B52CF46AF8B4DE74A4FA80DD386F (void);
// 0x000005AB System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::SendMessageToConnectedBluetooth(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.Byte[],System.Boolean)
extern void ConnectivityBluetoothMod_SendMessageToConnectedBluetooth_m15C5218D173372A5B94C7A9FB592E43CC5E3B662 (void);
// 0x000005AC System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::IsConnectedToAnyBluetoothDevice()
extern void ConnectivityBluetoothMod_IsConnectedToAnyBluetoothDevice_m99CF0FB743561C7F28A519A94535809E27FD9CBC (void);
// 0x000005AD System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::IsConnectedToBluetoothDevice(System.String)
extern void ConnectivityBluetoothMod_IsConnectedToBluetoothDevice_m11530B9E4F4C40168A3E4D2CF2BCD751AB62F271 (void);
// 0x000005AE Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::GetBluetoothDevice(System.String)
extern void ConnectivityBluetoothMod_GetBluetoothDevice_mA77D278BC1DE37BA832CE80144B3CE797D77AD93 (void);
// 0x000005AF Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::GetConnectedBluetoothDevice()
extern void ConnectivityBluetoothMod_GetConnectedBluetoothDevice_m1B77B59ECD00B43F4CEE1F98831D2D6606E235F4 (void);
// 0x000005B0 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_OnNewBluetoothDeviceScanned(System.String,System.String,System.Int32,System.Byte[])
extern void ConnectivityBluetoothMod__OnNewBluetoothDeviceScanned_mBDEFCE27A0B08B5649F753D11C36FEF651F8EF0B (void);
// 0x000005B1 System.Collections.IEnumerator Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_UpdateDevicesDictCoroutine()
extern void ConnectivityBluetoothMod__UpdateDevicesDictCoroutine_m66F58DEE4E1E18588B84E1A91177D6B4A3B5AB3A (void);
// 0x000005B2 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_UpdateDeviceDict()
extern void ConnectivityBluetoothMod__UpdateDeviceDict_mD8C0E3819B798DF8F3BDF27985F19B765605AC8E (void);
// 0x000005B3 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_OnBluetoothConnected(System.String)
extern void ConnectivityBluetoothMod__OnBluetoothConnected_m22BB245E9707F54F29232F95DB3DCF9444426742 (void);
// 0x000005B4 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_OnBluetoothDisconnected(System.String)
extern void ConnectivityBluetoothMod__OnBluetoothDisconnected_m9D822D2A877814CBD75ECC8BB1BA6EA5FDD378DE (void);
// 0x000005B5 System.Collections.IEnumerator Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_ConnectingTimeout()
extern void ConnectivityBluetoothMod__ConnectingTimeout_m0CB3327A7251A44E95C7F9E12BBD3527DFA327D3 (void);
// 0x000005B6 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_OnConnectionFailed(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
extern void ConnectivityBluetoothMod__OnConnectionFailed_mFAA621839FBE600C13B74EE4793DF240E2B850DB (void);
// 0x000005B7 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_SendMessage(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.Byte[])
extern void ConnectivityBluetoothMod__SendMessage_mCAB419CFF0F4EE829603993CD83AE8765E7FFE1A (void);
// 0x000005B8 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::_OnReceivedBluetooth(System.String,System.String,System.Byte[])
extern void ConnectivityBluetoothMod__OnReceivedBluetooth_m348644DAE04C35E5F10977EF772D060EF0841FD2 (void);
// 0x000005B9 System.String Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::FullUUID(System.String)
extern void ConnectivityBluetoothMod_FullUUID_mE67E757E51FBA978381D83655E8AE1D8D92A1FDB (void);
// 0x000005BA System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::IsEqual(System.String,System.String)
extern void ConnectivityBluetoothMod_IsEqual_mA4E679440930F6F9DFC5FDC4AC6E274A58311655 (void);
// 0x000005BB System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::.ctor()
extern void ConnectivityBluetoothMod__ctor_m599AC676CBF52448A3E76DC5A0E3D11824D99CED (void);
// 0x000005BC System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod::<UStart>b__16_1(System.String)
extern void ConnectivityBluetoothMod_U3CUStartU3Eb__16_1_m9C9B262B3B1F99276E4CFEA1E09ADD3D2ED7F77C (void);
// 0x000005BD System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions::.ctor(System.String,System.String)
extern void WrittingOptions__ctor_mFBA789066556201EC5179B222E5FBB01E5BF3A33 (void);
// 0x000005BE System.String Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions::get_ServiceUUID()
extern void WrittingOptions_get_ServiceUUID_m9FE5667A305B4990FB7E89C394389CE66B19205A (void);
// 0x000005BF System.String Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions::get_WrittingCharacteristicUUID()
extern void WrittingOptions_get_WrittingCharacteristicUUID_m6E4B8220C3658DB13C8B751B60426DFC07746F86 (void);
// 0x000005C0 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<>c::.cctor()
extern void U3CU3Ec__cctor_mD9BABA6D089A377FAE40F68CD590FA0DE3CD1D69 (void);
// 0x000005C1 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<>c::.ctor()
extern void U3CU3Ec__ctor_mFBA90D6A11A14AEAFDB089F058B8A4E5BBEE21BE (void);
// 0x000005C2 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<>c::<UStart>b__16_0()
extern void U3CU3Ec_U3CUStartU3Eb__16_0_m6F274CF11388BA11CE77B0E7076FDD2222BA81FA (void);
// 0x000005C3 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_UpdateDevicesDictCoroutine>d__29::.ctor(System.Int32)
extern void U3C_UpdateDevicesDictCoroutineU3Ed__29__ctor_m9F35222C1B4EE5B1CC1EFC81B95DBE984DD48E80 (void);
// 0x000005C4 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_UpdateDevicesDictCoroutine>d__29::System.IDisposable.Dispose()
extern void U3C_UpdateDevicesDictCoroutineU3Ed__29_System_IDisposable_Dispose_mEA6CAA59626221B0CA9E00CF330406F113C276BB (void);
// 0x000005C5 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_UpdateDevicesDictCoroutine>d__29::MoveNext()
extern void U3C_UpdateDevicesDictCoroutineU3Ed__29_MoveNext_m4171CBFDF126E9C333594B9B2E1F45D035B744D8 (void);
// 0x000005C6 System.Object Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_UpdateDevicesDictCoroutine>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_UpdateDevicesDictCoroutineU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD92BF00321587BFFAE3DD9745E96F27FA1A4537B (void);
// 0x000005C7 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_UpdateDevicesDictCoroutine>d__29::System.Collections.IEnumerator.Reset()
extern void U3C_UpdateDevicesDictCoroutineU3Ed__29_System_Collections_IEnumerator_Reset_mE9A8848F3FF141E5E8933D3E190711F213B0B380 (void);
// 0x000005C8 System.Object Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_UpdateDevicesDictCoroutine>d__29::System.Collections.IEnumerator.get_Current()
extern void U3C_UpdateDevicesDictCoroutineU3Ed__29_System_Collections_IEnumerator_get_Current_mFF452C4A0C4D18FDEE3C6111707FB42489FDE4DA (void);
// 0x000005C9 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_ConnectingTimeout>d__33::.ctor(System.Int32)
extern void U3C_ConnectingTimeoutU3Ed__33__ctor_m928E1438A517DD9CD041CFF2D45311684FF2DFA9 (void);
// 0x000005CA System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_ConnectingTimeout>d__33::System.IDisposable.Dispose()
extern void U3C_ConnectingTimeoutU3Ed__33_System_IDisposable_Dispose_m311FD9ACE79C3007C00B8C707550DA4318D9322D (void);
// 0x000005CB System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_ConnectingTimeout>d__33::MoveNext()
extern void U3C_ConnectingTimeoutU3Ed__33_MoveNext_m0A418B91F4FA6C41E58925A5338FE5FD1FB3DCF7 (void);
// 0x000005CC System.Object Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_ConnectingTimeout>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_ConnectingTimeoutU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE631ADC063FA57F92EFD0E2D9A69B87E5C9BE4F (void);
// 0x000005CD System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_ConnectingTimeout>d__33::System.Collections.IEnumerator.Reset()
extern void U3C_ConnectingTimeoutU3Ed__33_System_Collections_IEnumerator_Reset_mDC7A51226F8F03B37DF804E94AB4796D803B905C (void);
// 0x000005CE System.Object Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/<_ConnectingTimeout>d__33::System.Collections.IEnumerator.get_Current()
extern void U3C_ConnectingTimeoutU3Ed__33_System_Collections_IEnumerator_get_Current_m03A9B90356959758481729A957BF151A1E41538E (void);
// 0x000005CF System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::StartScanningBluetoothDeviceListAsync(System.String)
// 0x000005D0 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::StopScanningBluetoothDeviceList()
// 0x000005D1 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::IsScanningBluetoothDevices()
// 0x000005D2 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::ConnectToBluetoothDeviceAsync(System.String)
// 0x000005D3 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::DisconnectFromBluetoothDeviceAsync()
// 0x000005D4 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::SendMessageToConnectedBluetooth(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.String,System.Boolean)
// 0x000005D5 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::SendMessageToConnectedBluetooth(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.ConnectivityBluetoothMod/WrittingOptions,System.Byte[],System.Boolean)
// 0x000005D6 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::IsConnectedToAnyBluetoothDevice()
// 0x000005D7 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::IsConnectedToBluetoothDevice(System.String)
// 0x000005D8 Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::GetConnectedBluetoothDevice()
// 0x000005D9 Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core.IConnectivityBluetoothMod::GetBluetoothDevice(System.String)
// 0x000005DA System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::set_DeviceName(System.String)
extern void ConnectivityBluetoothDevice_set_DeviceName_mC9480657085DE7012C65F68C0EB568209E85BE5C (void);
// 0x000005DB System.String Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::get_DeviceName()
extern void ConnectivityBluetoothDevice_get_DeviceName_m669AEE4E40AFBFE95968796C6BEBD4A34219AB4B (void);
// 0x000005DC System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::set_DeviceAddress(System.String)
extern void ConnectivityBluetoothDevice_set_DeviceAddress_mDD064497D3272AE5EE4FB23E8F6A6535FC6665A4 (void);
// 0x000005DD System.String Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::get_DeviceAddress()
extern void ConnectivityBluetoothDevice_get_DeviceAddress_m6ABEEB67D017CA79644D12BA0D75A8BB956ED80C (void);
// 0x000005DE System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::set_TimeUpdated(System.Single)
extern void ConnectivityBluetoothDevice_set_TimeUpdated_m1603CD330D2A3CD27ADE800BF8A34E14C32ED709 (void);
// 0x000005DF System.Single Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::get_TimeUpdated()
extern void ConnectivityBluetoothDevice_get_TimeUpdated_m04E9E07770BBD593D13CF4DC3175DFBB19FA148B (void);
// 0x000005E0 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::.ctor(System.String,System.String,System.Single)
extern void ConnectivityBluetoothDevice__ctor_m563173B542AB4C5A28B926C77D2087D1889F77DC (void);
// 0x000005E1 System.Boolean Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::Equals(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice)
extern void ConnectivityBluetoothDevice_Equals_m9BD668F9D01419A834EE427989FF750C9D1F3FFE (void);
// 0x000005E2 System.Int32 Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice::CompareTo(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice)
extern void ConnectivityBluetoothDevice_CompareTo_mA72D02EA02698CF9D46E3B8CEDB6D1CE3BB6CFED (void);
// 0x000005E3 System.Void Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables::.ctor()
extern void ConnectivityVariables__ctor_mDEDA285F45649553E0B2AF6C2D3F98B6A00466B7 (void);
// 0x000005E4 System.Int32 NatCorder.GIFRecorder::get_pixelWidth()
extern void GIFRecorder_get_pixelWidth_m9BACC5B1EC0F95CC30CF9952E7B0A35EB8D76057 (void);
// 0x000005E5 System.Int32 NatCorder.GIFRecorder::get_pixelHeight()
extern void GIFRecorder_get_pixelHeight_mEE6A62FBA76BFFE3A83733A5E9FFD8258D4AFEFC (void);
// 0x000005E6 System.Void NatCorder.GIFRecorder::.ctor(System.Int32,System.Int32,System.Single,System.Action`1<System.String>)
extern void GIFRecorder__ctor_mCCCDFEE06E3452EA45B2BB6EEFA04F7812CFB69F (void);
// 0x000005E7 System.Void NatCorder.GIFRecorder::Dispose()
extern void GIFRecorder_Dispose_mEE1FD356D6E9FE80403343DDC2B04ED1DD5C127B (void);
// 0x000005E8 System.Void NatCorder.GIFRecorder::CommitFrame(T[],System.Int64)
// 0x000005E9 System.Void NatCorder.GIFRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void GIFRecorder_CommitFrame_m083F3F8E7632FD7D2A71921E50C5EEDA020211C5 (void);
// 0x000005EA System.Void NatCorder.GIFRecorder::CommitSamples(System.Single[],System.Int64)
extern void GIFRecorder_CommitSamples_mAC2388D005653E08572E6B54C3EFBA526CE103A3 (void);
// 0x000005EB System.Int32 NatCorder.HEVCRecorder::get_pixelWidth()
extern void HEVCRecorder_get_pixelWidth_mA79F8CCC89B197940532C072DFC9F2B2402422D1 (void);
// 0x000005EC System.Int32 NatCorder.HEVCRecorder::get_pixelHeight()
extern void HEVCRecorder_get_pixelHeight_mF1FF242F69209A2CBCDEBDA65E276FDB9937C825 (void);
// 0x000005ED System.Void NatCorder.HEVCRecorder::.ctor(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Action`1<System.String>,System.Int32,System.Int32)
extern void HEVCRecorder__ctor_m60B35AA9DF4647D4B885FFABF0CDA77C0AE37D2D (void);
// 0x000005EE System.Void NatCorder.HEVCRecorder::Dispose()
extern void HEVCRecorder_Dispose_m4C35637D90FFB1240982AF0BB3C7A1F04CCD22B5 (void);
// 0x000005EF System.Void NatCorder.HEVCRecorder::CommitFrame(T[],System.Int64)
// 0x000005F0 System.Void NatCorder.HEVCRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void HEVCRecorder_CommitFrame_m43CD654CDCDE1A367450B7F0791739658DD180F5 (void);
// 0x000005F1 System.Void NatCorder.HEVCRecorder::CommitSamples(System.Single[],System.Int64)
extern void HEVCRecorder_CommitSamples_m9A498A28FC77598EEDCD2F5F1E3D7CC7344BDB89 (void);
// 0x000005F2 System.Int32 NatCorder.IMediaRecorder::get_pixelWidth()
// 0x000005F3 System.Int32 NatCorder.IMediaRecorder::get_pixelHeight()
// 0x000005F4 System.Void NatCorder.IMediaRecorder::CommitFrame(T[],System.Int64)
// 0x000005F5 System.Void NatCorder.IMediaRecorder::CommitFrame(System.IntPtr,System.Int64)
// 0x000005F6 System.Void NatCorder.IMediaRecorder::CommitSamples(System.Single[],System.Int64)
// 0x000005F7 System.Int32 NatCorder.JPGRecorder::get_pixelWidth()
extern void JPGRecorder_get_pixelWidth_mFE5DC0E2D91FEDF6E7D217A76396480F4A4CB4C6 (void);
// 0x000005F8 System.Int32 NatCorder.JPGRecorder::get_pixelHeight()
extern void JPGRecorder_get_pixelHeight_m4E96F5B9217ED7BB9F33C818404F07B2CEF1AA8E (void);
// 0x000005F9 System.Void NatCorder.JPGRecorder::.ctor(System.Int32,System.Int32,System.Action`1<System.String>)
extern void JPGRecorder__ctor_m68925C076D07B49B8889A0E8174690A0EE45FDBE (void);
// 0x000005FA System.Void NatCorder.JPGRecorder::Dispose()
extern void JPGRecorder_Dispose_mF525A4D90BF6F36055C9DD56488B69990F184985 (void);
// 0x000005FB System.Void NatCorder.JPGRecorder::CommitFrame(T[],System.Int64)
// 0x000005FC System.Void NatCorder.JPGRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void JPGRecorder_CommitFrame_mE291CF99B2219375E18331B5DC0B8352000E68C5 (void);
// 0x000005FD System.Void NatCorder.JPGRecorder::CommitSamples(System.Single[],System.Int64)
extern void JPGRecorder_CommitSamples_mB72B6F564ED34442DD6FF728DED9BA62FA14F86D (void);
// 0x000005FE System.Void NatCorder.JPGRecorder/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m836680AD1E908D253A5E4A1699FF6C116748AA74 (void);
// 0x000005FF System.Void NatCorder.JPGRecorder/<>c__DisplayClass4_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mFD00057C06CC757D7BF28073B511D368529B9209 (void);
// 0x00000600 System.Void NatCorder.JPGRecorder/<>c__DisplayClass4_0::<.ctor>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__1_m5FAC3F1BC07E7620CA0F614C66FDDDBA1D505BF6 (void);
// 0x00000601 System.Int32 NatCorder.MP4Recorder::get_pixelWidth()
extern void MP4Recorder_get_pixelWidth_mAF074D9DDD74EEF9255E84BA12763816B609C1BC (void);
// 0x00000602 System.Int32 NatCorder.MP4Recorder::get_pixelHeight()
extern void MP4Recorder_get_pixelHeight_mE7C72A66415850352A37C3918ACA7AB8B93D98B1 (void);
// 0x00000603 System.Void NatCorder.MP4Recorder::.ctor(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Action`1<System.String>,System.Int32,System.Int32)
extern void MP4Recorder__ctor_mCB79216EB6F12DE47C8B105E24C97BBAE038DEB6 (void);
// 0x00000604 System.Void NatCorder.MP4Recorder::Dispose()
extern void MP4Recorder_Dispose_m017663B9B455170ED943D118A978FF8E7E14C31D (void);
// 0x00000605 System.Void NatCorder.MP4Recorder::CommitFrame(T[],System.Int64)
// 0x00000606 System.Void NatCorder.MP4Recorder::CommitFrame(System.IntPtr,System.Int64)
extern void MP4Recorder_CommitFrame_m16AAE4E064C24A088AE4DF8F47D815AE0C8825D0 (void);
// 0x00000607 System.Void NatCorder.MP4Recorder::CommitSamples(System.Single[],System.Int64)
extern void MP4Recorder_CommitSamples_mAC7DB615D4F1D5DB0D291084DE8D49BE43EE8257 (void);
// 0x00000608 System.Int32 NatCorder.Internal.MediaRecorderAndroid::get_pixelWidth()
extern void MediaRecorderAndroid_get_pixelWidth_m0E23E101BD19DC44818C4FE0E8053A0641920C97 (void);
// 0x00000609 System.Void NatCorder.Internal.MediaRecorderAndroid::set_pixelWidth(System.Int32)
extern void MediaRecorderAndroid_set_pixelWidth_m20A15A638F55F9E73B4346B540328CFF6BAAEDBB (void);
// 0x0000060A System.Int32 NatCorder.Internal.MediaRecorderAndroid::get_pixelHeight()
extern void MediaRecorderAndroid_get_pixelHeight_m24C3F12DC6839881F72FD264CB8289DB6C6E95FA (void);
// 0x0000060B System.Void NatCorder.Internal.MediaRecorderAndroid::set_pixelHeight(System.Int32)
extern void MediaRecorderAndroid_set_pixelHeight_m7A2BF60FF8D05304B9FCAA84E8DEE1464B431510 (void);
// 0x0000060C System.Void NatCorder.Internal.MediaRecorderAndroid::.ctor(UnityEngine.AndroidJavaObject,System.Int32,System.Int32,System.String,System.Action`1<System.String>)
extern void MediaRecorderAndroid__ctor_mE6FB9F2711DC2287519C848A21130ECD41E8679E (void);
// 0x0000060D System.Void NatCorder.Internal.MediaRecorderAndroid::Dispose()
extern void MediaRecorderAndroid_Dispose_mB57F5D9C990072BFFB57497CE286A31E964D2A06 (void);
// 0x0000060E System.Void NatCorder.Internal.MediaRecorderAndroid::CommitFrame(T[],System.Int64)
// 0x0000060F System.Void NatCorder.Internal.MediaRecorderAndroid::CommitFrame(System.IntPtr,System.Int64)
extern void MediaRecorderAndroid_CommitFrame_mC7D62685D5F820BC11419B56B77A39D529D275D7 (void);
// 0x00000610 System.Void NatCorder.Internal.MediaRecorderAndroid::CommitSamples(System.Single[],System.Int64)
extern void MediaRecorderAndroid_CommitSamples_m2F689E28142A770D98AFF33F952430255E58D3C9 (void);
// 0x00000611 System.Void NatCorder.Internal.MediaRecorderAndroid::onRecording(System.String)
extern void MediaRecorderAndroid_onRecording_m7045DD6231BD95C434BE9337C52C60B9A1EA4C25 (void);
// 0x00000612 System.IntPtr NatCorder.Internal.MediaRecorderBridge::CreateMP4Recorder(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void MediaRecorderBridge_CreateMP4Recorder_m58A6C1F7018A908181A2142385E776E11F38642C (void);
// 0x00000613 System.IntPtr NatCorder.Internal.MediaRecorderBridge::CreateHEVCRecorder(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void MediaRecorderBridge_CreateHEVCRecorder_mFE25374D7A4C2807A3EB98FD7C9DD89C5FEF30F0 (void);
// 0x00000614 System.IntPtr NatCorder.Internal.MediaRecorderBridge::CreateGIFRecorder(System.Int32,System.Int32,System.Single)
extern void MediaRecorderBridge_CreateGIFRecorder_m4148146363D13169CED1E5C7375B57DABD9AB1D8 (void);
// 0x00000615 System.Void NatCorder.Internal.MediaRecorderBridge::StartRecording(System.IntPtr,System.String,System.Action`2<System.IntPtr,System.IntPtr>,System.IntPtr)
extern void MediaRecorderBridge_StartRecording_m1F25D2D0ECB5208E825948736E1ABC7C38C4120A (void);
// 0x00000616 System.Void NatCorder.Internal.MediaRecorderBridge::StopRecording(System.IntPtr)
extern void MediaRecorderBridge_StopRecording_mC855C6DD8199E21AAD124FBA059EC8806BA38954 (void);
// 0x00000617 System.Void NatCorder.Internal.MediaRecorderBridge::EncodeFrame(System.IntPtr,System.IntPtr,System.Int64)
extern void MediaRecorderBridge_EncodeFrame_m619ED14A5CC1077420461B57F3CC49944F4E04C0 (void);
// 0x00000618 System.Void NatCorder.Internal.MediaRecorderBridge::EncodeSamples(System.IntPtr,System.Single[],System.Int32,System.Int64)
extern void MediaRecorderBridge_EncodeSamples_m9D38D08F94BA38407BB3F5FA1B1A18E8492E3596 (void);
// 0x00000619 System.Void NatCorder.Internal.IDispatcher::Dispatch(System.Action)
// 0x0000061A System.Void NatCorder.Internal.MainDispatcher::.ctor()
extern void MainDispatcher__ctor_mE0DDCD632F93E1D69C359FCF5D8A90AE6BC10F47 (void);
// 0x0000061B System.Void NatCorder.Internal.MainDispatcher::Dispose()
extern void MainDispatcher_Dispose_mF89D36DDE3EDD7E803EE1EC4AACE9A9B2EE2B5E5 (void);
// 0x0000061C System.Void NatCorder.Internal.MainDispatcher::Dispatch(System.Action)
extern void MainDispatcher_Dispatch_mBFC12264784571E0C4D075C1D9D8E0711A4C281E (void);
// 0x0000061D System.Collections.IEnumerator NatCorder.Internal.MainDispatcher::Dispatch()
extern void MainDispatcher_Dispatch_mA2DB3927DDE0FD114D65DA997AF3AD7AAD9B7394 (void);
// 0x0000061E System.Void NatCorder.Internal.MainDispatcher::<Dispose>b__3_0()
extern void MainDispatcher_U3CDisposeU3Eb__3_0_mB21B7CE05AD414BF3EB0D16DE904997CAD0B87CA (void);
// 0x0000061F System.Void NatCorder.Internal.MainDispatcher/MainDispatcherAttachment::.ctor()
extern void MainDispatcherAttachment__ctor_mC3B6A643408DC00EB2B967920A241160B924255F (void);
// 0x00000620 System.Void NatCorder.Internal.MainDispatcher/<Dispatch>d__5::.ctor(System.Int32)
extern void U3CDispatchU3Ed__5__ctor_mA30F123D252463D1C5B7EAE5BF2FC52D206C7C36 (void);
// 0x00000621 System.Void NatCorder.Internal.MainDispatcher/<Dispatch>d__5::System.IDisposable.Dispose()
extern void U3CDispatchU3Ed__5_System_IDisposable_Dispose_m23814C2E32D13F9A865F68857ECB5408F7F6D84C (void);
// 0x00000622 System.Boolean NatCorder.Internal.MainDispatcher/<Dispatch>d__5::MoveNext()
extern void U3CDispatchU3Ed__5_MoveNext_mFEEDFDFED9525D58EBE889622465488F76001A05 (void);
// 0x00000623 System.Object NatCorder.Internal.MainDispatcher/<Dispatch>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDispatchU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7466FE9F0ED9E13B00AB02A5DFA1C1EC9D79CE74 (void);
// 0x00000624 System.Void NatCorder.Internal.MainDispatcher/<Dispatch>d__5::System.Collections.IEnumerator.Reset()
extern void U3CDispatchU3Ed__5_System_Collections_IEnumerator_Reset_mA2DC9EFA5BB726718062FB492421042D547CAEB9 (void);
// 0x00000625 System.Object NatCorder.Internal.MainDispatcher/<Dispatch>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CDispatchU3Ed__5_System_Collections_IEnumerator_get_Current_m60AB35AB3AAE9A9647A9B2A94EB962DDCB0C39F9 (void);
// 0x00000626 System.Void NatCorder.Internal.RenderDispatcher::Dispose()
extern void RenderDispatcher_Dispose_m3BB88176F76AAE5E9E244634BC601D69C97701E6 (void);
// 0x00000627 System.Void NatCorder.Internal.RenderDispatcher::Dispatch(System.Action)
extern void RenderDispatcher_Dispatch_mFFBC9D2F2402DD16B8719459733425820837EAC8 (void);
// 0x00000628 System.Void NatCorder.Internal.RenderDispatcher::DequeueRender(System.Int32)
extern void RenderDispatcher_DequeueRender_mEFFD3475E3F871614C267EA217B6342B76758068 (void);
// 0x00000629 System.Void NatCorder.Internal.RenderDispatcher::.ctor()
extern void RenderDispatcher__ctor_mEC3DC5C2BC4351A1E3FDF1AA7E4EBD5275251B8A (void);
// 0x0000062A System.Void NatCorder.Internal.RenderDispatcher/UnityRenderingEvent::.ctor(System.Object,System.IntPtr)
extern void UnityRenderingEvent__ctor_m2853E29B957CD11906BCD10FA826BB1FF23F902A (void);
// 0x0000062B System.Void NatCorder.Internal.RenderDispatcher/UnityRenderingEvent::Invoke(System.Int32)
extern void UnityRenderingEvent_Invoke_m0F12D4E8F72E45DEF43A8D8A4A1E2E9A4DB3F5F6 (void);
// 0x0000062C System.IAsyncResult NatCorder.Internal.RenderDispatcher/UnityRenderingEvent::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void UnityRenderingEvent_BeginInvoke_m5A08DE6D56DEAAAEA7F2279CE1A8595C0B85887F (void);
// 0x0000062D System.Void NatCorder.Internal.RenderDispatcher/UnityRenderingEvent::EndInvoke(System.IAsyncResult)
extern void UnityRenderingEvent_EndInvoke_mE3E71FD57414876051ADB9ED30E26D7B86BA52CC (void);
// 0x0000062E System.Void NatCorder.Internal.DocAttribute::.ctor(System.String)
extern void DocAttribute__ctor_mBC82A60195829F2472D17A6C4CABCFEF3DFBBB5F (void);
// 0x0000062F System.Void NatCorder.Internal.DocAttribute::.ctor(System.String,System.String)
extern void DocAttribute__ctor_m4525DD9B6D7AD8F9DDF75D215A69EF62460EA994 (void);
// 0x00000630 System.Void NatCorder.Internal.CodeAttribute::.ctor(System.String)
extern void CodeAttribute__ctor_m34351EB38EA83AA3881F57FAD8A5E2E76C0C6E5D (void);
// 0x00000631 System.Void NatCorder.Internal.RefAttribute::.ctor(System.String[])
extern void RefAttribute__ctor_mC1A02BA2DF07E77D619DB8B81F9A5F0CC324F56C (void);
// 0x00000632 System.Int32 NatCorder.Internal.MediaRecorderiOS::get_pixelWidth()
extern void MediaRecorderiOS_get_pixelWidth_m4780C0C24E7980C65EFD7E2D89F59F9F3ED95CB4 (void);
// 0x00000633 System.Void NatCorder.Internal.MediaRecorderiOS::set_pixelWidth(System.Int32)
extern void MediaRecorderiOS_set_pixelWidth_mF4499037A49E41164D79AB73147725B5CE9FE6D3 (void);
// 0x00000634 System.Int32 NatCorder.Internal.MediaRecorderiOS::get_pixelHeight()
extern void MediaRecorderiOS_get_pixelHeight_mA60D74F64CC0C20CBFFA45478C1942E222EFAC6C (void);
// 0x00000635 System.Void NatCorder.Internal.MediaRecorderiOS::set_pixelHeight(System.Int32)
extern void MediaRecorderiOS_set_pixelHeight_mF5FAB82E599F910E07D4E743267CEB07ADCBDAFB (void);
// 0x00000636 System.Void NatCorder.Internal.MediaRecorderiOS::.ctor(System.IntPtr,System.Int32,System.Int32,System.String,System.Action`1<System.String>)
extern void MediaRecorderiOS__ctor_mD371B35454380FA80A028B8A4101273B4A3EE3EF (void);
// 0x00000637 System.Void NatCorder.Internal.MediaRecorderiOS::Dispose()
extern void MediaRecorderiOS_Dispose_m4D9ECC93697DEB592A632718E3BBE3125FCD9160 (void);
// 0x00000638 System.Void NatCorder.Internal.MediaRecorderiOS::CommitFrame(T[],System.Int64)
// 0x00000639 System.Void NatCorder.Internal.MediaRecorderiOS::CommitFrame(System.IntPtr,System.Int64)
extern void MediaRecorderiOS_CommitFrame_mD382263D5BDABCFCDAA10646D557CE908EF26FDB (void);
// 0x0000063A System.Void NatCorder.Internal.MediaRecorderiOS::CommitSamples(System.Single[],System.Int64)
extern void MediaRecorderiOS_CommitSamples_m81CA1B084B8E5132D98EB7356F6B7B5082125926 (void);
// 0x0000063B System.Void NatCorder.Internal.MediaRecorderiOS::OnRecording(System.IntPtr,System.IntPtr)
extern void MediaRecorderiOS_OnRecording_mE56D0FE0CEB3ADDB92F9346A3DCDAAF7F7F1C82B (void);
// 0x0000063C System.Void NatCorder.Internal.MediaRecorderiOS/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m21470B24BC9D1F1F63E117F442547FDEDCB39774 (void);
// 0x0000063D System.Void NatCorder.Internal.MediaRecorderiOS/<>c__DisplayClass16_0::<OnRecording>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3COnRecordingU3Eb__0_mD9C2D36A7D6327ACEA90B9B19685D6A9B21B6FA7 (void);
// 0x0000063E System.Void NatCorder.Internal.ReadableTexture::.ctor(UnityEngine.RenderTexture)
extern void ReadableTexture__ctor_m5850062F78C931AA622C414E6E8C4A270D354694 (void);
// 0x0000063F System.Void NatCorder.Internal.ReadableTexture::Dispose()
// 0x00000640 System.Void NatCorder.Internal.ReadableTexture::Readback(System.Action`1<System.IntPtr>)
// 0x00000641 UnityEngine.RenderTexture NatCorder.Internal.ReadableTexture::op_Implicit(NatCorder.Internal.ReadableTexture)
extern void ReadableTexture_op_Implicit_mBD7C376F81F9640007ED1B4626EADD60026F75C9 (void);
// 0x00000642 NatCorder.Internal.ReadableTexture NatCorder.Internal.ReadableTexture::ToReadable(UnityEngine.RenderTexture)
extern void ReadableTexture_ToReadable_m2EA76694B861E20A30EA150F6623878B84702D1B (void);
// 0x00000643 System.Void NatCorder.Internal.Readback.SyncReadableTexture::.ctor(UnityEngine.RenderTexture)
extern void SyncReadableTexture__ctor_m2B7A5898B9FF5180D7D4D86B8CBB4D014B9AC8CC (void);
// 0x00000644 System.Void NatCorder.Internal.Readback.SyncReadableTexture::Dispose()
extern void SyncReadableTexture_Dispose_mA6758F17642BF12A421E4B6EB429F573138EF315 (void);
// 0x00000645 System.Void NatCorder.Internal.Readback.SyncReadableTexture::Readback(System.Action`1<System.IntPtr>)
extern void SyncReadableTexture_Readback_mD4CA026B7A01E2B95176A6978E588D2462F502B8 (void);
// 0x00000646 System.Void NatCorder.Internal.Readback.AsyncReadableTexture::.ctor(UnityEngine.RenderTexture)
extern void AsyncReadableTexture__ctor_m046C0476F150F3EC21BDA5E7701AB4472F37E0BE (void);
// 0x00000647 System.Void NatCorder.Internal.Readback.AsyncReadableTexture::Dispose()
extern void AsyncReadableTexture_Dispose_m426EB6F7C55DF2A12F249C09AB56A2AE560B0508 (void);
// 0x00000648 System.Void NatCorder.Internal.Readback.AsyncReadableTexture::Readback(System.Action`1<System.IntPtr>)
extern void AsyncReadableTexture_Readback_m10412E74072CE54D6FD28CBFB492320F2690DDF3 (void);
// 0x00000649 System.Void NatCorder.Internal.Readback.AsyncReadableTexture/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mCFF2986C3CBC52BF7ADCAC742F7B1BB23685D581 (void);
// 0x0000064A System.Void NatCorder.Internal.Readback.AsyncReadableTexture/<>c__DisplayClass2_0::<Readback>b__0(UnityEngine.Rendering.AsyncGPUReadbackRequest)
extern void U3CU3Ec__DisplayClass2_0_U3CReadbackU3Eb__0_mDA4D7E7EA7AD4E4B0F1BBF61195A1FEBC2ED57EC (void);
// 0x0000064B System.Void NatCorder.Internal.Readback.GLESReadableTexture::.ctor(UnityEngine.RenderTexture)
extern void GLESReadableTexture__ctor_m196DB4300B4CD96AD6057AC0BB0C996F95716811 (void);
// 0x0000064C System.Void NatCorder.Internal.Readback.GLESReadableTexture::Dispose()
extern void GLESReadableTexture_Dispose_mF260A297CC4D02A6BCC30254B34C917B679FEDCB (void);
// 0x0000064D System.Void NatCorder.Internal.Readback.GLESReadableTexture::Readback(System.Action`1<System.IntPtr>)
extern void GLESReadableTexture_Readback_m620FB940BC720BFB5F23945547C2B3DFD4E887A4 (void);
// 0x0000064E System.Void NatCorder.Internal.Readback.GLESReadableTexture::<.ctor>b__0_0(System.Int64,UnityEngine.AndroidJavaObject)
extern void GLESReadableTexture_U3C_ctorU3Eb__0_0_m4A864B01073354DACAAEBC56388EAF70F2958AA8 (void);
// 0x0000064F System.Void NatCorder.Internal.Readback.GLESReadableTexture/Callback::.ctor(System.Action`2<System.Int64,UnityEngine.AndroidJavaObject>)
extern void Callback__ctor_mD130616C7015ED5193A197E080108DF86D69EBBF (void);
// 0x00000650 System.Void NatCorder.Internal.Readback.GLESReadableTexture/Callback::onReadback(System.Int64,UnityEngine.AndroidJavaObject)
extern void Callback_onReadback_m18AFD33BCB5CB9463C58D6EEA948B7297D00E785 (void);
// 0x00000651 System.Void NatCorder.Internal.Readback.GLESReadableTexture/<>c::.cctor()
extern void U3CU3Ec__cctor_mE3DE84F7A001560AB3A3B19BF81159316048A07D (void);
// 0x00000652 System.Void NatCorder.Internal.Readback.GLESReadableTexture/<>c::.ctor()
extern void U3CU3Ec__ctor_m2C7C5375F4DC925A731601B6E42111CA7F9B96DD (void);
// 0x00000653 System.Void NatCorder.Internal.Readback.GLESReadableTexture/<>c::<.ctor>b__0_1()
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_m753A83CC4C54BDCAFF428F3E0324479AD3CF56A4 (void);
// 0x00000654 System.Void NatCorder.Internal.Readback.GLESReadableTexture/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m79DF2E9C30EED79E92D318D28A818FF9264A838C (void);
// 0x00000655 System.Void NatCorder.Internal.Readback.GLESReadableTexture/<>c__DisplayClass2_0::<Readback>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CReadbackU3Eb__0_mBC36D143E4D108834471126145A420E712EF9AAD (void);
// 0x00000656 System.Void NatCorder.Inputs.AudioInput::.ctor(NatCorder.IMediaRecorder,NatCorder.Clocks.IClock,UnityEngine.AudioListener)
extern void AudioInput__ctor_m25269E64AF348B8C5DC4FF7726675C61F87E0552 (void);
// 0x00000657 System.Void NatCorder.Inputs.AudioInput::.ctor(NatCorder.IMediaRecorder,NatCorder.Clocks.IClock,UnityEngine.AudioSource,System.Boolean)
extern void AudioInput__ctor_m21EB345A2B01E9C96299ACA2182DB698A5BCDEB9 (void);
// 0x00000658 System.Void NatCorder.Inputs.AudioInput::Dispose()
extern void AudioInput_Dispose_mD5BA89EB97D9BDCFDB3FB337AF453CCFEDB3906D (void);
// 0x00000659 System.Void NatCorder.Inputs.AudioInput::OnSampleBuffer(System.Single[])
extern void AudioInput_OnSampleBuffer_m8369B434029C6D28AC7A4B1FB335FCBB745AC462 (void);
// 0x0000065A System.Void NatCorder.Inputs.AudioInput/AudioInputAttachment::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioInputAttachment_OnAudioFilterRead_m77DE56234BCDD8101CBA924807F8FF306F9F58A0 (void);
// 0x0000065B System.Void NatCorder.Inputs.AudioInput/AudioInputAttachment::.ctor()
extern void AudioInputAttachment__ctor_m4DE723266ACFA80DF3D0177F4D55CD2D6CED2705 (void);
// 0x0000065C System.Void NatCorder.Inputs.CameraInput::.ctor(NatCorder.IMediaRecorder,NatCorder.Clocks.IClock,UnityEngine.Camera[])
extern void CameraInput__ctor_m376BFFB128C731067BEDDB7B9FC2446BE9A29FE5 (void);
// 0x0000065D System.Void NatCorder.Inputs.CameraInput::Dispose()
extern void CameraInput_Dispose_mD8812F99A4F961A0345344BA51E3D72A23C7422C (void);
// 0x0000065E System.Collections.IEnumerator NatCorder.Inputs.CameraInput::OnFrame()
extern void CameraInput_OnFrame_mFAA93E723FD2ADA7F4E71210D4D5F1D50502D960 (void);
// 0x0000065F System.Void NatCorder.Inputs.CameraInput/CameraInputAttachment::.ctor()
extern void CameraInputAttachment__ctor_m43E111D84719E8E36D65DBD960079DB3A85321A0 (void);
// 0x00000660 System.Void NatCorder.Inputs.CameraInput/<>c::.cctor()
extern void U3CU3Ec__cctor_m80357168A84276574A6935C27CD9D63E0A299C98 (void);
// 0x00000661 System.Void NatCorder.Inputs.CameraInput/<>c::.ctor()
extern void U3CU3Ec__ctor_m8037E2EFD8C8DB296B18C97860F45C4BBFCF2942 (void);
// 0x00000662 System.Int32 NatCorder.Inputs.CameraInput/<>c::<.ctor>b__1_0(UnityEngine.Camera,UnityEngine.Camera)
extern void U3CU3Ec_U3C_ctorU3Eb__1_0_m283396D7EFBF662235B11F77A140F1B13D12A6B1 (void);
// 0x00000663 System.Void NatCorder.Inputs.CameraInput/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m9E170F06ED28EC21EA5DF1B2B2FDC31ED0F498D8 (void);
// 0x00000664 System.Void NatCorder.Inputs.CameraInput/<>c__DisplayClass9_0::<OnFrame>b__0(System.IntPtr)
extern void U3CU3Ec__DisplayClass9_0_U3COnFrameU3Eb__0_mD35D5D5D4F10A43B0968AD7AB39F6465C1AB5BF1 (void);
// 0x00000665 System.Void NatCorder.Inputs.CameraInput/<OnFrame>d__9::.ctor(System.Int32)
extern void U3COnFrameU3Ed__9__ctor_mF19EBABAE8E9F0046C94AB4943D6E08318DE47C0 (void);
// 0x00000666 System.Void NatCorder.Inputs.CameraInput/<OnFrame>d__9::System.IDisposable.Dispose()
extern void U3COnFrameU3Ed__9_System_IDisposable_Dispose_m363610CACB70E6B6DAEBEB7EF97675F66C31393D (void);
// 0x00000667 System.Boolean NatCorder.Inputs.CameraInput/<OnFrame>d__9::MoveNext()
extern void U3COnFrameU3Ed__9_MoveNext_m515C848F2489BAA470002045F68906F2684D6767 (void);
// 0x00000668 System.Object NatCorder.Inputs.CameraInput/<OnFrame>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnFrameU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93B13B9770A1E119715908B5291E37A8F89AEEDE (void);
// 0x00000669 System.Void NatCorder.Inputs.CameraInput/<OnFrame>d__9::System.Collections.IEnumerator.Reset()
extern void U3COnFrameU3Ed__9_System_Collections_IEnumerator_Reset_m0DA41DF41004E2B0ED40E774958E382773183696 (void);
// 0x0000066A System.Object NatCorder.Inputs.CameraInput/<OnFrame>d__9::System.Collections.IEnumerator.get_Current()
extern void U3COnFrameU3Ed__9_System_Collections_IEnumerator_get_Current_m328D113DC66B0B7C486C7B7A400C9F732483E220 (void);
// 0x0000066B System.Double NatCorder.Clocks.FixedIntervalClock::get_Interval()
extern void FixedIntervalClock_get_Interval_mBB76A5A9122DB9C81BF38ED7A21B69293BEECAC8 (void);
// 0x0000066C System.Void NatCorder.Clocks.FixedIntervalClock::set_Interval(System.Double)
extern void FixedIntervalClock_set_Interval_m040BECACD7031410D74E3F031A6F1B9C1AA25338 (void);
// 0x0000066D System.Int64 NatCorder.Clocks.FixedIntervalClock::get_Timestamp()
extern void FixedIntervalClock_get_Timestamp_m050A3A243CE7F1972688E30A246AE9A08C995984 (void);
// 0x0000066E System.Void NatCorder.Clocks.FixedIntervalClock::.ctor(System.Int32,System.Boolean)
extern void FixedIntervalClock__ctor_m802625172681F6292BD3988A3260407BB5F7E481 (void);
// 0x0000066F System.Void NatCorder.Clocks.FixedIntervalClock::.ctor(System.Double,System.Boolean)
extern void FixedIntervalClock__ctor_mCC9A1FB9012428D73BF5ADC57F565A083DD12869 (void);
// 0x00000670 System.Void NatCorder.Clocks.FixedIntervalClock::Tick()
extern void FixedIntervalClock_Tick_m342D5598E31A4C3690B345D73086A7361F7A6F4A (void);
// 0x00000671 System.Int64 NatCorder.Clocks.IClock::get_Timestamp()
// 0x00000672 System.Int64 NatCorder.Clocks.RealtimeClock::get_Timestamp()
extern void RealtimeClock_get_Timestamp_m30135424830FF8FB4D7314951C156C9AD0DBA035 (void);
// 0x00000673 System.Boolean NatCorder.Clocks.RealtimeClock::get_Paused()
extern void RealtimeClock_get_Paused_m396C02C9576E0579A2872FC7D717BC36BA5F72B6 (void);
// 0x00000674 System.Void NatCorder.Clocks.RealtimeClock::set_Paused(System.Boolean)
extern void RealtimeClock_set_Paused_mCBB8426AC777F406E210693B8ED14A40FEEB58A4 (void);
// 0x00000675 System.Void NatCorder.Clocks.RealtimeClock::.ctor()
extern void RealtimeClock__ctor_m6DA82146AABEE26E0F84F52993FDE838DCE60D9D (void);
// 0x00000676 System.Void NatCorder.Examples.Giffy::StartRecording()
extern void Giffy_StartRecording_m20CDDD22A37E21F18D149A5600E2DA086AE1266C (void);
// 0x00000677 System.Void NatCorder.Examples.Giffy::StopRecording()
extern void Giffy_StopRecording_m66489FCC5A4747044EAC1E9FE473F41841B18421 (void);
// 0x00000678 System.Void NatCorder.Examples.Giffy::OnGIF(System.String)
extern void Giffy_OnGIF_m8F7AF2803F5BC3951AC959A4240F93ADD40B9792 (void);
// 0x00000679 System.Void NatCorder.Examples.Giffy::.ctor()
extern void Giffy__ctor_m1306C6996698E6931A35429F38723B5187AA6161 (void);
// 0x0000067A UnityEngine.WebCamTexture NatCorder.Examples.CameraPreview::get_cameraTexture()
extern void CameraPreview_get_cameraTexture_m6A50043A8AE306D1A0DC813D0883416777E9913D (void);
// 0x0000067B System.Void NatCorder.Examples.CameraPreview::set_cameraTexture(UnityEngine.WebCamTexture)
extern void CameraPreview_set_cameraTexture_mD53551045B0ADE714163EAFAB77C402D390E1C0D (void);
// 0x0000067C System.Collections.IEnumerator NatCorder.Examples.CameraPreview::Start()
extern void CameraPreview_Start_m97AD7612CD33BCC3D74F43A1BDEBA56740CFA3E7 (void);
// 0x0000067D System.Void NatCorder.Examples.CameraPreview::.ctor()
extern void CameraPreview__ctor_m27F007C9E0AECDAED14DBE234E9B37D49D2058CF (void);
// 0x0000067E System.Boolean NatCorder.Examples.CameraPreview::<Start>b__6_0()
extern void CameraPreview_U3CStartU3Eb__6_0_mCD40B5BC39952B4C3CBC7C8B97D354C2423184B7 (void);
// 0x0000067F System.Void NatCorder.Examples.CameraPreview/<Start>d__6::.ctor(System.Int32)
extern void U3CStartU3Ed__6__ctor_m61BEBDA2E46AC32151C858C7F53C9A914ACEE5E1 (void);
// 0x00000680 System.Void NatCorder.Examples.CameraPreview/<Start>d__6::System.IDisposable.Dispose()
extern void U3CStartU3Ed__6_System_IDisposable_Dispose_mAF75455F3F2B57699528B948BDF2C95447D1A159 (void);
// 0x00000681 System.Boolean NatCorder.Examples.CameraPreview/<Start>d__6::MoveNext()
extern void U3CStartU3Ed__6_MoveNext_m2CA9F69736E0E748B0DE20F3410D3138C0A2CD24 (void);
// 0x00000682 System.Object NatCorder.Examples.CameraPreview/<Start>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m084DE236D6A9FE77E3A15E7980A94F736A28BECF (void);
// 0x00000683 System.Void NatCorder.Examples.CameraPreview/<Start>d__6::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_m94B9B92CAC65CFD019E52EB2ABAC56C0FB8CC802 (void);
// 0x00000684 System.Object NatCorder.Examples.CameraPreview/<Start>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_m67880265D537388330521F8F1D4633FDCBF36A3C (void);
// 0x00000685 System.Void NatCorder.Examples.RecordButton::Start()
extern void RecordButton_Start_mD5668CFD5A334CB4ADCB5CAA3CECAD557703F938 (void);
// 0x00000686 System.Void NatCorder.Examples.RecordButton::Reset()
extern void RecordButton_Reset_mAA277DB1180D42D192A2183164B547B5CF1B558E (void);
// 0x00000687 System.Void NatCorder.Examples.RecordButton::UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void RecordButton_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_mF943B91B92F4E59868646EE573A5A1997CC43F5E (void);
// 0x00000688 System.Void NatCorder.Examples.RecordButton::UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void RecordButton_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m23CFF22D04CAE280229A16400A813D6C5E541A00 (void);
// 0x00000689 System.Collections.IEnumerator NatCorder.Examples.RecordButton::Countdown()
extern void RecordButton_Countdown_m58C67C3558679FA5D607E04F53AC6427525E0F06 (void);
// 0x0000068A System.Void NatCorder.Examples.RecordButton::.ctor()
extern void RecordButton__ctor_mD1F4AD2CC2FB8A5A830074A6B58EC0E86CE929C4 (void);
// 0x0000068B System.Void NatCorder.Examples.RecordButton/<Countdown>d__10::.ctor(System.Int32)
extern void U3CCountdownU3Ed__10__ctor_m7FD8BC0A80F9462FEE9E5F5CF1F5A86A2486AD29 (void);
// 0x0000068C System.Void NatCorder.Examples.RecordButton/<Countdown>d__10::System.IDisposable.Dispose()
extern void U3CCountdownU3Ed__10_System_IDisposable_Dispose_mD4E0A17948270F69CD6F9E4E40FF6B5D60A2DA9F (void);
// 0x0000068D System.Boolean NatCorder.Examples.RecordButton/<Countdown>d__10::MoveNext()
extern void U3CCountdownU3Ed__10_MoveNext_mE0158E4A4CD062F2789CA9F77D25FF45BF960E7D (void);
// 0x0000068E System.Object NatCorder.Examples.RecordButton/<Countdown>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountdownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05EA6877C6ECE68D7AD3613C1C06E21F3DCBA3DD (void);
// 0x0000068F System.Void NatCorder.Examples.RecordButton/<Countdown>d__10::System.Collections.IEnumerator.Reset()
extern void U3CCountdownU3Ed__10_System_Collections_IEnumerator_Reset_m84AA071F5BB96BD17066B96313F48A1FEC52E1DE (void);
// 0x00000690 System.Object NatCorder.Examples.RecordButton/<Countdown>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CCountdownU3Ed__10_System_Collections_IEnumerator_get_Current_m03F6543DFBF0F712D8AA73F7254E0C48F02681E5 (void);
// 0x00000691 System.Void NatCorder.Examples.ReplayCam::StartRecording()
extern void ReplayCam_StartRecording_m4D5BE9397F4BB95DF0580D7BD97628F4A1F2BA10 (void);
// 0x00000692 System.Void NatCorder.Examples.ReplayCam::StartMicrophone()
extern void ReplayCam_StartMicrophone_m875D22D40FAEE23453E49D8734C88A58462FDD72 (void);
// 0x00000693 System.Void NatCorder.Examples.ReplayCam::StopRecording()
extern void ReplayCam_StopRecording_mD36D259861DAB3F8DED157D52810A05065283578 (void);
// 0x00000694 System.Void NatCorder.Examples.ReplayCam::StopMicrophone()
extern void ReplayCam_StopMicrophone_mAC9FE62E3D85F436B2EE4D2F76C14A009FC46C0E (void);
// 0x00000695 System.Void NatCorder.Examples.ReplayCam::OnReplay(System.String)
extern void ReplayCam_OnReplay_mBE1DEF2C56CAF0B9E0A14C6F998BD730520C50D3 (void);
// 0x00000696 System.Void NatCorder.Examples.ReplayCam::.ctor()
extern void ReplayCam__ctor_mFCBF5AF5ABF84B739D20FAE3B55871EB91FC5F77 (void);
// 0x00000697 System.Void NatCorder.Examples.WebCam::StartRecording()
extern void WebCam_StartRecording_m321ADA5BC74C232CCCDB110F778BB03A148BBE74 (void);
// 0x00000698 System.Void NatCorder.Examples.WebCam::StopRecording()
extern void WebCam_StopRecording_m472B20ED414C0C529E6906124E1A8B335C3193E4 (void);
// 0x00000699 System.Collections.IEnumerator NatCorder.Examples.WebCam::Start()
extern void WebCam_Start_mC494E829B80039BC1F9554FC495E697B895E7707 (void);
// 0x0000069A System.Void NatCorder.Examples.WebCam::Update()
extern void WebCam_Update_m3A79EDDA2E38E636755143F25BEA0E9A3DDA6767 (void);
// 0x0000069B System.Void NatCorder.Examples.WebCam::OnRecording(System.String)
extern void WebCam_OnRecording_m75081A9E922A625A74D21A6EEF1765F6DA6CAD6F (void);
// 0x0000069C System.Void NatCorder.Examples.WebCam::.ctor()
extern void WebCam__ctor_m41B8CB3DA7F937A2E196CE45FC8A00C4D89E6776 (void);
// 0x0000069D System.Boolean NatCorder.Examples.WebCam::<Start>b__8_0()
extern void WebCam_U3CStartU3Eb__8_0_m91D44E50129C6182EAB1F328C61A730B0DB06BA7 (void);
// 0x0000069E System.Void NatCorder.Examples.WebCam/<Start>d__8::.ctor(System.Int32)
extern void U3CStartU3Ed__8__ctor_mF47DBA782E463958505ED30A62B4BD427843B556 (void);
// 0x0000069F System.Void NatCorder.Examples.WebCam/<Start>d__8::System.IDisposable.Dispose()
extern void U3CStartU3Ed__8_System_IDisposable_Dispose_m34A2B4BC4CE9B1A0864BFC72C100FB4381BF0DB3 (void);
// 0x000006A0 System.Boolean NatCorder.Examples.WebCam/<Start>d__8::MoveNext()
extern void U3CStartU3Ed__8_MoveNext_m55BDD9B2171F9FF44DAD6FFA70E187B9D6E466DA (void);
// 0x000006A1 System.Object NatCorder.Examples.WebCam/<Start>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01996306B749DDCDF55B17EF0D7E078E5B3E0B83 (void);
// 0x000006A2 System.Void NatCorder.Examples.WebCam/<Start>d__8::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__8_System_Collections_IEnumerator_Reset_mDA213E06F07CFA944EB5E3E57E2CE3648D4D153D (void);
// 0x000006A3 System.Object NatCorder.Examples.WebCam/<Start>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__8_System_Collections_IEnumerator_get_Current_m06AF9C8ECA7E02213812F62AE80FE17AB6C78FE5 (void);
// 0x000006A4 System.Void ArRobot.Modules.Logic.Scripts.Handlers.ILogicGyroscopeHandler::OnNewGyroscopeStatus(System.Single,System.Single)
// 0x000006A5 System.Void ArRobot.Modules.Logic.Scripts.Handlers.ILogicMediaMaker::OnPhotoSaved(System.Boolean,System.String)
// 0x000006A6 System.Void ArRobot.Modules.Logic.Scripts.Handlers.ILogicMediaMaker::OnVideoSaved(System.Boolean,System.String)
// 0x000006A7 System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::StartGettingBluetoothDeviceListAsync()
// 0x000006A8 System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::StopGettingBluetoothDeviceList()
// 0x000006A9 System.Boolean ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::IsScanningBluetoothDevices()
// 0x000006AA System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::ConnectToBluetoothDeviceAsync(System.String)
// 0x000006AB System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::DisconnectFromBluetoothDeviceAsync()
// 0x000006AC System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::ScanAndConnectToCompatibleDeviceAsync()
// 0x000006AD System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::StopScanningAndConnectingToCompatibleDevice()
// 0x000006AE ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::GetBluetoothDevice(System.String)
// 0x000006AF ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::GetConnectedBluetoothDevice()
// 0x000006B0 System.Boolean ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::IsBluetoothConnected()
// 0x000006B1 System.String ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::get_ServiceUUID()
// 0x000006B2 System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::set_ServiceUUID(System.String)
// 0x000006B3 System.String ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::get_CharacteristicUUID()
// 0x000006B4 System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::set_CharacteristicUUID(System.String)
// 0x000006B5 System.String ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::get_DeviceName()
// 0x000006B6 System.Void ArRobot.Modules.Logic.Scripts.Core.IArRobotLogicBluetoothConnectivityMod::set_DeviceName(System.String)
// 0x000006B7 System.String ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice::get_Name()
extern void LogicBluetoothDevice_get_Name_m56F1745B39998C9FA12426D2AFD129B0B16C95BF (void);
// 0x000006B8 System.String ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice::get_Address()
extern void LogicBluetoothDevice_get_Address_m7E621404ED680D71E3A5FBA7C06E56F4DA4E860B (void);
// 0x000006B9 System.Void ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice::.ctor(System.String,System.String)
extern void LogicBluetoothDevice__ctor_m07CDC6E060E4C29F6AA2810C5C1239DC43F6C14B (void);
// 0x000006BA System.Void ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice::.ctor(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityBluetoothDevice)
extern void LogicBluetoothDevice__ctor_m52EE06D8081617B9951EED4E8E822D0E1D961FFD (void);
// 0x000006BB System.Boolean ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice::Equals(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice)
extern void LogicBluetoothDevice_Equals_m352CC57816E6CCD72BE53D86F7862F7AC66D958E (void);
// 0x000006BC System.Int32 ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice::CompareTo(ArRobot.Modules.Logic.Scripts.Common.LogicBluetoothDevice)
extern void LogicBluetoothDevice_CompareTo_mB385E2A8AD585B19CD4131ED96E1DEC5CABE1C98 (void);
// 0x000006BD System.Void ArRobot.Modules.Logic.Scripts.Common.StoringRoutineTemplate::.ctor(System.String,System.String,RobotAction/MotionAction[],RobotAction/ArmsAction[],RobotAction/EmotionAction[],RobotAction/SoundAction[])
extern void StoringRoutineTemplate__ctor_m1F43478193AD1B31F04F64FC09DC53E2E3CD9E37 (void);
// 0x000006BE System.Void ArRobot.Modules.Common.DancingMode::.ctor()
extern void DancingMode__ctor_mCB8994B88388F5C6E0BA8F686A837DC49E6A0E30 (void);
// 0x000006BF ArRobot.Modules.Common.ArRobotLogicConnectivityVariables/ConnectionState ArRobot.Modules.Common.ArRobotLogicConnectivityVariables::GetState(Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common.ConnectivityVariables/ConnectionState)
extern void ArRobotLogicConnectivityVariables_GetState_m9FBD91A77587AA5044630F0A2116502A89DAEB6B (void);
// 0x000006C0 System.Void ArRobot.Modules.Common.ArRobotLogicConnectivityVariables::.ctor()
extern void ArRobotLogicConnectivityVariables__ctor_mD38A6AF84F5773718F8DAB97E82F90AA7EFFBC3D (void);
// 0x000006C1 System.Void Senso.Dialog.ISensoDialogMod::ShowDialog(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action,System.Boolean,System.Boolean)
// 0x000006C2 System.Void Senso.Dialog.ISensoDialogMod::ShowDialog(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action,System.String,System.Action,System.Boolean,System.Boolean)
// 0x000006C3 System.Void Senso.Dialog.ISensoDialogMod::ShowDialogWithInputField(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action`1<System.String>,System.Boolean,System.Boolean)
// 0x000006C4 System.Void Senso.Dialog.ISensoDialogMod::ShowDialogWithInputField(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action`1<System.String>,System.String,System.Action,System.Boolean,System.Boolean)
// 0x000006C5 System.Boolean Senso.Dialog.SensoDialogMod::get_DialogEnabled()
extern void SensoDialogMod_get_DialogEnabled_m64D88EC0432BBE9048196E77ED531321ADD6DCE0 (void);
// 0x000006C6 System.Void Senso.Dialog.SensoDialogMod::set_DialogEnabled(System.Boolean)
extern void SensoDialogMod_set_DialogEnabled_m99961CA83305AF7DE9A5CC2CC5B623E037DF6E59 (void);
// 0x000006C7 System.Void Senso.Dialog.SensoDialogMod::ShowDialog(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action,System.Boolean,System.Boolean)
extern void SensoDialogMod_ShowDialog_mF516188C0088437C5F5E167F9E9F78FBD5CBC0C7 (void);
// 0x000006C8 System.Void Senso.Dialog.SensoDialogMod::ShowDialog(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action,System.String,System.Action,System.Boolean,System.Boolean)
extern void SensoDialogMod_ShowDialog_m0DC3D2A39DFDAA6731BAE104797EB2A2D178DC83 (void);
// 0x000006C9 System.Void Senso.Dialog.SensoDialogMod::ShowDialogWithInputField(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action`1<System.String>,System.Boolean,System.Boolean)
extern void SensoDialogMod_ShowDialogWithInputField_m521584534CEE26662EFF64386867F48E0A5E4CEB (void);
// 0x000006CA System.Void Senso.Dialog.SensoDialogMod::ShowDialogWithInputField(Senso.Dialog.SensoDialogEnums/SensoDialogModDialogType,System.String,System.String,System.String,System.Action`1<System.String>,System.String,System.Action,System.Boolean,System.Boolean)
extern void SensoDialogMod_ShowDialogWithInputField_mE7E9629C060ECDC0E423252E9396DBF8C169EB49 (void);
// 0x000006CB System.Void Senso.Dialog.SensoDialogMod::OnListenersAndModRegistration()
extern void SensoDialogMod_OnListenersAndModRegistration_mE2E217076C9AFD3B7609DE58B7819DB16DA294B1 (void);
// 0x000006CC System.Void Senso.Dialog.SensoDialogMod::OnDestroyListenersAndModUnregistration()
extern void SensoDialogMod_OnDestroyListenersAndModUnregistration_m4F5CD70FBD0CFC15DE8F68C58B3242FF2DEC0E95 (void);
// 0x000006CD System.Void Senso.Dialog.SensoDialogMod::UStart()
extern void SensoDialogMod_UStart_m0D3D0C8C9D2BB8CB6975A94F6F36886D6B19342B (void);
// 0x000006CE System.Void Senso.Dialog.SensoDialogMod::OnAcceptButtonClicked()
extern void SensoDialogMod_OnAcceptButtonClicked_m3AEBEF3F5FE218B9208BCBC28FA75BD08401806F (void);
// 0x000006CF System.Void Senso.Dialog.SensoDialogMod::OnCancelButtonClicked()
extern void SensoDialogMod_OnCancelButtonClicked_m778C255AE5B3AB74E21A39576C92F502F7E5CCE1 (void);
// 0x000006D0 System.Void Senso.Dialog.SensoDialogMod::.ctor()
extern void SensoDialogMod__ctor_mE069BF94C37D9DF551972ED235BBBBA4137976E3 (void);
// 0x000006D1 System.Void Senso.UI.ArRobotAppConfiguration::OnInitialializeConfiguration()
extern void ArRobotAppConfiguration_OnInitialializeConfiguration_mC35593F448004FD4AD29D3F832A925EE54E2BA92 (void);
// 0x000006D2 System.Void Senso.UI.ArRobotAppConfiguration::.ctor()
extern void ArRobotAppConfiguration__ctor_mA388AC3F8805BC8FA75636ECEF768FFEA079C951 (void);
static Il2CppMethodPointer s_methodPointers[1746] = 
{
	AboutScene_UStart_m07EAD6D287CA38326A087A3D92885B209996E8E4,
	AboutScene__ctor_mA36347B427AAEED5705DAF8385BF6A7E9E8F4FAC,
	ArrowsRealTime_Start_mB9548DBED1AEC1098BA2D54E0326C2CC7AC6FC67,
	ArrowsRealTime__ctor_m27CA6EF245D048841AFFF448F83237AF3C177BF2,
	ArrowsRealTime_U3CStartU3Eb__5_0_mA80DEB38BB5337B827A156426C8BF9047B9E6F38,
	ArrowsRealTime_U3CStartU3Eb__5_1_mBE23A2430EA93A8C14E8A962E31E1A38E88AAA0B,
	BarProgress_Start_mD2A21A1C0F8551453B254A87FBB12D5C4E6FE5D5,
	BarProgress_StartSong_mD41DE672D75646B2A57ABB8EA21D5B96BC37089A,
	BarProgress_BarCoroutine_m42E33E823A5AF5B084074B7E02A6D456B101C14F,
	BarProgress_StopSong_mDC7A7E69D1DF2524637BD44BD1A864DD725AB961,
	BarProgress__ctor_m10E71C8CECD7C75B9D8A4C1527CC315C7DE754E7,
	U3CBarCoroutineU3Ed__7__ctor_m23C1539725D2119616718EF7BA05B3F0EB414D5F,
	U3CBarCoroutineU3Ed__7_System_IDisposable_Dispose_m9835E294522F630BC99CD44E8332590317EB0EDB,
	U3CBarCoroutineU3Ed__7_MoveNext_mECD7CF9F84B385B7CD5A727DB6E9FE5ABAA6BB51,
	U3CBarCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72FCE8FE471CB3AA7EE811EE21D35DDF31B50B99,
	U3CBarCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m67BDED7D58737BAB2559AE1D44E2D7E4E64AA71F,
	U3CBarCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_m2653EC05BEBFE9545C2D0DBBA592AAA317DBA6D4,
	Flash_Start_mB3F283392DE2C490DC1EF9F48CFE865CE9791F0A,
	Flash_DoFlash_m6CA10AA41E15D586EB0B8366C56EFE2C8D5454FD,
	Flash_FlashRoutine_m2757A18F9D0FB2C6E2F6E208D73232394561F624,
	Flash__ctor_mD42700F6BDFE87A28A3301EAB53DF4A8B562D10C,
	U3CFlashRoutineU3Ed__3__ctor_mB26267773BF6336204DB3EDAB396AEE24B7AF3DA,
	U3CFlashRoutineU3Ed__3_System_IDisposable_Dispose_mE34F87C5B58FEBF1C71A7899F4AEC7FEFB579571,
	U3CFlashRoutineU3Ed__3_MoveNext_mCF33647F38E3E1D34A992BE86894906833B0F01A,
	U3CFlashRoutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35F71B21278725AE070D651D3AB7693BF5D83CD1,
	U3CFlashRoutineU3Ed__3_System_Collections_IEnumerator_Reset_m50A65F73FD1C18C207AF05302780FB95E5A66C2C,
	U3CFlashRoutineU3Ed__3_System_Collections_IEnumerator_get_Current_m1A07A88FDD81376B68505EE64E5B5D8724B86705,
	GreenPlay__ctor_mCA8E371BF41D2B16C1227F7154637123C0A43BB3,
	GyrosScript_Update_mDC58AAE8503976546FEF4F01098D570E8537CCC3,
	GyrosScript__ctor_mF7672C14C4D57BD6B867480E0BD4956D02FC77ED,
	OpenScene_Start_mF143B95937024F6DDE3BC6821D773A056E1E9821,
	OpenScene_OnClick_mD80DBCD7A44CAC345CA646F3BC1D12ED09971799,
	OpenScene__ctor_m2AC575BDAA09D5BE6DED2B619860AA0C44A96513,
	MainMenuScene_UStart_m19E4D53A5EAFD9C5EE30D04C6DA91720EBBC4E19,
	MainMenuScene_Update_mBEB4B608FF11A182883494FB88A8EC7B23E4D94B,
	MainMenuScene__ctor_mF74AC5A080C085EE614238A09C55287F48B9B412,
	U3CU3Ec__DisplayClass2_0__ctor_mCE1250F1C47436877E6F952107411E47E9F2F710,
	U3CU3Ec__DisplayClass2_0_U3CUStartU3Eb__0_m390A93E25BD1CFC783A3747CD43DA86D8784C52A,
	U3CU3Ec__DisplayClass2_0_U3CUStartU3Eb__2_mF51EFA624AE4CBA0C9773FACEFD8039E3F0972CE,
	U3CU3Ec__cctor_mF52C849060A83B7B7628D604ABD7271EA532B1EA,
	U3CU3Ec__ctor_mD81BC8F1170D0F752DFEFAFC11BD7DA3D0FA0319,
	U3CU3Ec_U3CUStartU3Eb__2_1_m8EC299EBD4DE60775E80945A8B95B6A2A5D5DD10,
	MouseOverViewPort_OnPointerEnter_mEF8185C27622FDDF6919D7DC59BF341BCC61EF83,
	MouseOverViewPort_OnPointerExit_m617578E9394B24E4BE3CD982C185A51C28CE045B,
	MouseOverViewPort__ctor_m14E192911CEDB0C316174B2AC8941F7A66A335C3,
	MusicButton_Start_m470B3D841B003920A256A3F875DB8C10A0D2C5F0,
	MusicButton_OnClick_m27695C33321ABC30A26E27BC49FE5254CBBFA217,
	MusicButton_StartSong_mECD707F068E435705FCC1630326A48D613BA7386,
	MusicButton_StopSong_m11ED8F987F0078EAB52B77B0E83AC14C33CE5B94,
	MusicButton__ctor_m1ED0930034301A38D16858A9FCB40E326405CA2D,
	MusicButtonPhotoMode_Start_m20DF21D9FB26A0E7323D47D1A1C0BA10793DBAD4,
	MusicButtonPhotoMode_OnClick_m299F7BD9E873825365D98AF2951D636B28C14807,
	MusicButtonPhotoMode_StartSong_m6A261BC914AB1645125A96A055923956C2BC9080,
	MusicButtonPhotoMode_StopSong_m94DFD03F3DBD7B5F7E9169ACDCAE6549DE152323,
	MusicButtonPhotoMode__ctor_m03FC52C6C38B35EB76D24DE4C9971ED08085F6A3,
	OnClickGoToWeb_Start_m174D187C041D182F9A265BCCA56CCFE4E978D209,
	OnClickGoToWeb_Update_m577AAB559AE048D1BE6E8C4073EF36BEF8958504,
	OnClickGoToWeb_OnPointerClick_mD9C248A602AB1B763074A950E07A74B6CCDAB0C8,
	OnClickGoToWeb__ctor_mBE13D538381B0E5649E8550B7680FA27C6C48DA3,
	OnClickSendEmail_Start_mBC579B612C620883494369D23A529BD83A6E2370,
	OnClickSendEmail_Update_mA0F3C5A2EA651FECF6578B1EEE1A128DB14165C2,
	OnClickSendEmail_OnPointerClick_m14E688D118ADF29A1339C3E53300E1F73F79131B,
	OnClickSendEmail__ctor_m3E1B305A1C14899C6789A7688D237AF6A89050FD,
	ArrowToShowButtons_Start_m57582BD827D7EC83A9781EFF79ADF3E8EA0D87A5,
	ArrowToShowButtons_Hide_mA706197DB653A4A0D753BEED44273CBADD28AE37,
	ArrowToShowButtons__ctor_m35CAE886B7B185F782C0CFFD8D33FD97773B7FF5,
	EmptyScript__ctor_mAF769F24203F563BD14920402063E1AEFE02A1B4,
	HomeButton_Start_mDE53CD878E625747C9DDF9F38984CA7DDFCAF3FE,
	HomeButton_OnClick_m99A3EC7748F9358AE581F45F0517F16A67FFA201,
	HomeButton__ctor_m3F8B7A08E900FBEA261080C2BB164BDCCB902107,
	PlusButton_Start_mA070D7E6690C70DFC0150DAADDB4AE58CE115CD8,
	PlusButton_OnClick_m7A385547D3C6CF71D71DE9C06997F70D1C13035E,
	PlusButton__ctor_m5FFB7FE1FFCC8CE10DDB483A12494CAB87FE79F9,
	ResetButton_UStart_m3849863D6C0CF9AB1004879FC0EEB23EC9247CF5,
	ResetButton_OnClick_m1E0A7953522631143E3429F15F79D19F51C68F21,
	ResetButton__ctor_m6F0DF3952E54AA88046E41814EE352B5A6CBE3FD,
	RowColumn_get_Row_m333F000183C44948345F44B2FCF6366DD4455E0B,
	RowColumn_set_Row_m3690A6E67C35653D3B2DF253C835DA41E2604D21,
	RowColumn_get_Column_m96B3038B0A009E52A89C9D5796F81500E9772B12,
	RowColumn_set_Column_m2F4103415D215EAA830F16995C1702AD68B36231,
	RowColumn__ctor_mF06489A8F9F9A52FB754B4506ADD5EE68FEEF444,
	TransparentButton_Start_m422B0B73FD677F32CDBE8EC3E775289C503EA59E,
	TransparentButton_OnClick_m1388453B6E17B86892B731DD8B38FBA0094172DB,
	TransparentButton__ctor_m4606C7B677E25F517DB3DC23C97ADDA43FB70019,
	RobotAction_get_Color_mC51F6EABBA7092392357773E63849F4AA8C4B543,
	RobotAction_set_Color_mCBA8760E0F8ABD373953E1084041A0979A0E4F0F,
	RobotAction_get_Category_mC5450A085B339918E7CCDD3AC4FB670E188D1014,
	RobotAction_set_Category_m2B9BBEFC1F72F0FF101F7CD590576487CF0077DD,
	RobotAction__ctor_m93C27B58B1F2BA0D36D97C57EFD552F4EE093A31,
	RobotAction_get_Name_m37F3BC13A9D8988162BF6D1AE29222FDAD647196,
	RobotAction_get_Icon_m76C8E8FD48A93732B77B710940FCC39F41E19A68,
	RobotAction_get_Duration_m379F6085A8241B2AB319C6ABE8950548E471AEA4,
	RobotAction_GetFaceActions_m2FACB2D2C318958BDFB7FDAAD9148F27C04AA62B,
	RobotActionCategory__ctor_m6F4F2CB2642D56D0731D432563C06E454F61AB57,
	RobotActionCategory_get_Name_m1A0781878E3F63996AD8746522C6F46C722B512C,
	RobotActionCategory_get_Color_mC94763CD0F499A064F1130CCD0F87D2BE1BDD6C8,
	RobotActionCategory_get_RobotActions_mC331CE64E2FA269FAA7813A47A87310B8717D041,
	RobotActionCategory_AddAction_m43C55CFDF7D0259FFE111E4A6B5EBD22B5D204D0,
	RobotActionCategory_GetAction_m1124F155D27176CAAFCE9FE05DE98F079778B889,
	RotateItSelf_Start_mF85CD71BB0EB1B404C18A262D725798592B071DF,
	RotateItSelf_Update_m29163586C1FB849926C7675FC7ED3E64FAD90307,
	RotateItSelf__ctor_mF84D89DF31E7A7BD181F27108251F29828688E80,
	ScaleFloor_Start_mA3F617C8B98ED4E16A3283E3C68367A8890E782C,
	ScaleFloor_Update_mE6644DE891E5A769CBD9F91A1F2053173874B92E,
	ScaleFloor__ctor_mC271939B84F8E8A186751056210F595D88FB8DBE,
	SquareScript_Start_m77FE50CB869C7C5531F5F0E0D8B5D86918A03859,
	SquareScript_StartSong_mC59987E0AC6A8FCC2E5875A680CE6470A7D8B147,
	SquareScript_StopSong_mE058CA63F28869C94494A9D48B573A52760A33A5,
	SquareScript_SquaresCoroutine_mBCCDFC602667926201146A9A3655270B10F5D9B5,
	SquareScript_SetColumnHight_m5A2F20C440158A25262AAE6119FAD54BDC4EB35B,
	SquareScript__ctor_m08A51BD8536D3082AB04900120943485D6EE90C2,
	U3CSquaresCoroutineU3Ed__7__ctor_m6563D6EE6FE3EAEC4F954344013782F3D71A49FC,
	U3CSquaresCoroutineU3Ed__7_System_IDisposable_Dispose_mC37CF4764B37CB91917AAEE28E2279BACDA52977,
	U3CSquaresCoroutineU3Ed__7_MoveNext_m41313DB756AF8A08B4DEA6D51E1453AAE18C1E2C,
	U3CSquaresCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10C7A5FDD125FCCAC5F7CD5DEB1EB84845B97D65,
	U3CSquaresCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m1EBD89DD246CA868F9C1E044493B42EE67130A58,
	U3CSquaresCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mAF01FC8EC09AC387231FB31A681EA86537C8D9B7,
	WebCamPhotoCamera_Start_m246A1152801FFAA8843BF5D4775B462B24295A47,
	WebCamPhotoCamera_TakePhoto_m3F0BEC7432FE1BE320B4A4CAD230C4C702C82DD9,
	WebCamPhotoCamera__ctor_mC37C8FC9D1B2CD7E1C5319D10B619E92C02DE3E1,
	U3CTakePhotoU3Ed__2__ctor_m18715F20CEE37803C8CF9CA87B7E9D5744A52DC1,
	U3CTakePhotoU3Ed__2_System_IDisposable_Dispose_m305CD191A679B1656C60627758C88E791E3471F3,
	U3CTakePhotoU3Ed__2_MoveNext_mC84B534F4AFDE5B142285A83496054D3CBBC6FD3,
	U3CTakePhotoU3Ed__2_System_Collections_Generic_IEnumeratorU3CUnityEngine_WaitForEndOfFrameU3E_get_Current_mEE7C5280D66E7661C26759A3E79EFAF152D3C3B9,
	U3CTakePhotoU3Ed__2_System_Collections_IEnumerator_Reset_m1128DB9726EC4E7F87E93AEEAAAC242948282A43,
	U3CTakePhotoU3Ed__2_System_Collections_IEnumerator_get_Current_m4F13E74A055BC74585057C2D23B618212DE9D2CB,
	U3CTakePhotoU3Ed__2_System_Collections_Generic_IEnumerableU3CUnityEngine_WaitForEndOfFrameU3E_GetEnumerator_mE83D842777CEE388E8AD5ED2B8CA3D8A74850AEB,
	U3CTakePhotoU3Ed__2_System_Collections_IEnumerable_GetEnumerator_mF16F8DD01D18A6813DB5F6C82DCC4A8037D8AA63,
	SavedRoutine_get_Name_m34FA47460187AAFAB4CCA8079500748AC3BC6CA5,
	SavedRoutine_get_Id_m81229AF45B9C0E933C25924FC29CBC2783DA8157,
	SavedRoutine__ctor_m03585835C5841F999BD1A09DDDE97BE86F17F876,
	ArRobotLogicMod_get_ServiceUUID_m407DA635A23E9CFA69D206651790985AEF4A0046,
	ArRobotLogicMod_set_ServiceUUID_m2417B641F869D08F81904CB3D2CBD0DA1283BE55,
	ArRobotLogicMod_get_CharacteristicUUID_mFDADCA916B90CF056CE2D17F2332427F82472967,
	ArRobotLogicMod_set_CharacteristicUUID_m150B6520F8AB6CEB7052B8BD719BE95431398119,
	ArRobotLogicMod_get_DeviceName_m169B250ED427BF5E3326531317FE66750BC30D25,
	ArRobotLogicMod_set_DeviceName_mFF8413F510B6E8F5624B740DB0FC4E310CB053BC,
	ArRobotLogicMod_UStart_m3831A293859F9B585688989C48F2E2E923B6A0A1,
	ArRobotLogicMod_OnListenersAndModRegistration_mBE4869C2027160C20FCC596032306B79D2276F81,
	ArRobotLogicMod_OnDestroyListenersAndModUnregistration_m12323DA3D816B30F1F245E02A5ABDC9C7E674260,
	ArRobotLogicMod_StartGettingBluetoothDeviceListAsync_mC73B3469538698A01184472AB12105AD597EB348,
	ArRobotLogicMod_StopGettingBluetoothDeviceList_m604385E5AFFF3249911C35A93EAD3FE086580CB4,
	ArRobotLogicMod_ConnectToBluetoothDeviceAsync_mDFC1EFE3DD745DA6EE5C32F238C5D5E69629F964,
	ArRobotLogicMod_DisconnectFromBluetoothDeviceAsync_m671F93CBBF099BC27DEAD3D0028986BCFA268FFA,
	ArRobotLogicMod_ScanAndConnectToCompatibleDeviceAsync_mBDE2F96F961AECBDF0C3A2F3973502EE76A88E54,
	ArRobotLogicMod__ScanningAndConnectingTimeOut_m08BB0B209A845F676397FF0423E553E7A485511B,
	ArRobotLogicMod_StopScanningAndConnectingToCompatibleDevice_m68247679079D2C185A878B9BCD5F39626D3CF685,
	ArRobotLogicMod_GetBluetoothDevice_mAEDF468CB0361A03499227C3912F2D90F9B1D454,
	ArRobotLogicMod_GetConnectedBluetoothDevice_m3ECA9F5AF158AD0ADA9D00A5F1F3E94A1428DFE3,
	ArRobotLogicMod_IsBluetoothConnected_mE45A57D27023E09BBC4209100FD0212CDD87FB18,
	ArRobotLogicMod_IsScanningBluetoothDevices_m723635D72B9743F900A0236753B1F4C8BD054BF3,
	ArRobotLogicMod__SendMessage_mFA0CB5A382E63715EBE090BA2AE4757AB2E5E838,
	ArRobotLogicMod_GetSavedRoutinesList_mD2632743D6701CE2B1BC5EEB5947C32E491D876C,
	ArRobotLogicMod_LoadRoutineAsync_mA4AE0887F79421E4D701FA854E981CE88586454C,
	ArRobotLogicMod_SaveRoutineAsync_m8E80DF5AAD872F35F4198AED395138878E3875C5,
	ArRobotLogicMod_PlayRoutine_mEEBD57FE151A7E32DED5F7718F5587D3B4485EC6,
	ArRobotLogicMod__PlayingRoutineCoroutine_mC47460CB44B872022722548BCED178A271EC090D,
	ArRobotLogicMod__PlayingRoutineCoroutineWithPartial_mAB241A53E3ECF6597FACE1EA09D81C5834DF33C1,
	ArRobotLogicMod__PlayActionsOnRobot_m2997A8A1F3A06E678F87B49272D404575421669B,
	ArRobotLogicMod_PauseRoutine_mDB494E58ACB2F46D0DCA0B4F8303C8DCF5414796,
	ArRobotLogicMod__StopRobot_m425E423E35233A1BB1D364834ED9CAA0440786E1,
	ArRobotLogicMod__StopFace_mBB65C61E5D55F375EF4762444E638960CC2607BA,
	ArRobotLogicMod_UnpauseRoutine_m877DA837D14523273F0B3E70905344B68ED4E2C9,
	ArRobotLogicMod_StopRoutine_mE58B0A1FA3B5C9EFE4D7068A718F3DA42930B3CD,
	ArRobotLogicMod_SendRealtimeAction_m436D2A6E19C11B1590432356FA439E9A459C0DC3,
	ArRobotLogicMod_SendRealtimeAction_m1275F481D2CF6B9272309F44B08EBC006269B823,
	ArRobotLogicMod_SendRealtimeAction_m59986D575902CCF4E1AF6D90D984D89881334715,
	ArRobotLogicMod__SendRealTimeAction_mD8E6B0A3D2EF683E362A5A557A4BE4F2DB51739D,
	ArRobotLogicMod_StartSendingRealtimeAction_m7366977948488BD11C63A0FA00CF637136710412,
	ArRobotLogicMod__SendContinuousRealTimeAction_m24A2FB5F19586A97632067D70E8B6028C0D657CC,
	ArRobotLogicMod_StopSendingRealtimeAction_m45B148B0046C3DD127E2711423C2201248D3FEBF,
	ArRobotLogicMod_StartMotionWithGyroscope_mE8B28F36C5AFF3F1AF78FE874C4100602D789A76,
	ArRobotLogicMod_StopMotionWithGyroscope_mA65EC353B870424150BDB501C55BEEB864CE2B6B,
	ArRobotLogicMod_StartDancing_m0A840581C0917F4E003E9729CCC3436F370BADE1,
	ArRobotLogicMod_DancingCoroutine_m8F0B70B41DDF8037D6066CA7AA3203A017DEC1EB,
	ArRobotLogicMod_PauseDancing_m2DA5AAFC2AFC15737671FB3CF5D3469D47FA9275,
	ArRobotLogicMod_StopDancing_m2C44BCF026FA6CC9009C41BD409FA2B5068E715B,
	ArRobotLogicMod_ShowCameraOnTexture_mC9D667762C7B8DC8203D28D7955F68D89381276D,
	ArRobotLogicMod_StopShowingCameraOnTexture_mFAA5BA9684AD5F108A7A3ABF2298EE101FC80B28,
	ArRobotLogicMod_TakePhotoAsync_mBC81D9FF80F01BB0763CA923CB7D0B298C7E6B3E,
	ArRobotLogicMod_StartMakingVideo_m7C6579924A67D9CA1F379F9486C8B2BE884DFCC9,
	ArRobotLogicMod_StopAndSaveVideoAsync_mF714030D08538D00F0F36F70D1FE500897E9C6B6,
	ArRobotLogicMod_OnBluetoothDeviceListUpdated_m6BBFAFD60DFF55B1048E07B34B89F3F23100FDBB,
	ArRobotLogicMod_OnBluetoothConnected_mCA2624614B9E8CDFD81F1BF1026AE5C31EAED3F2,
	ArRobotLogicMod_OnBluetoothDisconnected_mBEFA4659C4DFADBE5D595E3F1DC79BA6B329A41E,
	ArRobotLogicMod_OnMessageSent_m728FD7C82A939657F250692BFDB3A0FD5F8F158D,
	ArRobotLogicMod_OnLoadCompleted_mC5C8B75BC3F37612E1CDE126D14B658C41CEA123,
	ArRobotLogicMod_OnStorageCompleted_m3123B252BEB1AA49256EA60124023A5FA7B63A70,
	ArRobotLogicMod__TransformActionsToMessage_m1782A584E40573EF976E9BCA4E0818701F6A8F7C,
	ArRobotLogicMod_OnNewGyroscopeStatus_mB9042421429EE21BBB000F9764A0F2ED060EA5ED,
	ArRobotLogicMod_StabilizeGyroscospe_m7025B5CBC02C8119E4205A5497B17B077974AFA5,
	ArRobotLogicMod__ctor_mF22B7BA1C2B2B3341D8F59F4CEB047B04EAA5608,
	ArRobotLogicMod__cctor_m4E511158D61B1AD09C4B90DBF6634463AA734C51,
	DanceRoutine__ctor_mB7B75820AE220161C3CD1857D0B6A339AEAF8263,
	U3C_ScanningAndConnectingTimeOutU3Ed__63__ctor_mFE121B757E547AB140F7F35BFD6131C2344D20B2,
	U3C_ScanningAndConnectingTimeOutU3Ed__63_System_IDisposable_Dispose_m556B56249CAF17311F95DC8C967757CB710EF37F,
	U3C_ScanningAndConnectingTimeOutU3Ed__63_MoveNext_mC6BAA00F6A9F4F6D7542D58DA542AA9B28AF1795,
	U3C_ScanningAndConnectingTimeOutU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5837FD6B1017425588F98AE19B82011EBF5CC287,
	U3C_ScanningAndConnectingTimeOutU3Ed__63_System_Collections_IEnumerator_Reset_m152C0FA1A11B0D092093AFD72E81EA19689E3D9B,
	U3C_ScanningAndConnectingTimeOutU3Ed__63_System_Collections_IEnumerator_get_Current_m6CBAB3122A5A7B626AB237B51C9F4DF57A6D9C32,
	U3C_PlayingRoutineCoroutineU3Ed__75__ctor_m15EE7DD2A43EFBDCCDE1AA10549BB47762151728,
	U3C_PlayingRoutineCoroutineU3Ed__75_System_IDisposable_Dispose_m856048495F47D845B3CD613D91B5B58BDFD53AFE,
	U3C_PlayingRoutineCoroutineU3Ed__75_MoveNext_mB4416C0E5662EFEBEC11788770ECE482C457C76A,
	U3C_PlayingRoutineCoroutineU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7B2152A44501419E0E94508D5B24E01AC41D9D6,
	U3C_PlayingRoutineCoroutineU3Ed__75_System_Collections_IEnumerator_Reset_m4616D580F2781DD99851861BC8492EC07CC2B180,
	U3C_PlayingRoutineCoroutineU3Ed__75_System_Collections_IEnumerator_get_Current_mAA9899C8C3C86B116CA8F9197DE4740048D3986B,
	U3C_PlayingRoutineCoroutineWithPartialU3Ed__76__ctor_mD427AEF03961EA698C17CEC424ECCE21A1558404,
	U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_IDisposable_Dispose_m4BB9B29062C83C2747659EC95C69DC7395068775,
	U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_MoveNext_mEC07B9910C7E6584B2707CA33DC18DF7DBFD93B9,
	U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FDA8D962017E023C2B630311D2B9A449942E2A5,
	U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_Collections_IEnumerator_Reset_m8F03BA586EE7181F57BE5A9D688DAB7C7FD120A1,
	U3C_PlayingRoutineCoroutineWithPartialU3Ed__76_System_Collections_IEnumerator_get_Current_mC4FAD2740C095B7C7CC0B623168A8D353A64A7A0,
	U3C_SendContinuousRealTimeActionU3Ed__90__ctor_mC4C6FFA9C977CD42F5CB106B2723D67E5D5E4AE7,
	U3C_SendContinuousRealTimeActionU3Ed__90_System_IDisposable_Dispose_mAF1FFA5B3A4DF8A4788A17ABCA059DAF34AAFB08,
	U3C_SendContinuousRealTimeActionU3Ed__90_MoveNext_m09E5C8F035B2F655E7E000DCEB1C4A992C61263A,
	U3C_SendContinuousRealTimeActionU3Ed__90_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m136BF5268335439F97CBB3C3E0FE22A2E87DEE91,
	U3C_SendContinuousRealTimeActionU3Ed__90_System_Collections_IEnumerator_Reset_m706D25D38C718115446748CD847E79FA8C04DD74,
	U3C_SendContinuousRealTimeActionU3Ed__90_System_Collections_IEnumerator_get_Current_m5208934A5D375B33B930DB480A37718DFCF4D4E8,
	U3CDancingCoroutineU3Ed__96__ctor_mBB91F6F21C9FEF3B6C856E53A76E29425C56AEB3,
	U3CDancingCoroutineU3Ed__96_System_IDisposable_Dispose_m7D8BBD747E6D80DD2FE8924E33B124E74D1D99A4,
	U3CDancingCoroutineU3Ed__96_MoveNext_m083D7AFAE0BE3BF953174EAD8ADD31F2F8C42E83,
	U3CDancingCoroutineU3Ed__96_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55444C112E81CC65A33ECC887012496147411479,
	U3CDancingCoroutineU3Ed__96_System_Collections_IEnumerator_Reset_mCF32A1E12F57A310D5457D3666AE56672122FAF8,
	U3CDancingCoroutineU3Ed__96_System_Collections_IEnumerator_get_Current_mBC74819129F44BE34F6A2F14E54D4DA09C18F283,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ActionConnectivityTestingScene_UStart_m4C27107991CF4E96B8C0CC5273391B2B0843CE27,
	ActionConnectivityTestingScene_SendNewButton_m5E247128720B4EA37688BF177A772A914B3287B2,
	ActionConnectivityTestingScene__SendActions_m946DC7EBC4202D3F7039FC0E95582720AA9B9028,
	ActionConnectivityTestingScene__ConnectToAddress_mA937241E8E927D9A3A3962119DC99741B6D7C41E,
	ActionConnectivityTestingScene_ShowWarning_mBA6FDA9101094329FFD163178E7159879A516E34,
	ActionConnectivityTestingScene_HideWarning_m8659A6747669C2153E1DE768F968FE6E1FDAB9AB,
	ActionConnectivityTestingScene__ButtonPressed_m5CF5A4A46F26AE165BEAE632D8C5FC52001BE6B9,
	ActionConnectivityTestingScene__ButtonPressed_mD36E7C87DE2900FCC751810DA6EC3FAD4A9B757F,
	ActionConnectivityTestingScene__ButtonPressed_m69A6485C154E28F1DFAE75EE5AB2983963B1B5B8,
	ActionConnectivityTestingScene__ButtonPressed_mD6769AEDD589566EC0F721003C545D82AF38FA1D,
	ActionConnectivityTestingScene_OnBluetoothDeviceListUpdated_m84AD6E07E04F48FCA8DEC6AE39FF188344D4A213,
	ActionConnectivityTestingScene_OnBluetoothConnected_mE58509F53E0C7CB7C49D1475D0665AC5F78A2E87,
	ActionConnectivityTestingScene_OnBluetoothDisconnected_m6DAD62364EA5BE63899089E4D8D5949F55BB7B6B,
	ActionConnectivityTestingScene__ctor_m031088F99CCDAB27A5009A0C58297B96E81AE3A5,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_0_m390524D5D3CF843D826963FA75ECBACAC8AAE78A,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_1_mDE1F6CD4606AE71B09E6E9E0858E40D5A6931D20,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_2_mE21A32AA0BB7ACA548D146979774D7D79407044C,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_3_mD53564B28566B96868E8D7CA612ED8720B908387,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_4_m0B8FA85FD4554F32989546AE05289D7DFEB8C674,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_5_mCDAAB46A622265754DAECD2A1CF218FBC1B4B11A,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_6_mF3C1ADA1CDBE331B70211BD0D86506A4F939D14B,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_7_m95E632F07D5AC64258EFFE30BEE8842A11513A54,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_8_m83FAEAF90AD367CB3917A77A13D07E6EABDDF2B3,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_9_m5BD82AA5407C1B1E028943EA2CE16E3DFC4B1C26,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_10_m5721E8DA95C149186072C891D138489FECFB7F84,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_11_m0F7DFDA9CC4D4600E70B5CC39FB7CE883F06BD68,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_12_m1D81AF41EB55C8DEB3B5808928A347827AB45AAD,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_13_mCB45D92AD96CA650C52BA5076B4967CDA5D65297,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_14_m398F6C8C5D5E6673137EC0FCBAB636239E87253B,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_15_mBC0B47724E8345CBDC2E8ABA4554676B18152FE0,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_16_mBC9B10E379B14186D7C0590094F9967D2A497AAF,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_17_mBFD559418E61C4EBCBD3137580E4DDC21F6AC846,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_18_m04E5CB22703C4B4D3CECA506132733DC497DEFC2,
	ActionConnectivityTestingScene_U3CUStartU3Eb__54_19_m1A74AE701CDE1F0CE2750C4F2B2B4FACB9D9E8B6,
	AutomaticConnection_UStart_m7AAE51172F1AD1FC57845F9842F158F6D13ADA66,
	AutomaticConnection__SendActions_m1E9B6750869427C39CA6954F36EC12B427C8F8E5,
	AutomaticConnection__ConnectToAddress_m83132FC7925FA137CE6B61F22E27DB0AEB06929C,
	AutomaticConnection_ShowWarning_mD9B8F04D8C16069EEA231E55A9687878AF5B0BFE,
	AutomaticConnection_HideWarning_mB6A018368C561F9F257E04BD59399EF0F6515D8D,
	AutomaticConnection__ButtonPressed_mE0F3423216A50AD279E33AF2C83E7555C634C661,
	AutomaticConnection__ButtonPressed_m82FF526DB35CFC61130083408F7A926FD2BD5C7E,
	AutomaticConnection__ButtonPressed_m497F20F54D85D52445EB28096D2AF4800C41D408,
	AutomaticConnection__ButtonPressed_m7EEBDFBF8CF7E3EE34B781C9145DCE8C5E4A7C36,
	AutomaticConnection_OnBluetoothDeviceListUpdated_m71682EA1F409371FFFCB227194F50D24C5E89C48,
	AutomaticConnection_OnBluetoothConnected_mED6845F11E89716085766F9B7DC173B9A798D3D0,
	AutomaticConnection_OnBluetoothDisconnected_mA56BF756CAFC5FE06749D0E00EFC94FEBB628F11,
	AutomaticConnection__ctor_m44D1B22BA97E5A38E0341CEB67D9DE7E9B71B03B,
	AutomaticConnection_U3CUStartU3Eb__43_0_mF765595BC70D9AB5C7C03D6A8681D2C0C32E06D8,
	AutomaticConnection_U3CUStartU3Eb__43_1_m0D38D92FAAEA2D59DE40B37129245BCC9AF1D2FD,
	AutomaticConnection_U3CUStartU3Eb__43_2_m6EB30D7F6A9D8461CB61736E37B83AECA92FF14A,
	AutomaticConnection_U3CUStartU3Eb__43_3_mB36784BB77AA7F318DB5F51974499A5237678E27,
	AutomaticConnection_U3CUStartU3Eb__43_4_mFD73030E15B39CDB5121F6AF69FCBF88997EA7B0,
	AutomaticConnection_U3CUStartU3Eb__43_5_m5B3548488ECE0739212CE3E22FBB31C1DC459004,
	AutomaticConnection_U3CUStartU3Eb__43_6_mA47D36D860E942BAEDA27D6AC40FEA4D9355D460,
	AutomaticConnection_U3CUStartU3Eb__43_7_m80697AAA5F82D662613F43ADB6C98E16E5470BFE,
	AutomaticConnection_U3CUStartU3Eb__43_8_m48DBAF739BA5E10313DA7DE8A9C8A34987B17483,
	AutomaticConnection_U3CUStartU3Eb__43_9_mE9233D6FBC3F3863F27B3307C95412B8DFF9E6CE,
	AutomaticConnection_U3CUStartU3Eb__43_10_m7D54EFC7C83EAEA77F519E62751FE4793BBEC2F8,
	AutomaticConnection_U3CUStartU3Eb__43_11_m6FA76EEA22B437115FE7BC6D27A164FA72FB7129,
	AutomaticConnection_U3CUStartU3Eb__43_12_mC101F7834F4EDAE0A853D75A9F95F9A5895994A6,
	AutomaticConnection_U3CUStartU3Eb__43_13_mBEDAAA76CE5DC7ACAD3C3E73D2395E07C66DFBD4,
	AutomaticConnection_U3CUStartU3Eb__43_14_m6587C607FA38D1B19B0216EC9EA696323736579C,
	AutomaticConnection_U3CUStartU3Eb__43_15_m4317D9CD25D5EA7761CBA23B15CC8DC954F3D15C,
	AutomaticConnection_U3CUStartU3Eb__43_16_m311283953885D7ADD516FD5EAE490C494282F03F,
	AutomaticConnection_U3CUStartU3Eb__43_17_m9C0C06F8766024C8639A446D869730FC9928518D,
	GyroTestingScene_UStart_m396F7BBD9250B27275E0579EFA14E956ED89BA6B,
	GyroTestingScene__PlayButton_m89ED6BF30E6E28EA7FCD96DAA229DF55DD980E99,
	GyroTestingScene__StopButton_mDA899F7EE46C92C208427A89F51D4C3828A7ACE1,
	GyroTestingScene_OnNewGyroscopeStatus_m891DDA7E7346DDDCED6D029AE6003FBDED673D03,
	GyroTestingScene__ctor_m056A62668679A2A27A96952EC35EC338D7F5CEF8,
	PlayingRoutineTestingScene_UStart_m38E264340404821113B2EF1B032B12CFFAF9C99A,
	PlayingRoutineTestingScene__PlayButton_m3939D6CC4DE476DE8C50A19BB41C4A682F6EEF73,
	PlayingRoutineTestingScene_OnNewStepPlayed_mBB086569ADA31C71B1A21A322378A8CC0C6FDDAD,
	PlayingRoutineTestingScene__ctor_m11D258AA28ED5505EA8B20D7469F28ACC4761D5A,
	StorageActionsTestingScene_UStart_m9ACB433ACE733D29DF5962C7784AA2951C4CCCAB,
	StorageActionsTestingScene_OnRoutineLoaded_mC8EADD17BE7F18CF42F87EDC8565BD20B9057C52,
	StorageActionsTestingScene_OnRoutineSaved_m585AFCA66AA698A148FF1DDC35B0ED2B5FA33B9C,
	StorageActionsTestingScene__ctor_m991B1647A9A28E44886FC16664B472A69800A18D,
	TestArRobotLogicConnectivityScene_UStart_m9E08D191A05B874080AD80BBA1A321EC5AE3EEB5,
	TestArRobotLogicConnectivityScene_OnBluetoothDeviceListUpdated_m471528E81052DBBA63AFA21EB5D17999D08426DC,
	TestArRobotLogicConnectivityScene_OnBluetoothConnected_m29A53A35411CCC655E3018FB4C52A3936DF98426,
	TestArRobotLogicConnectivityScene_OnBluetoothDisconnected_mF6A7BC471B732FC0483C574458380DA1AE36CA74,
	TestArRobotLogicConnectivityScene__OnConnectButtonPressed_mA8F6E4508B658622144357D879BFB7C3E49AE0C4,
	TestArRobotLogicConnectivityScene__SendMessage_m6EEA66E853C9DE178FFC22B26C8431FCD58DABC9,
	TestArRobotLogicConnectivityScene__ctor_m43488CDE0FE99AD8FAF316DE6C209DFDACD76B64,
	TestArRobotLogicConnectivityScene_U3CUStartU3Eb__9_0_mF06F70BF9F06DE13A687E91531C1A1AFF8B4C07E,
	TestArRobotLogicConnectivityScene_U3CUStartU3Eb__9_1_m90189EC68363249E9BDDEC170FF8871133065497,
	TestArRobotLogicConnectivityScene_U3CUStartU3Eb__9_2_mC80B5635C8230A4DB52BE390F79DA4E4F921685F,
	ArRobotUIMod_UStart_mEF519500F791847FF11D2A6EBA97B0EDAD8E404C,
	ArRobotUIMod_UUpdate_m7D73F19E5EB5F95F38433F0B80AF6006C72B5C19,
	ArRobotUIMod_OnListenersAndModRegistration_mE9B7B437BA7963A558A4B1A365CE562E1ED70B50,
	ArRobotUIMod_OnDestroyListenersAndModUnregistration_m9654CE1C2C95E89AB7698F7FEFD3840F127A406A,
	ArRobotUIMod_OnRoutinesListReceived_mDADF51C0E8926AA0A6AB6FCAE0C3EA5D7F54695F,
	ArRobotUIMod_OnRoutineLoaded_m47177F2123FB79E3C117E996FF025354697D5861,
	ArRobotUIMod_OnRoutineSaved_m27891E846AF6EFF5C833A99EB75E51FF487DD291,
	ArRobotUIMod__ctor_mF7411B84ABB0A7259FDA5CC594AFE4FB38DD0781,
	PreloadScene_UStart_mAB547A62766E02D3DA96ADE25372B25131D5A0F2,
	PreloadScene__ctor_m672134C8D3ECBC944DC5162A507FF29605A5DE29,
	DancingModeButtonScript_Start_m21256A0ED1A36F8E0FEB70D33EFE32B941D7F7FB,
	DancingModeButtonScript_ToState_mDBABC1C1C6D1BC0622CB5ECE6D13EC10C5CBE972,
	DancingModeButtonScript__ctor_m177B14B4AEEB334A8E7BBB79BD6B0ABEA9EC6BFC,
	UIAboutAppScene_UITopButtonAction_m40F1B64FA6ADCD1DFE52A8FB774564795A39BDA6,
	UIAboutAppScene__ctor_mF5BB81ED979046B5E7763817CE6B58B8DAD353FE,
	UIDanceModeScene_UITopButtonAction_m9D40F6B7593B9F392EF563B7E3AD6954B5B77B95,
	UIDanceModeScene__ctor_m21ADBF7D1674D093895A2A8EA6152C7EFC59EF57,
	UIDancingModePhotoScene_UAwake_mD227190894148CD5B985F334EBA9E16F0A9E474F,
	UIDancingModePhotoScene_UStart_mCC2FF574C2313739A90FA0BD32A413419A02BED9,
	UIDancingModePhotoScene__PhotoButtonPressed_m6E24590A30480FABD356FB8AA019171E40F9985C,
	UIDancingModePhotoScene__VideoButtonPressed_mD2BADA17FD4D5576B0FCFDF2D7980EFFE5F5F268,
	UIDancingModePhotoScene_OnButtonPressed_m8D7C70FFD2E6EC6EE3479D0F0D460F4ECA886151,
	UIDancingModePhotoScene_StopDancing_m87EBB982B1E005796C37CDB3E38B3F89825B128D,
	UIDancingModePhotoScene_UITopButtonAction_mDAC3E0ACE66AED4627E5813DEDC81FA9BABAD11E,
	UIDancingModePhotoScene_OnEndSongCoroutine_m89F290715955EB9FA611697CB83DA48E49580E7E,
	UIDancingModePhotoScene__ctor_m0E148E32C4399D5DED1C286566D347F2932B9DBC,
	UIDancingModePhotoScene_U3CUStartU3Eb__7_0_m375ED372FBF72A93993944759D422B8050086BB7,
	UIDancingModePhotoScene_U3CUStartU3Eb__7_1_m3A0D131E331ABCC4569A4988CA42005C1439D3DB,
	U3CU3Ec__DisplayClass7_0__ctor_mDE9A66AAAA140145FA9F822B9144C7E2512C5BAE,
	U3CU3Ec__DisplayClass7_0_U3CUStartU3Eb__2_mEE2FE10D0A4D801F5172BD1BA07823E53D0DA54F,
	U3COnEndSongCoroutineU3Ed__17__ctor_mB70D8A90057205572BE3F7D5E9FECA6EAFBE7493,
	U3COnEndSongCoroutineU3Ed__17_System_IDisposable_Dispose_m0204E67F0D0C67B569D6BD92E85E0963C9597F7E,
	U3COnEndSongCoroutineU3Ed__17_MoveNext_m9D349CFF4726015BCDAB4AB267366E005DC7E075,
	U3COnEndSongCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29CFCDFB1EDEE5A805D444A042BD2ADECECF7C86,
	U3COnEndSongCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m7C669CA62E616A1AE62E560ABD72A8322DB9682D,
	U3COnEndSongCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m98D62F2AA5A03863FFE34C373FA39E664E84B5DE,
	UIDancingModeScene_UStart_m2018258783A1D1972B9DB41E4632C28CE1EC77D9,
	UIDancingModeScene_OnButtonPressed_mB9361184BBFE1E579EA07A9440CAE05D8EECE9B4,
	UIDancingModeScene_OnEnable_m169D5C6F6106DA9826269DD24DCB7A6D62875C51,
	UIDancingModeScene_StopDancing_m8239DA899AFCFDED45BF1774CDF261AA5424251B,
	UIDancingModeScene_UITopButtonAction_m631A9FD1973005DC88516F12F4B7DA51E50B384D,
	UIDancingModeScene_OnEndSongCoroutine_m8340878C6F8464ABCF1F114ACC192B84B5F5CD40,
	UIDancingModeScene__ctor_m56E1A9362F99540A3F40D35C8DAEF077CBF05DEB,
	U3CU3Ec__DisplayClass7_0__ctor_m9CF705BA026E90FFBCCE02BE0AADD2EC2ACD78CE,
	U3CU3Ec__DisplayClass7_0_U3CUStartU3Eb__0_m9F6D0565A7B74A48936D4E168117C5DDB817D0E2,
	U3COnEndSongCoroutineU3Ed__12__ctor_mD8486A36A5BC9054806B48AAA7EA84F97B18F87A,
	U3COnEndSongCoroutineU3Ed__12_System_IDisposable_Dispose_m726CCD8F8B06BD56E629EF882F79C010C59F8A42,
	U3COnEndSongCoroutineU3Ed__12_MoveNext_mFB04A6B16A970FE4E2FA3E640C93C4AA7CE55F2F,
	U3COnEndSongCoroutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D905BC0896E98EAB9DFB23CC59A390BB9BC169E,
	U3COnEndSongCoroutineU3Ed__12_System_Collections_IEnumerator_Reset_m47A62F444AC70D86A665237F2782BDCE2E558499,
	U3COnEndSongCoroutineU3Ed__12_System_Collections_IEnumerator_get_Current_m646069AD90BE12F77928402EC30EDA2ED2502765,
	UIGyroscopeModeScene_UStart_m141E18F7C4DDC6C6C50FACB95F7138433DA69CAD,
	UIGyroscopeModeScene_AddActionToButton_mDB0723318B41EA0E27FE5D8CA33A27D2839B9B0F,
	UIGyroscopeModeScene_UITopButtonAction_mC913F8D63C74FDFE05059A49104074F246ED198E,
	UIGyroscopeModeScene_OnNewGyroscopeStatus_m69C748815BC7EDB97C261558D6A63A436BDC863B,
	UIGyroscopeModeScene_PlaySound_m13AD3F46645AA7CA99CFB9379304680753151BEC,
	UIGyroscopeModeScene_SoundCoroutine_mAA185400DB8131AC9FFDCDC44AB734DADD799EA9,
	UIGyroscopeModeScene_StopSound_m516A9528471D3CDF59AF05588F87C763753A3AD1,
	UIGyroscopeModeScene_StabilizeCoroutine_m36476B9CCDFE1316FEAAD31D180373CC35898089,
	UIGyroscopeModeScene__ctor_m9CB896303BEB61EFE5F1EC50594590C20F626398,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_0_mF36264461536DBBFD6DE7ECD9D1E34CAAB0E929F,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_1_mB873D32976C6FD25E7E58602B2952434CF6701C1,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_2_m760EF6D69B373302935ECF25E0B9A3A54A506063,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_3_mBA768F734127A4854707420560ECD39D6F6106A4,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_4_mB65E6D27EDC5EFA2B12BB10F19791955BBFAD347,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_5_mC075B9F03AE9E611D0CB19026C01DB0398069837,
	UIGyroscopeModeScene_U3CUStartU3Eb__23_6_m9DBEF4130BE5DAFE7F8223803D32597592F1AA31,
	U3CU3Ec__DisplayClass23_0__ctor_mB69F80D478CD2025F11C5C039B9326A23C463E81,
	U3CU3Ec__DisplayClass23_0_U3CUStartU3Eb__7_m72A128EAE3013D40C49507BCDE1D24EE87E3BC27,
	U3CU3Ec__DisplayClass23_1__ctor_m66B5600E52FCBC18B851A7681A54C42CBD25C4B7,
	U3CU3Ec__DisplayClass23_1_U3CUStartU3Eb__8_m22323C02BEB1FB96687AD0BF1B06A973E533CE7E,
	U3CU3Ec__DisplayClass24_0__ctor_mB3520F95D5D7C040F26E1EE68BBF4A4E1D2080B6,
	U3CU3Ec__DisplayClass24_0_U3CAddActionToButtonU3Eb__0_mD6BBDF719F3E635D85C7FD7837DA129B35E3CE44,
	U3CSoundCoroutineU3Ed__34__ctor_m3DD5DD5C7E038C247B87D17A23FAE279CDA28A6B,
	U3CSoundCoroutineU3Ed__34_System_IDisposable_Dispose_m1D4387776066F998CDF90927917B7DCF7234B66B,
	U3CSoundCoroutineU3Ed__34_MoveNext_m162CA23B3CD8E6FB39F36C1AD2C1E4C4C08581CD,
	U3CSoundCoroutineU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m440B3DE2257AC59C3AEB36729A8BFDB018339206,
	U3CSoundCoroutineU3Ed__34_System_Collections_IEnumerator_Reset_m956302EE2A28A6248C6351636C9FB598CF071010,
	U3CSoundCoroutineU3Ed__34_System_Collections_IEnumerator_get_Current_m73611655B954D5226F1209F5B492DF8B4ED6F4B1,
	U3CStabilizeCoroutineU3Ed__36__ctor_mA29524B1BDFC05A3F9EC1239D66DCC9B693F32C7,
	U3CStabilizeCoroutineU3Ed__36_System_IDisposable_Dispose_m3CE2330EFF0DF2A8154E04C138E80626271CC858,
	U3CStabilizeCoroutineU3Ed__36_MoveNext_m736BCADF61F146881D5A6C97644133D5D6165E42,
	U3CStabilizeCoroutineU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE1BA0E4C8497B7586AB00AB8D8648787E11BD22,
	U3CStabilizeCoroutineU3Ed__36_System_Collections_IEnumerator_Reset_mAE912BA59414010E9D3605F33DD64DBB50792560,
	U3CStabilizeCoroutineU3Ed__36_System_Collections_IEnumerator_get_Current_m52C25F38575633C085680BBAF0D9074C6B2993DA,
	UIMainMenuScene_UStart_m96B69A9F1B60338E16539BD62975A25F1EFD4E13,
	UIMainMenuScene_AskAndroidPerssions_m523BEEF3F6691967439571D694BDA3AA271CB919,
	UIMainMenuScene__OnProgrammingModeClicked_m7CA146E31C11DE55382BE971F4695D2A5856BC46,
	UIMainMenuScene__OnSettingsClicked_m07E785F58BF8C56F861FE044C2241766380E2271,
	UIMainMenuScene_UITopButtonAction_m70F4439FF306C42D5BC44E36CE9A27799D38F067,
	UIMainMenuScene__OnDancingModeClicked_mF8977A72013DB80B3B67B133011A1BFB11A0F051,
	UIMainMenuScene__ctor_mF8934EDDF01C9ECD3BC3B5DCD25C041B2BEF6D24,
	UIMainMenuScene_U3CUStartU3Eb__11_0_mB98FC33B713F5BCAECF17FD9DF03A0CA33154C01,
	UIMainMenuScene_U3CUStartU3Eb__11_1_m4EBF693F78C4B05E36B9C0F8C962635687EC1E64,
	UIMainMenuScene_U3CAskAndroidPerssionsU3Eb__14_0_m6A92B97C824432FA02AE967702F3E6C0CFAD24BD,
	UIMainMenuScene_U3CAskAndroidPerssionsU3Eb__14_1_m8B2B0FF7845258FF4736F0820FE8A83183EA0245,
	UIMainMenuScene_U3CAskAndroidPerssionsU3Eb__14_2_mB629A0DD26F0765563942FE0508078C9A49F7212,
	UIProgrammingModePhotoScene_UAwake_m2BF4E651D7C6D933E895233765DEAA312EF1647F,
	UIProgrammingModePhotoScene_UStart_m72052516C36DD24EE8E43F1DCFECA72E862F4556,
	UIProgrammingModePhotoScene__PowerButtonPressed_mEB5D207D4BE1ABB5EC7DD2395D678BAD3D37261B,
	UIProgrammingModePhotoScene__PhotoButtonPressed_m36AFC0D8DF08D6BF940951E9D4F76C0A738F6C0D,
	UIProgrammingModePhotoScene__VideoButtonPressed_mB313A1AC1D048DD07D201EC7EE0D31C9B44FF007,
	UIProgrammingModePhotoScene__SetRestButtonPlaying_m05C1B6A54CDEE38E2BA1F1DC2345DE45AE768536,
	UIProgrammingModePhotoScene_OnNewStepPlayed_m1EC4CD104724DFCA06AC3641442D7EA8D9F06ED8,
	UIProgrammingModePhotoScene_StopPlaying_m7302A09432612E21044CF3A300E6311DB0A39875,
	UIProgrammingModePhotoScene_UITopButtonAction_mA8D582E0585A0F5641CF97BC752127F8096FCAB9,
	UIProgrammingModePhotoScene_PlaySound_mE0FD01B9D14646390558FC84C65D3FC6BD181F1C,
	UIProgrammingModePhotoScene_StopSound_m0B1FA961B8783E2EB07620F5708710A8C70E9486,
	UIProgrammingModePhotoScene__ctor_m880EEDE20EE757C84B2E7F0CECCA624EBA852D0D,
	UIProgrammingModePhotoScene_U3CUStartU3Eb__13_0_m5E8B00DA3A247F8C5854BB4264969DBDF68A6465,
	UIProgrammingModePhotoScene_U3CUStartU3Eb__13_1_m72181D1A9797F3153BA827953A89FA7CB528AB41,
	UIProgrammingModePhotoScene_U3CUStartU3Eb__13_2_m0F236F06639AC1A9374E76060AA85D76096B8081,
	UIProgrammingModePhotoScene_U3C_SetRestButtonPlayingU3Eb__18_0_mEA66FE49FF17B7E8CDF8C920801470B32AE6CC73,
	UIProgrammingModeScene_UAwake_mE058E577EB8970BA6DE03BEC50EEAA89789BCDBA,
	UIProgrammingModeScene_UStart_m36433A02E644AB9C129E21F2C569B7086681C4F3,
	UIProgrammingModeScene__TestButtonPressed_m8D161CA2C4297C97225E8DDDE8A1863326D4534D,
	UIProgrammingModeScene__PowerButtonPressed_m508917C62E53D9B23F25B33836CEF4D585236820,
	UIProgrammingModeScene__SetRestButtonPlaying_mA02FAB777945A63502B59549A95819E8DEB4F22C,
	UIProgrammingModeScene_StopPlaying_m206C1620641F0D8920072EB9C87359E95124BA48,
	UIProgrammingModeScene_OnRoutineLoaded_m650AC391C2B8ED9F8615CCC743D0785E52060CEA,
	UIProgrammingModeScene_OnRoutineSaved_mB165CAF51B02059A2E59D7F5D37FC244A803E8BD,
	UIProgrammingModeScene_OnNewStepPlayed_mFA9648FB2EA701EFE6A08F3F7CAA2E12C7135028,
	UIProgrammingModeScene_UITopButtonAction_m59BAD238119C4BCA1176A4E9C1AF7748304FA326,
	UIProgrammingModeScene_PlaySound_m1E58793FA0BE0DE14B0B99535CDBA9F04F3E442F,
	UIProgrammingModeScene_StopSound_mD38F1D6E8641037A90A02A9FAC719CE7C33854DF,
	UIProgrammingModeScene_OnBluetoothDeviceListUpdated_m317AE40D13883D060EF18656A94D3ECA05BDB230,
	UIProgrammingModeScene_OnBluetoothConnected_mB214535479BA69A43F5DC296AC93D6111835B06B,
	UIProgrammingModeScene_OnBluetoothDisconnected_mFD97406461F12A2F707ECF83DCAF2CE06E50DE62,
	UIProgrammingModeScene__ctor_m692F908D72CD96A48AE3557F36FF73E12C844C54,
	UIProgrammingModeScene_U3CUStartU3Eb__41_0_m600136CED779E8E594251A09E00EDF7F7CF753C6,
	UIProgrammingModeScene_U3CUStartU3Eb__41_1_mDDFEBE2D6F81A828B17C08B7B1E691B9DF5A2CF5,
	UIProgrammingModeScene_U3CUStartU3Eb__41_2_mE4089A085E5A30833821F9970617A9F7B2587353,
	UIProgrammingModeScene_U3CUStartU3Eb__41_3_m411E4C27F70072D80B65A9D7E32C796C47DE2C84,
	UIProgrammingModeScene_U3CUStartU3Eb__41_4_m7F0E39407B1607F70CEB06406BDA370E50EAC8BE,
	UIProgrammingModeScene_U3CUStartU3Eb__41_5_mD5369B2F6E299DA277F9D57FE18853466EBD4DAC,
	UIProgrammingModeScene_U3CUStartU3Eb__41_6_m9680FF86966C77E7C17DF5C017DF43F3060C2845,
	UIProgrammingModeScene_U3CUStartU3Eb__41_7_m78DC5083D24AD32C526E9C170F834C1348679709,
	UIProgrammingModeScene_U3CUStartU3Eb__41_8_m0AF6CB82BA4C2BDD50883657C467D9616F1C8BE8,
	UIProgrammingModeScene_U3CUStartU3Eb__41_9_mB8C2703026ED2313785658A4397928B3391C177C,
	UIProgrammingModeScene_U3CUStartU3Eb__41_13_mF315FE20249D26287121660A87A92AAFB605D450,
	UIProgrammingModeScene_U3CUStartU3Eb__41_10_m83C5ADF7C04BDC979E7FE0387C60F33C84D4BAD4,
	UIProgrammingModeScene_U3CUStartU3Eb__41_11_m571DBFB860E41846E456F5CB6480ED6F73FB0815,
	UIProgrammingModeScene_U3COnRoutineLoadedU3Eb__46_0_m95B66EA934FDEB64562A0A9D4DA8E28F8D303BE0,
	UIProgrammingModeScene_U3COnRoutineLoadedU3Eb__46_1_m696DD93517C87421153CCFDCDBE10020C06E3473,
	UIProgrammingModeScene_U3COnRoutineSavedU3Eb__47_0_m1E842B54248C213875E28C34797B345CA62E24DF,
	UIProgrammingModeScene_U3COnRoutineSavedU3Eb__47_1_m811EC9FE3D7B0D3B7F6595625B6396F770C0D519,
	U3CU3Ec__cctor_m710C86201C67A33B24C7BEAB162FEA8B9627553C,
	U3CU3Ec__ctor_mCDEF9D3AFFF4F179E7F8244F44107AC69E360C80,
	U3CU3Ec_U3CUStartU3Eb__41_12_m97674252F813DC99AD94015FEAFD47741C6065C4,
	UIProgrammingModeSimulationScene_UStart_m706537D6AEFBEFE8B639049CC58791EA174AC1F3,
	UIProgrammingModeSimulationScene__PowerButtonPressed_mCCCCADCC71C7285DE1042D2534C2D77552A3EBF9,
	UIProgrammingModeSimulationScene_StopPlaying_m6BE60499869931004A95F09C893D806E7F61F26F,
	UIProgrammingModeSimulationScene__SetRestButtonPlaying_m72662523AF23FEA53A4B4559A23CC15BBAD7825C,
	UIProgrammingModeSimulationScene_OnNewStepPlayed_mA99E5C79AB8A332EF55B0B60744DCC8B94D69318,
	UIProgrammingModeSimulationScene_UITopButtonAction_mA5467E250B948D6E9F9F8867646F2726D88FC5EC,
	UIProgrammingModeSimulationScene_PlaySound_m638363D19C5B3208D57990C5EDEB747D85ED4DA8,
	UIProgrammingModeSimulationScene_StopSound_m806EB915C75A9FC64B07B863213CDE41EAAA3FD5,
	UIProgrammingModeSimulationScene__ctor_m338F35FE67A4A839FEA09670EB71E192E5DE5D62,
	UIProgrammingModeSimulationScene_U3CUStartU3Eb__11_0_mDA19CE37FD735D0831F4F812DDA0B09732DDB19E,
	UIRealTimeModePhotoScene_UAwake_m86B1B6147C759D13443C73B3D7BA307902DDEA50,
	UIRealTimeModePhotoScene_UStart_mF1DB008391597EB207369CA1DEC1AECB10B10B4E,
	UIRealTimeModePhotoScene_AddActionToButton_m6E0F1B15EDD7597A611D6B548BA63C4EA41BED4B,
	UIRealTimeModePhotoScene__PhotoButtonPressed_mAF78AC70D7B731109D2EB7FCBCC763976D5B72BC,
	UIRealTimeModePhotoScene__VideoButtonPressed_mA48B79A8327DF118CC5F612172B94A005A650963,
	UIRealTimeModePhotoScene_UITopButtonAction_m9AEC3AC971F95CB6EFD528D71F9D6EFA3C309528,
	UIRealTimeModePhotoScene_PlaySound_mA3B3579C1E44516406F412F5C3AD1699993A8CEE,
	UIRealTimeModePhotoScene_SoundCoroutine_m37627DEACB6EBA3EB7E19A471775B67A49ABDD28,
	UIRealTimeModePhotoScene_StopSound_m77FF05F61C1CB29DF37AA2BE37BF9BC1F2896B5D,
	UIRealTimeModePhotoScene__ctor_m342C609EB2EEE7D301E69370D303940FCF937C8B,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_0_m2B8E329C37BEC924D8C9C902875D54401AE51B00,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_1_mD5812571B327F29A68A74A676FBFF8DDEEB46B60,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_2_m097B5542446DA48904F8C2E497ED68B948AD16F3,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_3_m9289E416544E889DC57E00B4F9A6F503C7DDEE56,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_4_m4A5C477555043CA6C7A1F6B92A6102500CD9A38B,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_5_m8C170E92E62B68D6AC8F373CDB452DDF769EA848,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_6_mC51FA53E29B33DCE246F146CAD2ED600CF0F18AC,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_7_mF101B1ACBA4EB8AA743C5AF982D42C57CA326FB4,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_8_m8D7BB5D5F60EC8658A3458C2E3F750FC2A4C2CB8,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_9_mB60D9F6944E13800595AC1C0DC5EA58B2E1C7888,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_10_m9DF2BD115902B1706A5FB4BDBD15C8EA8E5376A8,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_11_m1FF8441869B29A35B660DAF9901762CAEEFC0BE8,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_12_m74F0FAD79C33B5B4FC92A3581373FAB26EEFE86F,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_13_m2D11193C5D103A4E3B6F8B8DB6CBEF931F46198F,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_14_m2B306A8B72A5D8FA67D6EC2D66416ED404719C76,
	UIRealTimeModePhotoScene_U3CUStartU3Eb__22_15_mEC2DEC2AD742AC6B08710D5A95B31848F8AE9026,
	U3CU3Ec__DisplayClass22_0__ctor_m8556457FE80981E9224B670DA2D508AE72D8CCC3,
	U3CU3Ec__DisplayClass22_0_U3CUStartU3Eb__16_m357E715B17117AB043045E720936675BF742F096,
	U3CU3Ec__DisplayClass22_1__ctor_m9FCA61FE32BE64B21A3C23C94F2ACDDE86596C13,
	U3CU3Ec__DisplayClass22_1_U3CUStartU3Eb__17_m1236D45D2564DA2DA2CB94616C435F3E80E5454D,
	U3CU3Ec__DisplayClass23_0__ctor_m11F131070385918ACE097BBB998E7D8EA1A6B10D,
	U3CU3Ec__DisplayClass23_0_U3CAddActionToButtonU3Eb__0_mF0D73F7E2C1BEDDF620E959C5C9989BA1B6A4616,
	U3CSoundCoroutineU3Ed__33__ctor_mA29129E7915E0ED663D17579F701BD363B32E7A8,
	U3CSoundCoroutineU3Ed__33_System_IDisposable_Dispose_m9EDE82CD37F4E9ACF310B054DDABE3444FE5334E,
	U3CSoundCoroutineU3Ed__33_MoveNext_m86BD48AF3FBE793A633A6375099BC73FD26B70E5,
	U3CSoundCoroutineU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m580B3FF31CEF007BB00E3C9E93C0D07B7183DBE4,
	U3CSoundCoroutineU3Ed__33_System_Collections_IEnumerator_Reset_m0ED918FA48C48DF1140BE4AB461345D7E888EF50,
	U3CSoundCoroutineU3Ed__33_System_Collections_IEnumerator_get_Current_m6091FBF00CF9C8FA3F1E981D82852128A54F97CF,
	UIRealTimeModeScene_UStart_mB45AD01A7A40C4C58B3591C5E902C632D1F565F4,
	UIRealTimeModeScene_AddActionToButton_mDAF9E3DBEEA9EA14C0021433B4ECE423617D14FC,
	UIRealTimeModeScene_UITopButtonAction_m14C1D2AC07DD9E90C4629BE35C6DEA1E88ACC83A,
	UIRealTimeModeScene_PlaySound_m61DB7BC6E12B32BCE1B3F7C630EDE3EF9A930685,
	UIRealTimeModeScene_SoundCoroutine_m2AD9F74A79AE3BC337C9D0EDBC6A59B7E94C3EE9,
	UIRealTimeModeScene_StopSound_mA64ADA64454A8AD26BF5D4861A2E1FD019C1F90C,
	UIRealTimeModeScene__ctor_mA4AC992A5D576677F057634D74FD445ED122FB62,
	UIRealTimeModeScene_U3CUStartU3Eb__18_0_mA0C46BBE42151146CC6BAC44ABADD495B6C7DCFD,
	UIRealTimeModeScene_U3CUStartU3Eb__18_1_mBF69B2A0CAD81FB1B590193FB5C8B597E5458086,
	UIRealTimeModeScene_U3CUStartU3Eb__18_2_m88EC9C1999E2B34D95BC424AA81C9F9EA3F9BE24,
	UIRealTimeModeScene_U3CUStartU3Eb__18_3_m4D68BB6BE63EC3F64FF924702AAF7437D8717556,
	UIRealTimeModeScene_U3CUStartU3Eb__18_4_m9D62DBECBFC71F98240C87FE78839AE158689C0E,
	UIRealTimeModeScene_U3CUStartU3Eb__18_5_mFFF148B84B7346BF1074821DC8929709DCFDA284,
	UIRealTimeModeScene_U3CUStartU3Eb__18_6_mDA2E26B30E3EA4D0F1C41CBA86F5557B50215BD7,
	UIRealTimeModeScene_U3CUStartU3Eb__18_7_mB58A68E48FC7A71F9B9CE304F63DC7D0857C4D73,
	UIRealTimeModeScene_U3CUStartU3Eb__18_8_mDB5626CC47695EABF8C03C0D72D0E738EB89F5B2,
	UIRealTimeModeScene_U3CUStartU3Eb__18_9_m8BA942BA4A2EE6ACCF975B7EA17EA96A7784FF4C,
	UIRealTimeModeScene_U3CUStartU3Eb__18_10_m5EE476A0864809739E0112572D32088E793522B6,
	UIRealTimeModeScene_U3CUStartU3Eb__18_11_m57A571E7FCE5CEEA04F0AB29042AA14A9974E7DE,
	UIRealTimeModeScene_U3CUStartU3Eb__18_12_mEF69AACD3E1F768102A50E3C78ADE53EF8B677BF,
	UIRealTimeModeScene_U3CUStartU3Eb__18_13_m1E52460F4F7F6131DD752020D8DF0F47F7D24DC7,
	U3CU3Ec__DisplayClass18_0__ctor_m0D697401A49F8ABA0BE5AAC0F6895C27D5E96FE5,
	U3CU3Ec__DisplayClass18_0_U3CUStartU3Eb__14_m65AA5028EE53B0A4A5EB332CEE04A630751280D3,
	U3CU3Ec__DisplayClass18_1__ctor_mAEA09BD62A20C4B5A42A2FE857AF7B70A94735AA,
	U3CU3Ec__DisplayClass18_1_U3CUStartU3Eb__15_mCC2A097CE6FCA5FFBB90927CA15C5D8783839538,
	U3CU3Ec__DisplayClass19_0__ctor_m1DB0FE2629FFAB15F36921F10C41E907A6591F56,
	U3CU3Ec__DisplayClass19_0_U3CAddActionToButtonU3Eb__0_mFD0286BF88AABFCC4DE7423A6D39172B117E9CFE,
	U3CSoundCoroutineU3Ed__26__ctor_mBEA94C617C4AD6FC6D0DAB14688871E77F7EAE16,
	U3CSoundCoroutineU3Ed__26_System_IDisposable_Dispose_mE08FEC327BFBE8EADFD7F3A7E9622DFF17FD2D90,
	U3CSoundCoroutineU3Ed__26_MoveNext_m29863AD2C100C40FED5A3B8D442ACFACCEE76F4D,
	U3CSoundCoroutineU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BDD797107163AF497EE1C5368E2A60B94F93D26,
	U3CSoundCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m72FFAC3D0ADEFB06DDA3BE5C06A82BCAB10AFE47,
	U3CSoundCoroutineU3Ed__26_System_Collections_IEnumerator_get_Current_m6DF4F75E2D852B563239D4A906BCED4EF2E0F956,
	UISettingsScene_UStart_mF5B7DAD9A464A1CC173796208C4B68F8082EF0F4,
	UISettingsScene__OnSynchornizeClicked_mE5FD26F996FE5AEDEEBCCEBED418F9BC78313FC2,
	UISettingsScene__OnAboutAppClicked_mD34541E1DE124DC1EC31A1BBDB451BC5C0C8D4D5,
	UISettingsScene_UITopButtonAction_m62DD0BAB473F7D5FA1734296A76C8650CB988CA3,
	UISettingsScene__ctor_m34CACF920879CB586B52AA4E3309AE82E8D773F8,
	UISyncSetupScene_UAwake_m040ED1B3866A4A06CBB15729D1D9CD622E7079E0,
	UISyncSetupScene_UStart_m8171A64F544ECE10AD813B5BD89E2B4CC1CD6DB2,
	UISyncSetupScene__ContinueOfflineButton_m990A9C9965A038F7A1A98E9B7459432FE9D162FA,
	UISyncSetupScene__SynchronizeButton_m04DB7CF464DF69782DC14721484104D446521D56,
	UISyncSetupScene__DisconnectButton_mB86B246ADCE68D2D011C66795ACC57B2EFC6B92D,
	UISyncSetupScene_OnBluetoothDeviceListUpdated_mD40E119DEA5543495827EE340C62A72C45AE0069,
	UISyncSetupScene_OnBluetoothConnected_m54B5F9016A6CB939B48953FE71C69C5E879471C8,
	UISyncSetupScene_OnBluetoothDisconnected_m29A937CF28967D254DA4EE4C4767F72ADEF2F9BA,
	UISyncSetupScene_UITopButtonAction_m0BE6AC4F8F0E90EDE38EE06CF8A0A245825A166D,
	UISyncSetupScene__ctor_m19C5F143CC13F1D90E0E59CCDAF5F07A526C5038,
	UISyncSetupScene_U3COnBluetoothConnectedU3Eb__20_0_m217DF1852C52BAB37AFB32CA2A66F9252F8AF226,
	UISyncSetupScene_U3COnBluetoothConnectedU3Eb__20_1_mB79AE6756303EB3A1CE4FDFD6377D385FEB18BC6,
	U3CU3Ec__cctor_m8E95854478F00AF4E51DBBC91AECF62850CAF0AB,
	U3CU3Ec__ctor_mF6548E4432B1AD3D8712824343031B0C3E7C6704,
	U3CU3Ec_U3CUAwakeU3Eb__14_0_m2AAADCA42CE0CC67314DBDF04C47D2A36722919E,
	U3CU3Ec_U3CUAwakeU3Eb__14_1_mEADE934E140CBC52F8EF41143F79466DB0BF9A35,
	UIBluetoothButton_UStart_m9F258870EC1D1CB264435FF629839EF82039B946,
	UIBluetoothButton_UUpdate_m9BD196624FD301CF2B4025003837FFC569ADA7FF,
	UIBluetoothButton_OpenSyncScene_mA92B945A4BEDBE9AB82764D7BD80A17C77266F2C,
	UIBluetoothButton_InitWithData_m51CB469545FCCFD25382458877B78E79FD9EFBF2,
	UIBluetoothButton__ctor_mD528AC622117F537E86D310C5944378ECF181517,
	UICustomButton_Awake_mF5ED792172D0312415C75FD1A2F31615E1BE3749,
	UICustomButton_ExecuteAction_mC102F537754B90F88EBA5F9402CDB9DD3E9B0D35,
	UICustomButton__ctor_mFD2FC56C30D7A37D1BF2E74387BDD2CA950C6973,
	UIMenuNavigationButton_ExecuteAction_m4FC18D4C7B4D32C5CD3ED66EC8E9C2037C351B86,
	UIMenuNavigationButton__ctor_mEF815BEEA7FB40ABDFD479F707F49466CDB28898,
	UIProgrammingPanel_Awake_m488BFCD2B7A2588C0552BA35F9E153F2BB7A3BBD,
	UIProgrammingPanel_Start_m40D9AA76193686C1A598BA4CBF0580F4701FD77B,
	UIProgrammingPanel_ShowPanel_mD31BB1BE319A835E5FDFEEB917C97F784A470CF2,
	UIProgrammingPanel__OnButtonPressed_mBD4C08CDE9AB4D68BFEAB1D2F854D912B3F34BB4,
	UIProgrammingPanel_GetButtonPrefab_m8D587B51758BAD48EC82CC4DEEEB3E8E9A4A0C81,
	UIProgrammingPanel_GetButtonPrefab_mCA24AF61820DDAA232F74C0603086BCA77CCCB60,
	UIProgrammingPanel_GetButtonPrefab_mB79D3D8F98A530ED788ABC813024FCCAAB44A695,
	UIProgrammingPanel_GetButtonPrefab_m41BAFD6BAE20EB6EA455B504EA102FADD267C20F,
	UIProgrammingPanel__ctor_m0148261C5A1B5B47B30F930C01C6D79BE9E864AE,
	U3CU3Ec__DisplayClass6_0__ctor_m8A12614BA8A880DF83E9508CB5C50300ECBBF7FF,
	U3CU3Ec__DisplayClass6_0_U3CStartU3Eb__0_m67501FFFD47D778EFFC6ABDACBBC9F5FE3B73735,
	UIProgrammingPanelButton_Awake_mDF896DC3185821E99DADBBB18CAACA32AD697806,
	UIProgrammingPanelButton__ctor_m9E75DE0112FF7141773E6A44A4318432370B01DA,
	UIRobotAnimation_UStart_m7C17A9E3DFF1B917D027FF77C966AB155F9CE7F7,
	UIRobotAnimation_StopAnimation_mCE616DC8938E4B2D84A34309AF780B4867C4A49A,
	UIRobotAnimation_SetAction_m2E24A0BD7F7673379D62A4E267A13868DCED4878,
	UIRobotAnimation_StartMoving_m95D0ADE252ECDC9CBB144DF31189D78609181E33,
	UIRobotAnimation_StartRotating_m9A149A8FB55868C29B0C9FE51209A4C83114E5AF,
	UIRobotAnimation_SetAction_mD7AA4D627C3EFD783359026F79FB206B54A0879C,
	UIRobotAnimation_SetAction_mABCE45C6BFE1FFEA892358AC2B5BABBE1ABFFBD8,
	UIRobotAnimation__ctor_mC811CA51AD713E408F3C0CCB2A559398C1B0DD7E,
	U3CStartMovingU3Ed__20__ctor_mF8751983B2BF9B97EEF59E4525E78FEDA6164979,
	U3CStartMovingU3Ed__20_System_IDisposable_Dispose_m738174DD9920FDE512B08742B86B28073C0B09B1,
	U3CStartMovingU3Ed__20_MoveNext_mC7E156EBD4466ECB69AD5FF478F4B0ADE875F92D,
	U3CStartMovingU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77C738DFD77C76E6D8F7C2ABEEA51DBB4F6280E2,
	U3CStartMovingU3Ed__20_System_Collections_IEnumerator_Reset_m0DA38B4C519C9EE2DEB0B0B1DE847D3373A32F4C,
	U3CStartMovingU3Ed__20_System_Collections_IEnumerator_get_Current_mA7AC08E32F6E70890F1623B31261BB8793255931,
	U3CStartRotatingU3Ed__21__ctor_m21F90FE00C8DB0168B075A9A19ACABFC4FD716D8,
	U3CStartRotatingU3Ed__21_System_IDisposable_Dispose_m811767697723ED47240D70273B0B0767FE7FA2E6,
	U3CStartRotatingU3Ed__21_MoveNext_m5D4425EAF60D4830B61C54AB773C4511D9C9E1C8,
	U3CStartRotatingU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4DD8247F9B2C063D1AE21AFA5E9387DF731136C,
	U3CStartRotatingU3Ed__21_System_Collections_IEnumerator_Reset_m3FBB4FE304908E60065853B9F782AF5AAD7A64C7,
	U3CStartRotatingU3Ed__21_System_Collections_IEnumerator_get_Current_m4C75AD00B16975C0075650479384070116E27061,
	UIRoutineSelector_Populate_m6C29C1CEE867F84F6E674C8AD64977A0C672E21A,
	UIRoutineSelector_GetChoice_m7D129357AD5D6EA4CDD3AE6924879DCE4B311E72,
	UIRoutineSelector__OnButtonPressed_mB6B862B808812C18C2FBF7142323BD89702699D7,
	UIRoutineSelector__ctor_mAD587121E3C86BFAD77629E83D943471D97F2746,
	U3CU3Ec__DisplayClass5_0__ctor_m3B4C62A76B31762F989716AD911573343AEB19E0,
	U3CU3Ec__DisplayClass5_0_U3CPopulateU3Eb__0_m96ACD101F45F24C2BEAA5FF0A4182FD314B2FF7B,
	SceneActionVisualData__ctor_m699F55AADE320AF748113D748FDCC0FEAEE8878E,
	UISceneActionBarInitialiser_InitWithData_mBBE379C8BA6DF984B854D15A4B4EFC4AEA66BE44,
	UISceneActionBarInitialiser_UnInit_mA7B276AE408DE9503774FABC068D5140CD0353D2,
	UISceneActionBarInitialiser_GetVisualDataByType_m52908A46BF230C5D06F50B3B45BADF68BFF69A87,
	UISceneActionBarInitialiser__ctor_mDC01032C4610647201E2490AB80F492CC715AE16,
	U3CU3Ec__cctor_mB5411FAFE955809747DA427ED922A634B52ADCEA,
	U3CU3Ec__ctor_mC559AB49B0A771DAF3A442A5C51139C28E1CD1F3,
	U3CU3Ec_U3CInitWithDataU3Eb__5_0_m43E8E9C6C316FDDFECC6A4A65D85EA2B973241DC,
	U3CU3Ec_U3CInitWithDataU3Eb__5_1_m5A5C9A29E8261B985ED3E14395AC273E844DEC96,
	U3CU3Ec__DisplayClass7_0__ctor_m2347059AFDE9DB21B222491DC8401E58443263FA,
	U3CU3Ec__DisplayClass7_0_U3CGetVisualDataByTypeU3Eb__0_mFEAF094133D0C011D240CFA50563CCD19FD45E2A,
	UISceneActionButton_SetSceneActionType_m2B5BB559466248AD92E29C75DCC00BE1ADBD8DF3,
	UISceneActionButton_InitialiseVisualData_m7BEEBD22FC02242EB21B90540C03D3E323F1BF85,
	UISceneActionButton_DeinitialiseVisualData_mCFC1C6A62F63F1757FEBB701DB979C871FF5C30B,
	UISceneActionButton_ExecuteAction_mAF84889418DFF114850B87DDBC5CEEE7733377DB,
	UISceneActionButton__ctor_mF9F4814D0A812B55EC484EB4A1AC660AECD63748,
	UISceneBase__ctor_mD4F168ED4C224CBD57CF3C0AB34EE7AF433124A6,
	UISceneInitialiser_Start_m46792CFF88603632C69EB250E2E97FA9EAC484B1,
	UISceneInitialiser_InitWithData_m527DEFEE57D196CC4E9750AC8FAEADE10A731E10,
	UISceneInitialiser_UnInit_m213E4C05BD7486367279509F546BACE72018458E,
	UISceneInitialiser__ctor_m69C162CD06917447448DD2CB8F2757FD20BFC724,
	OnSceneOpened__ctor_mA2A69CD371825FF3DDA5F1BE66BBA7394A205A9A,
	OnSceneClosed__ctor_mC9A627C64D695B3E863A7624C1D7A6E7B37013FA,
	UISceneManager_Awake_mA21498548D3D73C6EFFDCA3EA08B95C01DD3D44B,
	UISceneManager_Start_m4D6A3BCD743F0DC427CDF50E63FD109CA8049177,
	UISceneManager_OpenHomeScene_mDBB76E668F578E0A2449F4D1B19A76922A55BEA1,
	UISceneManager_OpenScene_m86A7A5C2C4C4EE8D377120F444155FECC43ADB45,
	UISceneManager_SetSceneToFullScreen_mD4D1E886E05CCBA14647EEC1E2A4ED45D91B0372,
	UISceneManager_DisableCurrentScene_m1915766D9A092C47B1B70CBA8EC7794DCFA5AB38,
	UISceneManager_CloseCurrentScene_m5B1EB54A44DFD44C25AB32E33B824EE515E9E460,
	UISceneManager_RestorePreviousScene_m443099568928A8E51374932137D429E029B84992,
	UISceneManager__ctor_mCC24A77F740109C0DC8BE616A88FC0D632AAAD4A,
	UISceneTitleInitialiser_InitWithData_m410A78567505DFAA725DF3873948AB9EFB7635E6,
	UISceneTitleInitialiser__ctor_m67961492CF30E1263C9E295AC4B56024DDDA1672,
	NULL,
	UITopButtonActionContainer__ctor_mC1E2F607E20320604A1C3CF405BE4EDABA566119,
	UIScene_UStart_m64FE30A98842D02F8E53D22E2A59890BB600CE26,
	UIScene_OnBluetoothDeviceListUpdated_mC901A53E6428A78BFC09BD296F281CD36A6A5BC5,
	UIScene_OnBluetoothConnected_mA7BA30BFA2259E253462D71E70A8EDC19ED04BDF,
	UIScene_OnBluetoothDisconnected_mE5E1FF4E697177F4D8F41419C7FB30E5152B3427,
	UIScene__ctor_mB379C8871BC01D4DDAB3CDB0BB56F41B1C9F4F05,
	ArduinoHM10Test_OnButton_m35132D3B3E892E51EED9C75E9F763856B7C55C6D,
	ArduinoHM10Test_Reset_m6E0CA937E454CFBDF6D26D732E1A20A5D6671403,
	ArduinoHM10Test_SetState_mC716B9182D126EBEAFADC91E89D772E0263DA979,
	ArduinoHM10Test_StartProcess_m8491400CDC3D27D5E77233FDA82DDA499BF6B4C3,
	ArduinoHM10Test_Start_m8C159A224C2185497E2212005A98902FE70D3FED,
	ArduinoHM10Test_Update_mA0011AB86A4471ABF01812FDB5DD86D2493EA08D,
	ArduinoHM10Test_FullUUID_mB6D69584D381072539E770EE3FC8FFC283C95CCB,
	ArduinoHM10Test_IsEqual_mA3F8EC92DDD423AFF8A4218B375BA75EDB693563,
	ArduinoHM10Test_SendString_mCA8D3274B8A4BD2D8EDB41E8E8FA8066D4CC5BBD,
	ArduinoHM10Test_SendByte_mC95545DE61432BF6017660BAE71E095B438D4F89,
	ArduinoHM10Test__ctor_m8F33BF8AC9D83124CBB478434DA80E1B84449E2E,
	ArduinoHM10Test_U3CStartProcessU3Eb__17_0_m36CE56CE68D3AA9F193AD27C9A20F7D0860CBB30,
	ArduinoHM10Test_U3CUpdateU3Eb__19_0_m7FFD8CF46039C98FE844A9A0AC854141D23C92FD,
	ArduinoHM10Test_U3CUpdateU3Eb__19_1_mA821486F22C40F40195F9D7983307E099B33739B,
	ArduinoHM10Test_U3CUpdateU3Eb__19_2_m7BD33C7816181A95720FE6D6CAF234486F3A2660,
	ArduinoHM10Test_U3CUpdateU3Eb__19_3_mDFEC7D0368D8AC4367C39631B6B632C16FA197E6,
	ArduinoHM10Test_U3CUpdateU3Eb__19_4_m8AE840EE065907AD031106B5E9B8B69C4F8C4BB6,
	ArduinoHM10Test_U3CUpdateU3Eb__19_6_m315891E662945ABB620C6EE79F18C36222424399,
	ArduinoHM10Test_U3CUpdateU3Eb__19_5_mEADDC51187F60DE571AF2921260C9A99CE1CDD9A,
	U3CU3Ec__cctor_mE78EC7643CD81ECAF4E3E99514C8697FB4230D57,
	U3CU3Ec__ctor_mD8E130AE3B755B4CB7ED3DCD893EF5A8DB3E9CA2,
	U3CU3Ec_U3CStartProcessU3Eb__17_1_mFFAB43C4B031B65171782557077EBC8F910B9133,
	U3CU3Ec_U3CSendStringU3Eb__22_0_m92A9ED888216707680C3A9B18B0968C799BEC000,
	U3CU3Ec_U3CSendByteU3Eb__23_0_m3A6C8566016F763A639E28466D8A419842DCCFFF,
	BLETestScript_Show_m44DAE6593ABC230177BA83EE5D1CED8200FF95D0,
	BLETestScript_Start_m419B6CAA6B3923D8889B8B76F03E9C902558A3C5,
	BLETestScript_Update_mD4EDD5F8DFEDE83C780222749932C3B5DE8D8ECB,
	BLETestScript__ctor_mF8B5330ED0A1C41A3172E30110E01685379479BC,
	BusyScript_get_IsBusy_m053AAB00709798F01F635E48D416B14559FB1228,
	BusyScript_set_IsBusy_m8F3F55E507FC7B112EE9DD0E27C0AB513BC4E46E,
	BusyScript_Start_mEDAE9C7175C8EC120FA22D47D36AFED0C0CBE5EC,
	BusyScript_Update_m14406B0FB0CE4092E25AF6241C2562EC1FBEE5F7,
	BusyScript__ctor_m1BB43F81288D9AF44EB71F0DBE0549B009844928,
	CentralNordicScript_get_Connected_mB4D10D0EE38ABB0FDA88001D9AE864EC1958655E,
	CentralNordicScript_set_Connected_m0B78ED9E8EDB2820B0A3DEB5FD229C1F7C074E2D,
	CentralNordicScript_Initialize_m72E1E64D62EED7EAFB982D6B42A6A424770E8D91,
	CentralNordicScript_disconnect_m783495C03AB660377C4A20AA5012D63C52F3DB0D,
	CentralNordicScript_OnSend_m54F39835F62A92CE61010819B694DC05CA64FDD7,
	CentralNordicScript_OnBack_m0BFD74C76F8FE7058AEEB405F982560A69A202F4,
	CentralNordicScript_OnConnect_m7BE0E5AECB0FCB7180AFA2542C547AED0EEA2CC7,
	CentralNordicScript_Start_m3E4FE04FD15692F00CFFCF694EBFE21E6E04BAD5,
	CentralNordicScript_Update_mE0B45FC1AB613823FE2FD2586507D4AE447C55E1,
	CentralNordicScript_FullUUID_mE618DF60A11E3BFE6C46269DCE44682562671436,
	CentralNordicScript_IsEqual_m475E8BCA5BB1AD13CD223FDBD2BE93E332332116,
	CentralNordicScript_SendByte_mAC095DFD459FD629271F9422755FDE7DCD708007,
	CentralNordicScript_SendBytes_m331D5E4F3A4F5DAB03BE2C20C5008A77FBC50C00,
	CentralNordicScript__ctor_m79F1FCE65DE23FAA6626CD29879D985DDC524689,
	CentralNordicScript_U3COnBackU3Eb__22_0_mEDFA1DEEEA1FAF80692CA440831CDD267E013342,
	CentralNordicScript_U3COnConnectU3Eb__23_0_m2142E584DF8C7AFAD4C54A4EEB30F68BE139E30D,
	CentralNordicScript_U3COnConnectU3Eb__23_3_m2665CE99F6C11A6795173C21CC3E88D29CEA50BD,
	CentralNordicScript_U3COnConnectU3Eb__23_4_mE2B4E2AAB01E40F8084C8DB006E3EC51377ED262,
	CentralNordicScript_U3CUpdateU3Eb__25_1_mFE8663531EC31E743D78C1348FB27199873EE008,
	U3CU3Ec__cctor_m6A8E460F4929AB0ADB3D7DD2841B886FAFAD6C20,
	U3CU3Ec__ctor_m75944093E3490CCAEA7B560225D751F00C517993,
	U3CU3Ec_U3COnConnectU3Eb__23_1_m560B54A3757F044B002ECE4E28001498991CA902,
	U3CU3Ec_U3COnConnectU3Eb__23_2_mEBB8CAE69D2C0842B0AB2852EC058AB18F3BDC70,
	U3CU3Ec_U3CUpdateU3Eb__25_0_m015EAD2038EE0AC6F5E4C5AB5341CB777F1E7045,
	U3CU3Ec_U3CSendByteU3Eb__28_0_m66A16B3E3332FF1519C560B45E3AFF382785A038,
	U3CU3Ec_U3CSendBytesU3Eb__29_0_mD9A4B4857C1925C052CDD2FECAF95FAF3FC67D6F,
	CentralPeripheralButtonScript_OnPeripheralSelected_mFF58F44D562A4FABE685A636388DECCC1FFF307A,
	CentralPeripheralButtonScript_Start_mC040BEE6056BF2DDBAD87DDB01B92C5CE6B47AB8,
	CentralPeripheralButtonScript_Update_m14BD22785B9A9A5ED812F0EE209C616BB8B1E2D5,
	CentralPeripheralButtonScript__ctor_mD0E46BF64008D537A7E0CDF8A6CC8DB4DA64D195,
	CentralRFduinoScript_get_Connected_m20510ED622A2DE87D5FEDA43D1DA9102DA080786,
	CentralRFduinoScript_set_Connected_m72B6D235967E98F1138524B406D5447831891AD4,
	CentralRFduinoScript_Initialize_mA8704F8353E1EF1E90BB24C61FBCF8355E3AFA95,
	CentralRFduinoScript_disconnect_m2F236AA8C44FBB3EB841AD70FE227C11AC7BC31F,
	CentralRFduinoScript_OnBack_mB1DCBC5552F9DCFEAC74F4E2AEB232CF2950A02D,
	CentralRFduinoScript_OnConnect_m832DE752BFB7C1E3B051A4A1BA904D4FA6FCC8E8,
	CentralRFduinoScript_OnLED_mD3AA209F374D10BA0F6DDC07AC1A1ED31A5EF79F,
	CentralRFduinoScript_Start_m7D632025823CA9084FB5CF1E54DBA531A78C7FFE,
	CentralRFduinoScript_Update_m697B777AF8954658BFB3DB150B2F07EC5B33E707,
	CentralRFduinoScript_FullUUID_mC5A378CCEA5E95F81E6E9D9480899F9B8928D387,
	CentralRFduinoScript_IsEqual_mB57FD5C1E73B16F6A7D49934D019B7068AF02A7A,
	CentralRFduinoScript_SendByte_m718437E0D5C987EAE18D8CFC84E0E253B6E32F09,
	CentralRFduinoScript_SendBytes_m5E5E454978C05AA38D26253E0381C037C89828E2,
	CentralRFduinoScript__ctor_m1F59B804BFE09506CB71D14B2FC929AABC790348,
	CentralRFduinoScript_U3COnBackU3Eb__19_0_m7087F3E0F6A52F3768C1F138CD245107D307A156,
	CentralRFduinoScript_U3COnConnectU3Eb__20_0_mCC9D7769D92795CAAC3D3CE6C0F2E26D3948F007,
	CentralRFduinoScript_U3COnConnectU3Eb__20_3_mEE735F883A8B38CDC665CDA1F5F4CDBD3C5FD5A2,
	CentralRFduinoScript_U3COnConnectU3Eb__20_4_mD335DD02D3CC6E4BDBF40858B4D4B5E2B37AC662,
	U3CU3Ec__DisplayClass20_0__ctor_mDC8ADB47D0DA675CE609B2ACC9FCDE46A6662F08,
	U3CU3Ec__DisplayClass20_0_U3COnConnectU3Eb__6_m3B2CFBE21A384D3B2C21A10027EFF7D58747DB5E,
	U3CU3Ec__cctor_m69377EAA0FD4291F00042C5AA39964104BE01519,
	U3CU3Ec__ctor_mC27DDF998C3AD274FA1988D8A900BB1564928467,
	U3CU3Ec_U3COnConnectU3Eb__20_1_m82A41C0804885F65F6ABC3EB1EB356BEFBE0390C,
	U3CU3Ec_U3COnConnectU3Eb__20_2_m8E5A417D6B76C4292AAD52334470EFC329F8997F,
	U3CU3Ec_U3COnConnectU3Eb__20_5_mD91A062BD332C9D3ED2512089C941286A3D3F225,
	U3CU3Ec_U3CSendByteU3Eb__27_0_mB8EDBFE3CEA981AAFBBF49501BEEBFC29D77B914,
	CentralScript_Initialize_m0978E4A9B16C8BFCE2225E6E9D5C46461641B8C2,
	CentralScript_OnBack_m9D19A090DBAD93723C422BADECB2017FE395E80B,
	CentralScript_BytesToString_m5D9B7D139F549EC76B12BE240AD63154AEF4D959,
	CentralScript_OnScan_mC39BC4344D05729F2DE42866ECE7FC63BF22CCEE,
	CentralScript_RemovePeripherals_mCA781D0EAE2A33774A0FB9D173FF71E2BF249625,
	CentralScript_AddPeripheral_m6C757EF948943858604AE2DD5E9D0BB864157E92,
	CentralScript_Start_m8753BD4B9B6DAAF1CD0B28B46B1CC8E1A0E069DF,
	CentralScript_Update_mFAE4CDCB729EA6C21E3719B18E0EE1B6C5413AD3,
	CentralScript__ctor_mA48C8F08550695BADCE5FDAEE879E3B2A58E34E3,
	CentralScript_U3COnBackU3Eb__10_0_m7ED365E89B5222E657B395005A18BFEB265B5B42,
	CentralScript_U3COnScanU3Eb__12_0_mA8C989B99223D88541E6928784E5EFAD0FDC790F,
	CentralScript_U3COnScanU3Eb__12_1_mD2ABEB0178A64FBAE05078028B298018092C6F95,
	U3CU3Ec__cctor_m41EEF78877B3DE37C2CB4E6DC659803C81148A4B,
	U3CU3Ec__ctor_m2DD03A7AED33D9960ACCDEA1E333919A5D599AB7,
	U3CU3Ec_U3CInitializeU3Eb__9_0_mCFF0125B90918A91EB3495AE2ADCACCAFB83FEBA,
	U3CU3Ec_U3CInitializeU3Eb__9_1_mAD2797F80B8CBD58E485F903AAF7BE0D3A8AC75E,
	CentralTISensorTagScript_get_Connected_mB7F2C137CDF052907A725053DA9C662F77296AAE,
	CentralTISensorTagScript_set_Connected_mB695AEF960E96E06D78165C8A32CEF65D1735852,
	CentralTISensorTagScript_Initialize_m067928A42BA9ACC712562D23CBBB4BC9CC519728,
	CentralTISensorTagScript_disconnect_m2C941948BCF094242A83A1E706B08E7F5C2B732D,
	CentralTISensorTagScript_OnBack_mCC21259E7191B1CEE4B4EB012FD943CB3D675779,
	CentralTISensorTagScript_OnCharacteristicNotification_m76A339033B8243BD51D8980527FC1CA74CF0F72C,
	CentralTISensorTagScript_get_TemperatureEnabled_mEEE95C38C687641D6F8FCD417BDAA037C2E8F2E7,
	CentralTISensorTagScript_set_TemperatureEnabled_m502C3B0745C87A39D7F4AC07F7FCCB35DDDDF379,
	CentralTISensorTagScript_OnTemperatureEnable_m26752055984FB52FA829ED7257EA526CA758E030,
	CentralTISensorTagScript_get_AccelerometerEnabled_m0BEEBE4E1765B941368545C830B84C6C827EF58D,
	CentralTISensorTagScript_set_AccelerometerEnabled_m981B7DA07BDE9FC2EC121CB8D5E5E6EC87967E24,
	CentralTISensorTagScript_OnAccelerometerEnable_m75CB038A90EE2168ED9FC2B063EABCEE6D8D124B,
	CentralTISensorTagScript_OnConnect_m5A9791F233463B0B969324313E1E3F23E9E6C4CF,
	CentralTISensorTagScript_Start_m2696A94C8F59B2B6AF1FC9CA3E3652482F6A2AF4,
	CentralTISensorTagScript_Update_mA74B1D6133F0CF08228DFCB26A5F3C1ECB7F2414,
	CentralTISensorTagScript_AmbientTemperature_mF58E0CFC5E974CFA83393A48FA160445FD1525F8,
	CentralTISensorTagScript_TargetTemperature_mC39D04D4FF69587E96FC2558D1890DCF601318FC,
	CentralTISensorTagScript_CharSignedAtOffset_mD8CFF14A1350C74475E5F38BEF4A235370CF13C5,
	CentralTISensorTagScript_ShortSignedAtOffset_m056676E8BA2D767FE98489E1FEDA98193071843B,
	CentralTISensorTagScript_ShortUnsignedAtOffset_m997A0962166040ABFE0EC576060222439D7B7051,
	CentralTISensorTagScript_EnableFeature_m6E917D612E797BD6EA107733D8167F2BD2EF1F65,
	CentralTISensorTagScript_FullUUID_m8F5C96109EF513FD218B0E89269A47413722ACB6,
	CentralTISensorTagScript_IsEqual_m23C63B8C5A306BF79D10D5C12A7FF26C50E96E03,
	CentralTISensorTagScript_SendByte_mB218754C562B5C857D7173B68BDC35E7A801E8AA,
	CentralTISensorTagScript_SendBytes_mA82CA88B7A85782CC73FB5A22F9188739C8E946C,
	CentralTISensorTagScript__ctor_m4591A737A97550438303C43A2395ED1F118EA421,
	CentralTISensorTagScript_U3COnBackU3Eb__27_0_mE21F73030B2AC0EDDCB78B0653AFA7F454C8A48A,
	CentralTISensorTagScript_U3COnConnectU3Eb__39_0_mEE9F19B743FC658B7FF6E42BF5A4353839C54F7F,
	CentralTISensorTagScript_U3COnConnectU3Eb__39_1_mE48ECDED84C58A11E9C5969ED8989561CF311502,
	CentralTISensorTagScript_U3COnConnectU3Eb__39_3_m1C283F12E9B7E1F32A11FC6539E8E8E066C8AEF0,
	CentralTISensorTagScript_U3COnConnectU3Eb__39_4_m985D75FEC053B42DEC8FDB2CBDAA7879BD82ADEA,
	U3CU3Ec__DisplayClass32_0__ctor_m94E6FBE0A52BAD4780A01C53BD786A7D9D38AAB6,
	U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__0_m131177BFC4483F7FE6C11EDE5228F3DE455FA96B,
	U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__2_m2BD0B640987509255E2FC540A30FAC85B9E3715C,
	U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__1_m11592206803A91127CA6AE24EDE32FD151D2CAB4,
	U3CU3Ec__DisplayClass32_0_U3Cset_TemperatureEnabledU3Eb__3_mA5F8098F407C003C6BADD666BA0AA01143272865,
	U3CU3Ec__DisplayClass37_0__ctor_mB3744DC6286098C5BF004E9291D4F18B20E4010A,
	U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__0_m96C60E416F26FB6F34659190FD194F5B9D4D6884,
	U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__2_mC2A59E4BC6412BA1B63D4D10C0A74FF54D385B92,
	U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__1_mADF2E99AFB4F28204138D9C78E5A6B4D93754662,
	U3CU3Ec__DisplayClass37_0_U3Cset_AccelerometerEnabledU3Eb__3_mFA3820B130C07B301F23DF610BF9875FEFD2EFA0,
	U3CU3Ec__cctor_m3B0E7CED6B7E3072A62C0BDA596D77B3A9A26122,
	U3CU3Ec__ctor_m733791A8C5123BF114A63E6E734BDF66FE47C15F,
	U3CU3Ec_U3COnConnectU3Eb__39_2_mC2B5E9CAD312DF5F8FF7D9D2869D2D11D78A4397,
	PeripheralScript_get_IsAdvertising_mBE94A46583148D318161B8FA08746F11B44120EE,
	PeripheralScript_set_IsAdvertising_mCE8CA30098CC427AA0266BEE74FF3D806B47A970,
	PeripheralScript_Initialize_m093F1CDC1A67F4A0682EA2F341E370441C314DC4,
	PeripheralScript_OnBack_m3F79CD4B20AF8B827304A44347C42CC1D022E885,
	PeripheralScript_OnStartAdvertising_m9D39DB3462F4C3A9FCAA0E824BBFCF029BE958AB,
	PeripheralScript_OnButton1_m53A14D555369954DB73F3CF9D9F4EEBFDC7B14FF,
	PeripheralScript_Start_m45DBB471F29CB0ECD212B12A3792BF25A23BF076,
	PeripheralScript_Update_m17CD3DE2DF754B770A1B5E2FE8247D9DF75D0741,
	PeripheralScript__ctor_mB2122C6CD3B43BACFE1A478ADC8D08162B9D9C47,
	PeripheralScript_U3CInitializeU3Eb__11_0_m34A5A132CE918EDB975268E83B9AA6AFB0B1373E,
	PeripheralScript_U3CInitializeU3Eb__11_2_m39B8669653FAC17847340087FEDB5A69FE2915C5,
	PeripheralScript_U3CInitializeU3Eb__11_3_m0C09DF069961ACBCB9008C934646CC3361E43E03,
	PeripheralScript_U3COnBackU3Eb__12_0_mF4DC435670714AD4819ACDC4A97FD1936088260A,
	PeripheralScript_U3COnStartAdvertisingU3Eb__13_0_mE0D49D91F4470D4B81737597F5A62FDABE2834E5,
	PeripheralScript_U3COnStartAdvertisingU3Eb__13_1_mC4CDF009EFB10744DA26A115D4AE635716C2CF80,
	U3CU3Ec__cctor_mDFB60C35C9DF473D1025C6DA6C4B967E778D3695,
	U3CU3Ec__ctor_m33E34EF634E8B745E21A0600C03A7C9B7F778EA8,
	U3CU3Ec_U3CInitializeU3Eb__11_1_mDE0A2D427F95E39F34D735922FA613A53D3E9D32,
	TypeSelectionScript_OnCentral_m7FD1E97097B5A048EBEF7DBE77CACE2A8EB3FD8A,
	TypeSelectionScript_OnPeripheral_mC81264BAE3333F1C513BAABDC7C554568D89B434,
	TypeSelectionScript_Start_m2D3A1E8144C78329439A3B04892F6F014DCAC1A0,
	TypeSelectionScript__ctor_mF0B6024CFA2EFB3D34CC310B2386EBA9F037C90E,
	iBeaconExampleScript_Start_mE12533AC500E0A0EEBC4976EC40171E3A656BC4A,
	iBeaconExampleScript_Distance_mDEF0A10D0D1AC4B1A416DC0332078F1420BABDF9,
	iBeaconExampleScript_Update_m344CB6059B26C94FBAD76DD5E6D2F923B8CBE735,
	iBeaconExampleScript__ctor_m11B3ABE05E3549CCEDB1CFEC8D3BB12B21C1B96A,
	iBeaconExampleScript_U3CStartU3Eb__6_0_m5A04CD1F43207010FAD424E6145119ED20014BD9,
	iBeaconExampleScript_U3CUpdateU3Eb__8_0_m19806E1BBBDC1C425677FD1C8DE3D8CB4F140206,
	U3CU3Ec__cctor_m4993D19C65869E19D658DED647919196B52B1584,
	U3CU3Ec__ctor_m22878EA6C45B2A8A4615F4D981C7D76A7AA8497B,
	U3CU3Ec_U3CStartU3Eb__6_1_m5133AC403E9880F614B3088C42E41FEE1B559416,
	iBeaconItemScript__ctor_m0AF4014B443A8AB590D0F56A80FA37A9CE7EDE54,
	DeviceObject__ctor_mE090EDCDD1FB691D9EACF3522E91C067102D6F74,
	DeviceObject__ctor_m6A6654880F94D7D8A7900543BCBF737656A94DBF,
	FoundDeviceListScript_Start_m3F1C07D5694A849CEC04532FAE1BF5DA9B9AE086,
	FoundDeviceListScript__ctor_m11A65DDA40FD186744AB950BF15E9A9D1FBA4CB2,
	Level1Script_OnScanClick_m22231BA949C2D38047E56957DFCA0ADFDF578835,
	Level1Script_OnStartLevel2_mF2C3A93EB77EFCFB69549CA43C4096211BB932A0,
	Level1Script__ctor_m965D1608582E72436468DB0406A73314ADEE2EEE,
	U3CU3Ec__cctor_m853EDED29231D330FD822AF7642579B39C494528,
	U3CU3Ec__ctor_mB35F80B1CBCC080A78FEE3C6A7F6313EB1C36CF7,
	U3CU3Ec_U3COnScanClickU3Eb__0_0_m05364810185D819E1296523100444D8535D2DECF,
	U3CU3Ec_U3COnScanClickU3Eb__0_2_mA84D57366C68FF13646F16E786B460BD0A7CEB2C,
	U3CU3Ec_U3COnScanClickU3Eb__0_1_m5A1AACBBF71106FA49977250CDCD9F997A993C9F,
	Level2Script_Start_m222ECCEE69D8F0F4DE2F8FCE40228242AFD1560F,
	Level2Script_OnCharacteristic_m676D517DCEEA51872E8CB5E6A8A9B39742CD4707,
	Level2Script_OnSubscribeClick_mF1BFBCAED3E17B14C473EA655D19ED75161FC796,
	Level2Script_OnButtonClick_m5B19F725271C5E0E4CC9A8A7072603C90D267306,
	Level2Script_FullUUID_m84FE10A19C25D264DED67BCF0FEF51777E6C2353,
	Level2Script__ctor_mD3E1FC4F5E33B58AE34EE1C1B61C5D7152EDF0EC,
	U3CU3Ec__cctor_mA6154BDEDA26CF2335EA3AF750C54ED0DD3ABB3A,
	U3CU3Ec__ctor_mD36CA45D8E35F45299DBA81A924AE72FC5007BA1,
	U3CU3Ec_U3COnSubscribeClickU3Eb__5_0_m8C5D3BE4EA21484F4EA7D3D2B9AF799575FD898F,
	U3CU3Ec_U3COnButtonClickU3Eb__6_2_mF61D472EB0362B48AA39A5559877306486E5F784,
	U3CU3Ec__DisplayClass6_0__ctor_m2C0C20881BE1D52FA31F0217AC977398DB9D9FC0,
	U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__0_mE13DBFF3AA5C8107A793E9140351DACD17B87502,
	U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__4_m74661962373AA309E09591FE93CFD444539D2B3C,
	U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__1_m0F84027C9D91ECE90F5B5A9469CDA8B6A64EDF91,
	U3CU3Ec__DisplayClass6_0_U3COnButtonClickU3Eb__3_mB7D08797F6D47F61601D4566FF7A971B938B3B00,
	NetworkExampleTest_get_AllCharacteristicsFound_mA5DC9422D6E455BC2504FFBFC70551C83DF7A38F,
	NetworkExampleTest_GetCharacteristic_mFA15AADD72536F934283D20BD2DBA4E164483C0A,
	NetworkExampleTest_set_StatusMessage_mFF8F8F1A9B1123947E7CB4A9C10EFB02D61035CA,
	NetworkExampleTest_OnButton_m0A03681FC8D18625BC90FA9A860F4271477868F5,
	NetworkExampleTest_Reset_m3C63CF541DDEE63A714E195BFB95A403709E6F9B,
	NetworkExampleTest_SetState_m04548AD181831D96BFAD2DA1D24A6D6290A93E10,
	NetworkExampleTest_StartProcess_mC7D4ADC4757F9296C7EAE11AB17DBD3C07B76627,
	NetworkExampleTest_Start_mAC97BE5F3CDC86B84A6551DA44328DC135C06B52,
	NetworkExampleTest_Update_m43895EA27FAB0F6FC239CB158233846927AD062A,
	NetworkExampleTest_IsEqual_m950B533BA38B0C814F5AB8FC52390FD688A0AD90,
	NetworkExampleTest__ctor_mB4EF9134B3A6FE5E022E355B0F82C4985D89A4EC,
	NetworkExampleTest__cctor_m2E372A1C57E6E8A93A414FE33C75C400CCC82868,
	NetworkExampleTest_U3COnButtonU3Eb__20_2_m37F1B0C7042F33D4158305C65BB5BF7F5010F5D5,
	NetworkExampleTest_U3COnButtonU3Eb__20_3_mB2EB84E726EFD7533C11AE4C86FFF50D964F7288,
	NetworkExampleTest_U3COnButtonU3Eb__20_4_mAA214E57BF35B422A9C21FC17F8C851D71ADE6B3,
	NetworkExampleTest_U3COnButtonU3Eb__20_1_m37F3A199B6261BC58CF28CA6DD8950AF7199958B,
	NetworkExampleTest_U3CStartProcessU3Eb__23_1_mC3CCC037117F008D48F2E156BE979D112A11A37B,
	NetworkExampleTest_U3CUpdateU3Eb__25_0_mFEB49F11F15755663C83F39340D89285FCCF4B95,
	NetworkExampleTest_U3CUpdateU3Eb__25_1_m95A9A2A9E0052B54483A0FE70966F148B8BF2DC9,
	NetworkExampleTest_U3CUpdateU3Eb__25_2_m5626B2246956E2E3DD88DD150786658D187A7E67,
	NetworkExampleTest_U3CUpdateU3Eb__25_3_m9835E71C44C1BF1090809677FDFAD7C9ABC089B5,
	Characteristic__ctor_m420E5DC74EF033AE15161A7EDCB9CE08A5FDCA2F,
	U3CU3Ec__cctor_mEF08672377086230E16E84410DCCBB053CE02E5C,
	U3CU3Ec__ctor_mD2DE66AF526BB6DD578852404D06040798FC0B28,
	U3CU3Ec_U3Cget_AllCharacteristicsFoundU3Eb__10_0_m08D35E079583C95E540B7F278BB6B66669BF4B12,
	U3CU3Ec_U3COnButtonU3Eb__20_0_m776E530755002F4564E3A9475BA3861EF08941B5,
	U3CU3Ec_U3CStartProcessU3Eb__23_0_mDAF160B65CC95D8E48CE310891900967E790C804,
	U3CU3Ec_U3CUpdateU3Eb__25_4_mC008716C143E64AEACD193C2D16F51C0A766ECB9,
	U3CU3Ec__DisplayClass11_0__ctor_m7D220B79A9FB9717E7CD0F38A14EA442B184758E,
	U3CU3Ec__DisplayClass11_0_U3CGetCharacteristicU3Eb__0_mFB7660D53AEF6E8B5C12B643BDFA8B90011AC943,
	ScannedItemScript__ctor_mCB4CA033D4B1B39258584BC06BC7B25196497AA9,
	ScannerTestScript_OnButton_mC9E7979EFE3346A12E9A76D3BF09BAA4D9A904E3,
	ScannerTestScript_OnStopScanning_m9A966894A1AADD160B4AF393609255CBBEA26769,
	ScannerTestScript_Start_m481E51D8E43FBFD9A261C5E732F525CF034C21F6,
	ScannerTestScript_Update_m07240E7DF1152543BD37823CEE1BE959C0C7CC66,
	ScannerTestScript__ctor_m495FC70CB6DCF75540B96469159A8C0F90CB01E7,
	ScannerTestScript_U3CStartU3Eb__8_0_m1E4AF5A07F2C5026F3D042D50C0A1EA49EAE144E,
	ScannerTestScript_U3CUpdateU3Eb__9_0_m9974D219746A29786BDCF7A01827BAD1F82D7700,
	U3CU3Ec__cctor_m307241419024A810E34D0E111CFAAAFB9334D54B,
	U3CU3Ec__ctor_mBBAFE31356C8A6D31EF45095F68CF38DAC4293D4,
	U3CU3Ec_U3COnButtonU3Eb__6_0_mED4215E85EB44D0BA7507CDE4313FF6E46D7579F,
	U3CU3Ec_U3CStartU3Eb__8_1_m57973128606DDD35C40CA66B93F0925C24E0995A,
	SensorBugTest_get_AllCharacteristicsFound_m33075EF2FEB9E60BB4FA738EC4DA0DD190CADFA8,
	SensorBugTest_GetCharacteristic_m6D9FFBC8655AA66847C84406AC83DA37AFABBE64,
	SensorBugTest_set_SensorBugStatusMessage_mD3A5202E28E5A3D6A635261F2A45A69742F78587,
	SensorBugTest_Reset_mDE8104994D7B69CF76B259282877739C8EE88C1A,
	SensorBugTest_SetState_m0C9CC41A7EF281487488FD95F5AEEE31A3B72E84,
	SensorBugTest_StartProcess_m3CE0B838BB42459744DFABB1B6B474231B68127B,
	SensorBugTest_Start_m31A103A54C1C078E403A39EDC15A81723E45C1EE,
	SensorBugTest_Update_m71B88BDD303887AB954EDFFA8D03CF2EA32AE770,
	SensorBugTest_IsEqual_m7628CEC6FC6F70B8C87D1D9D896820C7697DA45C,
	SensorBugTest__ctor_m63BE02B1B067751A5EDD743C6DC746C2359BE503,
	SensorBugTest__cctor_m00B411B6529224A74BC1041517C7BF820C0697B7,
	SensorBugTest_U3CStartProcessU3Eb__25_0_m85E0CAA6F2459A40E04B1F01E37045FDF9153532,
	SensorBugTest_U3CStartProcessU3Eb__25_1_mFA6596ED773F248128D73D331506D68499816011,
	SensorBugTest_U3CUpdateU3Eb__27_0_mA8469B4049030722819FF53D8BCF81D7F63A3DA7,
	SensorBugTest_U3CUpdateU3Eb__27_1_mDECE6B9EF3E702D27E9FDC72876239098E1F33B2,
	SensorBugTest_U3CUpdateU3Eb__27_2_m7AA858938C103785CB5FB16F528AAAC16F300AD1,
	SensorBugTest_U3CUpdateU3Eb__27_3_mF0323734218D7C6F9CD0C7CA7044422EBA99DB21,
	SensorBugTest_U3CUpdateU3Eb__27_7_m253B16D5D7242C0C06E077221D46EF864CAF9021,
	SensorBugTest_U3CUpdateU3Eb__27_4_m1E70AD0D9A91B4E83542C5C0FA89E21C84014A98,
	SensorBugTest_U3CUpdateU3Eb__27_5_m87A2C9210740362D86216EABDB4DCA168A075727,
	Characteristic__ctor_m266178646E61F88863548B588E2AD3B7A68F73CE,
	U3CU3Ec__cctor_m18A0A1758406011AB3CB42882123DB5F59B4D21C,
	U3CU3Ec__ctor_mDAF968BB3AF2BDD5F6A7F651178705CBDD0BE5BD,
	U3CU3Ec_U3Cget_AllCharacteristicsFoundU3Eb__12_0_m2E1ACFC5B850F4A5B50E3489B4058B43499506C6,
	U3CU3Ec_U3CUpdateU3Eb__27_6_m7FFB4EF6D637E6F96DB518C9A3F67C4E3E9A0928,
	U3CU3Ec__DisplayClass13_0__ctor_m36BE155F1C26F2675DAAC6CFB14FC264DF958C2A,
	U3CU3Ec__DisplayClass13_0_U3CGetCharacteristicU3Eb__0_m79608C0B32BA8CA93ED708FE350305918FEF39C7,
	SimpleTest_Reset_m80E513ECB92683264167AF85DED868621137E410,
	SimpleTest_SetState_mF962EB279E59AEE870D2A5331FE96F7EE7192A7C,
	SimpleTest_StartProcess_m12581D538084A6CDAE1B9512300021570B8CFDE5,
	SimpleTest_Start_mDB6704185764EDC0B2D7562DFA1F7A85E9B359BC,
	SimpleTest_Update_mFBC1EA9EA621A9B65A227AC4E3C937F6E83DC659,
	SimpleTest_OnLED_m6EE42B4201AB3758DCE645F8CD0BD15D40E232AF,
	SimpleTest_FullUUID_m5AA6C63647B01186CF35C5B3C381FF354A1749A9,
	SimpleTest_IsEqual_m613C6F3963C31C1A8EEB00E190975B25DE356B6F,
	SimpleTest_SendByte_m9929428B66E5530CF6D574C060AC2F26630E8B84,
	SimpleTest_OnGUI_m412A3AB144275112E89F3BB9FEB970070954464C,
	SimpleTest__ctor_m7F3C8BBDD1C82B8681B1175815F1510C006BDF43,
	SimpleTest_U3CStartProcessU3Eb__16_0_m54B2036E98E536D90E6169FB03BABA34DC793E17,
	SimpleTest_U3CUpdateU3Eb__18_0_mB3B3E6D1C3FEE48C0EE43BBFDC0322FB404983F4,
	SimpleTest_U3CUpdateU3Eb__18_1_mFFEE5D5435DBF0B9B5B121BC25EDE630118473FF,
	SimpleTest_U3CUpdateU3Eb__18_2_m55C97653760F39773959B494C311F1252B9AF21F,
	SimpleTest_U3CUpdateU3Eb__18_3_m2A086E84FB55476DF776730E58A34532948BDB8D,
	SimpleTest_U3CUpdateU3Eb__18_4_m94DE1498A0B6B8898A4C73C216B485EF2402C6C6,
	SimpleTest_U3CUpdateU3Eb__18_6_mA60AF5965D710D30B8DE67C2F345EE0657842691,
	SimpleTest_U3CUpdateU3Eb__18_5_m81398BFEB07BCD92500D1D93DA9394B1A5BA80A4,
	U3CU3Ec__cctor_m263DA1BF0CC71ABDB88951746576429D7C97D47D,
	U3CU3Ec__ctor_m55938074A36BD2D27F9B0B8F99F843DFD9451C34,
	U3CU3Ec_U3CStartProcessU3Eb__16_1_mE13AB464770AAAACE72297D4ABA7D8BA5E962511,
	U3CU3Ec_U3CSendByteU3Eb__23_0_m498F24D6DDBB2FCCC41AB49ABE4891988681F5FD,
	StartingExample_set_StatusMessage_mCF27E70635BFE9BBD40D8FED1B636582FBDC3000,
	StartingExample_OnDeinitializeButton_m70E689BCDBC46139AED9A0E724EB6398EF7AF793,
	StartingExample_Reset_mD3EB0C8AF064F5080C41942BB1262FC13A9F4FD7,
	StartingExample_SetState_mF70F17891E022D7EA6273482893945676571BFCC,
	StartingExample_StartProcess_m195D4CE68A4462079E1B969D2D177A72400E00C1,
	StartingExample_Start_mE95C824F385E4E8B5E914F7762FA1F99F69DCC36,
	StartingExample_ProcessButton_m1A4CAD411D83AFA27A492F4C8E8A9F8FD85CA5BD,
	StartingExample_Update_mA4FCCB1DA38170D98E86A0D524430418530D89D1,
	StartingExample_OnLED_m991AFF2485892CDBC85E014243489E261EDAE849,
	StartingExample_FullUUID_m2476FACD1766F07AF4189FDCDC3806D1CDFDF3DE,
	StartingExample_IsEqual_m694468A3EDCFFAAB91E2DCD364D8EB2952EE0BDF,
	StartingExample_SendByte_m975A02F2E57A955DD824C27962BAA91EEFA6BB7B,
	StartingExample__ctor_mBF3B2454334B5DA59E6491145E48E8369AFF935C,
	StartingExample_U3COnDeinitializeButtonU3Eb__18_0_m88BF2909375A7E96CC6FEBAF7EF33838C282454F,
	StartingExample_U3CStartProcessU3Eb__21_0_m8218934FDDCAF92B8F7AA61FE15A46C3767D36CE,
	StartingExample_U3CStartProcessU3Eb__21_1_mF15049C1F70CBB04E82C11D6C4D5FCDCFB3F507C,
	StartingExample_U3CUpdateU3Eb__24_0_m5B6A3B28BF17FB6B55C23D8E2E45A16282049752,
	StartingExample_U3CUpdateU3Eb__24_1_mF8AC0930A3ECC9BD459023D5E34DAF5142CB3491,
	StartingExample_U3CUpdateU3Eb__24_2_mDAB43D31AA5CD1F4BBF3929570744249454EE396,
	StartingExample_U3CUpdateU3Eb__24_3_m9083A004B1BC7DF50E688B3D40023FA19141B00B,
	StartingExample_U3CUpdateU3Eb__24_7_mEC427A7282221075DA4A600BA3A2B2CC9A5837F8,
	StartingExample_U3CUpdateU3Eb__24_4_m3C0E94C3647EF816DAC2639B87B65CD8A875E2BC,
	StartingExample_U3CUpdateU3Eb__24_5_m3C214ADE95D1C9E90899A06CA0E365F73BEACD1E,
	StartingExample_U3CUpdateU3Eb__24_8_m278798714171FAFC93C746690A0A85618AEEF398,
	StartingExample_U3CUpdateU3Eb__24_6_m96A0BC0D103E7A896FED234E6AD8C61865CFAA57,
	U3CU3Ec__cctor_mF0C0530FB59791CE409C167F44210CB68985F606,
	U3CU3Ec__ctor_mD5A931D8103C0C7E273BE47487ADDB16BA4549D8,
	U3CU3Ec_U3CSendByteU3Eb__29_0_mC3753EBC6652A03B64169F270F0B090FE08F7B3A,
	MSACC_CameraType__ctor_mDB7AFD2E5107B108781A4D05B0FFE0FA878E89D4,
	MSACC_CameraSetting__ctor_m72368248876D21F9B170A4A9640304456FFC8B3F,
	MSACC_SettingsCameraFirstPerson__ctor_mE2DE8258E795985C28D33552563261D1C3B7D705,
	MSACC_SettingsCameraFollow__ctor_m3DEC64866088069A87DFABDEFC1757D8E3A1D65E,
	MSACC_SettingsCameraOrbital__ctor_mFCCA1A72E73925036018E8F2A2AE2BCB45E34718,
	MSACC_SettingsCameraOrbitalThatFollows__ctor_m40DEBD895CC9E377ACF56F40ACBE4E6727AC8499,
	MSACC_SettingsCameraETS_StyleCamera__ctor_mDAB2AC9191B18103DB25B350D15016DBF6DC60DA,
	MSACC_SettingsFlyCamera__ctor_m57D08ABE98311A69BD68B0C2542360EFC52ECC2D,
	MSCameraController_OnValidate_m6D119F54E733251A60B269E6D484E0DF96CB060B,
	MSCameraController_Awake_m1BC31F7837B878E3EFDF3849470AC4E346482DD2,
	MSCameraController_Start_m80BCFE915AF73AFF61096C37DD494D1AB6DA9698,
	MSCameraController_EnableCameras_m1F566605667BC223A30145AD5A8A0156D39EF9BB,
	MSCameraController_ManageCameras_m8B723097FA4F0F2BE1A414CF45DB4B5B7439BF51,
	MSCameraController_ClampAngle_m9A1F11FEF832ACAC930DBB2116819B1B24118680,
	MSCameraController_MSADCCChangeCameras_mB6493F1660815E8F9F2CD705F29B7357D08C5A7E,
	MSCameraController_Update_mAF405C540FFF02C40678807CBA1F702B38427135,
	MSCameraController_LateUpdate_m70B84369EDDF916AC62E10E917C69496A9699FE6,
	MSCameraController_FixedUpdate_m9937C518488367D93E1655BDDE26A89B72B66437,
	MSCameraController__ctor_mA407D6DF47C960B2568D4016B616EFC03C203A56,
	ExampleScene_OnReceiveReply_m4578DE21D5486FB98800305628605AFED0CC99BA,
	ExampleScene_UStart_m5BB5CA6042DC9DEA5FD40BFCAB5A3D1D4B3358B1,
	ExampleScene__ctor_mE7C97B051BC893F36C80844A97B831FE9D995446,
	ExampleMod_SendMsg_m107DE94914AAEDFFF6FCFC67ED2E111A08D91EDA,
	ExampleMod_OnListenersAndModRegistration_m2B686875831DF39E665606FE9C9935ABA84FC346,
	ExampleMod_OnDestroyListenersAndModUnregistration_m4C048561BA2E8830AF2495BE2D52BE8974F990D0,
	ExampleMod__ctor_m1E70AF07EE96009EA10BC8A7E768A57DF23B43E8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ProgrammingGridMod_CreateGrid_m88773DA9A0BC8C201EDA7A1AB08A60F1196E9EB6,
	ProgrammingGridMod_InitializeGrid_m7560E9BF05FA9844EB16221D939D8C69AFB1F68F,
	ProgrammingGridMod_InitializeColumn_m50F7D01F09181A0310F4A961B4991EAE0184FA0E,
	ProgrammingGridMod__CreateColumn_m9D2DB1BEF0FD623B8C1555E2B540101E5A668C08,
	ProgrammingGridMod__CreateGrid_m27682F9B29F8A2D188AFBE505198A3A2F0335DE9,
	ProgrammingGridMod_RemoveButton_m0E7D74568730F2D419B1BBD2363A8E7168FCE9B9,
	ProgrammingGridMod_RemoveButtonFromColumn_m0A962BB22700568F68876E4D527511C738174EB8,
	ProgrammingGridMod_ClearGrid_mB567CCEF9EE7FB4AD210800F2FD545F5686B2517,
	ProgrammingGridMod_CreateColumn_mC3F9DC420F6E9D285D5B9CB13DB0B6C42A46A2EE,
	ProgrammingGridMod_FillColumn_mC51AF4A9CFE143C65EDA5B6CBB47064198572B16,
	ProgrammingGridMod_AddButton_mFF71C452DFD7B5E1F8E1385EC9B0E8D31260F209,
	ProgrammingGridMod_AddButton_m3F1076150F83152271941DD898030F32F7C150C7,
	ProgrammingGridMod_AddButton_mDD5881647ECC5BA5F6027D5289A0FFE612F8B0EC,
	ProgrammingGridMod_AddButton_mD9E3EA365D8E661B202C163EE27900BE519B7CCF,
	ProgrammingGridMod_GetGridActions_mA7C319E0D3B50589FEA66EF1B52E2E56810AE690,
	ProgrammingGridMod_FillGrid_mD8B4CE320469ADF055AE0D4592F96AFB7A999043,
	ProgrammingGridMod_SetColumnActivation_m87A921762A20E41D2516AD7D3CD550B602F03012,
	ProgrammingGridMod_SetEditable_m6DFA1B6C60318CAA6152D16170800E2EB306F3C9,
	ProgrammingGridMod_UAwake_m16AC4F43D45B525F1E7E6FF81A0EEA2342C52D85,
	ProgrammingGridMod_PlusButtonPressed_mFB3B5A78BCEDFBA5F5647FC33094F14C04784DA4,
	ProgrammingGridMod_OnPanelPressed_mEEB89F0475176E4E214ADAD52C370608AB686193,
	ProgrammingGridMod_ExpandRect_m01B9BA3626C68B51810B52BF3652239788AB4760,
	ProgrammingGridMod__AddButton_m8EB0E914B71BCF7565641BB2385972357BD2F7FC,
	ProgrammingGridMod__AddButtonToColumn_mB551F7FF176D70F831579A4FF63D34AF7E474396,
	ProgrammingGridMod_OnListenersAndModRegistration_m4784F5F84BDDEF2F224FD6A2936D772D353B4F64,
	ProgrammingGridMod_OnDestroyListenersAndModUnregistration_m77000B3A9CC1727962A0EE42B3C2527B8A4FB3ED,
	ProgrammingGridMod__ScrollGrill_m063FD1BB9503B954A07B16CE5CF0FEBAD79A48CD,
	ProgrammingGridMod_ScrollAnimation_m137D845FA9663D3D5DC9625AFE491365F644168B,
	ProgrammingGridMod__ctor_m6A740CE39C3AD6CEBABFFFF8AB338D8C349103EE,
	ProgrammingGridMod_U3CInitializeGridU3Eb__38_0_mCE10DFF0D0ED426BF7A70E77DE342A4009FD26C6,
	ProgrammingGridMod_U3CInitializeGridU3Eb__38_1_m71E62CD1EBA405CBCBC0E782DE2F41DFBC92998A,
	U3CU3Ec__DisplayClass41_0__ctor_m0F36DF38C22D086D862027174908A784F9C38ADF,
	U3CU3Ec__DisplayClass41_0_U3C_CreateGridU3Eb__0_mA5F8DE8D01036334EDD8D2F2423CE0983E70BD2C,
	U3C_CreateGridU3Ed__41__ctor_mF5C74F212896F8994B8EA3A41EAC0859A103CF9E,
	U3C_CreateGridU3Ed__41_System_IDisposable_Dispose_m73D732B957AE810C6492643D8195D29CC803761D,
	U3C_CreateGridU3Ed__41_MoveNext_m7E92DE61D2613D9B4FE2F7E99020FE53EF01EE57,
	U3C_CreateGridU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1175994B3A50A861572978E0A4C40AE5FFCD33F7,
	U3C_CreateGridU3Ed__41_System_Collections_IEnumerator_Reset_m513E1AB08612B590760B76A1B4600414E5F4838D,
	U3C_CreateGridU3Ed__41_System_Collections_IEnumerator_get_Current_m5D059BA4129F0E1B23A3EB37ADB052EBFD22790F,
	U3CU3Ec__DisplayClass59_0__ctor_mFD81F5A6965DAF4447CB147CF50523B5817E9EA5,
	U3CU3Ec__DisplayClass59_0_U3C_AddButtonU3Eb__0_m2701327FEC66751EC8D4F544C689295279541536,
	U3CScrollAnimationU3Ed__64__ctor_mA0B824A208DD7EAA21D8DB2E42912DC2D45C4139,
	U3CScrollAnimationU3Ed__64_System_IDisposable_Dispose_m88468C273FE8161A2DB428286B62D73A6968D5E7,
	U3CScrollAnimationU3Ed__64_MoveNext_m7643FF774B04826B287525C8FE6A1A246DADDD1B,
	U3CScrollAnimationU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC115A161A44F5FF6E3B5CF394569DE31C5E3260,
	U3CScrollAnimationU3Ed__64_System_Collections_IEnumerator_Reset_m91E3BE8EEAC930248C2FD1FA3C64F8A01A2B5D13,
	U3CScrollAnimationU3Ed__64_System_Collections_IEnumerator_get_Current_mD308AE5AFC5394AB0F090821AB87B281E949418F,
	CameraMediaMakerMod_UStart_m70486CC84C0A9B368F07CC24EB61B5FB3B654FF1,
	CameraMediaMakerMod_Configuration_m3E60B07D5534139012E74BE6D9AAFFF44BF9BF94,
	CameraMediaMakerMod_UUpdate_mDDE9A03AE61E85764587CA791B0A4D527F2B67A1,
	CameraMediaMakerMod_ShowCameraOnTexture_m0B0C4A208C7AB11E1989316BD09BAA08802AA0F8,
	CameraMediaMakerMod_ShowCameraOnTexture_m466F9275B5341D4AFA29B00CC8A80A70320064F8,
	CameraMediaMakerMod_StopShowingCameraOnTexture_mCEAAF658EBD5C9B849011B66D502132253452991,
	CameraMediaMakerMod_TextureToTexture2D_m2E38432F233F09C8068E5AED0F9F4BA4D7B22833,
	CameraMediaMakerMod_TakePhotoAsync_m5C8919A30E4AF7028D9C47C1DD5E1209D56B53D0,
	CameraMediaMakerMod_StartRecordingVideo_mBAE746EB310EA59AA62DF923FAF798FBE2E982F1,
	CameraMediaMakerMod_OnRecorded_m4539CF1D83CF7CE2BA5C1C8E02DC55C2467A1814,
	CameraMediaMakerMod_StopAndSaveRecordedVideoAsync_mC4E4641E25C04D7CEBF08E2C3C7A0353C6CC5B33,
	CameraMediaMakerMod_OnListenersAndModRegistration_m88537F3E5F4552502E1F71B15B40E48338F10BB4,
	CameraMediaMakerMod_OnDestroyListenersAndModUnregistration_m8190DFE324F9C43978E9C19FC5D51C92D2440F2A,
	CameraMediaMakerMod_OnMediaTaken_m90BE49DA51A8429064ED8A5F8D7E35D1C9655DF8,
	CameraMediaMakerMod__ctor_m9D77BFD038A78A250E49840D20B6D399C061302A,
	VideoCameraMediaMakerScene_UStart_m236A7BAE47C30BC576A5E71B33F23FA22FE06376,
	VideoCameraMediaMakerScene__TakePhoto_mB3643B1A011D00D06000CA677678AE7C901CBEEB,
	VideoCameraMediaMakerScene__TakeVideo_mCBF21311606EEC791BC6FAD78B2D3F9BEDC8678D,
	VideoCameraMediaMakerScene__SaveVideo_m342D6D2AE1967B022A7B8D23093E36D5E8F671BC,
	VideoCameraMediaMakerScene_OnPhotoSaved_mC0509903D33525E3D085053C8FAA63E68F88B225,
	VideoCameraMediaMakerScene_OnVideoSaved_mE48D4CE0514EE71C0DB56BFD8758ABF81E208776,
	VideoCameraMediaMakerScene__ctor_m83A908B45FFEEB148510C2698832FB8D15A90AFE,
	CellStruct__ctor_mFE61576AF832638D4D8080AD8D82E5B76DC823B0,
	StorageTemplate__ctor_mA9B4DE796F6FF19CA475D0F9A36088E0C8359C78,
	TestingScene_UStart_m6C5E3BB3E24D7C9C3002A32F9158A74179847717,
	TestingScene_OnStorageCompleted_mDBE921C38EBB6981F1B358CECB8681901AD903DE,
	TestingScene_OnLoadCompleted_m4E45948116977C7AD92E9E284A1FE268672FA34D,
	TestingScene__ctor_mEA9FFBACF83304F30AB2F7156D8734A41FD139B4,
	ScopeScreenManager__ctor_m82D1678C469F0C4F30824A75C633C855B311E4C1,
	ScreenManagerModTestScene_Start_mEAE27A8D32FE7CF72EDB98EE15F20935777F5CDD,
	ScreenManagerModTestScene_Update_mD23532C3BB3D2DAAAAA4EABE0021CFD88F794D1B,
	ScreenManagerModTestScene__ctor_m3F8D1E408EA8FCC84A3F7997B378902E62DB764F,
	NULL,
	NULL,
	NULL,
	Images__ctor_m6D3C3358F518CB4C2D7868641CD0F85BA58BCAF2,
	Reporter_get_TotalMemUsage_m10C1968BA6C2F34F7FFE70B152DA436016B2C9E9,
	Reporter_Awake_mA05F12CC643D84A43D7BE3C98E4A4BCAB2CAFA84,
	Reporter_OnDestroy_mE98F92395C2BE83BF582ADB802E170E1A826BA1C,
	Reporter_OnEnable_mE1F5751BD00633D30038260E71CC6C5C27929194,
	Reporter_OnDisable_mB6B693AA18CA1AB35300F1703FCDD8BFDCD93C05,
	Reporter_addSample_mAD4EFAF6C2D5568AD4A2012EB64D1053C12E213B,
	Reporter_Initialize_mAA682DFB4BC0F75FD5A155177C0CE29AA911FDDA,
	Reporter_initializeStyle_mFA953E046D71C4283D69BCA59FFE004A43375594,
	Reporter_Start_m446802F56F56E3160BD26A917B18D06FB3EB775F,
	Reporter_clear_m7D610B4FDEE7966EDB5949114B95081B0FF2AF3E,
	Reporter_calculateCurrentLog_mFB0B224FD7E17B906D6D67EF1CB6A9CEC729CA13,
	Reporter_DrawInfo_m86A5EB0BF0B5F7F46432490808B6F123AB01FC6F,
	Reporter_drawInfo_enableDisableToolBarButtons_m52E96CBE8C62CF30F324857974B5ECF60F7087BD,
	Reporter_DrawReport_mD476D5C0ED66B1AB1795C38505BFC474B0C6B738,
	Reporter_drawToolBar_m9AE16E4DD9B3987C5344D7FB7B7B237E618216FA,
	Reporter_DrawLogs_m543EA7CD2688AD9F23D2938F9D2A0902239B8E05,
	Reporter_drawGraph_m0FB0706C34D21D3EACED1DBFC3C885D762E04CAB,
	Reporter_drawStack_mDE34775477887CCEE8AB1EDBB8936B443B1445A8,
	Reporter_OnGUIDraw_m989E3903ABD3D4172D03E6B2F70C882A519E2207,
	Reporter_isGestureDone_m3CC6FADB0F178A730DA4D461FFAA307688FD9D56,
	Reporter_isDoubleClickDone_mBD57415E98A74C5D88ED962973EE0562A9AB6098,
	Reporter_getDownPos_m386F88542FDA509FA8D52F99F14BBC6F9040F5E7,
	Reporter_getDrag_mDB7C7B3DDAB95B33DCE693ABB90CE35706475E00,
	Reporter_calculateStartIndex_mF006CA3233D72E188852B5017D2DA609ED526D61,
	Reporter_doShow_m48F015370C942A304F060D53AC6B631789B5C49C,
	Reporter_Update_m10DDBD3CE80D61FED5928E7F4A72D0FBA0688DE9,
	Reporter_CaptureLog_m2B074EDFEB14E8DFFE601BE8408C79DF1D2A0828,
	Reporter_AddLog_m1DC979DC17934CF2AB2C463F4AC325B3C8DAEEDE,
	Reporter_CaptureLogThread_m10D6873709D9988908720E92DCECFEE9CEF7B063,
	Reporter__OnLevelWasLoaded_m6299EA2B588C6F405E264FADAA0A619C9B4F072E,
	Reporter_OnApplicationQuit_mBC8D59AAABE9EDEA29DCC34744D8F69B4DC3BDC9,
	Reporter_readInfo_m5CEA679FCEF5D1855FAA34D0AB171AE781F52DB0,
	Reporter_SaveLogsToDevice_mE722E0FDDF5444E1FE4EE9C26F6AD87272D51A58,
	Reporter__ctor_mA165B1C3F3534A08DFE14C785290E70564217326,
	Sample_MemSize_m58B3E52C74A47040F2BEDE76EEEBA0313591A6F8,
	Sample_GetSceneName_m39205B31355C11E75F737F341C3E00B95F418BCF,
	Sample__ctor_m91CD0ECF2B9B33DD9391FD3A89AD51805C40210C,
	Log_CreateCopy_m6F7B2A38CEF5B721E70636B252CECD58153B7960,
	Log_GetMemoryUsage_mBEF407ED7EC6FEAF56316D4139096FE8E10851B0,
	Log__ctor_m20C60492A00E977718447AF5C9A51F95BF9F84E2,
	U3CreadInfoU3Ed__188__ctor_m254978808992314EA07A7CC5E70AF72E6C7A9AAB,
	U3CreadInfoU3Ed__188_System_IDisposable_Dispose_m5249C6D7C6927C5700525A86D05890776541F96D,
	U3CreadInfoU3Ed__188_MoveNext_m11006FBF4C7E561FD9EE8D51D0952211FD856351,
	U3CreadInfoU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFFD2E9F27F9240543634E6731841BDD5EE9D8881,
	U3CreadInfoU3Ed__188_System_Collections_IEnumerator_Reset_mEB09690D6F41312D7EED5B9FE4BF1320E88099D6,
	U3CreadInfoU3Ed__188_System_Collections_IEnumerator_get_Current_m93150D71A6E5064FC2A2F530E798404D25ED2F8B,
	ReporterGUI_Awake_m3A87B0F7A2669A4A492555A0DEC5FFB9815C96EE,
	ReporterGUI_OnGUI_m0259C45E54B81A6EF065DF865B40DEFD58995475,
	ReporterGUI__ctor_m86C037AF5B2F644D5ADE38485AFBECB75A05F220,
	ReporterMessageReceiver_Start_m9A80C850878106B725E2B7B331AAF9A48923B772,
	ReporterMessageReceiver_OnPreStart_m5CF7C808C27165116648227A6BB3135F5B36585C,
	ReporterMessageReceiver_OnHideReporter_m73097CEC01550AB1EECB974FF2A3E09B5F8EE0F4,
	ReporterMessageReceiver_OnShowReporter_mD63CC8AF909A9ADC2EF833AF08C9F73CE54F53D1,
	ReporterMessageReceiver_OnLog_m9A92C7CEB82867BDC79311A912A93D02D7617ADB,
	ReporterMessageReceiver__ctor_m7923B38EE7568AA0D472AACB2EA61B52D0EF0F66,
	Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB,
	Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7,
	Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4,
	TestReporter_Start_m21A1734784671210AAF7510311A801A87D7938FE,
	TestReporter_OnDestroy_m51622CA5CD9F3AD7DCE7CE9F5332101863128096,
	TestReporter_threadLogTest_m770ED19CDCF1906DA5B44F3E6D0E1C54644BDBE3,
	TestReporter_Update_mFAB961D73F7C686FC07F73D3C15953DD49AF0BD5,
	TestReporter_OnGUI_mFEA69290A684C9CC25CCB3E5D1BAAD93C500889E,
	TestReporter__ctor_mA5EDF9AD6D70A294B65B0AB838E6DB753F7C0710,
	ScreenScope_UStart_m93B383192637F79E49D70EA73CFCF7421778AD13,
	ScreenScope_GetRootScreen_m1F1A9C9E1070DB19BED7307BF6ABA0BE25A89E7E,
	ScreenScope__LoadScreen_m41BAAC33DD5186920964C38EE10C8AC006AD637E,
	ScreenScope__ctor_mC04E7F7C883B423F1BF453BAD080137BCC81DAB3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UmodsSceneBehaviour_OnInitializeSceneBehaviour_mF18EDF847827049BDE5C1866DED7D984A77CD24B,
	UmodsSceneBehaviour_HideLoadingScreen_m84CAE20931558BCA52ED0D1E57C39065A5A6812F,
	UmodsSceneBehaviour_LoadScene_mA25A4DD4C9BA16B6B4E49B529401D2D20C80D2FC,
	UmodsSceneBehaviour_LoadPreviousScene_m39069DDF01EB848D506A2B4806A754B130F3EF65,
	UmodsSceneBehaviour_LoadRootScene_mC32A0553C322B31CBAB8D45CF7C12C7CA23357AC,
	UmodsSceneBehaviour__ctor_mDCD0ED2A4E6F737B776770EE1AE04017B8B43C4D,
	UmodsSceneManager_LoadScene_m36B3818CCF2345DCE381AF986817E19A80603AF4,
	UmodsSceneManager_LoadPreviousScene_mD731528C4F37F99B459808740FE61A687EFF934D,
	UmodsSceneManager_get_LoadingProgress_mB29A8FFE652E2F58DC0C23FA8A29EDEF6441A65D,
	UmodsSceneManager_set_LoadingProgress_m82FBFB92B81FAF41AF37B570CA84A5425EFD9290,
	UmodsSceneManager_LoadRootScene_m6E90EB81C0B1B5DD2BDD7F048DF2B2F812C6C1EC,
	UmodsSceneManager_HideLoadingScreen_mD3A1081588369D78A9375B5F351CE75133877EEB,
	UmodsSceneManager__LoadSceneWithLoadingScene_mEE6B81E041BD064814084510639E8AE1FBB050E7,
	UmodsSceneManager__LoadSceneWLSCoroutine_m3FF11DF925CCBE1728AC7510F67B90D724171CD3,
	UmodsSceneManager_OnListenersAndModRegistration_m11368CE2F11083E26653C677FBD210B482B49F0F,
	UmodsSceneManager_OnDestroyListenersAndModUnregistration_mB6419CA71BE6BCD515292D34C48911E28A3A1A55,
	UmodsSceneManager__ctor_mD8D9F8212B79B1A277160016BFE256F5101A6F6D,
	U3C_LoadSceneWithLoadingSceneU3Ed__14__ctor_m81C467AE102CF9E496C3707C5000ADFDA5D0B8C7,
	U3C_LoadSceneWithLoadingSceneU3Ed__14_System_IDisposable_Dispose_m66E2D552B1F3F76AC0461C003462563EFFC02E10,
	U3C_LoadSceneWithLoadingSceneU3Ed__14_MoveNext_mD494D0CD5A7CB5E0AD493E65A4F30639A9F7E416,
	U3C_LoadSceneWithLoadingSceneU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7B19E9FFEA9516935FF621847C946A770F5EC8F,
	U3C_LoadSceneWithLoadingSceneU3Ed__14_System_Collections_IEnumerator_Reset_mC48B1FB57434D2BEA8AF056F76984F0005A05314,
	U3C_LoadSceneWithLoadingSceneU3Ed__14_System_Collections_IEnumerator_get_Current_mED8E7732E5D421FB99858C3646CF7DCA6C835A4D,
	U3C_LoadSceneWLSCoroutineU3Ed__15__ctor_m50F6845A118FD81E306CD942C66AB0D2BAB42762,
	U3C_LoadSceneWLSCoroutineU3Ed__15_System_IDisposable_Dispose_mC81A36BD9503D2FCC03969CEF2FA076DBC412A3F,
	U3C_LoadSceneWLSCoroutineU3Ed__15_MoveNext_m86C0215F6C7EBEFAB402CA2595AC86B33ACD95F2,
	U3C_LoadSceneWLSCoroutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE0EFD83CFF38637105FF67D2F71BDA3391BEB77,
	U3C_LoadSceneWLSCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_mE9BA8B61286881D809B25880AE8D8682A5851FFD,
	U3C_LoadSceneWLSCoroutineU3Ed__15_System_Collections_IEnumerator_get_Current_m2328167C9476A1E5937BFC8DAA24801FAF02B050,
	NULL,
	NULL,
	UmodsExceptionEventManager_SendExceptionEvent_m385ACF9D893A5D9C2A383078DCE79C9364B59A3F,
	UmodsExceptionEventManager_OnListenersAndModRegistration_mC8EBD41255F96C7C5D7C7805BAB58E890933D4AE,
	UmodsExceptionEventManager_OnDestroyListenersAndModUnregistration_m419F79635E141557205F12005E6233DD0147F175,
	UmodsExceptionEventManager__ctor_m4B0A0E3446796490F99F061C7FEC8910F18580DA,
	NULL,
	NULL,
	NULL,
	NULL,
	UmodsAppConfiguration_Start_m756A31A3818FE3575454278CA9FA099D54DC4BAC,
	NULL,
	UmodsAppConfiguration__ctor_m6D68E5A6CDCDB98F18B38142113F277142C65358,
	UmodsApplication_get_DebugInstance_mF3BEB5D0E0B7B1182A88D6A75A409E0AFBAAF1F8,
	UmodsApplication_set_DebugInstance_m6BC90B09FD280FB7212AC4F4002881053AEAF9DB,
	NULL,
	UmodsApplication_get_EventListenerManager_m195F930F81CE11BC82E9C02D797A66224570E342,
	UmodsApplication_get_EventHandlerManager_mA27BB145410EC52464E968C75D0B1D5F58315E70,
	UmodsApplication_Awake_m692697763EE0E14295E5647A9DF9C9F0C3DFC35F,
	UmodsApplication_UStart_m32523CA197AD3D54339AE5DF8939608E9895288C,
	UmodsApplication_OnDestroy_mCF386BB411D50D847AE84E9E50B4243A90B405E0,
	UmodsApplication_InitializeLifeCycleListenerAndEventRegistrationManager_m3BDF1D4446389C1C7A44C949AE4AF08A1A3C5C47,
	UmodsApplication_InitializeInternalMods_m4A0C7A994723B2EED279F4ECCD19F8F3A51C3053,
	UmodsApplication_RegisterInLifeCycle_m49CEBA4BA567776C5EA4EA84288628A932573716,
	UmodsApplication_UnregisterInLifeCycle_mD27DDE9B9198A750E1C89555CCD67594F264A487,
	UmodsApplication_RegisterAppConfiguration_m976537DFB920FEFA97122915CA98F9D9DE1E73C5,
	UmodsApplication_UnregisterAppConfiguration_m6BD2EB56E5CDD62049F9A3EB65EC7E9525E3AB07,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UmodsApplication__ctor_m23381177433EE98ABA788B4AFA10C64EC1025DDF,
	UmodsBehaviour_get_UmodsApplication_m96F40DAD3183272A19075FEC6EF8F1CDB01C6CBD,
	UmodsBehaviour_Start_mE3C02481C4BAD0BFF16CD1A560DA5DE6611A6EDE,
	UmodsBehaviour_OnRegisterHandler_m41C5C9E5AD5C19B385A4A528C994171487E7044F,
	UmodsBehaviour_OnDestroy_m90AC2450B8E797D32B52873BC1539ABB0C4BFDD8,
	UmodsBehaviour_InternalUOnDestroy_m3907A5E90BA1E2618BD279A364C3161BF5485F76,
	UmodsBehaviour__ctor_m9B23039613C214337707F448630A167E8293DD83,
	UmodsEventListener__ctor_mD5518F4FF6657E76D28857DDE299CF0819A6535B,
	UmodsEventListener_AddObject_mD057299D76C01C9310D8BCA3AF0AD50062BE28E0,
	UmodsEventListener_RemoveObject_mDDC41C774A5AF11A0A2A2EE69208BC3D550569AB,
	UmodsEventListener_PullEvent_m80CD5D0AF7A142A6F9FD3CFD357D6913EFB1899C,
	UmodsEventListenerHandlerManager_RegisterHandler_m3CB3CD4F526005C8971C8BCA726CCDC640573CF0,
	UmodsEventListenerHandlerManager_UnregisterHandler_m370565EC288C3B6E0ED949D80FF6A69F21CB42B3,
	NULL,
	NULL,
	NULL,
	UmodsEventListenerHandlerManager_ClearListeners_mEA4B1FC88D1F872F75E4D7C36213BC890F0355D2,
	UmodsEventListenerHandlerManager__ctor_mB551AD2C5AFDAB0B50135AEF6981B47C636F24CB,
	UmodsInternalModBehaviour__ctor_m7987CB1FC12147238876E420629A5CBF829BA80F,
	UmodsLifeCycleListener_RegisterHandler_m733C0C190D4A1C6C4A00CC6D7942FBF2EC963BE1,
	UmodsLifeCycleListener_UnregisterHandler_m076C0466BB7F9E6B8C7C6D8A604B64B19A37FD8F,
	UmodsLifeCycleListener_UnregisterAll_m1E8992AAB50679C9A0C1C888BFB5A60182A9182E,
	UmodsLifeCycleListener__RegisterUmodsBehaviour_m7CE22E8325F0C97C9A215FFA71E23915CD4FE0E3,
	UmodsLifeCycleListener__RegistrationChange_m2165F468436681F1437BF857A55A4C119EE9F1E8,
	UmodsLifeCycleListener_Update_m15C92ECF3099EC42E882440E1064453214C95608,
	UmodsLifeCycleListener_OnDestroyCore_m31AE6A78DC000BA99078EDEE2448EFBBF61FDD62,
	UmodsLifeCycleListener__ctor_m7C9CC9BB3B07718FFCE8C36B42FA673F16A40717,
	OnUmodsLifeCycleEventHandler__ctor_m8B7784EAC8EEEF49CB3C7793A095FE1088874074,
	OnUmodsLifeCycleEventHandler_Invoke_m95B7632F30205D9AE8DDF249DCAA2F50DE90EFEC,
	OnUmodsLifeCycleEventHandler_BeginInvoke_m763DF62BF687D64EE7FFF6000D97DD419739BAC1,
	OnUmodsLifeCycleEventHandler_EndInvoke_m76FE5D48B0EA6E8C5D3090F1B112F7955348F322,
	U3CU3Ec__DisplayClass32_0__ctor_m9E6466B364C702711AD7B76359C3481D91627208,
	U3CU3Ec__DisplayClass32_0_U3C_RegisterUmodsBehaviourU3Eb__0_m3A21958093DAD21CCD4798F8048EC52AA65974F3,
	U3CU3Ec__DisplayClass33_0__ctor_mE900DF73CBF2801C37D11EA78C012DE9986C5D29,
	U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__0_mC478BB26B1F987CB5854B8AB268343F741BBA493,
	U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__1_m13252784B3E6D057BF29CBCA302EA721282B0735,
	U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__2_m9F4247B9FF77082B0868ADA13CE2CBF4076DEA1B,
	U3CU3Ec__DisplayClass33_0_U3C_RegistrationChangeU3Eb__3_mE7594930A6AACF9179CD882B5A616299369B3712,
	UmodsModBehaviour_get_DebugLog_mAE099FC40A8DE6CE6D53E42562940302CEE60E29,
	UmodsModBehaviour_set_DebugLog_m79793141656BEEF92682E2EF95E7EBC3EB8EDE9D,
	UmodsModBehaviour_OnInitializeModBehaviour_m9589E49E2B7AE63FB9C8A1A3F1FB2AD7D40BB929,
	NULL,
	NULL,
	UmodsModBehaviour_UOnModDestroy_m4E10F2F54573C95502E9F66C779267D5328A9898,
	UmodsModBehaviour__ctor_m24D793B7A7F5BBD8A38F16207EF91F92BBD65E2A,
	DebugObject__ctor_mDEE53DE83E87C16DD5200C7CE23E601AAC6115CF,
	DebugObject_Log_mAE6B3DE529532CC9E1E890376DFB0F5335873514,
	UmodsSceneBehaviour_OnInitializeSceneBehaviour_m3BF0E5E8FABD3CB42E457005F460E0896C086BA8,
	UmodsSceneBehaviour__ctor_mECEEBA6FA289E8E745B831BDDAF787F8DA067FBA,
	NULL,
	NULL,
	UmodsExtensions_SafeCallMethod_m744E1D91A0AEF2C7FD6F6738951E06529CC5FB49,
	UmodsExtensions_AddListenerToEventTrigger_m9FBFF45C7CEE2736786E92B84136B203FF0F7AB8,
	UmodsExtensions_RemoveListenerToEventTrigger_mA515624ACB8C2586DC50107E30780B890AA2F26D,
	UmodsException__ctor_m92FFCD7FC71D5ACA6683853D5AEB9E67D3F90FBF,
	UmodsEvent__ctor_m2F58B1A195AF6F704719694E1817412186D30D2A,
	ScreenManagerMod_OnListenersAndModRegistration_m49095AA6767AE2CA646699898BE49F95114E928A,
	ScreenManagerMod_OnDestroyListenersAndModUnregistration_m62E8906B8238EFF0B3184AFC0852D7C73DB6CE96,
	ScreenManagerMod_UStart_m31C82B93298132DF45D6C7340E5D241360D4A25C,
	ScreenManagerMod_OnBackScreenButtonPressed_mBD7DCFE0E394927BE757693E5619710EF4E48275,
	ScreenManagerMod_OnNextScreenButtonPressed_mB692A1EC12E9B558D6A8CA9FACEDFA16B47E79FF,
	ScreenManagerMod_OnBackScopeButtonPressed_mF01FFC11CA2DFE71A083E3975BE5B9BD3814270E,
	ScreenManagerMod_OnNextScopeButtonPressed_m8CC7D4E49E65EFCE9AACC0152511D17C4E5A4829,
	ScreenManagerMod_ChangeScope_m33593BE6F00CC2D87BCEFC9EEFAF845362987C9C,
	ScreenManagerMod__ctor_m2DB508868617F70D9A2E1FECF9843FF6C9B694B3,
	Button_get_OnBackScreenButtonPressed_m609BE5D9024DB96AF14E7AB48784954853352132,
	Button_set_OnBackScreenButtonPressed_mA362E4510D5A37494C70C4B381560738EA1BD81B,
	Button_get_OnNextScreenButtonPressed_m1FDD0BEC3151CD74F2CC59A01F0493E253BC7885,
	Button_set_OnNextScreenButtonPressed_mE52744DF31765A99EB18C023DEB3BFBBAFFC0D47,
	Button_get_OnBackScopeButtonPressed_mBE8EA96606E44492996D9940B73E23C873018C4D,
	Button_set_OnBackScopeButtonPressed_mF6683A24AF42B2E9DEB85A03A23540C62A1B2088,
	Button_get_OnNextScopeButtonPressed_m4DBD3F41902CB89636DD1A404FFAC42912AB0EDA,
	Button_set_OnNextScopeButtonPressed_mFEBE8FDCDE00876EFA6A9A1907D3649AC79345CD,
	Button_UStart_m4B852EBF9ED7E204ED9665D439E01B8EFB7041CF,
	Button__ctor_m123DAA14105BD1A16B6D3F62CF6B9D5FE303DED2,
	Button_U3CUStartU3Eb__21_0_mB25A26CF57A1A4A5132E983FB09DA07BE2BB5E8F,
	Button_U3CUStartU3Eb__21_1_mD5F99A0DC8A1EF0188A5052662B013B2797F1F95,
	Screen_get_OnBackScreenButtonPressed_m416ADFCFEB9CE02B307E8D5A2F99015310A8289B,
	Screen_set_OnBackScreenButtonPressed_mE7457A9B5CDE69822AC99CDAF872354363518D71,
	Screen_get_OnNextScreenButtonPressed_m75EB01E0B09CB84EB7844319D295B2A5F73A221C,
	Screen_set_OnNextScreenButtonPressed_m083B7E28C7643FFB2311ACAB0D73C5A18C48AA67,
	Screen_get_OnBackScopeButtonPressed_mE420686898F625E539812F5677E47AC7C95561E1,
	Screen_set_OnBackScopeButtonPressed_mC7B8C39A2EBE52FF6DA55EFCD4CD774E8D6E1E4A,
	Screen_get_OnNextScopeButtonPressed_mE8A6296D3B83A8C7A6EE5037D9ABB05BAB0A7D04,
	Screen_set_OnNextScopeButtonPressed_m0832AE6B1BCAB7BCB4E90D77703929DBF1F84020,
	Screen_UStart_m47F7D69C04A82F3F1DC8DFA85880E26ABE1927E3,
	Screen__OnBackButtonPressed_m6A7394F5D6FC55B9275C70ABD81878110682F54E,
	Screen__ctor_m819E5C490D6859F1161DCC0A2B60444DC18A9BE9,
	TestingObjectTemplate__ctor_m47973D8EF1DDA3DFABF6F17E47D402B85A2E05EA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StorageMod_OnListenersAndModRegistration_mB18E95189E806D24D5D795041EA6DA81621068A7,
	StorageMod_OnDestroyListenersAndModUnregistration_m05087236084D37A5681BC6CD840C092186C12EA3,
	StorageMod_StorageFileAsync_m7A6FF8FE2E35092C07808AF31420B0B123ED687A,
	StorageMod_LoadFileAsync_mEA22A8A562D4947EB508BEFF8BACF42033034283,
	NULL,
	StorageMod__WriteTextAsync_m8AB6A9591235A1950E6DB2A086C1020993ABF53A,
	NULL,
	StorageMod_GetFilesInDirectory_m7FCAEEB844A746DB75CB3D72EAB2F4727E8D33AB,
	NULL,
	StorageMod__ctor_m27CAF7DE6FA2B0B1A4FCB8924DD6D36CFD53A9E8,
	U3C_WriteTextAsyncU3Ed__10_MoveNext_mB047908016279CE15714F3FA197BEF9447B2C19F,
	U3C_WriteTextAsyncU3Ed__10_SetStateMachine_m796A7F9239E531781DEFA9126F8DE85368105461,
	NULL,
	NULL,
	StorageVariables__ctor_mD87338FE7ADD19ED1FC8B3D45A0A4562AAC4EDD0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GyroscopeControllerMod_StartReceivingGyroscopeStatus_m922E31AE36E62B59BD197565A604EB5B757FDC6E,
	GyroscopeControllerMod_StopReceivingGyroscopeStatus_mA1EED369367269455988FBD65DD31CDB0BC5D848,
	GyroscopeControllerMod_OnListenersAndModRegistration_mBF9A501D66F3102F7F0FAD4A1924E9C52DB77FB0,
	GyroscopeControllerMod_OnDestroyListenersAndModUnregistration_mD070ED5A7687E2D8D413FC83DBAA96950EE17350,
	GyroscopeControllerMod__UpdateGyroscopeData_m0600F9A96FDEA94FC3702F9C032B581C2666F950,
	GyroscopeControllerMod_GyroToUnity_mF1CE12C5653D791341CAAA2DAA3FCB2A0528C533,
	GyroscopeControllerMod__ctor_m25C6FC32EC6CEE08CC3E09D16AD553598807603E,
	U3C_UpdateGyroscopeDataU3Ed__7__ctor_mDFD95E244D6F408CFC29BEC5906DC0A00188029C,
	U3C_UpdateGyroscopeDataU3Ed__7_System_IDisposable_Dispose_m44573EF4EE073167E47E135AED33EDE468119ED9,
	U3C_UpdateGyroscopeDataU3Ed__7_MoveNext_m386B68E7765872F261712E61429803F7FC4EC77C,
	U3C_UpdateGyroscopeDataU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D059DBFBF4649D156F14D5D5D992F073255ECF1,
	U3C_UpdateGyroscopeDataU3Ed__7_System_Collections_IEnumerator_Reset_mB79A8FCC7850F965BEF869458CB983B58D8296E4,
	U3C_UpdateGyroscopeDataU3Ed__7_System_Collections_IEnumerator_get_Current_m4FE3B44ED999C89B3E9DC1304126B33CE28811D9,
	NULL,
	NULL,
	SimulationConnectivityBluetoothModMod_OnListenersAndModRegistration_m0D9E2A3FABD92AAF51D1EAE72BA6D1F7EC42B422,
	SimulationConnectivityBluetoothModMod_OnDestroyListenersAndModUnregistration_mE1EC35681D1B9B03C5F4762F034816E217AC796A,
	SimulationConnectivityBluetoothModMod_StartScanningBluetoothDeviceListAsync_mDE326AA90A37832F9907367E19F90AEA6A7A4B63,
	SimulationConnectivityBluetoothModMod__SimulateGettingBluetoothDeviceList_m19B8F4ED3653F39997198431E6B784BD4A852165,
	SimulationConnectivityBluetoothModMod_StopScanningBluetoothDeviceList_mDF37C964655C6716461E60D6DF79941BB4FACEDE,
	SimulationConnectivityBluetoothModMod_ConnectToBluetoothDeviceAsync_m1B1B995069A6DA0A58283FD817CB4190C61F2C89,
	SimulationConnectivityBluetoothModMod_DisconnectFromBluetoothDeviceAsync_m746261DEA30AEE1EE5EC5824F9BEDCE1F369A5D4,
	SimulationConnectivityBluetoothModMod_SendMessageToConnectedBluetooth_m731BF742913F41DD13249D18CE1A8D0A42A9C702,
	SimulationConnectivityBluetoothModMod_SendMessageToConnectedBluetooth_m1CC8EBD3856D3EF28B39063C20B89D58BAC95B1A,
	SimulationConnectivityBluetoothModMod_IsConnectedToAnyBluetoothDevice_m4EE2287AE8DB577F3C25C0E1C32E05A7BE04CB5F,
	SimulationConnectivityBluetoothModMod_IsConnectedToBluetoothDevice_mA025A222EA5EADD7EC9E8DCDA5C3FE2F606ED204,
	SimulationConnectivityBluetoothModMod_GetConnectedBluetoothDevice_mF541C5AFCE73F1C3799709D1AC1A7AE87DE3E653,
	SimulationConnectivityBluetoothModMod_GetBluetoothDevice_mE09AFD52EC26ECAB5AFD8F5F7A1C975F488872CD,
	SimulationConnectivityBluetoothModMod_IsScanningBluetoothDevices_m758233A97527A56B47E72F141061B1AE0E43D3DA,
	SimulationConnectivityBluetoothModMod__SimulateBluetoothConection_mCABA65A36D851FD462C4D893A241323B298E52B1,
	SimulationConnectivityBluetoothModMod__SimulateBluetoothDisconection_m135B7F00F9C3210E96649005DB7163C81806D557,
	SimulationConnectivityBluetoothModMod_OnBluetoothDeviceListUpdated_m9A2777DCFF584732728D422ACE9C7804002F016E,
	SimulationConnectivityBluetoothModMod_OnBluetoothConnected_mD4766D2A08A7DBA1E07D40663D9484B45A0E4A94,
	SimulationConnectivityBluetoothModMod_OnBluetoothDisconnected_mAEB0751732561566D1CE1997982C9EF4E8534398,
	SimulationConnectivityBluetoothModMod__ctor_m422B3A214E6994CDEBB612356FA889254CB5994C,
	U3C_SimulateGettingBluetoothDeviceListU3Ed__14__ctor_mA36A2DDC811386A2D396148928E0AACD12A4A5D6,
	U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_IDisposable_Dispose_mF1C60528FDD9D435DD268C418D681634D5AE2AAD,
	U3C_SimulateGettingBluetoothDeviceListU3Ed__14_MoveNext_mB4AB6BA6BA7BB2D6BF4F66ED6D239E08D3890C1E,
	U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FBADF02C7E7828D0384895172F9E921AE4B6365,
	U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_Collections_IEnumerator_Reset_m8A76CDC2DD87648EBE8B6CE2A406B85A2549C9C8,
	U3C_SimulateGettingBluetoothDeviceListU3Ed__14_System_Collections_IEnumerator_get_Current_mBC5FF564E0FC3B0B0EE0719BCA9184291D9B71A6,
	NULL,
	NULL,
	NULL,
	NULL,
	ConnectivityBluetoothMod_OnListenersAndModRegistration_m7F534B438B6DC3AA591D98AB2092D90DCC400D46,
	ConnectivityBluetoothMod_OnDestroyListenersAndModUnregistration_mD651EC19300F1168FDB7D365812EAA7E563A7A64,
	ConnectivityBluetoothMod_UStart_m67BE4D88D1761FDE67A9088C115C45111B1E1101,
	ConnectivityBluetoothMod_StartScanningBluetoothDeviceListAsync_mB601FA85622EF602C7E83C6E27DE39E7361073F1,
	ConnectivityBluetoothMod_StopScanningBluetoothDeviceList_mD9F1844ECC08D1DB82A15F811F72C45AA3CE3A95,
	ConnectivityBluetoothMod_IsScanningBluetoothDevices_mFCB03978912BE0E1F9D62854A48263A366013960,
	ConnectivityBluetoothMod_ConnectToBluetoothDeviceAsync_mE9574FD7CA35A7CC6EA06584DD211FBAC5E63AF6,
	ConnectivityBluetoothMod_DisconnectFromBluetoothDeviceAsync_m0CF6773E6E4940935BACC7AA33B39C7C4821949E,
	ConnectivityBluetoothMod_SendMessageToConnectedBluetooth_m6A7FC5172EB6B52CF46AF8B4DE74A4FA80DD386F,
	ConnectivityBluetoothMod_SendMessageToConnectedBluetooth_m15C5218D173372A5B94C7A9FB592E43CC5E3B662,
	ConnectivityBluetoothMod_IsConnectedToAnyBluetoothDevice_m99CF0FB743561C7F28A519A94535809E27FD9CBC,
	ConnectivityBluetoothMod_IsConnectedToBluetoothDevice_m11530B9E4F4C40168A3E4D2CF2BCD751AB62F271,
	ConnectivityBluetoothMod_GetBluetoothDevice_mA77D278BC1DE37BA832CE80144B3CE797D77AD93,
	ConnectivityBluetoothMod_GetConnectedBluetoothDevice_m1B77B59ECD00B43F4CEE1F98831D2D6606E235F4,
	ConnectivityBluetoothMod__OnNewBluetoothDeviceScanned_mBDEFCE27A0B08B5649F753D11C36FEF651F8EF0B,
	ConnectivityBluetoothMod__UpdateDevicesDictCoroutine_m66F58DEE4E1E18588B84E1A91177D6B4A3B5AB3A,
	ConnectivityBluetoothMod__UpdateDeviceDict_mD8C0E3819B798DF8F3BDF27985F19B765605AC8E,
	ConnectivityBluetoothMod__OnBluetoothConnected_m22BB245E9707F54F29232F95DB3DCF9444426742,
	ConnectivityBluetoothMod__OnBluetoothDisconnected_m9D822D2A877814CBD75ECC8BB1BA6EA5FDD378DE,
	ConnectivityBluetoothMod__ConnectingTimeout_m0CB3327A7251A44E95C7F9E12BBD3527DFA327D3,
	ConnectivityBluetoothMod__OnConnectionFailed_mFAA621839FBE600C13B74EE4793DF240E2B850DB,
	ConnectivityBluetoothMod__SendMessage_mCAB419CFF0F4EE829603993CD83AE8765E7FFE1A,
	ConnectivityBluetoothMod__OnReceivedBluetooth_m348644DAE04C35E5F10977EF772D060EF0841FD2,
	ConnectivityBluetoothMod_FullUUID_mE67E757E51FBA978381D83655E8AE1D8D92A1FDB,
	ConnectivityBluetoothMod_IsEqual_mA4E679440930F6F9DFC5FDC4AC6E274A58311655,
	ConnectivityBluetoothMod__ctor_m599AC676CBF52448A3E76DC5A0E3D11824D99CED,
	ConnectivityBluetoothMod_U3CUStartU3Eb__16_1_m9C9B262B3B1F99276E4CFEA1E09ADD3D2ED7F77C,
	WrittingOptions__ctor_mFBA789066556201EC5179B222E5FBB01E5BF3A33,
	WrittingOptions_get_ServiceUUID_m9FE5667A305B4990FB7E89C394389CE66B19205A,
	WrittingOptions_get_WrittingCharacteristicUUID_m6E4B8220C3658DB13C8B751B60426DFC07746F86,
	U3CU3Ec__cctor_mD9BABA6D089A377FAE40F68CD590FA0DE3CD1D69,
	U3CU3Ec__ctor_mFBA90D6A11A14AEAFDB089F058B8A4E5BBEE21BE,
	U3CU3Ec_U3CUStartU3Eb__16_0_m6F274CF11388BA11CE77B0E7076FDD2222BA81FA,
	U3C_UpdateDevicesDictCoroutineU3Ed__29__ctor_m9F35222C1B4EE5B1CC1EFC81B95DBE984DD48E80,
	U3C_UpdateDevicesDictCoroutineU3Ed__29_System_IDisposable_Dispose_mEA6CAA59626221B0CA9E00CF330406F113C276BB,
	U3C_UpdateDevicesDictCoroutineU3Ed__29_MoveNext_m4171CBFDF126E9C333594B9B2E1F45D035B744D8,
	U3C_UpdateDevicesDictCoroutineU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD92BF00321587BFFAE3DD9745E96F27FA1A4537B,
	U3C_UpdateDevicesDictCoroutineU3Ed__29_System_Collections_IEnumerator_Reset_mE9A8848F3FF141E5E8933D3E190711F213B0B380,
	U3C_UpdateDevicesDictCoroutineU3Ed__29_System_Collections_IEnumerator_get_Current_mFF452C4A0C4D18FDEE3C6111707FB42489FDE4DA,
	U3C_ConnectingTimeoutU3Ed__33__ctor_m928E1438A517DD9CD041CFF2D45311684FF2DFA9,
	U3C_ConnectingTimeoutU3Ed__33_System_IDisposable_Dispose_m311FD9ACE79C3007C00B8C707550DA4318D9322D,
	U3C_ConnectingTimeoutU3Ed__33_MoveNext_m0A418B91F4FA6C41E58925A5338FE5FD1FB3DCF7,
	U3C_ConnectingTimeoutU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE631ADC063FA57F92EFD0E2D9A69B87E5C9BE4F,
	U3C_ConnectingTimeoutU3Ed__33_System_Collections_IEnumerator_Reset_mDC7A51226F8F03B37DF804E94AB4796D803B905C,
	U3C_ConnectingTimeoutU3Ed__33_System_Collections_IEnumerator_get_Current_m03A9B90356959758481729A957BF151A1E41538E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ConnectivityBluetoothDevice_set_DeviceName_mC9480657085DE7012C65F68C0EB568209E85BE5C,
	ConnectivityBluetoothDevice_get_DeviceName_m669AEE4E40AFBFE95968796C6BEBD4A34219AB4B,
	ConnectivityBluetoothDevice_set_DeviceAddress_mDD064497D3272AE5EE4FB23E8F6A6535FC6665A4,
	ConnectivityBluetoothDevice_get_DeviceAddress_m6ABEEB67D017CA79644D12BA0D75A8BB956ED80C,
	ConnectivityBluetoothDevice_set_TimeUpdated_m1603CD330D2A3CD27ADE800BF8A34E14C32ED709,
	ConnectivityBluetoothDevice_get_TimeUpdated_m04E9E07770BBD593D13CF4DC3175DFBB19FA148B,
	ConnectivityBluetoothDevice__ctor_m563173B542AB4C5A28B926C77D2087D1889F77DC,
	ConnectivityBluetoothDevice_Equals_m9BD668F9D01419A834EE427989FF750C9D1F3FFE,
	ConnectivityBluetoothDevice_CompareTo_mA72D02EA02698CF9D46E3B8CEDB6D1CE3BB6CFED,
	ConnectivityVariables__ctor_mDEDA285F45649553E0B2AF6C2D3F98B6A00466B7,
	GIFRecorder_get_pixelWidth_m9BACC5B1EC0F95CC30CF9952E7B0A35EB8D76057,
	GIFRecorder_get_pixelHeight_mEE6A62FBA76BFFE3A83733A5E9FFD8258D4AFEFC,
	GIFRecorder__ctor_mCCCDFEE06E3452EA45B2BB6EEFA04F7812CFB69F,
	GIFRecorder_Dispose_mEE1FD356D6E9FE80403343DDC2B04ED1DD5C127B,
	NULL,
	GIFRecorder_CommitFrame_m083F3F8E7632FD7D2A71921E50C5EEDA020211C5,
	GIFRecorder_CommitSamples_mAC2388D005653E08572E6B54C3EFBA526CE103A3,
	HEVCRecorder_get_pixelWidth_mA79F8CCC89B197940532C072DFC9F2B2402422D1,
	HEVCRecorder_get_pixelHeight_mF1FF242F69209A2CBCDEBDA65E276FDB9937C825,
	HEVCRecorder__ctor_m60B35AA9DF4647D4B885FFABF0CDA77C0AE37D2D,
	HEVCRecorder_Dispose_m4C35637D90FFB1240982AF0BB3C7A1F04CCD22B5,
	NULL,
	HEVCRecorder_CommitFrame_m43CD654CDCDE1A367450B7F0791739658DD180F5,
	HEVCRecorder_CommitSamples_m9A498A28FC77598EEDCD2F5F1E3D7CC7344BDB89,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JPGRecorder_get_pixelWidth_mFE5DC0E2D91FEDF6E7D217A76396480F4A4CB4C6,
	JPGRecorder_get_pixelHeight_m4E96F5B9217ED7BB9F33C818404F07B2CEF1AA8E,
	JPGRecorder__ctor_m68925C076D07B49B8889A0E8174690A0EE45FDBE,
	JPGRecorder_Dispose_mF525A4D90BF6F36055C9DD56488B69990F184985,
	NULL,
	JPGRecorder_CommitFrame_mE291CF99B2219375E18331B5DC0B8352000E68C5,
	JPGRecorder_CommitSamples_mB72B6F564ED34442DD6FF728DED9BA62FA14F86D,
	U3CU3Ec__DisplayClass4_0__ctor_m836680AD1E908D253A5E4A1699FF6C116748AA74,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mFD00057C06CC757D7BF28073B511D368529B9209,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__1_m5FAC3F1BC07E7620CA0F614C66FDDDBA1D505BF6,
	MP4Recorder_get_pixelWidth_mAF074D9DDD74EEF9255E84BA12763816B609C1BC,
	MP4Recorder_get_pixelHeight_mE7C72A66415850352A37C3918ACA7AB8B93D98B1,
	MP4Recorder__ctor_mCB79216EB6F12DE47C8B105E24C97BBAE038DEB6,
	MP4Recorder_Dispose_m017663B9B455170ED943D118A978FF8E7E14C31D,
	NULL,
	MP4Recorder_CommitFrame_m16AAE4E064C24A088AE4DF8F47D815AE0C8825D0,
	MP4Recorder_CommitSamples_mAC7DB615D4F1D5DB0D291084DE8D49BE43EE8257,
	MediaRecorderAndroid_get_pixelWidth_m0E23E101BD19DC44818C4FE0E8053A0641920C97,
	MediaRecorderAndroid_set_pixelWidth_m20A15A638F55F9E73B4346B540328CFF6BAAEDBB,
	MediaRecorderAndroid_get_pixelHeight_m24C3F12DC6839881F72FD264CB8289DB6C6E95FA,
	MediaRecorderAndroid_set_pixelHeight_m7A2BF60FF8D05304B9FCAA84E8DEE1464B431510,
	MediaRecorderAndroid__ctor_mE6FB9F2711DC2287519C848A21130ECD41E8679E,
	MediaRecorderAndroid_Dispose_mB57F5D9C990072BFFB57497CE286A31E964D2A06,
	NULL,
	MediaRecorderAndroid_CommitFrame_mC7D62685D5F820BC11419B56B77A39D529D275D7,
	MediaRecorderAndroid_CommitSamples_m2F689E28142A770D98AFF33F952430255E58D3C9,
	MediaRecorderAndroid_onRecording_m7045DD6231BD95C434BE9337C52C60B9A1EA4C25,
	MediaRecorderBridge_CreateMP4Recorder_m58A6C1F7018A908181A2142385E776E11F38642C,
	MediaRecorderBridge_CreateHEVCRecorder_mFE25374D7A4C2807A3EB98FD7C9DD89C5FEF30F0,
	MediaRecorderBridge_CreateGIFRecorder_m4148146363D13169CED1E5C7375B57DABD9AB1D8,
	MediaRecorderBridge_StartRecording_m1F25D2D0ECB5208E825948736E1ABC7C38C4120A,
	MediaRecorderBridge_StopRecording_mC855C6DD8199E21AAD124FBA059EC8806BA38954,
	MediaRecorderBridge_EncodeFrame_m619ED14A5CC1077420461B57F3CC49944F4E04C0,
	MediaRecorderBridge_EncodeSamples_m9D38D08F94BA38407BB3F5FA1B1A18E8492E3596,
	NULL,
	MainDispatcher__ctor_mE0DDCD632F93E1D69C359FCF5D8A90AE6BC10F47,
	MainDispatcher_Dispose_mF89D36DDE3EDD7E803EE1EC4AACE9A9B2EE2B5E5,
	MainDispatcher_Dispatch_mBFC12264784571E0C4D075C1D9D8E0711A4C281E,
	MainDispatcher_Dispatch_mA2DB3927DDE0FD114D65DA997AF3AD7AAD9B7394,
	MainDispatcher_U3CDisposeU3Eb__3_0_mB21B7CE05AD414BF3EB0D16DE904997CAD0B87CA,
	MainDispatcherAttachment__ctor_mC3B6A643408DC00EB2B967920A241160B924255F,
	U3CDispatchU3Ed__5__ctor_mA30F123D252463D1C5B7EAE5BF2FC52D206C7C36,
	U3CDispatchU3Ed__5_System_IDisposable_Dispose_m23814C2E32D13F9A865F68857ECB5408F7F6D84C,
	U3CDispatchU3Ed__5_MoveNext_mFEEDFDFED9525D58EBE889622465488F76001A05,
	U3CDispatchU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7466FE9F0ED9E13B00AB02A5DFA1C1EC9D79CE74,
	U3CDispatchU3Ed__5_System_Collections_IEnumerator_Reset_mA2DC9EFA5BB726718062FB492421042D547CAEB9,
	U3CDispatchU3Ed__5_System_Collections_IEnumerator_get_Current_m60AB35AB3AAE9A9647A9B2A94EB962DDCB0C39F9,
	RenderDispatcher_Dispose_m3BB88176F76AAE5E9E244634BC601D69C97701E6,
	RenderDispatcher_Dispatch_mFFBC9D2F2402DD16B8719459733425820837EAC8,
	RenderDispatcher_DequeueRender_mEFFD3475E3F871614C267EA217B6342B76758068,
	RenderDispatcher__ctor_mEC3DC5C2BC4351A1E3FDF1AA7E4EBD5275251B8A,
	UnityRenderingEvent__ctor_m2853E29B957CD11906BCD10FA826BB1FF23F902A,
	UnityRenderingEvent_Invoke_m0F12D4E8F72E45DEF43A8D8A4A1E2E9A4DB3F5F6,
	UnityRenderingEvent_BeginInvoke_m5A08DE6D56DEAAAEA7F2279CE1A8595C0B85887F,
	UnityRenderingEvent_EndInvoke_mE3E71FD57414876051ADB9ED30E26D7B86BA52CC,
	DocAttribute__ctor_mBC82A60195829F2472D17A6C4CABCFEF3DFBBB5F,
	DocAttribute__ctor_m4525DD9B6D7AD8F9DDF75D215A69EF62460EA994,
	CodeAttribute__ctor_m34351EB38EA83AA3881F57FAD8A5E2E76C0C6E5D,
	RefAttribute__ctor_mC1A02BA2DF07E77D619DB8B81F9A5F0CC324F56C,
	MediaRecorderiOS_get_pixelWidth_m4780C0C24E7980C65EFD7E2D89F59F9F3ED95CB4,
	MediaRecorderiOS_set_pixelWidth_mF4499037A49E41164D79AB73147725B5CE9FE6D3,
	MediaRecorderiOS_get_pixelHeight_mA60D74F64CC0C20CBFFA45478C1942E222EFAC6C,
	MediaRecorderiOS_set_pixelHeight_mF5FAB82E599F910E07D4E743267CEB07ADCBDAFB,
	MediaRecorderiOS__ctor_mD371B35454380FA80A028B8A4101273B4A3EE3EF,
	MediaRecorderiOS_Dispose_m4D9ECC93697DEB592A632718E3BBE3125FCD9160,
	NULL,
	MediaRecorderiOS_CommitFrame_mD382263D5BDABCFCDAA10646D557CE908EF26FDB,
	MediaRecorderiOS_CommitSamples_m81CA1B084B8E5132D98EB7356F6B7B5082125926,
	MediaRecorderiOS_OnRecording_mE56D0FE0CEB3ADDB92F9346A3DCDAAF7F7F1C82B,
	U3CU3Ec__DisplayClass16_0__ctor_m21470B24BC9D1F1F63E117F442547FDEDCB39774,
	U3CU3Ec__DisplayClass16_0_U3COnRecordingU3Eb__0_mD9C2D36A7D6327ACEA90B9B19685D6A9B21B6FA7,
	ReadableTexture__ctor_m5850062F78C931AA622C414E6E8C4A270D354694,
	NULL,
	NULL,
	ReadableTexture_op_Implicit_mBD7C376F81F9640007ED1B4626EADD60026F75C9,
	ReadableTexture_ToReadable_m2EA76694B861E20A30EA150F6623878B84702D1B,
	SyncReadableTexture__ctor_m2B7A5898B9FF5180D7D4D86B8CBB4D014B9AC8CC,
	SyncReadableTexture_Dispose_mA6758F17642BF12A421E4B6EB429F573138EF315,
	SyncReadableTexture_Readback_mD4CA026B7A01E2B95176A6978E588D2462F502B8,
	AsyncReadableTexture__ctor_m046C0476F150F3EC21BDA5E7701AB4472F37E0BE,
	AsyncReadableTexture_Dispose_m426EB6F7C55DF2A12F249C09AB56A2AE560B0508,
	AsyncReadableTexture_Readback_m10412E74072CE54D6FD28CBFB492320F2690DDF3,
	U3CU3Ec__DisplayClass2_0__ctor_mCFF2986C3CBC52BF7ADCAC742F7B1BB23685D581,
	U3CU3Ec__DisplayClass2_0_U3CReadbackU3Eb__0_mDA4D7E7EA7AD4E4B0F1BBF61195A1FEBC2ED57EC,
	GLESReadableTexture__ctor_m196DB4300B4CD96AD6057AC0BB0C996F95716811,
	GLESReadableTexture_Dispose_mF260A297CC4D02A6BCC30254B34C917B679FEDCB,
	GLESReadableTexture_Readback_m620FB940BC720BFB5F23945547C2B3DFD4E887A4,
	GLESReadableTexture_U3C_ctorU3Eb__0_0_m4A864B01073354DACAAEBC56388EAF70F2958AA8,
	Callback__ctor_mD130616C7015ED5193A197E080108DF86D69EBBF,
	Callback_onReadback_m18AFD33BCB5CB9463C58D6EEA948B7297D00E785,
	U3CU3Ec__cctor_mE3DE84F7A001560AB3A3B19BF81159316048A07D,
	U3CU3Ec__ctor_m2C7C5375F4DC925A731601B6E42111CA7F9B96DD,
	U3CU3Ec_U3C_ctorU3Eb__0_1_m753A83CC4C54BDCAFF428F3E0324479AD3CF56A4,
	U3CU3Ec__DisplayClass2_0__ctor_m79DF2E9C30EED79E92D318D28A818FF9264A838C,
	U3CU3Ec__DisplayClass2_0_U3CReadbackU3Eb__0_mBC36D143E4D108834471126145A420E712EF9AAD,
	AudioInput__ctor_m25269E64AF348B8C5DC4FF7726675C61F87E0552,
	AudioInput__ctor_m21EB345A2B01E9C96299ACA2182DB698A5BCDEB9,
	AudioInput_Dispose_mD5BA89EB97D9BDCFDB3FB337AF453CCFEDB3906D,
	AudioInput_OnSampleBuffer_m8369B434029C6D28AC7A4B1FB335FCBB745AC462,
	AudioInputAttachment_OnAudioFilterRead_m77DE56234BCDD8101CBA924807F8FF306F9F58A0,
	AudioInputAttachment__ctor_m4DE723266ACFA80DF3D0177F4D55CD2D6CED2705,
	CameraInput__ctor_m376BFFB128C731067BEDDB7B9FC2446BE9A29FE5,
	CameraInput_Dispose_mD8812F99A4F961A0345344BA51E3D72A23C7422C,
	CameraInput_OnFrame_mFAA93E723FD2ADA7F4E71210D4D5F1D50502D960,
	CameraInputAttachment__ctor_m43E111D84719E8E36D65DBD960079DB3A85321A0,
	U3CU3Ec__cctor_m80357168A84276574A6935C27CD9D63E0A299C98,
	U3CU3Ec__ctor_m8037E2EFD8C8DB296B18C97860F45C4BBFCF2942,
	U3CU3Ec_U3C_ctorU3Eb__1_0_m283396D7EFBF662235B11F77A140F1B13D12A6B1,
	U3CU3Ec__DisplayClass9_0__ctor_m9E170F06ED28EC21EA5DF1B2B2FDC31ED0F498D8,
	U3CU3Ec__DisplayClass9_0_U3COnFrameU3Eb__0_mD35D5D5D4F10A43B0968AD7AB39F6465C1AB5BF1,
	U3COnFrameU3Ed__9__ctor_mF19EBABAE8E9F0046C94AB4943D6E08318DE47C0,
	U3COnFrameU3Ed__9_System_IDisposable_Dispose_m363610CACB70E6B6DAEBEB7EF97675F66C31393D,
	U3COnFrameU3Ed__9_MoveNext_m515C848F2489BAA470002045F68906F2684D6767,
	U3COnFrameU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93B13B9770A1E119715908B5291E37A8F89AEEDE,
	U3COnFrameU3Ed__9_System_Collections_IEnumerator_Reset_m0DA41DF41004E2B0ED40E774958E382773183696,
	U3COnFrameU3Ed__9_System_Collections_IEnumerator_get_Current_m328D113DC66B0B7C486C7B7A400C9F732483E220,
	FixedIntervalClock_get_Interval_mBB76A5A9122DB9C81BF38ED7A21B69293BEECAC8,
	FixedIntervalClock_set_Interval_m040BECACD7031410D74E3F031A6F1B9C1AA25338,
	FixedIntervalClock_get_Timestamp_m050A3A243CE7F1972688E30A246AE9A08C995984,
	FixedIntervalClock__ctor_m802625172681F6292BD3988A3260407BB5F7E481,
	FixedIntervalClock__ctor_mCC9A1FB9012428D73BF5ADC57F565A083DD12869,
	FixedIntervalClock_Tick_m342D5598E31A4C3690B345D73086A7361F7A6F4A,
	NULL,
	RealtimeClock_get_Timestamp_m30135424830FF8FB4D7314951C156C9AD0DBA035,
	RealtimeClock_get_Paused_m396C02C9576E0579A2872FC7D717BC36BA5F72B6,
	RealtimeClock_set_Paused_mCBB8426AC777F406E210693B8ED14A40FEEB58A4,
	RealtimeClock__ctor_m6DA82146AABEE26E0F84F52993FDE838DCE60D9D,
	Giffy_StartRecording_m20CDDD22A37E21F18D149A5600E2DA086AE1266C,
	Giffy_StopRecording_m66489FCC5A4747044EAC1E9FE473F41841B18421,
	Giffy_OnGIF_m8F7AF2803F5BC3951AC959A4240F93ADD40B9792,
	Giffy__ctor_m1306C6996698E6931A35429F38723B5187AA6161,
	CameraPreview_get_cameraTexture_m6A50043A8AE306D1A0DC813D0883416777E9913D,
	CameraPreview_set_cameraTexture_mD53551045B0ADE714163EAFAB77C402D390E1C0D,
	CameraPreview_Start_m97AD7612CD33BCC3D74F43A1BDEBA56740CFA3E7,
	CameraPreview__ctor_m27F007C9E0AECDAED14DBE234E9B37D49D2058CF,
	CameraPreview_U3CStartU3Eb__6_0_mCD40B5BC39952B4C3CBC7C8B97D354C2423184B7,
	U3CStartU3Ed__6__ctor_m61BEBDA2E46AC32151C858C7F53C9A914ACEE5E1,
	U3CStartU3Ed__6_System_IDisposable_Dispose_mAF75455F3F2B57699528B948BDF2C95447D1A159,
	U3CStartU3Ed__6_MoveNext_m2CA9F69736E0E748B0DE20F3410D3138C0A2CD24,
	U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m084DE236D6A9FE77E3A15E7980A94F736A28BECF,
	U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_m94B9B92CAC65CFD019E52EB2ABAC56C0FB8CC802,
	U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_m67880265D537388330521F8F1D4633FDCBF36A3C,
	RecordButton_Start_mD5668CFD5A334CB4ADCB5CAA3CECAD557703F938,
	RecordButton_Reset_mAA277DB1180D42D192A2183164B547B5CF1B558E,
	RecordButton_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_mF943B91B92F4E59868646EE573A5A1997CC43F5E,
	RecordButton_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m23CFF22D04CAE280229A16400A813D6C5E541A00,
	RecordButton_Countdown_m58C67C3558679FA5D607E04F53AC6427525E0F06,
	RecordButton__ctor_mD1F4AD2CC2FB8A5A830074A6B58EC0E86CE929C4,
	U3CCountdownU3Ed__10__ctor_m7FD8BC0A80F9462FEE9E5F5CF1F5A86A2486AD29,
	U3CCountdownU3Ed__10_System_IDisposable_Dispose_mD4E0A17948270F69CD6F9E4E40FF6B5D60A2DA9F,
	U3CCountdownU3Ed__10_MoveNext_mE0158E4A4CD062F2789CA9F77D25FF45BF960E7D,
	U3CCountdownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05EA6877C6ECE68D7AD3613C1C06E21F3DCBA3DD,
	U3CCountdownU3Ed__10_System_Collections_IEnumerator_Reset_m84AA071F5BB96BD17066B96313F48A1FEC52E1DE,
	U3CCountdownU3Ed__10_System_Collections_IEnumerator_get_Current_m03F6543DFBF0F712D8AA73F7254E0C48F02681E5,
	ReplayCam_StartRecording_m4D5BE9397F4BB95DF0580D7BD97628F4A1F2BA10,
	ReplayCam_StartMicrophone_m875D22D40FAEE23453E49D8734C88A58462FDD72,
	ReplayCam_StopRecording_mD36D259861DAB3F8DED157D52810A05065283578,
	ReplayCam_StopMicrophone_mAC9FE62E3D85F436B2EE4D2F76C14A009FC46C0E,
	ReplayCam_OnReplay_mBE1DEF2C56CAF0B9E0A14C6F998BD730520C50D3,
	ReplayCam__ctor_mFCBF5AF5ABF84B739D20FAE3B55871EB91FC5F77,
	WebCam_StartRecording_m321ADA5BC74C232CCCDB110F778BB03A148BBE74,
	WebCam_StopRecording_m472B20ED414C0C529E6906124E1A8B335C3193E4,
	WebCam_Start_mC494E829B80039BC1F9554FC495E697B895E7707,
	WebCam_Update_m3A79EDDA2E38E636755143F25BEA0E9A3DDA6767,
	WebCam_OnRecording_m75081A9E922A625A74D21A6EEF1765F6DA6CAD6F,
	WebCam__ctor_m41B8CB3DA7F937A2E196CE45FC8A00C4D89E6776,
	WebCam_U3CStartU3Eb__8_0_m91D44E50129C6182EAB1F328C61A730B0DB06BA7,
	U3CStartU3Ed__8__ctor_mF47DBA782E463958505ED30A62B4BD427843B556,
	U3CStartU3Ed__8_System_IDisposable_Dispose_m34A2B4BC4CE9B1A0864BFC72C100FB4381BF0DB3,
	U3CStartU3Ed__8_MoveNext_m55BDD9B2171F9FF44DAD6FFA70E187B9D6E466DA,
	U3CStartU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01996306B749DDCDF55B17EF0D7E078E5B3E0B83,
	U3CStartU3Ed__8_System_Collections_IEnumerator_Reset_mDA213E06F07CFA944EB5E3E57E2CE3648D4D153D,
	U3CStartU3Ed__8_System_Collections_IEnumerator_get_Current_m06AF9C8ECA7E02213812F62AE80FE17AB6C78FE5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LogicBluetoothDevice_get_Name_m56F1745B39998C9FA12426D2AFD129B0B16C95BF,
	LogicBluetoothDevice_get_Address_m7E621404ED680D71E3A5FBA7C06E56F4DA4E860B,
	LogicBluetoothDevice__ctor_m07CDC6E060E4C29F6AA2810C5C1239DC43F6C14B,
	LogicBluetoothDevice__ctor_m52EE06D8081617B9951EED4E8E822D0E1D961FFD,
	LogicBluetoothDevice_Equals_m352CC57816E6CCD72BE53D86F7862F7AC66D958E,
	LogicBluetoothDevice_CompareTo_mB385E2A8AD585B19CD4131ED96E1DEC5CABE1C98,
	StoringRoutineTemplate__ctor_m1F43478193AD1B31F04F64FC09DC53E2E3CD9E37,
	DancingMode__ctor_mCB8994B88388F5C6E0BA8F686A837DC49E6A0E30,
	ArRobotLogicConnectivityVariables_GetState_m9FBD91A77587AA5044630F0A2116502A89DAEB6B,
	ArRobotLogicConnectivityVariables__ctor_mD38A6AF84F5773718F8DAB97E82F90AA7EFFBC3D,
	NULL,
	NULL,
	NULL,
	NULL,
	SensoDialogMod_get_DialogEnabled_m64D88EC0432BBE9048196E77ED531321ADD6DCE0,
	SensoDialogMod_set_DialogEnabled_m99961CA83305AF7DE9A5CC2CC5B623E037DF6E59,
	SensoDialogMod_ShowDialog_mF516188C0088437C5F5E167F9E9F78FBD5CBC0C7,
	SensoDialogMod_ShowDialog_m0DC3D2A39DFDAA6731BAE104797EB2A2D178DC83,
	SensoDialogMod_ShowDialogWithInputField_m521584534CEE26662EFF64386867F48E0A5E4CEB,
	SensoDialogMod_ShowDialogWithInputField_mE7E9629C060ECDC0E423252E9396DBF8C169EB49,
	SensoDialogMod_OnListenersAndModRegistration_mE2E217076C9AFD3B7609DE58B7819DB16DA294B1,
	SensoDialogMod_OnDestroyListenersAndModUnregistration_m4F5CD70FBD0CFC15DE8F68C58B3242FF2DEC0E95,
	SensoDialogMod_UStart_m0D3D0C8C9D2BB8CB6975A94F6F36886D6B19342B,
	SensoDialogMod_OnAcceptButtonClicked_m3AEBEF3F5FE218B9208BCBC28FA75BD08401806F,
	SensoDialogMod_OnCancelButtonClicked_m778C255AE5B3AB74E21A39576C92F502F7E5CCE1,
	SensoDialogMod__ctor_mE069BF94C37D9DF551972ED235BBBBA4137976E3,
	ArRobotAppConfiguration_OnInitialializeConfiguration_mC35593F448004FD4AD29D3F832A925EE54E2BA92,
	ArRobotAppConfiguration__ctor_mA388AC3F8805BC8FA75636ECEF768FFEA079C951,
};
extern void U3C_WriteTextAsyncU3Ed__10_MoveNext_mB047908016279CE15714F3FA197BEF9447B2C19F_AdjustorThunk (void);
extern void U3C_WriteTextAsyncU3Ed__10_SetStateMachine_m796A7F9239E531781DEFA9126F8DE85368105461_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600056A, U3C_WriteTextAsyncU3Ed__10_MoveNext_mB047908016279CE15714F3FA197BEF9447B2C19F_AdjustorThunk },
	{ 0x0600056B, U3C_WriteTextAsyncU3Ed__10_SetStateMachine_m796A7F9239E531781DEFA9126F8DE85368105461_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1746] = 
{
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2755,
	3267,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	2417,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	5231,
	3352,
	3352,
	2723,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3248,
	2706,
	3248,
	2706,
	3352,
	3352,
	3352,
	3352,
	3211,
	2669,
	3267,
	2723,
	847,
	3267,
	3267,
	3248,
	4978,
	1533,
	3267,
	3211,
	3267,
	2723,
	2415,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3267,
	1403,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	2415,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3267,
	3267,
	3267,
	3267,
	1543,
	3267,
	2723,
	3267,
	2723,
	3267,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	3267,
	3352,
	2415,
	3267,
	3209,
	3209,
	279,
	3267,
	2723,
	305,
	130,
	3267,
	1194,
	279,
	3352,
	2755,
	2755,
	3352,
	3352,
	1931,
	1931,
	1931,
	632,
	2706,
	2413,
	2706,
	1981,
	3352,
	2706,
	2413,
	3352,
	3352,
	1543,
	3352,
	3352,
	3352,
	3352,
	2723,
	1539,
	1539,
	2723,
	2723,
	2706,
	3681,
	2734,
	3352,
	3352,
	5231,
	596,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	1419,
	1419,
	1419,
	1419,
	2723,
	1539,
	1539,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	2723,
	2723,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	1419,
	1419,
	1419,
	1419,
	2723,
	1539,
	1539,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	2723,
	2723,
	2723,
	3352,
	3352,
	3352,
	1571,
	3352,
	3352,
	3352,
	279,
	3352,
	3352,
	147,
	1268,
	3352,
	3352,
	2723,
	1539,
	1539,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	147,
	1268,
	3352,
	3352,
	3352,
	3352,
	2666,
	3352,
	2706,
	3352,
	2706,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	2706,
	2417,
	3352,
	3352,
	3352,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	2723,
	3352,
	3352,
	2706,
	2417,
	3352,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	833,
	2706,
	1571,
	2706,
	3267,
	3352,
	2417,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	3352,
	3352,
	2706,
	3352,
	3352,
	3352,
	3352,
	2723,
	2723,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2666,
	279,
	3352,
	2706,
	2706,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2666,
	3352,
	147,
	1268,
	279,
	2706,
	2706,
	3352,
	2723,
	1539,
	1539,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	2706,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	5231,
	3352,
	2415,
	3352,
	3352,
	3352,
	2666,
	279,
	2706,
	2706,
	3352,
	3352,
	3352,
	3352,
	3352,
	833,
	3352,
	3352,
	2706,
	2706,
	3267,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	833,
	2706,
	2706,
	3267,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	3352,
	2706,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	1539,
	1539,
	2706,
	3352,
	3352,
	3352,
	5231,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2784,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2706,
	2723,
	2413,
	2413,
	2413,
	2413,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2706,
	2411,
	2411,
	2706,
	2706,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2723,
	3248,
	2706,
	3352,
	3352,
	2666,
	3352,
	2784,
	3352,
	2413,
	3352,
	5231,
	3352,
	2706,
	2706,
	3352,
	1950,
	2706,
	1543,
	3352,
	3352,
	3352,
	3352,
	3352,
	2784,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	1531,
	2723,
	3352,
	3352,
	3352,
	3352,
	2784,
	3352,
	0,
	3352,
	3352,
	2723,
	1539,
	1539,
	3352,
	2723,
	3352,
	1440,
	3352,
	3352,
	3352,
	2415,
	955,
	2723,
	2666,
	3352,
	3352,
	1543,
	848,
	2723,
	848,
	2723,
	3352,
	3352,
	5231,
	3352,
	2723,
	2723,
	2723,
	5121,
	3352,
	3352,
	3352,
	5186,
	5114,
	3352,
	3352,
	3352,
	3209,
	2666,
	2723,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2415,
	955,
	2666,
	2723,
	3352,
	2723,
	2723,
	848,
	2723,
	848,
	5231,
	3352,
	2723,
	1543,
	1543,
	2723,
	2723,
	3352,
	3352,
	3352,
	3352,
	3209,
	2666,
	2723,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2415,
	955,
	2666,
	2723,
	3352,
	2723,
	2723,
	848,
	2723,
	3352,
	848,
	5231,
	3352,
	2723,
	1543,
	1543,
	2723,
	3352,
	3352,
	2415,
	3352,
	3352,
	1543,
	3352,
	3352,
	3352,
	3352,
	1543,
	591,
	5231,
	3352,
	3352,
	2723,
	3209,
	2666,
	2723,
	2723,
	3352,
	848,
	3209,
	2666,
	3352,
	3209,
	2666,
	3352,
	3352,
	3352,
	3352,
	2461,
	1222,
	1088,
	1088,
	1088,
	587,
	4981,
	955,
	587,
	596,
	3352,
	2723,
	2723,
	2723,
	848,
	2723,
	3352,
	2723,
	2723,
	2723,
	1543,
	3352,
	2723,
	2723,
	2723,
	1543,
	5231,
	3352,
	1543,
	3209,
	2666,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	1543,
	2723,
	3352,
	3352,
	3352,
	5231,
	3352,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	731,
	3352,
	3352,
	3352,
	2806,
	5231,
	3352,
	2723,
	3352,
	3352,
	1543,
	3352,
	3352,
	3352,
	3352,
	3352,
	5231,
	3352,
	3352,
	1543,
	2723,
	3352,
	1543,
	2706,
	2706,
	2415,
	3352,
	5231,
	3352,
	1543,
	2723,
	3352,
	2723,
	2723,
	2723,
	848,
	3209,
	1203,
	2723,
	2723,
	3352,
	1440,
	3352,
	3352,
	3352,
	955,
	3352,
	5231,
	1543,
	2723,
	3352,
	3352,
	2723,
	1543,
	848,
	2723,
	848,
	3352,
	5231,
	3352,
	1950,
	2723,
	3352,
	2723,
	3352,
	1950,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	591,
	5231,
	3352,
	3352,
	2723,
	3209,
	1203,
	2723,
	3352,
	1440,
	3352,
	3352,
	3352,
	955,
	3352,
	5231,
	3352,
	2723,
	1543,
	848,
	2723,
	1543,
	2723,
	2723,
	848,
	3352,
	5231,
	3352,
	1950,
	2723,
	3352,
	1950,
	3352,
	1440,
	3352,
	3352,
	3352,
	3352,
	2415,
	955,
	2666,
	3352,
	3352,
	3352,
	1543,
	591,
	848,
	848,
	2723,
	3352,
	3352,
	5231,
	3352,
	2723,
	2723,
	2723,
	3352,
	3352,
	1440,
	3352,
	3352,
	2723,
	3352,
	3352,
	2415,
	955,
	2666,
	3352,
	3352,
	3352,
	2723,
	1543,
	591,
	848,
	1543,
	1543,
	848,
	2723,
	3352,
	3352,
	5231,
	3352,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2706,
	3352,
	4245,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	2723,
	3352,
	3352,
	3352,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3352,
	305,
	2723,
	2706,
	1192,
	1403,
	2706,
	3352,
	3352,
	2706,
	1403,
	1403,
	1403,
	1403,
	508,
	596,
	1376,
	2666,
	3352,
	1403,
	1419,
	2723,
	799,
	1419,
	3352,
	3352,
	2666,
	723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	3352,
	1543,
	3352,
	3352,
	4981,
	1543,
	3352,
	2723,
	1543,
	3352,
	3352,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	2666,
	2666,
	3352,
	1592,
	3352,
	3352,
	2706,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	0,
	0,
	0,
	3352,
	3304,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3209,
	3209,
	3343,
	3343,
	3352,
	3352,
	3352,
	847,
	847,
	847,
	1566,
	3352,
	3267,
	3352,
	3352,
	5220,
	3267,
	3352,
	3267,
	3304,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3267,
	2723,
	3352,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3352,
	3352,
	1531,
	2666,
	2666,
	3352,
	1531,
	2666,
	3304,
	2755,
	2666,
	3352,
	2415,
	2415,
	3352,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	0,
	0,
	2723,
	3352,
	3352,
	3352,
	0,
	0,
	0,
	0,
	3352,
	0,
	3352,
	3209,
	2666,
	0,
	3267,
	3267,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	2723,
	2723,
	3352,
	0,
	0,
	0,
	0,
	0,
	0,
	3352,
	3267,
	3352,
	3352,
	3352,
	3352,
	3352,
	2723,
	1950,
	1950,
	955,
	2723,
	1950,
	0,
	0,
	0,
	3352,
	3352,
	3352,
	2723,
	2723,
	3352,
	3442,
	1531,
	3352,
	3352,
	3352,
	1541,
	3352,
	1203,
	2723,
	3352,
	2723,
	3352,
	2723,
	2723,
	2723,
	2723,
	3209,
	2666,
	3352,
	0,
	0,
	3352,
	3352,
	2723,
	2723,
	3352,
	3352,
	0,
	0,
	4436,
	4203,
	4436,
	804,
	804,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	2723,
	2723,
	3352,
	3267,
	2723,
	3267,
	2723,
	3267,
	2723,
	3267,
	2723,
	3352,
	3352,
	3352,
	3352,
	3267,
	2723,
	3267,
	2723,
	3267,
	2723,
	3267,
	2723,
	3352,
	3352,
	3352,
	3352,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3352,
	3352,
	1543,
	2723,
	0,
	1543,
	0,
	2415,
	0,
	3352,
	3352,
	2723,
	0,
	0,
	3352,
	0,
	0,
	0,
	0,
	0,
	0,
	1981,
	3352,
	3352,
	3352,
	2417,
	4998,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	0,
	0,
	3352,
	3352,
	2723,
	3267,
	3352,
	1950,
	3352,
	845,
	845,
	3209,
	1950,
	3267,
	2415,
	3209,
	3352,
	3352,
	2723,
	1539,
	1539,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	0,
	0,
	0,
	0,
	3352,
	3352,
	3352,
	2723,
	3352,
	3209,
	1950,
	3352,
	845,
	845,
	3209,
	1950,
	2415,
	3267,
	591,
	3267,
	3209,
	2723,
	2723,
	3267,
	2706,
	1543,
	848,
	2415,
	955,
	3352,
	2723,
	1543,
	3267,
	3267,
	5231,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	2723,
	3267,
	2723,
	3267,
	2755,
	3304,
	849,
	1950,
	2288,
	3352,
	3248,
	3248,
	540,
	3352,
	0,
	1524,
	1540,
	3248,
	3248,
	57,
	3352,
	0,
	1524,
	1540,
	0,
	0,
	0,
	0,
	0,
	3248,
	3248,
	799,
	3352,
	0,
	1524,
	1540,
	3352,
	3352,
	3352,
	3248,
	3248,
	57,
	3352,
	0,
	1524,
	1540,
	3248,
	2706,
	3248,
	2706,
	294,
	3352,
	0,
	1524,
	1540,
	2723,
	3479,
	3479,
	4163,
	3986,
	5119,
	4317,
	3985,
	0,
	3352,
	3352,
	2723,
	3267,
	3352,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	2723,
	5117,
	3352,
	1541,
	2706,
	703,
	2723,
	2723,
	1543,
	2723,
	2723,
	3248,
	2706,
	3248,
	2706,
	286,
	3352,
	0,
	1524,
	1540,
	4736,
	3352,
	3352,
	2723,
	0,
	0,
	4981,
	4981,
	2723,
	3352,
	2723,
	2723,
	3352,
	2723,
	3352,
	2662,
	2723,
	3352,
	2723,
	1520,
	2723,
	1520,
	5231,
	3352,
	3352,
	3352,
	3352,
	848,
	594,
	3352,
	2723,
	1539,
	3352,
	848,
	3352,
	3267,
	3352,
	5231,
	3352,
	1089,
	3352,
	2708,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3225,
	2686,
	3249,
	1376,
	1274,
	3352,
	0,
	3249,
	3209,
	2666,
	3352,
	3352,
	3352,
	2723,
	3352,
	3267,
	2723,
	3267,
	3352,
	3209,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	2723,
	2723,
	3267,
	3352,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	3352,
	3352,
	3352,
	3352,
	2723,
	3352,
	3352,
	3352,
	3267,
	3352,
	2723,
	3352,
	3209,
	2706,
	3352,
	3209,
	3267,
	3352,
	3267,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3267,
	3267,
	1543,
	2723,
	1950,
	2288,
	175,
	3352,
	4921,
	3352,
	0,
	0,
	0,
	0,
	3209,
	2666,
	91,
	17,
	91,
	17,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
	3352,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[2] = 
{
	{ 0x06000628, 3,  (void**)&RenderDispatcher_DequeueRender_mEFFD3475E3F871614C267EA217B6342B76758068_RuntimeMethod_var, 0 },
	{ 0x0600063B, 1,  (void**)&MediaRecorderiOS_OnRecording_mE56D0FE0CEB3ADDB92F9346A3DCDAAF7F7F1C82B_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[20] = 
{
	{ 0x020000CF, { 0, 8 } },
	{ 0x0200010F, { 32, 3 } },
	{ 0x060004EE, { 8, 1 } },
	{ 0x060004FA, { 9, 2 } },
	{ 0x060004FB, { 11, 1 } },
	{ 0x060004FC, { 12, 2 } },
	{ 0x060004FD, { 14, 2 } },
	{ 0x060004FE, { 16, 1 } },
	{ 0x060004FF, { 17, 2 } },
	{ 0x0600050D, { 19, 1 } },
	{ 0x0600050E, { 20, 1 } },
	{ 0x0600050F, { 21, 1 } },
	{ 0x06000531, { 22, 3 } },
	{ 0x06000532, { 25, 4 } },
	{ 0x06000564, { 29, 1 } },
	{ 0x06000566, { 30, 1 } },
	{ 0x06000568, { 31, 1 } },
	{ 0x060005E8, { 35, 1 } },
	{ 0x060005EF, { 36, 1 } },
	{ 0x06000605, { 37, 1 } },
};
extern const uint32_t g_rgctx_Dictionary_2_ContainsKey_m39F38AE1185BF89AD9AE162EBC1DCDD5C0AB7864;
extern const uint32_t g_rgctx_Dictionary_2_t84E9CD7DAEFF95B95793F8C9C2895624B866947C;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mDB7BF6A5FE1C84CDCD57188D65F18D3A750DCB42;
extern const uint32_t g_rgctx_Dictionary_2_Add_m5CD97EB0EE90917CD971BF0921DCBA176042F169;
extern const uint32_t g_rgctx_Dictionary_2_TryGetValue_mF84EA547D74950F68AB30653E5AFE470F55CA9B8;
extern const uint32_t g_rgctx_Dictionary_2_ContainsKey_m7FA5DEDB4A902575CBECF950521E6F3486E2B632;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m201E6C7023ED418BCF2B5A54A872BA1E18AF98B7;
extern const uint32_t g_rgctx_Dictionary_2_t9B17EBA8BD7FC60E51F0A2271AAE2A86164DDDED;
extern const uint32_t g_rgctx_T_t691B9F8999A7DABF1427E3D390A7D8F03FE39403;
extern const uint32_t g_rgctx_T_t0A595DD806A07F296246ECB4AE0A621A95578346;
extern const uint32_t g_rgctx_T_t0A595DD806A07F296246ECB4AE0A621A95578346;
extern const uint32_t g_rgctx_T_tCCF38E870EF0B5B1BF200F6F2FF00D262B165BD2;
extern const uint32_t g_rgctx_T_t94B391F90E850B3A33E6A46B26BCA462CBA0CE47;
extern const uint32_t g_rgctx_T_t94B391F90E850B3A33E6A46B26BCA462CBA0CE47;
extern const uint32_t g_rgctx_T_t8082EEDD24170A605523576E3548A0E8CD92192C;
extern const uint32_t g_rgctx_T_t8082EEDD24170A605523576E3548A0E8CD92192C;
extern const uint32_t g_rgctx_T_t04AC430F3B13C69A5CC8D3B13768B7D0D42D385A;
extern const uint32_t g_rgctx_T_tC880EC598EDEBB55B800DD6B6571AFCD4C1C562D;
extern const uint32_t g_rgctx_T_tC880EC598EDEBB55B800DD6B6571AFCD4C1C562D;
extern const uint32_t g_rgctx_T_tAA0685086F0A29E355848B5CDCF0DA4BA3CD5D62;
extern const uint32_t g_rgctx_T_t52FDE31CC12C33476106F133E5302976B34060F8;
extern const uint32_t g_rgctx_T_tAE02B0ED03B3361AAC6B45F9562E3F65C478052D;
extern const uint32_t g_rgctx_T_tA2C2575480AC04EFCA3BF2E461169EAC0292EA98;
extern const uint32_t g_rgctx_Action_1_tEDB39B7A6FE469E69A08C0E2711422C83AD4EEE4;
extern const uint32_t g_rgctx_Action_1_Invoke_m75F3F78D5DF8CE35552DBD19411ED3DF9DB66C98;
extern const uint32_t g_rgctx_T_t828DF75DD021B59AE4BB5B622C3CED374902EEA4;
extern const uint32_t g_rgctx_T_t828DF75DD021B59AE4BB5B622C3CED374902EEA4;
extern const uint32_t g_rgctx_Action_1_t2B054397C973F16AE47312FEB5C3C9E293095085;
extern const uint32_t g_rgctx_Action_1_Invoke_mD0F171D077C4A36AF29F2AFD39084FEC9AF544C2;
extern const uint32_t g_rgctx_T_t2877B872C8DC15DDFA664A9266EBD42C249189ED;
extern const uint32_t g_rgctx_StorageMod__ReadTextAsync_TisT_t21F40CFAE04CA1A95F860C091F1983A5994B4829_m2389BA6720999CE91A1B871673055AF901E841B4;
extern const uint32_t g_rgctx_AsyncVoidMethodBuilder_Start_TisU3C_ReadTextAsyncU3Ed__14_1_tAB2FAB117700700F6677202C67D31C85A9E54AED_mA68CDA4EA837FD0DE6791815E268853D3E546DE7;
extern const uint32_t g_rgctx_AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_t6207F4E776722AC1BA32517028D30E6E3071F48E_TisU3C_ReadTextAsyncU3Ed__14_1_t6D25B939757CFB113E8812F9502C7CD3643E949B_m4496A69BC2779073D5CD4628CF88DA2233F576E0;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisT_t15176190D64802DDA12A1B7418276C28F4BBB197_m692CD82F26EA0FE9BE440D8DFF311BF6C21F124E;
extern const uint32_t g_rgctx_T_t15176190D64802DDA12A1B7418276C28F4BBB197;
extern const uint32_t g_rgctx_IMediaRecorder_CommitFrame_TisT_t4DD924B2910EC161D78FE990B75EBA3086B64B94_m9362A2981559D200173DD7BB1D60920FC636A143;
extern const uint32_t g_rgctx_IMediaRecorder_CommitFrame_TisT_tB61707F7A3D83D3AF05D75A1F9D0EA438EF88511_m49DAEB92AA80702F7F6D51A1140D4BA7FB0047C6;
extern const uint32_t g_rgctx_IMediaRecorder_CommitFrame_TisT_tEE1FDAF0BEDCC9CD3B5470E4A0C4C2A9E9254392_mA03A47EA3127A81C77FB0930A2829FED8CB363F9;
static const Il2CppRGCTXDefinition s_rgctxValues[38] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_ContainsKey_m39F38AE1185BF89AD9AE162EBC1DCDD5C0AB7864 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t84E9CD7DAEFF95B95793F8C9C2895624B866947C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_mDB7BF6A5FE1C84CDCD57188D65F18D3A750DCB42 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_Add_m5CD97EB0EE90917CD971BF0921DCBA176042F169 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_TryGetValue_mF84EA547D74950F68AB30653E5AFE470F55CA9B8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_ContainsKey_m7FA5DEDB4A902575CBECF950521E6F3486E2B632 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_m201E6C7023ED418BCF2B5A54A872BA1E18AF98B7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t9B17EBA8BD7FC60E51F0A2271AAE2A86164DDDED },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t691B9F8999A7DABF1427E3D390A7D8F03FE39403 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t0A595DD806A07F296246ECB4AE0A621A95578346 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t0A595DD806A07F296246ECB4AE0A621A95578346 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tCCF38E870EF0B5B1BF200F6F2FF00D262B165BD2 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t94B391F90E850B3A33E6A46B26BCA462CBA0CE47 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t94B391F90E850B3A33E6A46B26BCA462CBA0CE47 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t8082EEDD24170A605523576E3548A0E8CD92192C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t8082EEDD24170A605523576E3548A0E8CD92192C },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t04AC430F3B13C69A5CC8D3B13768B7D0D42D385A },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tC880EC598EDEBB55B800DD6B6571AFCD4C1C562D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tC880EC598EDEBB55B800DD6B6571AFCD4C1C562D },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tAA0685086F0A29E355848B5CDCF0DA4BA3CD5D62 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t52FDE31CC12C33476106F133E5302976B34060F8 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tAE02B0ED03B3361AAC6B45F9562E3F65C478052D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tA2C2575480AC04EFCA3BF2E461169EAC0292EA98 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tEDB39B7A6FE469E69A08C0E2711422C83AD4EEE4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m75F3F78D5DF8CE35552DBD19411ED3DF9DB66C98 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t828DF75DD021B59AE4BB5B622C3CED374902EEA4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t828DF75DD021B59AE4BB5B622C3CED374902EEA4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t2B054397C973F16AE47312FEB5C3C9E293095085 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_mD0F171D077C4A36AF29F2AFD39084FEC9AF544C2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t2877B872C8DC15DDFA664A9266EBD42C249189ED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_StorageMod__ReadTextAsync_TisT_t21F40CFAE04CA1A95F860C091F1983A5994B4829_m2389BA6720999CE91A1B871673055AF901E841B4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncVoidMethodBuilder_Start_TisU3C_ReadTextAsyncU3Ed__14_1_tAB2FAB117700700F6677202C67D31C85A9E54AED_mA68CDA4EA837FD0DE6791815E268853D3E546DE7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_t6207F4E776722AC1BA32517028D30E6E3071F48E_TisU3C_ReadTextAsyncU3Ed__14_1_t6D25B939757CFB113E8812F9502C7CD3643E949B_m4496A69BC2779073D5CD4628CF88DA2233F576E0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisT_t15176190D64802DDA12A1B7418276C28F4BBB197_m692CD82F26EA0FE9BE440D8DFF311BF6C21F124E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t15176190D64802DDA12A1B7418276C28F4BBB197 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IMediaRecorder_CommitFrame_TisT_t4DD924B2910EC161D78FE990B75EBA3086B64B94_m9362A2981559D200173DD7BB1D60920FC636A143 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IMediaRecorder_CommitFrame_TisT_tB61707F7A3D83D3AF05D75A1F9D0EA438EF88511_m49DAEB92AA80702F7F6D51A1140D4BA7FB0047C6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IMediaRecorder_CommitFrame_TisT_tEE1FDAF0BEDCC9CD3B5470E4A0C4C2A9E9254392_mA03A47EA3127A81C77FB0930A2829FED8CB363F9 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1746,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	2,
	s_reversePInvokeIndices,
	20,
	s_rgctxIndices,
	38,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
