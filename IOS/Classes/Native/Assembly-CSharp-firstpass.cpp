﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InvokerActionInvoker1;
template <typename T1>
struct InvokerActionInvoker1<T1*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2>
struct InvokerActionInvoker2;
template <typename T1, typename T2>
struct InvokerActionInvoker2<T1*, T2*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Action`1<BluetoothLEHardwareInterface/iBeaconData>
struct Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE;
// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C;
// System.Action`2<System.String,System.Byte[]>
struct Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831;
// System.Action`2<System.String,System.String>
struct Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_tCDB60724FE7702C8848DCEE7A25283B015D0DA58;
// System.Action`3<System.String,System.String,System.Byte[]>
struct Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7;
// System.Action`4<System.Object,System.Object,System.Int32,System.Object>
struct Action_4_tB13889672F2D689F21857C968EBBDF091CA42E46;
// System.Action`4<System.String,System.String,System.Int32,System.Byte[]>
struct Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>
struct Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>
struct Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>
struct Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>
struct Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>
struct Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>
struct Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>
struct Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>
struct Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Action`1<System.String>>
struct KeyCollection_tDC9905DAB7B3D843994B8AF143B97B5C1D5B6936;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Action`2<System.String,System.Byte[]>>
struct KeyCollection_t6B68A129EC8149AACE58C5F34312148132BE3A5D;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Action`2<System.String,System.String>>
struct KeyCollection_t2B4949A3310A54180CF25ECDBB063042F07D3846;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Action`3<System.String,System.String,System.Byte[]>>
struct KeyCollection_tDEB7EBAE34ACB36F488B64E3ACCA223414080409;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>
struct KeyCollection_t62E580CDD4AD2C58AB764BA8E96B51A9A408EF14;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>
struct KeyCollection_tD5F7D890C8347A2C4B342C2D2B64AFC23157E272;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>
struct KeyCollection_t28070F1CBB6F27FB971DEA73EA1F1B5C99CCCBD7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>
struct KeyCollection_t55BF89DF4C34E4D0DD7186A21DF7FBADBAFF9B57;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<System.String>>
struct ValueCollection_t1BFE2D9246539713D76621C5D038C86BA2DA3F76;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`2<System.String,System.Byte[]>>
struct ValueCollection_t6FE7F1553C6213D89E25A379E6F130A43116E93B;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`2<System.String,System.String>>
struct ValueCollection_tE72EBB4992A3B7027945352F3E85E6E4EA3CCC1E;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`3<System.String,System.String,System.Byte[]>>
struct ValueCollection_t693954EDFB989578797C65FD6346D986760277F6;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>
struct ValueCollection_tF277A139BF9A5A5C6F4F4F5FD6A8000A8658FDCE;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>
struct ValueCollection_tBEC71E03B4233FB8DD3447320101C69756B06B6D;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>
struct ValueCollection_t8882E34082E7EEA63E8D352364FBC4C7FCD16BA2;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>
struct ValueCollection_t7E2768C272CC12D1213F42E60FEC61D271B5504B;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Action`1<System.String>>[]
struct EntryU5BU5D_t17B4B52BA0C980E519D97C6C5B12E911F4167F02;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Action`2<System.String,System.Byte[]>>[]
struct EntryU5BU5D_tD85663E0D3F56DB5B4D7B61F59CF3547EC4EFDD4;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Action`2<System.String,System.String>>[]
struct EntryU5BU5D_tDF3A60011A7699193B41342EB254CE91C0D52CA2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Action`3<System.String,System.String,System.Byte[]>>[]
struct EntryU5BU5D_t900C98EE15F3E9A35BE1B92E61E4F1F1B9271B19;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>[]
struct EntryU5BU5D_tE24E820FA547791800AC4A6BE2EA390A44BDDE80;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>[]
struct EntryU5BU5D_t922FDAC1576C36D69E46214258EE7745F1BA7C7E;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>[]
struct EntryU5BU5D_t17A4C74CE24E617D8325CEAD39942B17AD8BA004;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>[]
struct EntryU5BU5D_t6F24BDD2DFE83F9B6CC78F9B7B25A58AA334389E;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0;
// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D;
// AndroidPermissionCallback
struct AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2;
// AndroidPermissionsManager
struct AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8;
// AndroidPermissionsUsageExample
struct AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D;
// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// BluetoothDeviceScript
struct BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39;
// BluetoothLEHardwareInterface
struct BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.IO.DirectoryInfo
struct DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2;
// System.Exception
struct Exception_t;
// System.IO.FileNotFoundException
struct FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t20D8E5AAFC2EB2518FCABBF40465855E797FF0D8;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// UnityEngine.LocationService
struct LocationService_tF2F2720FE2C07562EBFD128889F9A99F4B41B1B2;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// NativeGalleryNamespace.NGMediaReceiveCallbackiOS
struct NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097;
// NativeGalleryNamespace.NGMediaSaveCallbackiOS
struct NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// AndroidPermissionsUsageExample/<>c
struct U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241;
// NativeGallery/MediaPickCallback
struct MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D;
// NativeGallery/MediaPickMultipleCallback
struct MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401;
// NativeGallery/MediaSaveCallback
struct MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityException_tA1EC1E95ADE689CF6EB7FAFF77C160AE1F559067_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral01FC300FC7D69084F12B27BCC0C38122C800B8C9;
IL2CPP_EXTERN_C String_t* _stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8;
IL2CPP_EXTERN_C String_t* _stringLiteral0365E1814018F9007D2AF7114C462B24D8928C59;
IL2CPP_EXTERN_C String_t* _stringLiteral04938D88CCBF5A98466865086ED2669F409064BE;
IL2CPP_EXTERN_C String_t* _stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F;
IL2CPP_EXTERN_C String_t* _stringLiteral0A79B0229EB4F27A4505CE75160E9133E944F111;
IL2CPP_EXTERN_C String_t* _stringLiteral0CA4721FC9D82D780671DE2AB61257837402697D;
IL2CPP_EXTERN_C String_t* _stringLiteral1256BD059A8D156C0578EF505C83E5862F0EFCD2;
IL2CPP_EXTERN_C String_t* _stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680;
IL2CPP_EXTERN_C String_t* _stringLiteral12F5EB18DE661BBE815BE7C12357AB74AFA912BC;
IL2CPP_EXTERN_C String_t* _stringLiteral133352F79A1BF69B9869BD9AD412DDFD203002D4;
IL2CPP_EXTERN_C String_t* _stringLiteral15332598528DB5F9A0B9473BE7DCE0BB1F8DCAA7;
IL2CPP_EXTERN_C String_t* _stringLiteral18B82B6B7DC4FE1988BA61A3784D1768F6C925DF;
IL2CPP_EXTERN_C String_t* _stringLiteral1DA1565418493517EA4A1928E378AD0548A223E7;
IL2CPP_EXTERN_C String_t* _stringLiteral22CA8A8DED4214E6F20217D70C471C5D80B2E4B6;
IL2CPP_EXTERN_C String_t* _stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A;
IL2CPP_EXTERN_C String_t* _stringLiteral283D774A5141A159CCA779600D4FD489AFD39105;
IL2CPP_EXTERN_C String_t* _stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415;
IL2CPP_EXTERN_C String_t* _stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32;
IL2CPP_EXTERN_C String_t* _stringLiteral30F7CAA3903ABC311FB9B0881B8937BE76A5526D;
IL2CPP_EXTERN_C String_t* _stringLiteral30F8B1D027E92AF30A25606539C4A0E635BF0BBB;
IL2CPP_EXTERN_C String_t* _stringLiteral3253A6F3765D0068274AF56D2189610ED308CCB0;
IL2CPP_EXTERN_C String_t* _stringLiteral36F1A6EA85D2491AD75EA710ED443F78D6D39328;
IL2CPP_EXTERN_C String_t* _stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53;
IL2CPP_EXTERN_C String_t* _stringLiteral3E96C9BB1B953A85290371E8CE7BB3F3ABB307CC;
IL2CPP_EXTERN_C String_t* _stringLiteral491B4D9271839F0BD63211437BF7CEE5B2C6ADE9;
IL2CPP_EXTERN_C String_t* _stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42;
IL2CPP_EXTERN_C String_t* _stringLiteral4D147215C82FF43E4366FAF1CD51F52CFE8DF7EF;
IL2CPP_EXTERN_C String_t* _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078;
IL2CPP_EXTERN_C String_t* _stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84;
IL2CPP_EXTERN_C String_t* _stringLiteral75E05143EB132AAA8A22B48813DB8E6047380821;
IL2CPP_EXTERN_C String_t* _stringLiteral78890CE9DBB1337BC5B353A9D2ECF645ABCDC860;
IL2CPP_EXTERN_C String_t* _stringLiteral7BB4B5568C63748C896DED09B403F2FC4F274E91;
IL2CPP_EXTERN_C String_t* _stringLiteral7D22511CC292B1C86526CD5212677E0053AC1C87;
IL2CPP_EXTERN_C String_t* _stringLiteral7E90946BC7628636C60D094F905E96CD1360A930;
IL2CPP_EXTERN_C String_t* _stringLiteral7F7876094B3FA10965A88A7D08B74EA3DC22CAE9;
IL2CPP_EXTERN_C String_t* _stringLiteral8280B83AC948E7BC53467CEFDF2AF27F4B2209E5;
IL2CPP_EXTERN_C String_t* _stringLiteral8BF951CF903ECC622812D47B1157D1A3AFA9FBDC;
IL2CPP_EXTERN_C String_t* _stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2;
IL2CPP_EXTERN_C String_t* _stringLiteral95B70EA0EBEA74FB776F022984B7AB5D2D304AF7;
IL2CPP_EXTERN_C String_t* _stringLiteral985B72B30ECE05DD4EF5FE142CEE0FB8BF53A98C;
IL2CPP_EXTERN_C String_t* _stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6;
IL2CPP_EXTERN_C String_t* _stringLiteral9EB291071F866E890E2ACF69DE89CBBA36BD00B8;
IL2CPP_EXTERN_C String_t* _stringLiteralA10411FCA4F6A342C7FF683134054CC32BFC6755;
IL2CPP_EXTERN_C String_t* _stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26;
IL2CPP_EXTERN_C String_t* _stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827;
IL2CPP_EXTERN_C String_t* _stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3;
IL2CPP_EXTERN_C String_t* _stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9;
IL2CPP_EXTERN_C String_t* _stringLiteralA79D3CC7BFC44D6628EA61E78D1855415661E3F8;
IL2CPP_EXTERN_C String_t* _stringLiteralAFF4AA19F30B5DC5A240F413D92917103536F1AD;
IL2CPP_EXTERN_C String_t* _stringLiteralB2113A207765BA2D8ABB7F50B4388B872AC1E2D2;
IL2CPP_EXTERN_C String_t* _stringLiteralB41F48CD412D791DA9AC5C2A4F59FAD4CEFEDE8B;
IL2CPP_EXTERN_C String_t* _stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51;
IL2CPP_EXTERN_C String_t* _stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28;
IL2CPP_EXTERN_C String_t* _stringLiteralC8A49BFAF7682C736E77C5743933C2176CF14A7B;
IL2CPP_EXTERN_C String_t* _stringLiteralCB4507437E3E619ECBAD84410155675EBEB3DB3F;
IL2CPP_EXTERN_C String_t* _stringLiteralCDC8B946EE0851853017EBA616344D4F97D44411;
IL2CPP_EXTERN_C String_t* _stringLiteralCE1C37FFA5FD702CB48BBF8546A5E8BECD45601C;
IL2CPP_EXTERN_C String_t* _stringLiteralCFA0E830C16EC57623615165EA8FBC2817BACCCF;
IL2CPP_EXTERN_C String_t* _stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49;
IL2CPP_EXTERN_C String_t* _stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0;
IL2CPP_EXTERN_C String_t* _stringLiteralD4E22C84D059FC69AA54802023A8B68B6F4F4C7E;
IL2CPP_EXTERN_C String_t* _stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDD638980A42773DBA4D111CE8D3979093BAC27E5;
IL2CPP_EXTERN_C String_t* _stringLiteralF47527D7B72D5957FA184B6B1B6A7A1A9A56BF37;
IL2CPP_EXTERN_C String_t* _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_mD7D192A35EB2B2DA3775FAB081958B72088251DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidPermissionsUsageExample_U3COnGrantButtonPressU3Eb__3_0_m6A17A2F63AF061CFF345D721B1226FF6CEE64B24_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m49883733B2BD00869A51867DF487463D793B72AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m4A499F6EAC71DC55F8B1A7DF3EA4AE5FCDFE83F1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m913953EB949920103EE45B0C497AED20472529CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m15E112E61AB3E2CDF5F8C4D5478C173E7CE98B4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m2C28FA742C2910206687FAC6656082BB86091AB5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m567D53C6E9424E1769E746B2F2F7CB666BD28746_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mA59DD81658DA8792C58F4AF9ABB667457AE7888F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mFACB7E9C70103BA8CD2CFFC7D56AB4B8B9FADD7F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m29D67E65079024F5C041D7F9AA960C3FB8C61727_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mBBC25AE20AA64D8E9CB489C3F455282573A79550_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mC10FD3398D9C91885BEE2ED01F03E6EA8F2458DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m317D3193C4CC26B216C7316AA83011B747927A26_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mD7531E55E7078F3CA3DB2492418BEADF7229D468_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDF56051C4B061E3956A03F35FC9F34BEB3280610_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisNGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_mF5EFD3397ADDE73CB43C1A935AB7D0D6A1C29082_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisNGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_m8EC160CD8F792A4F3207AD92BB7F66059B644F22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDBD85FE90312A38DA98B298A2B6052631AE960F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeGallery_GetImageProperties_m56B49E642441A04CDD2606987B5F998ED6D9FA31_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeGallery_GetVideoProperties_mF68A6E413B11767F89F0CBFD80808BF414E12D0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeGallery_LoadImageAtPath_m8D5D9C1FD10E95E49E9889DE5C87A0699FDD7F82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeGallery_SaveImageToGallery_m920DEF4DDB5909F787646AA94D21235B4AE85169_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3COnGrantButtonPressU3Eb__3_1_mB1A74CA187DCCF123410C5512FC4E0474A47063B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3COnGrantButtonPressU3Eb__3_2_m1620A2686F99CB67A19A4B869CCC65740C4B49A6_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tF062866229C4952B8051AD32AB6E9D931142CC95 
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>
struct Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t17B4B52BA0C980E519D97C6C5B12E911F4167F02* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tDC9905DAB7B3D843994B8AF143B97B5C1D5B6936* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t1BFE2D9246539713D76621C5D038C86BA2DA3F76* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>
struct Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_tD85663E0D3F56DB5B4D7B61F59CF3547EC4EFDD4* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t6B68A129EC8149AACE58C5F34312148132BE3A5D* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t6FE7F1553C6213D89E25A379E6F130A43116E93B* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>
struct Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_tDF3A60011A7699193B41342EB254CE91C0D52CA2* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t2B4949A3310A54180CF25ECDBB063042F07D3846* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tE72EBB4992A3B7027945352F3E85E6E4EA3CCC1E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>
struct Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t900C98EE15F3E9A35BE1B92E61E4F1F1B9271B19* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tDEB7EBAE34ACB36F488B64E3ACCA223414080409* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t693954EDFB989578797C65FD6346D986760277F6* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>
struct Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_tE24E820FA547791800AC4A6BE2EA390A44BDDE80* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t62E580CDD4AD2C58AB764BA8E96B51A9A408EF14* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tF277A139BF9A5A5C6F4F4F5FD6A8000A8658FDCE* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>
struct Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t922FDAC1576C36D69E46214258EE7745F1BA7C7E* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tD5F7D890C8347A2C4B342C2D2B64AFC23157E272* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tBEC71E03B4233FB8DD3447320101C69756B06B6D* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>
struct Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t17A4C74CE24E617D8325CEAD39942B17AD8BA004* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t28070F1CBB6F27FB971DEA73EA1F1B5C99CCCBD7* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t8882E34082E7EEA63E8D352364FBC4C7FCD16BA2* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>
struct Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t6F24BDD2DFE83F9B6CC78F9B7B25A58AA334389E* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t55BF89DF4C34E4D0DD7186A21DF7FBADBAFF9B57* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t7E2768C272CC12D1213F42E60FEC61D271B5504B* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.EmptyArray`1<System.Object>
struct EmptyArray_1_tDF0DD7256B115243AA6BD5558417387A734240EE  : public RuntimeObject
{
};

struct EmptyArray_1_tDF0DD7256B115243AA6BD5558417387A734240EE_StaticFields
{
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___Value_0;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___s_emptyArray_5;
};

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0  : public RuntimeObject
{
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jobject
	GlobalJavaObjectRef_t20D8E5AAFC2EB2518FCABBF40465855E797FF0D8* ___m_jobject_1;
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jclass
	GlobalJavaObjectRef_t20D8E5AAFC2EB2518FCABBF40465855E797FF0D8* ___m_jclass_2;
};

struct AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_StaticFields
{
	// System.Boolean UnityEngine.AndroidJavaObject::enableDebugPrints
	bool ___enableDebugPrints_0;
};

// AndroidPermissionsManager
struct AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8  : public RuntimeObject
{
};

struct AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields
{
	// UnityEngine.AndroidJavaObject AndroidPermissionsManager::m_Activity
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ___m_Activity_0;
	// UnityEngine.AndroidJavaObject AndroidPermissionsManager::m_PermissionService
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ___m_PermissionService_1;
};
struct Il2CppArrayBounds;

// BluetoothLEHardwareInterface
struct BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B  : public RuntimeObject
{
};

struct BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields
{
	// BluetoothDeviceScript BluetoothLEHardwareInterface::bluetoothDeviceScript
	BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* ___bluetoothDeviceScript_0;
};

// UnityEngine.LocationService
struct LocationService_tF2F2720FE2C07562EBFD128889F9A99F4B41B1B2  : public RuntimeObject
{
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// NativeGallery
struct NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E  : public RuntimeObject
{
};

struct NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields
{
	// System.String NativeGallery::m_temporaryImagePath
	String_t* ___m_temporaryImagePath_0;
	// System.String NativeGallery::m_selectedImagePath
	String_t* ___m_selectedImagePath_1;
	// System.String NativeGallery::m_selectedVideoPath
	String_t* ___m_selectedVideoPath_2;
	// System.String NativeGallery::m_selectedAudioPath
	String_t* ___m_selectedAudioPath_3;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// AndroidPermissionsUsageExample/<>c
struct U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241  : public RuntimeObject
{
};

struct U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields
{
	// AndroidPermissionsUsageExample/<>c AndroidPermissionsUsageExample/<>c::<>9
	U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* ___U3CU3E9_0;
	// System.Action`1<System.String> AndroidPermissionsUsageExample/<>c::<>9__3_1
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___U3CU3E9__3_1_1;
	// System.Action`1<System.String> AndroidPermissionsUsageExample/<>c::<>9__3_2
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___U3CU3E9__3_2_2;
};

// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03  : public AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// BluetoothLEHardwareInterface/iBeaconData
struct iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96 
{
	// System.String BluetoothLEHardwareInterface/iBeaconData::UUID
	String_t* ___UUID_0;
	// System.Int32 BluetoothLEHardwareInterface/iBeaconData::Major
	int32_t ___Major_1;
	// System.Int32 BluetoothLEHardwareInterface/iBeaconData::Minor
	int32_t ___Minor_2;
	// System.Int32 BluetoothLEHardwareInterface/iBeaconData::RSSI
	int32_t ___RSSI_3;
	// System.Int32 BluetoothLEHardwareInterface/iBeaconData::AndroidSignalPower
	int32_t ___AndroidSignalPower_4;
	// BluetoothLEHardwareInterface/iOSProximity BluetoothLEHardwareInterface/iBeaconData::iOSProximity
	int32_t ___iOSProximity_5;
};
// Native definition for P/Invoke marshalling of BluetoothLEHardwareInterface/iBeaconData
struct iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_pinvoke
{
	char* ___UUID_0;
	int32_t ___Major_1;
	int32_t ___Minor_2;
	int32_t ___RSSI_3;
	int32_t ___AndroidSignalPower_4;
	int32_t ___iOSProximity_5;
};
// Native definition for COM marshalling of BluetoothLEHardwareInterface/iBeaconData
struct iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_com
{
	Il2CppChar* ___UUID_0;
	int32_t ___Major_1;
	int32_t ___Minor_2;
	int32_t ___RSSI_3;
	int32_t ___AndroidSignalPower_4;
	int32_t ___iOSProximity_5;
};

// NativeGallery/ImageProperties
struct ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90 
{
	// System.Int32 NativeGallery/ImageProperties::width
	int32_t ___width_0;
	// System.Int32 NativeGallery/ImageProperties::height
	int32_t ___height_1;
	// System.String NativeGallery/ImageProperties::mimeType
	String_t* ___mimeType_2;
	// NativeGallery/ImageOrientation NativeGallery/ImageProperties::orientation
	int32_t ___orientation_3;
};
// Native definition for P/Invoke marshalling of NativeGallery/ImageProperties
struct ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_pinvoke
{
	int32_t ___width_0;
	int32_t ___height_1;
	char* ___mimeType_2;
	int32_t ___orientation_3;
};
// Native definition for COM marshalling of NativeGallery/ImageProperties
struct ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_com
{
	int32_t ___width_0;
	int32_t ___height_1;
	Il2CppChar* ___mimeType_2;
	int32_t ___orientation_3;
};

// NativeGallery/VideoProperties
struct VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426 
{
	// System.Int32 NativeGallery/VideoProperties::width
	int32_t ___width_0;
	// System.Int32 NativeGallery/VideoProperties::height
	int32_t ___height_1;
	// System.Int64 NativeGallery/VideoProperties::duration
	int64_t ___duration_2;
	// System.Single NativeGallery/VideoProperties::rotation
	float ___rotation_3;
};

// Interop/Sys/FileStatus
struct FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 
{
	// Interop/Sys/FileStatusFlags Interop/Sys/FileStatus::Flags
	int32_t ___Flags_0;
	// System.Int32 Interop/Sys/FileStatus::Mode
	int32_t ___Mode_1;
	// System.UInt32 Interop/Sys/FileStatus::Uid
	uint32_t ___Uid_2;
	// System.UInt32 Interop/Sys/FileStatus::Gid
	uint32_t ___Gid_3;
	// System.Int64 Interop/Sys/FileStatus::Size
	int64_t ___Size_4;
	// System.Int64 Interop/Sys/FileStatus::ATime
	int64_t ___ATime_5;
	// System.Int64 Interop/Sys/FileStatus::ATimeNsec
	int64_t ___ATimeNsec_6;
	// System.Int64 Interop/Sys/FileStatus::MTime
	int64_t ___MTime_7;
	// System.Int64 Interop/Sys/FileStatus::MTimeNsec
	int64_t ___MTimeNsec_8;
	// System.Int64 Interop/Sys/FileStatus::CTime
	int64_t ___CTime_9;
	// System.Int64 Interop/Sys/FileStatus::CTimeNsec
	int64_t ___CTimeNsec_10;
	// System.Int64 Interop/Sys/FileStatus::BirthTime
	int64_t ___BirthTime_11;
	// System.Int64 Interop/Sys/FileStatus::BirthTimeNsec
	int64_t ___BirthTimeNsec_12;
	// System.Int64 Interop/Sys/FileStatus::Dev
	int64_t ___Dev_13;
	// System.Int64 Interop/Sys/FileStatus::Ino
	int64_t ___Ino_14;
	// System.UInt32 Interop/Sys/FileStatus::UserFlags
	uint32_t ___UserFlags_15;
};

// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D  : public RuntimeObject
{
	// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaProxy::javaInterface
	AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* ___javaInterface_0;
	// System.IntPtr UnityEngine.AndroidJavaProxy::proxyObject
	intptr_t ___proxyObject_1;
};

struct AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D_StaticFields
{
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaProxy::s_JavaLangSystemClass
	GlobalJavaObjectRef_t20D8E5AAFC2EB2518FCABBF40465855E797FF0D8* ___s_JavaLangSystemClass_2;
	// System.IntPtr UnityEngine.AndroidJavaProxy::s_HashCodeMethodID
	intptr_t ___s_HashCodeMethodID_3;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// System.IO.FileStatus
struct FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5 
{
	// Interop/Sys/FileStatus System.IO.FileStatus::_fileStatus
	FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 ____fileStatus_0;
	// System.Int32 System.IO.FileStatus::_fileStatusInitialized
	int32_t ____fileStatusInitialized_1;
	// System.Boolean System.IO.FileStatus::<InitiallyDirectory>k__BackingField
	bool ___U3CInitiallyDirectoryU3Ek__BackingField_2;
	// System.Boolean System.IO.FileStatus::_isDirectory
	bool ____isDirectory_3;
	// System.Boolean System.IO.FileStatus::_exists
	bool ____exists_4;
};
// Native definition for P/Invoke marshalling of System.IO.FileStatus
struct FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5_marshaled_pinvoke
{
	FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 ____fileStatus_0;
	int32_t ____fileStatusInitialized_1;
	int32_t ___U3CInitiallyDirectoryU3Ek__BackingField_2;
	int32_t ____isDirectory_3;
	int32_t ____exists_4;
};
// Native definition for COM marshalling of System.IO.FileStatus
struct FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5_marshaled_com
{
	FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 ____fileStatus_0;
	int32_t ____fileStatusInitialized_1;
	int32_t ___U3CInitiallyDirectoryU3Ek__BackingField_2;
	int32_t ____isDirectory_3;
	int32_t ____exists_4;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// AndroidPermissionCallback
struct AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2  : public AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D
{
	// System.Action`1<System.String> AndroidPermissionCallback::OnPermissionGrantedAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___OnPermissionGrantedAction_4;
	// System.Action`1<System.String> AndroidPermissionCallback::OnPermissionDeniedAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___OnPermissionDeniedAction_5;
	// System.Action`1<System.String> AndroidPermissionCallback::OnPermissionDeniedAndDontAskAgainAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___OnPermissionDeniedAndDontAskAgainAction_6;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.IO.FileSystemInfo
struct FileSystemInfo_tE3063B9229F46B05A5F6D018C8C4CA510104E8E9  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.FileStatus System.IO.FileSystemInfo::_fileStatus
	FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5 ____fileStatus_1;
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_2;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_3;
	// System.String System.IO.FileSystemInfo::_name
	String_t* ____name_4;
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// UnityEngine.UnityException
struct UnityException_tA1EC1E95ADE689CF6EB7FAFF77C160AE1F559067  : public Exception_t
{
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A  : public MulticastDelegate_t
{
};

// System.Action`1<BluetoothLEHardwareInterface/iBeaconData>
struct Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE  : public MulticastDelegate_t
{
};

// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C  : public MulticastDelegate_t
{
};

// System.Action`2<System.String,System.Byte[]>
struct Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831  : public MulticastDelegate_t
{
};

// System.Action`2<System.String,System.String>
struct Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D  : public MulticastDelegate_t
{
};

// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_tCDB60724FE7702C8848DCEE7A25283B015D0DA58  : public MulticastDelegate_t
{
};

// System.Action`3<System.String,System.String,System.Byte[]>
struct Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB  : public MulticastDelegate_t
{
};

// System.Action`3<System.String,System.String,System.String>
struct Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7  : public MulticastDelegate_t
{
};

// System.Action`4<System.Object,System.Object,System.Int32,System.Object>
struct Action_4_tB13889672F2D689F21857C968EBBDF091CA42E46  : public MulticastDelegate_t
{
};

// System.Action`4<System.String,System.String,System.Int32,System.Byte[]>
struct Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D  : public MulticastDelegate_t
{
};

// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07  : public MulticastDelegate_t
{
};

// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	// System.String System.ArgumentException::_paramName
	String_t* ____paramName_18;
};

// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.IO.DirectoryInfo
struct DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2  : public FileSystemInfo_tE3063B9229F46B05A5F6D018C8C4CA510104E8E9
{
};

// System.IO.IOException
struct IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// NativeGallery/MediaPickCallback
struct MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D  : public MulticastDelegate_t
{
};

// NativeGallery/MediaPickMultipleCallback
struct MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401  : public MulticastDelegate_t
{
};

// NativeGallery/MediaSaveCallback
struct MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD  : public MulticastDelegate_t
{
};

// System.IO.FileNotFoundException
struct FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A  : public IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910
{
	// System.String System.IO.FileNotFoundException::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_18;
	// System.String System.IO.FileNotFoundException::<FusionLog>k__BackingField
	String_t* ___U3CFusionLogU3Ek__BackingField_19;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// AndroidPermissionsUsageExample
struct AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// BluetoothDeviceScript
struct BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<System.String> BluetoothDeviceScript::DiscoveredDeviceList
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___DiscoveredDeviceList_4;
	// System.Action BluetoothDeviceScript::InitializedAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___InitializedAction_5;
	// System.Action BluetoothDeviceScript::DeinitializedAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___DeinitializedAction_6;
	// System.Action`1<System.String> BluetoothDeviceScript::ErrorAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___ErrorAction_7;
	// System.Action`1<System.String> BluetoothDeviceScript::ServiceAddedAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___ServiceAddedAction_8;
	// System.Action BluetoothDeviceScript::StartedAdvertisingAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___StartedAdvertisingAction_9;
	// System.Action BluetoothDeviceScript::StoppedAdvertisingAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___StoppedAdvertisingAction_10;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::DiscoveredPeripheralAction
	Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___DiscoveredPeripheralAction_11;
	// System.Action`4<System.String,System.String,System.Int32,System.Byte[]> BluetoothDeviceScript::DiscoveredPeripheralWithAdvertisingInfoAction
	Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* ___DiscoveredPeripheralWithAdvertisingInfoAction_12;
	// System.Action`1<BluetoothLEHardwareInterface/iBeaconData> BluetoothDeviceScript::DiscoveredBeaconAction
	Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* ___DiscoveredBeaconAction_13;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::RetrievedConnectedPeripheralAction
	Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___RetrievedConnectedPeripheralAction_14;
	// System.Action`2<System.String,System.Byte[]> BluetoothDeviceScript::PeripheralReceivedWriteDataAction
	Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* ___PeripheralReceivedWriteDataAction_15;
	// System.Action`1<System.String> BluetoothDeviceScript::ConnectedPeripheralAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___ConnectedPeripheralAction_16;
	// System.Action`1<System.String> BluetoothDeviceScript::ConnectedDisconnectPeripheralAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___ConnectedDisconnectPeripheralAction_17;
	// System.Action`1<System.String> BluetoothDeviceScript::DisconnectedPeripheralAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___DisconnectedPeripheralAction_18;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::DiscoveredServiceAction
	Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___DiscoveredServiceAction_19;
	// System.Action`3<System.String,System.String,System.String> BluetoothDeviceScript::DiscoveredCharacteristicAction
	Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7* ___DiscoveredCharacteristicAction_20;
	// System.Action`1<System.String> BluetoothDeviceScript::DidWriteCharacteristicAction
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___DidWriteCharacteristicAction_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>> BluetoothDeviceScript::DidUpdateNotificationStateForCharacteristicAction
	Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* ___DidUpdateNotificationStateForCharacteristicAction_22;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>> BluetoothDeviceScript::DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction
	Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* ___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>> BluetoothDeviceScript::DidUpdateCharacteristicValueAction
	Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* ___DidUpdateCharacteristicValueAction_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>> BluetoothDeviceScript::DidUpdateCharacteristicValueWithDeviceAddressAction
	Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* ___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
};

// NativeGalleryNamespace.NGMediaReceiveCallbackiOS
struct NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// NativeGallery/MediaPickCallback NativeGalleryNamespace.NGMediaReceiveCallbackiOS::callback
	MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback_5;
	// System.Single NativeGalleryNamespace.NGMediaReceiveCallbackiOS::nextBusyCheckTime
	float ___nextBusyCheckTime_6;
};

struct NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields
{
	// NativeGalleryNamespace.NGMediaReceiveCallbackiOS NativeGalleryNamespace.NGMediaReceiveCallbackiOS::instance
	NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* ___instance_4;
	// System.Boolean NativeGalleryNamespace.NGMediaReceiveCallbackiOS::<IsBusy>k__BackingField
	bool ___U3CIsBusyU3Ek__BackingField_7;
};

// NativeGalleryNamespace.NGMediaSaveCallbackiOS
struct NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// NativeGallery/MediaSaveCallback NativeGalleryNamespace.NGMediaSaveCallbackiOS::callback
	MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback_5;
};

struct NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields
{
	// NativeGalleryNamespace.NGMediaSaveCallbackiOS NativeGalleryNamespace.NGMediaSaveCallbackiOS::instance
	NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* ___instance_4;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771  : public RuntimeArray
{
	ALIGN_FIELD (8) Delegate_t* m_Items[1];

	inline Delegate_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, const RuntimeMethod* method) ;
// System.Void System.Action`4<System.Object,System.Object,System.Int32,System.Object>::Invoke(T1,T2,T3,T4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_4_Invoke_mD9FB5E2CF8A8ED11C3135A1A910CDCAD6538A1FC_gshared_inline (Action_4_tB13889672F2D689F21857C968EBBDF091CA42E46* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, int32_t ___arg32, RuntimeObject* ___arg43, const RuntimeMethod* method) ;
// System.Void System.Action`1<BluetoothLEHardwareInterface/iBeaconData>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mE56360F8A5B137A1714DEF08A9266EF9166480B8_gshared_inline (Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* __this, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96 ___obj0, const RuntimeMethod* method) ;
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_3_Invoke_m399A0EB5E51EFD9B7D25DFE0EB7BF5EC0BE98346_gshared_inline (Action_3_tCDB60724FE7702C8848DCEE7A25283B015D0DA58* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, RuntimeObject* ___arg32, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, RuntimeObject* ___value1, const RuntimeMethod* method) ;
// FieldType UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AndroidJavaObject_GetStatic_TisRuntimeObject_m4EF4E4761A0A6E99E0A298F653E8129B1494E4C9_gshared (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* __this, String_t* ___fieldName0, const RuntimeMethod* method) ;
// T[] System.Array::Empty<System.Object>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_gshared_inline (const RuntimeMethod* method) ;
// ReturnType UnityEngine.AndroidJavaObject::Call<System.Boolean>(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF_gshared (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* __this, String_t* ___methodName0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>::.ctor()
inline void Dictionary_2__ctor_mFACB7E9C70103BA8CD2CFFC7D56AB4B8B9FADD7F (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>::.ctor()
inline void Dictionary_2__ctor_m15E112E61AB3E2CDF5F8C4D5478C173E7CE98B4C (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>::.ctor()
inline void Dictionary_2__ctor_m2C28FA742C2910206687FAC6656082BB86091AB5 (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>::.ctor()
inline void Dictionary_2__ctor_mA59DD81658DA8792C58F4AF9ABB667457AE7888F (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* String_Split_m101D35FEC86371D2BB4E3480F6F896880093B2E9 (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___separator0, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806 (String_t* ___format0, RuntimeObject* ___arg01, RuntimeObject* ___arg12, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::Log(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method) ;
// System.Void System.Action::Invoke()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::FinishDeInitialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_FinishDeInitialize_m4E276D6FFA746F913E33B5274891788DD55A017E (const RuntimeMethod* method) ;
// System.Void System.Action`1<System.String>::Invoke(T)
inline void Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* __this, String_t* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, String_t*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___obj0, method);
}
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
inline bool List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1 (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, String_t*, const RuntimeMethod*))List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
inline void List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, String_t*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Action`2<System.String,System.String>::Invoke(T1,T2)
inline void Action_2_Invoke_m8FA30194997244EC0072D6B437818A22B65F2854_inline (Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* __this, String_t* ___arg10, String_t* ___arg21, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D*, String_t*, String_t*, const RuntimeMethod*))Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline)(__this, ___arg10, ___arg21, method);
}
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4 (String_t* ___s0, int32_t* ___result1, const RuntimeMethod* method) ;
// System.Byte[] System.Convert::FromBase64String(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* Convert_FromBase64String_m421F8600CA5124E047E3D7C2BC1B653F67BC48A1 (String_t* ___s0, const RuntimeMethod* method) ;
// System.Void System.Action`4<System.String,System.String,System.Int32,System.Byte[]>::Invoke(T1,T2,T3,T4)
inline void Action_4_Invoke_mD9DD80B61F675BE34D06BB382884E69323B1C8C1_inline (Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* __this, String_t* ___arg10, String_t* ___arg21, int32_t ___arg32, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___arg43, const RuntimeMethod* method)
{
	((  void (*) (Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D*, String_t*, String_t*, int32_t, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, const RuntimeMethod*))Action_4_Invoke_mD9FB5E2CF8A8ED11C3135A1A910CDCAD6538A1FC_gshared_inline)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method);
}
// System.Void System.Action`1<BluetoothLEHardwareInterface/iBeaconData>::Invoke(T)
inline void Action_1_Invoke_mE56360F8A5B137A1714DEF08A9266EF9166480B8_inline (Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* __this, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96 ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE*, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96, const RuntimeMethod*))Action_1_Invoke_mE56360F8A5B137A1714DEF08A9266EF9166480B8_gshared_inline)(__this, ___obj0, method);
}
// System.Void BluetoothDeviceScript::OnPeripheralData(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_OnPeripheralData_mD3F92D2EA496B24CFB212E6D72E2C6BB0891CE96 (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, String_t* ___characteristic0, String_t* ___base64Data1, const RuntimeMethod* method) ;
// System.Void System.Action`3<System.String,System.String,System.String>::Invoke(T1,T2,T3)
inline void Action_3_Invoke_mABFB1B378BF33588992DD5A0180457B4ADB790D9_inline (Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7* __this, String_t* ___arg10, String_t* ___arg21, String_t* ___arg32, const RuntimeMethod* method)
{
	((  void (*) (Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7*, String_t*, String_t*, String_t*, const RuntimeMethod*))Action_3_Invoke_m399A0EB5E51EFD9B7D25DFE0EB7BF5EC0BE98346_gshared_inline)(__this, ___arg10, ___arg21, ___arg32, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>::get_Item(TKey)
inline Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61 (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* (*) (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::get_Item(TKey)
inline Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* (*) (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23 (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>::get_Item(TKey)
inline Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* (*) (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m913953EB949920103EE45B0C497AED20472529CD (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>::get_Item(TKey)
inline Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* Dictionary_2_get_Item_mBBC25AE20AA64D8E9CB489C3F455282573A79550 (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* (*) (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_OnBluetoothData_mC3C7BE5FBFA895495E5D3F2F2147C73378437C2D (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, String_t* ___deviceAddress0, String_t* ___characteristic1, String_t* ___base64Data2, const RuntimeMethod* method) ;
// System.String System.String::ToUpper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49 (String_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30 (String_t* ___format0, RuntimeObject* ___arg01, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>::get_Item(TKey)
inline Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0 (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* (*) (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m4A499F6EAC71DC55F8B1A7DF3EA4AE5FCDFE83F1 (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>::get_Item(TKey)
inline Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* Dictionary_2_get_Item_mC10FD3398D9C91885BEE2ED01F03E6EA8F2458DF (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* (*) (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Void System.Action`2<System.String,System.Byte[]>::Invoke(T1,T2)
inline void Action_2_Invoke_m30A65A90206175F850BCF9448E5227069C5FC9CF_inline (Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* __this, String_t* ___arg10, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___arg21, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831*, String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, const RuntimeMethod*))Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline)(__this, ___arg10, ___arg21, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0 (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>::get_Item(TKey)
inline Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2 (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* (*) (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m49883733B2BD00869A51867DF487463D793B72AE (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___key0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>::get_Item(TKey)
inline Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* Dictionary_2_get_Item_m29D67E65079024F5C041D7F9AA960C3FB8C61727 (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* (*) (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Void System.Action`3<System.String,System.String,System.Byte[]>::Invoke(T1,T2,T3)
inline void Action_3_Invoke_m621E4DDF08B7AB38B686646F5E5821CBB4B85A14_inline (Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* __this, String_t* ___arg10, String_t* ___arg21, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___arg32, const RuntimeMethod* method)
{
	((  void (*) (Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB*, String_t*, String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, const RuntimeMethod*))Action_3_Invoke_m399A0EB5E51EFD9B7D25DFE0EB7BF5EC0BE98346_gshared_inline)(__this, ___arg10, ___arg21, ___arg32, method);
}
// UnityEngine.LocationService UnityEngine.Input::get_location()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LocationService_tF2F2720FE2C07562EBFD128889F9A99F4B41B1B2* Input_get_location_m6F753D9369213F07EC556FF6240D723DCF3C689D (const RuntimeMethod* method) ;
// System.Void UnityEngine.LocationService::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocationService_Stop_mB9332CB653E7A7CE6AE07240EA6C0B6C9AEC0D96 (LocationService_tF2F2720FE2C07562EBFD128889F9A99F4B41B1B2* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Application::get_isEditor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234 (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLELog(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLELog_m55C7594137F4E0415929262CD5626CFC644C5BF0 (String_t* ___message0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51 (String_t* ___name0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_m37D512B05D292F954792225E6C6EEE95293A9B88 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, String_t* ___name0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<BluetoothDeviceScript>()
inline BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* GameObject_GetComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDBD85FE90312A38DA98B298A2B6052631AE960F6 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// T UnityEngine.GameObject::AddComponent<BluetoothDeviceScript>()
inline BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* GameObject_AddComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDF56051C4B061E3956A03F35FC9F34BEB3280610 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared)(__this, method);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m303AA1C4DC810349F285B4809E426CBBA8F834F9 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___target0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component_SendMessage_mA1D80D8BB7836EADB7799CAE71F10710298F4CDB (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, String_t* ___methodName0, RuntimeObject* ___value1, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEInitialize(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEInitialize_m0C6993C007FEB26D51E76381D1E0D3D36543E758 (bool ___asCentral0, bool ___asPeripheral1, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDeInitialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEDeInitialize_mC6C9DF17BC7C3565A9D0A69FDBCAE93DB2560A0F (const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPauseMessages(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEPauseMessages_mB6C3F788494EB2DF55240EBB50FE0F45BA1DD5E6 (bool ___isPaused0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEScanForBeacons(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEScanForBeacons_m2922203625442A6519864551FFA0FEF77A2A9528 (String_t* ___proximityUUIDsString0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
inline void List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEScanForPeripheralsWithServices(System.String,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEScanForPeripheralsWithServices_m9DD8A9BB753B1FD0737421D28522492934767A48 (String_t* ___serviceUUIDsString0, bool ___allowDuplicates1, bool ___rssiOnly2, bool ___clearPeripheralList3, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERetrieveListOfPeripheralsWithServices(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERetrieveListOfPeripheralsWithServices_m1DD242C933E0288D7C769F4BB18A6F95942D5F3D (String_t* ___serviceUUIDsString0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopScan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStopScan_m0F53F04F9967FFF7E6ABDF404FFFB5E08B00C89E (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopBeaconScan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStopBeaconScan_mF48DC11DEF9AFABA6096F66487E3656AC4847775 (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDisconnectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectAll_m22125A7EDCFDFA19C968485496521BF28E6549B7 (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEConnectToPeripheral(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEConnectToPeripheral_m8555AF34E8007C9C461C26E96FAC539B9BA1DBE8 (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDisconnectPeripheral(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectPeripheral_mF6C5C73D59330F45513B8E59CE56BEF79FAB0E0F (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>::.ctor()
inline void Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476 (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* __this, String_t* ___key0, Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78*, String_t*, Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.Byte[]>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2 (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* __this, String_t* ___key0, Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*, String_t*, Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEReadCharacteristic(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEReadCharacteristic_mEAD0320BAA7DBB9CCAC2BE8BC0016AE071CEE7AE (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEWriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEWriteCharacteristic_mCF6B7042D85D3FB2D4D6F60E21A51C15949A50DC (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data3, int32_t ___length4, bool ___withResponse5, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::.ctor()
inline void Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2 (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706 (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* __this, String_t* ___key0, Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D*, String_t*, Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189 (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___key0, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*, String_t*, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLESubscribeCharacteristic(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m271256C090D07D968CB4EB1BD3DBAFDFF5940DB4 (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>::.ctor()
inline void Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11 (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7 (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* __this, String_t* ___key0, Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74*, String_t*, Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,System.String>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87 (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* __this, String_t* ___key0, Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*, String_t*, Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>::.ctor()
inline void Dictionary_2__ctor_m567D53C6E9424E1769E746B2F2F7CB666BD28746 (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_mD7531E55E7078F3CA3DB2492418BEADF7229D468 (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* __this, String_t* ___key0, Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883*, String_t*, Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`3<System.String,System.String,System.Byte[]>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m317D3193C4CC26B216C7316AA83011B747927A26 (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* __this, String_t* ___key0, Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB*, String_t*, Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUnSubscribeCharacteristic(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEUnSubscribeCharacteristic_m0680535F4C5EFB91FA7EC74B3DE95A79131BAE2B (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPeripheralName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEPeripheralName_mDAC39AB7B05C695755E524F11FB8ABF413AC073A (String_t* ___newName0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateService(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLECreateService_m128948471801B37A8AF1327C7DE7DD4049C8FAD3 (String_t* ___uuid0, bool ___primary1, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveService(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveService_mC1879AF8FA6266EBFADD279369F31DD19DD90682 (String_t* ___uuid0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveServices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveServices_mBEB8BC1047FFB2F99B2B69C53D073A69142105CE (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateCharacteristic(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLECreateCharacteristic_m9403BFAC8E9864142CF2CE5DE7F9916C16732457 (String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data3, int32_t ___length4, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristic(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristic_m3AC1CA3579C1BB514EFD41292D6B811F0A0339A6 (String_t* ___uuid0, const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristics_m9C1A7AA96B7FC791B0B036340C8261F8A311E47E (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStartAdvertising()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStartAdvertising_mE27FFB143EA6F48390E94F6F40CF828340164628 (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopAdvertising()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStopAdvertising_mE457E3D9C37A42A70D12591601A7FE8FC4EA9488 (const RuntimeMethod* method) ;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEUpdateCharacteristicValue_m19DBF301078337BBDCA08119260575BD24EE1767 (String_t* ___uuid0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data1, int32_t ___length2, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Application::get_temporaryCachePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60 (const RuntimeMethod* method) ;
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474 (String_t* ___path10, String_t* ___path21, const RuntimeMethod* method) ;
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* Directory_CreateDirectory_mD89FECDFB25BC52F866DC0B1BB8552334FB249D2 (String_t* ___path0, const RuntimeMethod* method) ;
// System.Int32 NativeGallery::_NativeGallery_CheckPermission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery__NativeGallery_CheckPermission_m45AEE9475814FD651808364FE2166A058FE2E134 (const RuntimeMethod* method) ;
// System.Int32 NativeGallery::_NativeGallery_RequestPermission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery__NativeGallery_RequestPermission_mD85483634D04DBAF88D0411A0C0BBF31594D37DB (const RuntimeMethod* method) ;
// System.Int32 NativeGallery::_NativeGallery_CanOpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery__NativeGallery_CanOpenSettings_mD681FE9951AFDE42A5E26A105605551733D5BD7A (const RuntimeMethod* method) ;
// System.Void NativeGallery::_NativeGallery_OpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_OpenSettings_mB48D04687A8B7F4A39BD3E6B1DA39612F756216A (const RuntimeMethod* method) ;
// NativeGallery/Permission NativeGallery::SaveToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___mediaBytes0, String_t* ___album1, String_t* ___filename2, int32_t ___mediaType3, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback4, const RuntimeMethod* method) ;
// NativeGallery/Permission NativeGallery::SaveToGallery(System.String,System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A (String_t* ___existingMediaPath0, String_t* ___album1, String_t* ___filename2, int32_t ___mediaType3, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback4, const RuntimeMethod* method) ;
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465 (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Boolean System.String::EndsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_EndsWith_mCD3754F5401E19CE7821CD398986E4EAA6AD87DC (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Byte[] NativeGallery::GetTextureBytes(UnityEngine.Texture2D,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NativeGallery_GetTextureBytes_mBDA97D1727DD64A8A295A4D8F556F830DD6DAA2D (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___texture0, bool ___isJpeg1, const RuntimeMethod* method) ;
// NativeGallery/Permission NativeGallery::GetMediaFromGallery(NativeGallery/MediaPickCallback,NativeGallery/MediaType,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetMediaFromGallery_m890AD2506069812EF1F08AADC376799DF3513B23 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, int32_t ___mediaType1, String_t* ___mime2, String_t* ___title3, const RuntimeMethod* method) ;
// NativeGallery/Permission NativeGallery::GetMultipleMediaFromGallery(NativeGallery/MediaPickMultipleCallback,NativeGallery/MediaType,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetMultipleMediaFromGallery_m423CEDD28542066C638803009A9917FECD5B61ED (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* ___callback0, int32_t ___mediaType1, String_t* ___mime2, String_t* ___title3, const RuntimeMethod* method) ;
// System.Boolean NativeGalleryNamespace.NGMediaReceiveCallbackiOS::get_IsBusy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool NGMediaReceiveCallbackiOS_get_IsBusy_mAECD29ABE59BCEA398CBE4E9920C1F4A0FADE428_inline (const RuntimeMethod* method) ;
// NativeGallery/Permission NativeGallery::RequestPermission(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_RequestPermission_mBD7A8FDD105249998911185157C71411E7DD2DE5 (bool ___readPermissionOnly0, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetExtension(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7 (String_t* ___path0, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A (String_t* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.String NativeGallery::GetTemporarySavePath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_GetTemporarySavePath_m29E58001A5CB64D1A1AF5A494E318D40604490B1 (String_t* ___filename0, const RuntimeMethod* method) ;
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllBytes_m72C1A24339B30F84A655E6BF516AE1638B2C4668 (String_t* ___path0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___bytes1, const RuntimeMethod* method) ;
// System.Void NativeGallery::SaveToGalleryInternal(System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery_SaveToGalleryInternal_m3B9ED4FA03CC0D8CF9049EA93678766BC81C0BC1 (String_t* ___path0, String_t* ___album1, int32_t ___mediaType2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) ;
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85 (String_t* ___path0, const RuntimeMethod* method) ;
// System.Void System.IO.FileNotFoundException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1 (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void System.IO.File::Copy(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_Copy_m68957393F932BED668C5725704E5DA9B71925395 (String_t* ___sourceFileName0, String_t* ___destFileName1, bool ___overwrite2, const RuntimeMethod* method) ;
// System.Void NativeGallery/MediaSaveCallback::Invoke(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_inline (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetFileName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetFileName_mEBC73E0C8D8C56214D1DA4BA8409C5B5F00457A5 (String_t* ___path0, const RuntimeMethod* method) ;
// System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::Initialize(NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaSaveCallbackiOS_Initialize_mE5AC16A419E72FF81137DF8977D5A909D6E79A10 (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback0, const RuntimeMethod* method) ;
// System.Void NativeGallery::_NativeGallery_ImageWriteToAlbum(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_ImageWriteToAlbum_m3809743794B56DAC2DBF80E70078353EFC80C2B6 (String_t* ___path0, String_t* ___album1, const RuntimeMethod* method) ;
// System.Void NativeGallery::_NativeGallery_VideoWriteToAlbum(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_VideoWriteToAlbum_m84E34BCA6153E0E45A90C0F2784E08355AE64971 (String_t* ___path0, String_t* ___album1, const RuntimeMethod* method) ;
// System.String UnityEngine.Application::get_persistentDataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_persistentDataPath_m787EBC9B0862E7617DCD6CABD2147E61717EAC17 (const RuntimeMethod* method) ;
// System.String System.IO.Path::GetFileNameWithoutExtension(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetFileNameWithoutExtension_mD24A7CA7C45FF7A265EF7F8D5E19D1F3C014D90F (String_t* ___path0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mA299F8BBD1426934E2DAA95859E11C2063A116C8 (RuntimeObject* ___arg00, RuntimeObject* ___arg11, RuntimeObject* ___arg22, const RuntimeMethod* method) ;
// System.Boolean NativeGallery::IsMediaPickerBusy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeGallery_IsMediaPickerBusy_m05EF325B7999ECF2388E56485D3108B6869F43B9 (const RuntimeMethod* method) ;
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Initialize(NativeGallery/MediaPickCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_Initialize_m5825805938A7EB8FC2A1697CD11C2E98F4F95D6D (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, const RuntimeMethod* method) ;
// System.String NativeGallery::get_SelectedImagePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_SelectedImagePath_m79998DCAF1EAD91AB43E1229D733F1C9B21AA6D6 (const RuntimeMethod* method) ;
// System.Void NativeGallery::_NativeGallery_PickImage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_PickImage_m3136AAB028A66318A09844416015C93EBD0B7256 (String_t* ___imageSavePath0, const RuntimeMethod* method) ;
// System.String NativeGallery::get_SelectedVideoPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_SelectedVideoPath_mF6C9F14DC409D86A8E77AEA127F27D090B8E0FF6 (const RuntimeMethod* method) ;
// System.Void NativeGallery::_NativeGallery_PickVideo(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_PickVideo_m4B4F154AEF7503135D17B3D7B3F0C96B90741017 (String_t* ___videoSavePath0, const RuntimeMethod* method) ;
// System.Void NativeGallery/MediaPickCallback::Invoke(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_inline (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method) ;
// System.Boolean NativeGallery::CanSelectMultipleFilesFromGallery()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeGallery_CanSelectMultipleFilesFromGallery_mC1041FEED2AF374AC572B796BAFAFD9B2A0BC8C7 (const RuntimeMethod* method) ;
// System.Void NativeGallery/MediaPickMultipleCallback::Invoke(System.String[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_inline (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method) ;
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___tex0, const RuntimeMethod* method) ;
// System.Byte[] UnityEngine.ImageConversion::EncodeToJPG(UnityEngine.Texture2D,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ImageConversion_EncodeToJPG_mABBC4FA7AF9A69EB41FDE1CFE73A3F8656546385 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___tex0, int32_t ___quality1, const RuntimeMethod* method) ;
// System.Byte[] NativeGallery::GetTextureBytesFromCopy(UnityEngine.Texture2D,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NativeGallery_GetTextureBytesFromCopy_m5AF093F441E4DF9CDA5F17E00496C69C6F497A7C (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___texture0, bool ___isJpeg1, const RuntimeMethod* method) ;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* RenderTexture_GetTemporary_mCD6ECAD5EDABF63A1F2F496ABC4E2502F0883A6E (int32_t ___width0, int32_t ___height1, const RuntimeMethod* method) ;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* RenderTexture_get_active_m2204DF860773F9A8CDFF92BDB35CEB72A0643345 (const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_set_active_m045EA53D62FBF79693265E33D14D8E0E8151A37E (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___temp0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_m066854D684B6042B266D306E8E012D2C6C1787BE (Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___dest1, const RuntimeMethod* method) ;
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Texture2D_get_format_mE39DD922F83CA1097383309278BB6F20636A7D9D (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m7483DB211233F02E46418E9A6077487925F0024C (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___source0, int32_t ___destX1, int32_t ___destY2, bool ___recalculateMipMaps3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m36EE27E6F1BF7FB8C70A1D749DC4EE249810AA3A (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogException_m82E44FEC6B03BC34AFC2CAF6583051570C60CB9E (Exception_t* ___exception0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_m8249CABCDF344BE3A67EE765122EBB415DC2BC57 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SystemInfo_get_maxTextureSize_mB4272D4D85179CEF11FF1CDB3E8F3786D10CA04E (const RuntimeMethod* method) ;
// System.String NativeGallery::get_TemporaryImagePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_TemporaryImagePath_m32BA1844727D8D2D8F1AFC4FABC7D027CA42641C (const RuntimeMethod* method) ;
// System.String NativeGallery::_NativeGallery_LoadImageAtPath(System.String,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery__NativeGallery_LoadImageAtPath_m557D7EBC832C927A8DB4C4F3FE02697C8F99E5DF (String_t* ___path0, String_t* ___temporaryFilePath1, int32_t ___maxSize2, const RuntimeMethod* method) ;
// System.String System.String::ToLowerInvariant()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToLowerInvariant_mBE32C93DE27C5353FEA3FA654FC1DDBE3D0EB0F2 (String_t* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mC3F84195D1DCEFC0536B3FBD40A8F8E865A4F32A (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, bool ___linear4, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0FBE5AC4931D312E5B347BAA603755676E6DA2FE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method) ;
// System.Void System.IO.File::Delete(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_Delete_mB5CB249F5370D49747243BCA9C56CCC48D3E6A08 (String_t* ___path0, const RuntimeMethod* method) ;
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* File_ReadAllBytes_mE0C2C5A0270CD40C496E85B1D8BA15C607E7011E (String_t* ___path0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[],System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_mE2D612F3895FDD7A87805E1C9D77A79C019213E2 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___tex0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data1, bool ___markNonReadable2, const RuntimeMethod* method) ;
// System.String NativeGallery::_NativeGallery_GetImageProperties(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery__NativeGallery_GetImageProperties_m4130139DD3C6DF9E3750094D85F2A88317E7DFC3 (String_t* ___path0, const RuntimeMethod* method) ;
// System.String[] System.String::Split(System.Char,System.StringSplitOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* String_Split_m9530B73D02054692283BF35C3A27C8F2230946F4 (String_t* __this, Il2CppChar ___separator0, int32_t ___options1, const RuntimeMethod* method) ;
// System.String System.String::Trim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5 (String_t* __this, const RuntimeMethod* method) ;
// System.Void NativeGallery/ImageProperties::.ctor(System.Int32,System.Int32,System.String,NativeGallery/ImageOrientation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageProperties__ctor_m82A240218A9A84787616EE7D627864523555CA68 (ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90* __this, int32_t ___width0, int32_t ___height1, String_t* ___mimeType2, int32_t ___orientation3, const RuntimeMethod* method) ;
// System.String NativeGallery::_NativeGallery_GetVideoProperties(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery__NativeGallery_GetVideoProperties_m1230C4EEBA839FF1479E5B1206BB8EB812198B96 (String_t* ___path0, const RuntimeMethod* method) ;
// System.Boolean System.Int64::TryParse(System.String,System.Int64&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int64_TryParse_m61AAE5CC4A0B716556765798C22FE12D87554986 (String_t* ___s0, int64_t* ___result1, const RuntimeMethod* method) ;
// System.Boolean System.Single::TryParse(System.String,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_TryParse_mF23E88B4B12DDC9E82179BB2483A714005BF006F (String_t* ___s0, float* ___result1, const RuntimeMethod* method) ;
// System.Void NativeGallery/VideoProperties::.ctor(System.Int32,System.Int32,System.Int64,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoProperties__ctor_m8BF47B718C580EF51037607E56B20BE54AAACA8D (VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426* __this, int32_t ___width0, int32_t ___height1, int64_t ___duration2, float ___rotation3, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C (Delegate_t* ___a0, Delegate_t* ___b1, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116 (Delegate_t* ___source0, Delegate_t* ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.AndroidJavaProxy::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaProxy__ctor_m2832886A0E1BBF6702653A7C6A4609F11FB712C7 (AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D* __this, String_t* ___javaInterface0, const RuntimeMethod* method) ;
// System.Void AndroidPermissionCallback::add_OnPermissionGrantedAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_add_OnPermissionGrantedAction_m44E6F5E74954131D0E5A71EDCF2429760A3400E6 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) ;
// System.Void AndroidPermissionCallback::add_OnPermissionDeniedAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_add_OnPermissionDeniedAction_mAF41F02A3DACD3653F4B8FC5CD90CFFC0946B999 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) ;
// System.Void AndroidPermissionCallback::add_OnPermissionDeniedAndDontAskAgainAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_add_OnPermissionDeniedAndDontAskAgainAction_m6141AEA2C6889368FCFA0A6EB0A95FB773D32BB8 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_mB5466169E1151B8CC44C8FED234D79984B431389 (AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* __this, String_t* ___className0, const RuntimeMethod* method) ;
// FieldType UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
inline AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_mD7D192A35EB2B2DA3775FAB081958B72088251DD (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* __this, String_t* ___fieldName0, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* (*) (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0*, String_t*, const RuntimeMethod*))AndroidJavaObject_GetStatic_TisRuntimeObject_m4EF4E4761A0A6E99E0A298F653E8129B1494E4C9_gshared)(__this, ___fieldName0, method);
}
// T[] System.Array::Empty<System.Object>()
inline ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_inline (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_gshared_inline)(method);
}
// System.Void UnityEngine.AndroidJavaObject::.ctor(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject__ctor_m5A65B5D325C2CEFAC4097A0D3813F8E158178DD7 (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* __this, String_t* ___className0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method) ;
// UnityEngine.AndroidJavaObject AndroidPermissionsManager::GetPermissionsService()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* AndroidPermissionsManager_GetPermissionsService_m3E7C258986D3D1CCA125A14B60B631A4CE7BB249 (const RuntimeMethod* method) ;
// UnityEngine.AndroidJavaObject AndroidPermissionsManager::GetActivity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* AndroidPermissionsManager_GetActivity_m3209BA56C4C200CD1E4D9A421A0E03E1C3636CE9 (const RuntimeMethod* method) ;
// ReturnType UnityEngine.AndroidJavaObject::Call<System.Boolean>(System.String,System.Object[])
inline bool AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* __this, String_t* ___methodName0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method)
{
	return ((  bool (*) (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0*, String_t*, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*, const RuntimeMethod*))AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF_gshared)(__this, ___methodName0, ___args1, method);
}
// System.Void AndroidPermissionsManager::RequestPermission(System.String[],AndroidPermissionCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsManager_RequestPermission_mE1C5F9692C4F4E0FF709553367E5CBF735C53BE5 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___permissionNames0, AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* ___callback1, const RuntimeMethod* method) ;
// System.Void UnityEngine.AndroidJavaObject::Call(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject_Call_mDEF7846E2AB1C5379069BB21049ED55A9D837B1C (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* __this, String_t* ___methodName0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method) ;
// System.Boolean AndroidPermissionsUsageExample::CheckPermissions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidPermissionsUsageExample_CheckPermissions_m16510B9ADE6A8B12197040D2D23AB1F166D9C94D (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, const RuntimeMethod* method) ;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m1AB34E71D9885B120F6021EB2B11DCB28CD6008D (const RuntimeMethod* method) ;
// System.Boolean AndroidPermissionsManager::IsPermissionGranted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidPermissionsManager_IsPermissionGranted_m98B2891605E6A1CEEFBDFAE7263DD6A3449287B9 (String_t* ___permissionName0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m9DC2953C55C4D7D4B7BEFE03D84DA1F9362D652C (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void AndroidPermissionCallback::.ctor(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback__ctor_mC050EA0B34D96B54DAB4EA1352A92569847C05F4 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onGrantedCallback0, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onDeniedCallback1, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onDeniedAndDontAskAgainCallback2, const RuntimeMethod* method) ;
// System.Void AndroidPermissionsUsageExample::OnBrowseGalleryButtonPress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsUsageExample_OnBrowseGalleryButtonPress_mF935173F287EC2F2E2ABF2918C2CD511644006CF (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, const RuntimeMethod* method) ;
// System.Void AndroidPermissionsUsageExample/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEAE1E9212598050ACEFFB064B66AA4D702409461 (U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<NativeGalleryNamespace.NGMediaReceiveCallbackiOS>()
inline NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* GameObject_AddComponent_TisNGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_mF5EFD3397ADDE73CB43C1A935AB7D0D6A1C29082 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_realtimeSinceStartup_mB49A5622E38FFE9589EB9B3E75573E443B8D63EC (const RuntimeMethod* method) ;
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::set_IsBusy(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_set_IsBusy_m1A7EE6ECFA48392D1162D1EFAA5402E343762731_inline (bool ___value0, const RuntimeMethod* method) ;
// System.Int32 NativeGalleryNamespace.NGMediaReceiveCallbackiOS::_NativeGallery_IsMediaPickerBusy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_mA785D9122A92E8F0C3179725A475AFED74349AAF (const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<NativeGalleryNamespace.NGMediaSaveCallbackiOS>()
inline NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* GameObject_AddComponent_TisNGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_m8EC160CD8F792A4F3207AD92BB7F66059B644F22 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared)(__this, method);
}
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B (RuntimeArray* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLELog(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEInitialize(int32_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEDeInitialize();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEPauseMessages(int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEScanForPeripheralsWithServices(char*, int32_t, int32_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLERetrieveListOfPeripheralsWithServices(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEStopScan();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEConnectToPeripheral(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEDisconnectPeripheral(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEReadCharacteristic(char*, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEWriteCharacteristic(char*, char*, char*, uint8_t*, int32_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLESubscribeCharacteristic(char*, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEUnSubscribeCharacteristic(char*, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEDisconnectAll();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEScanForBeacons(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEStopBeaconScan();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEPeripheralName(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLECreateService(char*, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLERemoveService(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLERemoveServices();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLECreateCharacteristic(char*, int32_t, int32_t, uint8_t*, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLERemoveCharacteristic(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLERemoveCharacteristics();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEStartAdvertising();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEStopAdvertising();
IL2CPP_EXTERN_C void DEFAULT_CALL _iOSBluetoothLEUpdateCharacteristicValue(char*, uint8_t*, int32_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _NativeGallery_CheckPermission();
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _NativeGallery_RequestPermission();
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _NativeGallery_CanOpenSettings();
IL2CPP_EXTERN_C void DEFAULT_CALL _NativeGallery_OpenSettings();
IL2CPP_EXTERN_C void DEFAULT_CALL _NativeGallery_ImageWriteToAlbum(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _NativeGallery_VideoWriteToAlbum(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _NativeGallery_PickImage(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _NativeGallery_PickVideo(char*);
IL2CPP_EXTERN_C char* DEFAULT_CALL _NativeGallery_GetImageProperties(char*);
IL2CPP_EXTERN_C char* DEFAULT_CALL _NativeGallery_GetVideoProperties(char*);
IL2CPP_EXTERN_C char* DEFAULT_CALL _NativeGallery_LoadImageAtPath(char*, char*, int32_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _NativeGallery_IsMediaPickerBusy();
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BluetoothDeviceScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_Start_m53F8F286D6BE17577FAF0DD4E5D671D9B2DDFE64 (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m15E112E61AB3E2CDF5F8C4D5478C173E7CE98B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m2C28FA742C2910206687FAC6656082BB86091AB5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA59DD81658DA8792C58F4AF9ABB667457AE7888F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mFACB7E9C70103BA8CD2CFFC7D56AB4B8B9FADD7F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DiscoveredDeviceList = new List<string> ();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_0 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_0, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___DiscoveredDeviceList_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___DiscoveredDeviceList_4), (void*)L_0);
		// DidUpdateNotificationStateForCharacteristicAction = new Dictionary<string, Dictionary<string, Action<string>>> ();
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_1 = (Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D*)il2cpp_codegen_object_new(Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		Dictionary_2__ctor_mFACB7E9C70103BA8CD2CFFC7D56AB4B8B9FADD7F(L_1, Dictionary_2__ctor_mFACB7E9C70103BA8CD2CFFC7D56AB4B8B9FADD7F_RuntimeMethod_var);
		__this->___DidUpdateNotificationStateForCharacteristicAction_22 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___DidUpdateNotificationStateForCharacteristicAction_22), (void*)L_1);
		// DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction = new Dictionary<string, Dictionary<string, Action<string, string>>> ();
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_2 = (Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74*)il2cpp_codegen_object_new(Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Dictionary_2__ctor_m15E112E61AB3E2CDF5F8C4D5478C173E7CE98B4C(L_2, Dictionary_2__ctor_m15E112E61AB3E2CDF5F8C4D5478C173E7CE98B4C_RuntimeMethod_var);
		__this->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23), (void*)L_2);
		// DidUpdateCharacteristicValueAction = new Dictionary<string, Dictionary<string, Action<string, byte[]>>> ();
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_3 = (Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78*)il2cpp_codegen_object_new(Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		Dictionary_2__ctor_m2C28FA742C2910206687FAC6656082BB86091AB5(L_3, Dictionary_2__ctor_m2C28FA742C2910206687FAC6656082BB86091AB5_RuntimeMethod_var);
		__this->___DidUpdateCharacteristicValueAction_24 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___DidUpdateCharacteristicValueAction_24), (void*)L_3);
		// DidUpdateCharacteristicValueWithDeviceAddressAction = new Dictionary<string, Dictionary<string, Action<string, string, byte[]>>> ();
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_4 = (Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883*)il2cpp_codegen_object_new(Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Dictionary_2__ctor_mA59DD81658DA8792C58F4AF9ABB667457AE7888F(L_4, Dictionary_2__ctor_mA59DD81658DA8792C58F4AF9ABB667457AE7888F_RuntimeMethod_var);
		__this->___DidUpdateCharacteristicValueWithDeviceAddressAction_25 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___DidUpdateCharacteristicValueWithDeviceAddressAction_25), (void*)L_4);
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_Update_m7CE25112852AB386721649D3849111E326B465A0 (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::OnBluetoothMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_OnBluetoothMessage_m80BB33FD05C1343CCDF235788EFB697AA33831DF (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m913953EB949920103EE45B0C497AED20472529CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mBBC25AE20AA64D8E9CB489C3F455282573A79550_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01FC300FC7D69084F12B27BCC0C38122C800B8C9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral04938D88CCBF5A98466865086ED2669F409064BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283D774A5141A159CCA779600D4FD489AFD39105);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA10411FCA4F6A342C7FF683134054CC32BFC6755);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCDC8B946EE0851853017EBA616344D4F97D44411);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_0 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_5 = NULL;
	iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96 V_6;
	memset((&V_6), 0, sizeof(V_6));
	int32_t V_7 = 0;
	Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* V_8 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_9 = NULL;
	Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* V_10 = NULL;
	Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* V_11 = NULL;
	{
		// if (message != null)
		String_t* L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0786;
		}
	}
	{
		// char[] delim = new char[] { '~' };
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)126));
		V_0 = L_2;
		// string[] parts = message.Split (delim);
		String_t* L_3 = ___message0;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_4 = V_0;
		NullCheck(L_3);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5;
		L_5 = String_Split_m101D35FEC86371D2BB4E3480F6F896880093B2E9(L_3, L_4, NULL);
		V_1 = L_5;
		// for (int i = 0; i < parts.Length; ++i)
		V_2 = 0;
		goto IL_003a;
	}

IL_001e:
	{
		// BluetoothLEHardwareInterface.Log (string.Format ("Part: {0} - {1}", i, parts[i]));
		int32_t L_6 = V_2;
		int32_t L_7 = L_6;
		RuntimeObject* L_8 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_7);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		String_t* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		String_t* L_13;
		L_13 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(_stringLiteralA10411FCA4F6A342C7FF683134054CC32BFC6755, L_8, L_12, NULL);
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(L_13, NULL);
		// for (int i = 0; i < parts.Length; ++i)
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_003a:
	{
		// for (int i = 0; i < parts.Length; ++i)
		int32_t L_15 = V_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))
		{
			goto IL_001e;
		}
	}
	{
		// if (message.Length >= deviceInitializedString.Length && message.Substring (0, deviceInitializedString.Length) == deviceInitializedString)
		String_t* L_17 = ___message0;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_17, NULL);
		NullCheck(_stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680);
		int32_t L_19;
		L_19 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680, NULL);
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0086;
		}
	}
	{
		String_t* L_20 = ___message0;
		NullCheck(_stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680);
		int32_t L_21;
		L_21 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680, NULL);
		NullCheck(L_20);
		String_t* L_22;
		L_22 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_20, 0, L_21, NULL);
		bool L_23;
		L_23 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_22, _stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680, NULL);
		if (!L_23)
		{
			goto IL_0086;
		}
	}
	{
		// if (InitializedAction != null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_24 = __this->___InitializedAction_5;
		if (!L_24)
		{
			goto IL_0786;
		}
	}
	{
		// InitializedAction ();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_25 = __this->___InitializedAction_5;
		NullCheck(L_25);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_25, NULL);
		return;
	}

IL_0086:
	{
		// else if (message.Length >= deviceLog.Length && message.Substring (0, deviceLog.Length) == deviceLog)
		String_t* L_26 = ___message0;
		NullCheck(L_26);
		int32_t L_27;
		L_27 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_26, NULL);
		NullCheck(_stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0);
		int32_t L_28;
		L_28 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0, NULL);
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_00be;
		}
	}
	{
		String_t* L_29 = ___message0;
		NullCheck(_stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0);
		int32_t L_30;
		L_30 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0, NULL);
		NullCheck(L_29);
		String_t* L_31;
		L_31 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_29, 0, L_30, NULL);
		bool L_32;
		L_32 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_31, _stringLiteralD2185E2B320102DBAC16E15976BE9935D7508BC0, NULL);
		if (!L_32)
		{
			goto IL_00be;
		}
	}
	{
		// Debug.Log (parts[1]);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = 1;
		String_t* L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_35, NULL);
		return;
	}

IL_00be:
	{
		// else if (message.Length >= deviceDeInitializedString.Length && message.Substring (0, deviceDeInitializedString.Length) == deviceDeInitializedString)
		String_t* L_36 = ___message0;
		NullCheck(L_36);
		int32_t L_37;
		L_37 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_36, NULL);
		NullCheck(_stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415);
		int32_t L_38;
		L_38 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415, NULL);
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0109;
		}
	}
	{
		String_t* L_39 = ___message0;
		NullCheck(_stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415);
		int32_t L_40;
		L_40 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415, NULL);
		NullCheck(L_39);
		String_t* L_41;
		L_41 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_39, 0, L_40, NULL);
		bool L_42;
		L_42 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_41, _stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415, NULL);
		if (!L_42)
		{
			goto IL_0109;
		}
	}
	{
		// BluetoothLEHardwareInterface.FinishDeInitialize ();
		BluetoothLEHardwareInterface_FinishDeInitialize_m4E276D6FFA746F913E33B5274891788DD55A017E(NULL);
		// if (DeinitializedAction != null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_43 = __this->___DeinitializedAction_6;
		if (!L_43)
		{
			goto IL_0786;
		}
	}
	{
		// DeinitializedAction ();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_44 = __this->___DeinitializedAction_6;
		NullCheck(L_44);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_44, NULL);
		return;
	}

IL_0109:
	{
		// else if (message.Length >= deviceErrorString.Length && message.Substring (0, deviceErrorString.Length) == deviceErrorString)
		String_t* L_45 = ___message0;
		NullCheck(L_45);
		int32_t L_46;
		L_46 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_45, NULL);
		NullCheck(_stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7);
		int32_t L_47;
		L_47 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7, NULL);
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_0160;
		}
	}
	{
		String_t* L_48 = ___message0;
		NullCheck(_stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7);
		int32_t L_49;
		L_49 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7, NULL);
		NullCheck(L_48);
		String_t* L_50;
		L_50 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_48, 0, L_49, NULL);
		bool L_51;
		L_51 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_50, _stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7, NULL);
		if (!L_51)
		{
			goto IL_0160;
		}
	}
	{
		// string error = "";
		V_3 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// if (parts.Length >= 2)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_52 = V_1;
		NullCheck(L_52);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_52)->max_length))) < ((int32_t)2)))
		{
			goto IL_0148;
		}
	}
	{
		// error = parts[1];
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_53 = V_1;
		NullCheck(L_53);
		int32_t L_54 = 1;
		String_t* L_55 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		V_3 = L_55;
	}

IL_0148:
	{
		// if (ErrorAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_56 = __this->___ErrorAction_7;
		if (!L_56)
		{
			goto IL_0786;
		}
	}
	{
		// ErrorAction (error);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_57 = __this->___ErrorAction_7;
		String_t* L_58 = V_3;
		NullCheck(L_57);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_57, L_58, NULL);
		return;
	}

IL_0160:
	{
		// else if (message.Length >= deviceServiceAdded.Length && message.Substring (0, deviceServiceAdded.Length) == deviceServiceAdded)
		String_t* L_59 = ___message0;
		NullCheck(L_59);
		int32_t L_60;
		L_60 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_59, NULL);
		NullCheck(_stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2);
		int32_t L_61;
		L_61 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2, NULL);
		if ((((int32_t)L_60) < ((int32_t)L_61)))
		{
			goto IL_01b2;
		}
	}
	{
		String_t* L_62 = ___message0;
		NullCheck(_stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2);
		int32_t L_63;
		L_63 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2, NULL);
		NullCheck(L_62);
		String_t* L_64;
		L_64 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_62, 0, L_63, NULL);
		bool L_65;
		L_65 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_64, _stringLiteral8ECB79B6EBAF555ECC8A534557CBA387332ABDB2, NULL);
		if (!L_65)
		{
			goto IL_01b2;
		}
	}
	{
		// if (parts.Length >= 2)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_66 = V_1;
		NullCheck(L_66);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_66)->max_length))) < ((int32_t)2)))
		{
			goto IL_0786;
		}
	}
	{
		// if (ServiceAddedAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_67 = __this->___ServiceAddedAction_8;
		if (!L_67)
		{
			goto IL_0786;
		}
	}
	{
		// ServiceAddedAction (parts[1]);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_68 = __this->___ServiceAddedAction_8;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_69 = V_1;
		NullCheck(L_69);
		int32_t L_70 = 1;
		String_t* L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck(L_68);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_68, L_71, NULL);
		return;
	}

IL_01b2:
	{
		// else if (message.Length >= deviceStartedAdvertising.Length && message.Substring (0, deviceStartedAdvertising.Length) == deviceStartedAdvertising)
		String_t* L_72 = ___message0;
		NullCheck(L_72);
		int32_t L_73;
		L_73 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_72, NULL);
		NullCheck(_stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F);
		int32_t L_74;
		L_74 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F, NULL);
		if ((((int32_t)L_73) < ((int32_t)L_74)))
		{
			goto IL_0202;
		}
	}
	{
		String_t* L_75 = ___message0;
		NullCheck(_stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F);
		int32_t L_76;
		L_76 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F, NULL);
		NullCheck(L_75);
		String_t* L_77;
		L_77 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_75, 0, L_76, NULL);
		bool L_78;
		L_78 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_77, _stringLiteral055547E9CF7CA46009A0E7BBE0EBFD1D965FA37F, NULL);
		if (!L_78)
		{
			goto IL_0202;
		}
	}
	{
		// BluetoothLEHardwareInterface.Log ("Started Advertising");
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(_stringLiteral01FC300FC7D69084F12B27BCC0C38122C800B8C9, NULL);
		// if (StartedAdvertisingAction != null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_79 = __this->___StartedAdvertisingAction_9;
		if (!L_79)
		{
			goto IL_0786;
		}
	}
	{
		// StartedAdvertisingAction ();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_80 = __this->___StartedAdvertisingAction_9;
		NullCheck(L_80);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_80, NULL);
		return;
	}

IL_0202:
	{
		// else if (message.Length >= deviceStoppedAdvertising.Length && message.Substring (0, deviceStoppedAdvertising.Length) == deviceStoppedAdvertising)
		String_t* L_81 = ___message0;
		NullCheck(L_81);
		int32_t L_82;
		L_82 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_81, NULL);
		NullCheck(_stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51);
		int32_t L_83;
		L_83 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51, NULL);
		if ((((int32_t)L_82) < ((int32_t)L_83)))
		{
			goto IL_0252;
		}
	}
	{
		String_t* L_84 = ___message0;
		NullCheck(_stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51);
		int32_t L_85;
		L_85 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51, NULL);
		NullCheck(L_84);
		String_t* L_86;
		L_86 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_84, 0, L_85, NULL);
		bool L_87;
		L_87 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_86, _stringLiteralB8DEA5867E81465D0D2D4C07103BBEB6CDADFD51, NULL);
		if (!L_87)
		{
			goto IL_0252;
		}
	}
	{
		// BluetoothLEHardwareInterface.Log ("Stopped Advertising");
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(_stringLiteralCDC8B946EE0851853017EBA616344D4F97D44411, NULL);
		// if (StoppedAdvertisingAction != null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_88 = __this->___StoppedAdvertisingAction_10;
		if (!L_88)
		{
			goto IL_0786;
		}
	}
	{
		// StoppedAdvertisingAction ();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_89 = __this->___StoppedAdvertisingAction_10;
		NullCheck(L_89);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_89, NULL);
		return;
	}

IL_0252:
	{
		// else if (message.Length >= deviceDiscoveredPeripheral.Length && message.Substring (0, deviceDiscoveredPeripheral.Length) == deviceDiscoveredPeripheral)
		String_t* L_90 = ___message0;
		NullCheck(L_90);
		int32_t L_91;
		L_91 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_90, NULL);
		NullCheck(_stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49);
		int32_t L_92;
		L_92 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49, NULL);
		if ((((int32_t)L_91) < ((int32_t)L_92)))
		{
			goto IL_030d;
		}
	}
	{
		String_t* L_93 = ___message0;
		NullCheck(_stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49);
		int32_t L_94;
		L_94 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49, NULL);
		NullCheck(L_93);
		String_t* L_95;
		L_95 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_93, 0, L_94, NULL);
		bool L_96;
		L_96 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_95, _stringLiteralD11AE57CAF5237AA62C8FC603DA1381EB3BF7B49, NULL);
		if (!L_96)
		{
			goto IL_030d;
		}
	}
	{
		// if (parts.Length >= 3)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_97 = V_1;
		NullCheck(L_97);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_97)->max_length))) < ((int32_t)3)))
		{
			goto IL_0786;
		}
	}
	{
		// if (!DiscoveredDeviceList.Contains (parts[1]))
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = __this->___DiscoveredDeviceList_4;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = V_1;
		NullCheck(L_99);
		int32_t L_100 = 1;
		String_t* L_101 = (L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_98);
		bool L_102;
		L_102 = List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1(L_98, L_101, List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1_RuntimeMethod_var);
		if (L_102)
		{
			goto IL_02c7;
		}
	}
	{
		// DiscoveredDeviceList.Add (parts[1]);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_103 = __this->___DiscoveredDeviceList_4;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = V_1;
		NullCheck(L_104);
		int32_t L_105 = 1;
		String_t* L_106 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		NullCheck(L_103);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_103, L_106, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// if (DiscoveredPeripheralAction != null)
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_107 = __this->___DiscoveredPeripheralAction_11;
		if (!L_107)
		{
			goto IL_02c7;
		}
	}
	{
		// DiscoveredPeripheralAction (parts[1], parts[2]);
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_108 = __this->___DiscoveredPeripheralAction_11;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_109 = V_1;
		NullCheck(L_109);
		int32_t L_110 = 1;
		String_t* L_111 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_112 = V_1;
		NullCheck(L_112);
		int32_t L_113 = 2;
		String_t* L_114 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		NullCheck(L_108);
		Action_2_Invoke_m8FA30194997244EC0072D6B437818A22B65F2854_inline(L_108, L_111, L_114, NULL);
	}

IL_02c7:
	{
		// if (parts.Length >= 5 && DiscoveredPeripheralWithAdvertisingInfoAction != null)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_115 = V_1;
		NullCheck(L_115);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_115)->max_length))) < ((int32_t)5)))
		{
			goto IL_0786;
		}
	}
	{
		Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* L_116 = __this->___DiscoveredPeripheralWithAdvertisingInfoAction_12;
		if (!L_116)
		{
			goto IL_0786;
		}
	}
	{
		// int rssi = 0;
		V_4 = 0;
		// if (!int.TryParse (parts[3], out rssi))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_117 = V_1;
		NullCheck(L_117);
		int32_t L_118 = 3;
		String_t* L_119 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		bool L_120;
		L_120 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_119, (&V_4), NULL);
		if (L_120)
		{
			goto IL_02ed;
		}
	}
	{
		// rssi = 0;
		V_4 = 0;
	}

IL_02ed:
	{
		// byte[] bytes = System.Convert.FromBase64String (parts[4]);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_121 = V_1;
		NullCheck(L_121);
		int32_t L_122 = 4;
		String_t* L_123 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_122));
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_124;
		L_124 = Convert_FromBase64String_m421F8600CA5124E047E3D7C2BC1B653F67BC48A1(L_123, NULL);
		V_5 = L_124;
		// DiscoveredPeripheralWithAdvertisingInfoAction (parts[1], parts[2], rssi, bytes);
		Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* L_125 = __this->___DiscoveredPeripheralWithAdvertisingInfoAction_12;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_126 = V_1;
		NullCheck(L_126);
		int32_t L_127 = 1;
		String_t* L_128 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_129 = V_1;
		NullCheck(L_129);
		int32_t L_130 = 2;
		String_t* L_131 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_130));
		int32_t L_132 = V_4;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_133 = V_5;
		NullCheck(L_125);
		Action_4_Invoke_mD9DD80B61F675BE34D06BB382884E69323B1C8C1_inline(L_125, L_128, L_131, L_132, L_133, NULL);
		return;
	}

IL_030d:
	{
		// else if (message.Length >= deviceDiscoveredBeacon.Length && message.Substring (0, deviceDiscoveredBeacon.Length) == deviceDiscoveredBeacon)
		String_t* L_134 = ___message0;
		NullCheck(L_134);
		int32_t L_135;
		L_135 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_134, NULL);
		NullCheck(_stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53);
		int32_t L_136;
		L_136 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53, NULL);
		if ((((int32_t)L_135) < ((int32_t)L_136)))
		{
			goto IL_03f5;
		}
	}
	{
		String_t* L_137 = ___message0;
		NullCheck(_stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53);
		int32_t L_138;
		L_138 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53, NULL);
		NullCheck(L_137);
		String_t* L_139;
		L_139 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_137, 0, L_138, NULL);
		bool L_140;
		L_140 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_139, _stringLiteral3DB71419DD7D9B880A3A7641C61DD34A55D73E53, NULL);
		if (!L_140)
		{
			goto IL_03f5;
		}
	}
	{
		// if (parts.Length >= 7)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_141 = V_1;
		NullCheck(L_141);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_141)->max_length))) < ((int32_t)7)))
		{
			goto IL_0786;
		}
	}
	{
		// var iBeaconData = new BluetoothLEHardwareInterface.iBeaconData ();
		il2cpp_codegen_initobj((&V_6), sizeof(iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96));
		// iBeaconData.UUID = parts[1];
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_142 = V_1;
		NullCheck(L_142);
		int32_t L_143 = 1;
		String_t* L_144 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		(&V_6)->___UUID_0 = L_144;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_6)->___UUID_0), (void*)L_144);
		// if (!int.TryParse (parts[2], out iBeaconData.Major))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_145 = V_1;
		NullCheck(L_145);
		int32_t L_146 = 2;
		String_t* L_147 = (L_145)->GetAt(static_cast<il2cpp_array_size_t>(L_146));
		int32_t* L_148 = (&(&V_6)->___Major_1);
		bool L_149;
		L_149 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_147, L_148, NULL);
		if (L_149)
		{
			goto IL_0376;
		}
	}
	{
		// iBeaconData.Major = 0;
		(&V_6)->___Major_1 = 0;
	}

IL_0376:
	{
		// if (!int.TryParse (parts[3], out iBeaconData.Minor))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_150 = V_1;
		NullCheck(L_150);
		int32_t L_151 = 3;
		String_t* L_152 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_151));
		int32_t* L_153 = (&(&V_6)->___Minor_2);
		bool L_154;
		L_154 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_152, L_153, NULL);
		if (L_154)
		{
			goto IL_038f;
		}
	}
	{
		// iBeaconData.Minor = 0;
		(&V_6)->___Minor_2 = 0;
	}

IL_038f:
	{
		// if (!int.TryParse (parts[4], out iBeaconData.RSSI))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_155 = V_1;
		NullCheck(L_155);
		int32_t L_156 = 4;
		String_t* L_157 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		int32_t* L_158 = (&(&V_6)->___RSSI_3);
		bool L_159;
		L_159 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_157, L_158, NULL);
		if (L_159)
		{
			goto IL_03a8;
		}
	}
	{
		// iBeaconData.RSSI = 0;
		(&V_6)->___RSSI_3 = 0;
	}

IL_03a8:
	{
		// if (!int.TryParse (parts[5], out iBeaconData.AndroidSignalPower))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_160 = V_1;
		NullCheck(L_160);
		int32_t L_161 = 5;
		String_t* L_162 = (L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		int32_t* L_163 = (&(&V_6)->___AndroidSignalPower_4);
		bool L_164;
		L_164 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_162, L_163, NULL);
		if (L_164)
		{
			goto IL_03c1;
		}
	}
	{
		// iBeaconData.AndroidSignalPower = 0;
		(&V_6)->___AndroidSignalPower_4 = 0;
	}

IL_03c1:
	{
		// int iOSProximity = 0;
		V_7 = 0;
		// if (!int.TryParse (parts[6], out iOSProximity))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_165 = V_1;
		NullCheck(L_165);
		int32_t L_166 = 6;
		String_t* L_167 = (L_165)->GetAt(static_cast<il2cpp_array_size_t>(L_166));
		bool L_168;
		L_168 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_167, (&V_7), NULL);
		if (L_168)
		{
			goto IL_03d3;
		}
	}
	{
		// iOSProximity = 0;
		V_7 = 0;
	}

IL_03d3:
	{
		// iBeaconData.iOSProximity = (BluetoothLEHardwareInterface.iOSProximity)iOSProximity;
		int32_t L_169 = V_7;
		(&V_6)->___iOSProximity_5 = L_169;
		// if (DiscoveredBeaconAction != null)
		Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* L_170 = __this->___DiscoveredBeaconAction_13;
		if (!L_170)
		{
			goto IL_0786;
		}
	}
	{
		// DiscoveredBeaconAction (iBeaconData);
		Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* L_171 = __this->___DiscoveredBeaconAction_13;
		iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96 L_172 = V_6;
		NullCheck(L_171);
		Action_1_Invoke_mE56360F8A5B137A1714DEF08A9266EF9166480B8_inline(L_171, L_172, NULL);
		return;
	}

IL_03f5:
	{
		// else if (message.Length >= deviceRetrievedConnectedPeripheral.Length && message.Substring (0, deviceRetrievedConnectedPeripheral.Length) == deviceRetrievedConnectedPeripheral)
		String_t* L_173 = ___message0;
		NullCheck(L_173);
		int32_t L_174;
		L_174 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_173, NULL);
		NullCheck(_stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28);
		int32_t L_175;
		L_175 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28, NULL);
		if ((((int32_t)L_174) < ((int32_t)L_175)))
		{
			goto IL_0458;
		}
	}
	{
		String_t* L_176 = ___message0;
		NullCheck(_stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28);
		int32_t L_177;
		L_177 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28, NULL);
		NullCheck(L_176);
		String_t* L_178;
		L_178 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_176, 0, L_177, NULL);
		bool L_179;
		L_179 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_178, _stringLiteralBC1F19B5269AF2F944325E84A78744A84AC90E28, NULL);
		if (!L_179)
		{
			goto IL_0458;
		}
	}
	{
		// if (parts.Length >= 3)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_180 = V_1;
		NullCheck(L_180);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_180)->max_length))) < ((int32_t)3)))
		{
			goto IL_0786;
		}
	}
	{
		// DiscoveredDeviceList.Add (parts[1]);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_181 = __this->___DiscoveredDeviceList_4;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_182 = V_1;
		NullCheck(L_182);
		int32_t L_183 = 1;
		String_t* L_184 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_183));
		NullCheck(L_181);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_181, L_184, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// if (RetrievedConnectedPeripheralAction != null)
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_185 = __this->___RetrievedConnectedPeripheralAction_14;
		if (!L_185)
		{
			goto IL_0786;
		}
	}
	{
		// RetrievedConnectedPeripheralAction (parts[1], parts[2]);
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_186 = __this->___RetrievedConnectedPeripheralAction_14;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_187 = V_1;
		NullCheck(L_187);
		int32_t L_188 = 1;
		String_t* L_189 = (L_187)->GetAt(static_cast<il2cpp_array_size_t>(L_188));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_190 = V_1;
		NullCheck(L_190);
		int32_t L_191 = 2;
		String_t* L_192 = (L_190)->GetAt(static_cast<il2cpp_array_size_t>(L_191));
		NullCheck(L_186);
		Action_2_Invoke_m8FA30194997244EC0072D6B437818A22B65F2854_inline(L_186, L_189, L_192, NULL);
		return;
	}

IL_0458:
	{
		// else if (message.Length >= devicePeripheralReceivedWriteData.Length && message.Substring (0, devicePeripheralReceivedWriteData.Length) == devicePeripheralReceivedWriteData)
		String_t* L_193 = ___message0;
		NullCheck(L_193);
		int32_t L_194;
		L_194 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_193, NULL);
		NullCheck(_stringLiteral04938D88CCBF5A98466865086ED2669F409064BE);
		int32_t L_195;
		L_195 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral04938D88CCBF5A98466865086ED2669F409064BE, NULL);
		if ((((int32_t)L_194) < ((int32_t)L_195)))
		{
			goto IL_049d;
		}
	}
	{
		String_t* L_196 = ___message0;
		NullCheck(_stringLiteral04938D88CCBF5A98466865086ED2669F409064BE);
		int32_t L_197;
		L_197 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral04938D88CCBF5A98466865086ED2669F409064BE, NULL);
		NullCheck(L_196);
		String_t* L_198;
		L_198 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_196, 0, L_197, NULL);
		bool L_199;
		L_199 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_198, _stringLiteral04938D88CCBF5A98466865086ED2669F409064BE, NULL);
		if (!L_199)
		{
			goto IL_049d;
		}
	}
	{
		// if (parts.Length >= 3)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_200 = V_1;
		NullCheck(L_200);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_200)->max_length))) < ((int32_t)3)))
		{
			goto IL_0786;
		}
	}
	{
		// OnPeripheralData (parts[1], parts[2]);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_201 = V_1;
		NullCheck(L_201);
		int32_t L_202 = 1;
		String_t* L_203 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_202));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_204 = V_1;
		NullCheck(L_204);
		int32_t L_205 = 2;
		String_t* L_206 = (L_204)->GetAt(static_cast<il2cpp_array_size_t>(L_205));
		BluetoothDeviceScript_OnPeripheralData_mD3F92D2EA496B24CFB212E6D72E2C6BB0891CE96(__this, L_203, L_206, NULL);
		return;
	}

IL_049d:
	{
		// else if (message.Length >= deviceConnectedPeripheral.Length && message.Substring (0, deviceConnectedPeripheral.Length) == deviceConnectedPeripheral)
		String_t* L_207 = ___message0;
		NullCheck(L_207);
		int32_t L_208;
		L_208 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_207, NULL);
		NullCheck(_stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9);
		int32_t L_209;
		L_209 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9, NULL);
		if ((((int32_t)L_208) < ((int32_t)L_209)))
		{
			goto IL_04ef;
		}
	}
	{
		String_t* L_210 = ___message0;
		NullCheck(_stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9);
		int32_t L_211;
		L_211 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9, NULL);
		NullCheck(L_210);
		String_t* L_212;
		L_212 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_210, 0, L_211, NULL);
		bool L_213;
		L_213 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_212, _stringLiteralA7311ED0828EB188F47CB67E1036A7572167C2F9, NULL);
		if (!L_213)
		{
			goto IL_04ef;
		}
	}
	{
		// if (parts.Length >= 2 && ConnectedPeripheralAction != null)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_214 = V_1;
		NullCheck(L_214);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_214)->max_length))) < ((int32_t)2)))
		{
			goto IL_0786;
		}
	}
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_215 = __this->___ConnectedPeripheralAction_16;
		if (!L_215)
		{
			goto IL_0786;
		}
	}
	{
		// ConnectedPeripheralAction (parts[1]);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_216 = __this->___ConnectedPeripheralAction_16;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_217 = V_1;
		NullCheck(L_217);
		int32_t L_218 = 1;
		String_t* L_219 = (L_217)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		NullCheck(L_216);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_216, L_219, NULL);
		return;
	}

IL_04ef:
	{
		// else if (message.Length >= deviceDisconnectedPeripheral.Length && message.Substring (0, deviceDisconnectedPeripheral.Length) == deviceDisconnectedPeripheral)
		String_t* L_220 = ___message0;
		NullCheck(L_220);
		int32_t L_221;
		L_221 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_220, NULL);
		NullCheck(_stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3);
		int32_t L_222;
		L_222 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3, NULL);
		if ((((int32_t)L_221) < ((int32_t)L_222)))
		{
			goto IL_0557;
		}
	}
	{
		String_t* L_223 = ___message0;
		NullCheck(_stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3);
		int32_t L_224;
		L_224 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3, NULL);
		NullCheck(L_223);
		String_t* L_225;
		L_225 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_223, 0, L_224, NULL);
		bool L_226;
		L_226 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_225, _stringLiteralA4D91B5857748A8BA4721A92F64CB7597B1037E3, NULL);
		if (!L_226)
		{
			goto IL_0557;
		}
	}
	{
		// if (parts.Length >= 2)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_227 = V_1;
		NullCheck(L_227);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_227)->max_length))) < ((int32_t)2)))
		{
			goto IL_0786;
		}
	}
	{
		// if (ConnectedDisconnectPeripheralAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_228 = __this->___ConnectedDisconnectPeripheralAction_17;
		if (!L_228)
		{
			goto IL_053d;
		}
	}
	{
		// ConnectedDisconnectPeripheralAction (parts[1]);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_229 = __this->___ConnectedDisconnectPeripheralAction_17;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_230 = V_1;
		NullCheck(L_230);
		int32_t L_231 = 1;
		String_t* L_232 = (L_230)->GetAt(static_cast<il2cpp_array_size_t>(L_231));
		NullCheck(L_229);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_229, L_232, NULL);
	}

IL_053d:
	{
		// if (DisconnectedPeripheralAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_233 = __this->___DisconnectedPeripheralAction_18;
		if (!L_233)
		{
			goto IL_0786;
		}
	}
	{
		// DisconnectedPeripheralAction (parts[1]);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_234 = __this->___DisconnectedPeripheralAction_18;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_235 = V_1;
		NullCheck(L_235);
		int32_t L_236 = 1;
		String_t* L_237 = (L_235)->GetAt(static_cast<il2cpp_array_size_t>(L_236));
		NullCheck(L_234);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_234, L_237, NULL);
		return;
	}

IL_0557:
	{
		// else if (message.Length >= deviceDiscoveredService.Length && message.Substring (0, deviceDiscoveredService.Length) == deviceDiscoveredService)
		String_t* L_238 = ___message0;
		NullCheck(L_238);
		int32_t L_239;
		L_239 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_238, NULL);
		NullCheck(_stringLiteral283D774A5141A159CCA779600D4FD489AFD39105);
		int32_t L_240;
		L_240 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral283D774A5141A159CCA779600D4FD489AFD39105, NULL);
		if ((((int32_t)L_239) < ((int32_t)L_240)))
		{
			goto IL_05ac;
		}
	}
	{
		String_t* L_241 = ___message0;
		NullCheck(_stringLiteral283D774A5141A159CCA779600D4FD489AFD39105);
		int32_t L_242;
		L_242 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral283D774A5141A159CCA779600D4FD489AFD39105, NULL);
		NullCheck(L_241);
		String_t* L_243;
		L_243 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_241, 0, L_242, NULL);
		bool L_244;
		L_244 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_243, _stringLiteral283D774A5141A159CCA779600D4FD489AFD39105, NULL);
		if (!L_244)
		{
			goto IL_05ac;
		}
	}
	{
		// if (parts.Length >= 3 && DiscoveredServiceAction != null)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_245 = V_1;
		NullCheck(L_245);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_245)->max_length))) < ((int32_t)3)))
		{
			goto IL_0786;
		}
	}
	{
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_246 = __this->___DiscoveredServiceAction_19;
		if (!L_246)
		{
			goto IL_0786;
		}
	}
	{
		// DiscoveredServiceAction (parts[1], parts[2]);
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_247 = __this->___DiscoveredServiceAction_19;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_248 = V_1;
		NullCheck(L_248);
		int32_t L_249 = 1;
		String_t* L_250 = (L_248)->GetAt(static_cast<il2cpp_array_size_t>(L_249));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_251 = V_1;
		NullCheck(L_251);
		int32_t L_252 = 2;
		String_t* L_253 = (L_251)->GetAt(static_cast<il2cpp_array_size_t>(L_252));
		NullCheck(L_247);
		Action_2_Invoke_m8FA30194997244EC0072D6B437818A22B65F2854_inline(L_247, L_250, L_253, NULL);
		return;
	}

IL_05ac:
	{
		// else if (message.Length >= deviceDiscoveredCharacteristic.Length && message.Substring (0, deviceDiscoveredCharacteristic.Length) == deviceDiscoveredCharacteristic)
		String_t* L_254 = ___message0;
		NullCheck(L_254);
		int32_t L_255;
		L_255 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_254, NULL);
		NullCheck(_stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8);
		int32_t L_256;
		L_256 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8, NULL);
		if ((((int32_t)L_255) < ((int32_t)L_256)))
		{
			goto IL_0604;
		}
	}
	{
		String_t* L_257 = ___message0;
		NullCheck(_stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8);
		int32_t L_258;
		L_258 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8, NULL);
		NullCheck(L_257);
		String_t* L_259;
		L_259 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_257, 0, L_258, NULL);
		bool L_260;
		L_260 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_259, _stringLiteral02C3126AC268EB4CA84EB0E1826E079B33CEE7E8, NULL);
		if (!L_260)
		{
			goto IL_0604;
		}
	}
	{
		// if (parts.Length >= 4 && DiscoveredCharacteristicAction != null)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_261 = V_1;
		NullCheck(L_261);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_261)->max_length))) < ((int32_t)4)))
		{
			goto IL_0786;
		}
	}
	{
		Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7* L_262 = __this->___DiscoveredCharacteristicAction_20;
		if (!L_262)
		{
			goto IL_0786;
		}
	}
	{
		// DiscoveredCharacteristicAction (parts[1], parts[2], parts[3]);
		Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7* L_263 = __this->___DiscoveredCharacteristicAction_20;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_264 = V_1;
		NullCheck(L_264);
		int32_t L_265 = 1;
		String_t* L_266 = (L_264)->GetAt(static_cast<il2cpp_array_size_t>(L_265));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_267 = V_1;
		NullCheck(L_267);
		int32_t L_268 = 2;
		String_t* L_269 = (L_267)->GetAt(static_cast<il2cpp_array_size_t>(L_268));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_270 = V_1;
		NullCheck(L_270);
		int32_t L_271 = 3;
		String_t* L_272 = (L_270)->GetAt(static_cast<il2cpp_array_size_t>(L_271));
		NullCheck(L_263);
		Action_3_Invoke_mABFB1B378BF33588992DD5A0180457B4ADB790D9_inline(L_263, L_266, L_269, L_272, NULL);
		return;
	}

IL_0604:
	{
		// else if (message.Length >= deviceDidWriteCharacteristic.Length && message.Substring (0, deviceDidWriteCharacteristic.Length) == deviceDidWriteCharacteristic)
		String_t* L_273 = ___message0;
		NullCheck(L_273);
		int32_t L_274;
		L_274 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_273, NULL);
		NullCheck(_stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6);
		int32_t L_275;
		L_275 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6, NULL);
		if ((((int32_t)L_274) < ((int32_t)L_275)))
		{
			goto IL_0656;
		}
	}
	{
		String_t* L_276 = ___message0;
		NullCheck(_stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6);
		int32_t L_277;
		L_277 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6, NULL);
		NullCheck(L_276);
		String_t* L_278;
		L_278 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_276, 0, L_277, NULL);
		bool L_279;
		L_279 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_278, _stringLiteral9AF66B6B54D90A98DBA38E21C39BEFB461EAA6A6, NULL);
		if (!L_279)
		{
			goto IL_0656;
		}
	}
	{
		// if (parts.Length >= 2 && DidWriteCharacteristicAction != null)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_280 = V_1;
		NullCheck(L_280);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_280)->max_length))) < ((int32_t)2)))
		{
			goto IL_0786;
		}
	}
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_281 = __this->___DidWriteCharacteristicAction_21;
		if (!L_281)
		{
			goto IL_0786;
		}
	}
	{
		// DidWriteCharacteristicAction (parts[1]);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_282 = __this->___DidWriteCharacteristicAction_21;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_283 = V_1;
		NullCheck(L_283);
		int32_t L_284 = 1;
		String_t* L_285 = (L_283)->GetAt(static_cast<il2cpp_array_size_t>(L_284));
		NullCheck(L_282);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_282, L_285, NULL);
		return;
	}

IL_0656:
	{
		// else if (message.Length >= deviceDidUpdateNotificationStateForCharacteristic.Length && message.Substring (0, deviceDidUpdateNotificationStateForCharacteristic.Length) == deviceDidUpdateNotificationStateForCharacteristic)
		String_t* L_286 = ___message0;
		NullCheck(L_286);
		int32_t L_287;
		L_287 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_286, NULL);
		NullCheck(_stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32);
		int32_t L_288;
		L_288 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32, NULL);
		if ((((int32_t)L_287) < ((int32_t)L_288)))
		{
			goto IL_0742;
		}
	}
	{
		String_t* L_289 = ___message0;
		NullCheck(_stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32);
		int32_t L_290;
		L_290 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32, NULL);
		NullCheck(L_289);
		String_t* L_291;
		L_291 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_289, 0, L_290, NULL);
		bool L_292;
		L_292 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_291, _stringLiteral2C5CA582C472914803F7B097F586DA4F20FF1D32, NULL);
		if (!L_292)
		{
			goto IL_0742;
		}
	}
	{
		// if (parts.Length >= 3)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_293 = V_1;
		NullCheck(L_293);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_293)->max_length))) < ((int32_t)3)))
		{
			goto IL_0786;
		}
	}
	{
		// if (DidUpdateNotificationStateForCharacteristicAction != null && DidUpdateNotificationStateForCharacteristicAction.ContainsKey (parts[1]))
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_294 = __this->___DidUpdateNotificationStateForCharacteristicAction_22;
		if (!L_294)
		{
			goto IL_06e6;
		}
	}
	{
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_295 = __this->___DidUpdateNotificationStateForCharacteristicAction_22;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_296 = V_1;
		NullCheck(L_296);
		int32_t L_297 = 1;
		String_t* L_298 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_297));
		NullCheck(L_295);
		bool L_299;
		L_299 = Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F(L_295, L_298, Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		if (!L_299)
		{
			goto IL_06e6;
		}
	}
	{
		// var characteristicAction = DidUpdateNotificationStateForCharacteristicAction[parts[1]];
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_300 = __this->___DidUpdateNotificationStateForCharacteristicAction_22;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_301 = V_1;
		NullCheck(L_301);
		int32_t L_302 = 1;
		String_t* L_303 = (L_301)->GetAt(static_cast<il2cpp_array_size_t>(L_302));
		NullCheck(L_300);
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_304;
		L_304 = Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61(L_300, L_303, Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		V_8 = L_304;
		// if (characteristicAction != null && characteristicAction.ContainsKey (parts[2]))
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_305 = V_8;
		if (!L_305)
		{
			goto IL_06e6;
		}
	}
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_306 = V_8;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_307 = V_1;
		NullCheck(L_307);
		int32_t L_308 = 2;
		String_t* L_309 = (L_307)->GetAt(static_cast<il2cpp_array_size_t>(L_308));
		NullCheck(L_306);
		bool L_310;
		L_310 = Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E(L_306, L_309, Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var);
		if (!L_310)
		{
			goto IL_06e6;
		}
	}
	{
		// var action = characteristicAction[parts[2]];
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_311 = V_8;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_312 = V_1;
		NullCheck(L_312);
		int32_t L_313 = 2;
		String_t* L_314 = (L_312)->GetAt(static_cast<il2cpp_array_size_t>(L_313));
		NullCheck(L_311);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_315;
		L_315 = Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D(L_311, L_314, Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D_RuntimeMethod_var);
		V_9 = L_315;
		// if (action != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_316 = V_9;
		if (!L_316)
		{
			goto IL_06e6;
		}
	}
	{
		// action (parts[2]);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_317 = V_9;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_318 = V_1;
		NullCheck(L_318);
		int32_t L_319 = 2;
		String_t* L_320 = (L_318)->GetAt(static_cast<il2cpp_array_size_t>(L_319));
		NullCheck(L_317);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_317, L_320, NULL);
	}

IL_06e6:
	{
		// if (DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction != null && DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction.ContainsKey (parts[1]))
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_321 = __this->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		if (!L_321)
		{
			goto IL_0786;
		}
	}
	{
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_322 = __this->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_323 = V_1;
		NullCheck(L_323);
		int32_t L_324 = 1;
		String_t* L_325 = (L_323)->GetAt(static_cast<il2cpp_array_size_t>(L_324));
		NullCheck(L_322);
		bool L_326;
		L_326 = Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23(L_322, L_325, Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var);
		if (!L_326)
		{
			goto IL_0786;
		}
	}
	{
		// var characteristicAction = DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction[parts[1]];
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_327 = __this->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_328 = V_1;
		NullCheck(L_328);
		int32_t L_329 = 1;
		String_t* L_330 = (L_328)->GetAt(static_cast<il2cpp_array_size_t>(L_329));
		NullCheck(L_327);
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_331;
		L_331 = Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF(L_327, L_330, Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var);
		V_10 = L_331;
		// if (characteristicAction != null && characteristicAction.ContainsKey (parts[2]))
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_332 = V_10;
		if (!L_332)
		{
			goto IL_0786;
		}
	}
	{
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_333 = V_10;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_334 = V_1;
		NullCheck(L_334);
		int32_t L_335 = 2;
		String_t* L_336 = (L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_335));
		NullCheck(L_333);
		bool L_337;
		L_337 = Dictionary_2_ContainsKey_m913953EB949920103EE45B0C497AED20472529CD(L_333, L_336, Dictionary_2_ContainsKey_m913953EB949920103EE45B0C497AED20472529CD_RuntimeMethod_var);
		if (!L_337)
		{
			goto IL_0786;
		}
	}
	{
		// var action = characteristicAction[parts[2]];
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_338 = V_10;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_339 = V_1;
		NullCheck(L_339);
		int32_t L_340 = 2;
		String_t* L_341 = (L_339)->GetAt(static_cast<il2cpp_array_size_t>(L_340));
		NullCheck(L_338);
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_342;
		L_342 = Dictionary_2_get_Item_mBBC25AE20AA64D8E9CB489C3F455282573A79550(L_338, L_341, Dictionary_2_get_Item_mBBC25AE20AA64D8E9CB489C3F455282573A79550_RuntimeMethod_var);
		V_11 = L_342;
		// if (action != null)
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_343 = V_11;
		if (!L_343)
		{
			goto IL_0786;
		}
	}
	{
		// action (parts[1], parts[2]);
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_344 = V_11;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_345 = V_1;
		NullCheck(L_345);
		int32_t L_346 = 1;
		String_t* L_347 = (L_345)->GetAt(static_cast<il2cpp_array_size_t>(L_346));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_348 = V_1;
		NullCheck(L_348);
		int32_t L_349 = 2;
		String_t* L_350 = (L_348)->GetAt(static_cast<il2cpp_array_size_t>(L_349));
		NullCheck(L_344);
		Action_2_Invoke_m8FA30194997244EC0072D6B437818A22B65F2854_inline(L_344, L_347, L_350, NULL);
		return;
	}

IL_0742:
	{
		// else if (message.Length >= deviceDidUpdateValueForCharacteristic.Length && message.Substring (0, deviceDidUpdateValueForCharacteristic.Length) == deviceDidUpdateValueForCharacteristic)
		String_t* L_351 = ___message0;
		NullCheck(L_351);
		int32_t L_352;
		L_352 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_351, NULL);
		NullCheck(_stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84);
		int32_t L_353;
		L_353 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84, NULL);
		if ((((int32_t)L_352) < ((int32_t)L_353)))
		{
			goto IL_0786;
		}
	}
	{
		String_t* L_354 = ___message0;
		NullCheck(_stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84);
		int32_t L_355;
		L_355 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84, NULL);
		NullCheck(L_354);
		String_t* L_356;
		L_356 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_354, 0, L_355, NULL);
		bool L_357;
		L_357 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_356, _stringLiteral571BE214A9046BE6BECE9693FC64F752B55BDE84, NULL);
		if (!L_357)
		{
			goto IL_0786;
		}
	}
	{
		// if (parts.Length >= 4)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_358 = V_1;
		NullCheck(L_358);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_358)->max_length))) < ((int32_t)4)))
		{
			goto IL_0786;
		}
	}
	{
		// OnBluetoothData (parts[1], parts[2], parts[3]);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_359 = V_1;
		NullCheck(L_359);
		int32_t L_360 = 1;
		String_t* L_361 = (L_359)->GetAt(static_cast<il2cpp_array_size_t>(L_360));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_362 = V_1;
		NullCheck(L_362);
		int32_t L_363 = 2;
		String_t* L_364 = (L_362)->GetAt(static_cast<il2cpp_array_size_t>(L_363));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_365 = V_1;
		NullCheck(L_365);
		int32_t L_366 = 3;
		String_t* L_367 = (L_365)->GetAt(static_cast<il2cpp_array_size_t>(L_366));
		BluetoothDeviceScript_OnBluetoothData_mC3C7BE5FBFA895495E5D3F2F2147C73378437C2D(__this, L_361, L_364, L_367, NULL);
	}

IL_0786:
	{
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_OnBluetoothData_mF56A6ED89634D42E0D2EED26ED77B11E54BDD1A2 (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, String_t* ___base64Data0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnBluetoothData ("", "", base64Data);
		String_t* L_0 = ___base64Data0;
		BluetoothDeviceScript_OnBluetoothData_mC3C7BE5FBFA895495E5D3F2F2147C73378437C2D(__this, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, L_0, NULL);
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_OnBluetoothData_mC3C7BE5FBFA895495E5D3F2F2147C73378437C2D (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, String_t* ___deviceAddress0, String_t* ___characteristic1, String_t* ___base64Data2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m49883733B2BD00869A51867DF487463D793B72AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m4A499F6EAC71DC55F8B1A7DF3EA4AE5FCDFE83F1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m29D67E65079024F5C041D7F9AA960C3FB8C61727_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mC10FD3398D9C91885BEE2ED01F03E6EA8F2458DF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12F5EB18DE661BBE815BE7C12357AB74AFA912BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral95B70EA0EBEA74FB776F022984B7AB5D2D304AF7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD638980A42773DBA4D111CE8D3979093BAC27E5);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	String_t* V_1 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_2 = NULL;
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* V_5 = NULL;
	Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* V_6 = NULL;
	Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* V_7 = NULL;
	Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* V_8 = NULL;
	{
		// if (base64Data != null)
		String_t* L_0 = ___base64Data2;
		if (!L_0)
		{
			goto IL_0106;
		}
	}
	{
		// byte[] bytes = System.Convert.FromBase64String (base64Data);
		String_t* L_1 = ___base64Data2;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2;
		L_2 = Convert_FromBase64String_m421F8600CA5124E047E3D7C2BC1B653F67BC48A1(L_1, NULL);
		V_0 = L_2;
		// if (bytes.Length > 0)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = V_0;
		NullCheck(L_3);
		if (!(((RuntimeArray*)L_3)->max_length))
		{
			goto IL_0106;
		}
	}
	{
		// deviceAddress = deviceAddress.ToUpper ();
		String_t* L_4 = ___deviceAddress0;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_4, NULL);
		___deviceAddress0 = L_5;
		// characteristic = characteristic.ToUpper ();
		String_t* L_6 = ___characteristic1;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_6, NULL);
		___characteristic1 = L_7;
		// BluetoothLEHardwareInterface.Log ("Device: " + deviceAddress + " Characteristic Received: " + characteristic);
		String_t* L_8 = ___deviceAddress0;
		String_t* L_9 = ___characteristic1;
		String_t* L_10;
		L_10 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteral95B70EA0EBEA74FB776F022984B7AB5D2D304AF7, L_8, _stringLiteral12F5EB18DE661BBE815BE7C12357AB74AFA912BC, L_9, NULL);
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(L_10, NULL);
		// string byteString = "";
		V_1 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// foreach (byte b in bytes)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11 = V_0;
		V_2 = L_11;
		V_3 = 0;
		goto IL_0067;
	}

IL_0046:
	{
		// foreach (byte b in bytes)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_12 = V_2;
		int32_t L_13 = V_3;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		// byteString += string.Format ("{0:X2}", b);
		String_t* L_16 = V_1;
		uint8_t L_17 = V_4;
		uint8_t L_18 = L_17;
		RuntimeObject* L_19 = Box(Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, &L_18);
		String_t* L_20;
		L_20 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteralDD638980A42773DBA4D111CE8D3979093BAC27E5, L_19, NULL);
		String_t* L_21;
		L_21 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_16, L_20, NULL);
		V_1 = L_21;
		int32_t L_22 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_22, 1));
	}

IL_0067:
	{
		// foreach (byte b in bytes)
		int32_t L_23 = V_3;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_24 = V_2;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))))
		{
			goto IL_0046;
		}
	}
	{
		// BluetoothLEHardwareInterface.Log (byteString);
		String_t* L_25 = V_1;
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(L_25, NULL);
		// if (DidUpdateCharacteristicValueAction != null && DidUpdateCharacteristicValueAction.ContainsKey (deviceAddress))
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_26 = __this->___DidUpdateCharacteristicValueAction_24;
		if (!L_26)
		{
			goto IL_00bc;
		}
	}
	{
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_27 = __this->___DidUpdateCharacteristicValueAction_24;
		String_t* L_28 = ___deviceAddress0;
		NullCheck(L_27);
		bool L_29;
		L_29 = Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE(L_27, L_28, Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var);
		if (!L_29)
		{
			goto IL_00bc;
		}
	}
	{
		// var characteristicAction = DidUpdateCharacteristicValueAction[deviceAddress];
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_30 = __this->___DidUpdateCharacteristicValueAction_24;
		String_t* L_31 = ___deviceAddress0;
		NullCheck(L_30);
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_32;
		L_32 = Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0(L_30, L_31, Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var);
		V_5 = L_32;
		// if (characteristicAction != null && characteristicAction.ContainsKey (characteristic))
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_33 = V_5;
		if (!L_33)
		{
			goto IL_00bc;
		}
	}
	{
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_34 = V_5;
		String_t* L_35 = ___characteristic1;
		NullCheck(L_34);
		bool L_36;
		L_36 = Dictionary_2_ContainsKey_m4A499F6EAC71DC55F8B1A7DF3EA4AE5FCDFE83F1(L_34, L_35, Dictionary_2_ContainsKey_m4A499F6EAC71DC55F8B1A7DF3EA4AE5FCDFE83F1_RuntimeMethod_var);
		if (!L_36)
		{
			goto IL_00bc;
		}
	}
	{
		// var action = characteristicAction[characteristic];
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_37 = V_5;
		String_t* L_38 = ___characteristic1;
		NullCheck(L_37);
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_39;
		L_39 = Dictionary_2_get_Item_mC10FD3398D9C91885BEE2ED01F03E6EA8F2458DF(L_37, L_38, Dictionary_2_get_Item_mC10FD3398D9C91885BEE2ED01F03E6EA8F2458DF_RuntimeMethod_var);
		V_6 = L_39;
		// if (action != null)
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_40 = V_6;
		if (!L_40)
		{
			goto IL_00bc;
		}
	}
	{
		// action (characteristic, bytes);
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_41 = V_6;
		String_t* L_42 = ___characteristic1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_43 = V_0;
		NullCheck(L_41);
		Action_2_Invoke_m30A65A90206175F850BCF9448E5227069C5FC9CF_inline(L_41, L_42, L_43, NULL);
	}

IL_00bc:
	{
		// if (DidUpdateCharacteristicValueWithDeviceAddressAction != null && DidUpdateCharacteristicValueWithDeviceAddressAction.ContainsKey (deviceAddress))
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_44 = __this->___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
		if (!L_44)
		{
			goto IL_0106;
		}
	}
	{
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_45 = __this->___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
		String_t* L_46 = ___deviceAddress0;
		NullCheck(L_45);
		bool L_47;
		L_47 = Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0(L_45, L_46, Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0_RuntimeMethod_var);
		if (!L_47)
		{
			goto IL_0106;
		}
	}
	{
		// var characteristicAction = DidUpdateCharacteristicValueWithDeviceAddressAction[deviceAddress];
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_48 = __this->___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
		String_t* L_49 = ___deviceAddress0;
		NullCheck(L_48);
		Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* L_50;
		L_50 = Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2(L_48, L_49, Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2_RuntimeMethod_var);
		V_7 = L_50;
		// if (characteristicAction != null && characteristicAction.ContainsKey (characteristic))
		Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* L_51 = V_7;
		if (!L_51)
		{
			goto IL_0106;
		}
	}
	{
		Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* L_52 = V_7;
		String_t* L_53 = ___characteristic1;
		NullCheck(L_52);
		bool L_54;
		L_54 = Dictionary_2_ContainsKey_m49883733B2BD00869A51867DF487463D793B72AE(L_52, L_53, Dictionary_2_ContainsKey_m49883733B2BD00869A51867DF487463D793B72AE_RuntimeMethod_var);
		if (!L_54)
		{
			goto IL_0106;
		}
	}
	{
		// var action = characteristicAction[characteristic];
		Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* L_55 = V_7;
		String_t* L_56 = ___characteristic1;
		NullCheck(L_55);
		Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* L_57;
		L_57 = Dictionary_2_get_Item_m29D67E65079024F5C041D7F9AA960C3FB8C61727(L_55, L_56, Dictionary_2_get_Item_m29D67E65079024F5C041D7F9AA960C3FB8C61727_RuntimeMethod_var);
		V_8 = L_57;
		// if (action != null)
		Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* L_58 = V_8;
		if (!L_58)
		{
			goto IL_0106;
		}
	}
	{
		// action (deviceAddress, characteristic, bytes);
		Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* L_59 = V_8;
		String_t* L_60 = ___deviceAddress0;
		String_t* L_61 = ___characteristic1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_62 = V_0;
		NullCheck(L_59);
		Action_3_Invoke_m621E4DDF08B7AB38B686646F5E5821CBB4B85A14_inline(L_59, L_60, L_61, L_62, NULL);
	}

IL_0106:
	{
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::OnPeripheralData(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_OnPeripheralData_mD3F92D2EA496B24CFB212E6D72E2C6BB0891CE96 (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, String_t* ___characteristic0, String_t* ___base64Data1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8A49BFAF7682C736E77C5743933C2176CF14A7B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD638980A42773DBA4D111CE8D3979093BAC27E5);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	String_t* V_1 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_2 = NULL;
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	{
		// if (base64Data != null)
		String_t* L_0 = ___base64Data1;
		if (!L_0)
		{
			goto IL_006c;
		}
	}
	{
		// byte[] bytes = System.Convert.FromBase64String (base64Data);
		String_t* L_1 = ___base64Data1;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2;
		L_2 = Convert_FromBase64String_m421F8600CA5124E047E3D7C2BC1B653F67BC48A1(L_1, NULL);
		V_0 = L_2;
		// if (bytes.Length > 0)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = V_0;
		NullCheck(L_3);
		if (!(((RuntimeArray*)L_3)->max_length))
		{
			goto IL_006c;
		}
	}
	{
		// BluetoothLEHardwareInterface.Log ("Peripheral Received: " + characteristic);
		String_t* L_4 = ___characteristic0;
		String_t* L_5;
		L_5 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralC8A49BFAF7682C736E77C5743933C2176CF14A7B, L_4, NULL);
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(L_5, NULL);
		// string byteString = "";
		V_1 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// foreach (byte b in bytes)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_6 = V_0;
		V_2 = L_6;
		V_3 = 0;
		goto IL_004b;
	}

IL_002a:
	{
		// foreach (byte b in bytes)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_4 = L_10;
		// byteString += string.Format ("{0:X2}", b);
		String_t* L_11 = V_1;
		uint8_t L_12 = V_4;
		uint8_t L_13 = L_12;
		RuntimeObject* L_14 = Box(Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15;
		L_15 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteralDD638980A42773DBA4D111CE8D3979093BAC27E5, L_14, NULL);
		String_t* L_16;
		L_16 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_11, L_15, NULL);
		V_1 = L_16;
		int32_t L_17 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_17, 1));
	}

IL_004b:
	{
		// foreach (byte b in bytes)
		int32_t L_18 = V_3;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_19 = V_2;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		// BluetoothLEHardwareInterface.Log (byteString);
		String_t* L_20 = V_1;
		BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385(L_20, NULL);
		// if (PeripheralReceivedWriteDataAction != null)
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_21 = __this->___PeripheralReceivedWriteDataAction_15;
		if (!L_21)
		{
			goto IL_006c;
		}
	}
	{
		// PeripheralReceivedWriteDataAction (characteristic, bytes);
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_22 = __this->___PeripheralReceivedWriteDataAction_15;
		String_t* L_23 = ___characteristic0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_24 = V_0;
		NullCheck(L_22);
		Action_2_Invoke_m30A65A90206175F850BCF9448E5227069C5FC9CF_inline(L_22, L_23, L_24, NULL);
	}

IL_006c:
	{
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::IncludeCoreLocationFramework()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript_IncludeCoreLocationFramework_m1DEAEB63131432C58B7C3DA578919DC88A77A9DA (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, const RuntimeMethod* method) 
{
	{
		// Input.location.Stop ();
		LocationService_tF2F2720FE2C07562EBFD128889F9A99F4B41B1B2* L_0;
		L_0 = Input_get_location_m6F753D9369213F07EC556FF6240D723DCF3C689D(NULL);
		NullCheck(L_0);
		LocationService_Stop_mB9332CB653E7A7CE6AE07240EA6C0B6C9AEC0D96(L_0, NULL);
		// }
		return;
	}
}
// System.Void BluetoothDeviceScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothDeviceScript__ctor_m5F849731E2A15C2B4581BAD3E9F949809793E373 (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLELog(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLELog_m55C7594137F4E0415929262CD5626CFC644C5BF0 (String_t* ___message0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLELog)(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEInitialize(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEInitialize_m0C6993C007FEB26D51E76381D1E0D3D36543E758 (bool ___asCentral0, bool ___asPeripheral1, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEInitialize)(static_cast<int32_t>(___asCentral0), static_cast<int32_t>(___asPeripheral1));

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDeInitialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEDeInitialize_mC6C9DF17BC7C3565A9D0A69FDBCAE93DB2560A0F (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEDeInitialize)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPauseMessages(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEPauseMessages_mB6C3F788494EB2DF55240EBB50FE0F45BA1DD5E6 (bool ___isPaused0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEPauseMessages)(static_cast<int32_t>(___isPaused0));

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEScanForPeripheralsWithServices(System.String,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEScanForPeripheralsWithServices_m9DD8A9BB753B1FD0737421D28522492934767A48 (String_t* ___serviceUUIDsString0, bool ___allowDuplicates1, bool ___rssiOnly2, bool ___clearPeripheralList3, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, int32_t);

	// Marshaling of parameter '___serviceUUIDsString0' to native representation
	char* ____serviceUUIDsString0_marshaled = NULL;
	____serviceUUIDsString0_marshaled = il2cpp_codegen_marshal_string(___serviceUUIDsString0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEScanForPeripheralsWithServices)(____serviceUUIDsString0_marshaled, static_cast<int32_t>(___allowDuplicates1), static_cast<int32_t>(___rssiOnly2), static_cast<int32_t>(___clearPeripheralList3));

	// Marshaling cleanup of parameter '___serviceUUIDsString0' native representation
	il2cpp_codegen_marshal_free(____serviceUUIDsString0_marshaled);
	____serviceUUIDsString0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERetrieveListOfPeripheralsWithServices(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERetrieveListOfPeripheralsWithServices_m1DD242C933E0288D7C769F4BB18A6F95942D5F3D (String_t* ___serviceUUIDsString0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___serviceUUIDsString0' to native representation
	char* ____serviceUUIDsString0_marshaled = NULL;
	____serviceUUIDsString0_marshaled = il2cpp_codegen_marshal_string(___serviceUUIDsString0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERetrieveListOfPeripheralsWithServices)(____serviceUUIDsString0_marshaled);

	// Marshaling cleanup of parameter '___serviceUUIDsString0' native representation
	il2cpp_codegen_marshal_free(____serviceUUIDsString0_marshaled);
	____serviceUUIDsString0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopScan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStopScan_m0F53F04F9967FFF7E6ABDF404FFFB5E08B00C89E (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStopScan)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEConnectToPeripheral(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEConnectToPeripheral_m8555AF34E8007C9C461C26E96FAC539B9BA1DBE8 (String_t* ___name0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEConnectToPeripheral)(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDisconnectPeripheral(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectPeripheral_mF6C5C73D59330F45513B8E59CE56BEF79FAB0E0F (String_t* ___name0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEDisconnectPeripheral)(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEReadCharacteristic(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEReadCharacteristic_mEAD0320BAA7DBB9CCAC2BE8BC0016AE071CEE7AE (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEReadCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEWriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEWriteCharacteristic_mCF6B7042D85D3FB2D4D6F60E21A51C15949A50DC (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data3, int32_t ___length4, bool ___withResponse5, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, uint8_t*, int32_t, int32_t);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Marshaling of parameter '___data3' to native representation
	uint8_t* ____data3_marshaled = NULL;
	if (___data3 != NULL)
	{
		____data3_marshaled = reinterpret_cast<uint8_t*>((___data3)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEWriteCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled, ____data3_marshaled, ___length4, static_cast<int32_t>(___withResponse5));

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLESubscribeCharacteristic(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m271256C090D07D968CB4EB1BD3DBAFDFF5940DB4 (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLESubscribeCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUnSubscribeCharacteristic(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEUnSubscribeCharacteristic_m0680535F4C5EFB91FA7EC74B3DE95A79131BAE2B (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEUnSubscribeCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDisconnectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectAll_m22125A7EDCFDFA19C968485496521BF28E6549B7 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEDisconnectAll)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEScanForBeacons(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEScanForBeacons_m2922203625442A6519864551FFA0FEF77A2A9528 (String_t* ___proximityUUIDsString0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___proximityUUIDsString0' to native representation
	char* ____proximityUUIDsString0_marshaled = NULL;
	____proximityUUIDsString0_marshaled = il2cpp_codegen_marshal_string(___proximityUUIDsString0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEScanForBeacons)(____proximityUUIDsString0_marshaled);

	// Marshaling cleanup of parameter '___proximityUUIDsString0' native representation
	il2cpp_codegen_marshal_free(____proximityUUIDsString0_marshaled);
	____proximityUUIDsString0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopBeaconScan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStopBeaconScan_mF48DC11DEF9AFABA6096F66487E3656AC4847775 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStopBeaconScan)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPeripheralName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEPeripheralName_mDAC39AB7B05C695755E524F11FB8ABF413AC073A (String_t* ___newName0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___newName0' to native representation
	char* ____newName0_marshaled = NULL;
	____newName0_marshaled = il2cpp_codegen_marshal_string(___newName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEPeripheralName)(____newName0_marshaled);

	// Marshaling cleanup of parameter '___newName0' native representation
	il2cpp_codegen_marshal_free(____newName0_marshaled);
	____newName0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateService(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLECreateService_m128948471801B37A8AF1327C7DE7DD4049C8FAD3 (String_t* ___uuid0, bool ___primary1, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLECreateService)(____uuid0_marshaled, static_cast<int32_t>(___primary1));

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveService(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveService_mC1879AF8FA6266EBFADD279369F31DD19DD90682 (String_t* ___uuid0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveService)(____uuid0_marshaled);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveServices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveServices_mBEB8BC1047FFB2F99B2B69C53D073A69142105CE (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveServices)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateCharacteristic(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLECreateCharacteristic_m9403BFAC8E9864142CF2CE5DE7F9916C16732457 (String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data3, int32_t ___length4, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, uint8_t*, int32_t);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Marshaling of parameter '___data3' to native representation
	uint8_t* ____data3_marshaled = NULL;
	if (___data3 != NULL)
	{
		____data3_marshaled = reinterpret_cast<uint8_t*>((___data3)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLECreateCharacteristic)(____uuid0_marshaled, ___properties1, ___permissions2, ____data3_marshaled, ___length4);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristic(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristic_m3AC1CA3579C1BB514EFD41292D6B811F0A0339A6 (String_t* ___uuid0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveCharacteristic)(____uuid0_marshaled);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristics_m9C1A7AA96B7FC791B0B036340C8261F8A311E47E (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveCharacteristics)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStartAdvertising()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStartAdvertising_mE27FFB143EA6F48390E94F6F40CF828340164628 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStartAdvertising)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopAdvertising()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEStopAdvertising_mE457E3D9C37A42A70D12591601A7FE8FC4EA9488 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStopAdvertising)();

}
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__iOSBluetoothLEUpdateCharacteristicValue_m19DBF301078337BBDCA08119260575BD24EE1767 (String_t* ___uuid0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data1, int32_t ___length2, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Marshaling of parameter '___data1' to native representation
	uint8_t* ____data1_marshaled = NULL;
	if (___data1 != NULL)
	{
		____data1_marshaled = reinterpret_cast<uint8_t*>((___data1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEUpdateCharacteristicValue)(____uuid0_marshaled, ____data1_marshaled, ___length2);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::Log(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_Log_mE8BD6E73FB65405834DBC8CADB78FD01B1C67385 (String_t* ___message0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		// _iOSBluetoothLELog (message);
		String_t* L_1 = ___message0;
		BluetoothLEHardwareInterface__iOSBluetoothLELog_m55C7594137F4E0415929262CD5626CFC644C5BF0(L_1, NULL);
	}

IL_000d:
	{
		// }
		return;
	}
}
// BluetoothDeviceScript BluetoothLEHardwareInterface::Initialize(System.Boolean,System.Boolean,System.Action,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* BluetoothLEHardwareInterface_Initialize_m7062917740E1AD537565200E86142950E763E32E (bool ___asCentral0, bool ___asPeripheral1, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action2, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___errorAction3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDF56051C4B061E3956A03F35FC9F34BEB3280610_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDBD85FE90312A38DA98B298A2B6052631AE960F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral22CA8A8DED4214E6F20217D70C471C5D80B2E4B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EB291071F866E890E2ACF69DE89CBBA36BD00B8);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_0 = NULL;
	{
		// bluetoothDeviceScript = null;
		((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0 = (BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0), (void*)(BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39*)NULL);
		// GameObject bluetoothLEReceiver = GameObject.Find("BluetoothLEReceiver");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral9EB291071F866E890E2ACF69DE89CBBA36BD00B8, NULL);
		V_0 = L_0;
		// if (bluetoothLEReceiver == null)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		// bluetoothLEReceiver = new GameObject ("BluetoothLEReceiver");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)il2cpp_codegen_object_new(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		GameObject__ctor_m37D512B05D292F954792225E6C6EEE95293A9B88(L_3, _stringLiteral9EB291071F866E890E2ACF69DE89CBBA36BD00B8, NULL);
		V_0 = L_3;
	}

IL_0025:
	{
		// if (bluetoothLEReceiver != null)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_0074;
		}
	}
	{
		// bluetoothDeviceScript = bluetoothLEReceiver.GetComponent<BluetoothDeviceScript> ();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = V_0;
		NullCheck(L_6);
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_7;
		L_7 = GameObject_GetComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDBD85FE90312A38DA98B298A2B6052631AE960F6(L_6, GameObject_GetComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDBD85FE90312A38DA98B298A2B6052631AE960F6_RuntimeMethod_var);
		((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0), (void*)L_7);
		// if (bluetoothDeviceScript == null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_8 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_8, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_9)
		{
			goto IL_0051;
		}
	}
	{
		// bluetoothDeviceScript = bluetoothLEReceiver.AddComponent<BluetoothDeviceScript> ();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = V_0;
		NullCheck(L_10);
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_11;
		L_11 = GameObject_AddComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDF56051C4B061E3956A03F35FC9F34BEB3280610(L_10, GameObject_AddComponent_TisBluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39_mDF56051C4B061E3956A03F35FC9F34BEB3280610_RuntimeMethod_var);
		((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0), (void*)L_11);
	}

IL_0051:
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_12 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_12, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		// bluetoothDeviceScript.InitializedAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_14 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_15 = ___action2;
		NullCheck(L_14);
		L_14->___InitializedAction_5 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&L_14->___InitializedAction_5), (void*)L_15);
		// bluetoothDeviceScript.ErrorAction = errorAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_16 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_17 = ___errorAction3;
		NullCheck(L_16);
		L_16->___ErrorAction_7 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&L_16->___ErrorAction_7), (void*)L_17);
	}

IL_0074:
	{
		// GameObject.DontDestroyOnLoad (bluetoothLEReceiver);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m303AA1C4DC810349F285B4809E426CBBA8F834F9(L_18, NULL);
		// if (Application.isEditor)
		bool L_19;
		L_19 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (!L_19)
		{
			goto IL_00a4;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_20 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_21;
		L_21 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_20, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_21)
		{
			goto IL_00ab;
		}
	}
	{
		// bluetoothDeviceScript.SendMessage ("OnBluetoothMessage", "Initialized");
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_22 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_22);
		Component_SendMessage_mA1D80D8BB7836EADB7799CAE71F10710298F4CDB(L_22, _stringLiteral22CA8A8DED4214E6F20217D70C471C5D80B2E4B6, _stringLiteral12DB585CD7C58149D9E30B1F88C95AA55FCFC680, NULL);
		goto IL_00ab;
	}

IL_00a4:
	{
		// _iOSBluetoothLEInitialize (asCentral, asPeripheral);
		bool L_23 = ___asCentral0;
		bool L_24 = ___asPeripheral1;
		BluetoothLEHardwareInterface__iOSBluetoothLEInitialize_m0C6993C007FEB26D51E76381D1E0D3D36543E758(L_23, L_24, NULL);
	}

IL_00ab:
	{
		// return bluetoothDeviceScript;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_25 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		return L_25;
	}
}
// System.Void BluetoothLEHardwareInterface::DeInitialize(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_DeInitialize_mE5681E45C181D4467C56D8D7479041531106DEA9 (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral22CA8A8DED4214E6F20217D70C471C5D80B2E4B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_0 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// bluetoothDeviceScript.DeinitializedAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_2 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_3 = ___action0;
		NullCheck(L_2);
		L_2->___DeinitializedAction_6 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_2->___DeinitializedAction_6), (void*)L_3);
	}

IL_0018:
	{
		// if (Application.isEditor)
		bool L_4;
		L_4 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_5 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_5, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		// bluetoothDeviceScript.SendMessage ("OnBluetoothMessage", "DeInitialized");
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_7 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_7);
		Component_SendMessage_mA1D80D8BB7836EADB7799CAE71F10710298F4CDB(L_7, _stringLiteral22CA8A8DED4214E6F20217D70C471C5D80B2E4B6, _stringLiteral2C05153E5BC0D6FFB349C1B45AB8FDAE44F99415, NULL);
		return;
	}

IL_0041:
	{
		// _iOSBluetoothLEDeInitialize ();
		BluetoothLEHardwareInterface__iOSBluetoothLEDeInitialize_mC6C9DF17BC7C3565A9D0A69FDBCAE93DB2560A0F(NULL);
	}

IL_0046:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::FinishDeInitialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_FinishDeInitialize_m4E276D6FFA746F913E33B5274891788DD55A017E (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EB291071F866E890E2ACF69DE89CBBA36BD00B8);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_0 = NULL;
	{
		// GameObject bluetoothLEReceiver = GameObject.Find("BluetoothLEReceiver");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral9EB291071F866E890E2ACF69DE89CBBA36BD00B8, NULL);
		V_0 = L_0;
		// if (bluetoothLEReceiver != null)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		// GameObject.Destroy(bluetoothLEReceiver);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_3, NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::BluetoothEnable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_BluetoothEnable_mED0F4C98CF496AEB5B64E76CE962350901F9ECC9 (bool ___enable0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::BluetoothScanMode(BluetoothLEHardwareInterface/ScanMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_BluetoothScanMode_m7E1DB9699B060B717BE2AF8D94A3F635B9453AD1 (int32_t ___scanMode0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::BluetoothConnectionPriority(BluetoothLEHardwareInterface/ConnectionPriority)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_BluetoothConnectionPriority_m51B44DA83D16CE60E5FC91742821CFFC5B9C8455 (int32_t ___connectionPriority0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::PauseMessages(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_PauseMessages_mBDCE4B50AEF019D6736CAC510217ACB4AD0510B9 (bool ___isPaused0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		// _iOSBluetoothLEPauseMessages (isPaused);
		bool L_1 = ___isPaused0;
		BluetoothLEHardwareInterface__iOSBluetoothLEPauseMessages_mB6C3F788494EB2DF55240EBB50FE0F45BA1DD5E6(L_1, NULL);
	}

IL_000d:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ScanForBeacons(System.String[],System.Action`1<BluetoothLEHardwareInterface/iBeaconData>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_ScanForBeacons_m8CD0D88AE9121D7E82420AC0F5E5550037BE854F (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___proximityUUIDs0, Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* ___actionBeaconResponse1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		// if (proximityUUIDs != null && proximityUUIDs.Length >= 0)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = ___proximityUUIDs0;
		if (!L_0)
		{
			goto IL_006e;
		}
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = ___proximityUUIDs0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))) < ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		// if (!Application.isEditor)
		bool L_2;
		L_2 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_2)
		{
			goto IL_006e;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		// bluetoothDeviceScript.DiscoveredBeaconAction = actionBeaconResponse;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_5 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* L_6 = ___actionBeaconResponse1;
		NullCheck(L_5);
		L_5->___DiscoveredBeaconAction_13 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___DiscoveredBeaconAction_13), (void*)L_6);
	}

IL_0028:
	{
		// string proximityUUIDsString = null;
		V_0 = (String_t*)NULL;
		// if (proximityUUIDs != null && proximityUUIDs.Length > 0)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = ___proximityUUIDs0;
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = ___proximityUUIDs0;
		NullCheck(L_8);
		if (!(((RuntimeArray*)L_8)->max_length))
		{
			goto IL_0068;
		}
	}
	{
		// proximityUUIDsString = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// foreach (string proximityUUID in proximityUUIDs)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = ___proximityUUIDs0;
		V_1 = L_9;
		V_2 = 0;
		goto IL_0052;
	}

IL_003d:
	{
		// foreach (string proximityUUID in proximityUUIDs)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_3 = L_13;
		// proximityUUIDsString += proximityUUID + "|";
		String_t* L_14 = V_0;
		String_t* L_15 = V_3;
		String_t* L_16;
		L_16 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_14, L_15, _stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827, NULL);
		V_0 = L_16;
		int32_t L_17 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_17, 1));
	}

IL_0052:
	{
		// foreach (string proximityUUID in proximityUUIDs)
		int32_t L_18 = V_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = V_1;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length)))))
		{
			goto IL_003d;
		}
	}
	{
		// proximityUUIDsString = proximityUUIDsString.Substring (0, proximityUUIDsString.Length - 1);
		String_t* L_20 = V_0;
		String_t* L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22;
		L_22 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_21, NULL);
		NullCheck(L_20);
		String_t* L_23;
		L_23 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_20, 0, ((int32_t)il2cpp_codegen_subtract(L_22, 1)), NULL);
		V_0 = L_23;
	}

IL_0068:
	{
		// _iOSBluetoothLEScanForBeacons (proximityUUIDsString);
		String_t* L_24 = V_0;
		BluetoothLEHardwareInterface__iOSBluetoothLEScanForBeacons_m2922203625442A6519864551FFA0FEF77A2A9528(L_24, NULL);
	}

IL_006e:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ScanForPeripheralsWithServices(System.String[],System.Action`2<System.String,System.String>,System.Action`4<System.String,System.String,System.Int32,System.Byte[]>,System.Boolean,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_ScanForPeripheralsWithServices_m6D1E754D07E052DCD46116963B1B63A69374987D (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___serviceUUIDs0, Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___action1, Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* ___actionAdvertisingInfo2, bool ___rssiOnly3, bool ___clearPeripheralList4, int32_t ___recordType5, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0095;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		// bluetoothDeviceScript.DiscoveredPeripheralAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_4 = ___action1;
		NullCheck(L_3);
		L_3->___DiscoveredPeripheralAction_11 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___DiscoveredPeripheralAction_11), (void*)L_4);
		// bluetoothDeviceScript.DiscoveredPeripheralWithAdvertisingInfoAction = actionAdvertisingInfo;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_5 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* L_6 = ___actionAdvertisingInfo2;
		NullCheck(L_5);
		L_5->___DiscoveredPeripheralWithAdvertisingInfoAction_12 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___DiscoveredPeripheralWithAdvertisingInfoAction_12), (void*)L_6);
		// if (bluetoothDeviceScript.DiscoveredDeviceList != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_7 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_7);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8 = L_7->___DiscoveredDeviceList_4;
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		// bluetoothDeviceScript.DiscoveredDeviceList.Clear ();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_9 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_9);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_10 = L_9->___DiscoveredDeviceList_4;
		NullCheck(L_10);
		List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_10, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
	}

IL_0048:
	{
		// string serviceUUIDsString = null;
		V_0 = (String_t*)NULL;
		// if (serviceUUIDs != null && serviceUUIDs.Length > 0)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = ___serviceUUIDs0;
		if (!L_11)
		{
			goto IL_0088;
		}
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = ___serviceUUIDs0;
		NullCheck(L_12);
		if (!(((RuntimeArray*)L_12)->max_length))
		{
			goto IL_0088;
		}
	}
	{
		// serviceUUIDsString = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// foreach (string serviceUUID in serviceUUIDs)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = ___serviceUUIDs0;
		V_1 = L_13;
		V_2 = 0;
		goto IL_0072;
	}

IL_005d:
	{
		// foreach (string serviceUUID in serviceUUIDs)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		String_t* L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_3 = L_17;
		// serviceUUIDsString += serviceUUID + "|";
		String_t* L_18 = V_0;
		String_t* L_19 = V_3;
		String_t* L_20;
		L_20 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_18, L_19, _stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827, NULL);
		V_0 = L_20;
		int32_t L_21 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_21, 1));
	}

IL_0072:
	{
		// foreach (string serviceUUID in serviceUUIDs)
		int32_t L_22 = V_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = V_1;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length)))))
		{
			goto IL_005d;
		}
	}
	{
		// serviceUUIDsString = serviceUUIDsString.Substring (0, serviceUUIDsString.Length - 1);
		String_t* L_24 = V_0;
		String_t* L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26;
		L_26 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_25, NULL);
		NullCheck(L_24);
		String_t* L_27;
		L_27 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_24, 0, ((int32_t)il2cpp_codegen_subtract(L_26, 1)), NULL);
		V_0 = L_27;
	}

IL_0088:
	{
		// _iOSBluetoothLEScanForPeripheralsWithServices (serviceUUIDsString, (actionAdvertisingInfo != null), rssiOnly, clearPeripheralList);
		String_t* L_28 = V_0;
		Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D* L_29 = ___actionAdvertisingInfo2;
		bool L_30 = ___rssiOnly3;
		bool L_31 = ___clearPeripheralList4;
		BluetoothLEHardwareInterface__iOSBluetoothLEScanForPeripheralsWithServices_m9DD8A9BB753B1FD0737421D28522492934767A48(L_28, (bool)((!(((RuntimeObject*)(Action_4_t2EE4CD6F8DD9CA2246E15DED8A5F3C473FF68E1D*)L_29) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0), L_30, L_31, NULL);
	}

IL_0095:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RetrieveListOfPeripheralsWithServices(System.String[],System.Action`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_RetrieveListOfPeripheralsWithServices_mCE991C4BD0394988533925B991FA29442D71A45C (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___serviceUUIDs0, Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___action1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* G_B7_0 = NULL;
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_007e;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		// bluetoothDeviceScript.RetrievedConnectedPeripheralAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_4 = ___action1;
		NullCheck(L_3);
		L_3->___RetrievedConnectedPeripheralAction_14 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___RetrievedConnectedPeripheralAction_14), (void*)L_4);
		// if (bluetoothDeviceScript.DiscoveredDeviceList != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_5 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_5);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = L_5->___DiscoveredDeviceList_4;
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		// bluetoothDeviceScript.DiscoveredDeviceList.Clear ();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_7 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_7);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8 = L_7->___DiscoveredDeviceList_4;
		NullCheck(L_8);
		List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_8, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
	}

IL_003a:
	{
		// string serviceUUIDsString = serviceUUIDs.Length > 0 ? "" : null;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = ___serviceUUIDs0;
		NullCheck(L_9);
		if ((((RuntimeArray*)L_9)->max_length))
		{
			goto IL_0041;
		}
	}
	{
		G_B7_0 = ((String_t*)(NULL));
		goto IL_0046;
	}

IL_0041:
	{
		G_B7_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0046:
	{
		V_0 = G_B7_0;
		// foreach (string serviceUUID in serviceUUIDs)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10 = ___serviceUUIDs0;
		V_1 = L_10;
		V_2 = 0;
		goto IL_0062;
	}

IL_004d:
	{
		// foreach (string serviceUUID in serviceUUIDs)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = V_1;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_3 = L_14;
		// serviceUUIDsString += serviceUUID + "|";
		String_t* L_15 = V_0;
		String_t* L_16 = V_3;
		String_t* L_17;
		L_17 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_15, L_16, _stringLiteralA42779B09629BCE81B76EF626A57A0B40F2AD827, NULL);
		V_0 = L_17;
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_18, 1));
	}

IL_0062:
	{
		// foreach (string serviceUUID in serviceUUIDs)
		int32_t L_19 = V_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))
		{
			goto IL_004d;
		}
	}
	{
		// serviceUUIDsString = serviceUUIDsString.Substring (0, serviceUUIDsString.Length - 1);
		String_t* L_21 = V_0;
		String_t* L_22 = V_0;
		NullCheck(L_22);
		int32_t L_23;
		L_23 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_22, NULL);
		NullCheck(L_21);
		String_t* L_24;
		L_24 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_21, 0, ((int32_t)il2cpp_codegen_subtract(L_23, 1)), NULL);
		V_0 = L_24;
		// _iOSBluetoothLERetrieveListOfPeripheralsWithServices (serviceUUIDsString);
		String_t* L_25 = V_0;
		BluetoothLEHardwareInterface__iOSBluetoothLERetrieveListOfPeripheralsWithServices_m1DD242C933E0288D7C769F4BB18A6F95942D5F3D(L_25, NULL);
	}

IL_007e:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StopScan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_StopScan_m56E0C221751F793ED8426C95C37E9BDD0762998F (const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		// _iOSBluetoothLEStopScan ();
		BluetoothLEHardwareInterface__iOSBluetoothLEStopScan_m0F53F04F9967FFF7E6ABDF404FFFB5E08B00C89E(NULL);
	}

IL_000c:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StopBeaconScan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_StopBeaconScan_m971B492FFA716A78FEA769807D83AAE94A57EDEF (const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		// _iOSBluetoothLEStopBeaconScan ();
		BluetoothLEHardwareInterface__iOSBluetoothLEStopBeaconScan_mF48DC11DEF9AFABA6096F66487E3656AC4847775(NULL);
	}

IL_000c:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::DisconnectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_DisconnectAll_m54C4F5855C8086A47B4DB13AD47255CC666C94CF (const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		// _iOSBluetoothLEDisconnectAll ();
		BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectAll_m22125A7EDCFDFA19C968485496521BF28E6549B7(NULL);
	}

IL_000c:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ConnectToPeripheral(System.String,System.Action`1<System.String>,System.Action`2<System.String,System.String>,System.Action`3<System.String,System.String,System.String>,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_ConnectToPeripheral_mFAF4A4FAF9DE4631AB912747B9A2EB6BA7D3EC0B (String_t* ___name0, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___connectAction1, Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___serviceAction2, Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7* ___characteristicAction3, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___disconnectAction4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0047;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		// bluetoothDeviceScript.ConnectedPeripheralAction = connectAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = ___connectAction1;
		NullCheck(L_3);
		L_3->___ConnectedPeripheralAction_16 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___ConnectedPeripheralAction_16), (void*)L_4);
		// bluetoothDeviceScript.DiscoveredServiceAction = serviceAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_5 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_6 = ___serviceAction2;
		NullCheck(L_5);
		L_5->___DiscoveredServiceAction_19 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___DiscoveredServiceAction_19), (void*)L_6);
		// bluetoothDeviceScript.DiscoveredCharacteristicAction = characteristicAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_7 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_3_t9B83CE1387ECB52C4E519D213AC210F7946330F7* L_8 = ___characteristicAction3;
		NullCheck(L_7);
		L_7->___DiscoveredCharacteristicAction_20 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&L_7->___DiscoveredCharacteristicAction_20), (void*)L_8);
		// bluetoothDeviceScript.ConnectedDisconnectPeripheralAction = disconnectAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_9 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = ___disconnectAction4;
		NullCheck(L_9);
		L_9->___ConnectedDisconnectPeripheralAction_17 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&L_9->___ConnectedDisconnectPeripheralAction_17), (void*)L_10);
	}

IL_0041:
	{
		// _iOSBluetoothLEConnectToPeripheral (name);
		String_t* L_11 = ___name0;
		BluetoothLEHardwareInterface__iOSBluetoothLEConnectToPeripheral_m8555AF34E8007C9C461C26E96FAC539B9BA1DBE8(L_11, NULL);
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::DisconnectPeripheral(System.String,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_DisconnectPeripheral_mDABC20CB04ED6DCDF3676124B63DF34D7EF692D0 (String_t* ___name0, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___action1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// bluetoothDeviceScript.DisconnectedPeripheralAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = ___action1;
		NullCheck(L_3);
		L_3->___DisconnectedPeripheralAction_18 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___DisconnectedPeripheralAction_18), (void*)L_4);
	}

IL_001f:
	{
		// _iOSBluetoothLEDisconnectPeripheral (name);
		String_t* L_5 = ___name0;
		BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectPeripheral_mF6C5C73D59330F45513B8E59CE56BEF79FAB0E0F(L_5, NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ReadCharacteristic(System.String,System.String,System.String,System.Action`2<System.String,System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_ReadCharacteristic_m4EB7E31255248E088FC18BBF901681095C162F3C (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* ___action3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_005a;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0052;
		}
	}
	{
		// if (!bluetoothDeviceScript.DidUpdateCharacteristicValueAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_3);
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_4 = L_3->___DidUpdateCharacteristicValueAction_24;
		String_t* L_5 = ___name0;
		NullCheck(L_4);
		bool L_6;
		L_6 = Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE(L_4, L_5, Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateCharacteristicValueAction[name] = new Dictionary<string, Action<string, byte[]>>();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_7 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_7);
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_8 = L_7->___DidUpdateCharacteristicValueAction_24;
		String_t* L_9 = ___name0;
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_10 = (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*)il2cpp_codegen_object_new(Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD(L_10, Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD_RuntimeMethod_var);
		NullCheck(L_8);
		Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476(L_8, L_9, L_10, Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476_RuntimeMethod_var);
	}

IL_003b:
	{
		// bluetoothDeviceScript.DidUpdateCharacteristicValueAction [name] [characteristic] = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_11 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_11);
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_12 = L_11->___DidUpdateCharacteristicValueAction_24;
		String_t* L_13 = ___name0;
		NullCheck(L_12);
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_14;
		L_14 = Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0(L_12, L_13, Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var);
		String_t* L_15 = ___characteristic2;
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_16 = ___action3;
		NullCheck(L_14);
		Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2(L_14, L_15, L_16, Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2_RuntimeMethod_var);
	}

IL_0052:
	{
		// _iOSBluetoothLEReadCharacteristic (name, service, characteristic);
		String_t* L_17 = ___name0;
		String_t* L_18 = ___service1;
		String_t* L_19 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLEReadCharacteristic_mEAD0320BAA7DBB9CCAC2BE8BC0016AE071CEE7AE(L_17, L_18, L_19, NULL);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::WriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_WriteCharacteristic_m60A9318801C147A7EC79FF0C506E411AA0B56585 (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data3, int32_t ___length4, bool ___withResponse5, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___action6, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		// bluetoothDeviceScript.DidWriteCharacteristicAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = ___action6;
		NullCheck(L_3);
		L_3->___DidWriteCharacteristicAction_21 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___DidWriteCharacteristicAction_21), (void*)L_4);
	}

IL_0020:
	{
		// _iOSBluetoothLEWriteCharacteristic (name, service, characteristic, data, length, withResponse);
		String_t* L_5 = ___name0;
		String_t* L_6 = ___service1;
		String_t* L_7 = ___characteristic2;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_8 = ___data3;
		int32_t L_9 = ___length4;
		bool L_10 = ___withResponse5;
		BluetoothLEHardwareInterface__iOSBluetoothLEWriteCharacteristic_mCF6B7042D85D3FB2D4D6F60E21A51C15949A50DC(L_5, L_6, L_7, L_8, L_9, L_10, NULL);
	}

IL_002d:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::SubscribeCharacteristic(System.String,System.String,System.String,System.Action`1<System.String>,System.Action`2<System.String,System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_SubscribeCharacteristic_m6969EB33904897C298831B26FF6E05D3377E11DC (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___notificationAction3, Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* ___action4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_00b7;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_00af;
		}
	}
	{
		// name = name.ToUpper ();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_3, NULL);
		___name0 = L_4;
		// service = service.ToUpper ();
		String_t* L_5 = ___service1;
		NullCheck(L_5);
		String_t* L_6;
		L_6 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_5, NULL);
		___service1 = L_6;
		// characteristic = characteristic.ToUpper ();
		String_t* L_7 = ___characteristic2;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_7, NULL);
		___characteristic2 = L_8;
		// if (!bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_9 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_9);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_10 = L_9->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_11 = ___name0;
		NullCheck(L_10);
		bool L_12;
		L_12 = Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F(L_10, L_11, Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		if (L_12)
		{
			goto IL_0059;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction [name] = new Dictionary<string, Action<string>> ();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_13 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_13);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_14 = L_13->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_15 = ___name0;
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_16 = (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*)il2cpp_codegen_object_new(Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2(L_16, Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		NullCheck(L_14);
		Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706(L_14, L_15, L_16, Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var);
	}

IL_0059:
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction [name] [characteristic] = notificationAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_17 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_17);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_18 = L_17->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_19 = ___name0;
		NullCheck(L_18);
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_20;
		L_20 = Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61(L_18, L_19, Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		String_t* L_21 = ___characteristic2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_22 = ___notificationAction3;
		NullCheck(L_20);
		Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189(L_20, L_21, L_22, Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		// if (!bluetoothDeviceScript.DidUpdateCharacteristicValueAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_23 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_23);
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_24 = L_23->___DidUpdateCharacteristicValueAction_24;
		String_t* L_25 = ___name0;
		NullCheck(L_24);
		bool L_26;
		L_26 = Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE(L_24, L_25, Dictionary_2_ContainsKey_mD16546696C90538611676CE4D546FB23AE9B8FEE_RuntimeMethod_var);
		if (L_26)
		{
			goto IL_0097;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateCharacteristicValueAction [name] = new Dictionary<string, Action<string, byte[]>> ();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_27 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_27);
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_28 = L_27->___DidUpdateCharacteristicValueAction_24;
		String_t* L_29 = ___name0;
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_30 = (Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8*)il2cpp_codegen_object_new(Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8_il2cpp_TypeInfo_var);
		NullCheck(L_30);
		Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD(L_30, Dictionary_2__ctor_m33F7CC3A700785AE5317A03FF6119F01AB8C3CAD_RuntimeMethod_var);
		NullCheck(L_28);
		Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476(L_28, L_29, L_30, Dictionary_2_set_Item_m9801DB6B846090C50887E169823DB1E40E078476_RuntimeMethod_var);
	}

IL_0097:
	{
		// bluetoothDeviceScript.DidUpdateCharacteristicValueAction [name] [characteristic] = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_31 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_31);
		Dictionary_2_t3F93014C5C5D4552847D7AF33226D44359917A78* L_32 = L_31->___DidUpdateCharacteristicValueAction_24;
		String_t* L_33 = ___name0;
		NullCheck(L_32);
		Dictionary_2_t8743F7AD7AFB649C6C67382C6D22AC90DAE2B3D8* L_34;
		L_34 = Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0(L_32, L_33, Dictionary_2_get_Item_mA0D0E6E95D1A307A4F7FE00BCDC3392D537551F0_RuntimeMethod_var);
		String_t* L_35 = ___characteristic2;
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_36 = ___action4;
		NullCheck(L_34);
		Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2(L_34, L_35, L_36, Dictionary_2_set_Item_m5BC0806F55FF11B11C0CCA09E2B62F1F225721B2_RuntimeMethod_var);
	}

IL_00af:
	{
		// _iOSBluetoothLESubscribeCharacteristic (name, service, characteristic);
		String_t* L_37 = ___name0;
		String_t* L_38 = ___service1;
		String_t* L_39 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m271256C090D07D968CB4EB1BD3DBAFDFF5940DB4(L_37, L_38, L_39, NULL);
	}

IL_00b7:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::SubscribeCharacteristicWithDeviceAddress(System.String,System.String,System.String,System.Action`2<System.String,System.String>,System.Action`3<System.String,System.String,System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_SubscribeCharacteristicWithDeviceAddress_m2901F4ABF3852CB67100C6E4654BCCBA4AC9D3A6 (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* ___notificationAction3, Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* ___action4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m567D53C6E9424E1769E746B2F2F7CB666BD28746_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m317D3193C4CC26B216C7316AA83011B747927A26_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mD7531E55E7078F3CA3DB2492418BEADF7229D468_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_00f5;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_00ed;
		}
	}
	{
		// name = name.ToUpper ();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_3, NULL);
		___name0 = L_4;
		// service = service.ToUpper ();
		String_t* L_5 = ___service1;
		NullCheck(L_5);
		String_t* L_6;
		L_6 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_5, NULL);
		___service1 = L_6;
		// characteristic = characteristic.ToUpper ();
		String_t* L_7 = ___characteristic2;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_7, NULL);
		___characteristic2 = L_8;
		// if (!bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_9 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_9);
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_10 = L_9->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		String_t* L_11 = ___name0;
		NullCheck(L_10);
		bool L_12;
		L_12 = Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23(L_10, L_11, Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var);
		if (L_12)
		{
			goto IL_0059;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction[name] = new Dictionary<string, Action<string, string>>();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_13 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_13);
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_14 = L_13->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		String_t* L_15 = ___name0;
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_16 = (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*)il2cpp_codegen_object_new(Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11(L_16, Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11_RuntimeMethod_var);
		NullCheck(L_14);
		Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7(L_14, L_15, L_16, Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7_RuntimeMethod_var);
	}

IL_0059:
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction[name][characteristic] = notificationAction;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_17 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_17);
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_18 = L_17->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		String_t* L_19 = ___name0;
		NullCheck(L_18);
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_20;
		L_20 = Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF(L_18, L_19, Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var);
		String_t* L_21 = ___characteristic2;
		Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D* L_22 = ___notificationAction3;
		NullCheck(L_20);
		Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87(L_20, L_21, L_22, Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87_RuntimeMethod_var);
		// if (!bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_23 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_23);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_24 = L_23->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_25 = ___name0;
		NullCheck(L_24);
		bool L_26;
		L_26 = Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F(L_24, L_25, Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		if (L_26)
		{
			goto IL_0097;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction[name] = new Dictionary<string, Action<string>>();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_27 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_27);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_28 = L_27->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_29 = ___name0;
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_30 = (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*)il2cpp_codegen_object_new(Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		NullCheck(L_30);
		Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2(L_30, Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		NullCheck(L_28);
		Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706(L_28, L_29, L_30, Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var);
	}

IL_0097:
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction[name][characteristic] = null;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_31 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_31);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_32 = L_31->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_33 = ___name0;
		NullCheck(L_32);
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_34;
		L_34 = Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61(L_32, L_33, Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		String_t* L_35 = ___characteristic2;
		NullCheck(L_34);
		Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189(L_34, L_35, (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)NULL, Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		// if (!bluetoothDeviceScript.DidUpdateCharacteristicValueWithDeviceAddressAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_36 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_36);
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_37 = L_36->___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
		String_t* L_38 = ___name0;
		NullCheck(L_37);
		bool L_39;
		L_39 = Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0(L_37, L_38, Dictionary_2_ContainsKey_m150AA8E90D327107E170A339453F8681CBC34FD0_RuntimeMethod_var);
		if (L_39)
		{
			goto IL_00d5;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateCharacteristicValueWithDeviceAddressAction[name] = new Dictionary<string, Action<string, string, byte[]>>();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_40 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_40);
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_41 = L_40->___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
		String_t* L_42 = ___name0;
		Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* L_43 = (Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB*)il2cpp_codegen_object_new(Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB_il2cpp_TypeInfo_var);
		NullCheck(L_43);
		Dictionary_2__ctor_m567D53C6E9424E1769E746B2F2F7CB666BD28746(L_43, Dictionary_2__ctor_m567D53C6E9424E1769E746B2F2F7CB666BD28746_RuntimeMethod_var);
		NullCheck(L_41);
		Dictionary_2_set_Item_mD7531E55E7078F3CA3DB2492418BEADF7229D468(L_41, L_42, L_43, Dictionary_2_set_Item_mD7531E55E7078F3CA3DB2492418BEADF7229D468_RuntimeMethod_var);
	}

IL_00d5:
	{
		// bluetoothDeviceScript.DidUpdateCharacteristicValueWithDeviceAddressAction[name][characteristic] = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_44 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_44);
		Dictionary_2_t69776E38AD22E2FF77D6D297264AC70AA2A08883* L_45 = L_44->___DidUpdateCharacteristicValueWithDeviceAddressAction_25;
		String_t* L_46 = ___name0;
		NullCheck(L_45);
		Dictionary_2_t50080CCAA08CD4E9D7D9471EA121047E29FBD7AB* L_47;
		L_47 = Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2(L_45, L_46, Dictionary_2_get_Item_mB9B806A3CA27CFDDB280FFBBD54F692165DB5DE2_RuntimeMethod_var);
		String_t* L_48 = ___characteristic2;
		Action_3_t5A0962ABAB9B3F862F898284CDA0D4B7762D61DB* L_49 = ___action4;
		NullCheck(L_47);
		Dictionary_2_set_Item_m317D3193C4CC26B216C7316AA83011B747927A26(L_47, L_48, L_49, Dictionary_2_set_Item_m317D3193C4CC26B216C7316AA83011B747927A26_RuntimeMethod_var);
	}

IL_00ed:
	{
		// _iOSBluetoothLESubscribeCharacteristic (name, service, characteristic);
		String_t* L_50 = ___name0;
		String_t* L_51 = ___service1;
		String_t* L_52 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m271256C090D07D968CB4EB1BD3DBAFDFF5940DB4(L_50, L_51, L_52, NULL);
	}

IL_00f5:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::UnSubscribeCharacteristic(System.String,System.String,System.String,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_UnSubscribeCharacteristic_mD74FA8EBC972ADEE671AAE356F3B9E12EA839637 (String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___action3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_00b6;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_00ae;
		}
	}
	{
		// name = name.ToUpper ();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_3, NULL);
		___name0 = L_4;
		// service = service.ToUpper ();
		String_t* L_5 = ___service1;
		NullCheck(L_5);
		String_t* L_6;
		L_6 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_5, NULL);
		___service1 = L_6;
		// characteristic = characteristic.ToUpper ();
		String_t* L_7 = ___characteristic2;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = String_ToUpper_m5F499BC30C2A5F5C96248B4C3D1A3B4694748B49(L_7, NULL);
		___characteristic2 = L_8;
		// if (!bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_9 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_9);
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_10 = L_9->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		String_t* L_11 = ___name0;
		NullCheck(L_10);
		bool L_12;
		L_12 = Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23(L_10, L_11, Dictionary_2_ContainsKey_m50B2EE54AA6D9476F059F1EE328549BD78E6CC23_RuntimeMethod_var);
		if (L_12)
		{
			goto IL_0059;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction[name] = new Dictionary<string, Action<string, string>>();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_13 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_13);
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_14 = L_13->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		String_t* L_15 = ___name0;
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_16 = (Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6*)il2cpp_codegen_object_new(Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11(L_16, Dictionary_2__ctor_mA41215374A14C39327A0F3E564767C0E5F736E11_RuntimeMethod_var);
		NullCheck(L_14);
		Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7(L_14, L_15, L_16, Dictionary_2_set_Item_mC95C71C51EBCC5FB69D800D28A4DA517C27A77E7_RuntimeMethod_var);
	}

IL_0059:
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction[name][characteristic] = null;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_17 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_17);
		Dictionary_2_t1BB573A1F6F5D0C87BF64897F77E904846640D74* L_18 = L_17->___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_23;
		String_t* L_19 = ___name0;
		NullCheck(L_18);
		Dictionary_2_tAFFFC9BCDC0E8601FDB252CD80C438376B1177C6* L_20;
		L_20 = Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF(L_18, L_19, Dictionary_2_get_Item_mE49699F82AF7275CA25DC352FB9BCB00BCD229CF_RuntimeMethod_var);
		String_t* L_21 = ___characteristic2;
		NullCheck(L_20);
		Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87(L_20, L_21, (Action_2_t3EDD987DFCD31953576008A0D7D4F44D8C984B1D*)NULL, Dictionary_2_set_Item_m5C248D01E5A71B59F3D041ECB2766EF9A97F4E87_RuntimeMethod_var);
		// if (!bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction.ContainsKey (name))
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_22 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_22);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_23 = L_22->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_24 = ___name0;
		NullCheck(L_23);
		bool L_25;
		L_25 = Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F(L_23, L_24, Dictionary_2_ContainsKey_m21762A3A1079E3FEDE127462BFB85ABA3730694F_RuntimeMethod_var);
		if (L_25)
		{
			goto IL_0097;
		}
	}
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction[name] = new Dictionary<string, Action<string>> ();
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_26 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_26);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_27 = L_26->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_28 = ___name0;
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_29 = (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*)il2cpp_codegen_object_new(Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		NullCheck(L_29);
		Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2(L_29, Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		NullCheck(L_27);
		Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706(L_27, L_28, L_29, Dictionary_2_set_Item_mD9454082A26057918E2FF23A85B2DBC5791A5706_RuntimeMethod_var);
	}

IL_0097:
	{
		// bluetoothDeviceScript.DidUpdateNotificationStateForCharacteristicAction[name][characteristic] = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_30 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_30);
		Dictionary_2_t4C40EAC1FEB8449C06FF324574D50495496EB89D* L_31 = L_30->___DidUpdateNotificationStateForCharacteristicAction_22;
		String_t* L_32 = ___name0;
		NullCheck(L_31);
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_33;
		L_33 = Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61(L_31, L_32, Dictionary_2_get_Item_m09D1488785E808C7E32BB21E5AB3E7422F591D61_RuntimeMethod_var);
		String_t* L_34 = ___characteristic2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_35 = ___action3;
		NullCheck(L_33);
		Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189(L_33, L_34, L_35, Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
	}

IL_00ae:
	{
		// _iOSBluetoothLEUnSubscribeCharacteristic (name, service, characteristic);
		String_t* L_36 = ___name0;
		String_t* L_37 = ___service1;
		String_t* L_38 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLEUnSubscribeCharacteristic_m0680535F4C5EFB91FA7EC74B3DE95A79131BAE2B(L_36, L_37, L_38, NULL);
	}

IL_00b6:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::PeripheralName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_PeripheralName_m277F4322E7F32A295370EB8F6065FAECB20A6B51 (String_t* ___newName0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		// _iOSBluetoothLEPeripheralName (newName);
		String_t* L_1 = ___newName0;
		BluetoothLEHardwareInterface__iOSBluetoothLEPeripheralName_mDAC39AB7B05C695755E524F11FB8ABF413AC073A(L_1, NULL);
	}

IL_000d:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::CreateService(System.String,System.Boolean,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_CreateService_mAE63186C54F74F89E389F640CD03503C864A5D17 (String_t* ___uuid0, bool ___primary1, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___action2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// bluetoothDeviceScript.ServiceAddedAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = ___action2;
		NullCheck(L_3);
		L_3->___ServiceAddedAction_8 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___ServiceAddedAction_8), (void*)L_4);
	}

IL_001f:
	{
		// _iOSBluetoothLECreateService (uuid, primary);
		String_t* L_5 = ___uuid0;
		bool L_6 = ___primary1;
		BluetoothLEHardwareInterface__iOSBluetoothLECreateService_m128948471801B37A8AF1327C7DE7DD4049C8FAD3(L_5, L_6, NULL);
	}

IL_0026:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveService(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_RemoveService_m3A0E3287757FB4E10C86EAC611F1DBF6569CF1C5 (String_t* ___uuid0, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		// _iOSBluetoothLERemoveService (uuid);
		String_t* L_1 = ___uuid0;
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveService_mC1879AF8FA6266EBFADD279369F31DD19DD90682(L_1, NULL);
	}

IL_000d:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveServices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_RemoveServices_m9D72D31BFB0B6EEAE6A0E4938A144328808D6BA8 (const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		// _iOSBluetoothLERemoveServices ();
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveServices_mBEB8BC1047FFB2F99B2B69C53D073A69142105CE(NULL);
	}

IL_000c:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::CreateCharacteristic(System.String,BluetoothLEHardwareInterface/CBCharacteristicProperties,BluetoothLEHardwareInterface/CBAttributePermissions,System.Byte[],System.Int32,System.Action`2<System.String,System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_CreateCharacteristic_mF96C5F892393FD3B930FCA83702AC549EE7DC7E2 (String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data3, int32_t ___length4, Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* ___action5, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		// bluetoothDeviceScript.PeripheralReceivedWriteDataAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831* L_4 = ___action5;
		NullCheck(L_3);
		L_3->___PeripheralReceivedWriteDataAction_15 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___PeripheralReceivedWriteDataAction_15), (void*)L_4);
	}

IL_0020:
	{
		// _iOSBluetoothLECreateCharacteristic (uuid, (int)properties, (int)permissions, data, length);
		String_t* L_5 = ___uuid0;
		int32_t L_6 = ___properties1;
		int32_t L_7 = ___permissions2;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_8 = ___data3;
		int32_t L_9 = ___length4;
		BluetoothLEHardwareInterface__iOSBluetoothLECreateCharacteristic_m9403BFAC8E9864142CF2CE5DE7F9916C16732457(L_5, L_6, L_7, L_8, L_9, NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveCharacteristic(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_RemoveCharacteristic_m6FAC311598EEFBBBD927CA848C3A5C47F50508E4 (String_t* ___uuid0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// bluetoothDeviceScript.PeripheralReceivedWriteDataAction = null;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		NullCheck(L_3);
		L_3->___PeripheralReceivedWriteDataAction_15 = (Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___PeripheralReceivedWriteDataAction_15), (void*)(Action_2_t6167C7DD369F0ADA5FD8FB5C2476453CF404C831*)NULL);
	}

IL_001f:
	{
		// _iOSBluetoothLERemoveCharacteristic (uuid);
		String_t* L_4 = ___uuid0;
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristic_m3AC1CA3579C1BB514EFD41292D6B811F0A0339A6(L_4, NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveCharacteristics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_RemoveCharacteristics_m70A33AB4B06F3E9C0891A3E35D6077CAC32C64B2 (const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		// _iOSBluetoothLERemoveCharacteristics ();
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristics_m9C1A7AA96B7FC791B0B036340C8261F8A311E47E(NULL);
	}

IL_000c:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StartAdvertising(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_StartAdvertising_mD2DA07943EDFE33EC208F564D7209C2C3D341AD5 (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// bluetoothDeviceScript.StartedAdvertisingAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = ___action0;
		NullCheck(L_3);
		L_3->___StartedAdvertisingAction_9 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___StartedAdvertisingAction_9), (void*)L_4);
	}

IL_001f:
	{
		// _iOSBluetoothLEStartAdvertising ();
		BluetoothLEHardwareInterface__iOSBluetoothLEStartAdvertising_mE27FFB143EA6F48390E94F6F40CF828340164628(NULL);
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StopAdvertising(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_StopAdvertising_mF770FD1B20379CAD00089300BE7A87D731E1153E (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		// if (bluetoothDeviceScript != null)
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_1 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// bluetoothDeviceScript.StoppedAdvertisingAction = action;
		BluetoothDeviceScript_tD8A11BBB5C0575F9408FA848D6D931A1B43DCE39* L_3 = ((BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_StaticFields*)il2cpp_codegen_static_fields_for(BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B_il2cpp_TypeInfo_var))->___bluetoothDeviceScript_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = ___action0;
		NullCheck(L_3);
		L_3->___StoppedAdvertisingAction_10 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___StoppedAdvertisingAction_10), (void*)L_4);
	}

IL_001f:
	{
		// _iOSBluetoothLEStopAdvertising ();
		BluetoothLEHardwareInterface__iOSBluetoothLEStopAdvertising_mE457E3D9C37A42A70D12591601A7FE8FC4EA9488(NULL);
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::UpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface_UpdateCharacteristicValue_mFDEDEC1F3641A4F8E49CDE3BD12E8F14043AF30E (String_t* ___uuid0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data1, int32_t ___length2, const RuntimeMethod* method) 
{
	{
		// if (!Application.isEditor)
		bool L_0;
		L_0 = Application_get_isEditor_m0377DB707B566C8E21DA3CD99963210F6D57D234(NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// _iOSBluetoothLEUpdateCharacteristicValue (uuid, data, length);
		String_t* L_1 = ___uuid0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = ___data1;
		int32_t L_3 = ___length2;
		BluetoothLEHardwareInterface__iOSBluetoothLEUpdateCharacteristicValue_m19DBF301078337BBDCA08119260575BD24EE1767(L_1, L_2, L_3, NULL);
	}

IL_000f:
	{
		// }
		return;
	}
}
// System.String BluetoothLEHardwareInterface::FullUUID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BluetoothLEHardwareInterface_FullUUID_m9F0BA49662A17B8415741DD5FE9CA57A09917816 (String_t* ___uuid0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3253A6F3765D0068274AF56D2189610ED308CCB0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8BF951CF903ECC622812D47B1157D1A3AFA9FBDC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (uuid.Length == 4)
		String_t* L_0 = ___uuid0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_0, NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_001a;
		}
	}
	{
		// return "0000" + uuid + "-0000-1000-8000-00805f9b34fb";
		String_t* L_2 = ___uuid0;
		String_t* L_3;
		L_3 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(_stringLiteral8BF951CF903ECC622812D47B1157D1A3AFA9FBDC, L_2, _stringLiteral3253A6F3765D0068274AF56D2189610ED308CCB0, NULL);
		return L_3;
	}

IL_001a:
	{
		// return uuid;
		String_t* L_4 = ___uuid0;
		return L_4;
	}
}
// System.Void BluetoothLEHardwareInterface::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BluetoothLEHardwareInterface__ctor_m2CBB7B185D09793C14DA2E5A5A7D1AD758C02C5B (BluetoothLEHardwareInterface_tDF6EBDC625EDEB026E3D58D0978B8ABD7644D19B* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: BluetoothLEHardwareInterface/iBeaconData
IL2CPP_EXTERN_C void iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshal_pinvoke(const iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96& unmarshaled, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_pinvoke& marshaled)
{
	marshaled.___UUID_0 = il2cpp_codegen_marshal_string(unmarshaled.___UUID_0);
	marshaled.___Major_1 = unmarshaled.___Major_1;
	marshaled.___Minor_2 = unmarshaled.___Minor_2;
	marshaled.___RSSI_3 = unmarshaled.___RSSI_3;
	marshaled.___AndroidSignalPower_4 = unmarshaled.___AndroidSignalPower_4;
	marshaled.___iOSProximity_5 = unmarshaled.___iOSProximity_5;
}
IL2CPP_EXTERN_C void iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshal_pinvoke_back(const iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_pinvoke& marshaled, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96& unmarshaled)
{
	unmarshaled.___UUID_0 = il2cpp_codegen_marshal_string_result(marshaled.___UUID_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___UUID_0), (void*)il2cpp_codegen_marshal_string_result(marshaled.___UUID_0));
	int32_t unmarshaledMajor_temp_1 = 0;
	unmarshaledMajor_temp_1 = marshaled.___Major_1;
	unmarshaled.___Major_1 = unmarshaledMajor_temp_1;
	int32_t unmarshaledMinor_temp_2 = 0;
	unmarshaledMinor_temp_2 = marshaled.___Minor_2;
	unmarshaled.___Minor_2 = unmarshaledMinor_temp_2;
	int32_t unmarshaledRSSI_temp_3 = 0;
	unmarshaledRSSI_temp_3 = marshaled.___RSSI_3;
	unmarshaled.___RSSI_3 = unmarshaledRSSI_temp_3;
	int32_t unmarshaledAndroidSignalPower_temp_4 = 0;
	unmarshaledAndroidSignalPower_temp_4 = marshaled.___AndroidSignalPower_4;
	unmarshaled.___AndroidSignalPower_4 = unmarshaledAndroidSignalPower_temp_4;
	int32_t unmarshalediOSProximity_temp_5 = 0;
	unmarshalediOSProximity_temp_5 = marshaled.___iOSProximity_5;
	unmarshaled.___iOSProximity_5 = unmarshalediOSProximity_temp_5;
}
// Conversion method for clean up from marshalling of: BluetoothLEHardwareInterface/iBeaconData
IL2CPP_EXTERN_C void iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshal_pinvoke_cleanup(iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___UUID_0);
	marshaled.___UUID_0 = NULL;
}
// Conversion methods for marshalling of: BluetoothLEHardwareInterface/iBeaconData
IL2CPP_EXTERN_C void iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshal_com(const iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96& unmarshaled, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_com& marshaled)
{
	marshaled.___UUID_0 = il2cpp_codegen_marshal_bstring(unmarshaled.___UUID_0);
	marshaled.___Major_1 = unmarshaled.___Major_1;
	marshaled.___Minor_2 = unmarshaled.___Minor_2;
	marshaled.___RSSI_3 = unmarshaled.___RSSI_3;
	marshaled.___AndroidSignalPower_4 = unmarshaled.___AndroidSignalPower_4;
	marshaled.___iOSProximity_5 = unmarshaled.___iOSProximity_5;
}
IL2CPP_EXTERN_C void iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshal_com_back(const iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_com& marshaled, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96& unmarshaled)
{
	unmarshaled.___UUID_0 = il2cpp_codegen_marshal_bstring_result(marshaled.___UUID_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___UUID_0), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___UUID_0));
	int32_t unmarshaledMajor_temp_1 = 0;
	unmarshaledMajor_temp_1 = marshaled.___Major_1;
	unmarshaled.___Major_1 = unmarshaledMajor_temp_1;
	int32_t unmarshaledMinor_temp_2 = 0;
	unmarshaledMinor_temp_2 = marshaled.___Minor_2;
	unmarshaled.___Minor_2 = unmarshaledMinor_temp_2;
	int32_t unmarshaledRSSI_temp_3 = 0;
	unmarshaledRSSI_temp_3 = marshaled.___RSSI_3;
	unmarshaled.___RSSI_3 = unmarshaledRSSI_temp_3;
	int32_t unmarshaledAndroidSignalPower_temp_4 = 0;
	unmarshaledAndroidSignalPower_temp_4 = marshaled.___AndroidSignalPower_4;
	unmarshaled.___AndroidSignalPower_4 = unmarshaledAndroidSignalPower_temp_4;
	int32_t unmarshalediOSProximity_temp_5 = 0;
	unmarshalediOSProximity_temp_5 = marshaled.___iOSProximity_5;
	unmarshaled.___iOSProximity_5 = unmarshalediOSProximity_temp_5;
}
// Conversion method for clean up from marshalling of: BluetoothLEHardwareInterface/iBeaconData
IL2CPP_EXTERN_C void iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshal_com_cleanup(iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___UUID_0);
	marshaled.___UUID_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 NativeGallery::_NativeGallery_CheckPermission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery__NativeGallery_CheckPermission_m45AEE9475814FD651808364FE2166A058FE2E134 (const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_CheckPermission)();

	return returnValue;
}
// System.Int32 NativeGallery::_NativeGallery_RequestPermission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery__NativeGallery_RequestPermission_mD85483634D04DBAF88D0411A0C0BBF31594D37DB (const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_RequestPermission)();

	return returnValue;
}
// System.Int32 NativeGallery::_NativeGallery_CanOpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery__NativeGallery_CanOpenSettings_mD681FE9951AFDE42A5E26A105605551733D5BD7A (const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_CanOpenSettings)();

	return returnValue;
}
// System.Void NativeGallery::_NativeGallery_OpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_OpenSettings_mB48D04687A8B7F4A39BD3E6B1DA39612F756216A (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_NativeGallery_OpenSettings)();

}
// System.Void NativeGallery::_NativeGallery_ImageWriteToAlbum(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_ImageWriteToAlbum_m3809743794B56DAC2DBF80E70078353EFC80C2B6 (String_t* ___path0, String_t* ___album1, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Marshaling of parameter '___album1' to native representation
	char* ____album1_marshaled = NULL;
	____album1_marshaled = il2cpp_codegen_marshal_string(___album1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_NativeGallery_ImageWriteToAlbum)(____path0_marshaled, ____album1_marshaled);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	// Marshaling cleanup of parameter '___album1' native representation
	il2cpp_codegen_marshal_free(____album1_marshaled);
	____album1_marshaled = NULL;

}
// System.Void NativeGallery::_NativeGallery_VideoWriteToAlbum(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_VideoWriteToAlbum_m84E34BCA6153E0E45A90C0F2784E08355AE64971 (String_t* ___path0, String_t* ___album1, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Marshaling of parameter '___album1' to native representation
	char* ____album1_marshaled = NULL;
	____album1_marshaled = il2cpp_codegen_marshal_string(___album1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_NativeGallery_VideoWriteToAlbum)(____path0_marshaled, ____album1_marshaled);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	// Marshaling cleanup of parameter '___album1' native representation
	il2cpp_codegen_marshal_free(____album1_marshaled);
	____album1_marshaled = NULL;

}
// System.Void NativeGallery::_NativeGallery_PickImage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_PickImage_m3136AAB028A66318A09844416015C93EBD0B7256 (String_t* ___imageSavePath0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___imageSavePath0' to native representation
	char* ____imageSavePath0_marshaled = NULL;
	____imageSavePath0_marshaled = il2cpp_codegen_marshal_string(___imageSavePath0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_NativeGallery_PickImage)(____imageSavePath0_marshaled);

	// Marshaling cleanup of parameter '___imageSavePath0' native representation
	il2cpp_codegen_marshal_free(____imageSavePath0_marshaled);
	____imageSavePath0_marshaled = NULL;

}
// System.Void NativeGallery::_NativeGallery_PickVideo(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery__NativeGallery_PickVideo_m4B4F154AEF7503135D17B3D7B3F0C96B90741017 (String_t* ___videoSavePath0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___videoSavePath0' to native representation
	char* ____videoSavePath0_marshaled = NULL;
	____videoSavePath0_marshaled = il2cpp_codegen_marshal_string(___videoSavePath0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_NativeGallery_PickVideo)(____videoSavePath0_marshaled);

	// Marshaling cleanup of parameter '___videoSavePath0' native representation
	il2cpp_codegen_marshal_free(____videoSavePath0_marshaled);
	____videoSavePath0_marshaled = NULL;

}
// System.String NativeGallery::_NativeGallery_GetImageProperties(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery__NativeGallery_GetImageProperties_m4130139DD3C6DF9E3750094D85F2A88317E7DFC3 (String_t* ___path0, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_GetImageProperties)(____path0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.String NativeGallery::_NativeGallery_GetVideoProperties(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery__NativeGallery_GetVideoProperties_m1230C4EEBA839FF1479E5B1206BB8EB812198B96 (String_t* ___path0, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_GetVideoProperties)(____path0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.String NativeGallery::_NativeGallery_LoadImageAtPath(System.String,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery__NativeGallery_LoadImageAtPath_m557D7EBC832C927A8DB4C4F3FE02697C8F99E5DF (String_t* ___path0, String_t* ___temporaryFilePath1, int32_t ___maxSize2, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Marshaling of parameter '___temporaryFilePath1' to native representation
	char* ____temporaryFilePath1_marshaled = NULL;
	____temporaryFilePath1_marshaled = il2cpp_codegen_marshal_string(___temporaryFilePath1);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_LoadImageAtPath)(____path0_marshaled, ____temporaryFilePath1_marshaled, ___maxSize2);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	// Marshaling cleanup of parameter '___temporaryFilePath1' native representation
	il2cpp_codegen_marshal_free(____temporaryFilePath1_marshaled);
	____temporaryFilePath1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.String NativeGallery::get_TemporaryImagePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_TemporaryImagePath_m32BA1844727D8D2D8F1AFC4FABC7D027CA42641C (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1256BD059A8D156C0578EF505C83E5862F0EFCD2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( m_temporaryImagePath == null )
		String_t* L_0 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_temporaryImagePath_0;
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		// m_temporaryImagePath = Path.Combine( Application.temporaryCachePath, "tmpImg" );
		String_t* L_1;
		L_1 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_1, _stringLiteral1256BD059A8D156C0578EF505C83E5862F0EFCD2, NULL);
		((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_temporaryImagePath_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_temporaryImagePath_0), (void*)L_2);
		// Directory.CreateDirectory( Application.temporaryCachePath );
		String_t* L_3;
		L_3 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_4;
		L_4 = Directory_CreateDirectory_mD89FECDFB25BC52F866DC0B1BB8552334FB249D2(L_3, NULL);
	}

IL_0026:
	{
		// return m_temporaryImagePath;
		String_t* L_5 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_temporaryImagePath_0;
		return L_5;
	}
}
// System.String NativeGallery::get_SelectedImagePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_SelectedImagePath_m79998DCAF1EAD91AB43E1229D733F1C9B21AA6D6 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE1C37FFA5FD702CB48BBF8546A5E8BECD45601C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( m_selectedImagePath == null )
		String_t* L_0 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedImagePath_1;
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		// m_selectedImagePath = Path.Combine( Application.temporaryCachePath, "pickedImg" );
		String_t* L_1;
		L_1 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_1, _stringLiteralCE1C37FFA5FD702CB48BBF8546A5E8BECD45601C, NULL);
		((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedImagePath_1 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedImagePath_1), (void*)L_2);
		// Directory.CreateDirectory( Application.temporaryCachePath );
		String_t* L_3;
		L_3 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_4;
		L_4 = Directory_CreateDirectory_mD89FECDFB25BC52F866DC0B1BB8552334FB249D2(L_3, NULL);
	}

IL_0026:
	{
		// return m_selectedImagePath;
		String_t* L_5 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedImagePath_1;
		return L_5;
	}
}
// System.String NativeGallery::get_SelectedVideoPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_SelectedVideoPath_mF6C9F14DC409D86A8E77AEA127F27D090B8E0FF6 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA79D3CC7BFC44D6628EA61E78D1855415661E3F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( m_selectedVideoPath == null )
		String_t* L_0 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedVideoPath_2;
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		// m_selectedVideoPath = Path.Combine( Application.temporaryCachePath, "pickedVideo" );
		String_t* L_1;
		L_1 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_1, _stringLiteralA79D3CC7BFC44D6628EA61E78D1855415661E3F8, NULL);
		((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedVideoPath_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedVideoPath_2), (void*)L_2);
		// Directory.CreateDirectory( Application.temporaryCachePath );
		String_t* L_3;
		L_3 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_4;
		L_4 = Directory_CreateDirectory_mD89FECDFB25BC52F866DC0B1BB8552334FB249D2(L_3, NULL);
	}

IL_0026:
	{
		// return m_selectedVideoPath;
		String_t* L_5 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedVideoPath_2;
		return L_5;
	}
}
// System.String NativeGallery::get_SelectedAudioPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_get_SelectedAudioPath_mE05B57DA909873F0A036EA253B3F06F2B342F806 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8280B83AC948E7BC53467CEFDF2AF27F4B2209E5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( m_selectedAudioPath == null )
		String_t* L_0 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedAudioPath_3;
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		// m_selectedAudioPath = Path.Combine( Application.temporaryCachePath, "pickedAudio" );
		String_t* L_1;
		L_1 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_1, _stringLiteral8280B83AC948E7BC53467CEFDF2AF27F4B2209E5, NULL);
		((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedAudioPath_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedAudioPath_3), (void*)L_2);
		// Directory.CreateDirectory( Application.temporaryCachePath );
		String_t* L_3;
		L_3 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_4;
		L_4 = Directory_CreateDirectory_mD89FECDFB25BC52F866DC0B1BB8552334FB249D2(L_3, NULL);
	}

IL_0026:
	{
		// return m_selectedAudioPath;
		String_t* L_5 = ((NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_StaticFields*)il2cpp_codegen_static_fields_for(NativeGallery_tBF2ABDB6EB1B1ECC42C7DF6A43CEB2720949D13E_il2cpp_TypeInfo_var))->___m_selectedAudioPath_3;
		return L_5;
	}
}
// NativeGallery/Permission NativeGallery::CheckPermission(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_CheckPermission_mF20525D4CBACF8CF02AA42589F6A20D59EC3CD5F (bool ___readPermissionOnly0, const RuntimeMethod* method) 
{
	{
		// return (Permission) _NativeGallery_CheckPermission();
		int32_t L_0;
		L_0 = NativeGallery__NativeGallery_CheckPermission_m45AEE9475814FD651808364FE2166A058FE2E134(NULL);
		return (int32_t)(L_0);
	}
}
// NativeGallery/Permission NativeGallery::RequestPermission(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_RequestPermission_mBD7A8FDD105249998911185157C71411E7DD2DE5 (bool ___readPermissionOnly0, const RuntimeMethod* method) 
{
	{
		// return (Permission) _NativeGallery_RequestPermission();
		int32_t L_0;
		L_0 = NativeGallery__NativeGallery_RequestPermission_mD85483634D04DBAF88D0411A0C0BBF31594D37DB(NULL);
		return (int32_t)(L_0);
	}
}
// System.Boolean NativeGallery::CanOpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeGallery_CanOpenSettings_m9254373B2D244D8BF223B6F508FD9BF0F1B046C8 (const RuntimeMethod* method) 
{
	{
		// return _NativeGallery_CanOpenSettings() == 1;
		int32_t L_0;
		L_0 = NativeGallery__NativeGallery_CanOpenSettings_mD681FE9951AFDE42A5E26A105605551733D5BD7A(NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void NativeGallery::OpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery_OpenSettings_mB9CB34470D606B63F15FD3FA8581BDD9DF369BB0 (const RuntimeMethod* method) 
{
	{
		// _NativeGallery_OpenSettings();
		NativeGallery__NativeGallery_OpenSettings_mB48D04687A8B7F4A39BD3E6B1DA39612F756216A(NULL);
		// }
		return;
	}
}
// NativeGallery/Permission NativeGallery::SaveImageToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveImageToGallery_mACAB9BE2E7C2F3BF36FD8835BC4EF9F9941CD1B3 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___mediaBytes0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	{
		// return SaveToGallery( mediaBytes, album, filename, MediaType.Image, callback );
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___mediaBytes0;
		String_t* L_1 = ___album1;
		String_t* L_2 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = ___callback3;
		int32_t L_4;
		L_4 = NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66(L_0, L_1, L_2, 0, L_3, NULL);
		return L_4;
	}
}
// NativeGallery/Permission NativeGallery::SaveImageToGallery(System.String,System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveImageToGallery_mC2D88DA2620FC08BE11C375346B9A862E5420CEB (String_t* ___existingMediaPath0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	{
		// return SaveToGallery( existingMediaPath, album, filename, MediaType.Image, callback );
		String_t* L_0 = ___existingMediaPath0;
		String_t* L_1 = ___album1;
		String_t* L_2 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = ___callback3;
		int32_t L_4;
		L_4 = NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A(L_0, L_1, L_2, 0, L_3, NULL);
		return L_4;
	}
}
// NativeGallery/Permission NativeGallery::SaveImageToGallery(UnityEngine.Texture2D,System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveImageToGallery_m920DEF4DDB5909F787646AA94D21235B4AE85169 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___image0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( image == null )
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_0 = ___image0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// throw new ArgumentException( "Parameter 'image' is null!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_2 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_2);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral15332598528DB5F9A0B9473BE7DCE0BB1F8DCAA7)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveImageToGallery_m920DEF4DDB5909F787646AA94D21235B4AE85169_RuntimeMethod_var)));
	}

IL_0014:
	{
		// if( filename.EndsWith( ".jpeg" ) || filename.EndsWith( ".jpg" ) )
		String_t* L_3 = ___filename2;
		NullCheck(L_3);
		bool L_4;
		L_4 = String_EndsWith_mCD3754F5401E19CE7821CD398986E4EAA6AD87DC(L_3, _stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42, NULL);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_5 = ___filename2;
		NullCheck(L_5);
		bool L_6;
		L_6 = String_EndsWith_mCD3754F5401E19CE7821CD398986E4EAA6AD87DC(L_5, _stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A, NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}

IL_002e:
	{
		// return SaveToGallery( GetTextureBytes( image, true ), album, filename, MediaType.Image, callback );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_7 = ___image0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_8;
		L_8 = NativeGallery_GetTextureBytes_mBDA97D1727DD64A8A295A4D8F556F830DD6DAA2D(L_7, (bool)1, NULL);
		String_t* L_9 = ___album1;
		String_t* L_10 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_11 = ___callback3;
		int32_t L_12;
		L_12 = NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66(L_8, L_9, L_10, 0, L_11, NULL);
		return L_12;
	}

IL_003f:
	{
		// else if( filename.EndsWith( ".png" ) )
		String_t* L_13 = ___filename2;
		NullCheck(L_13);
		bool L_14;
		L_14 = String_EndsWith_mCD3754F5401E19CE7821CD398986E4EAA6AD87DC(L_13, _stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26, NULL);
		if (!L_14)
		{
			goto IL_005d;
		}
	}
	{
		// return SaveToGallery( GetTextureBytes( image, false ), album, filename, MediaType.Image, callback );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_15 = ___image0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_16;
		L_16 = NativeGallery_GetTextureBytes_mBDA97D1727DD64A8A295A4D8F556F830DD6DAA2D(L_15, (bool)0, NULL);
		String_t* L_17 = ___album1;
		String_t* L_18 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_19 = ___callback3;
		int32_t L_20;
		L_20 = NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66(L_16, L_17, L_18, 0, L_19, NULL);
		return L_20;
	}

IL_005d:
	{
		// return SaveToGallery( GetTextureBytes( image, false ), album, filename + ".png", MediaType.Image, callback );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_21 = ___image0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_22;
		L_22 = NativeGallery_GetTextureBytes_mBDA97D1727DD64A8A295A4D8F556F830DD6DAA2D(L_21, (bool)0, NULL);
		String_t* L_23 = ___album1;
		String_t* L_24 = ___filename2;
		String_t* L_25;
		L_25 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_24, _stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26, NULL);
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_26 = ___callback3;
		int32_t L_27;
		L_27 = NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66(L_22, L_23, L_25, 0, L_26, NULL);
		return L_27;
	}
}
// NativeGallery/Permission NativeGallery::SaveVideoToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveVideoToGallery_mF6BDD55EFED5372CE746C4D434B3B6002F8D6A5E (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___mediaBytes0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	{
		// return SaveToGallery( mediaBytes, album, filename, MediaType.Video, callback );
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___mediaBytes0;
		String_t* L_1 = ___album1;
		String_t* L_2 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = ___callback3;
		int32_t L_4;
		L_4 = NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66(L_0, L_1, L_2, 1, L_3, NULL);
		return L_4;
	}
}
// NativeGallery/Permission NativeGallery::SaveVideoToGallery(System.String,System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveVideoToGallery_mAF6AD9B8F58CA686807DE6624D21E29938AF7F88 (String_t* ___existingMediaPath0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	{
		// return SaveToGallery( existingMediaPath, album, filename, MediaType.Video, callback );
		String_t* L_0 = ___existingMediaPath0;
		String_t* L_1 = ___album1;
		String_t* L_2 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = ___callback3;
		int32_t L_4;
		L_4 = NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A(L_0, L_1, L_2, 1, L_3, NULL);
		return L_4;
	}
}
// NativeGallery/Permission NativeGallery::SaveAudioToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveAudioToGallery_m0A11F082B031A8F36229DA626621E53A0AED1F3D (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___mediaBytes0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	{
		// return SaveToGallery( mediaBytes, album, filename, MediaType.Audio, callback );
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___mediaBytes0;
		String_t* L_1 = ___album1;
		String_t* L_2 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = ___callback3;
		int32_t L_4;
		L_4 = NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66(L_0, L_1, L_2, 2, L_3, NULL);
		return L_4;
	}
}
// NativeGallery/Permission NativeGallery::SaveAudioToGallery(System.String,System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveAudioToGallery_m5FC57141EB1C617DB214BAFF9755EA8B26F22B5A (String_t* ___existingMediaPath0, String_t* ___album1, String_t* ___filename2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	{
		// return SaveToGallery( existingMediaPath, album, filename, MediaType.Audio, callback );
		String_t* L_0 = ___existingMediaPath0;
		String_t* L_1 = ___album1;
		String_t* L_2 = ___filename2;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = ___callback3;
		int32_t L_4;
		L_4 = NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A(L_0, L_1, L_2, 2, L_3, NULL);
		return L_4;
	}
}
// System.Boolean NativeGallery::CanSelectMultipleFilesFromGallery()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeGallery_CanSelectMultipleFilesFromGallery_mC1041FEED2AF374AC572B796BAFAFD9B2A0BC8C7 (const RuntimeMethod* method) 
{
	{
		// return false;
		return (bool)0;
	}
}
// NativeGallery/Permission NativeGallery::GetImageFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetImageFromGallery_mFEDC77A9786DE41533CB9415674871456BD22991 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method) 
{
	{
		// return GetMediaFromGallery( callback, MediaType.Image, mime, title );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_0 = ___callback0;
		String_t* L_1 = ___mime2;
		String_t* L_2 = ___title1;
		int32_t L_3;
		L_3 = NativeGallery_GetMediaFromGallery_m890AD2506069812EF1F08AADC376799DF3513B23(L_0, 0, L_1, L_2, NULL);
		return L_3;
	}
}
// NativeGallery/Permission NativeGallery::GetVideoFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetVideoFromGallery_mF74741B99075902DE42647280DAA4E8121CFB551 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method) 
{
	{
		// return GetMediaFromGallery( callback, MediaType.Video, mime, title );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_0 = ___callback0;
		String_t* L_1 = ___mime2;
		String_t* L_2 = ___title1;
		int32_t L_3;
		L_3 = NativeGallery_GetMediaFromGallery_m890AD2506069812EF1F08AADC376799DF3513B23(L_0, 1, L_1, L_2, NULL);
		return L_3;
	}
}
// NativeGallery/Permission NativeGallery::GetAudioFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetAudioFromGallery_m91FFA0FA35A315B819477E6A3E34D71FF8312017 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method) 
{
	{
		// return GetMediaFromGallery( callback, MediaType.Audio, mime, title );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_0 = ___callback0;
		String_t* L_1 = ___mime2;
		String_t* L_2 = ___title1;
		int32_t L_3;
		L_3 = NativeGallery_GetMediaFromGallery_m890AD2506069812EF1F08AADC376799DF3513B23(L_0, 2, L_1, L_2, NULL);
		return L_3;
	}
}
// NativeGallery/Permission NativeGallery::GetImagesFromGallery(NativeGallery/MediaPickMultipleCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetImagesFromGallery_m56DDD52D37B9DC584652ED7FFB8B0058B6C16636 (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method) 
{
	{
		// return GetMultipleMediaFromGallery( callback, MediaType.Image, mime, title );
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_0 = ___callback0;
		String_t* L_1 = ___mime2;
		String_t* L_2 = ___title1;
		int32_t L_3;
		L_3 = NativeGallery_GetMultipleMediaFromGallery_m423CEDD28542066C638803009A9917FECD5B61ED(L_0, 0, L_1, L_2, NULL);
		return L_3;
	}
}
// NativeGallery/Permission NativeGallery::GetVideosFromGallery(NativeGallery/MediaPickMultipleCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetVideosFromGallery_mD6CFDBFDC321971B7B2958D8E195411F404141CB (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method) 
{
	{
		// return GetMultipleMediaFromGallery( callback, MediaType.Video, mime, title );
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_0 = ___callback0;
		String_t* L_1 = ___mime2;
		String_t* L_2 = ___title1;
		int32_t L_3;
		L_3 = NativeGallery_GetMultipleMediaFromGallery_m423CEDD28542066C638803009A9917FECD5B61ED(L_0, 1, L_1, L_2, NULL);
		return L_3;
	}
}
// NativeGallery/Permission NativeGallery::GetAudiosFromGallery(NativeGallery/MediaPickMultipleCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetAudiosFromGallery_mEADF01639F7148ECF378FAE8BFCEE01737BC32D9 (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method) 
{
	{
		// return GetMultipleMediaFromGallery( callback, MediaType.Audio, mime, title );
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_0 = ___callback0;
		String_t* L_1 = ___mime2;
		String_t* L_2 = ___title1;
		int32_t L_3;
		L_3 = NativeGallery_GetMultipleMediaFromGallery_m423CEDD28542066C638803009A9917FECD5B61ED(L_0, 2, L_1, L_2, NULL);
		return L_3;
	}
}
// System.Boolean NativeGallery::IsMediaPickerBusy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeGallery_IsMediaPickerBusy_m05EF325B7999ECF2388E56485D3108B6869F43B9 (const RuntimeMethod* method) 
{
	{
		// return NGMediaReceiveCallbackiOS.IsBusy;
		bool L_0;
		L_0 = NGMediaReceiveCallbackiOS_get_IsBusy_mAECD29ABE59BCEA398CBE4E9920C1F4A0FADE428_inline(NULL);
		return L_0;
	}
}
// NativeGallery/Permission NativeGallery::SaveToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___mediaBytes0, String_t* ___album1, String_t* ___filename2, int32_t ___mediaType3, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1DA1565418493517EA4A1928E378AD0548A223E7);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B13_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B11_0 = 0;
	{
		// Permission result = RequestPermission( false );
		int32_t L_0;
		L_0 = NativeGallery_RequestPermission_mBD7A8FDD105249998911185157C71411E7DD2DE5((bool)0, NULL);
		// if( result == Permission.Granted )
		int32_t L_1 = L_0;
		G_B1_0 = L_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B13_0 = L_1;
			goto IL_0075;
		}
	}
	{
		// if( mediaBytes == null || mediaBytes.Length == 0 )
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = ___mediaBytes0;
		G_B2_0 = G_B1_0;
		if (!L_2)
		{
			G_B3_0 = G_B1_0;
			goto IL_0011;
		}
	}
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = ___mediaBytes0;
		NullCheck(L_3);
		G_B3_0 = G_B2_0;
		if ((((RuntimeArray*)L_3)->max_length))
		{
			G_B4_0 = G_B2_0;
			goto IL_001c;
		}
	}

IL_0011:
	{
		// throw new ArgumentException( "Parameter 'mediaBytes' is null or empty!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_4 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_4);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_4, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7BB4B5568C63748C896DED09B403F2FC4F274E91)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66_RuntimeMethod_var)));
	}

IL_001c:
	{
		// if( album == null || album.Length == 0 )
		String_t* L_5 = ___album1;
		G_B5_0 = G_B4_0;
		if (!L_5)
		{
			G_B6_0 = G_B4_0;
			goto IL_0027;
		}
	}
	{
		String_t* L_6 = ___album1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_6, NULL);
		G_B6_0 = G_B5_0;
		if (L_7)
		{
			G_B7_0 = G_B5_0;
			goto IL_0032;
		}
	}

IL_0027:
	{
		// throw new ArgumentException( "Parameter 'album' is null or empty!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_8 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_8);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30F8B1D027E92AF30A25606539C4A0E635BF0BBB)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66_RuntimeMethod_var)));
	}

IL_0032:
	{
		// if( filename == null || filename.Length == 0 )
		String_t* L_9 = ___filename2;
		G_B8_0 = G_B7_0;
		if (!L_9)
		{
			G_B9_0 = G_B7_0;
			goto IL_003d;
		}
	}
	{
		String_t* L_10 = ___filename2;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_10, NULL);
		G_B9_0 = G_B8_0;
		if (L_11)
		{
			G_B10_0 = G_B8_0;
			goto IL_0048;
		}
	}

IL_003d:
	{
		// throw new ArgumentException( "Parameter 'filename' is null or empty!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_12 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_12);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_12, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30F7CAA3903ABC311FB9B0881B8937BE76A5526D)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveToGallery_mCB71BEC2D8E1B98A0F76467BE441EF7FF6416C66_RuntimeMethod_var)));
	}

IL_0048:
	{
		// if( string.IsNullOrEmpty( Path.GetExtension( filename ) ) )
		String_t* L_13 = ___filename2;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_14;
		L_14 = Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7(L_13, NULL);
		bool L_15;
		L_15 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_14, NULL);
		G_B11_0 = G_B10_0;
		if (!L_15)
		{
			G_B12_0 = G_B10_0;
			goto IL_005f;
		}
	}
	{
		// Debug.LogWarning( "'filename' doesn't have an extension, this might result in unexpected behaviour!" );
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28(_stringLiteral1DA1565418493517EA4A1928E378AD0548A223E7, NULL);
		G_B12_0 = G_B11_0;
	}

IL_005f:
	{
		// string path = GetTemporarySavePath( filename );
		String_t* L_16 = ___filename2;
		String_t* L_17;
		L_17 = NativeGallery_GetTemporarySavePath_m29E58001A5CB64D1A1AF5A494E318D40604490B1(L_16, NULL);
		// File.WriteAllBytes( path, mediaBytes );
		String_t* L_18 = L_17;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_19 = ___mediaBytes0;
		File_WriteAllBytes_m72C1A24339B30F84A655E6BF516AE1638B2C4668(L_18, L_19, NULL);
		// SaveToGalleryInternal( path, album, mediaType, callback );
		String_t* L_20 = ___album1;
		int32_t L_21 = ___mediaType3;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_22 = ___callback4;
		NativeGallery_SaveToGalleryInternal_m3B9ED4FA03CC0D8CF9049EA93678766BC81C0BC1(L_18, L_20, L_21, L_22, NULL);
		G_B13_0 = G_B12_0;
	}

IL_0075:
	{
		// return result;
		return G_B13_0;
	}
}
// NativeGallery/Permission NativeGallery::SaveToGallery(System.String,System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A (String_t* ___existingMediaPath0, String_t* ___album1, String_t* ___filename2, int32_t ___mediaType3, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1DA1565418493517EA4A1928E378AD0548A223E7);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t G_B14_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B11_0 = 0;
	{
		// Permission result = RequestPermission( false );
		int32_t L_0;
		L_0 = NativeGallery_RequestPermission_mBD7A8FDD105249998911185157C71411E7DD2DE5((bool)0, NULL);
		// if( result == Permission.Granted )
		int32_t L_1 = L_0;
		G_B1_0 = L_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B14_0 = L_1;
			goto IL_009c;
		}
	}
	{
		// if( !File.Exists( existingMediaPath ) )
		String_t* L_2 = ___existingMediaPath0;
		bool L_3;
		L_3 = File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85(L_2, NULL);
		G_B2_0 = G_B1_0;
		if (L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0026;
		}
	}
	{
		// throw new FileNotFoundException( "File not found at " + existingMediaPath );
		String_t* L_4 = ___existingMediaPath0;
		String_t* L_5;
		L_5 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral491B4D9271839F0BD63211437BF7CEE5B2C6ADE9)), L_4, NULL);
		FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* L_6 = (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var)));
		NullCheck(L_6);
		FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1(L_6, L_5, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A_RuntimeMethod_var)));
	}

IL_0026:
	{
		// if( album == null || album.Length == 0 )
		String_t* L_7 = ___album1;
		G_B4_0 = G_B3_0;
		if (!L_7)
		{
			G_B5_0 = G_B3_0;
			goto IL_0031;
		}
	}
	{
		String_t* L_8 = ___album1;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_8, NULL);
		G_B5_0 = G_B4_0;
		if (L_9)
		{
			G_B6_0 = G_B4_0;
			goto IL_003c;
		}
	}

IL_0031:
	{
		// throw new ArgumentException( "Parameter 'album' is null or empty!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_10 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_10);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_10, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30F8B1D027E92AF30A25606539C4A0E635BF0BBB)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A_RuntimeMethod_var)));
	}

IL_003c:
	{
		// if( filename == null || filename.Length == 0 )
		String_t* L_11 = ___filename2;
		G_B7_0 = G_B6_0;
		if (!L_11)
		{
			G_B8_0 = G_B6_0;
			goto IL_0047;
		}
	}
	{
		String_t* L_12 = ___filename2;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_12, NULL);
		G_B8_0 = G_B7_0;
		if (L_13)
		{
			G_B9_0 = G_B7_0;
			goto IL_0052;
		}
	}

IL_0047:
	{
		// throw new ArgumentException( "Parameter 'filename' is null or empty!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_14 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_14);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_14, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30F7CAA3903ABC311FB9B0881B8937BE76A5526D)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_SaveToGallery_m4CC82F34B65273F46CDFDD8014F56F2C0813300A_RuntimeMethod_var)));
	}

IL_0052:
	{
		// if( string.IsNullOrEmpty( Path.GetExtension( filename ) ) )
		String_t* L_15 = ___filename2;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_16;
		L_16 = Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7(L_15, NULL);
		bool L_17;
		L_17 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_16, NULL);
		G_B10_0 = G_B9_0;
		if (!L_17)
		{
			G_B13_0 = G_B9_0;
			goto IL_0083;
		}
	}
	{
		// string originalExtension = Path.GetExtension( existingMediaPath );
		String_t* L_18 = ___existingMediaPath0;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_19;
		L_19 = Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7(L_18, NULL);
		V_1 = L_19;
		// if( string.IsNullOrEmpty( originalExtension ) )
		String_t* L_20 = V_1;
		bool L_21;
		L_21 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_20, NULL);
		G_B11_0 = G_B10_0;
		if (!L_21)
		{
			G_B12_0 = G_B10_0;
			goto IL_007a;
		}
	}
	{
		// Debug.LogWarning( "'filename' doesn't have an extension, this might result in unexpected behaviour!" );
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28(_stringLiteral1DA1565418493517EA4A1928E378AD0548A223E7, NULL);
		G_B13_0 = G_B11_0;
		goto IL_0083;
	}

IL_007a:
	{
		// filename += originalExtension;
		String_t* L_22 = ___filename2;
		String_t* L_23 = V_1;
		String_t* L_24;
		L_24 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_22, L_23, NULL);
		___filename2 = L_24;
		G_B13_0 = G_B12_0;
	}

IL_0083:
	{
		// string path = GetTemporarySavePath( filename );
		String_t* L_25 = ___filename2;
		String_t* L_26;
		L_26 = NativeGallery_GetTemporarySavePath_m29E58001A5CB64D1A1AF5A494E318D40604490B1(L_25, NULL);
		V_0 = L_26;
		// File.Copy( existingMediaPath, path, true );
		String_t* L_27 = ___existingMediaPath0;
		String_t* L_28 = V_0;
		File_Copy_m68957393F932BED668C5725704E5DA9B71925395(L_27, L_28, (bool)1, NULL);
		// SaveToGalleryInternal( path, album, mediaType, callback );
		String_t* L_29 = V_0;
		String_t* L_30 = ___album1;
		int32_t L_31 = ___mediaType3;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_32 = ___callback4;
		NativeGallery_SaveToGalleryInternal_m3B9ED4FA03CC0D8CF9049EA93678766BC81C0BC1(L_29, L_30, L_31, L_32, NULL);
		G_B14_0 = G_B13_0;
	}

IL_009c:
	{
		// return result;
		return G_B14_0;
	}
}
// System.Void NativeGallery::SaveToGalleryInternal(System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeGallery_SaveToGalleryInternal_m3B9ED4FA03CC0D8CF9049EA93678766BC81C0BC1 (String_t* ___path0, String_t* ___album1, int32_t ___mediaType2, MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0365E1814018F9007D2AF7114C462B24D8928C59);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD4E22C84D059FC69AA54802023A8B68B6F4F4C7E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( mediaType == MediaType.Audio )
		int32_t L_0 = ___mediaType2;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0013;
		}
	}
	{
		// if( callback != null )
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_1 = ___callback3;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		// callback( "Saving audio files is not supported on iOS" );
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_2 = ___callback3;
		NullCheck(L_2);
		MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_inline(L_2, _stringLiteralD4E22C84D059FC69AA54802023A8B68B6F4F4C7E, NULL);
	}

IL_0012:
	{
		// return;
		return;
	}

IL_0013:
	{
		// Debug.Log( "Saving to Pictures: " + Path.GetFileName( path ) );
		String_t* L_3 = ___path0;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_4;
		L_4 = Path_GetFileName_mEBC73E0C8D8C56214D1DA4BA8409C5B5F00457A5(L_3, NULL);
		String_t* L_5;
		L_5 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral0365E1814018F9007D2AF7114C462B24D8928C59, L_4, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_5, NULL);
		// NGMediaSaveCallbackiOS.Initialize( callback );
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_6 = ___callback3;
		NGMediaSaveCallbackiOS_Initialize_mE5AC16A419E72FF81137DF8977D5A909D6E79A10(L_6, NULL);
		// if( mediaType == MediaType.Image )
		int32_t L_7 = ___mediaType2;
		if (L_7)
		{
			goto IL_0039;
		}
	}
	{
		// _NativeGallery_ImageWriteToAlbum( path, album );
		String_t* L_8 = ___path0;
		String_t* L_9 = ___album1;
		NativeGallery__NativeGallery_ImageWriteToAlbum_m3809743794B56DAC2DBF80E70078353EFC80C2B6(L_8, L_9, NULL);
		return;
	}

IL_0039:
	{
		// else if( mediaType == MediaType.Video )
		int32_t L_10 = ___mediaType2;
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_0044;
		}
	}
	{
		// _NativeGallery_VideoWriteToAlbum( path, album );
		String_t* L_11 = ___path0;
		String_t* L_12 = ___album1;
		NativeGallery__NativeGallery_VideoWriteToAlbum_m84E34BCA6153E0E45A90C0F2784E08355AE64971(L_11, L_12, NULL);
	}

IL_0044:
	{
		// }
		return;
	}
}
// System.String NativeGallery::GetTemporarySavePath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NativeGallery_GetTemporarySavePath_m29E58001A5CB64D1A1AF5A494E318D40604490B1 (String_t* ___filename0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCFA0E830C16EC57623615165EA8FBC2817BACCCF);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		// string saveDir = Path.Combine( Application.persistentDataPath, "NGallery" );
		String_t* L_0;
		L_0 = Application_get_persistentDataPath_m787EBC9B0862E7617DCD6CABD2147E61717EAC17(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_0, _stringLiteralCFA0E830C16EC57623615165EA8FBC2817BACCCF, NULL);
		V_0 = L_1;
		// Directory.CreateDirectory( saveDir );
		String_t* L_2 = V_0;
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_3;
		L_3 = Directory_CreateDirectory_mD89FECDFB25BC52F866DC0B1BB8552334FB249D2(L_2, NULL);
		// string path = Path.Combine( saveDir, filename );
		String_t* L_4 = V_0;
		String_t* L_5 = ___filename0;
		String_t* L_6;
		L_6 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_4, L_5, NULL);
		V_1 = L_6;
		// if( File.Exists( path ) )
		String_t* L_7 = V_1;
		bool L_8;
		L_8 = File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85(L_7, NULL);
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		// int fileIndex = 0;
		V_2 = 0;
		// string filenameWithoutExtension = Path.GetFileNameWithoutExtension( filename );
		String_t* L_9 = ___filename0;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_10;
		L_10 = Path_GetFileNameWithoutExtension_mD24A7CA7C45FF7A265EF7F8D5E19D1F3C014D90F(L_9, NULL);
		V_3 = L_10;
		// string extension = Path.GetExtension( filename );
		String_t* L_11 = ___filename0;
		String_t* L_12;
		L_12 = Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7(L_11, NULL);
		V_4 = L_12;
	}

IL_0038:
	{
		// path = Path.Combine( saveDir, string.Concat( filenameWithoutExtension, ++fileIndex, extension ) );
		String_t* L_13 = V_0;
		String_t* L_14 = V_3;
		int32_t L_15 = V_2;
		int32_t L_16 = ((int32_t)il2cpp_codegen_add(L_15, 1));
		V_2 = L_16;
		int32_t L_17 = L_16;
		RuntimeObject* L_18 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_17);
		String_t* L_19 = V_4;
		String_t* L_20;
		L_20 = String_Concat_mA299F8BBD1426934E2DAA95859E11C2063A116C8(L_14, L_18, L_19, NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_21;
		L_21 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_13, L_20, NULL);
		V_1 = L_21;
		// } while( File.Exists( path ) );
		String_t* L_22 = V_1;
		bool L_23;
		L_23 = File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85(L_22, NULL);
		if (L_23)
		{
			goto IL_0038;
		}
	}

IL_0059:
	{
		// return path;
		String_t* L_24 = V_1;
		return L_24;
	}
}
// NativeGallery/Permission NativeGallery::GetMediaFromGallery(NativeGallery/MediaPickCallback,NativeGallery/MediaType,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetMediaFromGallery_m890AD2506069812EF1F08AADC376799DF3513B23 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, int32_t ___mediaType1, String_t* ___mime2, String_t* ___title3, const RuntimeMethod* method) 
{
	int32_t G_B8_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		// Permission result = RequestPermission( true );
		int32_t L_0;
		L_0 = NativeGallery_RequestPermission_mBD7A8FDD105249998911185157C71411E7DD2DE5((bool)1, NULL);
		// if( result == Permission.Granted && !IsMediaPickerBusy() )
		int32_t L_1 = L_0;
		G_B1_0 = L_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B8_0 = L_1;
			goto IL_003e;
		}
	}
	{
		bool L_2;
		L_2 = NativeGallery_IsMediaPickerBusy_m05EF325B7999ECF2388E56485D3108B6869F43B9(NULL);
		G_B2_0 = G_B1_0;
		if (L_2)
		{
			G_B8_0 = G_B1_0;
			goto IL_003e;
		}
	}
	{
		// NGMediaReceiveCallbackiOS.Initialize( callback );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_3 = ___callback0;
		NGMediaReceiveCallbackiOS_Initialize_m5825805938A7EB8FC2A1697CD11C2E98F4F95D6D(L_3, NULL);
		// if( mediaType == MediaType.Image )
		int32_t L_4 = ___mediaType1;
		G_B3_0 = G_B2_0;
		if (L_4)
		{
			G_B4_0 = G_B2_0;
			goto IL_0025;
		}
	}
	{
		// _NativeGallery_PickImage( SelectedImagePath );
		String_t* L_5;
		L_5 = NativeGallery_get_SelectedImagePath_m79998DCAF1EAD91AB43E1229D733F1C9B21AA6D6(NULL);
		NativeGallery__NativeGallery_PickImage_m3136AAB028A66318A09844416015C93EBD0B7256(L_5, NULL);
		return G_B3_0;
	}

IL_0025:
	{
		// else if( mediaType == MediaType.Video )
		int32_t L_6 = ___mediaType1;
		G_B5_0 = G_B4_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			G_B6_0 = G_B4_0;
			goto IL_0034;
		}
	}
	{
		// _NativeGallery_PickVideo( SelectedVideoPath );
		String_t* L_7;
		L_7 = NativeGallery_get_SelectedVideoPath_mF6C9F14DC409D86A8E77AEA127F27D090B8E0FF6(NULL);
		NativeGallery__NativeGallery_PickVideo_m4B4F154AEF7503135D17B3D7B3F0C96B90741017(L_7, NULL);
		return G_B5_0;
	}

IL_0034:
	{
		// else if( callback != null ) // Selecting audio files is not supported on iOS
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_8 = ___callback0;
		G_B7_0 = G_B6_0;
		if (!L_8)
		{
			G_B8_0 = G_B6_0;
			goto IL_003e;
		}
	}
	{
		// callback( null );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_9 = ___callback0;
		NullCheck(L_9);
		MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_inline(L_9, (String_t*)NULL, NULL);
		G_B8_0 = G_B7_0;
	}

IL_003e:
	{
		// return result;
		return G_B8_0;
	}
}
// NativeGallery/Permission NativeGallery::GetMultipleMediaFromGallery(NativeGallery/MediaPickMultipleCallback,NativeGallery/MediaType,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetMultipleMediaFromGallery_m423CEDD28542066C638803009A9917FECD5B61ED (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* ___callback0, int32_t ___mediaType1, String_t* ___mime2, String_t* ___title3, const RuntimeMethod* method) 
{
	int32_t G_B7_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		// Permission result = RequestPermission( true );
		int32_t L_0;
		L_0 = NativeGallery_RequestPermission_mBD7A8FDD105249998911185157C71411E7DD2DE5((bool)1, NULL);
		// if( result == Permission.Granted && !IsMediaPickerBusy() )
		int32_t L_1 = L_0;
		G_B1_0 = L_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B7_0 = L_1;
			goto IL_002d;
		}
	}
	{
		bool L_2;
		L_2 = NativeGallery_IsMediaPickerBusy_m05EF325B7999ECF2388E56485D3108B6869F43B9(NULL);
		G_B2_0 = G_B1_0;
		if (L_2)
		{
			G_B7_0 = G_B1_0;
			goto IL_002d;
		}
	}
	{
		// if( CanSelectMultipleFilesFromGallery() )
		bool L_3;
		L_3 = NativeGallery_CanSelectMultipleFilesFromGallery_mC1041FEED2AF374AC572B796BAFAFD9B2A0BC8C7(NULL);
		G_B3_0 = G_B2_0;
		if (!L_3)
		{
			G_B5_0 = G_B2_0;
			goto IL_0023;
		}
	}
	{
		// if( callback != null )
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_4 = ___callback0;
		G_B4_0 = G_B3_0;
		if (!L_4)
		{
			G_B7_0 = G_B3_0;
			goto IL_002d;
		}
	}
	{
		// callback( null );
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_5 = ___callback0;
		NullCheck(L_5);
		MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_inline(L_5, (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL, NULL);
		return G_B4_0;
	}

IL_0023:
	{
		// else if( callback != null )
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_6 = ___callback0;
		G_B6_0 = G_B5_0;
		if (!L_6)
		{
			G_B7_0 = G_B5_0;
			goto IL_002d;
		}
	}
	{
		// callback( null );
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* L_7 = ___callback0;
		NullCheck(L_7);
		MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_inline(L_7, (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL, NULL);
		G_B7_0 = G_B6_0;
	}

IL_002d:
	{
		// return result;
		return G_B7_0;
	}
}
// System.Byte[] NativeGallery::GetTextureBytes(UnityEngine.Texture2D,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NativeGallery_GetTextureBytes_mBDA97D1727DD64A8A295A4D8F556F830DD6DAA2D (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___texture0, bool ___isJpeg1, const RuntimeMethod* method) 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* G_B3_0 = NULL;
	try
	{// begin try (depth: 1)
		{
			// return isJpeg ? texture.EncodeToJPG( 100 ) : texture.EncodeToPNG();
			bool L_0 = ___isJpeg1;
			if (L_0)
			{
				goto IL_000b_1;
			}
		}
		{
			Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_1 = ___texture0;
			ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2;
			L_2 = ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391(L_1, NULL);
			G_B3_0 = L_2;
			goto IL_0013_1;
		}

IL_000b_1:
		{
			Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_3 = ___texture0;
			ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4;
			L_4 = ImageConversion_EncodeToJPG_mABBC4FA7AF9A69EB41FDE1CFE73A3F8656546385(L_3, ((int32_t)100), NULL);
			G_B3_0 = L_4;
		}

IL_0013_1:
		{
			V_0 = G_B3_0;
			goto IL_002c;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnityException_tA1EC1E95ADE689CF6EB7FAFF77C160AE1F559067_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0016;
		}
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0021;
		}
		throw e;
	}

CATCH_0016:
	{// begin catch(UnityEngine.UnityException)
		// catch( UnityException )
		// return GetTextureBytesFromCopy( texture, isJpeg );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_5 = ___texture0;
		bool L_6 = ___isJpeg1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_7;
		L_7 = NativeGallery_GetTextureBytesFromCopy_m5AF093F441E4DF9CDA5F17E00496C69C6F497A7C(L_5, L_6, NULL);
		V_0 = L_7;
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_002c;
	}// end catch (depth: 1)

CATCH_0021:
	{// begin catch(System.ArgumentException)
		// catch( ArgumentException )
		// return GetTextureBytesFromCopy( texture, isJpeg );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_8 = ___texture0;
		bool L_9 = ___isJpeg1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_10;
		L_10 = NativeGallery_GetTextureBytesFromCopy_m5AF093F441E4DF9CDA5F17E00496C69C6F497A7C(L_8, L_9, NULL);
		V_0 = L_10;
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_002c;
	}// end catch (depth: 1)

IL_002c:
	{
		// }
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11 = V_0;
		return L_11;
	}
}
// System.Byte[] NativeGallery::GetTextureBytesFromCopy(UnityEngine.Texture2D,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NativeGallery_GetTextureBytesFromCopy_m5AF093F441E4DF9CDA5F17E00496C69C6F497A7C (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___texture0, bool ___isJpeg1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F7876094B3FA10965A88A7D08B74EA3DC22CAE9);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* V_0 = NULL;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_1 = NULL;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_2 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_3 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* G_B8_0 = NULL;
	{
		// Debug.LogWarning( "Saving non-readable textures is slower than saving readable textures" );
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28(_stringLiteral7F7876094B3FA10965A88A7D08B74EA3DC22CAE9, NULL);
		// Texture2D sourceTexReadable = null;
		V_0 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)NULL;
		// RenderTexture rt = RenderTexture.GetTemporary( texture.width, texture.height );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_0 = ___texture0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_2 = ___texture0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = VirtualFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_4;
		L_4 = RenderTexture_GetTemporary_mCD6ECAD5EDABF63A1F2F496ABC4E2502F0883A6E(L_1, L_3, NULL);
		V_1 = L_4;
		// RenderTexture activeRT = RenderTexture.active;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_5;
		L_5 = RenderTexture_get_active_m2204DF860773F9A8CDFF92BDB35CEB72A0643345(NULL);
		V_2 = L_5;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0089:
			{// begin finally (depth: 1)
				// RenderTexture.active = activeRT;
				RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_6 = V_2;
				RenderTexture_set_active_m045EA53D62FBF79693265E33D14D8E0E8151A37E(L_6, NULL);
				// RenderTexture.ReleaseTemporary( rt );
				RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_7 = V_1;
				RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F(L_7, NULL);
				// }
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			try
			{// begin try (depth: 2)
				// Graphics.Blit( texture, rt );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_8 = ___texture0;
				RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_9 = V_1;
				il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
				Graphics_Blit_m066854D684B6042B266D306E8E012D2C6C1787BE(L_8, L_9, NULL);
				// RenderTexture.active = rt;
				RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_10 = V_1;
				RenderTexture_set_active_m045EA53D62FBF79693265E33D14D8E0E8151A37E(L_10, NULL);
				// sourceTexReadable = new Texture2D( texture.width, texture.height, texture.format, false );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_11 = ___texture0;
				NullCheck(L_11);
				int32_t L_12;
				L_12 = VirtualFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_11);
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_13 = ___texture0;
				NullCheck(L_13);
				int32_t L_14;
				L_14 = VirtualFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_13);
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_15 = ___texture0;
				NullCheck(L_15);
				int32_t L_16;
				L_16 = Texture2D_get_format_mE39DD922F83CA1097383309278BB6F20636A7D9D(L_15, NULL);
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_17 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
				NullCheck(L_17);
				Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917(L_17, L_12, L_14, L_16, (bool)0, NULL);
				V_0 = L_17;
				// sourceTexReadable.ReadPixels( new Rect( 0, 0, texture.width, texture.height ), 0, 0, false );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_18 = V_0;
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_19 = ___texture0;
				NullCheck(L_19);
				int32_t L_20;
				L_20 = VirtualFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_19);
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_21 = ___texture0;
				NullCheck(L_21);
				int32_t L_22;
				L_22 = VirtualFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_21);
				Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_23;
				memset((&L_23), 0, sizeof(L_23));
				Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&L_23), (0.0f), (0.0f), ((float)L_20), ((float)L_22), /*hidden argument*/NULL);
				NullCheck(L_18);
				Texture2D_ReadPixels_m7483DB211233F02E46418E9A6077487925F0024C(L_18, L_23, 0, 0, (bool)0, NULL);
				// sourceTexReadable.Apply( false, false );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_24 = V_0;
				NullCheck(L_24);
				Texture2D_Apply_m36EE27E6F1BF7FB8C70A1D749DC4EE249810AA3A(L_24, (bool)0, (bool)0, NULL);
				// }
				goto IL_0096;
			}// end try (depth: 2)
			catch(Il2CppExceptionWrapper& e)
			{
				if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				{
					IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
					goto CATCH_007a_1;
				}
				throw e;
			}

CATCH_007a_1:
			{// begin catch(System.Exception)
				// Debug.LogException( e );
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
				Debug_LogException_m82E44FEC6B03BC34AFC2CAF6583051570C60CB9E(((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*)), NULL);
				// Object.DestroyImmediate( sourceTexReadable );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_25 = V_0;
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var)));
				Object_DestroyImmediate_m8249CABCDF344BE3A67EE765122EBB415DC2BC57(L_25, NULL);
				// return null;
				V_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)NULL;
				IL2CPP_POP_ACTIVE_EXCEPTION();
				goto IL_00bd;
			}// end catch (depth: 2)
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0096:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00b6:
			{// begin finally (depth: 1)
				// Object.DestroyImmediate( sourceTexReadable );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_26 = V_0;
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				Object_DestroyImmediate_m8249CABCDF344BE3A67EE765122EBB415DC2BC57(L_26, NULL);
				// }
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			try
			{// begin try (depth: 2)
				{
					// return isJpeg ? sourceTexReadable.EncodeToJPG( 100 ) : sourceTexReadable.EncodeToPNG();
					bool L_27 = ___isJpeg1;
					if (L_27)
					{
						goto IL_00a2_2;
					}
				}
				{
					Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_28 = V_0;
					ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_29;
					L_29 = ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391(L_28, NULL);
					G_B8_0 = L_29;
					goto IL_00aa_2;
				}

IL_00a2_2:
				{
					Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_30 = V_0;
					ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_31;
					L_31 = ImageConversion_EncodeToJPG_mABBC4FA7AF9A69EB41FDE1CFE73A3F8656546385(L_30, ((int32_t)100), NULL);
					G_B8_0 = L_31;
				}

IL_00aa_2:
				{
					V_3 = G_B8_0;
					goto IL_00bd;
				}
			}// end try (depth: 2)
			catch(Il2CppExceptionWrapper& e)
			{
				if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				{
					IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
					goto CATCH_00ad_1;
				}
				throw e;
			}

CATCH_00ad_1:
			{// begin catch(System.Exception)
				// Debug.LogException( e );
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
				Debug_LogException_m82E44FEC6B03BC34AFC2CAF6583051570C60CB9E(((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*)), NULL);
				// return null;
				V_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)NULL;
				IL2CPP_POP_ACTIVE_EXCEPTION();
				goto IL_00bd;
			}// end catch (depth: 2)
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00bd:
	{
		// }
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_32 = V_3;
		return L_32;
	}
}
// UnityEngine.Texture2D NativeGallery::LoadImageAtPath(System.String,System.Int32,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* NativeGallery_LoadImageAtPath_m8D5D9C1FD10E95E49E9889DE5C87A0699FDD7F82 (String_t* ___imagePath0, int32_t ___maxSize1, bool ___markTextureNonReadable2, bool ___generateMipmaps3, bool ___linearColorSpace4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* V_3 = NULL;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* V_4 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	int32_t G_B10_0 = 0;
	{
		// if( string.IsNullOrEmpty( imagePath ) )
		String_t* L_0 = ___imagePath0;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_0, NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		// throw new ArgumentException( "Parameter 'imagePath' is null or empty!" );
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_2 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_2);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral18B82B6B7DC4FE1988BA61A3784D1768F6C925DF)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_LoadImageAtPath_m8D5D9C1FD10E95E49E9889DE5C87A0699FDD7F82_RuntimeMethod_var)));
	}

IL_0013:
	{
		// if( !File.Exists( imagePath ) )
		String_t* L_3 = ___imagePath0;
		bool L_4;
		L_4 = File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85(L_3, NULL);
		if (L_4)
		{
			goto IL_002c;
		}
	}
	{
		// throw new FileNotFoundException( "File not found at " + imagePath );
		String_t* L_5 = ___imagePath0;
		String_t* L_6;
		L_6 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral491B4D9271839F0BD63211437BF7CEE5B2C6ADE9)), L_5, NULL);
		FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* L_7 = (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var)));
		NullCheck(L_7);
		FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1(L_7, L_6, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_LoadImageAtPath_m8D5D9C1FD10E95E49E9889DE5C87A0699FDD7F82_RuntimeMethod_var)));
	}

IL_002c:
	{
		// if( maxSize <= 0 )
		int32_t L_8 = ___maxSize1;
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		// maxSize = SystemInfo.maxTextureSize;
		int32_t L_9;
		L_9 = SystemInfo_get_maxTextureSize_mB4272D4D85179CEF11FF1CDB3E8F3786D10CA04E(NULL);
		___maxSize1 = L_9;
	}

IL_0037:
	{
		// string loadPath = _NativeGallery_LoadImageAtPath( imagePath, TemporaryImagePath, maxSize );
		String_t* L_10 = ___imagePath0;
		String_t* L_11;
		L_11 = NativeGallery_get_TemporaryImagePath_m32BA1844727D8D2D8F1AFC4FABC7D027CA42641C(NULL);
		int32_t L_12 = ___maxSize1;
		String_t* L_13;
		L_13 = NativeGallery__NativeGallery_LoadImageAtPath_m557D7EBC832C927A8DB4C4F3FE02697C8F99E5DF(L_10, L_11, L_12, NULL);
		V_0 = L_13;
		// String extension = Path.GetExtension( imagePath ).ToLowerInvariant();
		String_t* L_14 = ___imagePath0;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_15;
		L_15 = Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7(L_14, NULL);
		NullCheck(L_15);
		String_t* L_16;
		L_16 = String_ToLowerInvariant_mBE32C93DE27C5353FEA3FA654FC1DDBE3D0EB0F2(L_15, NULL);
		V_1 = L_16;
		// TextureFormat format = ( extension == ".jpg" || extension == ".jpeg" ) ? TextureFormat.RGB24 : TextureFormat.RGBA32;
		String_t* L_17 = V_1;
		bool L_18;
		L_18 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_17, _stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A, NULL);
		if (L_18)
		{
			goto IL_006d;
		}
	}
	{
		String_t* L_19 = V_1;
		bool L_20;
		L_20 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_19, _stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42, NULL);
		if (L_20)
		{
			goto IL_006d;
		}
	}
	{
		G_B10_0 = 4;
		goto IL_006e;
	}

IL_006d:
	{
		G_B10_0 = 3;
	}

IL_006e:
	{
		V_2 = G_B10_0;
		// Texture2D result = new Texture2D( 2, 2, format, generateMipmaps, linearColorSpace );
		int32_t L_21 = V_2;
		bool L_22 = ___generateMipmaps3;
		bool L_23 = ___linearColorSpace4;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_24 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		NullCheck(L_24);
		Texture2D__ctor_mC3F84195D1DCEFC0536B3FBD40A8F8E865A4F32A(L_24, 2, 2, L_21, L_22, L_23, NULL);
		V_3 = L_24;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00a7:
			{// begin finally (depth: 1)
				{
					// if( loadPath != imagePath )
					String_t* L_25 = V_0;
					String_t* L_26 = ___imagePath0;
					bool L_27;
					L_27 = String_op_Inequality_m0FBE5AC4931D312E5B347BAA603755676E6DA2FE(L_25, L_26, NULL);
					if (!L_27)
					{
						goto IL_00bb;
					}
				}
				try
				{// begin try (depth: 2)
					// File.Delete( loadPath );
					String_t* L_28 = V_0;
					File_Delete_mB5CB249F5370D49747243BCA9C56CCC48D3E6A08(L_28, NULL);
					// }
					goto IL_00bb;
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&RuntimeObject_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
					{
						IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
						goto CATCH_00b8;
					}
					throw e;
				}

CATCH_00b8:
				{// begin catch(System.Object)
					// catch { }
					// catch { }
					IL2CPP_POP_ACTIVE_EXCEPTION();
					goto IL_00bb;
				}// end catch (depth: 2)

IL_00bb:
				{
					// }
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			try
			{// begin try (depth: 2)
				{
					// if( !result.LoadImage( File.ReadAllBytes( loadPath ), markTextureNonReadable ) )
					Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_29 = V_3;
					String_t* L_30 = V_0;
					ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_31;
					L_31 = File_ReadAllBytes_mE0C2C5A0270CD40C496E85B1D8BA15C607E7011E(L_30, NULL);
					bool L_32 = ___markTextureNonReadable2;
					bool L_33;
					L_33 = ImageConversion_LoadImage_mE2D612F3895FDD7A87805E1C9D77A79C019213E2(L_29, L_31, L_32, NULL);
					if (L_33)
					{
						goto IL_0095_2;
					}
				}
				{
					// Object.DestroyImmediate( result );
					Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_34 = V_3;
					il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
					Object_DestroyImmediate_m8249CABCDF344BE3A67EE765122EBB415DC2BC57(L_34, NULL);
					// return null;
					V_4 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)NULL;
					goto IL_00be;
				}

IL_0095_2:
				{
					// }
					goto IL_00bc;
				}
			}// end try (depth: 2)
			catch(Il2CppExceptionWrapper& e)
			{
				if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				{
					IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
					goto CATCH_0097_1;
				}
				throw e;
			}

CATCH_0097_1:
			{// begin catch(System.Exception)
				// Debug.LogException( e );
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
				Debug_LogException_m82E44FEC6B03BC34AFC2CAF6583051570C60CB9E(((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*)), NULL);
				// Object.DestroyImmediate( result );
				Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_35 = V_3;
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var)));
				Object_DestroyImmediate_m8249CABCDF344BE3A67EE765122EBB415DC2BC57(L_35, NULL);
				// return null;
				V_4 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)NULL;
				IL2CPP_POP_ACTIVE_EXCEPTION();
				goto IL_00be;
			}// end catch (depth: 2)
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00bc:
	{
		// return result;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_36 = V_3;
		return L_36;
	}

IL_00be:
	{
		// }
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_37 = V_4;
		return L_37;
	}
}
// NativeGallery/ImageProperties NativeGallery::GetImageProperties(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90 NativeGallery_GetImageProperties_m56B49E642441A04CDD2606987B5F998ED6D9FA31 (String_t* ___imagePath0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0CA4721FC9D82D780671DE2AB61257837402697D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3E96C9BB1B953A85290371E8CE7BB3F3ABB307CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral75E05143EB132AAA8A22B48813DB8E6047380821);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral985B72B30ECE05DD4EF5FE142CEE0FB8BF53A98C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAFF4AA19F30B5DC5A240F413D92917103536F1AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB4507437E3E619ECBAD84410155675EBEB3DB3F);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_5 = NULL;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	{
		// if( !File.Exists( imagePath ) )
		String_t* L_0 = ___imagePath0;
		bool L_1;
		L_1 = File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85(L_0, NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		// throw new FileNotFoundException( "File not found at " + imagePath );
		String_t* L_2 = ___imagePath0;
		String_t* L_3;
		L_3 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral491B4D9271839F0BD63211437BF7CEE5B2C6ADE9)), L_2, NULL);
		FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* L_4 = (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var)));
		NullCheck(L_4);
		FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1(L_4, L_3, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_GetImageProperties_m56B49E642441A04CDD2606987B5F998ED6D9FA31_RuntimeMethod_var)));
	}

IL_0019:
	{
		// string value = _NativeGallery_GetImageProperties( imagePath );
		String_t* L_5 = ___imagePath0;
		String_t* L_6;
		L_6 = NativeGallery__NativeGallery_GetImageProperties_m4130139DD3C6DF9E3750094D85F2A88317E7DFC3(L_5, NULL);
		V_0 = L_6;
		// int width = 0, height = 0;
		V_1 = 0;
		// int width = 0, height = 0;
		V_2 = 0;
		// string mimeType = null;
		V_3 = (String_t*)NULL;
		// ImageOrientation orientation = ImageOrientation.Unknown;
		V_4 = (-1);
		// if( !string.IsNullOrEmpty( value ) )
		String_t* L_7 = V_0;
		bool L_8;
		L_8 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_7, NULL);
		if (L_8)
		{
			goto IL_0115;
		}
	}
	{
		// string[] properties = value.Split( '>' );
		String_t* L_9 = V_0;
		NullCheck(L_9);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10;
		L_10 = String_Split_m9530B73D02054692283BF35C3A27C8F2230946F4(L_9, ((int32_t)62), 0, NULL);
		V_5 = L_10;
		// if( properties != null && properties.Length >= 4 )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = V_5;
		if (!L_11)
		{
			goto IL_0115;
		}
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = V_5;
		NullCheck(L_12);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))) < ((int32_t)4)))
		{
			goto IL_0115;
		}
	}
	{
		// if( !int.TryParse( properties[0].Trim(), out width ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = 0;
		String_t* L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		String_t* L_16;
		L_16 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_15, NULL);
		bool L_17;
		L_17 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_16, (&V_1), NULL);
		if (L_17)
		{
			goto IL_0064;
		}
	}
	{
		// width = 0;
		V_1 = 0;
	}

IL_0064:
	{
		// if( !int.TryParse( properties[1].Trim(), out height ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = V_5;
		NullCheck(L_18);
		int32_t L_19 = 1;
		String_t* L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		String_t* L_21;
		L_21 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_20, NULL);
		bool L_22;
		L_22 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_21, (&V_2), NULL);
		if (L_22)
		{
			goto IL_0078;
		}
	}
	{
		// height = 0;
		V_2 = 0;
	}

IL_0078:
	{
		// mimeType = properties[2].Trim();
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = V_5;
		NullCheck(L_23);
		int32_t L_24 = 2;
		String_t* L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		String_t* L_26;
		L_26 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_25, NULL);
		V_3 = L_26;
		// if( mimeType.Length == 0 )
		String_t* L_27 = V_3;
		NullCheck(L_27);
		int32_t L_28;
		L_28 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_27, NULL);
		if (L_28)
		{
			goto IL_00ff;
		}
	}
	{
		// String extension = Path.GetExtension( imagePath ).ToLowerInvariant();
		String_t* L_29 = ___imagePath0;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_30;
		L_30 = Path_GetExtension_m52A28295599B87FA550D0654E531B56354C540C7(L_29, NULL);
		NullCheck(L_30);
		String_t* L_31;
		L_31 = String_ToLowerInvariant_mBE32C93DE27C5353FEA3FA654FC1DDBE3D0EB0F2(L_30, NULL);
		V_7 = L_31;
		// if( extension == ".png" )
		String_t* L_32 = V_7;
		bool L_33;
		L_33 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, _stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26, NULL);
		if (!L_33)
		{
			goto IL_00ad;
		}
	}
	{
		// mimeType = "image/png";
		V_3 = _stringLiteral75E05143EB132AAA8A22B48813DB8E6047380821;
		goto IL_00ff;
	}

IL_00ad:
	{
		// else if( extension == ".jpg" || extension == ".jpeg" )
		String_t* L_34 = V_7;
		bool L_35;
		L_35 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, _stringLiteral23DF9991B71463C240582D176E347E7E47AEFF5A, NULL);
		if (L_35)
		{
			goto IL_00c9;
		}
	}
	{
		String_t* L_36 = V_7;
		bool L_37;
		L_37 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_36, _stringLiteral4B9B40AAD718882F5C0B95FE844E4AA92BD49C42, NULL);
		if (!L_37)
		{
			goto IL_00d1;
		}
	}

IL_00c9:
	{
		// mimeType = "image/jpeg";
		V_3 = _stringLiteral3E96C9BB1B953A85290371E8CE7BB3F3ABB307CC;
		goto IL_00ff;
	}

IL_00d1:
	{
		// else if( extension == ".gif" )
		String_t* L_38 = V_7;
		bool L_39;
		L_39 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_38, _stringLiteral0CA4721FC9D82D780671DE2AB61257837402697D, NULL);
		if (!L_39)
		{
			goto IL_00e7;
		}
	}
	{
		// mimeType = "image/gif";
		V_3 = _stringLiteralAFF4AA19F30B5DC5A240F413D92917103536F1AD;
		goto IL_00ff;
	}

IL_00e7:
	{
		// else if( extension == ".bmp" )
		String_t* L_40 = V_7;
		bool L_41;
		L_41 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_40, _stringLiteral985B72B30ECE05DD4EF5FE142CEE0FB8BF53A98C, NULL);
		if (!L_41)
		{
			goto IL_00fd;
		}
	}
	{
		// mimeType = "image/bmp";
		V_3 = _stringLiteralCB4507437E3E619ECBAD84410155675EBEB3DB3F;
		goto IL_00ff;
	}

IL_00fd:
	{
		// mimeType = null;
		V_3 = (String_t*)NULL;
	}

IL_00ff:
	{
		// if( int.TryParse( properties[3].Trim(), out orientationInt ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_42 = V_5;
		NullCheck(L_42);
		int32_t L_43 = 3;
		String_t* L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		String_t* L_45;
		L_45 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_44, NULL);
		bool L_46;
		L_46 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_45, (&V_6), NULL);
		if (!L_46)
		{
			goto IL_0115;
		}
	}
	{
		// orientation = (ImageOrientation) orientationInt;
		int32_t L_47 = V_6;
		V_4 = L_47;
	}

IL_0115:
	{
		// return new ImageProperties( width, height, mimeType, orientation );
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		String_t* L_50 = V_3;
		int32_t L_51 = V_4;
		ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90 L_52;
		memset((&L_52), 0, sizeof(L_52));
		ImageProperties__ctor_m82A240218A9A84787616EE7D627864523555CA68((&L_52), L_48, L_49, L_50, L_51, /*hidden argument*/NULL);
		return L_52;
	}
}
// NativeGallery/VideoProperties NativeGallery::GetVideoProperties(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426 NativeGallery_GetVideoProperties_mF68A6E413B11767F89F0CBFD80808BF414E12D0F (String_t* ___videoPath0, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	float V_4 = 0.0f;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_5 = NULL;
	{
		// if( !File.Exists( videoPath ) )
		String_t* L_0 = ___videoPath0;
		bool L_1;
		L_1 = File_Exists_mD7E7A84A6B9E9A9BADBDA7C46AAE0624EF106D85(L_0, NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		// throw new FileNotFoundException( "File not found at " + videoPath );
		String_t* L_2 = ___videoPath0;
		String_t* L_3;
		L_3 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral491B4D9271839F0BD63211437BF7CEE5B2C6ADE9)), L_2, NULL);
		FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* L_4 = (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var)));
		NullCheck(L_4);
		FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1(L_4, L_3, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeGallery_GetVideoProperties_mF68A6E413B11767F89F0CBFD80808BF414E12D0F_RuntimeMethod_var)));
	}

IL_0019:
	{
		// string value = _NativeGallery_GetVideoProperties( videoPath );
		String_t* L_5 = ___videoPath0;
		String_t* L_6;
		L_6 = NativeGallery__NativeGallery_GetVideoProperties_m1230C4EEBA839FF1479E5B1206BB8EB812198B96(L_5, NULL);
		V_0 = L_6;
		// int width = 0, height = 0;
		V_1 = 0;
		// int width = 0, height = 0;
		V_2 = 0;
		// long duration = 0L;
		V_3 = ((int64_t)0);
		// float rotation = 0f;
		V_4 = (0.0f);
		// if( !string.IsNullOrEmpty( value ) )
		String_t* L_7 = V_0;
		bool L_8;
		L_8 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_7, NULL);
		if (L_8)
		{
			goto IL_00a2;
		}
	}
	{
		// string[] properties = value.Split( '>' );
		String_t* L_9 = V_0;
		NullCheck(L_9);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10;
		L_10 = String_Split_m9530B73D02054692283BF35C3A27C8F2230946F4(L_9, ((int32_t)62), 0, NULL);
		V_5 = L_10;
		// if( properties != null && properties.Length >= 4 )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = V_5;
		if (!L_11)
		{
			goto IL_00a2;
		}
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = V_5;
		NullCheck(L_12);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))) < ((int32_t)4)))
		{
			goto IL_00a2;
		}
	}
	{
		// if( !int.TryParse( properties[0].Trim(), out width ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = 0;
		String_t* L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		String_t* L_16;
		L_16 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_15, NULL);
		bool L_17;
		L_17 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_16, (&V_1), NULL);
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		// width = 0;
		V_1 = 0;
	}

IL_0060:
	{
		// if( !int.TryParse( properties[1].Trim(), out height ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = V_5;
		NullCheck(L_18);
		int32_t L_19 = 1;
		String_t* L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		String_t* L_21;
		L_21 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_20, NULL);
		bool L_22;
		L_22 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_21, (&V_2), NULL);
		if (L_22)
		{
			goto IL_0074;
		}
	}
	{
		// height = 0;
		V_2 = 0;
	}

IL_0074:
	{
		// if( !long.TryParse( properties[2].Trim(), out duration ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = V_5;
		NullCheck(L_23);
		int32_t L_24 = 2;
		String_t* L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		String_t* L_26;
		L_26 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_25, NULL);
		bool L_27;
		L_27 = Int64_TryParse_m61AAE5CC4A0B716556765798C22FE12D87554986(L_26, (&V_3), NULL);
		if (L_27)
		{
			goto IL_0089;
		}
	}
	{
		// duration = 0L;
		V_3 = ((int64_t)0);
	}

IL_0089:
	{
		// if( !float.TryParse( properties[3].Trim(), out rotation ) )
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = V_5;
		NullCheck(L_28);
		int32_t L_29 = 3;
		String_t* L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_30);
		String_t* L_31;
		L_31 = String_Trim_mCD6D8C6D4CFD15225D12DB7D3E0544CA80FB8DA5(L_30, NULL);
		bool L_32;
		L_32 = Single_TryParse_mF23E88B4B12DDC9E82179BB2483A714005BF006F(L_31, (&V_4), NULL);
		if (L_32)
		{
			goto IL_00a2;
		}
	}
	{
		// rotation = 0f;
		V_4 = (0.0f);
	}

IL_00a2:
	{
		// if( rotation == -90f )
		float L_33 = V_4;
		if ((!(((float)L_33) == ((float)(-90.0f)))))
		{
			goto IL_00b2;
		}
	}
	{
		// rotation = 270f;
		V_4 = (270.0f);
	}

IL_00b2:
	{
		// return new VideoProperties( width, height, duration, rotation );
		int32_t L_34 = V_1;
		int32_t L_35 = V_2;
		int64_t L_36 = V_3;
		float L_37 = V_4;
		VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426 L_38;
		memset((&L_38), 0, sizeof(L_38));
		VideoProperties__ctor_m8BF47B718C580EF51037607E56B20BE54AAACA8D((&L_38), L_34, L_35, L_36, L_37, /*hidden argument*/NULL);
		return L_38;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: NativeGallery/ImageProperties
IL2CPP_EXTERN_C void ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshal_pinvoke(const ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90& unmarshaled, ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_pinvoke& marshaled)
{
	marshaled.___width_0 = unmarshaled.___width_0;
	marshaled.___height_1 = unmarshaled.___height_1;
	marshaled.___mimeType_2 = il2cpp_codegen_marshal_string(unmarshaled.___mimeType_2);
	marshaled.___orientation_3 = unmarshaled.___orientation_3;
}
IL2CPP_EXTERN_C void ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshal_pinvoke_back(const ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_pinvoke& marshaled, ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90& unmarshaled)
{
	int32_t unmarshaledwidth_temp_0 = 0;
	unmarshaledwidth_temp_0 = marshaled.___width_0;
	unmarshaled.___width_0 = unmarshaledwidth_temp_0;
	int32_t unmarshaledheight_temp_1 = 0;
	unmarshaledheight_temp_1 = marshaled.___height_1;
	unmarshaled.___height_1 = unmarshaledheight_temp_1;
	unmarshaled.___mimeType_2 = il2cpp_codegen_marshal_string_result(marshaled.___mimeType_2);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___mimeType_2), (void*)il2cpp_codegen_marshal_string_result(marshaled.___mimeType_2));
	int32_t unmarshaledorientation_temp_3 = 0;
	unmarshaledorientation_temp_3 = marshaled.___orientation_3;
	unmarshaled.___orientation_3 = unmarshaledorientation_temp_3;
}
// Conversion method for clean up from marshalling of: NativeGallery/ImageProperties
IL2CPP_EXTERN_C void ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshal_pinvoke_cleanup(ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mimeType_2);
	marshaled.___mimeType_2 = NULL;
}
// Conversion methods for marshalling of: NativeGallery/ImageProperties
IL2CPP_EXTERN_C void ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshal_com(const ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90& unmarshaled, ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_com& marshaled)
{
	marshaled.___width_0 = unmarshaled.___width_0;
	marshaled.___height_1 = unmarshaled.___height_1;
	marshaled.___mimeType_2 = il2cpp_codegen_marshal_bstring(unmarshaled.___mimeType_2);
	marshaled.___orientation_3 = unmarshaled.___orientation_3;
}
IL2CPP_EXTERN_C void ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshal_com_back(const ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_com& marshaled, ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90& unmarshaled)
{
	int32_t unmarshaledwidth_temp_0 = 0;
	unmarshaledwidth_temp_0 = marshaled.___width_0;
	unmarshaled.___width_0 = unmarshaledwidth_temp_0;
	int32_t unmarshaledheight_temp_1 = 0;
	unmarshaledheight_temp_1 = marshaled.___height_1;
	unmarshaled.___height_1 = unmarshaledheight_temp_1;
	unmarshaled.___mimeType_2 = il2cpp_codegen_marshal_bstring_result(marshaled.___mimeType_2);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___mimeType_2), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___mimeType_2));
	int32_t unmarshaledorientation_temp_3 = 0;
	unmarshaledorientation_temp_3 = marshaled.___orientation_3;
	unmarshaled.___orientation_3 = unmarshaledorientation_temp_3;
}
// Conversion method for clean up from marshalling of: NativeGallery/ImageProperties
IL2CPP_EXTERN_C void ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshal_com_cleanup(ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___mimeType_2);
	marshaled.___mimeType_2 = NULL;
}
// System.Void NativeGallery/ImageProperties::.ctor(System.Int32,System.Int32,System.String,NativeGallery/ImageOrientation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageProperties__ctor_m82A240218A9A84787616EE7D627864523555CA68 (ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90* __this, int32_t ___width0, int32_t ___height1, String_t* ___mimeType2, int32_t ___orientation3, const RuntimeMethod* method) 
{
	{
		// this.width = width;
		int32_t L_0 = ___width0;
		__this->___width_0 = L_0;
		// this.height = height;
		int32_t L_1 = ___height1;
		__this->___height_1 = L_1;
		// this.mimeType = mimeType;
		String_t* L_2 = ___mimeType2;
		__this->___mimeType_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mimeType_2), (void*)L_2);
		// this.orientation = orientation;
		int32_t L_3 = ___orientation3;
		__this->___orientation_3 = L_3;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ImageProperties__ctor_m82A240218A9A84787616EE7D627864523555CA68_AdjustorThunk (RuntimeObject* __this, int32_t ___width0, int32_t ___height1, String_t* ___mimeType2, int32_t ___orientation3, const RuntimeMethod* method)
{
	ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ImageProperties_t8E83E560F7E7A7D52A5ADB1E30E612A3E8EC4B90*>(__this + _offset);
	ImageProperties__ctor_m82A240218A9A84787616EE7D627864523555CA68(_thisAdjusted, ___width0, ___height1, ___mimeType2, ___orientation3, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeGallery/VideoProperties::.ctor(System.Int32,System.Int32,System.Int64,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoProperties__ctor_m8BF47B718C580EF51037607E56B20BE54AAACA8D (VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426* __this, int32_t ___width0, int32_t ___height1, int64_t ___duration2, float ___rotation3, const RuntimeMethod* method) 
{
	{
		// this.width = width;
		int32_t L_0 = ___width0;
		__this->___width_0 = L_0;
		// this.height = height;
		int32_t L_1 = ___height1;
		__this->___height_1 = L_1;
		// this.duration = duration;
		int64_t L_2 = ___duration2;
		__this->___duration_2 = L_2;
		// this.rotation = rotation;
		float L_3 = ___rotation3;
		__this->___rotation_3 = L_3;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void VideoProperties__ctor_m8BF47B718C580EF51037607E56B20BE54AAACA8D_AdjustorThunk (RuntimeObject* __this, int32_t ___width0, int32_t ___height1, int64_t ___duration2, float ___rotation3, const RuntimeMethod* method)
{
	VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<VideoProperties_tBCBB524ACBF4165D44020BC8A01AF42687AED426*>(__this + _offset);
	VideoProperties__ctor_m8BF47B718C580EF51037607E56B20BE54AAACA8D(_thisAdjusted, ___width0, ___height1, ___duration2, ___rotation3, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_Multicast(MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* currentDelegate = reinterpret_cast<MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___error0, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_Open(MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___error0, method);
}
void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_OpenStaticInvoker(MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method)
{
	InvokerActionInvoker1< String_t* >::Invoke(__this->___method_ptr_0, method, NULL, ___error0);
}
void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_ClosedStaticInvoker(MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___error0);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___error0' to native representation
	char* ____error0_marshaled = NULL;
	____error0_marshaled = il2cpp_codegen_marshal_string(___error0);

	// Native function invocation
	il2cppPInvokeFunc(____error0_marshaled);

	// Marshaling cleanup of parameter '___error0' native representation
	il2cpp_codegen_marshal_free(____error0_marshaled);
	____error0_marshaled = NULL;

}
// System.Void NativeGallery/MediaSaveCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaSaveCallback__ctor_mC011B3FD5347FB732BF5EA250EDD1503A53B444E (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_Open;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_Open;
		}
		else
		{
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_Multicast;
}
// System.Void NativeGallery/MediaSaveCallback::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2 (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___error0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult NativeGallery/MediaSaveCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MediaSaveCallback_BeginInvoke_m44886E81B6F4CD8FCDB5167AEDE2D91AD270BC65 (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___callback1, RuntimeObject* ___object2, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___error0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void NativeGallery/MediaSaveCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaSaveCallback_EndInvoke_m4989324ED8DCD9D1CD82F200F1B6451AB93CB32B (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, RuntimeObject* ___result0, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_Multicast(MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* currentDelegate = reinterpret_cast<MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___path0, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_Open(MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___path0, method);
}
void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_OpenStaticInvoker(MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method)
{
	InvokerActionInvoker1< String_t* >::Invoke(__this->___method_ptr_0, method, NULL, ___path0);
}
void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_ClosedStaticInvoker(MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___path0);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	il2cppPInvokeFunc(____path0_marshaled);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

}
// System.Void NativeGallery/MediaPickCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickCallback__ctor_m3ABAD206DA5856E6B9FAA8E0D738A9108FBBD216 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_Open;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_Open;
		}
		else
		{
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_Multicast;
}
// System.Void NativeGallery/MediaPickCallback::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996 (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___path0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult NativeGallery/MediaPickCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MediaPickCallback_BeginInvoke_m12090C5295297E0B2A03344B15299A1A040ED03B (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___callback1, RuntimeObject* ___object2, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___path0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void NativeGallery/MediaPickCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickCallback_EndInvoke_m667C8D168461B718B37AEA47CC8C596E16099BCB (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, RuntimeObject* ___result0, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_Multicast(MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* currentDelegate = reinterpret_cast<MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___paths0, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_Open(MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___paths0, method);
}
void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_OpenStaticInvoker(MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method)
{
	InvokerActionInvoker1< StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* >::Invoke(__this->___method_ptr_0, method, NULL, ___paths0);
}
void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_ClosedStaticInvoker(MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___paths0);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401 (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char**);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___paths0' to native representation
	char** ____paths0_marshaled = NULL;
	if (___paths0 != NULL)
	{
		il2cpp_array_size_t ____paths0_Length = (___paths0)->max_length;
		____paths0_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paths0_Length + 1);
		(____paths0_marshaled)[____paths0_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paths0_Length); i++)
		{
			(____paths0_marshaled)[i] = il2cpp_codegen_marshal_string((___paths0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paths0_marshaled = NULL;
	}

	// Native function invocation
	il2cppPInvokeFunc(____paths0_marshaled);

	// Marshaling cleanup of parameter '___paths0' native representation
	if (____paths0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paths0_marshaled_CleanupLoopCount = (___paths0 != NULL) ? (___paths0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paths0_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paths0_marshaled)[i]);
			(____paths0_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paths0_marshaled);
		____paths0_marshaled = NULL;
	}

}
// System.Void NativeGallery/MediaPickMultipleCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickMultipleCallback__ctor_m3405A6AAC7FCAAA381A49D4C1EB364CE5295223A (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_Open;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_Open;
		}
		else
		{
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_Multicast;
}
// System.Void NativeGallery/MediaPickMultipleCallback::Invoke(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096 (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___paths0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult NativeGallery/MediaPickMultipleCallback::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MediaPickMultipleCallback_BeginInvoke_m8C3586682273CF5E635B41C3F9A15457487C893B (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___callback1, RuntimeObject* ___object2, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___paths0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void NativeGallery/MediaPickMultipleCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickMultipleCallback_EndInvoke_mB29E2B1DD9C48424492115DAD317663E3719F25A (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, RuntimeObject* ___result0, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AndroidPermissionCallback::add_OnPermissionGrantedAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_add_OnPermissionGrantedAction_m44E6F5E74954131D0E5A71EDCF2429760A3400E6 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionGrantedAction_4;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___OnPermissionGrantedAction_4);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void AndroidPermissionCallback::remove_OnPermissionGrantedAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_remove_OnPermissionGrantedAction_m3AB91BDF321365F3DA2C8755C1550125242C9519 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionGrantedAction_4;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___OnPermissionGrantedAction_4);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void AndroidPermissionCallback::add_OnPermissionDeniedAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_add_OnPermissionDeniedAction_mAF41F02A3DACD3653F4B8FC5CD90CFFC0946B999 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionDeniedAction_5;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___OnPermissionDeniedAction_5);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void AndroidPermissionCallback::remove_OnPermissionDeniedAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_remove_OnPermissionDeniedAction_mE1B6E461A7B22C1BF09CFB14FA98544031691CA1 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionDeniedAction_5;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___OnPermissionDeniedAction_5);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void AndroidPermissionCallback::add_OnPermissionDeniedAndDontAskAgainAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_add_OnPermissionDeniedAndDontAskAgainAction_m6141AEA2C6889368FCFA0A6EB0A95FB773D32BB8 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionDeniedAndDontAskAgainAction_6;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___OnPermissionDeniedAndDontAskAgainAction_6);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void AndroidPermissionCallback::remove_OnPermissionDeniedAndDontAskAgainAction(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_remove_OnPermissionDeniedAndDontAskAgainAction_m8F1001BE8E9CC0B083C770927B9C19BE5A57482B (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionDeniedAndDontAskAgainAction_6;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___OnPermissionDeniedAndDontAskAgainAction_6);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void AndroidPermissionCallback::.ctor(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback__ctor_mC050EA0B34D96B54DAB4EA1352A92569847C05F4 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onGrantedCallback0, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onDeniedCallback1, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onDeniedAndDontAskAgainCallback2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36F1A6EA85D2491AD75EA710ED443F78D6D39328);
		s_Il2CppMethodInitialized = true;
	}
	{
		// : base("com.unity3d.plugin.UnityAndroidPermissions$IPermissionRequestResult2")
		il2cpp_codegen_runtime_class_init_inline(AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D_il2cpp_TypeInfo_var);
		AndroidJavaProxy__ctor_m2832886A0E1BBF6702653A7C6A4609F11FB712C7(__this, _stringLiteral36F1A6EA85D2491AD75EA710ED443F78D6D39328, NULL);
		// if (onGrantedCallback != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = ___onGrantedCallback0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// OnPermissionGrantedAction += onGrantedCallback;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = ___onGrantedCallback0;
		AndroidPermissionCallback_add_OnPermissionGrantedAction_m44E6F5E74954131D0E5A71EDCF2429760A3400E6(__this, L_1, NULL);
	}

IL_0015:
	{
		// if (onDeniedCallback != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = ___onDeniedCallback1;
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// OnPermissionDeniedAction += onDeniedCallback;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___onDeniedCallback1;
		AndroidPermissionCallback_add_OnPermissionDeniedAction_mAF41F02A3DACD3653F4B8FC5CD90CFFC0946B999(__this, L_3, NULL);
	}

IL_001f:
	{
		// if (onDeniedAndDontAskAgainCallback != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = ___onDeniedAndDontAskAgainCallback2;
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		// OnPermissionDeniedAndDontAskAgainAction += onDeniedAndDontAskAgainCallback;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_5 = ___onDeniedAndDontAskAgainCallback2;
		AndroidPermissionCallback_add_OnPermissionDeniedAndDontAskAgainAction_m6141AEA2C6889368FCFA0A6EB0A95FB773D32BB8(__this, L_5, NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void AndroidPermissionCallback::OnPermissionGranted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_OnPermissionGranted_mA9E0912550D3EE6EBC0B63A774CA1BDBDB108A2E (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, String_t* ___permissionName0, const RuntimeMethod* method) 
{
	{
		// if (OnPermissionGrantedAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionGrantedAction_4;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// OnPermissionGrantedAction(permissionName);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___OnPermissionGrantedAction_4;
		String_t* L_2 = ___permissionName0;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void AndroidPermissionCallback::OnPermissionDenied(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_OnPermissionDenied_mCA9DCC4622977EAA455A891BF5AB6D54223639B8 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, String_t* ___permissionName0, const RuntimeMethod* method) 
{
	{
		// if (OnPermissionDeniedAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionDeniedAction_5;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// OnPermissionDeniedAction(permissionName);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___OnPermissionDeniedAction_5;
		String_t* L_2 = ___permissionName0;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void AndroidPermissionCallback::OnPermissionDeniedAndDontAskAgain(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionCallback_OnPermissionDeniedAndDontAskAgain_m8095A33F4AAD0A4A2188D8EDF95866AF1B67CE94 (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* __this, String_t* ___permissionName0, const RuntimeMethod* method) 
{
	{
		// if (OnPermissionDeniedAndDontAskAgainAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___OnPermissionDeniedAndDontAskAgainAction_6;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// OnPermissionDeniedAndDontAskAgainAction(permissionName);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___OnPermissionDeniedAndDontAskAgainAction_6;
		String_t* L_2 = ___permissionName0;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
		return;
	}

IL_0015:
	{
		// else if (OnPermissionDeniedAction != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = __this->___OnPermissionDeniedAction_5;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		// OnPermissionDeniedAction(permissionName);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = __this->___OnPermissionDeniedAction_5;
		String_t* L_5 = ___permissionName0;
		NullCheck(L_4);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_4, L_5, NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.AndroidJavaObject AndroidPermissionsManager::GetActivity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* AndroidPermissionsManager_GetActivity_m3209BA56C4C200CD1E4D9A421A0E03E1C3636CE9 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaObject_GetStatic_TisAndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_mD7D192A35EB2B2DA3775FAB081958B72088251DD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Activity == null)
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_0 = ((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_Activity_0;
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		// var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* L_1 = (AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03*)il2cpp_codegen_object_new(AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		AndroidJavaClass__ctor_mB5466169E1151B8CC44C8FED234D79984B431389(L_1, _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078, NULL);
		// m_Activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		NullCheck(L_1);
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_2;
		L_2 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_mD7D192A35EB2B2DA3775FAB081958B72088251DD(L_1, _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9, AndroidJavaObject_GetStatic_TisAndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_mD7D192A35EB2B2DA3775FAB081958B72088251DD_RuntimeMethod_var);
		((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_Activity_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_Activity_0), (void*)L_2);
	}

IL_0020:
	{
		// return m_Activity;
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_3 = ((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_Activity_0;
		return L_3;
	}
}
// UnityEngine.AndroidJavaObject AndroidPermissionsManager::GetPermissionsService()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* AndroidPermissionsManager_GetPermissionsService_m3E7C258986D3D1CCA125A14B60B631A4CE7BB249 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral78890CE9DBB1337BC5B353A9D2ECF645ABCDC860);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* G_B2_0 = NULL;
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* G_B1_0 = NULL;
	{
		// return m_PermissionService ??
		//     (m_PermissionService = new AndroidJavaObject("com.unity3d.plugin.UnityAndroidPermissions"));
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_0 = ((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_PermissionService_1;
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001e;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_2;
		L_2 = Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_inline(Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_RuntimeMethod_var);
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_3 = (AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0*)il2cpp_codegen_object_new(AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		AndroidJavaObject__ctor_m5A65B5D325C2CEFAC4097A0D3813F8E158178DD7(L_3, _stringLiteral78890CE9DBB1337BC5B353A9D2ECF645ABCDC860, L_2, NULL);
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_4 = L_3;
		((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_PermissionService_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_StaticFields*)il2cpp_codegen_static_fields_for(AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8_il2cpp_TypeInfo_var))->___m_PermissionService_1), (void*)L_4);
		G_B2_0 = L_4;
	}

IL_001e:
	{
		return G_B2_0;
	}
}
// System.Boolean AndroidPermissionsManager::IsPermissionGranted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidPermissionsManager_IsPermissionGranted_m98B2891605E6A1CEEFBDFAE7263DD6A3449287B9 (String_t* ___permissionName0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7D22511CC292B1C86526CD5212677E0053AC1C87);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return GetPermissionsService().Call<bool>("IsPermissionGranted", GetActivity(), permissionName);
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_0;
		L_0 = AndroidPermissionsManager_GetPermissionsService_m3E7C258986D3D1CCA125A14B60B631A4CE7BB249(NULL);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_2 = L_1;
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_3;
		L_3 = AndroidPermissionsManager_GetActivity_m3209BA56C4C200CD1E4D9A421A0E03E1C3636CE9(NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_3);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_2;
		String_t* L_5 = ___permissionName0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_5);
		NullCheck(L_0);
		bool L_6;
		L_6 = AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF(L_0, _stringLiteral7D22511CC292B1C86526CD5212677E0053AC1C87, L_4, AndroidJavaObject_Call_TisBoolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_m05D3284A3FA772D032190A0FE82363C61000F1DF_RuntimeMethod_var);
		return L_6;
	}
}
// System.Void AndroidPermissionsManager::RequestPermission(System.String,AndroidPermissionCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsManager_RequestPermission_mDA7F0DFB3C5B6E68DF370AFE9E512C151BD7FA55 (String_t* ___permissionName0, AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* ___callback1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// RequestPermission(new[] {permissionName}, callback);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		String_t* L_2 = ___permissionName0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_2);
		AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* L_3 = ___callback1;
		AndroidPermissionsManager_RequestPermission_mE1C5F9692C4F4E0FF709553367E5CBF735C53BE5(L_1, L_3, NULL);
		// }
		return;
	}
}
// System.Void AndroidPermissionsManager::RequestPermission(System.String[],AndroidPermissionCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsManager_RequestPermission_mE1C5F9692C4F4E0FF709553367E5CBF735C53BE5 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___permissionNames0, AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* ___callback1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D147215C82FF43E4366FAF1CD51F52CFE8DF7EF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetPermissionsService().Call("RequestPermissionAsync", GetActivity(), permissionNames, callback);
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_0;
		L_0 = AndroidPermissionsManager_GetPermissionsService_m3E7C258986D3D1CCA125A14B60B631A4CE7BB249(NULL);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)3);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_2 = L_1;
		AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* L_3;
		L_3 = AndroidPermissionsManager_GetActivity_m3209BA56C4C200CD1E4D9A421A0E03E1C3636CE9(NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_3);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = ___permissionNames0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_5);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = L_4;
		AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* L_7 = ___callback1;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_7);
		NullCheck(L_0);
		AndroidJavaObject_Call_mDEF7846E2AB1C5379069BB21049ED55A9D837B1C(L_0, _stringLiteral4D147215C82FF43E4366FAF1CD51F52CFE8DF7EF, L_6, NULL);
		// }
		return;
	}
}
// System.Void AndroidPermissionsManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsManager__ctor_mBA1962ACF75E88E9153E4ED4361F6DC8949E8F31 (AndroidPermissionsManager_tB3B6F9A732694F15CCE517C8F07A036FBC1C08A8* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AndroidPermissionsUsageExample::OnBrowseGalleryButtonPress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsUsageExample_OnBrowseGalleryButtonPress_mF935173F287EC2F2E2ABF2918C2CD511644006CF (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral133352F79A1BF69B9869BD9AD412DDFD203002D4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB41F48CD412D791DA9AC5C2A4F59FAD4CEFEDE8B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!CheckPermissions())
		bool L_0;
		L_0 = AndroidPermissionsUsageExample_CheckPermissions_m16510B9ADE6A8B12197040D2D23AB1F166D9C94D(__this, NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// Debug.LogWarning("Missing permission to browse device gallery, please grant the permission first");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28(_stringLiteralB41F48CD412D791DA9AC5C2A4F59FAD4CEFEDE8B, NULL);
		// return;
		return;
	}

IL_0013:
	{
		// Debug.Log("Browsing Gallery...");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteral133352F79A1BF69B9869BD9AD412DDFD203002D4, NULL);
		// }
		return;
	}
}
// System.Boolean AndroidPermissionsUsageExample::CheckPermissions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidPermissionsUsageExample_CheckPermissions_m16510B9ADE6A8B12197040D2D23AB1F166D9C94D (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB2113A207765BA2D8ABB7F50B4388B872AC1E2D2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Application.platform != RuntimePlatform.Android)
		int32_t L_0;
		L_0 = Application_get_platform_m1AB34E71D9885B120F6021EB2B11DCB28CD6008D(NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_000b;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_000b:
	{
		// return AndroidPermissionsManager.IsPermissionGranted(STORAGE_PERMISSION);
		bool L_1;
		L_1 = AndroidPermissionsManager_IsPermissionGranted_m98B2891605E6A1CEEFBDFAE7263DD6A3449287B9(_stringLiteralB2113A207765BA2D8ABB7F50B4388B872AC1E2D2, NULL);
		return L_1;
	}
}
// System.Void AndroidPermissionsUsageExample::OnGrantButtonPress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsUsageExample_OnGrantButtonPress_m3B2053B50824461FE5C9CFAEB2F94566D4900A2B (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidPermissionsUsageExample_U3COnGrantButtonPressU3Eb__3_0_m6A17A2F63AF061CFF345D721B1226FF6CEE64B24_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3COnGrantButtonPressU3Eb__3_1_mB1A74CA187DCCF123410C5512FC4E0474A47063B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3COnGrantButtonPressU3Eb__3_2_m1620A2686F99CB67A19A4B869CCC65740C4B49A6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB2113A207765BA2D8ABB7F50B4388B872AC1E2D2);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B2_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B2_1 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* G_B2_2 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B1_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B1_1 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* G_B1_2 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B4_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B4_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B4_2 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* G_B4_3 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B3_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B3_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B3_2 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* G_B3_3 = NULL;
	{
		// AndroidPermissionsManager.RequestPermission(new []{STORAGE_PERMISSION}, new AndroidPermissionCallback(
		//     grantedPermission =>
		//     {
		//         // The permission was successfully granted, restart the change avatar routine
		//         OnBrowseGalleryButtonPress();
		//     },
		//     deniedPermission =>
		//     {
		//         // The permission was denied
		//     },
		//     deniedPermissionAndDontAskAgain =>
		//     {
		//         // The permission was denied, and the user has selected "Don't ask again"
		//         // Show in-game pop-up message stating that the user can change permissions in Android Application Settings
		//         // if he changes his mind (also required by Google Featuring program)
		//     }));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralB2113A207765BA2D8ABB7F50B4388B872AC1E2D2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB2113A207765BA2D8ABB7F50B4388B872AC1E2D2);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)il2cpp_codegen_object_new(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Action_1__ctor_m9DC2953C55C4D7D4B7BEFE03D84DA1F9362D652C(L_2, __this, (intptr_t)((void*)AndroidPermissionsUsageExample_U3COnGrantButtonPressU3Eb__3_0_m6A17A2F63AF061CFF345D721B1226FF6CEE64B24_RuntimeMethod_var), NULL);
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9__3_1_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		G_B1_2 = L_1;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* L_5 = ((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)il2cpp_codegen_object_new(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Action_1__ctor_m9DC2953C55C4D7D4B7BEFE03D84DA1F9362D652C(L_6, L_5, (intptr_t)((void*)U3CU3Ec_U3COnGrantButtonPressU3Eb__3_1_mB1A74CA187DCCF123410C5512FC4E0474A47063B_RuntimeMethod_var), NULL);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = L_6;
		((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9__3_1_1 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9__3_1_1), (void*)L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0039:
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8 = ((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9__3_2_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = L_8;
		G_B3_0 = L_9;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		if (L_9)
		{
			G_B4_0 = L_9;
			G_B4_1 = G_B2_0;
			G_B4_2 = G_B2_1;
			G_B4_3 = G_B2_2;
			goto IL_0058;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* L_10 = ((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_11 = (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)il2cpp_codegen_object_new(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		Action_1__ctor_m9DC2953C55C4D7D4B7BEFE03D84DA1F9362D652C(L_11, L_10, (intptr_t)((void*)U3CU3Ec_U3COnGrantButtonPressU3Eb__3_2_m1620A2686F99CB67A19A4B869CCC65740C4B49A6_RuntimeMethod_var), NULL);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_12 = L_11;
		((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9__3_2_2 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9__3_2_2), (void*)L_12);
		G_B4_0 = L_12;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		G_B4_3 = G_B3_3;
	}

IL_0058:
	{
		AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2* L_13 = (AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2*)il2cpp_codegen_object_new(AndroidPermissionCallback_tD36724202FB04D9D7D0CACFCA30D77AC829E6EE2_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		AndroidPermissionCallback__ctor_mC050EA0B34D96B54DAB4EA1352A92569847C05F4(L_13, G_B4_2, G_B4_1, G_B4_0, NULL);
		AndroidPermissionsManager_RequestPermission_mE1C5F9692C4F4E0FF709553367E5CBF735C53BE5(G_B4_3, L_13, NULL);
		// }
		return;
	}
}
// System.Void AndroidPermissionsUsageExample::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsUsageExample__ctor_mAE46556AE08DE70A1468AF964C98B46F29BE266D (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void AndroidPermissionsUsageExample::<OnGrantButtonPress>b__3_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidPermissionsUsageExample_U3COnGrantButtonPressU3Eb__3_0_m6A17A2F63AF061CFF345D721B1226FF6CEE64B24 (AndroidPermissionsUsageExample_tBEC5F837574B2FB5F8FB87902D9161436B5AFD1D* __this, String_t* ___grantedPermission0, const RuntimeMethod* method) 
{
	{
		// OnBrowseGalleryButtonPress();
		AndroidPermissionsUsageExample_OnBrowseGalleryButtonPress_mF935173F287EC2F2E2ABF2918C2CD511644006CF(__this, NULL);
		// },
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AndroidPermissionsUsageExample/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mE1F3517EA5A0462BE5846F917BA367602AECA335 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* L_0 = (U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241*)il2cpp_codegen_object_new(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__ctor_mEAE1E9212598050ACEFFB064B66AA4D702409461(L_0, NULL);
		((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void AndroidPermissionsUsageExample/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEAE1E9212598050ACEFFB064B66AA4D702409461 (U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void AndroidPermissionsUsageExample/<>c::<OnGrantButtonPress>b__3_1(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3COnGrantButtonPressU3Eb__3_1_mB1A74CA187DCCF123410C5512FC4E0474A47063B (U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* __this, String_t* ___deniedPermission0, const RuntimeMethod* method) 
{
	{
		// },
		return;
	}
}
// System.Void AndroidPermissionsUsageExample/<>c::<OnGrantButtonPress>b__3_2(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3COnGrantButtonPressU3Eb__3_2_m1620A2686F99CB67A19A4B869CCC65740C4B49A6 (U3CU3Ec_t229AFBE034BB26A8231254B73C38B6F991F1B241* __this, String_t* ___deniedPermissionAndDontAskAgain0, const RuntimeMethod* method) 
{
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean NativeGalleryNamespace.NGMediaReceiveCallbackiOS::get_IsBusy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NGMediaReceiveCallbackiOS_get_IsBusy_mAECD29ABE59BCEA398CBE4E9920C1F4A0FADE428 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool IsBusy { get; private set; }
		bool L_0 = ((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___U3CIsBusyU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::set_IsBusy(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_set_IsBusy_m1A7EE6ECFA48392D1162D1EFAA5402E343762731 (bool ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool IsBusy { get; private set; }
		bool L_0 = ___value0;
		((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___U3CIsBusyU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Int32 NativeGalleryNamespace.NGMediaReceiveCallbackiOS::_NativeGallery_IsMediaPickerBusy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_mA785D9122A92E8F0C3179725A475AFED74349AAF (const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_NativeGallery_IsMediaPickerBusy)();

	return returnValue;
}
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Initialize(NativeGallery/MediaPickCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_Initialize_m5825805938A7EB8FC2A1697CD11C2E98F4F95D6D (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* ___callback0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisNGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_mF5EFD3397ADDE73CB43C1A935AB7D0D6A1C29082_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7E90946BC7628636C60D094F905E96CD1360A930);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( IsBusy )
		bool L_0;
		L_0 = NGMediaReceiveCallbackiOS_get_IsBusy_mAECD29ABE59BCEA398CBE4E9920C1F4A0FADE428_inline(NULL);
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// return;
		return;
	}

IL_0008:
	{
		// if( instance == null )
		NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* L_1 = ((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		// instance = new GameObject( "NGMediaReceiveCallbackiOS" ).AddComponent<NGMediaReceiveCallbackiOS>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)il2cpp_codegen_object_new(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		GameObject__ctor_m37D512B05D292F954792225E6C6EEE95293A9B88(L_3, _stringLiteral7E90946BC7628636C60D094F905E96CD1360A930, NULL);
		NullCheck(L_3);
		NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* L_4;
		L_4 = GameObject_AddComponent_TisNGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_mF5EFD3397ADDE73CB43C1A935AB7D0D6A1C29082(L_3, GameObject_AddComponent_TisNGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_mF5EFD3397ADDE73CB43C1A935AB7D0D6A1C29082_RuntimeMethod_var);
		((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___instance_4 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___instance_4), (void*)L_4);
		// DontDestroyOnLoad( instance.gameObject );
		NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* L_5 = ((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___instance_4;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_5, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m303AA1C4DC810349F285B4809E426CBBA8F834F9(L_6, NULL);
	}

IL_0038:
	{
		// instance.callback = callback;
		NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* L_7 = ((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___instance_4;
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_8 = ___callback0;
		NullCheck(L_7);
		L_7->___callback_5 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&L_7->___callback_5), (void*)L_8);
		// instance.nextBusyCheckTime = Time.realtimeSinceStartup + 1f;
		NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* L_9 = ((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___instance_4;
		float L_10;
		L_10 = Time_get_realtimeSinceStartup_mB49A5622E38FFE9589EB9B3E75573E443B8D63EC(NULL);
		NullCheck(L_9);
		L_9->___nextBusyCheckTime_6 = ((float)il2cpp_codegen_add(L_10, (1.0f)));
		// IsBusy = true;
		NGMediaReceiveCallbackiOS_set_IsBusy_m1A7EE6ECFA48392D1162D1EFAA5402E343762731_inline((bool)1, NULL);
		// }
		return;
	}
}
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_Update_m627A0A644709476C204B2DE3B5E1C66F5997EE00 (NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* __this, const RuntimeMethod* method) 
{
	{
		// if( IsBusy )
		bool L_0;
		L_0 = NGMediaReceiveCallbackiOS_get_IsBusy_mAECD29ABE59BCEA398CBE4E9920C1F4A0FADE428_inline(NULL);
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		// if( Time.realtimeSinceStartup >= nextBusyCheckTime )
		float L_1;
		L_1 = Time_get_realtimeSinceStartup_mB49A5622E38FFE9589EB9B3E75573E443B8D63EC(NULL);
		float L_2 = __this->___nextBusyCheckTime_6;
		if ((!(((float)L_1) >= ((float)L_2))))
		{
			goto IL_004d;
		}
	}
	{
		// nextBusyCheckTime = Time.realtimeSinceStartup + 1f;
		float L_3;
		L_3 = Time_get_realtimeSinceStartup_mB49A5622E38FFE9589EB9B3E75573E443B8D63EC(NULL);
		__this->___nextBusyCheckTime_6 = ((float)il2cpp_codegen_add(L_3, (1.0f)));
		// if( _NativeGallery_IsMediaPickerBusy() == 0 )
		int32_t L_4;
		L_4 = NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_mA785D9122A92E8F0C3179725A475AFED74349AAF(NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		// IsBusy = false;
		NGMediaReceiveCallbackiOS_set_IsBusy_m1A7EE6ECFA48392D1162D1EFAA5402E343762731_inline((bool)0, NULL);
		// if( callback != null )
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_5 = __this->___callback_5;
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		// callback( null );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_6 = __this->___callback_5;
		NullCheck(L_6);
		MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_inline(L_6, (String_t*)NULL, NULL);
		// callback = null;
		__this->___callback_5 = (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___callback_5), (void*)(MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D*)NULL);
	}

IL_004d:
	{
		// }
		return;
	}
}
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::OnMediaReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_OnMediaReceived_m197A91808CA0788ED4D40B1113121C51946507E9 (NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* __this, String_t* ___path0, const RuntimeMethod* method) 
{
	{
		// IsBusy = false;
		NGMediaReceiveCallbackiOS_set_IsBusy_m1A7EE6ECFA48392D1162D1EFAA5402E343762731_inline((bool)0, NULL);
		// if( string.IsNullOrEmpty( path ) )
		String_t* L_0 = ___path0;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_0, NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		// path = null;
		___path0 = (String_t*)NULL;
	}

IL_0011:
	{
		// if( callback != null )
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_2 = __this->___callback_5;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		// callback( path );
		MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* L_3 = __this->___callback_5;
		String_t* L_4 = ___path0;
		NullCheck(L_3);
		MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_inline(L_3, L_4, NULL);
		// callback = null;
		__this->___callback_5 = (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___callback_5), (void*)(MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D*)NULL);
	}

IL_002c:
	{
		// }
		return;
	}
}
// System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS__ctor_m50F4043DF55BEB8A46FDC9FD894EB274893FFF7F (NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::Initialize(NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaSaveCallbackiOS_Initialize_mE5AC16A419E72FF81137DF8977D5A909D6E79A10 (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* ___callback0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisNGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_m8EC160CD8F792A4F3207AD92BB7F66059B644F22_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A79B0229EB4F27A4505CE75160E9133E944F111);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( instance == null )
		NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* L_0 = ((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		// instance = new GameObject( "NGMediaSaveCallbackiOS" ).AddComponent<NGMediaSaveCallbackiOS>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)il2cpp_codegen_object_new(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		GameObject__ctor_m37D512B05D292F954792225E6C6EEE95293A9B88(L_2, _stringLiteral0A79B0229EB4F27A4505CE75160E9133E944F111, NULL);
		NullCheck(L_2);
		NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* L_3;
		L_3 = GameObject_AddComponent_TisNGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_m8EC160CD8F792A4F3207AD92BB7F66059B644F22(L_2, GameObject_AddComponent_TisNGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_m8EC160CD8F792A4F3207AD92BB7F66059B644F22_RuntimeMethod_var);
		((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4), (void*)L_3);
		// DontDestroyOnLoad( instance.gameObject );
		NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* L_4 = ((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m303AA1C4DC810349F285B4809E426CBBA8F834F9(L_5, NULL);
		goto IL_004e;
	}

IL_0032:
	{
		// else if( instance.callback != null )
		NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* L_6 = ((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4;
		NullCheck(L_6);
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_7 = L_6->___callback_5;
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		// instance.callback( null );
		NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* L_8 = ((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4;
		NullCheck(L_8);
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_9 = L_8->___callback_5;
		NullCheck(L_9);
		MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_inline(L_9, (String_t*)NULL, NULL);
	}

IL_004e:
	{
		// instance.callback = callback;
		NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* L_10 = ((NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE_il2cpp_TypeInfo_var))->___instance_4;
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_11 = ___callback0;
		NullCheck(L_10);
		L_10->___callback_5 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&L_10->___callback_5), (void*)L_11);
		// }
		return;
	}
}
// System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::OnMediaSaveCompleted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaSaveCallbackiOS_OnMediaSaveCompleted_mA87A26260D638B8C12FA9C368B24B3636BF3635C (NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	{
		// if( callback != null )
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_0 = __this->___callback_5;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		// callback( null );
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_1 = __this->___callback_5;
		NullCheck(L_1);
		MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_inline(L_1, (String_t*)NULL, NULL);
		// callback = null;
		__this->___callback_5 = (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___callback_5), (void*)(MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD*)NULL);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::OnMediaSaveFailed(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaSaveCallbackiOS_OnMediaSaveFailed_m126F0823281CCD6237CE9F59AE8ED897375E5A49 (NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* __this, String_t* ___error0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF47527D7B72D5957FA184B6B1B6A7A1A9A56BF37);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( string.IsNullOrEmpty( error ) )
		String_t* L_0 = ___error0;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_0, NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// error = "Unknown error";
		___error0 = _stringLiteralF47527D7B72D5957FA184B6B1B6A7A1A9A56BF37;
	}

IL_000f:
	{
		// if( callback != null )
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_2 = __this->___callback_5;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		// callback( error );
		MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* L_3 = __this->___callback_5;
		String_t* L_4 = ___error0;
		NullCheck(L_3);
		MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_inline(L_3, L_4, NULL);
		// callback = null;
		__this->___callback_5 = (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___callback_5), (void*)(MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD*)NULL);
	}

IL_002a:
	{
		// }
		return;
	}
}
// System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NGMediaSaveCallbackiOS__ctor_m45544850C3CBA03744C0879D2A41666B5A49311B (NGMediaSaveCallbackiOS_t84DD009CDDE052F5352AC29E7518C6DC7D9401BE* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool NGMediaReceiveCallbackiOS_get_IsBusy_mAECD29ABE59BCEA398CBE4E9920C1F4A0FADE428_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool IsBusy { get; private set; }
		bool L_0 = ((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___U3CIsBusyU3Ek__BackingField_7;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MediaSaveCallback_Invoke_mDB1597A2F1E7AA6F0F6EFE9C5B829B937A1735C2_inline (MediaSaveCallback_tDE715F09CBC74957C4F9FD6F9A682A5762A16AFD* __this, String_t* ___error0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___error0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MediaPickCallback_Invoke_m6C6C6C0A4E3A0E59573325ABDA410FFFAFA9E996_inline (MediaPickCallback_tBDC518911F5CCA4ADB33BDAEE8A51F6B054E5F1D* __this, String_t* ___path0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___path0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MediaPickMultipleCallback_Invoke_m8A0C729569A7D5C868D524570CE484C11C645096_inline (MediaPickMultipleCallback_tE03DE042D5274C536ADFAEB1FE6E2B76F5013401* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___paths0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___paths0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NGMediaReceiveCallbackiOS_set_IsBusy_m1A7EE6ECFA48392D1162D1EFAA5402E343762731_inline (bool ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool IsBusy { get; private set; }
		bool L_0 = ___value0;
		((NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_StaticFields*)il2cpp_codegen_static_fields_for(NGMediaReceiveCallbackiOS_t97F920D9435F6D828C47BFEF723F57C7E9570097_il2cpp_TypeInfo_var))->___U3CIsBusyU3Ek__BackingField_7 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___obj0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg10, ___arg21, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_4_Invoke_mD9FB5E2CF8A8ED11C3135A1A910CDCAD6538A1FC_gshared_inline (Action_4_tB13889672F2D689F21857C968EBBDF091CA42E46* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, int32_t ___arg32, RuntimeObject* ___arg43, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg10, ___arg21, ___arg32, ___arg43, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mE56360F8A5B137A1714DEF08A9266EF9166480B8_gshared_inline (Action_1_t59D9650BCC97814E3D7C53FCC12A9484950839CE* __this, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96 ___obj0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, iBeaconData_tBEF28446C1EBB9EA0546121555C9E043B4C0BD96, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___obj0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_3_Invoke_m399A0EB5E51EFD9B7D25DFE0EB7BF5EC0BE98346_gshared_inline (Action_3_tCDB60724FE7702C8848DCEE7A25283B015D0DA58* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, RuntimeObject* ___arg32, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg10, ___arg21, ___arg32, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_gshared_inline (const RuntimeMethod* method) 
{
	{
		il2cpp_codegen_runtime_class_init_inline(il2cpp_rgctx_data(method->rgctx_data, 0));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_0 = ((EmptyArray_1_tDF0DD7256B115243AA6BD5558417387A734240EE_StaticFields*)il2cpp_codegen_static_fields_for(il2cpp_rgctx_data(method->rgctx_data, 0)))->___Value_0;
		return L_0;
	}
}
