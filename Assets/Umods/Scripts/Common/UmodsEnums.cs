﻿namespace Umods.Scripts.Common
{
    /// <summary>
    /// Enumerations used by the Umods Framework.
    /// </summary>
    public static class UmodsEnums
    {
        public enum UmodsEventType
        {
        }

        public enum UmodsExceptionType
        {
            InputBlocking
        }

        public enum UmodsNextSceneOption
        {
            Success,
            Failure
        }
    }
}