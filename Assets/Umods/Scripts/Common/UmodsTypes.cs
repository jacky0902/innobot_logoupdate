﻿using UnityEngine;

namespace Umods.Scripts.Common
{
    public class UmodsException
    {
        /// <summary>
        ///     Description of the event key.
        /// </summary>
        public string EventKey;

        /// <summary>
        ///     Description of the event value.
        /// </summary>
        public string EventValue;

        /// <summary>
        ///     Timestamp, in seconds, of the event.
        /// </summary>
        public double Timestamp;

        /// <summary>
        ///     Type of event.
        /// </summary>
        public UmodsEnums.UmodsExceptionType Type;

        public UmodsException(UmodsEnums.UmodsExceptionType type, string eventKey, string eventValue)
        {
            Timestamp = Time.time;
            Type = type;
            EventKey = eventKey;
            EventValue = eventValue;
        }
    }

    public class UmodsEvent
    {
        /// <summary>
        ///     Description of the event key.
        /// </summary>
        public string EventKey;

        /// <summary>
        ///     Description of the event value.
        /// </summary>
        public string EventValue;

        /// <summary>
        ///     Timestamp, in seconds, of the event.
        /// </summary>
        public double Timestamp;

        /// <summary>
        ///     Type of event.
        /// </summary>
        public UmodsEnums.UmodsEventType Type;

        public UmodsEvent(UmodsEnums.UmodsEventType type, string eventKey, string eventValue)
        {
            Timestamp = Time.time;
            Type = type;
            EventKey = eventKey;
            EventValue = eventValue;
        }
    }
}