﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Umods.Scripts.Common
{
    /// <summary>
    ///     A container for extension methods used by the Umods Framework.
    /// </summary>
    internal static class UmodsExtensions
    {
        /// <summary>
        ///     Attempts to convert an object to type T and, if successful, calls the supplied callback with
        ///     the converted object as an argument.
        /// </summary>
        /// <typeparam name="T">The output type of the conversion.</typeparam>
        /// <param name="objectToConvert">The object to convert.</param>
        /// <param name="callback">The callback that will be invoked if the conversion was successful.</param>
        /// <returns>true if conversion was successful, false otherwise.</returns>
        public static bool SafeConvert<T>(this object objectToConvert, Action<T> callback) where T : class
        {
            var castResult = objectToConvert as T;
            if (castResult == null) return false;
            callback(castResult);
            return true;
        }

        public static bool SafeGetDelegate<T>(this object objectToLook, string methodName, Action<T> callback)
            where T : class
        {
            var lifeCycleClassType = objectToLook.GetType();
            MethodInfo lifeCycleMethod = null;
            while (lifeCycleClassType != null)
            {
                lifeCycleMethod = lifeCycleClassType.GetMethod(methodName,
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (lifeCycleMethod != null)
                    break;

                lifeCycleClassType = lifeCycleClassType.BaseType;
            }

            if (lifeCycleMethod == null) return false;

            callback(Delegate.CreateDelegate(typeof(T), objectToLook, lifeCycleMethod) as T);
            return true;
        }

        public static bool SafeCallMethod(this object objectToLook, string methodName)

        {
            var lifeCycleClassType = objectToLook.GetType();
            MethodInfo lifeCycleMethod = null;
            while (lifeCycleClassType != null)
            {
                lifeCycleMethod = lifeCycleClassType.GetMethod(methodName,
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (lifeCycleMethod != null)
                    break;

                lifeCycleClassType = lifeCycleClassType.BaseType;
            }

            if (lifeCycleMethod == null) return false;

            lifeCycleMethod.Invoke(objectToLook, new object[] { });
            return true;
        }
        
        public static EventTrigger.Entry AddListenerToEventTrigger(this GameObject objectWithEventTrigger,
            EventTriggerType type, UnityAction<BaseEventData> eventHandler)
        {
            var trigger = objectWithEventTrigger.GetComponent<EventTrigger>();
            if (trigger == null) return null;

            var entry = new EventTrigger.Entry {eventID = type};
            entry.callback.AddListener(eventHandler);
            trigger.triggers.Add(entry);
            return entry;
        }

        public static bool RemoveListenerToEventTrigger(this GameObject objectWithEventTrigger,
            EventTrigger.Entry entry)
        {
            var trigger = objectWithEventTrigger.GetComponent<EventTrigger>();
            return trigger != null && trigger.triggers.Remove(entry);
        }
    }
}