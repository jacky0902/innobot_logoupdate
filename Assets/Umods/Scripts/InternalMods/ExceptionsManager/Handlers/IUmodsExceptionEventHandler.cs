﻿using Umods.Scripts.Common;

namespace Umods.Scripts.InternalMods.ExceptionsManager.Handlers
{
    public interface IUmodsExceptionEventHandler
    {
        void OnExceptionEvent(UmodsException exception);
    }
}