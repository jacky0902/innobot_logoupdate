﻿using Umods.Scripts.Common;

namespace Umods.Scripts.InternalMods.ExceptionsManager.Core
{
    public interface IUmodsExceptionEventManager
    {
        void SendExceptionEvent(UmodsException exception);
    }
}