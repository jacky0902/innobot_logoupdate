﻿using Umods.Scripts.Common;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using Umods.Scripts.InternalMods.ExceptionsManager.Handlers;

namespace Umods.Scripts.InternalMods.ExceptionsManager.Core
{
    public class UmodsExceptionEventManager : UmodsInternalModBehaviour, IUmodsExceptionEventManager
    {
        private IUmodsEventListener _umodsEventListener;

        public void SendExceptionEvent(UmodsException exception)
        {
            _umodsEventListener.PullEvent("OnExceptionEvent", new[] {exception});
        }

        public override void OnListenersAndModRegistration()
        {
            UmodsApplication.RegisterInternalMod<IUmodsExceptionEventManager>(this);
            _umodsEventListener = UmodsApplication.EventListenerManager.RegisterListener<IUmodsExceptionEventHandler>();
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterInternalMod<IUmodsExceptionEventManager>();
            UmodsApplication.EventListenerManager.UnregisterListener<IUmodsExceptionEventHandler>();
        }
    }
}