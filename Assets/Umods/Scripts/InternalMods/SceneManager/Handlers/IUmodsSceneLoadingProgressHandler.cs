﻿namespace Umods.Scripts.InternalMods.SceneManager.Handlers
{
    public interface IUmodsSceneLoadingProgressHandler
    {
        void OnUmodsLoadingSceneProgress(float progress);
    }
}