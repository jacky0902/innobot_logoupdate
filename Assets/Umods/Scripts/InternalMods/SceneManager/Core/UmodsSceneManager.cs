﻿using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using Umods.Scripts.InternalMods.SceneManager.Handlers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Umods.Scripts.InternalMods.SceneManager.Core
{
    /// <summary>
    ///     Scene manager integrated in this framework
    /// </summary>
    public class UmodsSceneManager : UmodsInternalModBehaviour, IUmodsSceneManager
    {
        // history stack of scenes
        private readonly Stack<string> _scenesStack = new Stack<string>();
        private string _currentSceneName;

        private bool _loadingScreenEnabled;
        private IUmodsEventListener _umodsSceneLoadingProgressListener;

        public string loadingScreen;
        public string rootScene;

        public void LoadScene(string sceneName, bool withLoadingScreen = false)
        {
            if (withLoadingScreen)
                StartCoroutine(_LoadSceneWithLoadingScene(sceneName));
            else
                UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);

            _currentSceneName = sceneName;
            _scenesStack.Push(sceneName);
        }

        public void LoadPreviousScene(bool withLoadingScreen = false)
        {
            if (_scenesStack.Count == 0) return;
            LoadScene(_scenesStack.Pop(), withLoadingScreen);
        }

        public float LoadingProgress { get; private set; }

        public void LoadRootScene(bool withLoadingScreen = false)
        {
            _scenesStack.Clear();
            LoadScene(rootScene, withLoadingScreen);
        }

        public void HideLoadingScreen()
        {
            if (!_loadingScreenEnabled) return;

            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(loadingScreen);
            _loadingScreenEnabled = false;
        }


        // LOADING SCENE ASYNCH

        private IEnumerator _LoadSceneWithLoadingScene(string sceneName)
        {
            // LoadSceneAsync() returns an AsyncOperation, 
            // so will only continue past this point when the Operation has finished
            _loadingScreenEnabled = true;
            yield return UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Loading");

            // as soon as we've finished loading the loading screen, start loading the game scene
            yield return StartCoroutine(_LoadSceneWLSCoroutine(sceneName));
        }

        private IEnumerator _LoadSceneWLSCoroutine(string sceneName)
        {
            var asyncScene = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            // this value stops the scene from displaying when it's finished loading
            asyncScene.allowSceneActivation = false;

            while (!asyncScene.isDone)
            {
                // loading bar progress
                LoadingProgress = Mathf.Clamp01(asyncScene.progress / 0.9f) * 100;

                // scene has loaded as much as possible, the last 10% can't be multi-threaded
                if (asyncScene.progress >= 0.9f) asyncScene.allowSceneActivation = true;

                _umodsSceneLoadingProgressListener.PullEvent("SceneLoadingProgress", new[] {(object) LoadingProgress});
                yield return null;
            }

            _umodsSceneLoadingProgressListener.PullEvent("SceneLoadingProgress", new[] {(object) LoadingProgress});


            yield return null;
        }


        public override void OnListenersAndModRegistration()
        {
            UmodsApplication.RegisterInternalMod<IUmodsSceneManager>(this);
            _umodsSceneLoadingProgressListener = UmodsApplication.EventListenerManager
                .RegisterListener<IUmodsSceneLoadingProgressHandler>();
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterInternalMod<IUmodsSceneManager>();
            UmodsApplication.EventListenerManager.UnregisterListener<IUmodsSceneLoadingProgressHandler>();
        }
    }
}