﻿using Umods.Scripts.Core;

namespace Umods.Scripts.InternalMods.SceneManager.Core

{
    public abstract class UmodsSceneBehaviour : Scripts.Core.UmodsSceneBehaviour
    {
        public bool InitiateWithLoadingScreen;

        private IUmodsSceneManager _sceneManager;

        private void OnInitializeSceneBehaviour()
        {
            _sceneManager = UmodsApplication.GetInternalMod<IUmodsSceneManager>();
            if (!InitiateWithLoadingScreen) HideLoadingScreen();
        }

        protected void HideLoadingScreen()
        {
            _sceneManager.HideLoadingScreen();
        }

        protected void LoadScene(string sceneName, bool withLoadingScreen = false)
        {
            _sceneManager.LoadScene(sceneName, withLoadingScreen);
        }

        protected void LoadPreviousScene(bool withLoadingScreen = false)
        {
            _sceneManager.LoadPreviousScene(withLoadingScreen);
        }

        protected void LoadRootScene(bool withLoadingScreen = false)
        {
            _sceneManager.LoadRootScene(withLoadingScreen);
        }
    }
}