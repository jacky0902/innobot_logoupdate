﻿namespace Umods.Scripts.InternalMods.SceneManager.Core
{
    public interface IUmodsSceneManager
    {
        float LoadingProgress { get; }
        void LoadScene(string sceneName, bool withLoadingScreen = false);

        void LoadPreviousScene(bool withLoadingScreen = false);

        void HideLoadingScreen();

        void LoadRootScene(bool withLoadingScreen = false);
    }
}