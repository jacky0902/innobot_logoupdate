﻿using Umods.Scripts.Common;
using UnityEngine;

namespace Umods.Scripts.Core
{
    public abstract class UmodsAppConfiguration : UmodsBehaviour
    {
        private UmodsApplication _hiddenUmodsApplication;

        private void Start()
        {
            if(FindObjectsOfType<UmodsAppConfiguration>().Length > 1)
            {
                Destroy(gameObject);
                return;
            };
            
            DontDestroyOnLoad(this);

            UmodsApplication.RegisterAppConfiguration(this);

            this.SafeCallMethod("OnUInitializeConfiguration");
            OnInitialializeConfiguration();
        }

        protected abstract void OnInitialializeConfiguration();
    }
}