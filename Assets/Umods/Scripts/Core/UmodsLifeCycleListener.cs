﻿using System;
using System.Collections.Generic;
using Umods.Scripts.Common;
using UnityEngine;

namespace Umods.Scripts.Core
{
    /// <summary>
    /// This class handles the lifecycle of the application. It has all the elements registered and controls its execution in the order we defined.
    /// </summary>
    public partial class UmodsLifeCycleListener
    {
        private readonly HashSet<object> _registeredObjects = new HashSet<object>();

        private OnUmodsLifeCycleEventHandler _onInitializeSceneBehaviour;
        private OnUmodsLifeCycleEventHandler _onInitializeModBehaviour;

        private OnUmodsLifeCycleEventHandler _onListenersAndModRegistration;


        private OnUmodsLifeCycleEventHandler _onUmodsRegisterHandlerCore;
        private OnUmodsLifeCycleEventHandler _onUmodsRegisterHandlerInternalMod;
        private OnUmodsLifeCycleEventHandler _onUmodsRegisterHandlerMod;
        private OnUmodsLifeCycleEventHandler _onUmodsRegisterHandlerScene;
        private OnUmodsLifeCycleEventHandler _onUmodsRegisterHandlerDefault;

        private OnUmodsLifeCycleEventHandler _onUmodsStartCore;
        private OnUmodsLifeCycleEventHandler _onUmodsStartInternalMod;
        private OnUmodsLifeCycleEventHandler _onUmodsStartMod;
        private OnUmodsLifeCycleEventHandler _onUmodsStartScene;
        private OnUmodsLifeCycleEventHandler _onUmodsStartDefault;

        private OnUmodsLifeCycleEventHandler _onUmodsUpdateCore;
        private OnUmodsLifeCycleEventHandler _onUmodsUpdateInternalMod;
        private OnUmodsLifeCycleEventHandler _onUmodsUpdateMod;
        private OnUmodsLifeCycleEventHandler _onUmodsUpdateScene;
        private OnUmodsLifeCycleEventHandler _onUmodsUpdateDefault;

        private OnUmodsLifeCycleEventHandler _onUmodsLateUpdateCore;
        private OnUmodsLifeCycleEventHandler _onUmodsLateUpdateInternalMod;
        private OnUmodsLifeCycleEventHandler _onUmodsLateUpdateMod;
        private OnUmodsLifeCycleEventHandler _onUmodsLateUpdateScene;
        private OnUmodsLifeCycleEventHandler _onUmodsLateUpdateDefault;

        private OnUmodsLifeCycleEventHandler _onUmodsDestroyCore;
        private OnUmodsLifeCycleEventHandler _onUmodsDestroyInternalMod;
        private OnUmodsLifeCycleEventHandler _onUmodsDestroyMod;
        private OnUmodsLifeCycleEventHandler _onUmodsDestroyScene;
        private OnUmodsLifeCycleEventHandler _onUmodsDestroyDefault;


        /// <summary>
        ///     Registers an object for umods event callbacks.
        /// </summary>
        /// <param name="handlerObject">The object.</param>
        public void RegisterHandler(object handlerObject)
        {
            _RegistrationChange(handlerObject, true);
        }

        /// <summary>
        ///     Unregisters an object for umods event callbacks.
        /// </summary>
        /// <param name="handlerObject">The object.</param>
        public void UnregisterHandler(object handlerObject)
        {
            _RegistrationChange(handlerObject, false);
        }

        public void UnregisterAll()
        {
            _onInitializeSceneBehaviour = null;
            _onInitializeModBehaviour = null;
            _onListenersAndModRegistration = null;

            _onUmodsRegisterHandlerDefault = null;
            _onUmodsStartDefault = null;
            _onUmodsUpdateDefault = null;
            _onUmodsLateUpdateDefault = null;
            _onUmodsDestroyDefault = null;


            _onUmodsRegisterHandlerInternalMod = null;
            _onUmodsStartInternalMod = null;
            _onUmodsUpdateInternalMod = null;
            _onUmodsLateUpdateInternalMod = null;
            _onUmodsDestroyInternalMod = null;


            _onUmodsRegisterHandlerMod = null;
            _onUmodsStartMod = null;
            _onUmodsUpdateMod = null;
            _onUmodsLateUpdateMod = null;
            _onUmodsDestroyMod = null;


            _onUmodsRegisterHandlerScene = null;
            _onUmodsStartScene = null;
            _onUmodsUpdateScene = null;
            _onUmodsLateUpdateScene = null;
            _onUmodsDestroyScene = null;
        }

        private static bool _RegisterUmodsBehaviour(bool value, object umodsBehaviourObject, string methodName,
            ref OnUmodsLifeCycleEventHandler coreDelegate,
            ref OnUmodsLifeCycleEventHandler internalModuleDelegate, ref OnUmodsLifeCycleEventHandler moduleDelegate,
            ref OnUmodsLifeCycleEventHandler sceneDelegate, ref OnUmodsLifeCycleEventHandler defaultDelegate)
        {
            OnUmodsLifeCycleEventHandler delegateMethodOut = null;

            var success = umodsBehaviourObject.SafeGetDelegate<OnUmodsLifeCycleEventHandler>(methodName,
                delegateMethod => { delegateMethodOut = delegateMethod; });

            if (!success) return false;

            if (value)
            {
                if (umodsBehaviourObject is UmodsApplication)
                    coreDelegate += delegateMethodOut;
                // InternaModule must be first because it is also Module
                else if (umodsBehaviourObject is UmodsInternalModBehaviour)
                    internalModuleDelegate += delegateMethodOut;
                else if (umodsBehaviourObject is UmodsModBehaviour)
                    moduleDelegate += delegateMethodOut;
                else if (umodsBehaviourObject is UmodsSceneBehaviour)
                    sceneDelegate += delegateMethodOut;
                else
                    defaultDelegate += delegateMethodOut;
            }
            else
            {
                if (umodsBehaviourObject is UmodsApplication)
                    coreDelegate -= delegateMethodOut;
                else if (umodsBehaviourObject is UmodsInternalModBehaviour)
                    internalModuleDelegate -= delegateMethodOut;
                else if (umodsBehaviourObject is UmodsModBehaviour)
                    moduleDelegate -= delegateMethodOut;
                else if (umodsBehaviourObject is UmodsSceneBehaviour)
                    sceneDelegate -= delegateMethodOut;
                else
                    defaultDelegate -= delegateMethodOut;
            }

            return success;
        }

        /// <summary>
        ///     Registers or unregisters an object for callbacks.
        /// </summary>
        /// <param name="umodsObject">The object.</param>
        /// <param name="value">If this is a registration request (otherwise deregistration).</param>
        private void _RegistrationChange(object umodsObject, bool value)
        {
            var success = false;

            var isRegistered = _registeredObjects.Contains(umodsObject);
            if (value && isRegistered || !value && !isRegistered) return;

            success |= umodsObject.SafeConvert<UmodsModBehaviour>(moduleHandler =>
            {
                if (value)
                    _onListenersAndModRegistration += moduleHandler.OnListenersAndModRegistration;
                else
                    _onListenersAndModRegistration -= moduleHandler.OnListenersAndModRegistration;
            });

            if (umodsObject is UmodsBehaviour)
            {
                success |= _RegisterUmodsBehaviour(value, umodsObject, "UStart", ref _onUmodsStartCore,
                    ref _onUmodsStartInternalMod,
                    ref _onUmodsStartMod, ref _onUmodsStartScene, ref _onUmodsStartDefault);

                success |= _RegisterUmodsBehaviour(value, umodsObject, "UUpdate", ref _onUmodsUpdateCore,
                    ref _onUmodsUpdateInternalMod,
                    ref _onUmodsUpdateMod, ref _onUmodsUpdateScene, ref _onUmodsUpdateDefault);

                success |= _RegisterUmodsBehaviour(value, umodsObject, "ULateUpdate",
                    ref _onUmodsLateUpdateCore, ref _onUmodsLateUpdateInternalMod, ref _onUmodsLateUpdateMod,
                    ref _onUmodsLateUpdateScene,
                    ref _onUmodsLateUpdateDefault);

                success |= _RegisterUmodsBehaviour(value, umodsObject, "InternalUOnDestroy",
                    ref _onUmodsDestroyCore, ref _onUmodsDestroyInternalMod, ref _onUmodsDestroyMod,
                    ref _onUmodsDestroyScene,
                    ref _onUmodsDestroyDefault);

                success |= _RegisterUmodsBehaviour(value, umodsObject, "OnRegisterHandler",
                    ref _onUmodsRegisterHandlerCore, ref _onUmodsRegisterHandlerInternalMod,
                    ref _onUmodsRegisterHandlerMod,
                    ref _onUmodsRegisterHandlerScene, ref _onUmodsRegisterHandlerDefault);

                success |= umodsObject.SafeGetDelegate<OnUmodsLifeCycleEventHandler>("OnInitializeSceneBehaviour",
                    delegateMethod =>
                    {
                        if (value)
                            _onInitializeSceneBehaviour += delegateMethod;
                        else
                            _onInitializeSceneBehaviour -= delegateMethod;
                    });

                success |= umodsObject.SafeGetDelegate<OnUmodsLifeCycleEventHandler>("OnInitializeModBehaviour",
                    delegateMethod =>
                    {
                        if (value)
                            _onInitializeModBehaviour += delegateMethod;
                        else
                            _onInitializeModBehaviour -= delegateMethod;
                    });

                success |= umodsObject.SafeGetDelegate<OnUmodsLifeCycleEventHandler>("UAwake",
                    delegateMethod =>
                    {
                        if (value) delegateMethod();
                    }
                );
            }

            if (!success) return;
            if (value)
                _registeredObjects.Add(umodsObject);
            else

                _registeredObjects.Remove(umodsObject);
        }

        /// <summary>
        ///     Delegate for Umods life cycle events.
        /// </summary>
        private delegate void OnUmodsLifeCycleEventHandler();
    }

    public partial class UmodsLifeCycleListener : MonoBehaviour
    {
        private void Update()
        {
            if (_onListenersAndModRegistration != null)
            {
                _onListenersAndModRegistration();
                _onListenersAndModRegistration = null;
            }

            if (_onUmodsRegisterHandlerCore != null)
            {
                _onUmodsRegisterHandlerCore();
                _onUmodsRegisterHandlerCore = null;
            }

            if (_onUmodsRegisterHandlerInternalMod != null)
            {
                _onUmodsRegisterHandlerInternalMod();
                _onUmodsRegisterHandlerInternalMod = null;
            }

            if (_onUmodsRegisterHandlerMod != null)
            {
                _onUmodsRegisterHandlerMod();
                _onUmodsRegisterHandlerMod = null;
            }

            if (_onUmodsRegisterHandlerScene != null)
            {
                _onUmodsRegisterHandlerScene();
                _onUmodsRegisterHandlerScene = null;
            }

            if (_onUmodsRegisterHandlerDefault != null)
            {
                _onUmodsRegisterHandlerDefault();
                _onUmodsRegisterHandlerDefault = null;
            }

            if (_onUmodsStartCore != null)
            {
                _onUmodsStartCore();
                _onUmodsStartCore = null;
            }
            
            if (_onInitializeModBehaviour != null)
            {
                _onInitializeModBehaviour();
                _onInitializeModBehaviour = null;
            }

            if (_onUmodsStartInternalMod != null)
            {
                _onUmodsStartInternalMod();
                _onUmodsStartInternalMod = null;
            }

            if (_onUmodsStartMod != null)
            {
                _onUmodsStartMod();
                _onUmodsStartMod = null;
            }

            if (_onInitializeSceneBehaviour != null)
            {
                _onInitializeSceneBehaviour();
                _onInitializeSceneBehaviour = null;
            }

            if (_onUmodsStartScene != null)
            {
                _onUmodsStartScene();
                _onUmodsStartScene = null;
            }

            if (_onUmodsStartDefault != null)
            {
                _onUmodsStartDefault();
                _onUmodsStartDefault = null;
            }

            if (_onUmodsUpdateCore != null) _onUmodsUpdateCore();

            if (_onUmodsUpdateInternalMod != null) _onUmodsUpdateInternalMod();

            if (_onUmodsUpdateMod != null) _onUmodsUpdateMod();

            if (_onUmodsUpdateScene != null) _onUmodsUpdateScene();

            if (_onUmodsUpdateDefault != null) _onUmodsUpdateDefault();


            if (_onUmodsLateUpdateCore != null) _onUmodsLateUpdateCore();

            if (_onUmodsLateUpdateInternalMod != null) _onUmodsLateUpdateInternalMod();

            if (_onUmodsLateUpdateMod != null) _onUmodsLateUpdateMod();

            if (_onUmodsLateUpdateScene != null) _onUmodsLateUpdateScene();

            if (_onUmodsLateUpdateDefault != null) _onUmodsLateUpdateDefault();
        }

        public void OnDestroyCore()
        {
            if (_onUmodsDestroyDefault != null)
            {
                _onUmodsDestroyDefault();
                _onUmodsDestroyDefault = null;
            }

            if (_onUmodsDestroyScene != null)
            {
                _onUmodsDestroyScene();
                _onUmodsDestroyScene = null;
            }

            if (_onUmodsDestroyMod != null)
            {
                _onUmodsDestroyMod();
                _onUmodsDestroyMod = null;
            }

            if (_onUmodsDestroyInternalMod != null)
            {
                _onUmodsDestroyInternalMod();
                _onUmodsDestroyInternalMod = null;
            }

            if (_onUmodsDestroyCore != null)
            {
                _onUmodsDestroyCore();
                _onUmodsDestroyCore = null;
            }
        }
    }
}