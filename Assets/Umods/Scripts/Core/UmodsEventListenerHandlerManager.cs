﻿using System;
using System.Collections.Generic;
using Umods.Scripts.Interfaces;

namespace Umods.Scripts.Core
{
    /// <summary>
    /// Manager that handles listener and handlers
    /// </summary>
    public class UmodsEventListenerHandlerManager : IUmodsEventListenerManager, IUmodsEventHandlerManager
    {
        private readonly Dictionary<Type, UmodsEventListener> _listenerHandlersRegistered =
            new Dictionary<Type, UmodsEventListener>();

        public void RegisterHandler(object handlerObject)
        {
            foreach (var handlerType in _listenerHandlersRegistered.Keys)
                if (handlerType.IsInstanceOfType(handlerObject))
                    _listenerHandlersRegistered[handlerType].AddObject(handlerObject);
        }

        public bool UnregisterHandler(object handlerObject)
        {
            var value = false;
            foreach (var handlerType in _listenerHandlersRegistered.Keys)
                if (handlerType.IsInstanceOfType(handlerObject))
                    value |= _listenerHandlersRegistered[handlerType].RemoveObject(handlerObject);

            return value;
        }

        public IUmodsEventListener RegisterListener<T>()
        {
            if (_listenerHandlersRegistered.ContainsKey(typeof(T)))
                return _listenerHandlersRegistered[typeof(T)];
            var umodsEventListener = new UmodsEventListener(typeof(T));
            _listenerHandlersRegistered.Add(typeof(T), umodsEventListener);
            return umodsEventListener;
        }

        public IUmodsEventListener GetListener<T>()
        {
            if (!_listenerHandlersRegistered.ContainsKey(typeof(T)))
                return null;
            return _listenerHandlersRegistered[typeof(T)];
        }


        public bool UnregisterListener<T>()
        {
            return _listenerHandlersRegistered.Remove(typeof(T));
        }

        public void ClearListeners()
        {
            _listenerHandlersRegistered.Clear();
        }
    }
}