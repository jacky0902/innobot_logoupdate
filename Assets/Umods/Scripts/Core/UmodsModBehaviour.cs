﻿using UnityEngine;

namespace Umods.Scripts.Core
{
    public abstract class UmodsModBehaviour : UmodsBehaviour
    {
        [SerializeField] protected bool _debugLog = false;

        public bool DebugLog
        {
            get => _debugLog;
            set => _debugLog = value;
        }

        private void OnInitializeModBehaviour()
        {
            Debug = new DebugObject(this);
        }

        protected DebugObject Debug;

        public abstract void OnListenersAndModRegistration();

        public abstract void OnDestroyListenersAndModUnregistration();

        public void UOnModDestroy()
        {
            OnDestroyListenersAndModUnregistration();
        }

        protected class DebugObject
        {
            private UmodsModBehaviour _umodsModBehaviour;

            public DebugObject(UmodsModBehaviour umodsModBehaviour)
            {
                _umodsModBehaviour = umodsModBehaviour;
            }

            public void Log(string log)
            {
                if (_umodsModBehaviour._debugLog)
                    UnityEngine.Debug.Log(string.Format("{0}: {1}", _umodsModBehaviour.GetType().Name, log));
            }
        }
    }
}