﻿using System;
using System.Collections.Generic;
using Umods.Scripts.Interfaces;

namespace Umods.Scripts.Core
{
    /// <summary>
    /// Class that store a listener and makes it to be executed through its interface IUmodsEventListener
    /// </summary>
    public class UmodsEventListener : IUmodsEventListener
    {
        private readonly List<object> _objects;
        private readonly Type _handlerType;

        public UmodsEventListener(Type handlerType)
        {
            _handlerType = handlerType;
            _objects = new List<object>();
        }

        public bool AddObject(object item)
        {
            if (!_handlerType.IsInstanceOfType(item)) return false;
            _objects.Add(item);
            return true;
        }

        public bool RemoveObject(object item)
        {
            return _handlerType.IsInstanceOfType(item) && _objects.Remove(item);
        }

        public bool PullEvent(string methodName, object[] parameters = null)
        {
            try
            {
                var methodInfo = _handlerType.GetMethod(methodName);
                if (methodInfo == null) return false;
                foreach (var o in _objects) methodInfo.Invoke(o, parameters);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return true;
        }
    }
}