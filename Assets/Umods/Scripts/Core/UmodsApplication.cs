﻿using System;
using System.Collections.Generic;
using Umods.Scripts.Interfaces;
using Umods.Scripts.InternalMods.ExceptionsManager.Core;
using Umods.Scripts.InternalMods.SceneManager.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Umods.Scripts.Core
{
    /// <summary>
    /// Core class for the UmodsFramework. Main communication goes through its interface.
    /// </summary>
    public class UmodsApplication : UmodsBehaviour
    {
        //MODS REGISTERED
        private readonly Dictionary<Type, object> _modsRegistered = new Dictionary<Type, object>();
        private readonly Dictionary<Type, object> _internalModsRegistered = new Dictionary<Type, object>();

        //CORE ELEMENTS
        [SerializeField]
        private UmodsAppConfiguration _umodsAppConfiguration;
        private UmodsEventListenerHandlerManager _umodsEventListenerHandlerManager;
        private UmodsExceptionEventManager _umodsExceptionEventManager;
        private UmodsLifeCycleListener _umodsLifeCycleListener;
        private UmodsSceneManager _umodsSceneManager;

        //PUBLIC VARIABLE

        [Header("Debug")] public bool DebugMode;

        [Header("On Play Debug")] public bool LoadFirstScene;

        public bool DebugInstance { get; private set; } = false;


        //GETTERS CORE ELEMENTS
        public T GetUmodsAppConfiguration<T>() where T : UmodsAppConfiguration
        {
            if (_umodsAppConfiguration == null)
            {
                throw new NullReferenceException("UmodsConfiguration is not set in this project.");
            }

            return _umodsAppConfiguration as T;
        }

        public IUmodsEventListenerManager EventListenerManager
        {
            get { return _umodsEventListenerHandlerManager; }
        }

        public IUmodsEventHandlerManager EventHandlerManager
        {
            get { return _umodsEventListenerHandlerManager; }
        }


        //APPLICATION INITIALIZATION
        private void Awake()
        {
            if (FindObjectsOfType<UmodsApplication>().Length > 1)
            {
                Destroy(this);
                Destroy(gameObject);
                return;
            }
            
            if (DebugMode)
            {
                if (LoadFirstScene)
                {
                    SceneManager.LoadScene(0);
                    return;
                }
            }


            DontDestroyOnLoad(this);

            DebugInstance = true;

            InitializeLifeCycleListenerAndEventRegistrationManager();

            InitializeInternalMods();
        }

        private void UStart()
        {
           
        }

        private new void OnDestroy()
        {
            if (_umodsLifeCycleListener != null)
                _umodsLifeCycleListener.OnDestroyCore();
        }


        private void InitializeLifeCycleListenerAndEventRegistrationManager()
        {
            // Register the life cycle events
            _umodsLifeCycleListener = gameObject.AddComponent<UmodsLifeCycleListener>();
            // Register the event listener and handler
            _umodsEventListenerHandlerManager = new UmodsEventListenerHandlerManager();

            if (_umodsLifeCycleListener == null || _umodsEventListenerHandlerManager == null)
                throw new NullReferenceException(
                    "UF: UmodsLifeCycleListener or UmodsEventListenerHandlerManager has not been correctly initialized");
        }

        private void InitializeInternalMods()
        {
            // Initialize the exception event manager
            _umodsExceptionEventManager = GetComponentInChildren<UmodsExceptionEventManager>();
            if (_umodsExceptionEventManager == null)
            {
                throw new NullReferenceException("UmodsExceptionEventManager is not set in this project.");
            }

            // Initialize the scene manager
            _umodsSceneManager = GetComponentInChildren<UmodsSceneManager>();
            if (_umodsSceneManager == null)
            {
                throw new NullReferenceException("UmodsSeceneManager is not set in this project.");
            }
        }

        public void RegisterInLifeCycle(object umodsObject)
        {
            _umodsLifeCycleListener.RegisterHandler(umodsObject);
        }

        public void UnregisterInLifeCycle(object umodsObject)
        {
            _umodsLifeCycleListener.UnregisterHandler(umodsObject);
        }

        public void RegisterAppConfiguration(UmodsAppConfiguration umodsAppConfiguration)
        {
            _umodsAppConfiguration = umodsAppConfiguration;
        }

        public void UnregisterAppConfiguration()
        {
            _umodsAppConfiguration = null;
        }

        public void RegisterInternalMod<T>(T modObject) where T : class
        {
            _internalModsRegistered[typeof(T)] = modObject;
        }

        public void UnregisterInternalMod<T>() where T : class
        {
            _internalModsRegistered.Remove(typeof(T));
        }

        public T GetInternalMod<T>() where T : class
        {
            if (!_internalModsRegistered.ContainsKey(typeof(T)))
            {
                throw new NullReferenceException(typeof(T).ToString() + " Internal Mod is not set in this project.");
            }

            ;
            return (T) _internalModsRegistered[typeof(T)];
        }

        public void RegisterMod<T>(T modObject) where T : class
        {
            _modsRegistered[typeof(T)] = modObject;
        }

        public void UnregisterMod<T>() where T : class
        {
            _modsRegistered.Remove(typeof(T));
        }

        public T GetMod<T>() where T : class
        {
            if (!_modsRegistered.ContainsKey(typeof(T)))
            {
                throw new NullReferenceException(typeof(T).ToString() + " Mod is not set in this project.");
            }

            ;
            return (T) _modsRegistered[typeof(T)];
        }
    }
}