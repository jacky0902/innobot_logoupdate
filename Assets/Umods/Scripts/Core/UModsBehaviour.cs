﻿using System;
using Umods.Scripts.Common;
using Umods.Scripts.Interfaces;
using UnityEngine;

namespace Umods.Scripts.Core
{
    /// <summary>
    /// Main behaviour class base for elements in Umods Framework
    /// </summary>
    public abstract class UmodsBehaviour : MonoBehaviour
    {
        private UmodsApplication _umodsApplication;
        private IUmodsEventHandlerManager _umodsEventHandlerManager;

        private bool _destroyed = false;
        private bool _registered = false;

        /// <summary>
        /// Access to the framework core
        /// </summary>
        /// <exception cref="NullReferenceException"></exception>
        protected UmodsApplication UmodsApplication
        {
            get
            {
                if (_umodsApplication == null)
                {
                    // This is for avoiding getting the wrong GO when there are several UmodsApplications and one is destroying but still reachable
                    // TODO Need to be improved
                    var umodsApplications = FindObjectsOfType<UmodsApplication>();
                    if (umodsApplications.Length > 1)
                    {
                        foreach (var umodsApplication in umodsApplications)
                        {
                            if (umodsApplication.DebugInstance)
                                _umodsApplication = umodsApplication;
                        }
                    }
                    else
                    {
                        _umodsApplication = umodsApplications[0];
                    }

                    if (_umodsApplication == null)
                        throw new NullReferenceException(this.GetType() + " - UmodsCore is not set in this project.");
                }

                return _umodsApplication;
            }
        }

        private void Start()
        {
            // Register to lifecycle to set this up in the framework
            UmodsApplication.RegisterInLifeCycle(this);
            _registered = true;
        }

        private void OnRegisterHandler()
        {
            UmodsApplication.EventHandlerManager.RegisterHandler(this);
        }


        protected void OnDestroy()
        {
            if (!_destroyed && _registered)
                InternalUOnDestroy();
        }

        private void InternalUOnDestroy()
        {
            // Destroying from inside to outside
            this.SafeCallMethod("UOnDestroy");

            if(this is UmodsModBehaviour) ((UmodsModBehaviour) this).UOnModDestroy();
            
            // Unregister from handlers and lifecycle
            UmodsApplication.EventHandlerManager.UnregisterHandler(this);
            UmodsApplication.UnregisterInLifeCycle(this);

            _destroyed = true;
        }
    }
}