namespace Umods.Scripts.Interfaces
{
    public interface IUmodsEventListener
    {
        bool PullEvent(string methodName, object[] parameters = null);
    }
}