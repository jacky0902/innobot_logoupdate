﻿namespace Umods.Scripts.Interfaces
{
    public interface IUmodsLateLifeCycle
    {
        void OnULateUpdate();
    }
}