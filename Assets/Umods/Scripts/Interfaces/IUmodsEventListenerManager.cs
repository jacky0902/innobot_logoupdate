﻿using Umods.Scripts.Interfaces;

namespace Umods
{
    public interface IUmodsEventListenerManager
    {
        IUmodsEventListener RegisterListener<T>();
        IUmodsEventListener GetListener<T>();
        bool UnregisterListener<T>();
        void ClearListeners();
    }
}