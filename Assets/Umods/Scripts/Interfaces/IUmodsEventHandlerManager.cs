﻿namespace Umods.Scripts.Interfaces
{
    public interface IUmodsEventHandlerManager
    {
        void RegisterHandler(object handlerObject);
        bool UnregisterHandler(object handlerObject);
    }
}