﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellStruct
{
    public Vector3 position;
    public Quaternion rotation;

    public CellStruct(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }
}
