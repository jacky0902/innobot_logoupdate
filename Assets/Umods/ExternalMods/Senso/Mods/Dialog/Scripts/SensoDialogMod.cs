﻿using System;
using Umods.Scripts.Common;
using Umods.Scripts.Core;
using Umods.Scripts.InternalMods.ExceptionsManager.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Senso.Dialog
{
    public partial class SensoDialogMod : UmodsModBehaviour, ISensoDialogMod
    {
        protected const int DialogPanelWidth = 300;
        protected const int DialogPanelHeight = 300;

        private Action _acceptAction;
        private Action<string> _acceptInputFieldAction;
        private Action _cancelAction;
        private Action _cancelInputFieldAction;

        private GameObject _currentDialog;

        private bool _dialogEnabled;

        private IUmodsExceptionEventManager _exceptionEventManager;

        private bool _exceptionEventSent;
        private Text _inputFieldText;

        private bool _isInputFieldAction;


        public GameObject BackgroundPanelGo;
        public GameObject ConfirmationDialog1Prefab;
        public GameObject ConfirmationDialog2Prefab;

        public GameObject Dialog1Prefab;
        public GameObject Dialog2Prefab;
        public Canvas DialogCanvas;
        public GameObject InputFieldConfirmation1Prefab;

        public bool DialogEnabled
        {
            get { return _dialogEnabled; }
            set
            {
                DialogCanvas.gameObject.SetActive(value);
                if (!value && _currentDialog != null)
                    Destroy(_currentDialog);
                _dialogEnabled = value;
            }
        }

        public void ShowDialog(SensoDialogEnums.SensoDialogModDialogType type, string title, string description,
            string acceptButtonLabel, Action onAccept, bool withBackgorund = false,
            bool sendBlockInputExceptionEvent = false)
        {
            DialogEnabled = true;
            switch (type)
            {
                case SensoDialogEnums.SensoDialogModDialogType.Dialog1:
                    _currentDialog = Instantiate(Dialog1Prefab, DialogCanvas.transform);
                    break;

                case SensoDialogEnums.SensoDialogModDialogType.Dialog2:
                    _currentDialog = Instantiate(Dialog2Prefab, DialogCanvas.transform);
                    break;

                case SensoDialogEnums.SensoDialogModDialogType.Confirmation1:
                    _currentDialog = Instantiate(ConfirmationDialog1Prefab, DialogCanvas.transform);
                    break;

                case SensoDialogEnums.SensoDialogModDialogType.Confirmation2:
                    _currentDialog = Instantiate(ConfirmationDialog2Prefab, DialogCanvas.transform);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }

            // Not resizing by the moment
            //_currentDialog.GetComponent<RectTransform>().sizeDelta = new Vector2(DialogPanelWidth, DialogPanelHeight);
            _currentDialog.transform.Find("Title").GetComponent<Text>().text = title;
            _currentDialog.transform.Find("Description").GetComponent<Text>().text = description;
            _currentDialog.transform.Find("AcceptButton").GetComponentInChildren<Text>().text = acceptButtonLabel;
            _acceptAction = onAccept;
            _currentDialog.transform.Find("AcceptButton").GetComponent<Button>().onClick
                .AddListener(OnAcceptButtonClicked);

            BackgroundPanelGo.SetActive(withBackgorund);

            if (sendBlockInputExceptionEvent)
                if (_exceptionEventManager != null)
                    _exceptionEventManager.SendExceptionEvent(
                        new UmodsException(UmodsEnums.UmodsExceptionType.InputBlocking, "Block", ""));

            _exceptionEventSent = sendBlockInputExceptionEvent;
            _isInputFieldAction = false;
        }

        public void ShowDialog(SensoDialogEnums.SensoDialogModDialogType type, string title, string description,
            string acceptButtonLabel, Action onAccept, string cancelButtonLabel, Action onCancel,
            bool withBackgorund = false, bool sendBlockInputExceptionEvent = false)
        {
            ShowDialog(type, title, description, acceptButtonLabel, onAccept, withBackgorund,
                sendBlockInputExceptionEvent);
            _currentDialog.transform.Find("CancelButton").GetComponentInChildren<Text>().text = cancelButtonLabel;
            _cancelAction = onCancel;
            _currentDialog.transform.Find("CancelButton").GetComponent<Button>().onClick
                .AddListener(OnCancelButtonClicked);
        }

        public void ShowDialogWithInputField(SensoDialogEnums.SensoDialogModDialogType type, string title,
            string description, string acceptButtonLabel, Action<string> onAccept, bool withBackgorund = false,
            bool sendBlockInputExceptionEvent = false)
        {
            DialogEnabled = true;
            switch (type)
            {
                case SensoDialogEnums.SensoDialogModDialogType.InputFieldConfirmation1:
                    _currentDialog = Instantiate(InputFieldConfirmation1Prefab, DialogCanvas.transform);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }

            // Not resizing by the moment
            //_currentDialog.GetComponent<RectTransform>().sizeDelta = new Vector2(DialogPanelWidth, DialogPanelHeight);
            _currentDialog.transform.Find("Title").GetComponent<Text>().text = title;
            _currentDialog.transform.Find("Description").GetComponent<Text>().text = description;
            _currentDialog.transform.Find("AcceptButton").GetComponentInChildren<Text>().text = acceptButtonLabel;
            _acceptInputFieldAction = onAccept;
            _currentDialog.transform.Find("AcceptButton").GetComponent<Button>().onClick
                .AddListener(OnAcceptButtonClicked);

            _inputFieldText = _currentDialog.transform.Find("InputField").Find("Text").GetComponent<Text>();

            BackgroundPanelGo.SetActive(withBackgorund);

            if (sendBlockInputExceptionEvent)
                if (_exceptionEventManager != null)
                    _exceptionEventManager.SendExceptionEvent(
                        new UmodsException(UmodsEnums.UmodsExceptionType.InputBlocking, "Block", ""));

            _exceptionEventSent = sendBlockInputExceptionEvent;
            _isInputFieldAction = true;
        }

        public void ShowDialogWithInputField(SensoDialogEnums.SensoDialogModDialogType type, string title,
            string description, string acceptButtonLabel, Action<string> onAccept, string cancelButtonLabel,
            Action onCancel, bool withBackgorund = false, bool sendBlockInputExceptionEvent = false)
        {
            ShowDialogWithInputField(type, title, description, acceptButtonLabel, onAccept, withBackgorund,
                sendBlockInputExceptionEvent);
            _currentDialog.transform.Find("CancelButton").GetComponentInChildren<Text>().text = cancelButtonLabel;
            _cancelAction = onCancel;
            _currentDialog.transform.Find("CancelButton").GetComponent<Button>().onClick
                .AddListener(OnCancelButtonClicked);
        }

        public override void OnListenersAndModRegistration()
        {
            UmodsApplication.RegisterMod<ISensoDialogMod>(this);
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterMod<ISensoDialogMod>();
        }

        public void UStart()
        {
            _exceptionEventManager = UmodsApplication.GetInternalMod<IUmodsExceptionEventManager>();
        }

        private void OnAcceptButtonClicked()
        {
            //Very Important this order
            DialogEnabled = false;

            if (_exceptionEventSent && _exceptionEventManager != null)
            {
                _exceptionEventManager.SendExceptionEvent(new UmodsException(
                    UmodsEnums.UmodsExceptionType.InputBlocking,
                    "Unblock", ""));
                _exceptionEventSent = false;
            }

            if (!_isInputFieldAction && _acceptAction != null)
                _acceptAction();
            else if (_isInputFieldAction && _acceptInputFieldAction != null)
                _acceptInputFieldAction(_inputFieldText.text);
        }

        private void OnCancelButtonClicked()
        {
            //Very Important this order
            DialogEnabled = false;

            if (_exceptionEventSent && _exceptionEventManager != null)
            {
                _exceptionEventManager.SendExceptionEvent(new UmodsException(
                    UmodsEnums.UmodsExceptionType.InputBlocking,
                    "Unblock", ""));
                _exceptionEventSent = false;
            }

            if (_cancelAction != null) _cancelAction();
        }
    }

    public partial class SensoDialogMod
    {
    }
}