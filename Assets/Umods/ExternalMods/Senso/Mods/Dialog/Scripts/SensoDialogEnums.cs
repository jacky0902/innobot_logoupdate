﻿namespace Senso.Dialog
{
    public static class SensoDialogEnums
    {
        public enum SensoDialogModDialogType
        {
            Dialog1,
            Dialog2,
            Confirmation1,
            Confirmation2,
            InputFieldConfirmation1
        }
    }
}