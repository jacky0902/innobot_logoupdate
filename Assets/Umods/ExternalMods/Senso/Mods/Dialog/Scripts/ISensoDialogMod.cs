﻿using System;

namespace Senso.Dialog
{
    public interface ISensoDialogMod
    {
        void ShowDialog(SensoDialogEnums.SensoDialogModDialogType type, string title, string description,
            string acceptButtonLabel, Action onAccept, bool withBackgorund = false,
            bool sendBlockInputExceptionEvent = false);

        void ShowDialog(SensoDialogEnums.SensoDialogModDialogType type, string title, string description,
            string acceptButtonLabel, Action onAccept, string cancelButtonLabel, Action onCancel,
            bool withBackgorund = false, bool sendBlockInputExceptionEvent = false);

        void ShowDialogWithInputField(SensoDialogEnums.SensoDialogModDialogType type, string title, string description,
            string acceptButtonLabel, Action<string> onAccept, bool withBackgorund = false,
            bool sendBlockInputExceptionEvent = false);

        void ShowDialogWithInputField(SensoDialogEnums.SensoDialogModDialogType type, string title, string description,
            string acceptButtonLabel, Action<string> onAccept, string cancelButtonLabel, Action onCancel,
            bool withBackgorund = false, bool sendBlockInputExceptionEvent = false);
    }
}