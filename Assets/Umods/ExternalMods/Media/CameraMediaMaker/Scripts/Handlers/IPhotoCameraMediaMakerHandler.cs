using UnityEngine.UI;

namespace DefaultNamespace
{
    public interface IPhotoCameraMediaMakerHandler
    {
        void OnPhotoSaved(bool success);
    }
}