using UnityEngine.UI;

namespace DefaultNamespace
{
    public interface IVideoCameraMediaMakerHandler
    {
        void OnVideoSaved(bool success);
    }
}