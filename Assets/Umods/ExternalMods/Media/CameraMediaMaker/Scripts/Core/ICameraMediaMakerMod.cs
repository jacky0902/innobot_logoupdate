using UnityEngine.UI;

namespace Umods.ExternalMods.Media.Scripts.Core
{
    public interface ICameraMediaMakerMod
    {
        void ShowCameraOnTexture(RawImage rawImage, AspectRatioFitter aspectRatioFitter);
        void ShowCameraOnTexture();
        void StopShowingCameraOnTexture();
        void TakePhotoAsync(string albumName, string pictureName);
        void StartRecordingVideo();
        void StopAndSaveRecordedVideoAsync(string albumName, string videoName);
    }
}