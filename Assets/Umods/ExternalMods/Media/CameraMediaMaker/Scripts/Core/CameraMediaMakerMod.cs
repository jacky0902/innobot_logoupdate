﻿using DefaultNamespace;
using NatCorder;
using NatCorder.Clocks;
using Umods.ExternalMods.Media.Scripts.Core;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class CameraMediaMakerMod : UmodsModBehaviour, ICameraMediaMakerMod
{
    private IUmodsEventListener _photoListener;
    private IUmodsEventListener _videoListener;

    private WebCamTexture _mainCam;
    private Texture _defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;

    public bool forceBackCamera = true;

    public int frameRate = 60;

    private MP4Recorder videoRecorder;
    private IClock clock;
    private Color32[] pixelBuffer;

    private string _albumName, _photoName, _videoName;

    private bool _configured = false;

    private enum CamState
    {
        NotAvailable,
        Available,
        ShowingCamera,
        TakingPhoto,
        Recording,
        SavingVideo
    }

    private CamState _camState = CamState.NotAvailable;

    private void UStart()
    {
        Application.RequestUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone);
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone)) return;
    }

    void Configuration()
    {
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone)) return;

        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera detected");
            _camState = CamState.NotAvailable;
            return;
        }

        if (forceBackCamera)
        {
            for (int i = 0; i < devices.Length; i++)
            {
                if (!devices[i].isFrontFacing)
                {
                    _mainCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
                }
            }

            if (_mainCam == null)
            {
                Debug.Log("Unable to find back camera");
                _camState = CamState.NotAvailable;
                return;
            }
        }
        else
        {
            if (devices.Length == 0)
            {
                Debug.Log("Unable to find any camera");
                _camState = CamState.NotAvailable;
                return;
            }
            else
            {
                _mainCam = new WebCamTexture(devices[0].name, Screen.width, Screen.height);
            }
        }

        _camState = CamState.Available;

        _configured = true;
    }

    private void UUpdate()
    {
        if (_camState == CamState.ShowingCamera)
        {
            if (fit != null)
            {
                float ratio = (float) _mainCam.width / (float) _mainCam.height;
                fit.aspectRatio = ratio;
            }

            float scaleY = _mainCam.videoVerticallyMirrored ? -1f : 1f; //-1 vertical
            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

            int orient = -_mainCam.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        }

        if (_camState == CamState.Recording)
        {
            // Record frames
            if (_mainCam.didUpdateThisFrame)
            {
                _mainCam.GetPixels32(pixelBuffer);
                videoRecorder.CommitFrame(pixelBuffer, clock.Timestamp);
            }
        }
    }

    public void ShowCameraOnTexture(RawImage rawImage, AspectRatioFitter aspectRatioFitter)
    {
        if(!_configured) Configuration();
        if (_camState != CamState.Available) return;

        background = rawImage;
        fit = aspectRatioFitter;
        ShowCameraOnTexture();
    }

    public void ShowCameraOnTexture()
    {
        if(!_configured) Configuration();
        if (_camState != CamState.Available) return;

        if (background == null) return;

        _defaultBackground = background.texture;
        background.texture = _mainCam;
        _mainCam.Stop();
        _mainCam.Play();

        _camState = CamState.ShowingCamera;
    }

    public void StopShowingCameraOnTexture()
    {
        if (_camState != CamState.ShowingCamera) return;

        _mainCam.Stop();
        background.texture = _defaultBackground;

        _camState = CamState.Available;
    }

    private static Texture2D TextureToTexture2D(Texture texture)
    {
        Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 32);
        Graphics.Blit(texture, renderTexture);

        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();

        RenderTexture.active = currentRT;
        RenderTexture.ReleaseTemporary(renderTexture);
        return texture2D;
    }

    public void TakePhotoAsync(string albumName, string pictureName)
    {
        if (_camState != CamState.ShowingCamera) return;
        _camState = CamState.TakingPhoto;

        NativeGallery.SaveImageToGallery(TextureToTexture2D(background.texture), albumName, pictureName, OnMediaTaken);
    }

    public void StartRecordingVideo()
    {
        if (_camState != CamState.ShowingCamera) return;
        _camState = CamState.Recording;

        // Start recording
        clock = new RealtimeClock();
        videoRecorder = new MP4Recorder(_mainCam.width, _mainCam.height, frameRate, 0, 0, OnRecorded);
        pixelBuffer = _mainCam.GetPixels32();
    }

    void OnRecorded(string path)
    {
        Debug.Log("Saved recording to: " + path);
        NativeGallery.SaveVideoToGallery(path, _albumName, _videoName, OnMediaTaken);
    }


    public void StopAndSaveRecordedVideoAsync(string albumName, string videoName)
    {
        if (_camState != CamState.Recording) return;
        _camState = CamState.SavingVideo;

        _albumName = albumName;
        _videoName = videoName;
        
        videoRecorder.Dispose();
        videoRecorder = null;
        pixelBuffer = null;
    }

    public override void OnListenersAndModRegistration()
    {
        UmodsApplication.RegisterMod<ICameraMediaMakerMod>(this);
        _photoListener = UmodsApplication.EventListenerManager.RegisterListener<IPhotoCameraMediaMakerHandler>();
        _videoListener = UmodsApplication.EventListenerManager.RegisterListener<IVideoCameraMediaMakerHandler>();
    }

    public override void OnDestroyListenersAndModUnregistration()
    {
        UmodsApplication.UnregisterMod<ICameraMediaMakerMod>();
        UmodsApplication.EventListenerManager.UnregisterListener<IPhotoCameraMediaMakerHandler>();
        UmodsApplication.EventListenerManager.UnregisterListener<IVideoCameraMediaMakerHandler>();
    }

    public void OnMediaTaken(string value)
    {
        if (_camState == CamState.TakingPhoto)
            _photoListener.PullEvent("OnPhotoSaved", new object[]
            {
                value == null
            });
        if (_camState == CamState.SavingVideo)
            _videoListener.PullEvent("OnVideoSaved", new object[]
            {
                value == null
            });

        _camState = CamState.ShowingCamera;
    }
}