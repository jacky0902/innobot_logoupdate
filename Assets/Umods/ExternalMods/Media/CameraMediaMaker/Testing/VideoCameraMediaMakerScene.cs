﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Umods.ExternalMods.Media.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class VideoCameraMediaMakerScene : UmodsSceneBehaviour, IPhotoCameraMediaMakerHandler, IVideoCameraMediaMakerHandler
{
    public Button takePhotoButton;
    public Button takeVideoButton;
    public Button saveVideoButton;

    private ICameraMediaMakerMod _cameraMediaMakerMod;

    void UStart()
    {
        _cameraMediaMakerMod = UmodsApplication.GetMod<ICameraMediaMakerMod>();
        takePhotoButton.onClick.AddListener(_TakePhoto);
        takeVideoButton.onClick.AddListener(_TakeVideo);
        saveVideoButton.onClick.AddListener(_SaveVideo);
        _cameraMediaMakerMod.ShowCameraOnTexture();
    }

    private void _TakePhoto()
    {
        _cameraMediaMakerMod.TakePhotoAsync("INNBOT", "picture1.jpg");
    }
    
    private void _TakeVideo()
    {
        _cameraMediaMakerMod.StartRecordingVideo();
    }
    
    private void _SaveVideo()
    {
        _cameraMediaMakerMod.StopAndSaveRecordedVideoAsync("INNOBOT", "video1.mp4");
    }

    public void OnPhotoSaved(bool success)
    {
        if (success)
            Debug.Log("Photo saved");
        else
            Debug.Log("Photo not saved");
    }

    public void OnVideoSaved(bool success)
    {
        if (success)
            Debug.Log("Video saved");
        else
            Debug.Log("Video not saved");
    }
}