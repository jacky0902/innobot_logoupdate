namespace Umods.ExternalMods.Storage.Mods.StorageMod.Handlers
{
    public interface ILoadFileHandler
    {
        void OnLoadFileCompleted(byte[] contentLoaded);
    }
}