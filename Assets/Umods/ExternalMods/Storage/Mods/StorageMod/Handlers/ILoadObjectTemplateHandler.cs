namespace Umods.ExternalMods.Storage.Mods.StorageMod.Handlers
{
    public interface ILoadObjectTemplateHandler
    {
        void OnLoadCompleted(object loadedObjec);
    }
}