using Umods.ExternalMods.Storage.Mods.StorageMod.Common;

namespace Umods.ExternalMods.Storage.Mods.StorageMod.Handlers
{
    public interface IStorageCompletedHandler
    {
        void OnStorageCompleted(StorageVariables.StoragingState state);
    }
}