﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Umods.ExternalMods.Storage.Mods.StorageMod.Common;
using Umods.ExternalMods.Storage.Mods.StorageMod.Handlers;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;

namespace Umods.ExternalMods.Storage.Mods.StorageMod.Core
{
    public class StorageMod : UmodsModBehaviour, IStorageMod
    {
        private StoragingState _modState = StoragingState.None;
        private IUmodsEventListener _loadingFileListener;
        private IUmodsEventListener _storageCompletedListener;
        private IUmodsEventListener _loadingTemplateListener;

        private int _bufferSize = 0x1000;

        public override void OnListenersAndModRegistration()
        {
            UmodsApplication.RegisterMod<IStorageMod>(this);
            _loadingFileListener = UmodsApplication.EventListenerManager.RegisterListener<ILoadFileHandler>();
            _storageCompletedListener =
                UmodsApplication.EventListenerManager.RegisterListener<IStorageCompletedHandler>();
            _loadingTemplateListener =
                UmodsApplication.EventListenerManager.RegisterListener<ILoadObjectTemplateHandler>();
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterMod<IStorageMod>();
            UmodsApplication.EventListenerManager.UnregisterListener<ILoadFileHandler>();
            UmodsApplication.EventListenerManager.UnregisterListener<IStorageCompletedHandler>();
            UmodsApplication.EventListenerManager.UnregisterListener<ILoadObjectTemplateHandler>();
        }

        public void StorageFileAsync(byte[] contentToSave, string path)
        {
            if (_modState != StoragingState.None) return;
            _modState = StoragingState.Storaging;

            var directoryPath = Path.GetDirectoryName(path);
            if (!Directory.Exists(directoryPath))
            {
                try
                {
                    Directory.CreateDirectory(directoryPath);
                }
                catch (Exception e)
                {
                    Debug.Log(e.ToString());
                    throw;
                }
            }

            try
            {
                _WriteTextAsync(contentToSave, path);
            }
            catch (Exception e)
            {
                Debug.Log(string.Format("Object {0} not saved in \"{1}\": {2}", contentToSave.ToString(), path,
                    e.ToString()));
                throw;
            }
        }

        public void LoadFileAsync(string path)
        {
            if (_modState != StoragingState.None) return;
            _modState = StoragingState.Storaging;
        }

        public void StorageObjectTemplateAsync<T>(T objectToSave, string path) where T : StorageTemplate
        {
            if (_modState != StoragingState.None) return;

            var json = JsonUtility.ToJson(objectToSave);

            StorageFileAsync(Encoding.Unicode.GetBytes(json), path);
        }


        private async void _WriteTextAsync(byte[] contentToWrite, string filePath)
        {
            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Create, FileAccess.Write, FileShare.None,
                bufferSize: _bufferSize, useAsync: true))
            {
                try
                {
                    await sourceStream.WriteAsync(contentToWrite, 0, contentToWrite.Length);
                    sourceStream.Close();
                    _modState = StoragingState.None;

                    Debug.Log(string.Format("Object  saved in \"{1}\"", Encoding.Unicode.GetString(contentToWrite).Substring(0,30), filePath));
                    _storageCompletedListener.PullEvent("OnStorageCompleted",
                        new object[] {StorageVariables.StoragingState.Storaged});
                }
                catch (Exception e)
                {
                    _modState = StoragingState.None;
                    Debug.Log(string.Format("Object {0} not saved in \"{1}\": {2}", Encoding.UTF8.GetString(contentToWrite).Substring(0,30), filePath,
                        e.ToString()));

                    _storageCompletedListener.PullEvent("OnStorageCompleted",
                        new object[] {StorageVariables.StoragingState.NotStoraged});
                    throw;
                }
            }

            ;
        }

        public void LoadObjectTemplateAsync<T>(string path) where T : StorageTemplate
        {
            if (_modState != StoragingState.None) return;
            _modState = StoragingState.Storaging;
            _ReadTextAsync<T>(path);
        }

        public string[] GetFilesInDirectory(string path)
        {
            return Directory.GetFiles(path);
        }

        private string _objectLoaded;

        private async void _ReadTextAsync<T>(string filePath)
        {
            try
            {
                using (FileStream sourceStream = new FileStream(filePath,
                    FileMode.Open, FileAccess.Read, FileShare.Read,
                    bufferSize: _bufferSize, useAsync: true))
                {
                    StringBuilder sb = new StringBuilder();

                    byte[] buffer = new byte[_bufferSize];
                    int numRead;
                    while ((numRead = await sourceStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                    {
                        string text = Encoding.Unicode.GetString(buffer, 0, numRead);
                        sb.Append(text);
                    }

                    _modState = StoragingState.None;

                    sourceStream.Close();

                    Debug.Log(string.Format("Object {0} loaded from {1}.", sb.ToString().Substring(0,30), filePath));

                    _loadingTemplateListener.PullEvent("OnLoadCompleted",
                        new object[] {JsonUtility.FromJson<T>(sb.ToString())});
                }
            }
            catch (Exception e)
            {
                _modState = StoragingState.None;

                Debug.Log(string.Format("Object not loaded from \"{0}\": {1}", filePath, e.ToString()));

                _loadingTemplateListener.PullEvent("OnLoadCompleted",
                    new object[] {null});
                throw;
            }
        }

        enum StoragingState
        {
            None,
            Storaging,
            Loading
        }
    }
}