namespace Umods.ExternalMods.Storage.Mods.StorageMod.Core
{
    public interface IStorageMod
    {
        void StorageFileAsync(byte[] contentToSave, string path);
       // void LoadFileAsync(string path);
        
        void StorageObjectTemplateAsync<T>(T objectToSave, string path) where T: StorageTemplate;
        void LoadObjectTemplateAsync<T>(string path) where T: StorageTemplate;

        string[] GetFilesInDirectory(string path);
    }
}