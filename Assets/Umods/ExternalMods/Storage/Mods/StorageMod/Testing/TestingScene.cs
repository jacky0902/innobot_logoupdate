﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Umods.ExternalMods.Storage.Mods.StorageMod.Common;
using Umods.ExternalMods.Storage.Mods.StorageMod.Core;
using Umods.ExternalMods.Storage.Mods.StorageMod.Handlers;
using Umods.ExternalMods.Storage.Mods.StorageMod.Testing;
using Umods.Scripts.Core;
using UnityEngine;

public class TestingScene : UmodsSceneBehaviour, IStorageCompletedHandler, ILoadObjectTemplateHandler
{
    
    
    private IStorageMod _storageMod;

    void UStart()
    {
        _storageMod = UmodsApplication.GetMod<IStorageMod>();
        _storageMod.StorageObjectTemplateAsync<TestingObjectTemplate>(new TestingObjectTemplate(), Path.Combine(Application.persistentDataPath, "test_folder1/test_folder2/test_file1.txt"));
    }

    public void OnStorageCompleted(StorageVariables.StoragingState state)
    {
        Debug.Log(state.ToString());
        _storageMod.LoadObjectTemplateAsync<TestingObjectTemplate>(Path.Combine(Application.persistentDataPath, "test_folder1/test_folder2/test_file1.txt"));
    }

    public void OnLoadCompleted(object loadedObjec)
    {
        Debug.Log((TestingObjectTemplate)loadedObjec);

    }
}
