﻿using System.Collections;
using DefaultNamespace;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;

namespace Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core
{
    public class GyroscopeControllerMod : UmodsModBehaviour, IGyroscopeControllerMod
    {
        private GyroscopeState _gyroscopeState = GyroscopeState.NotEnabled;
        private IUmodsEventListener _umodsEventListener;
        private Coroutine _updateGyroscopeDataCoroutine;

        public bool StartReceivingGyroscopeStatus(float updateInterval = 0.0167f)
        {
            if (_gyroscopeState != GyroscopeState.NotEnabled) return false;
            if (!SystemInfo.supportsGyroscope)
            {
                Debug.Log("Device does not support gyroscope");
                return false;
            }

            _gyroscopeState = GyroscopeState.Enabled;

            Input.gyro.enabled = true;
            Input.gyro.updateInterval = updateInterval;

            _updateGyroscopeDataCoroutine = StartCoroutine(_UpdateGyroscopeData(updateInterval));
            return true;
        }


        public void StopReceivingGyroscopeStatus()
        {
            if (_gyroscopeState != GyroscopeState.Enabled) return;
            _gyroscopeState = GyroscopeState.NotEnabled;

            Input.gyro.enabled = false;
            StopCoroutine(_updateGyroscopeDataCoroutine);
            _updateGyroscopeDataCoroutine = null;
        }

        public override void OnListenersAndModRegistration()
        {
            UmodsApplication.RegisterMod<IGyroscopeControllerMod>(this);
            _umodsEventListener = UmodsApplication.EventListenerManager.RegisterListener<IGyroscopeControllerHandler>();
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterMod<IGyroscopeControllerMod>();
            UmodsApplication.EventListenerManager.UnregisterListener<IGyroscopeControllerHandler>();
        }


        private IEnumerator _UpdateGyroscopeData(float updateInterval)
        {
            while (true)
            {
                _umodsEventListener.PullEvent("OnNewGyroscopeStatus", new object[] {GyroToUnity(Input.gyro.attitude)});
                yield return new WaitForSeconds(updateInterval);
            }
        }

        private static Quaternion GyroToUnity(Quaternion q)
        {
            return new Quaternion(q.x, q.y, -q.z, -q.w);
        }

        private enum GyroscopeState
        {
            Enabled,
            NotEnabled
        }
    }
}