namespace Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core
{
    public interface IGyroscopeControllerMod
    {
        bool StartReceivingGyroscopeStatus(float updateInterval= 0.0167f); // 60 Hz
        void StopReceivingGyroscopeStatus();
    }
}