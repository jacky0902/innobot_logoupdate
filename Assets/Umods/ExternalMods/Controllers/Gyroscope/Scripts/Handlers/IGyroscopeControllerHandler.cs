using UnityEngine;

namespace DefaultNamespace
{
    public interface IGyroscopeControllerHandler
    {
        void OnNewGyroscopeStatus(Quaternion rotation);
    }
}