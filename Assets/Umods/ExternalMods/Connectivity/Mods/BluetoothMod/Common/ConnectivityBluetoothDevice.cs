using System;

namespace Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common
{
    public class ConnectivityBluetoothDevice : IEquatable<ConnectivityBluetoothDevice>, IComparable<ConnectivityBluetoothDevice>
    {
        public string DeviceName { protected set; get; }

        public string DeviceAddress { protected set; get; }
        
        public float TimeUpdated { set; get; }

        public ConnectivityBluetoothDevice( string deviceAddress, string deviceName, float timeUpdated)
        {
            this.DeviceAddress = deviceAddress;
            this.DeviceName = deviceName;
            this.TimeUpdated = timeUpdated;
        }

        public bool Equals(ConnectivityBluetoothDevice other)
        {
            return other != null && this.DeviceAddress.Equals(other.DeviceAddress) && this.DeviceName.Equals(other.DeviceName);        }

        public int CompareTo(ConnectivityBluetoothDevice other)
        {
            // A null value means that this object is greater.
            return other == null ? 1 : this.DeviceName.CompareTo(other.DeviceName);
        }
    }
}