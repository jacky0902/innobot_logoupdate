using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;

namespace Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core
{
    public class ConnectivityBluetoothMod : UmodsModBehaviour, IConnectivityBluetoothMod
    {
        // Mod variables
        private IUmodsEventListener _bluetoothConnectivityListener;

        // Connectivity variables
        private enum ConnectionStates
        {
            None,
            Connected,
            Connecting,
            Disconnecting,
            ScanningDevices,
        }
        
        private enum MessageStates
        {
            None,
            SendingMessage,
        }

        private ConnectionStates _connectionState = ConnectionStates.None;
        private MessageStates _messageState = MessageStates.None;
        private Coroutine _connectingTimeoutCoroutine = null;

        // Devices 
        private readonly Dictionary<string, ConnectivityBluetoothDevice> _bluetoothDevicesHistoricDict =
            new Dictionary<string, ConnectivityBluetoothDevice>();

        private ConnectivityBluetoothDevice _bluetoothDeviceConnected;
        private ConnectivityBluetoothDevice _bluetoothDeviceToConnect;

        private Coroutine _updateDevicesDictCoroutine;


        // Sending messages        
        private readonly Queue<byte[]> _messagesQueue = new Queue<byte[]>();


        // Public variables
        public float connectionTimeout;
        public bool simulateConnectivityMod;
        public float timeBluetoothDeviceActiveOnList = 1;


        // Umods methodes
        public override void OnListenersAndModRegistration()
        {
            if (!simulateConnectivityMod)
                UmodsApplication.RegisterMod<IConnectivityBluetoothMod>(this);
            else
                UmodsApplication.RegisterMod<IConnectivityBluetoothMod>(transform
                    .GetComponentInChildren<SimulationConnectivityBluetoothModMod>());

            _bluetoothConnectivityListener =
                UmodsApplication.EventListenerManager.RegisterListener<IConnectivityBluetoothHandler>();
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterMod<IConnectivityBluetoothMod>();
            UmodsApplication.EventListenerManager.UnregisterListener<IConnectivityBluetoothHandler>();
        }

        // Start Service
        private void UStart()
        {
            Debug.Log(string.Format("ConnectivityModule: BluetoothStarts"));

            BluetoothLEHardwareInterface.Initialize(true, false, () => { },
                error =>
                {
                    Debug.Log(string.Format("ConnectivityModule: Initialize error: {0}", error));

                    if (error.Contains("Bluetooth LE Not Enabled"))
                        BluetoothLEHardwareInterface.BluetoothEnable(true);
                });
        }

        // Scanning devices
        public void StartScanningBluetoothDeviceListAsync(string serviceUUID = null)
        {
            if (_connectionState != ConnectionStates.None) return;

            _bluetoothDevicesHistoricDict.Clear();

            _connectionState = ConnectionStates.ScanningDevices;
            BluetoothLEHardwareInterface.ScanForPeripheralsWithServices(serviceUUID == null ? null : new[] {serviceUUID}, null,
                _OnNewBluetoothDeviceScanned, true);

            Debug.Log(string.Format("ConnectivityModule: Start scanning bluetooth devices"));
        }

        public void StopScanningBluetoothDeviceList()
        {
            if (_connectionState != ConnectionStates.ScanningDevices) return;

            _connectionState = ConnectionStates.None;
            BluetoothLEHardwareInterface.StopScan();

            Debug.Log(string.Format("ConnectivityModule: Stop scanning bluetooth devices"));
        }

        public bool IsScanningBluetoothDevices()
        {
            return _connectionState == ConnectionStates.ScanningDevices;
        }


        // Connecting to device
        public bool ConnectToBluetoothDeviceAsync(string address)
        {
            if (_connectionState == ConnectionStates.ScanningDevices)
                StopScanningBluetoothDeviceList();
            if (_connectionState != ConnectionStates.None) return false;

            if (!_bluetoothDevicesHistoricDict.ContainsKey(address))
            {
                _bluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
                    new object[] {null, ConnectivityVariables.ConnectionState.WrongAddress});
                return false;
            }

            // By now only one bluetooth connection    
            if (_bluetoothDeviceConnected != null)
                DisconnectFromBluetoothDeviceAsync();

            BluetoothLEHardwareInterface.ConnectToPeripheral(address,
                _OnBluetoothConnected, null, null, _OnBluetoothDisconnected);

            _connectingTimeoutCoroutine = StartCoroutine(_ConnectingTimeout());
            _connectionState = ConnectionStates.Connecting;
            return true;
        }


        public void DisconnectFromBluetoothDeviceAsync()
        {
            if (_connectionState == ConnectionStates.Connected)
            {
                _connectionState = ConnectionStates.Disconnecting;
                BluetoothLEHardwareInterface.DisconnectAll();
            }
        }


        // Sending messages
        public void SendMessageToConnectedBluetooth(WrittingOptions writtingOptions, string message,
            bool waitUntilLastMessageResponded = false)
        {
            SendMessageToConnectedBluetooth(writtingOptions, Encoding.UTF8.GetBytes(message),
                waitUntilLastMessageResponded);
        }

        public void SendMessageToConnectedBluetooth(WrittingOptions writtingOptions, byte[] message,
            bool waitUntilLastMessageResponded = false)
        {
            if (_connectionState == ConnectionStates.Connected && _messageState == MessageStates.SendingMessage && waitUntilLastMessageResponded)
            {
                _messagesQueue.Enqueue(message);
            }
            else
            {
                if (_connectionState == ConnectionStates.Connected)
                {
                    _SendMessage(writtingOptions, message);
                }
            }
        }


        public bool IsConnectedToAnyBluetoothDevice()
        {
            return _connectionState == ConnectionStates.Connected;
        }

        public bool IsConnectedToBluetoothDevice(string address)
        {
            return _connectionState == ConnectionStates.Connected &&
                   _bluetoothDeviceConnected.DeviceAddress.Equals(address);
        }

        public ConnectivityBluetoothDevice GetBluetoothDevice(string address)
        {
            if (_bluetoothDevicesHistoricDict.ContainsKey(address)) return _bluetoothDevicesHistoricDict[address];
            return null;
        }


        public ConnectivityBluetoothDevice GetConnectedBluetoothDevice()
        {
            return _connectionState == ConnectionStates.Connected ? _bluetoothDeviceConnected : null;
        }


        // Scaning devices
        private void _OnNewBluetoothDeviceScanned(string address, string name, int rssi, byte[] bytes)
        {
            if (!_bluetoothDevicesHistoricDict.ContainsKey(address))
            {
                var bluetoothDevice = new ConnectivityBluetoothDevice(address, name, Time.time);
                _bluetoothDevicesHistoricDict.Add(address, bluetoothDevice);

                _UpdateDeviceDict();

                _bluetoothConnectivityListener.PullEvent("OnBluetoothDeviceListUpdated",
                    new object[] {_bluetoothDevicesHistoricDict.Values.ToList()});
                if (_updateDevicesDictCoroutine != null)
                {
                    StopCoroutine(_updateDevicesDictCoroutine);
                    _updateDevicesDictCoroutine = null;
                }

                _updateDevicesDictCoroutine = StartCoroutine(_UpdateDevicesDictCoroutine());
            }
            else
            {
                _bluetoothDevicesHistoricDict[address].TimeUpdated = Time.time;
            }
        }

        private IEnumerator _UpdateDevicesDictCoroutine()
        {
            while (true)
            {
                if (_UpdateDeviceDict())
                    _bluetoothConnectivityListener.PullEvent("OnBluetoothDeviceListUpdated",
                        new object[] {_bluetoothDevicesHistoricDict.Values.ToList()});

                if (_bluetoothDevicesHistoricDict.Count == 0)
                    break;

                yield return new WaitForSeconds(timeBluetoothDeviceActiveOnList);
            }

            _updateDevicesDictCoroutine = null;
        }

        private bool _UpdateDeviceDict()
        {
            var changed = false;
            var values = _bluetoothDevicesHistoricDict.Values.ToList();
            foreach (var connectivityBluetoothDevice in values)
                if (Time.time - connectivityBluetoothDevice.TimeUpdated > timeBluetoothDeviceActiveOnList)
                {
                    _bluetoothDevicesHistoricDict.Remove(connectivityBluetoothDevice.DeviceAddress);
                    changed = true;
                }

            return changed;
        }


        // Connecting device


        private void _OnBluetoothConnected(string address)
        {
            if (_connectionState != ConnectionStates.Connecting) BluetoothLEHardwareInterface.DisconnectAll();
            else
            {
                if (_connectingTimeoutCoroutine != null)
                {
                    StopCoroutine(_connectingTimeoutCoroutine);
                    _connectingTimeoutCoroutine = null;
                }

                _connectionState = ConnectionStates.Connected;
                _bluetoothDeviceConnected = _bluetoothDevicesHistoricDict[address];

                _bluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
                    new object[]
                    {
                        _bluetoothDeviceConnected, ConnectivityVariables.ConnectionState.Connected
                    });

                Debug.Log(string.Format("ConnectivityModule: Connected: \"{0}\"", address));
            }
        }


        private void _OnBluetoothDisconnected(string address)
        {
            if (_connectionState != ConnectionStates.Connected &&
                _connectionState != ConnectionStates.Disconnecting) return;

            _connectionState = ConnectionStates.None;
            var deviceAux = _bluetoothDeviceConnected;
            _bluetoothDeviceConnected = null;

            _bluetoothConnectivityListener.PullEvent("OnBluetoothDisconnected",
                new object[]
                {
                    deviceAux, ConnectivityVariables.ConnectionState.Disconnected
                });

            Debug.Log(string.Format("ConnectivityModule: Disconnected: \"{0}\"", address));
        }

        private IEnumerator _ConnectingTimeout()
        {
            yield return new WaitForSeconds(connectionTimeout);
            _OnConnectionFailed(ConnectivityVariables.ConnectionState.ConnectionTimeout);
            _connectingTimeoutCoroutine = null;
        }

        private void _OnConnectionFailed(ConnectivityVariables.ConnectionState connectionState)
        {
            if (_connectionState == ConnectionStates.Connecting)
            {
                _connectionState = ConnectionStates.None;

                _bluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
                    new object[]
                    {
                        _bluetoothDeviceToConnect, connectionState
                    });

                Debug.Log(string.Format("ConnectivityModule: Connection failed: \"{0}\"", connectionState.ToString()));
            }
        }

        // Sending messages

        private void _SendMessage(WrittingOptions writtingOptions, byte[] message)
        {
            _messageState = MessageStates.SendingMessage;
            BluetoothLEHardwareInterface.WriteCharacteristic(_bluetoothDeviceConnected.DeviceAddress,
                writtingOptions.ServiceUUID,
                writtingOptions.WrittingCharacteristicUUID, message, message.Length, false,
                null);

            Debug.Log(string.Format("ConnectivityModule: Message sent, \"{0}\"", Encoding.UTF8.GetString(message)));
        }

        private void _OnReceivedBluetooth(string address, string characteristicUUID, byte[] bytes)
        {
            Debug.Log("Received Serial: " + Encoding.UTF8.GetString(bytes));
        }


        public class WrittingOptions
        {
            public WrittingOptions(string serviceUUID, string writtingWrittingCharacteristicUuid)
            {
                ServiceUUID = serviceUUID;
                WrittingCharacteristicUUID = writtingWrittingCharacteristicUuid;
            }

            public string ServiceUUID { get; }
            public string WrittingCharacteristicUUID { get; }
        }
        
        string FullUUID (string uuid)
        {
            return "0000" + uuid + "-0000-1000-8000-00805f9b34fb";
        }
	
        bool IsEqual(string uuid1, string uuid2)
        {
            if (uuid1.Length == 4)
                uuid1 = FullUUID (uuid1);
            if (uuid2.Length == 4)
                uuid2 = FullUUID (uuid2);
		
            return (uuid1.ToUpper().CompareTo(uuid2.ToUpper()) == 0);
        }
    }
}