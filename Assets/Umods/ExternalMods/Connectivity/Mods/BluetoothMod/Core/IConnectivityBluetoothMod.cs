using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;

namespace Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core
{
    public interface IConnectivityBluetoothMod
    {
        void StartScanningBluetoothDeviceListAsync(string serviceUUID = null);
        void StopScanningBluetoothDeviceList();
        bool IsScanningBluetoothDevices();
        
        bool ConnectToBluetoothDeviceAsync(string address);
        void DisconnectFromBluetoothDeviceAsync();

        void SendMessageToConnectedBluetooth(ConnectivityBluetoothMod.WrittingOptions writtingOptions, string message,
            bool waitUntilLastMessageResponded = false);

        void SendMessageToConnectedBluetooth(ConnectivityBluetoothMod.WrittingOptions writtingOptions, byte[] message,
            bool waitUntilLastMessageResponded = false);


        bool IsConnectedToAnyBluetoothDevice();
        bool IsConnectedToBluetoothDevice(string address);
        ConnectivityBluetoothDevice GetConnectedBluetoothDevice();
        ConnectivityBluetoothDevice GetBluetoothDevice(string address);

    }
}