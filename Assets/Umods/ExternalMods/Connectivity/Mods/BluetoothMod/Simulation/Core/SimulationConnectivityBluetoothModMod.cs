using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core
{
    public class SimulationConnectivityBluetoothModMod : UmodsModBehaviour,
        ISimulationConnectivityBluetoothModMod
    {
        public float timePerBluetoothDevicesScan = 0.5f;
        public float probabilityBluetoothDevicesAppearsInList = 0.9f;

        public float connectionErrorProbability = 0.2f;

        public float timeWaintingUntilBluetoothConnects = 1.0f;
        public float timeBluetoothDisconnectsAfterConnection = 300.0f;

        private IUmodsEventListener _bluetoothConnectivityListener;

        Dictionary<string, ConnectivityBluetoothDevice> _bluetoothDevicesHistoricDict =
            new Dictionary<string, ConnectivityBluetoothDevice>();

        List<ConnectivityBluetoothDevice> _lastBluetoothDeviceList = new List<ConnectivityBluetoothDevice>();

        private Coroutine _simulateGettingBluetoothDeviceCoroutine = null;
        private string _connectingId;

        private ConnectivityBluetoothDevice _bluetoothDeviceConnected = null;

        public override void OnListenersAndModRegistration()
        {
            //UmodsApplication.RegisterMod<ISimulationConnectivityBluetoothMod>(this);
            _bluetoothConnectivityListener =
                UmodsApplication.EventListenerManager.RegisterListener<IConnectivityBluetoothHandler>();
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            //UmodsApplication.UnregisterMod<ISimulationConnectivityBluetoothMod>();
            UmodsApplication.EventListenerManager.UnregisterListener<IConnectivityBluetoothHandler>();
        }

        public void StartScanningBluetoothDeviceListAsync(string ServiceUUID = "")
        {
            if (_simulateGettingBluetoothDeviceCoroutine == null)
                _simulateGettingBluetoothDeviceCoroutine = StartCoroutine(_SimulateGettingBluetoothDeviceList());
        }

        // DEBUG
        private IEnumerator _SimulateGettingBluetoothDeviceList()
        {
            List<ConnectivityBluetoothDevice> posibleBluetoothDevicesList = new List<ConnectivityBluetoothDevice>()
            {
                new ConnectivityBluetoothDevice("123", "FOOL1", 0),
                new ConnectivityBluetoothDevice("234", "FOOL2", 0),
                new ConnectivityBluetoothDevice("345", "FOOL3", 0),
                new ConnectivityBluetoothDevice("456", "INNOBOT", 0),
                new ConnectivityBluetoothDevice("567", "DSD TECH", 0),
                new ConnectivityBluetoothDevice("678", "FOOL5", 0),
                new ConnectivityBluetoothDevice("789", "FOOL6", 0)
            };

            List<ConnectivityBluetoothDevice> bluetoothDevicesToSend = new List<ConnectivityBluetoothDevice>();
            bool[] added = new bool[posibleBluetoothDevicesList.Count];

            while (true)
            {
                yield return new WaitForSeconds(timePerBluetoothDevicesScan);

                bluetoothDevicesToSend.Clear();
                for (int i = 0; i < posibleBluetoothDevicesList.Count; i++)
                {
                    if (Random.Range(0.0f, 1.0f) <= probabilityBluetoothDevicesAppearsInList)
                    {
                        bluetoothDevicesToSend.Add(posibleBluetoothDevicesList[i]);
                    }
                }

                OnBluetoothDeviceListUpdated(bluetoothDevicesToSend);
            }
        }


        public void StopScanningBluetoothDeviceList()
        {
            if (_simulateGettingBluetoothDeviceCoroutine != null)
            {
                StopCoroutine(_simulateGettingBluetoothDeviceCoroutine);
                _simulateGettingBluetoothDeviceCoroutine = null;
            }
        }

        public bool ConnectToBluetoothDeviceAsync(string address)
        {
            if (_bluetoothDevicesHistoricDict.ContainsKey(address))
            {
                _connectingId = address;
                Invoke("_SimulateBluetoothConection", timeWaintingUntilBluetoothConnects);
                return true;
            }

            _bluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
                new object[] {null, ConnectivityVariables.ConnectionState.WrongAddress});

            return false;
        }

        public void DisconnectFromBluetoothDeviceAsync()
        {
            if (_bluetoothDeviceConnected != null)
                Invoke("_SimulateBluetoothDisconection", 0.5f);
        }

        public void SendMessageToConnectedBluetooth(ConnectivityBluetoothMod.WrittingOptions writtingOptions,
            string message,
            bool waitUntilLastMessageResponded = false)
        {
            Debug.Log(string.Format("Message sent: {0}", message));
        }

        public void SendMessageToConnectedBluetooth(ConnectivityBluetoothMod.WrittingOptions writtingOptions,
            byte[] message,
            bool waitUntilLastMessageResponded = false)
        {
            Debug.Log(string.Format("Message sent: {0}", BitConverter.ToString(message)));
        }

        public bool IsConnectedToAnyBluetoothDevice()
        {
            return _bluetoothDeviceConnected != null;
        }

        public bool IsConnectedToBluetoothDevice(string address)
        {
            return _bluetoothDeviceConnected != null && _bluetoothDeviceConnected.DeviceAddress.Equals(address);
        }

        public ConnectivityBluetoothDevice GetConnectedBluetoothDevice()
        {
            return _bluetoothDeviceConnected;
        }

        public ConnectivityBluetoothDevice GetBluetoothDevice(string address)
        {
            return _bluetoothDevicesHistoricDict.ContainsKey(address) ? _bluetoothDevicesHistoricDict[address] : null;
        }

        public bool IsScanningBluetoothDevices()
        {
            return _simulateGettingBluetoothDeviceCoroutine != null;
        }

        // DEBUG
        private void _SimulateBluetoothConection()
        {
            if (_bluetoothDeviceConnected != null)
            {
                OnBluetoothDisconnected(_bluetoothDeviceConnected, ConnectivityVariables.ConnectionState.Disconnected);
            }

            if (Random.Range(0.0f, 1.0f) <= connectionErrorProbability)
            {
                OnBluetoothConnected(_bluetoothDevicesHistoricDict[_connectingId],
                    ConnectivityVariables.ConnectionState.ConnectionError);
            }
            else
            {
                OnBluetoothConnected(_bluetoothDevicesHistoricDict[_connectingId],
                    ConnectivityVariables.ConnectionState.Connected);
                Invoke("_SimulateBluetoothDisconection", timeBluetoothDisconnectsAfterConnection);
            }
        }

        private void _SimulateBluetoothDisconection()
        {
            if (_bluetoothDeviceConnected != null)
                OnBluetoothDisconnected(_bluetoothDeviceConnected, ConnectivityVariables.ConnectionState.Disconnected);
        }

        private void OnBluetoothDeviceListUpdated(List<ConnectivityBluetoothDevice> bluetoothDevices)
        {
            var lastNotCurrent = _lastBluetoothDeviceList.Except(bluetoothDevices).Count();
            var currentNotLast = bluetoothDevices.Except(_lastBluetoothDeviceList).Count();

            if (lastNotCurrent != 0 || currentNotLast != 0)
            {
                foreach (var bluetoothDevice in bluetoothDevices)
                {
                    if (!_bluetoothDevicesHistoricDict.ContainsKey(bluetoothDevice.DeviceAddress))
                        _bluetoothDevicesHistoricDict.Add(bluetoothDevice.DeviceAddress, bluetoothDevice);
                }

                _lastBluetoothDeviceList = new List<ConnectivityBluetoothDevice>(bluetoothDevices);
                _bluetoothConnectivityListener.PullEvent("OnBluetoothDeviceListUpdated",
                    new object[] {bluetoothDevices});
            }
        }

        public void OnBluetoothConnected(ConnectivityBluetoothDevice ConnectivityBluetoothDevice,
            ConnectivityVariables.ConnectionState state)
        {
            if (state == ConnectivityVariables.ConnectionState.Connected)
            {
                _bluetoothDeviceConnected = ConnectivityBluetoothDevice;
            }

            _bluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
                new object[] {ConnectivityBluetoothDevice, state});
        }

        public void OnBluetoothDisconnected(ConnectivityBluetoothDevice ConnectivityBluetoothDevice,
            ConnectivityVariables.ConnectionState state)
        {
            _bluetoothDeviceConnected = null;
            _bluetoothConnectivityListener.PullEvent("OnBluetoothDisconnected",
                new object[] {ConnectivityBluetoothDevice, state});
        }
    }
}