using System.Collections.Generic;
using ArRobot.Modules.Common;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;

namespace Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers
{
    public interface IConnectivityBluetoothHandler
    {
        void OnBluetoothDeviceListUpdated(List<ConnectivityBluetoothDevice> bluetoothDevices);
        void OnBluetoothConnected(ConnectivityBluetoothDevice bluetoothDevice,
            ConnectivityVariables.ConnectionState state);
        void OnBluetoothDisconnected(ConnectivityBluetoothDevice bluetoothDevice,
            ConnectivityVariables.ConnectionState state);

        void OnMessageSent(string response);
    }
}