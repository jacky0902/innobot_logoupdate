﻿using System;
using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;

[Serializable]
public class ScopeScreenManager
{
   public string scopeName;
   public ScopeScreenManager[] nestedScopes = new ScopeScreenManager[0];
}
