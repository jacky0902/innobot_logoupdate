using System.Collections.Generic;
using Umods.Scripts.Core;

namespace UnityEngine
{
    public class ScreenScope: UmodsBehaviour
    {
        public string scopeName;
        public string rootScreen;

        public Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen _currentScreen;

        private Dictionary<string, Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen> _screens;

        void UStart()
        {
            foreach (var screen in GetComponentsInChildren<Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen>())
            {
                _screens.Add(screen.screenName, screen);
            }
        }

        public Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen GetRootScreen()
        {
            return _screens[rootScreen];
        }
        
        private void _LoadScreen(Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen screen)
        {
            screen.gameObject.SetActive(true);
            _currentScreen = screen;
        }
    }
}