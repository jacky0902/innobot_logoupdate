using System;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common
{
    public class Button : UmodsBehaviour
    {
        [SerializeField] public string buttonName;
        [SerializeField] private ButtonType type;
        [SerializeField] private UnityEngine.UI.Button buttonComponent;
        [SerializeField] private string nextScreenName;
        [SerializeField] private string nextScopeName;

        public Action OnBackScreenButtonPressed
        {
            get { return _onBackScreenButtonPressed; }
            set { _onBackScreenButtonPressed = value; }
        }

        public Action<string> OnNextScreenButtonPressed
        {
            get { return _onNextScreenButtonPressed; }
            set { _onNextScreenButtonPressed = value; }
        }

        private Action _onBackScreenButtonPressed;
        private Action<string> _onNextScreenButtonPressed;

        public Action OnBackScopeButtonPressed
        {
            get { return _onBackScopeButtonPressed; }
            set { _onBackScopeButtonPressed = value; }
        }

        public Action<string> OnNextScopeButtonPressed
        {
            get { return _onNextScopeButtonPressed; }
            set { _onNextScopeButtonPressed = value; }
        }

        private Action _onBackScopeButtonPressed;
        private Action<string> _onNextScopeButtonPressed;
        

        void UStart()
        {
            if (type == ButtonType.BackScreen)
            {
                buttonComponent.onClick.AddListener(() => _onBackScreenButtonPressed());
            }
            else if (type == ButtonType.NextScreen)
            {
                buttonComponent.onClick.AddListener(() => _onNextScreenButtonPressed(nextScreenName));
            }
        }
    }

    public enum ButtonType
    {
        None,
        BackScreen,
        NextScreen,
        BackScope,
        NextScope
    }
}