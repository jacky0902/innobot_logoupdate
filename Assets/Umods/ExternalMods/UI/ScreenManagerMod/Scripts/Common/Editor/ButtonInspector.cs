using UnityEditor;

namespace Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Editor
{
    [CustomEditor(typeof(Button)), CanEditMultipleObjects]
    public class ButtonInspector : UnityEditor.Editor
    {
        private Button _buttonTarget;
        private UnityEngine.UI.Button _unityButton;
        private Screen _screen;
        private Button[] _buttonsOnScreen;
        private bool _nameRepeated;
        private string _lastNameChecked;

        public void OnEnable()
        {
            _buttonTarget = (Button) target;
            if (_buttonTarget.buttonName.Equals("")) _buttonTarget.buttonName = target.name;

            _unityButton = _buttonTarget.GetComponent<UnityEngine.UI.Button>();

            _screen = _buttonTarget.gameObject.GetComponentInParent<Screen>();

            if (_screen != null)
                _buttonsOnScreen = _screen.GetComponentsInChildren<Button>();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            bool warning = false;

            if (_screen == null)
            {
                EditorGUILayout.HelpBox("This component should be children of a Screen component.", MessageType.Error);
                warning = true;
            }

            if (_unityButton == null)
            {
                EditorGUILayout.HelpBox("This component need a Unity Button component to work.", MessageType.Error);
                warning = true;
            }

            if (CheckNameRepeated())
            {
                EditorGUILayout.HelpBox("This button name is repeated in the same screen.", MessageType.Error);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("name"));

                warning = true;
            }

            if (!warning)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("name"));

                var type = serializedObject.FindProperty("type");
                EditorGUILayout.PropertyField(type);
                EditorGUI.indentLevel += 1;
                if (type.enumValueIndex == (int) ButtonType.NextScreen)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("nextScreenName"));
                EditorGUI.indentLevel -= 1;
            }

            serializedObject.ApplyModifiedProperties();
        }

        private bool CheckNameRepeated()
        {
            if (!_buttonTarget.buttonName.Equals(_lastNameChecked))
            {
                var counter = 0;
                foreach (var button in _buttonsOnScreen)
                {
                    if (_buttonTarget.buttonName == button.buttonName) counter++;
                }

                _lastNameChecked = _buttonTarget.buttonName;
                _nameRepeated = counter > 1;
            }

            return _nameRepeated;
        }
    }
}