﻿using Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core;
using UnityEditor;

[CustomEditor(typeof(ScreenManagerMod)), CanEditMultipleObjects]
public class ScreenManagerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorList.Show(serializedObject.FindProperty("scopes"),
            EditorListOption.Buttons | EditorListOption.ListLabel | EditorListOption.ElementLabels);
        serializedObject.ApplyModifiedProperties();
    }
}
