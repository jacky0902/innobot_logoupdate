using System;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;


namespace Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common
{
    public class Screen : UmodsBehaviour
    {
        public string screenName;
        public ScreenScope scope;
        private Dictionary<string, Button> _buttons = new Dictionary<string, Button>();

        public Action OnBackScreenButtonPressed
        {
            get { return _onBackScreenButtonPressed; }
            set { _onBackScreenButtonPressed = value; }
        }

        public Action<string> OnNextScreenButtonPressed
        {
            get { return _onNextScreenButtonPressed; }
            set { _onNextScreenButtonPressed = value; }
        }

        private Action _onBackScreenButtonPressed;
        private Action<string> _onNextScreenButtonPressed;

        public Action OnBackScopeButtonPressed
        {
            get { return _onBackScopeButtonPressed; }
            set { _onBackScopeButtonPressed = value; }
        }

        public Action<string> OnNextScopeButtonPressed
        {
            get { return _onNextScopeButtonPressed; }
            set { _onNextScopeButtonPressed = value; }
        }

        private Action _onBackScopeButtonPressed;
        private Action<string> _onNextScopeButtonPressed;

        private void UStart()
        {
            foreach (var buttonSM in GetComponentsInChildren<Button>(true))
            {
                _buttons.Add(buttonSM.buttonName, buttonSM);
                buttonSM.OnBackScreenButtonPressed = _onBackScreenButtonPressed;
                buttonSM.OnNextScreenButtonPressed = _onNextScreenButtonPressed;
                buttonSM.OnBackScopeButtonPressed = _onBackScopeButtonPressed;
                buttonSM.OnNextScopeButtonPressed = _onNextScopeButtonPressed;
            }
        }

        private void _OnBackButtonPressed()
        {
        }
    }
}