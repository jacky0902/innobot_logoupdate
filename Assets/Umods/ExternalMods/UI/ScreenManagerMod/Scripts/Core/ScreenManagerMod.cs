﻿using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;
using Screen = Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Common.Screen;

namespace Umods.ExternalMods.UI.ScreenManagerMod.Scripts.Core
{
    public class ScreenManagerMod : UmodsModBehaviour, IScreenManagerMod
    {
        private Dictionary<string, ScreenScope> _scopes;

        private Stack<Screen> _screensStack = new Stack<Screen>();
        private Stack<ScreenScope> _scopeStak = new Stack<ScreenScope>();

        private ScreenScope _currentScope = null;
        private Screen _currentScreen = null;

        private int _screensOnStackOfCurrentScope = 0;

        public string rootScope;
        public string rootScreen;

        public override void OnListenersAndModRegistration()
        {
            UmodsApplication.RegisterMod<IScreenManagerMod>(this);
        }

        public override void OnDestroyListenersAndModUnregistration()
        {
            UmodsApplication.UnregisterMod<IScreenManagerMod>();
        }

        void UStart()
        {
            foreach (var scope in FindObjectsOfType<ScreenScope>())
            {
                _scopes.Add(scope.scopeName, scope);
            }

            foreach (var screen in FindObjectsOfType<Screen>())
            {
                screen.OnBackScreenButtonPressed = OnBackScreenButtonPressed;
                screen.OnNextScreenButtonPressed = OnNextScreenButtonPressed;
                screen.OnBackScopeButtonPressed = OnBackScopeButtonPressed;
                screen.OnNextScopeButtonPressed = OnNextScopeButtonPressed;

                screen.gameObject.SetActive(false);
            }

           // _LoadScreen(_scopes[rootScope].GetRootScreen());
        }

        public void OnBackScreenButtonPressed()
        {
        }

        public void OnNextScreenButtonPressed(string screenName)
        {
        }

        public void OnBackScopeButtonPressed()
        {
        }

        public void OnNextScopeButtonPressed(string scopeName)
        {
        }

    

        private void ChangeScope(ScreenScope scope)
        {
           // _currentScope = screen.scope;

        }
    }
}