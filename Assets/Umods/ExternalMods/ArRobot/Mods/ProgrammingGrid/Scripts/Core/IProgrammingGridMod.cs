﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IProgrammingGridMod
{
    void RemoveButton(int row, int column);
    void ClearGrid();
    void CreateGrid();

    void InitializeGrid(GameObject gridContainer, ScrollRect scrollViewRect, UIProgrammingPanel[] panels, Button leftArrow,
        Button rightArrow);

    void InitializeColumn(GameObject gridContainer);


    void CreateColumn();

    void AddButton(RobotAction.MotionAction action, int column = 0);
    void AddButton(RobotAction.EmotionAction action, int column = 0);
    void AddButton(RobotAction.ArmsAction action,  int column = 0);
    void AddButton(RobotAction.SoundAction action,  int column = 0);

    void GetGridActions(out RobotAction.MotionAction[] motionActions, out RobotAction.EmotionAction[] emotionActions,
        out RobotAction.ArmsAction[] armsActions, out RobotAction.SoundAction[] soundActions);

    void FillGrid(RobotAction.MotionAction[] motionActions, RobotAction.EmotionAction[] emotionActions,
        RobotAction.ArmsAction[] armsActions, RobotAction.SoundAction[] soundActions);

    void FillColumn(int index);


    void SetColumnActivation(int column, bool actived);

    void SetEditable(bool value);
}