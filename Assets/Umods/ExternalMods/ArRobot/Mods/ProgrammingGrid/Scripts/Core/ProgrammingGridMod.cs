﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class ProgrammingGridMod : UmodsModBehaviour, IProgrammingGridMod
{
    private float _buttonIncrement;
    private float _pagerIncrement;
    private int _columnsInViewport;

    private GameObject[][] _buttonsAdded;
    private GameObject[][] _gridSlots;

    private GameObject[] _buttonsAddedCol;
    private GameObject[] _gridSlotsCol;


    private Dictionary<RobotAction.MotionAction, Sprite> _motionSpritesDict;
    private Dictionary<RobotAction.SoundAction, Sprite> _soundsSpritesDict;
    private Dictionary<RobotAction.EmotionAction, Sprite> _emotionsSpritesDict;
    private Dictionary<RobotAction.ArmsAction, Sprite> _armsSpritesDict;

    private Color[] _colors;

    private int _rowsNum;
    private int _columnsNum;


    private Coroutine _scrollCoroutine;

    private IUmodsEventListener _umodsEventListener;

    private float borderPerc = 3.656f;


    private int _lastColumnAsigned = -1;

    private bool _editable = true;

    // Collors
    public Color motionColor = Color.white;
    public Color emotionColor = Color.white;
    public Color armsColor = Color.white;
    public Color soundColor = Color.white;

    public GameObject actionBtnPref;

    public GameObject gridBtnPref;
    public GameObject plusBtnPref;

    public int gridLength = 100;

    private RobotAction.MotionAction[] _motionActions;
    private RobotAction.EmotionAction[] _emotionActions;
    private RobotAction.ArmsAction[] _armsActions;
    private RobotAction.SoundAction[] _soundActions;

    private UIProgrammingPanel[] _panels;

    private Button _leftArrow;
    private Button _rightArrow;
    private GameObject _gridContainerN;
    private GameObject _gridContainerCol;
    private ScrollRect _scrollViewRect;


    public void CreateGrid()
    {
        StartCoroutine(_CreateGrid(_rowsNum, _columnsNum));
    }

    public void InitializeGrid(GameObject gridContainer, ScrollRect scrollViewRect, UIProgrammingPanel[] panels,
        Button leftArrow, Button rightArrow)
    {
        _lastColumnAsigned = -1;

        _gridContainerN = gridContainer;
        _scrollViewRect = scrollViewRect;
        _panels = panels;
        _leftArrow = leftArrow;
        _rightArrow = rightArrow;

        _rowsNum = 4;
        _columnsNum = gridLength;

        // Inicializo las flechas
        _leftArrow.onClick.AddListener(() => _ScrollGrill(true));
        _rightArrow.onClick.AddListener(() => _ScrollGrill(false));

        foreach (var panel in _panels)
        {
            panel.OnButtonPressed += OnPanelPressed;
            panel.gameObject.SetActive(true);
            panel.gameObject.SetActive(false);
        }


        _motionActions = new RobotAction.MotionAction[_columnsNum];
        _emotionActions = new RobotAction.EmotionAction[_columnsNum];
        _armsActions = new RobotAction.ArmsAction[_columnsNum];
        _soundActions = new RobotAction.SoundAction[_columnsNum];
        for (int i = 0; i < _columnsNum; i++)
        {
            _motionActions[i] = RobotAction.MotionAction.None;
            _emotionActions[i] = RobotAction.EmotionAction.None;
            _armsActions[i] = RobotAction.ArmsAction.None;
            _soundActions[i] = RobotAction.SoundAction.None;
        }
    }

    public void InitializeColumn(GameObject gridContainer)
    {
        _gridContainerCol = gridContainer;
        _rowsNum = 4;
    }

    private void _CreateColumn(int rows)
    {
        // Compruebo los gameobjects que necesito
        if (_gridContainerCol == null) throw new UnityException("GridContainer is not assigned");
        if (plusBtnPref == null) throw new UnityException("PlusButtonPrefab is not assigned");
        if (gridBtnPref == null) throw new UnityException("GridButtonPrefab is not assigned");

        // Creo el array de slots
        _gridSlotsCol = new GameObject[rows];

        // Creo el array de botones
        _buttonsAddedCol = new GameObject[rows];

        // Cojo la altura del boton y de los bordes
        var buttonHeight =
            _gridContainerCol.GetComponent<RectTransform>().rect.height / rows;

        var borderHeight = buttonHeight * borderPerc / 100f;

        buttonHeight = buttonHeight + borderHeight / rows * (rows - 1);

        _gridContainerCol.GetComponent<GridLayoutGroup>().cellSize = new Vector3(buttonHeight, buttonHeight);
        _gridContainerCol.GetComponent<GridLayoutGroup>().spacing = new Vector3(-borderHeight, -borderHeight);

        for (var i = 0; i < rows; i++)
        {
            // Instancion un botón más y aplico tamaño
            var gridBtn = Instantiate(gridBtnPref, _gridContainerCol.transform).transform.Find("InnerButton")
                .gameObject;

            _gridSlotsCol[i] = gridBtn;
        }

        _gridContainerCol.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        _gridContainerCol.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
    }

    private IEnumerator _CreateGrid(int rows, int columns)
    {
        // Compruebo los gameobjects que necesito
        if (_gridContainerN == null) throw new UnityException("GridContainer is not assigned");
        if (plusBtnPref == null) throw new UnityException("PlusButtonPrefab is not assigned");
        if (gridBtnPref == null) throw new UnityException("GridButtonPrefab is not assigned");

        // Creo el array de slots
        _gridSlots = new GameObject[rows][];
        for (var i = 0; i < rows; i++)
        {
            _gridSlots[i] = new GameObject[columns];
        }

        // Creo el array de botones
        _buttonsAdded = new GameObject[rows][];
        for (var i = 0; i < rows; i++)
        {
            _buttonsAdded[i] = new GameObject[columns];
        }

        // Cojo la altura del boton y de los bordes
        var buttonHeight =
            _gridContainerN.GetComponent<RectTransform>().rect.height / rows;

        var borderHeight = buttonHeight * borderPerc / 100f;

        buttonHeight = buttonHeight + borderHeight / rows * (rows - 1);

        _gridContainerN.GetComponent<GridLayoutGroup>().cellSize = new Vector3(buttonHeight, buttonHeight);
        _gridContainerN.GetComponent<GridLayoutGroup>().spacing = new Vector3(-borderHeight, -borderHeight);

        for (var j = 0; j < columns; j++)
        {
            for (var i = 0; i < rows; i++)
            {
                // Instancion un botón más y aplico tamaño
                var gridBtn = Instantiate(gridBtnPref, _gridContainerN.transform).transform.Find("InnerButton")
                    .gameObject;

                if (columns != 1)
                {
                    var plusBtn = Instantiate(plusBtnPref, gridBtn.transform);

                    plusBtn.transform.Find("PlusImage").GetComponent<Image>().color = _colors[i];

                    // Si se pulsa el boton más
                    var rowClicked = i;
                    var columnClicked = j;
                    plusBtn.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        PlusButtonPressed(rowClicked, columnClicked);
                    });
                }

                _gridSlots[i][j] = gridBtn;
            }

            if (j % 10 == 0)
                yield return null;
        }

        var rectTransformContent = _gridContainerN.transform.parent.GetComponent<RectTransform>();
        var rectTransformGridPanel = _gridContainerN.GetComponent<RectTransform>();
        var rectTransformViewportPanel = _gridContainerN.transform.parent.parent.GetComponent<RectTransform>();

        // Modifica el tamaño del contenido
        rectTransformContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
            buttonHeight * columns - borderHeight * (columns - 1) + rectTransformGridPanel.offsetMin.x -
            rectTransformGridPanel.offsetMax.x);

        _columnsInViewport = (int) (rectTransformViewportPanel.rect.width / buttonHeight);
        _pagerIncrement = (_columnsInViewport * buttonHeight) /
                          rectTransformContent.rect.width;
    }

    public void RemoveButton(int row, int column)
    {
        if (_buttonsAdded[row][column] != null)
        {
            switch (_buttonsAdded[row][column].GetComponent<UIProgrammingPanelButton>().actionType)
            {
                case UIProgrammingPanel.ActionType.Motion:
                    _motionActions[column] = RobotAction.MotionAction.None;
                    break;
                case UIProgrammingPanel.ActionType.Emotion:
                    _emotionActions[column] = RobotAction.EmotionAction.None;
                    break;
                case UIProgrammingPanel.ActionType.Arms:
                    _armsActions[column] = RobotAction.ArmsAction.None;
                    break;
                case UIProgrammingPanel.ActionType.Sound:
                    _soundActions[column] = RobotAction.SoundAction.None;
                    break;
            }

            Destroy(_buttonsAdded[row][column]);
            _buttonsAdded[row][column] = null;
        }
    }

    public void RemoveButtonFromColumn(int row)
    {
        if (_buttonsAddedCol[row] != null)
        {
            Destroy(_buttonsAddedCol[row]);
            _buttonsAddedCol[row] = null;
        }
    }

    public void ClearGrid()
    {
        for (var i = 0; i < _rowsNum; i++)
        for (var j = 0; j < _columnsNum; j++)
            if (_buttonsAdded[i][j] != null)
            {
                Destroy(_buttonsAdded[i][j]);
                _buttonsAdded[i][j] = null;
            }

        _motionActions = new RobotAction.MotionAction[_columnsNum];
        _emotionActions = new RobotAction.EmotionAction[_columnsNum];
        _armsActions = new RobotAction.ArmsAction[_columnsNum];
        _soundActions = new RobotAction.SoundAction[_columnsNum];
        
        for (int i = 0; i < _columnsNum; i++)
        {
            _motionActions[i] = RobotAction.MotionAction.None;
            _emotionActions[i] = RobotAction.EmotionAction.None;
            _armsActions[i] = RobotAction.ArmsAction.None;
            _soundActions[i] = RobotAction.SoundAction.None;
        }

        _lastColumnAsigned = -1;
    }

    public void CreateColumn()
    {
        _CreateColumn(_rowsNum);
        FillColumn(0);
    }

    public void FillColumn(int index)
    {
        RemoveButtonFromColumn(0);
        RemoveButtonFromColumn(1);
        RemoveButtonFromColumn(2);
        RemoveButtonFromColumn(3);

        if (index < 0) return;
        
        if (_motionActions[index] != RobotAction.MotionAction.Stop  && _motionActions[index] != RobotAction.MotionAction.None)
            _AddButtonToColumn(0, _panels[0].GetButtonPrefab(_motionActions[index]));
        if (_emotionActions[index] != RobotAction.EmotionAction.Stop && _emotionActions[index] != RobotAction.EmotionAction.None)
            _AddButtonToColumn(1, _panels[1].GetButtonPrefab(_emotionActions[index]));
        if (_armsActions[index] != RobotAction.ArmsAction.Stop && _armsActions[index] != RobotAction.ArmsAction.None)
            _AddButtonToColumn(2, _panels[2].GetButtonPrefab(_armsActions[index]));
        if (_soundActions[index] != RobotAction.SoundAction.Stop && _soundActions[index] != RobotAction.SoundAction.None)
            _AddButtonToColumn(3, _panels[3].GetButtonPrefab(_soundActions[index]));
    }

    public void AddButton(RobotAction.MotionAction action, int column)
    {
        _AddButton(0, column, _panels[0].GetButtonPrefab(action));
        _motionActions[column] = action;
    }

    public void AddButton(RobotAction.EmotionAction action, int column)
    {
        _AddButton(1, column, _panels[1].GetButtonPrefab(action));
        _emotionActions[column] = action;
    }

    public void AddButton(RobotAction.ArmsAction action, int column)
    {
        _AddButton(2, column, _panels[2].GetButtonPrefab(action));
        _armsActions[column] = action;
    }

    public void AddButton(RobotAction.SoundAction action, int column)
    {
        _AddButton(3, column, _panels[3].GetButtonPrefab(action));
        _soundActions[column] = action;
    }

    public void GetGridActions(out RobotAction.MotionAction[] motionActions,
        out RobotAction.EmotionAction[] emotionActions, out RobotAction.ArmsAction[] armsActions,
        out RobotAction.SoundAction[] soundActions)
    {
        motionActions = _motionActions.Take(_lastColumnAsigned + 2).ToArray();
        emotionActions = _emotionActions.Take(_lastColumnAsigned + 2).ToArray();
        soundActions = _soundActions.Take(_lastColumnAsigned + 2).ToArray();
        armsActions = _armsActions.Take(_lastColumnAsigned + 2).ToArray();
        motionActions[_lastColumnAsigned + 1] = RobotAction.MotionAction.Stop;
        emotionActions[_lastColumnAsigned + 1] = RobotAction.EmotionAction.Stop;
        soundActions[_lastColumnAsigned + 1] = RobotAction.SoundAction.Stop;
        armsActions[_lastColumnAsigned + 1] = RobotAction.ArmsAction.Stop;
    }

    public void FillGrid(RobotAction.MotionAction[] motionActions, RobotAction.EmotionAction[] emotionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.SoundAction[] soundActions)
    {
        ClearGrid();

        int length = Mathf.Min(motionActions.Length, emotionActions.Length, armsActions.Length, soundActions.Length);

        for (int i = 0; i < length; i++)
        {
            if (motionActions[i] != RobotAction.MotionAction.None && motionActions[i] != RobotAction.MotionAction.Stop)
                AddButton(motionActions[i], i);
            if (emotionActions[i] != RobotAction.EmotionAction.None &&
                emotionActions[i] != RobotAction.EmotionAction.Stop)
                AddButton(emotionActions[i], i);
            if (armsActions[i] != RobotAction.ArmsAction.None && armsActions[i] != RobotAction.ArmsAction.Stop)
                AddButton(armsActions[i], i);
            if (soundActions[i] != RobotAction.SoundAction.None && soundActions[i] != RobotAction.SoundAction.Stop)
                AddButton(soundActions[i], i);
        }
    }

    public void SetColumnActivation(int column, bool actived)
    {
        for (int i = 0; i < _rowsNum; i++)
        {
            _gridSlots[i][column].transform.parent.Find("Actived").gameObject.SetActive(actived);
        }

        float x = 1f / (((float) (_columnsNum) / _columnsInViewport) - 1f);

        int w = (int) (column / _columnsInViewport);

        float m = x * w;

        if (!(_scrollViewRect.normalizedPosition.x >= m && _scrollViewRect.normalizedPosition.x <= m + x))
        {
            if (_scrollCoroutine == null)
                StartCoroutine(ScrollAnimation(_scrollViewRect.normalizedPosition.x,
                    _scrollViewRect.normalizedPosition.x > m + x ? m : m, 0.5f));
        }
    }

    public void SetEditable(bool value)
    {
        _editable = value;
    }

    private void UAwake()
    {
        // Inicializo los colores
        _colors = new Color[4] {motionColor, emotionColor, armsColor, soundColor};

        // Load Sprites
        _motionSpritesDict = new Dictionary<RobotAction.MotionAction, Sprite>
        {
            {
                RobotAction.MotionAction.TurnLeft, Resources.Load<Sprite>("motion/motion_left")
            },
            {
                RobotAction.MotionAction.MoveForward, Resources.Load<Sprite>("motion/motion_up")
            },
            {
                RobotAction.MotionAction.TurnRight, Resources.Load<Sprite>("motion/motion_right")
            },
            {
                RobotAction.MotionAction.MoveBackward, Resources.Load<Sprite>("motion/motion_down")
            }
        };

        _armsSpritesDict = new Dictionary<RobotAction.ArmsAction, Sprite>
        {
            {RobotAction.ArmsAction.OpenArms, Resources.Load<Sprite>("arms/arms_open")},
            {RobotAction.ArmsAction.CloseArms, Resources.Load<Sprite>("arms/arms_close")}
        };

        var faces = Resources.LoadAll<Sprite>("face/faces");
        _emotionsSpritesDict = new Dictionary<RobotAction.EmotionAction, Sprite>();

        for (var i = 0; i < faces.Length; i++) _emotionsSpritesDict.Add((RobotAction.EmotionAction) i, faces[i]);

        var sounds = Resources.LoadAll<Sprite>("sounds");
        _soundsSpritesDict = new Dictionary<RobotAction.SoundAction, Sprite>();

        for (var i = 0; i < sounds.Length; i++) _soundsSpritesDict.Add((RobotAction.SoundAction) i, sounds[i]);
    }

    // Botón más presionado
    private void PlusButtonPressed(int row, int column)
    {
        if (_editable)
            _panels[row].ShowPanel(column);
    }

    private void OnPanelPressed(int column, UIProgrammingPanelButton button)
    {
        if (button.isTrash)
            RemoveButton((int) button.actionType, column);
        else
        {
            switch (button.actionType)
            {
                case UIProgrammingPanel.ActionType.Motion:
                    AddButton(button.motionAction, column);
                    break;
                case UIProgrammingPanel.ActionType.Emotion:
                    AddButton(button.emotionAction, column);
                    break;
                case UIProgrammingPanel.ActionType.Arms:
                    AddButton(button.armsAction, column);
                    break;
                case UIProgrammingPanel.ActionType.Sound:
                    AddButton(button.soundAction, column);
                    break;
            }
        }
    }


    private void ExpandRect(RectTransform rect)
    {
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.zero;
        rect.pivot = new Vector2(0.5f, 0.5f);
    }

    private void _AddButton(int row, int column, GameObject buttonPref)
    {
        // Si ya había uno, se elimina
        if (_buttonsAdded[row][column] != null) Destroy(_buttonsAdded[row][column]);

        // Se instancia y se le da tamaño
        var btn = Instantiate(buttonPref, _gridSlots[row][column].transform);
        ExpandRect(btn.GetComponent<RectTransform>());

        btn.GetComponent<Button>().onClick.AddListener(() => PlusButtonPressed(row, column));

        _buttonsAdded[row][column] = btn;

        if (_lastColumnAsigned < column) _lastColumnAsigned = column;
    }

    private void _AddButtonToColumn(int row, GameObject buttonPref)
    {
        // Si ya había uno, se elimina
        if (_buttonsAddedCol[row] != null) Destroy(_buttonsAddedCol[row]);

        // Se instancia y se le da tamaño
        if (buttonPref != null)
        {
            var btn = Instantiate(buttonPref, _gridSlotsCol[row].transform);
            _buttonsAddedCol[row] = btn;
            ExpandRect(btn.GetComponent<RectTransform>());


            btn.transform.Find("Icon").GetComponent<Image>().color = Color.black;

            btn.GetComponent<Image>().color = Color.white;
            btn.GetComponent<Button>().enabled = false;

            _buttonsAddedCol[row] = btn;
        }
    }


    public override void OnListenersAndModRegistration()
    {
        UmodsApplication.RegisterMod<IProgrammingGridMod>(this);
        _umodsEventListener = UmodsApplication.EventListenerManager.RegisterListener<IProgrammingGridHandler>();
    }

    public override void OnDestroyListenersAndModUnregistration()
    {
        UmodsApplication.UnregisterMod<IProgrammingGridMod>();
        UmodsApplication.EventListenerManager.UnregisterListener<IProgrammingGridHandler>();
    }


    private void _ScrollGrill(bool left)
    {
        if (_scrollCoroutine == null)
            StartCoroutine(ScrollAnimation(_scrollViewRect.normalizedPosition.x,
                left
                    ? Mathf.Max(0, _scrollViewRect.normalizedPosition.x - _pagerIncrement)
                    : Mathf.Min(1, _scrollViewRect.normalizedPosition.x + _pagerIncrement), 0.5f));
    }

    private IEnumerator ScrollAnimation(float xCurrentPosition, float xTargetPosition, float duration)
    {
        var startTime = Time.time;

        while (true)
        {
            var t = (Time.time - startTime) / duration;
            if (t > 1)
            {
                _scrollViewRect.normalizedPosition = new Vector2(xTargetPosition, 0);
                _scrollCoroutine = null;
                yield break;
            }

            _scrollViewRect.normalizedPosition =
                new Vector2(Mathf.Lerp(xCurrentPosition, xTargetPosition, t), 0);
            yield return null;
        }
    }
}