﻿using Umods;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;

public class ExampleMod : UmodsModBehaviour, IExampleMod
{
    private IUmodsEventListener _umodsEventListener;

    public void SendMsg(string myName)
    {
        _umodsEventListener.PullEvent("OnReceiveReply", new[] {"Hi " + myName + "!"});
    }

    public override void OnListenersAndModRegistration()
    {
        UmodsApplication.RegisterMod<IExampleMod>(this);
        _umodsEventListener = UmodsApplication.EventListenerManager.RegisterListener<IExampleHandler>();
    }

    public override void OnDestroyListenersAndModUnregistration()
    {
        UmodsApplication.UnregisterMod<IExampleMod>();
        UmodsApplication.EventListenerManager.UnregisterListener<IExampleHandler>();
    }
}