﻿using Umods;
using Umods.Scripts.Core;
using UnityEngine;

public class ExampleScene : UmodsSceneBehaviour, IExampleHandler
{
    public void OnReceiveReply(string message)
    {
        Debug.Log("Message received: " + message);
    }

    public void UStart()
    {
        UmodsApplication.GetMod<IExampleMod>().SendMsg("Fran");
    }
}