﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowsRealTime : MonoBehaviour
{
    public Button upButton;
    public Button downButton;

    public List<GameObject> panels;

    private int counter = 0;
    private GameObject _activePanel;

    private void Start()
    {
        _activePanel = panels[counter];

        upButton.onClick.AddListener((() =>
        {
            _activePanel.SetActive(false);
            counter++;
            if (counter >= panels.Count)
                counter = 0;
            _activePanel = panels[counter];
            _activePanel.SetActive(true);
        }));

        downButton.onClick.AddListener((() =>
        {
            _activePanel.SetActive(false);
            counter--;
            if (counter < 0)
                counter = panels.Count -1;
            _activePanel = panels[counter];
            _activePanel.SetActive(true);
        }));
    }
}