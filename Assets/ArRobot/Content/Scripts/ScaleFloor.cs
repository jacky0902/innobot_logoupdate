﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleFloor : MonoBehaviour
{

    public int extension;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = -extension; i < extension; i++)
        {
            for (int j = -extension; j < extension; j++)
            {
               var go =  Instantiate(this, transform.position + Vector3.forward * i * transform.localScale.x + Vector3.left * j * transform.localScale.y, Quaternion.identity);
               Destroy(go.GetComponent<ScaleFloor>());
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
