﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnClickSendEmail : MonoBehaviour, IPointerClickHandler
{

    public string email;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Application.OpenURL("mailto:" + email + "?subject=" + "ARROBOT App" + "&body=" + "Hi Fran");
    }
}
