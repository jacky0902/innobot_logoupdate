﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class RobotActionCategory
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private Color _color;
    [SerializeField]
    private Dictionary<string, RobotAction> _robotActions = new Dictionary<string, RobotAction>();

    public RobotActionCategory(string name, Color color)
    {
        _name = name;
        _color = color;
    }

    public string Name
    {
        get { return _name; }
    }

    public Color Color
    {
        get { return _color; }
    }

    public List<RobotAction> RobotActions
    {
        get { return _robotActions.Values.ToList(); }
    }

    public void AddAction(RobotAction robotAction)
    {
        robotAction.Color = _color;
        robotAction.Category = this;
        _robotActions.Add(robotAction.Name, robotAction);
    }

    public RobotAction GetAction(string action)
    {
        return _robotActions[action];
    }
}
