﻿using UnityEngine;

public class RowColumn : MonoBehaviour
{
    // Start is called before the first frame update
    private int row, column;

    public int Row
    {
        get { return row; }
        set { row = value; }
    }

    public int Column
    {
        get { return column; }
        set { column = value; }
    }
}
