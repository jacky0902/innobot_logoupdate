﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransparentButton : MonoBehaviour {

	private GameObject _panelToHide;
	
	void Start()
	{
		_panelToHide = transform.parent.gameObject;
		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	public void OnClick()
	{
		_panelToHide.SetActive(false);
	}
}
