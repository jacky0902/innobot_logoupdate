﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowToShowButtons : MonoBehaviour
{
    public Button button1;
    public Button button2;
    public List<GameObject> button1ShowsObjects = new List<GameObject>();
    public List<GameObject> button2ShowsObjects = new List<GameObject>();

    private bool turn = false;

    private void Start()
    {
        button1.onClick.AddListener(Hide);
        button1.gameObject.SetActive(true);
        button2.onClick.AddListener(Hide);
        button2.gameObject.SetActive(false);
        
        foreach (var o in button1ShowsObjects)
        {
            o.SetActive(false);
        }
        
        foreach (var o in button2ShowsObjects)
        {
            o.SetActive(true);
        }
    }

    public void Hide()
    {
        button1.gameObject.SetActive(turn);
        button2.gameObject.SetActive(!turn);

        foreach (var o in button1ShowsObjects)
        {
            o.SetActive(!turn);
        }
        
        foreach (var o in button2ShowsObjects)
        {
            o.SetActive(turn);
        }

        turn = !turn;
    }
}
