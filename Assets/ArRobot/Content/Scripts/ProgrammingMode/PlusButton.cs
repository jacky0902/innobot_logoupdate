﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlusButton : MonoBehaviour
{

	public GameObject PanelToShow;
	public GameObject Empty;
	private EmptyScript _emptyScript;
	
	void Start()
	{
		GetComponent<Button>().onClick.AddListener(OnClick);
		_emptyScript = Empty.GetComponent<EmptyScript>();
	}

	public void OnClick()
	{
		PanelToShow.SetActive(true);
		_emptyScript.PlusButton = this.gameObject;
	}
}
