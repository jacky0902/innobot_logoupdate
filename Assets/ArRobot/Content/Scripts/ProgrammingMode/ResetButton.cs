﻿using System.Collections;
using System.Collections.Generic;
using Umods;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class ResetButton : UmodsBehaviour
{
    void UStart()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        UmodsApplication.GetMod<IProgrammingGridMod>().ClearGrid();
    }
}