﻿using System;
using UnityEngine;


[Serializable]
public partial class RobotAction
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private Sprite _icon;
    [SerializeField]
    private int _duration;
    [SerializeField]
    private Color _color;
    [SerializeField]
    private RobotActionCategory _category;

    public Color Color
    {
        get { return _color; }
        set { _color = value; }
    }

    public RobotActionCategory Category
    {
        get { return _category; }
        set { _category = value; }
    }

    public RobotAction(string name, Sprite icon, int duration)
    {
        _name = name;
        _icon = icon;
        _duration = duration;
    }

    public string Name
    {
        get { return _name; }
    }

    public Sprite Icon
    {
        get { return _icon; }
    }

    public int Duration
    {
        get { return _duration; }
    }
}
