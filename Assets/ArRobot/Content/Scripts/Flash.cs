﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flash : MonoBehaviour
{
    private Image _image;

    private void Start()
    {
        _image = GetComponent<Image>();
    }

    public void DoFlash()
    {
        StartCoroutine(FlashRoutine(0.2f));
    }

    IEnumerator FlashRoutine(float duration)
    {
        float timeStart = Time.time;
        while (Time.time - timeStart < duration*2)
        {
            if (Time.time - timeStart < duration)
                _image.color = new Color(_image.color.r, _image.color.g, _image.color.b,
                    Mathf.Lerp(0, 1, (Time.time - timeStart)/duration));
            else
                _image.color = new Color(_image.color.r, _image.color.g, _image.color.b,
                    Mathf.Lerp(1, 0, (Time.time - timeStart - duration)/duration));

            yield return null;
        }
        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b,
            0);
    }
}