﻿using System.Collections;
using System.Collections.Generic;
using Senso.Dialog;
using Umods;
using Umods.Scripts.InternalMods.SceneManager.Core;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScene : UmodsSceneBehaviour
{
    private ISensoDialogMod _sensoDialogMod;


    public List<Button> buttonsSynch;

    // Start is called before the first frame update
    void UStart()
    {
    
    Application.Quit();
        _sensoDialogMod = UmodsApplication.GetMod<ISensoDialogMod>();

        foreach (var b in buttonsSynch)
        {
           b.onClick.AddListener((() =>
           {
               if (!ConfigurationScript.BluetoothSynchronized)
               {
                   _sensoDialogMod.ShowDialog(SensoDialogEnums.SensoDialogModDialogType.Confirmation1,
                       "The robot is not synchronized",
                       "Do you want to synchronize it now?", "Synchronize",
                       () => { /*UmodsApplication.GetMod<IUIBluetooth>().StartBluetoothInterface(); */}, "Continue offline",
                       () => { LoadScene(b.gameObject.name); }, true, true);
               }
               else
               {
                   LoadScene(b.gameObject.name);
               }
           }));
        }
        
        //Try bluetooth

    }

    // Update is called once per frame
    void Update()
    {
    }
}