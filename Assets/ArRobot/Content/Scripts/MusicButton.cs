﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour
{
    public BarProgress _barProgress;
    public SquareScript _squares;
    
    public AudioClip audio;
    
    private bool _played = false;
    private GameObject _stop;
    private GameObject _text;
    private GameObject _locker;
    
    private AudioSource _audioSource;

    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
        _stop = transform.Find("Icon").gameObject;
        _text = transform.Find("Text").gameObject;
        _locker = transform.Find("Locker").gameObject;
        _audioSource = FindObjectOfType<AudioSource>();
    }

    public void OnClick()
    {
        if (!_played)
        {
            StartSong();
            Invoke("StopSong", 30);
            _played = true;
        }
        else
        {
            CancelInvoke();
            StopSong();
            _played = false;
        }
    }

    private void StartSong()
    {
        _barProgress.StartSong(audio.length);
        _squares.StartSong();
        _text.SetActive(false);
        _stop.SetActive(true);
        _locker.SetActive(true);
        transform.SetAsLastSibling();
        _audioSource.clip = audio;
        _audioSource.Play();
    }

    private void StopSong()
    {
        _barProgress.StopSong();
        _squares.StopSong();

        _text.SetActive(true);
        _stop.SetActive(false);
        _locker.SetActive(false);
        transform.SetAsFirstSibling();
        _audioSource.Stop();
    }
}