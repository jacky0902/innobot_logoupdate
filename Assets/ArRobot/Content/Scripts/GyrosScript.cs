﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyrosScript : MonoBehaviour
{

	public GameObject na, nb;
	public GameObject ea, eb;
	public GameObject sa, sb;
	public GameObject wa, wb;

	private float bLimit = 15; 
	private float aLimit = 35; 
	
	// Update is called once per frame
	void Update () {
		
		na.SetActive(false);
		nb.SetActive(false);
		sa.SetActive(false);
		sb.SetActive(false);
		ea.SetActive(false);
		eb.SetActive(false);
		wa.SetActive(false);
		wb.SetActive(false);
		
		var rotGyr = Input.gyro.attitude;
		Vector3 angles = rotGyr.eulerAngles;

		if (angles.x > bLimit)
		{
			nb.SetActive(true);
		}
		if (angles.x > aLimit)
		{
			na.SetActive(true);
		}
		
		if (angles.x < bLimit)
		{
			sb.SetActive(true);
		}
		if (angles.x < aLimit)
		{
			sa.SetActive(true);
		}
		
		if (angles.y > bLimit)
		{
			eb.SetActive(true);
		}
		if (angles.y > aLimit)
		{
			ea.SetActive(true);
		}
		
		if (angles.y < bLimit)
		{
			wb.SetActive(true);
		}
		if (angles.y < aLimit)
		{
			wa.SetActive(true);
		}
	}
}
