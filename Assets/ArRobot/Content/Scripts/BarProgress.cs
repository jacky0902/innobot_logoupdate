﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class BarProgress : MonoBehaviour
{

	private RectTransform _rect;
	private float _startTime;
	private Coroutine _coroutineToStop;
	private float _barTime;

	private bool _songStarted = false;

	private void Start()
	{
		_rect = GetComponent<RectTransform>();
	}

	public void StartSong(float time)
	{
		if(_songStarted) return;
		_rect.localScale = new Vector3(0,1,1);
		_startTime = Time.time;
		_barTime = time;
		_coroutineToStop = StartCoroutine(BarCoroutine());
		_songStarted = true;
	}

	IEnumerator BarCoroutine()
	{
		while (Time.time - _startTime < _barTime)
		{
			_rect.localScale = new Vector3((Time.time - _startTime) / _barTime,1,1);

			yield return null;
		}
		_songStarted = false;

	}
	
	public void StopSong()
	{
		if (!_songStarted) return;
		StopCoroutine(_coroutineToStop);
		_rect.localScale = new Vector3(0,1,1);
		_songStarted = false;
	}
}
