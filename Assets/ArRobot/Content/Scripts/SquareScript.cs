﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SquareScript : MonoBehaviour
{
    private GameObject[][] matrix;
    public GameObject ColorsCanvas;
    private Coroutine _coroutineToStop;

    private bool _songStarted = false;

    // Use this for initialization
    void Start()
    {
        matrix = new GameObject [5][];
        for (int x = 0; x < 5; x++)
        {
            matrix[x] = new GameObject[14];
        }

        int i = 0;
        int j = 0;
        foreach (Transform child in transform)
        {
            matrix[i][j] = child.gameObject;
            child.GetComponent<Image>().color =
                ColorsCanvas.transform.GetChild(Random.Range(0, ColorsCanvas.transform.childCount))
                    .GetComponent<Image>().color;
            child.gameObject.SetActive(false);
            
            j++;
            if (j >= 14)
            {
                j = 0;
                i++;
            }
        }
    }

    public void StartSong()
    {
        if (_songStarted) return;
        _coroutineToStop = StartCoroutine(SquaresCoroutine());
        _songStarted = true;
    }

    public void StopSong()
    {
        if (!_songStarted) return;

        StopCoroutine(_coroutineToStop);
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 14; j++)
            {
                matrix[i][j].SetActive(false);
            }
        }
        _songStarted = false;
    }


    IEnumerator SquaresCoroutine()
    {
        while (true)
        {
            for (int j = 0; j < 14; j++)
            {
                SetColumnHight(j, Random.Range(0, 6));
            }

            yield return new WaitForSeconds(0.3f);
        }
    }

    private void SetColumnHight(int column, int hight)
    {
        for (int i = 0; i < 5; i++)
        {
            matrix[i][column].SetActive(i < hight);
        }
    }
}