﻿using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.InternalMods.SceneManager.Core;
using UnityEngine;

public class PreloadScene : UmodsSceneBehaviour {

    private void UStart()
    {
        LoadRootScene();
    }
}