﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnSceneOpened : UnityEvent<UISceneData>
{
}

public class OnSceneClosed : UnityEvent
{
}

public class UISceneManager : MonoBehaviour
{
    public static UISceneManager instance;

    public UISceneBase homeScene;
    public Canvas mainLayer;

    public OnSceneOpened OnSceneOpened;
    public OnSceneClosed OnSceneClosed;

    private Stack<UISceneBase> sceneStack;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        sceneStack = new Stack<UISceneBase>();

        if (OnSceneOpened == null)
        {
            OnSceneOpened = new OnSceneOpened();
        }

        if (OnSceneClosed == null)
        {
            OnSceneClosed = new OnSceneClosed();
        }
    }

    private void Start()
    {
        OpenHomeScene(); // TODO: This should be called from outside (App manager?)
    }

    public void OpenHomeScene()
    {
        if (homeScene == null)
        {
            Debug.LogError("UISceneManager: invalid home scene. Cannot open the home scene.");
            return;
        }

        OpenScene(homeScene);
    }

    public void OpenScene(UISceneBase sceneToOpen, bool closeActiveScene = true)
    {
        if (mainLayer == null)
        {
            Debug.LogError("UISceneManager: invalid main layer canvas. Cannot open the scene.");
            return;
        }

        GameObject newSceneGameObject = Instantiate(sceneToOpen.gameObject) as GameObject;
        RectTransform newSceneRectTransform = newSceneGameObject.GetComponent<RectTransform>();
        if (newSceneRectTransform == null)
        {
            Debug.LogError(
                "UISceneManager: Trying to open a scene that does not have a valid RectTransform component. Aborting scene opening process.");
            return;
        }

        if (closeActiveScene)
            DisableCurrentScene();

        newSceneGameObject.transform.SetParent(mainLayer.gameObject.transform);
        SetSceneToFullScreen(newSceneRectTransform);

        sceneStack.Push(newSceneGameObject.GetComponent<UISceneBase>());
        OnSceneOpened.Invoke(sceneToOpen.sceneData);
    }

    public void SetSceneToFullScreen(RectTransform sceneRectTransform)
    {
        sceneRectTransform.pivot = new Vector2(0.5f, 0.5f);

        sceneRectTransform.anchorMin = Vector2.zero;
        sceneRectTransform.anchorMax = Vector2.one;

        sceneRectTransform.offsetMin = Vector2.zero;
        sceneRectTransform.offsetMax = Vector2.zero;

        sceneRectTransform.localScale = Vector2.one;
    }

    public void DisableCurrentScene()
    {
        if (sceneStack.Count == 0)
        {
            return;
        }

        UISceneBase currentScene = sceneStack.Peek();
        currentScene.gameObject.SetActive(false);
    }

    private void CloseCurrentScene()
    {
        if (sceneStack.Count == 0)
        {
            Debug.LogError("UISceneManager: There is no current active UI scene. Aborting scene closing process.");
            return;
        }

        UISceneBase currentScene = sceneStack.Pop();
        OnSceneClosed.Invoke();
        Destroy(currentScene.gameObject);
    }

    public void RestorePreviousScene()
    {
        CloseCurrentScene();

        if (sceneStack.Count == 0)
        {
            Debug.LogError("UISceneManager: There is no current active UI scene. Aborting scene restoring process.");
            return;
        }

        UISceneBase newCurrentScene = sceneStack.Peek();
        newCurrentScene.gameObject.SetActive(true);

        OnSceneOpened.Invoke(newCurrentScene.sceneData);
    }
}