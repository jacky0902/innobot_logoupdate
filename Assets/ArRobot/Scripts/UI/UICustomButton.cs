﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UICustomButton : MonoBehaviour
{
    private Button buttonComponent;

    void Awake()
    {
        buttonComponent = GetComponent<Button>();
        buttonComponent.onClick.AddListener(ExecuteAction);
    }

    public virtual void ExecuteAction() 
    { 
    }
}
