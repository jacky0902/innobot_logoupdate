﻿using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;

[System.Serializable]
public struct UISceneData 
{
    public string sceneTitle;
    public string sceneSubtitle;
    public SceneActionType leftAction;
    public SceneActionType rightAction;
    public UITopButtonActionContainer topButtonActionContainer;
    public bool hideBluetoothButton;
}

public abstract class UISceneBase : UITopButtonActionContainer
{
    public UISceneData sceneData;
}
