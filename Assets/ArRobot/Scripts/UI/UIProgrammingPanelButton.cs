﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProgrammingPanelButton : MonoBehaviour
{
    public RobotAction.MotionAction motionAction = RobotAction.MotionAction.None;
    public RobotAction.EmotionAction emotionAction = RobotAction.EmotionAction.None;
    public RobotAction.ArmsAction armsAction = RobotAction.ArmsAction.None;
    public RobotAction.SoundAction soundAction = RobotAction.SoundAction.None;
    
    public bool isTrash;

    [NonSerialized]
    public Button button;

    public UIProgrammingPanel.ActionType actionType;

    private void Awake()
    {
        button = GetComponent<Button>();
    }
}


