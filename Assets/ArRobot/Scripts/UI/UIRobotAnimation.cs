﻿using System;
using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;


public class UIRobotAnimation : UmodsBehaviour
{
    private Animator _animator;

    public float movementSpeed = 5;
    public float rotationSpeed = 5;

    public GameObject plane;
    public float modulePlane = 1.0f;


    public Material eyesTurnOnMaterial, eyesTurnOffMaterial;
    public Renderer LEU, REU, LEB, REB, LM, MM, RM;

    private Coroutine _movingCoroutine;
    private Coroutine _rotatingCoroutine;

    private bool _armsOpened = true;

    private void UStart()
    {
        _animator = FindObjectOfType<Animator>();
    }

    public void StopAnimation()
    {
        _animator.SetBool("walkRight", false);
        _animator.SetBool("walkLeft", false);
        _animator.SetBool("backRight", false);
        _animator.SetBool("backLeft", false);
        if (_movingCoroutine != null) StopCoroutine(_movingCoroutine);
        if (_rotatingCoroutine != null) StopCoroutine(_rotatingCoroutine);
        transform.rotation = Quaternion.identity;
        plane.transform.localPosition = Vector3.zero;
        
        SetAction(RobotAction.EmotionAction.None);
    }
    
    public void SetAction(RobotAction.MotionAction action)
    {
        _animator.SetBool("walkRight", false);
        _animator.SetBool("walkLeft", false);
        _animator.SetBool("backRight", false);
        _animator.SetBool("backLeft", false);
        if (_movingCoroutine != null) StopCoroutine(_movingCoroutine);
        if (_rotatingCoroutine != null) StopCoroutine(_rotatingCoroutine);
        switch (action)
        {
            case RobotAction.MotionAction.MoveForward:
                _animator.SetBool("walkRight", true);
                _animator.SetBool("walkLeft", true);
                _movingCoroutine = StartCoroutine(StartMoving(true));
                break;
            case RobotAction.MotionAction.MoveBackward:
                _animator.SetBool("backRight", true);
                _animator.SetBool("backLeft", true);
                _movingCoroutine = StartCoroutine(StartMoving(false));
                break;
            case RobotAction.MotionAction.TurnRight:
                _animator.SetBool("backRight", true);
                _animator.SetBool("walkLeft", true);
                _rotatingCoroutine = StartCoroutine(StartRotating(false));
                break;
            case RobotAction.MotionAction.TurnLeft:
                _animator.SetBool("walkRight", true);
                _animator.SetBool("backLeft", true);
                _rotatingCoroutine = StartCoroutine(StartRotating(true));
                break;
            case RobotAction.MotionAction.None:
                break;
        }
    }


    private IEnumerator StartMoving(bool forward)
    {
        while (true)
        {
            plane.transform.position = plane.transform.position -
                                       gameObject.transform.forward * movementSpeed * Time.deltaTime *
                                       (forward ? 1 : -1);
            
            var diference = gameObject.transform.position - plane.transform.position;
            var distance = new Vector3(Mathf.Abs(diference.x), Mathf.Abs(diference.y),Mathf.Abs(diference.z));
            if (distance.x > modulePlane / 2f)
            {
                plane.transform.position += Mathf.Sign(diference.x) * modulePlane * Vector3.right;
            }

            if (distance.z > modulePlane / 2f)
            {
                plane.transform.position += Mathf.Sign(diference.z) * modulePlane * Vector3.forward;
            }
            yield return null;
        }
    }

    private IEnumerator StartRotating(bool left)
    {
        while (true)
        {
            gameObject.transform.RotateAround(gameObject.transform.position, gameObject.transform.up,
                rotationSpeed * Time.deltaTime);
            yield return null;
        }
    }

    public void SetAction(RobotAction.EmotionAction action)
    {
        LEU.material = eyesTurnOffMaterial;
        REU.material = eyesTurnOffMaterial;
        LEB.material = eyesTurnOffMaterial;
        REB.material = eyesTurnOffMaterial;
        LM.material = eyesTurnOffMaterial;
        MM.material = eyesTurnOffMaterial;
        RM.material = eyesTurnOffMaterial;

        if (action == RobotAction.EmotionAction.None) return;

        foreach (var faceAction in ArRobotLogicMod.emotionToFaceActions[action])
        {
            switch (faceAction)
            {
                case RobotAction.FaceAction.UpperLeftEye:
                    LEU.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.UpperRightEye:
                    REU.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.LowerLeftEye:
                    LEB.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.LowerRightEye:
                    REB.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.LeftMouth:
                    LM.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.RightMouth:
                    RM.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.CenterMouth:
                    MM.material = eyesTurnOnMaterial;
                    break;
                case RobotAction.FaceAction.Stop:
                    break;
                case RobotAction.FaceAction.None:
                    break;
            }
        }
    }


    public void SetAction(RobotAction.ArmsAction action)
    {
        switch (action)
        {
            case RobotAction.ArmsAction.OpenArms:
                if (!_armsOpened)
                {
                    _animator.SetTrigger("open");
                    _armsOpened = true;
                }

                break;
            case RobotAction.ArmsAction.CloseArms:
                if (_armsOpened)
                {
                    _animator.SetTrigger("close");
                    _armsOpened = false;
                }

                break;
        }
    }
}