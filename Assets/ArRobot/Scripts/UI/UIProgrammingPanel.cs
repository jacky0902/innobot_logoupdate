﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProgrammingPanel : MonoBehaviour
{
    public enum ActionType
    {
        Motion,
        Emotion,
        Arms,
        Sound
    }

    public Action<int, UIProgrammingPanelButton> OnButtonPressed;

    public ActionType actionType;

    private UIProgrammingPanelButton[] _buttons;

    private int _column;
    
    private void Awake()
    {
        _buttons = transform.GetComponentsInChildren<UIProgrammingPanelButton>(true);
    }

    private void Start()
    {
        foreach (var button in _buttons)
        {
            //button.actionType = actionType;
            button.button.onClick.AddListener(() => _OnButtonPressed(button));
        }
    }

    public void ShowPanel(int column)
    {
        gameObject.SetActive(true);
        _column = column;
    }

    private void _OnButtonPressed(UIProgrammingPanelButton button)
    {
        OnButtonPressed(_column, button);
        gameObject.SetActive(false);
    }

    public GameObject GetButtonPrefab(RobotAction.MotionAction action)
    {
        foreach (var button in _buttons)
        {
            if (button.motionAction == action) return button.gameObject;
        }

        return null;
    }
    
    public GameObject GetButtonPrefab(RobotAction.EmotionAction action)
    {
        foreach (var button in _buttons)
        {
            if (button.emotionAction == action) return button.gameObject;
        }

        return null;
    }
    
    public GameObject GetButtonPrefab(RobotAction.ArmsAction action)
    {
        foreach (var button in _buttons)
        {
            if (button.armsAction == action) return button.gameObject;
        }

        return null;
    }
    
    public GameObject GetButtonPrefab(RobotAction.SoundAction action)
    {
        foreach (var button in _buttons)
        {
            if (button.soundAction == action) return button.gameObject;
        }

        return null;
    }
}
