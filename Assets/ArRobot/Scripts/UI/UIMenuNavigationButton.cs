﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class UIMenuNavigationButton : UICustomButton
{
    public UISceneBase sceneToNavigate;
    
    public override void ExecuteAction() 
    {
        UISceneManager.instance.OpenScene(sceneToNavigate);
    }
}
