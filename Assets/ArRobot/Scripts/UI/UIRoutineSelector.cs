﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Toggle = UnityEngine.UI.Toggle;

public class UIRoutineSelector : UmodsBehaviour
{
    public GameObject buttonPrefab;
    public Transform content;

    public Action<int> OnValueChanged;

    private Toggle[] _buttonList;
    private int _value;
    
    public void Populate(string[] names)
    {
        _buttonList = new Toggle[names.Length];

        for (int i = 0; i < names.Length; i++)
        {
            GameObject buttonGO = (GameObject) Instantiate(buttonPrefab, content);
            buttonGO.GetComponentInChildren<Text>().text = names[i];

            Toggle button = buttonGO.GetComponent<Toggle>();
            var index = i;
            button.onValueChanged.AddListener(((value) =>
            {
                if (value)
                    _OnButtonPressed(index);
            }));

            _buttonList[i] = button;
        }
    }

    public int GetChoice()
    {
        return _value;
    }

    private void _OnButtonPressed(int index)
    {
        foreach (var button in _buttonList)
        {
            button.SetIsOnWithoutNotify(false);
        }
        _buttonList[index].SetIsOnWithoutNotify(true);
        OnValueChanged(index);
        _value = index;
    }
}


