﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UISceneInitialiser : MonoBehaviour
{
    private void Start()
    {
        UISceneManager.instance.OnSceneOpened.AddListener(InitWithData);
        UISceneManager.instance.OnSceneClosed.AddListener(UnInit);
    }

    public virtual void InitWithData(UISceneData sceneData) 
    {
    }
    
    public virtual void UnInit() 
    {
    }
}
