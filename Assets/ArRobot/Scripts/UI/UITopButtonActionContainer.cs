﻿using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;

public abstract class UITopButtonActionContainer : UmodsBehaviour
{
    public abstract void UITopButtonAction(SceneActionType sceneActionType);
}
