﻿using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using UnityEngine;
using UnityEngine.UI;

public class DancingModeButtonScript : MonoBehaviour
{
    public Text text;
    public Image image;

    public AudioSource audioSource;
    public AudioClip audioClip;

    public DancingMode.DancingMusic dancingMusic;

    bool isPlaying;

    void Start()
    {
        isPlaying = false;
        ToState(false);
    }

    public void ToState(bool state)
    {
        isPlaying = state;

        if (isPlaying)
        {
            audioSource.clip = audioClip;
            audioSource.Play();
            image.gameObject.SetActive(true);
            text.gameObject.SetActive(false);
        }
        else
        {
            audioSource.Stop();
            image.gameObject.SetActive(false);
            text.gameObject.SetActive(true);
        }
    }
}
