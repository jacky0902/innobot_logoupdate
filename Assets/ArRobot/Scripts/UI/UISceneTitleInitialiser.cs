﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UISceneTitleInitialiser : UISceneInitialiser 
{
    public Text titleText;
    public Text subtitleText;
    public Image decorator;

    public override void InitWithData(UISceneData sceneData) 
    {
        titleText.text = sceneData.sceneTitle;
        subtitleText.text = sceneData.sceneSubtitle;

        bool shouldShowDecorator = titleText.text != "" || subtitleText.text != "";
        decorator.gameObject.SetActive(shouldShowDecorator);
    }
}
