﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SceneActionType
{
    None,
    Back,
    Camera,
    Home
};

[System.Serializable]
public class SceneActionVisualData
{
    public SceneActionType action;
    public Sprite texture;
    public Color tint;
}

public class UISceneActionBarInitialiser : UISceneInitialiser
{
    public UISceneActionButton leftActionButton;
    public UISceneActionButton rightActionButton;
    public Image decorator;
    public GameObject bluetooth;

    public SceneActionVisualData[] sceneActionsVisualConfiguration;

    public override void InitWithData(UISceneData sceneData)
    {
        SceneActionType leftAction = sceneData.leftAction;
        leftActionButton.SetSceneActionType(leftAction);

        SceneActionVisualData leftActionVisualData = GetVisualDataByType(leftAction);
        leftActionButton.InitialiseVisualData(leftActionVisualData,
            sceneData.topButtonActionContainer != null ? sceneData.topButtonActionContainer.UITopButtonAction : new Action<SceneActionType>((value) =>{}));

        SceneActionType rightAction = sceneData.rightAction;
        rightActionButton.SetSceneActionType(rightAction);

        SceneActionVisualData rightActionVisualData = GetVisualDataByType(rightAction);
        rightActionButton.InitialiseVisualData(rightActionVisualData,
            sceneData.topButtonActionContainer != null ? sceneData.topButtonActionContainer.UITopButtonAction : new Action<SceneActionType>((value) =>{}));

        bool shouldShowDecorator = leftAction != SceneActionType.None || rightAction != SceneActionType.None;
        decorator.gameObject.SetActive(shouldShowDecorator);
        
        bluetooth.gameObject.transform.SetSiblingIndex(0);

        if(leftAction == SceneActionType.None)
            bluetooth.gameObject.transform.SetSiblingIndex(1);
        if(rightAction == SceneActionType.None)
            bluetooth.gameObject.transform.SetSiblingIndex(3);
    }

    public override void UnInit()
    {
        leftActionButton.DeinitialiseVisualData();
        rightActionButton.DeinitialiseVisualData();
    }

    private SceneActionVisualData GetVisualDataByType(SceneActionType actionType)
    {
        return Array.Find(sceneActionsVisualConfiguration,
            element => element.action == actionType);
    }
}