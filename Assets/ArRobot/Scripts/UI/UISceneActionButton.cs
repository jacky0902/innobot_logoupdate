﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UISceneActionButton : UICustomButton
{
    private SceneActionType sceneAction = SceneActionType.None;

    private Stack<Action<SceneActionType>> _callbacksStack  = new Stack<Action<SceneActionType>>();

    public void SetSceneActionType(SceneActionType inType)
    {
        sceneAction = inType;
    }

    public void InitialiseVisualData(SceneActionVisualData sceneActionData, Action<SceneActionType> action) 
    {
        bool isActive = sceneActionData != null;
        gameObject.SetActive(isActive);

        Image imageComponent = GetComponent<Image>();
        if (imageComponent == null)
        {
            Debug.LogError("InitSceneAction: The button has no image component.");
            return;
        }

        if (sceneActionData != null)
        {
            imageComponent.sprite = sceneActionData.texture;
            imageComponent.color = sceneActionData.tint;
        }
        
        _callbacksStack.Push(action);
    }
    
    public void DeinitialiseVisualData()
    {
        _callbacksStack.Pop();
    }
    
    

    public override void ExecuteAction()
    {
        _callbacksStack.Peek()(sceneAction);

        switch (sceneAction) 
        {
            case SceneActionType.Back:
                UISceneManager.instance.RestorePreviousScene();
                break;
            case SceneActionType.None:
            default:
                Debug.LogWarning("No action has been set.");
                break;
        }
    }
}
