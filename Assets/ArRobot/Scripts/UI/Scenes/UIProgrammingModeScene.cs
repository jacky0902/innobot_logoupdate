﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIProgrammingModeScene : UISceneBase, ILogicRoutinesStorageHandler, ILogicActionsPlayerHandler,
    ILogicBluetoothConnectivityHandler
{
    public UISceneBase syncSetupScene;
    public UISceneBase simulationScene;
    public UISceneBase photoScene;

    public Button resetButton, saveAndLoadButton, testButton, powerButton;

    public Sprite stopSprite;
    public Sprite powerSprite;

    // Save and load panel
    public GameObject saveAndLoadPanel;
    public Button saveButtonSALP;
    public Button loadButtonSALP;
    public Button crossButtonSALP;

    // Save panel
    public GameObject savePanel;
    public Button saveButtonSP;
    public Button crossButtonSP;
    public InputField inputFieldSP;

    // Load panel
    public GameObject loadPanel;
    public Button loadButtonLP;
    public Button crossButtonLP;
    public UIRoutineSelector routineSelector;

    // Success and Error Panel
    public GameObject successPanel;
    public Button successButton;
    public Text successBoldText;
    public Text successRegularText;
    public GameObject errorPanel;
    public Button errorButton;
    public Text errorBoldText;
    public Text errorRegularText;


    private SavedRoutine[] _savedRoutines;


    private IArRobotLogicBluetoothConnectivityMod _arRobotLogicBluetoothConnectivityMod;
    private IProgrammingGridMod _programmingGridMod;
    private IArRobotLogicMod _arRobotLogicMod;

    // Grid Panel
    public GameObject gridContainer;
    public ScrollRect scrollViewRect;
    public UIProgrammingPanel[] panels;
    public Button leftArrow;
    public Button rightArrow;

    public AudioSource audioSource;
    public AudioClip[] audioClips;

    // Start is called before the first frame update
    void UAwake()
    {
        _arRobotLogicBluetoothConnectivityMod = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();

        if (_arRobotLogicBluetoothConnectivityMod.GetConnectedBluetoothDevice() == null)
        {
            UISceneManager.instance.OpenScene(syncSetupScene, false);
            powerButton.interactable = false;
        }
    }

    void UStart()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        _programmingGridMod = UmodsApplication.GetMod<IProgrammingGridMod>();
        _programmingGridMod.InitializeGrid(gridContainer, scrollViewRect, panels, leftArrow, rightArrow);
        _programmingGridMod.CreateGrid();


        resetButton.onClick.AddListener((() => _programmingGridMod.ClearGrid()));
        saveAndLoadButton.onClick.AddListener(() => saveAndLoadPanel.SetActive(true));
        testButton.onClick.AddListener((() => _TestButtonPressed()));
        powerButton.onClick.AddListener((() => _PowerButtonPressed()));

        saveAndLoadPanel.SetActive(false);
        savePanel.SetActive(false);
        loadPanel.SetActive(false);
        errorPanel.SetActive(false);
        successPanel.SetActive(false);

        // SAVE AND LOAD PANEL
        saveButtonSALP.GetComponent<Button>().onClick.AddListener((() =>
        {
            saveAndLoadPanel.SetActive(false);
            savePanel.SetActive(true);
            inputFieldSP.text = "";
        }));
        loadButtonSALP.GetComponent<Button>().onClick.AddListener((() =>
        {
            saveAndLoadPanel.SetActive(false);
            loadPanel.SetActive(true);

            _savedRoutines = _arRobotLogicMod.GetSavedRoutinesList();

            routineSelector.Populate(_savedRoutines.Select(routine => routine.Name).ToArray());
        }));

        crossButtonSALP.GetComponent<Button>().onClick
            .AddListener((() => saveAndLoadPanel.SetActive(false)));

        // SAVE PANEL
        saveButtonSP.interactable = false;
        inputFieldSP.onValueChanged.AddListener((value) => saveButtonSP.interactable = !value.Equals(""));

        saveButtonSP.onClick.AddListener((() =>
        {
            RobotAction.MotionAction[] motionActions;
            RobotAction.EmotionAction[] emotionActions;
            RobotAction.ArmsAction[] armsActions;
            RobotAction.SoundAction[] soundActions;

            _programmingGridMod.GetGridActions(out motionActions, out emotionActions, out armsActions,
                out soundActions);
            _arRobotLogicMod.SaveRoutineAsync(inputFieldSP.text, motionActions, emotionActions, armsActions,
                soundActions);

            savePanel.SetActive(false);
        }));

        crossButtonSP.GetComponent<Button>().onClick
            .AddListener((() => savePanel.SetActive(false)));


        // LOAD PANEL
        loadButtonLP.interactable = false;
        routineSelector.OnValueChanged += value => loadButtonLP.interactable = true;

        loadButtonLP.onClick.AddListener(() =>
            _arRobotLogicMod.LoadRoutineAsync(_savedRoutines[routineSelector.GetChoice()].Id));

        crossButtonLP.GetComponent<Button>().onClick
            .AddListener((() => loadPanel.SetActive(false)));
    }

    private void _TestButtonPressed()
    {
        UISceneManager.instance.OpenScene(simulationScene);
    }

    private void _PowerButtonPressed()
    {
        RobotAction.MotionAction[] motionActions;
        RobotAction.EmotionAction[] emotionActions;
        RobotAction.ArmsAction[] armsActions;
        RobotAction.SoundAction[] soundActions;

        _programmingGridMod.GetGridActions(out motionActions, out emotionActions, out armsActions,
            out soundActions);
        _programmingGridMod.SetEditable(false);

        _arRobotLogicMod.PlayRoutine(motionActions, emotionActions, armsActions,
            soundActions, true);

        _SetRestButtonPlaying(true);
    }
    

    private void _SetRestButtonPlaying(bool value)
    {
        resetButton.interactable = !value;
        saveAndLoadButton.interactable = !value;
        testButton.interactable = !value;
        if (value)
        {
            powerButton.transform.Find("Text").GetComponent<Text>().text = "STOP";
            powerButton.transform.Find("Icon").GetComponent<Image>().sprite = stopSprite;
            powerButton.transform.Find("Icon").GetComponent<Image>().preserveAspect = true;
            powerButton.onClick.RemoveAllListeners();
            powerButton.onClick.AddListener((StopPlaying));
        }
        else
        {
            powerButton.transform.Find("Text").GetComponent<Text>().text = "POWER";
            powerButton.transform.Find("Icon").GetComponent<Image>().sprite = powerSprite;
            powerButton.onClick.RemoveAllListeners();
            powerButton.onClick.AddListener((_PowerButtonPressed));
        }
    }

    private void StopPlaying()
    {
        _arRobotLogicMod.StopRoutine();
        _SetRestButtonPlaying(false);
        _programmingGridMod.SetColumnActivation(_lastStepActivated, false);
        _programmingGridMod.SetEditable(true);
        StopSound();
    }


    public void OnRoutineLoaded(bool success, SavedRoutine loadedRoutine, RobotAction.MotionAction[] motionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.EmotionAction[] emotionActions, RobotAction.SoundAction[] soundActions)
    {
        if (success)
        {
            _programmingGridMod.FillGrid(motionActions, emotionActions, armsActions, soundActions);

            // Panel staff
            loadPanel.SetActive(false);
            successPanel.SetActive(true);
            successRegularText.text = "Routine";
            successBoldText.text = "Loaded successfully";
            successButton.onClick.RemoveAllListeners();
            successButton.onClick.AddListener(() => successPanel.SetActive(false));
        }
        else
        {
            loadPanel.SetActive(false);
            errorPanel.SetActive(true);
            errorRegularText.text = "Routine";
            errorBoldText.text = "Not loaded";
            errorButton.onClick.RemoveAllListeners();
            errorButton.onClick.AddListener(() =>
            {
                errorPanel.SetActive(false);
                saveAndLoadPanel.SetActive(true);
            });
        }
    }

    public void OnRoutineSaved(bool success, SavedRoutine savedRoutine)
    {
        if (success)
        {
            // Panel staff
            successPanel.SetActive(true);
            successRegularText.text = "Routine";
            successBoldText.text = "Saved successfully";
            successButton.onClick.RemoveAllListeners();
            successButton.onClick.AddListener(() => successPanel.SetActive(false));
        }
        else
        {
            errorPanel.SetActive(true);
            errorRegularText.text = "Routine";
            errorBoldText.text = "Not saved";
            errorButton.onClick.RemoveAllListeners();
            errorButton.onClick.AddListener(() =>
            {
                errorPanel.SetActive(false);
                saveAndLoadPanel.SetActive(true);
            });
        }
    }

    private int _lastStepActivated;

    public void OnNewStepPlayed(int stepCounter, RobotAction.MotionAction motionAction,
        RobotAction.ArmsAction armsAction, RobotAction.EmotionAction emotionAction,
        RobotAction.SoundAction soundAction)
    {
        if (!gameObject.activeInHierarchy) return;
        if (motionAction == RobotAction.MotionAction.Stop)
        {
            StopPlaying();
            return;
        }

        if (stepCounter != 0)
            _programmingGridMod.SetColumnActivation(stepCounter - 1, false);
        _programmingGridMod.SetColumnActivation(stepCounter, true);
        _lastStepActivated = stepCounter;
        PlaySound(soundAction);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        switch (sceneActionType)
        {
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopRoutine();
                _SetRestButtonPlaying(false);
                if (_programmingGridMod == null) _programmingGridMod = UmodsApplication.GetMod<IProgrammingGridMod>();
                _programmingGridMod.SetColumnActivation(_lastStepActivated, false);
                StopSound();
                break;
            case SceneActionType.Camera:
                UISceneManager.instance.OpenScene(photoScene);
                StopSound();
                break;
            default:
                break;
        }
    }

    private void PlaySound(RobotAction.SoundAction soundAction)
    {
        if (soundAction == RobotAction.SoundAction.None || soundAction == RobotAction.SoundAction.Stop)
        {
            audioSource.Stop();
            return;
        }

        audioSource.Stop();
        audioSource.clip = audioClips[(int) soundAction];
        audioSource.Play();
    }

    private void StopSound()
    {
        audioSource.Stop();
    }

    public void OnBluetoothDeviceListUpdated(List<LogicBluetoothDevice> bluetoothDevices)
    {
    }

    public void OnBluetoothConnected(LogicBluetoothDevice logicBluetoothDevice,
        ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        if (state == ArRobotLogicConnectivityVariables.ConnectionState.Connected)
            powerButton.interactable = true;
    }

    public void OnBluetoothDisconnected(LogicBluetoothDevice logicBluetoothDevice,
        ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        powerButton.interactable = false;
    }
}