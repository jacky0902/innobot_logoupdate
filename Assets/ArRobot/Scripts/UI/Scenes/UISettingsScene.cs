﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UISettingsScene : UISceneBase
{
    public Button synchronizeButton;
    public UISceneBase syncSetupScene;

    public Button aboutAppButton;
    public UISceneBase aboutAppScene;

    void UStart()
    {
        synchronizeButton.onClick.AddListener(_OnSynchornizeClicked);
        aboutAppButton.onClick.AddListener(_OnAboutAppClicked);
    }

    private void _OnSynchornizeClicked()
    {
        UISceneManager.instance.OpenScene(syncSetupScene);
    }

    private void _OnAboutAppClicked()
    {
        UISceneManager.instance.OpenScene(aboutAppScene);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        
    }
}
