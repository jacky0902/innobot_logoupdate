﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRealTimeModeScene : UISceneBase
{
    public Button topFaceArrow, bottomFaceArrow;
    public GameObject[] facePanels;
    public Button[] faceButtons;

    public Button topSoundArrow, bottomSoundArrow;
    public GameObject[] soundPanels;
    public Button[] soundButtons;

    // Buttons
    public Button upArrow, rightArrow, bottomArrow, leftArrow;
    public Button openArms, closeArms;

    public UISceneBase photoScene;

    private int facePanelIndex;
    private int soundPanelIndex;

    private IArRobotLogicMod _arRobotLogicMod;

    void UStart()
    {
        // Modules
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();

        facePanels[0].SetActive(true);
        facePanels[1].SetActive(false);
        facePanels[2].SetActive(false);

        soundPanels[0].SetActive(true);
        soundPanels[1].SetActive(false);

        topFaceArrow.onClick.AddListener((() =>
        {
            facePanels[facePanelIndex].SetActive(false);
            if (--facePanelIndex < 0) facePanelIndex = 2;
            facePanels[facePanelIndex].SetActive(true);
        }));

        bottomFaceArrow.onClick.AddListener((() =>
        {
            facePanels[facePanelIndex].SetActive(false);
            if (++facePanelIndex > 2) facePanelIndex = 0;
            facePanels[facePanelIndex].SetActive(true);
        }));

        topSoundArrow.onClick.AddListener((() =>
        {
            soundPanels[soundPanelIndex].SetActive(false);
            if (--soundPanelIndex < 0) soundPanelIndex = 1;
            soundPanels[soundPanelIndex].SetActive(true);
        }));

        bottomSoundArrow.onClick.AddListener((() =>
        {
            soundPanels[soundPanelIndex].SetActive(false);
            if (++soundPanelIndex > 1) soundPanelIndex = 0;
            soundPanels[soundPanelIndex].SetActive(true);
        }));

        // Buttons
        AddActionToButton(upArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.MoveForward));
        AddActionToButton(bottomArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.MoveBackward));
        AddActionToButton(rightArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.TurnRight));
        AddActionToButton(leftArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.TurnLeft));

        AddActionToButton(upArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.MoveForward));
        AddActionToButton(bottomArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.MoveBackward));
        AddActionToButton(rightArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.TurnRight));
        AddActionToButton(leftArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.TurnLeft));
        
        openArms.onClick.AddListener(() => _arRobotLogicMod.SendRealtimeAction(RobotAction.ArmsAction.OpenArms));
        closeArms.onClick.AddListener(() => _arRobotLogicMod.SendRealtimeAction(RobotAction.ArmsAction.CloseArms));

        for (int i = 0; i < faceButtons.Length; i++)
        {
            var i1 = i;
            faceButtons[i].onClick
                .AddListener(() => _arRobotLogicMod.SendRealtimeAction((RobotAction.EmotionAction) (int.Parse(faceButtons[i1].name)+1)));
        }
        
        for (int i = 0; i < soundButtons.Length; i++)
        {
            var i2 = i;
            soundButtons[i].onClick
                .AddListener(() =>
                {
                    var i1 = i2;
                    PlaySound(i1);
                    if (i1 >= 4) i1 -= 2;
                    _arRobotLogicMod.SendRealtimeAction((RobotAction.SoundAction) i1);
                });
        }
    }

    private void AddActionToButton(Button button, EventTriggerType type, Action action)
    {
        EventTrigger trigger = button.gameObject.AddComponent<EventTrigger>();
        var eventToTrigger = new EventTrigger.Entry();
        eventToTrigger.eventID = type;
        eventToTrigger.callback.AddListener((e) => action());
        trigger.triggers.Add(eventToTrigger);
    }


    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        switch (sceneActionType)
        {
            case SceneActionType.Camera:
                UISceneManager.instance.OpenScene(photoScene);
                StopSound();
                break;
            case SceneActionType.Back:
                if(_arRobotLogicMod == null )  _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopSendingRealtimeAction();
                StopSound();
                break;
        }
    }
    
    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public float timePerAudio = 1;

    private Coroutine _soundCoroutine = null;
    private void PlaySound(int soundAction)
    {
        if (_soundCoroutine != null) StopCoroutine(_soundCoroutine);
        
        audioSource.Stop();
        audioSource.clip = audioClips[soundAction];
        audioSource.Play();
        _soundCoroutine = StartCoroutine(SoundCoroutine());
    }

    private IEnumerator SoundCoroutine()
    {
        float initialTime = Time.time;
        while (Time.time - initialTime < timePerAudio)
            yield return null;
        
        StopSound();
    }

    private void StopSound()
    {
        audioSource.Stop();
    }
}