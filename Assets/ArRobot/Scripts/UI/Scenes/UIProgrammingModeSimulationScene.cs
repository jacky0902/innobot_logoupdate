﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class UIProgrammingModeSimulationScene : UISceneBase, ILogicActionsPlayerHandler
{
    public Button powerButton;


    private UIRobotAnimation _robotAnimation;

    private IProgrammingGridMod _programmingGridMod;
    private IArRobotLogicMod _arRobotLogicMod;

    private Camera _robotCamera;


    // Grid Panel
    public GameObject gridContainer;

    public Sprite stopSprite;
    public Sprite powerSprite;

    public RawImage robotViewer;

    public AudioSource audioSource;
    public AudioClip[] audioClips;

    // Start is called before the first frame update

    void UStart()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        _programmingGridMod = UmodsApplication.GetMod<IProgrammingGridMod>();
        _programmingGridMod.InitializeColumn(gridContainer);
        _programmingGridMod.CreateColumn();

        powerButton.onClick.AddListener((() => _PowerButtonPressed()));

        _robotAnimation = FindObjectOfType<UIRobotAnimation>();

        Rect rect = robotViewer.gameObject.GetComponent<RectTransform>().rect;
        RenderTexture tex = new RenderTexture((int) rect.width, (int) rect.height, 24);
        _robotCamera = GameObject.Find("RobotCamera").GetComponent<Camera>();
        _robotCamera.targetTexture = tex;
        robotViewer.texture = tex;
    }

    private void _PowerButtonPressed()
    {
        RobotAction.MotionAction[] motionActions;
        RobotAction.EmotionAction[] emotionActions;
        RobotAction.ArmsAction[] armsActions;
        RobotAction.SoundAction[] soundActions;

        _programmingGridMod.GetGridActions(out motionActions, out emotionActions, out armsActions,
            out soundActions);

        _arRobotLogicMod.PlayRoutine(motionActions, emotionActions, armsActions,
            soundActions, false);

        _SetRestButtonPlaying(true);
    }

    private void StopPlaying()
    {
        _arRobotLogicMod.StopRoutine();
        _robotAnimation.StopAnimation();
        _SetRestButtonPlaying(false);
        StopSound();
        _programmingGridMod.FillColumn(-1);

    }

    private void _SetRestButtonPlaying(bool value)
    {
        if (value)
        {
            powerButton.transform.Find("Text").GetComponent<Text>().text = "STOP";
            powerButton.transform.Find("Icon").GetComponent<Image>().sprite = stopSprite;
            powerButton.transform.Find("Icon").GetComponent<Image>().preserveAspect = true;
            powerButton.onClick.RemoveAllListeners();
            powerButton.onClick.AddListener((StopPlaying));
        }
        else
        {
            powerButton.transform.Find("Text").GetComponent<Text>().text = "POWER";
            powerButton.transform.Find("Icon").GetComponent<Image>().sprite = powerSprite;
            powerButton.onClick.RemoveAllListeners();
            powerButton.onClick.AddListener((_PowerButtonPressed));
        }
    }

    public void OnNewStepPlayed(int stepCounter, RobotAction.MotionAction motionAction,
        RobotAction.ArmsAction armsAction, RobotAction.EmotionAction emotionAction,
        RobotAction.SoundAction soundAction)
    {
        if (motionAction == RobotAction.MotionAction.Stop)
        {
            StopPlaying();
            return;
        }

        _programmingGridMod.FillColumn(stepCounter);
        _programmingGridMod.SetColumnActivation(stepCounter, false);
        _robotAnimation.SetAction(armsAction);
        _robotAnimation.SetAction(motionAction);
        _robotAnimation.SetAction(emotionAction);
        
        PlaySound(soundAction);
    }


    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        switch (sceneActionType)
        {
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopRoutine();
                if (_robotAnimation == null) _robotAnimation = FindObjectOfType<UIRobotAnimation>();
                _robotAnimation.StopAnimation();
                _SetRestButtonPlaying(false);
                StopSound();
                break;
            default:
                break;
        }
    }

    private void PlaySound(RobotAction.SoundAction soundAction)
    {
        if (soundAction == RobotAction.SoundAction.None || soundAction == RobotAction.SoundAction.Stop)
        {
            audioSource.Stop();
            return;
        }
            
        audioSource.Stop();
        audioSource.clip = audioClips[(int) soundAction];
        audioSource.Play();
    }

    private void StopSound()
    {
        audioSource.Stop();
    }
}