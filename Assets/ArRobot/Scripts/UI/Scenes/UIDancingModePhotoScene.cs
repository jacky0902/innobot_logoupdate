﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.ExternalMods.Media.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class UIDancingModePhotoScene : UISceneBase
{
    public Button photoButton;
    public Button videoButton;
    
    private IArRobotLogicMod _arRobotLogicMod;

    public RawImage cameraRawImage;
    public AspectRatioFitter cameraAspectRatioFitter;
    
    private Coroutine _onEncSongCoroutine = null;


    // Start is called before the first frame update

    void UAwake()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
    }
    
    void UStart()
    {

        photoButton.onClick.AddListener((() => _PhotoButtonPressed()));
        videoButton.onClick.AddListener((() => _VideoButtonPressed()));
        
        _arRobotLogicMod.ShowCameraOnTexture(cameraRawImage, cameraAspectRatioFitter);
        
        //Dancing
        foreach (var dancingModesButton in dancingModesButtons)
        {
            var dancingModeButtonAux = dancingModesButton;
            dancingModesButton.gameObject.GetComponent<Button>().onClick
                .AddListener(() => OnButtonPressed(dancingModeButtonAux));
        }
    }

   
    
    private void _PhotoButtonPressed()
    {
        _arRobotLogicMod.TakePhotoAsync();
    }

    private bool videoRecording = false;
    private void _VideoButtonPressed()
    {
        if (!videoRecording)
        {
            _arRobotLogicMod.StartMakingVideo();
            videoButton.transform.Find("Text").GetComponent<Text>().text = "STOP";
        }
        else
        {
            _arRobotLogicMod.StopAndSaveVideoAsync();
            videoButton.transform.Find("Text").GetComponent<Text>().text = "VIDEO";
        }

        videoRecording = !videoRecording;
    }

  
    public DancingModeButtonScript[] dancingModesButtons;

    public BarProgress barProgress;

    private DancingModeButtonScript _currentButton = null;


    void OnButtonPressed(DancingModeButtonScript newButton)
    {
        if (_currentButton != null)
        {
            StopDancing();
        }

        if (_currentButton == newButton)
        {
            _currentButton = null;
        }

        else
        {
            newButton.ToState((true));
            _currentButton = newButton;
            barProgress.StartSong(_currentButton.audioClip.length);
            _arRobotLogicMod.StartDancing(_currentButton.dancingMusic);
            _onEncSongCoroutine = StartCoroutine(OnEndSongCoroutine(_currentButton.audioClip.length));

        }

    }
    
    void StopDancing()
    {
        if(_currentButton != null)
            _currentButton.ToState(false);
        barProgress.StopSong();
        if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        _arRobotLogicMod.StopDancing();
        if (_onEncSongCoroutine != null) StopCoroutine(_onEncSongCoroutine);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        switch (sceneActionType)
        {
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopShowingCameraOnTexture();
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopDancing();
                if (_onEncSongCoroutine != null) StopCoroutine(_onEncSongCoroutine);
                StopDancing();
                break;
            default:
                break;
        }
    }
    
    private IEnumerator OnEndSongCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        barProgress.StopSong();
        if (_currentButton != null) OnButtonPressed(_currentButton);
    }
}