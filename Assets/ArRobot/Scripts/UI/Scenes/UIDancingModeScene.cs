﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIDancingModeScene : UISceneBase
{
    public DancingModeButtonScript[] dancingModesButtons;

    public SquareScript squareScript;
    public BarProgress barProgress;

    public UIDancingModePhotoScene uiDancingModePhotoScene;

    private DancingModeButtonScript _currentButton = null;

    private Coroutine _onEncSongCoroutine = null;

    private IArRobotLogicMod _arRobotLogicMod;

    void UStart()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();

        foreach (var dancingModesButton in dancingModesButtons)
        {
            var dancingModeButtonAux = dancingModesButton;
            dancingModesButton.gameObject.GetComponent<Button>().onClick
                .AddListener(() => OnButtonPressed(dancingModeButtonAux));
        }
    }

    void OnButtonPressed(DancingModeButtonScript newButton)
    {
        if (_currentButton != null)
        {
            StopDancing();
        }

        if (_currentButton == newButton)
        {
            _currentButton = null;
        }

        else
        {
            newButton.ToState((true));
            _currentButton = newButton;
            squareScript.StartSong();
            barProgress.StartSong(_currentButton.audioClip.length);
            _arRobotLogicMod.StartDancing(_currentButton.dancingMusic);
            _onEncSongCoroutine = StartCoroutine(OnEndSongCoroutine(_currentButton.audioClip.length));

        }
    }

    private void OnEnable()
    {
        StopDancing();
    }

    void StopDancing()
    {
        if (_currentButton != null) _currentButton.ToState(false);
        squareScript.StopSong();
        barProgress.StopSong();
        if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        _arRobotLogicMod.StopDancing();
        if (_onEncSongCoroutine != null) StopCoroutine(_onEncSongCoroutine);

    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();

        switch (sceneActionType)
        {
            case SceneActionType.Camera:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopDancing();
                if (_onEncSongCoroutine != null) StopCoroutine(_onEncSongCoroutine);
                StopDancing();
                UISceneManager.instance.OpenScene(uiDancingModePhotoScene);
                break;
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopDancing();
                if (_onEncSongCoroutine != null) StopCoroutine(_onEncSongCoroutine);
                StopDancing();
                break;
        }
    }

    private IEnumerator OnEndSongCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        squareScript.StopSong();
        barProgress.StopSong();
        if (_currentButton != null) OnButtonPressed(_currentButton);
    }
}