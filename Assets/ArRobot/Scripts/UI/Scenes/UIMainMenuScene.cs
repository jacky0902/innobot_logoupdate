﻿using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public class UIMainMenuScene : UISceneBase
{
    public Button programmingModeButton;
    public UISceneBase programmingModeScene;

    public Button settingsButton;
    public UISceneBase settingsScene;

    public Button dancingModeButton;
    public UISceneBase dancingModeScene;

    public Button realtimeModeButton;
    public UISceneBase realtimeModeScene;

    public Button gyrosModeButton;
    public UISceneBase gyrosModeScene;

    private IArRobotLogicBluetoothConnectivityMod _arRobotLogicBluetoothConnectivityMod;

    // Start is called before the first frame update
    void UStart()
    {
#if PLATFORM_ANDROID
        AskAndroidPerssions();
#endif

        _arRobotLogicBluetoothConnectivityMod = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();
        programmingModeButton.onClick.AddListener(_OnProgrammingModeClicked);
        settingsButton.onClick.AddListener(_OnSettingsClicked);
        dancingModeButton.onClick.AddListener(_OnDancingModeClicked);

        realtimeModeButton.onClick.AddListener((() => UISceneManager.instance.OpenScene(realtimeModeScene)));
        gyrosModeButton.onClick.AddListener((() => UISceneManager.instance.OpenScene(gyrosModeScene)));
    }

    private string[] PERMISSIONS = new string[]
    {
        "android.permission.READ_EXTERNAL_STORAGE",
        "android.permission.WRITE_EXTERNAL_STORAGE",
        "android.permission.CAMERA",
        "android.permission.ACCESS_COARSE_LOCATION",
        "android.permission.BLUETOOTH_ADMIN",
        "android.permission.BLUETOOTH",
    };

    private int _permissionCounter = 0;

    private void AskAndroidPerssions()
    {
        if (Application.isEditor) return;
        if (_permissionCounter < PERMISSIONS.Length)
            if (!AndroidPermissionsManager.IsPermissionGranted(PERMISSIONS[_permissionCounter]))
            {
                AndroidPermissionsManager.RequestPermission(new[] {PERMISSIONS[_permissionCounter]},
                    new AndroidPermissionCallback(
                        grantedPermission =>
                        {
                            _permissionCounter++;
                            AskAndroidPerssions();
                        },
                        deniedPermission => { AskAndroidPerssions(); },
                        deniedPermissionAndDontAskAgain => { AskAndroidPerssions(); }));
            }
            else
            {
                _permissionCounter++;
                AskAndroidPerssions();
            }

        Debug.Log("Permissions asked");
    }

    private void _OnProgrammingModeClicked()
    {
        UISceneManager.instance.OpenScene(programmingModeScene);
    }

    private void _OnSettingsClicked()
    {
        UISceneManager.instance.OpenScene(settingsScene);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
    }

    private void _OnDancingModeClicked()
    {
        UISceneManager.instance.OpenScene(dancingModeScene);
    }
}