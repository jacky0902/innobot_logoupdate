﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.ExternalMods.Media.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRealTimeModePhotoScene : UISceneBase
{
    public Button photoButton;
    public Button videoButton;

    private IArRobotLogicMod _arRobotLogicMod;

    public Sprite stopSprite;
    public Sprite powerSprite;

    public RawImage cameraRawImage;
    public AspectRatioFitter cameraAspectRatioFitter;

    public Button topFaceArrow, bottomFaceArrow;
    public Button[] faceButtons;

    public Button topSoundArrow, bottomSoundArrow;
    public Button[] soundButtons;

    // Buttons
    public Button upArrow, rightArrow, bottomArrow, leftArrow;
    public Button openArms, closeArms;


    private int _facePanelIndex;
    private int _soundPanelIndex;


    // Start is called before the first frame update

    void UAwake()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
    }

    void UStart()
    {
        _arRobotLogicMod.ShowCameraOnTexture(cameraRawImage, cameraAspectRatioFitter);

        photoButton.onClick.AddListener((() => _PhotoButtonPressed()));
        videoButton.onClick.AddListener((() => _VideoButtonPressed()));

        //Buttons
        foreach (var faceButton in faceButtons)
        {
            faceButton.gameObject.SetActive(false);
        }

        faceButtons[0].gameObject.SetActive(true);

        foreach (var soundButton in soundButtons)
        {
            soundButton.gameObject.SetActive(false);
        }

        soundButtons[0].gameObject.SetActive(true);

        topFaceArrow.onClick.AddListener((() =>
        {
            faceButtons[_facePanelIndex].gameObject.SetActive(false);
            if (--_facePanelIndex < 0) _facePanelIndex = faceButtons.Length - 1;
            faceButtons[_facePanelIndex].gameObject.SetActive(true);
        }));

        bottomFaceArrow.onClick.AddListener((() =>
        {
            faceButtons[_facePanelIndex].gameObject.SetActive(false);
            if (++_facePanelIndex >= faceButtons.Length) _facePanelIndex = 0;
            faceButtons[_facePanelIndex].gameObject.SetActive(true);
        }));

        topSoundArrow.onClick.AddListener((() =>
        {
            soundButtons[_soundPanelIndex].gameObject.SetActive(false);
            if (--_soundPanelIndex < 0) _soundPanelIndex = soundButtons.Length - 1;
            soundButtons[_soundPanelIndex].gameObject.SetActive(true);
        }));

        bottomSoundArrow.onClick.AddListener((() =>
        {
            soundButtons[_soundPanelIndex].gameObject.SetActive(false);
            if (++_soundPanelIndex >= soundButtons.Length) _soundPanelIndex = 0;
            soundButtons[_soundPanelIndex].gameObject.SetActive(true);
        }));

        // Buttons
        AddActionToButton(upArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.MoveForward));
        AddActionToButton(bottomArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.MoveBackward));
        AddActionToButton(rightArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.TurnRight));
        AddActionToButton(leftArrow, EventTriggerType.PointerDown,
            () => _arRobotLogicMod.StartSendingRealtimeAction(RobotAction.MotionAction.TurnLeft));

        AddActionToButton(upArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.MoveForward));
        AddActionToButton(bottomArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.MoveBackward));
        AddActionToButton(rightArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.TurnRight));
        AddActionToButton(leftArrow, EventTriggerType.PointerUp,
            () => _arRobotLogicMod.StopSendingRealtimeAction(RobotAction.MotionAction.TurnLeft));

        openArms.onClick.AddListener(() => _arRobotLogicMod.SendRealtimeAction(RobotAction.ArmsAction.OpenArms));
        closeArms.onClick.AddListener(() => _arRobotLogicMod.SendRealtimeAction(RobotAction.ArmsAction.CloseArms));

        for (int i = 0; i < faceButtons.Length; i++)
        {
            var i1 = i;
            faceButtons[i].onClick
                .AddListener(() =>
                    _arRobotLogicMod.SendRealtimeAction(
                        (RobotAction.EmotionAction) (int.Parse(faceButtons[i1].name) + 1)));
        }

        for (int i = 0; i < soundButtons.Length; i++)
        {
            var i2 = i;
            soundButtons[i].onClick
                .AddListener(() =>
                {
                    var i1 = i2;
                    PlaySound((RobotAction.SoundAction) i1);
                    if (i1 >= 4) i1 -= 2;
                    _arRobotLogicMod.SendRealtimeAction((RobotAction.SoundAction) i1);
                });
        }
    }


    private void AddActionToButton(Button button, EventTriggerType type, Action action)
    {
        EventTrigger trigger = button.gameObject.AddComponent<EventTrigger>();
        var eventToTrigger = new EventTrigger.Entry();
        eventToTrigger.eventID = type;
        eventToTrigger.callback.AddListener((e) => action());
        trigger.triggers.Add(eventToTrigger);
    }


    private void _PhotoButtonPressed()
    {
        _arRobotLogicMod.TakePhotoAsync();
    }

    private bool videoRecording = false;

    private void _VideoButtonPressed()
    {
        if (!videoRecording)
        {
            _arRobotLogicMod.StartMakingVideo();
            videoButton.transform.Find("Text").GetComponent<Text>().text = "STOP";
        }
        else
        {
            _arRobotLogicMod.StopAndSaveVideoAsync();
            videoButton.transform.Find("Text").GetComponent<Text>().text = "VIDEO";
        }

        videoRecording = !videoRecording;
    }


    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        switch (sceneActionType)
        {
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopShowingCameraOnTexture();
                _arRobotLogicMod.StopSendingRealtimeAction();
                StopSound();
                break;
            default:
                break;
        }
    }

    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public float timePerAudio = 1;

    private Coroutine _soundCoroutine = null;

    private void PlaySound(RobotAction.SoundAction soundAction)
    {
        if (_soundCoroutine != null) StopCoroutine(_soundCoroutine);

        if (soundAction == RobotAction.SoundAction.None || soundAction == RobotAction.SoundAction.Stop)
        {
            audioSource.Stop();
            return;
        }

        audioSource.Stop();
        audioSource.clip = audioClips[(int) soundAction];
        audioSource.Play();
        _soundCoroutine = StartCoroutine(SoundCoroutine());
    }

    private IEnumerator SoundCoroutine()
    {
        float initialTime = Time.time;
        while (Time.time - initialTime < timePerAudio)
            yield return null;

        StopSound();
    }

    private void StopSound()
    {
        audioSource.Stop();
    }
}