﻿using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using ArRobot.Modules.Logic.Scripts.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UISyncSetupScene : UISceneBase, ILogicBluetoothConnectivityHandler
{
    public Button continueOfflineButton;
    public Button synchronizeButton;

    public GameObject dialogPanel;
    public GameObject loadingPanel;
    public GameObject successPanel;
    public Button successButton;
    public GameObject errorPanel;
    public Button errorButton;

    public GameObject disconnectPanel;
    public Button disconnectButton;
    public Button keepConnectedButton;
    public GameObject successDisconnectedPanel;
    public Button successDisconnectedButton;


    private IArRobotLogicBluetoothConnectivityMod _arRobotLogicBluetoothConnectivityMod;

    // Start is called before the first frame update
    void UAwake()
    {
        _arRobotLogicBluetoothConnectivityMod = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();

        continueOfflineButton.onClick.AddListener(_ContinueOfflineButton);
        synchronizeButton.onClick.AddListener(_SynchronizeButton);

        keepConnectedButton.onClick.AddListener((() => UISceneManager.instance.RestorePreviousScene()));
        disconnectButton.onClick.AddListener(_DisconnectButton);
        successDisconnectedButton.onClick.AddListener((() => UISceneManager.instance.RestorePreviousScene()));

        
        dialogPanel.SetActive(false);
        loadingPanel.SetActive(false);
        successPanel.SetActive(false);
        errorPanel.SetActive(false);
        disconnectPanel.SetActive(false);
        successDisconnectedPanel.SetActive(false);

        if (!_arRobotLogicBluetoothConnectivityMod.IsBluetoothConnected())
        {
            dialogPanel.SetActive(true);
        }
        else
        {
            disconnectPanel.SetActive(true);
        }
    }

    void UStart()
    {
    }

    private void _ContinueOfflineButton()
    {
        UISceneManager.instance.RestorePreviousScene();
    }

    private void _SynchronizeButton()
    {
        dialogPanel.SetActive(false);
        loadingPanel.SetActive(true);
        _arRobotLogicBluetoothConnectivityMod.ScanAndConnectToCompatibleDeviceAsync();
    }

    private void _DisconnectButton()
    {
        disconnectPanel.SetActive(false);
        loadingPanel.SetActive(true);
        _arRobotLogicBluetoothConnectivityMod.DisconnectFromBluetoothDeviceAsync();
    }

    public void OnBluetoothDeviceListUpdated(List<LogicBluetoothDevice> bluetoothDevices)
    {
    }

    public void OnBluetoothConnected(LogicBluetoothDevice logicBluetoothDevice,
        ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        loadingPanel.SetActive(false);
        if (state == ArRobotLogicConnectivityVariables.ConnectionState.Connected)
        {
            successPanel.SetActive(true);
            successButton.onClick.AddListener(() =>
            {
                successPanel.SetActive(false);
                UISceneManager.instance.RestorePreviousScene();
            });
        }
        else
        {
            errorPanel.SetActive(true);
            errorButton.onClick.AddListener(() =>
                {
                    errorPanel.SetActive(false);
                    dialogPanel.SetActive(true);
                }
            );
        }
    }

    public void OnBluetoothDisconnected(LogicBluetoothDevice logicBluetoothDevice,
        ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        loadingPanel.SetActive(false);
        successDisconnectedPanel.SetActive(true);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
    }
}