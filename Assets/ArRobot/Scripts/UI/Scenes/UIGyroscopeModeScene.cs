﻿using System;
using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Logic.Scripts.Handlers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIGyroscopeModeScene : UISceneBase, ILogicGyroscopeHandler
{
    public Button topFaceArrow, bottomFaceArrow;
    public GameObject[] facePanels;
    public Button[] faceButtons;

    public Button topSoundArrow, bottomSoundArrow;
    public GameObject[] soundPanels;
    public Button[] soundButtons;

    // Buttons
    public Button openArms, closeArms;

    public Image topOut, topIn, leftOut, leftIn, bottomOut, bottomIn, rightOut, rightIn;
    public Color arrowActiveColor;

    public Button stabilizatorButton;

    private int facePanelIndex;
    private int soundPanelIndex;

    private IArRobotLogicMod _arRobotLogicMod;

    void UStart()
    {
        facePanels[0].SetActive(true);
        facePanels[1].SetActive(false);
        facePanels[2].SetActive(false);

        soundPanels[0].SetActive(true);
        soundPanels[1].SetActive(false);

        topFaceArrow.onClick.AddListener((() =>
        {
            facePanels[facePanelIndex].SetActive(false);
            if (--facePanelIndex < 0) facePanelIndex = 2;
            facePanels[facePanelIndex].SetActive(true);
        }));

        bottomFaceArrow.onClick.AddListener((() =>
        {
            facePanels[facePanelIndex].SetActive(false);
            if (++facePanelIndex > 2) facePanelIndex = 0;
            facePanels[facePanelIndex].SetActive(true);
        }));

        topSoundArrow.onClick.AddListener((() =>
        {
            soundPanels[soundPanelIndex].SetActive(false);
            if (--soundPanelIndex < 0) soundPanelIndex = 1;
            soundPanels[soundPanelIndex].SetActive(true);
        }));

        bottomSoundArrow.onClick.AddListener((() =>
        {
            soundPanels[soundPanelIndex].SetActive(false);
            if (++soundPanelIndex > 1) soundPanelIndex = 0;
            soundPanels[soundPanelIndex].SetActive(true);
        }));

        // Gyro
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        _arRobotLogicMod.StartMotionWithGyroscope();
        StartCoroutine(StabilizeCoroutine(0.25f));
        

        topOut.color = topIn.color = leftOut.color =
            leftIn.color = bottomOut.color = bottomIn.color = rightOut.color = rightIn.color = Color.black;

        stabilizatorButton.onClick.AddListener((() => _arRobotLogicMod.StabilizeGyroscospe()));
        

        // Buttons
        openArms.onClick.AddListener(() => _arRobotLogicMod.SendRealtimeAction(RobotAction.ArmsAction.OpenArms));
        closeArms.onClick.AddListener(() => _arRobotLogicMod.SendRealtimeAction(RobotAction.ArmsAction.CloseArms));

        for (int i = 0; i < faceButtons.Length; i++)
        {
            var i1 = i;
            faceButtons[i].onClick
                .AddListener(() =>
                    {
                        var i2 = i1;
                        _arRobotLogicMod.SendRealtimeAction(
                            (RobotAction.EmotionAction) (int.Parse(faceButtons[i2].name) + 1));
                    });
        }

        for (int i = 0; i < soundButtons.Length; i++)
        {
            var i1 = i;
            soundButtons[i].onClick
                .AddListener(() =>
                {
                    var i2 = i1;
                    PlaySound(i2);
                    if (i2 >= 4) i2 -= 2;
                    _arRobotLogicMod.SendRealtimeAction((RobotAction.SoundAction) i2);
                });
        }
    }

    private void AddActionToButton(Button button, EventTriggerType type, Action action)
    {
        EventTrigger trigger = button.gameObject.AddComponent<EventTrigger>();
        var eventToTrigger = new EventTrigger.Entry();
        eventToTrigger.eventID = type;
        eventToTrigger.callback.AddListener((e) => action());
        trigger.triggers.Add(eventToTrigger);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();

        switch (sceneActionType)
        {
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopMotionWithGyroscope();
                _arRobotLogicMod.StopSendingRealtimeAction();
                StopSound();
                break;
        }
    }

    private int savedHorizontal, savedVertical;

    public void OnNewGyroscopeStatus(float horizontal, float vertical)
    {
        //Debug.Log(string.Format("Verticarl: {0} - Horizontal: {1}", vertical, horizontal));
        
        topOut.color = topIn.color = leftOut.color =
            leftIn.color = bottomOut.color = bottomIn.color = rightOut.color = rightIn.color = Color.black;
        
        
        if (horizontal > 0)
        {
            rightIn.color = arrowActiveColor;
            if (horizontal > 0.5f)
            {
                rightOut.color = arrowActiveColor;
            }
        }
        else if (horizontal < 0)
        {
            leftIn.color = arrowActiveColor;
            if (horizontal < -0.5f)
            {
                leftOut.color = arrowActiveColor;
            }
        }

        if (vertical > 0)
        {
            topIn.color = arrowActiveColor;
            if (vertical > 0.5f)
            {
                topOut.color = arrowActiveColor;
            }
        }
        else if (vertical < 0)
        {
            bottomIn.color = arrowActiveColor;
            if (vertical < -0.5f)
            {
                bottomOut.color = arrowActiveColor;
            }
        }

        int newHorizontal = 0;
        if (horizontal > 0) newHorizontal = 1;
        else if (horizontal < 0) newHorizontal = -1;
        

        if (savedHorizontal != 0)
            if (savedHorizontal != newHorizontal)
                _arRobotLogicMod.StopSendingRealtimeAction();
        
        if (newHorizontal != 0)
            if (savedHorizontal != newHorizontal)
                _arRobotLogicMod.StartSendingRealtimeAction(newHorizontal > 0 ? RobotAction.MotionAction.TurnRight : RobotAction.MotionAction.TurnLeft);

        savedHorizontal = newHorizontal;


        int newVertical = 0;
        if (vertical > 0) newVertical = 1;
        else if (vertical < 0) newVertical = -1;

        if (savedVertical != 0)
            if (savedVertical != newVertical)
                _arRobotLogicMod.StopSendingRealtimeAction();
        
        if (newVertical != 0)
            if (savedVertical != newVertical)
                _arRobotLogicMod.StartSendingRealtimeAction(newVertical > 0 ? RobotAction.MotionAction.MoveForward : RobotAction.MotionAction.MoveBackward);

        savedVertical = newVertical;
    }
    
    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public float timePerAudio = 1;

    private Coroutine _soundCoroutine = null;
    private void PlaySound(int soundAction)
    {
        if (_soundCoroutine != null) StopCoroutine(_soundCoroutine);

        audioSource.Stop();
        audioSource.clip = audioClips[soundAction];
        audioSource.Play();
        _soundCoroutine = StartCoroutine(SoundCoroutine());
    }

    private IEnumerator SoundCoroutine()
    {
        float initialTime = Time.time;
        while (Time.time - initialTime < timePerAudio)
            yield return null;
        
        StopSound();
    }

    private void StopSound()
    {
        audioSource.Stop();
    }

    private IEnumerator StabilizeCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        _arRobotLogicMod.StabilizeGyroscospe();
    }
}