﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.ExternalMods.Media.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class UIProgrammingModePhotoScene : UISceneBase, ILogicActionsPlayerHandler
{
    public Button powerButton;
    public Button photoButton;
    public Button videoButton;
    
    private IProgrammingGridMod _programmingGridMod;
    private IArRobotLogicMod _arRobotLogicMod;

    // Grid Panel
    public GameObject gridContainer;
    
    public Sprite stopSprite;
    public Sprite powerSprite;

    public RawImage cameraRawImage;
    public AspectRatioFitter cameraAspectRatioFitter;
    
    public AudioSource audioSource;
    public AudioClip[] audioClips;

    // Start is called before the first frame update

    void UAwake()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
    }
    
    void UStart()
    {
        _programmingGridMod = UmodsApplication.GetMod<IProgrammingGridMod>();
        _programmingGridMod.InitializeColumn(gridContainer);
        _programmingGridMod.CreateColumn();

        powerButton.onClick.AddListener((() => _PowerButtonPressed()));
        photoButton.onClick.AddListener((() => _PhotoButtonPressed()));
        videoButton.onClick.AddListener((() => _VideoButtonPressed()));
        
        _arRobotLogicMod.ShowCameraOnTexture(cameraRawImage, cameraAspectRatioFitter);
    }

    private void _PowerButtonPressed()
    {
        RobotAction.MotionAction[] motionActions;
        RobotAction.EmotionAction[] emotionActions;
        RobotAction.ArmsAction[] armsActions;
        RobotAction.SoundAction[] soundActions;

        _programmingGridMod.GetGridActions(out motionActions, out emotionActions, out armsActions,
            out soundActions);

        // I changed last parameter to true
        _arRobotLogicMod.PlayRoutine(motionActions, emotionActions, armsActions,
            soundActions, true);

        _SetRestButtonPlaying(true);
    }
    
    private void _PhotoButtonPressed()
    {
        _arRobotLogicMod.TakePhotoAsync();
    }

    private bool videoRecording = false;
    private void _VideoButtonPressed()
    {
        if (!videoRecording)
        {
            _arRobotLogicMod.StartMakingVideo();
            videoButton.transform.Find("Text").GetComponent<Text>().text = "STOP";
        }
        else
        {
            _arRobotLogicMod.StopAndSaveVideoAsync();
            videoButton.transform.Find("Text").GetComponent<Text>().text = "VIDEO";
        }

        videoRecording = !videoRecording;
    }

    private void _SetRestButtonPlaying(bool value)
    {
        if(value)
        {
            powerButton.transform.Find("Text").GetComponent<Text>().text = "STOP";
            powerButton.transform.Find("Icon").GetComponent<Image>().sprite = stopSprite;
            powerButton.transform.Find("Icon").GetComponent<Image>().preserveAspect = true;
            powerButton.onClick.RemoveAllListeners();
            powerButton.onClick.AddListener((() =>
            {
                _arRobotLogicMod.StopRoutine();
                _SetRestButtonPlaying(false);
            }));
        }
        else
        {
            powerButton.transform.Find("Text").GetComponent<Text>().text = "POWER";
            powerButton.transform.Find("Icon").GetComponent<Image>().sprite = powerSprite;
            powerButton.onClick.RemoveAllListeners();
            powerButton.onClick.AddListener((_PowerButtonPressed));
        }
    }

    public void OnNewStepPlayed(int stepCounter, RobotAction.MotionAction motionAction,
        RobotAction.ArmsAction armsAction, RobotAction.EmotionAction emotionAction,
        RobotAction.SoundAction soundAction)
    {
        if (motionAction == RobotAction.MotionAction.Stop)
        {
            StopPlaying();
            return;
        }
        _programmingGridMod.FillColumn(stepCounter);
        _programmingGridMod.SetColumnActivation(stepCounter,false);
        
        PlaySound(soundAction);
    }
    
    private void StopPlaying()
    {
        _arRobotLogicMod.StopRoutine();
        _SetRestButtonPlaying(false);
        StopSound();
        _programmingGridMod.FillColumn(-1);
    }

    public override void UITopButtonAction(SceneActionType sceneActionType)
    {
        switch (sceneActionType)
        {
            case SceneActionType.Back:
                if (_arRobotLogicMod == null) _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
                _arRobotLogicMod.StopShowingCameraOnTexture();
                StopSound();
                break;
            default:
                break;
        }
    }
    
    private void PlaySound(RobotAction.SoundAction soundAction)
    {
        if (soundAction == RobotAction.SoundAction.None || soundAction == RobotAction.SoundAction.Stop)
        {
            audioSource.Stop();
            return;
        }
            
        audioSource.Stop();
        audioSource.clip = audioClips[(int) soundAction];
        audioSource.Play();
    }

    private void StopSound()
    {
        audioSource.Stop();
    }
}