﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using ArRobot.Modules.Logic.Scripts.Core;
using Umods.Scripts.Core;


public class UIBluetoothButton : UmodsBehaviour
{
    public UISceneBase syncSetupScene;

    public Color connectedColor, disconnectedColor;

    private IArRobotLogicBluetoothConnectivityMod _arRobotLogicBluetoothConnectivityMod;
    private Image imageComponent;

    private void UStart()
    {
        _arRobotLogicBluetoothConnectivityMod = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();
        UISceneManager.instance.OnSceneOpened.AddListener(InitWithData);
        GetComponent<Button>().onClick.AddListener(OpenSyncScene);
        imageComponent = GetComponent<Image>();
    }

    private void UUpdate()
    {
        bool isConnected = _arRobotLogicBluetoothConnectivityMod.IsBluetoothConnected();
        Color buttonColor = isConnected ? connectedColor : disconnectedColor;
        imageComponent.color = buttonColor;
    }

    void OpenSyncScene()
    {
        UISceneManager.instance.OpenScene(syncSetupScene);
    }

    void InitWithData(UISceneData sceneData)
    {
        if (sceneData.hideBluetoothButton)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }
}
