﻿using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;

public class UIScene : UmodsBehaviour, ILogicBluetoothConnectivityHandler
{
    private IArRobotLogicBluetoothConnectivityMod _arRobotLogicBluetoothConnectivityMod;
        
    void UStart()
    {
        _arRobotLogicBluetoothConnectivityMod = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();
        
        _arRobotLogicBluetoothConnectivityMod.StartGettingBluetoothDeviceListAsync();
    }

    public void OnBluetoothDeviceListUpdated(List<LogicBluetoothDevice> bluetoothDevices)
    {
        
    }

    public void OnBluetoothConnected(LogicBluetoothDevice logicBluetoothDevice, ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        
    }

    public void OnBluetoothDisconnected(LogicBluetoothDevice logicBluetoothDevice, ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        
    }
}
