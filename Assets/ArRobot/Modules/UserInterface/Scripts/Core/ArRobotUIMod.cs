﻿using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;

public class ArRobotUIMod : UmodsModBehaviour, IArRobotUIMod, ILogicRoutinesStorageHandler
{
    private IUmodsEventListener _umodsEventListener;

    private IArRobotLogicMod _arRobotLogicMod;

    // Start is called before the first frame update
    void UStart()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        
        //_arRobotLogicMod.LoadRoutineAsync();
    }

    // Update is called once per frame
    void UUpdate()
    {
    }

    public override void OnListenersAndModRegistration()
    {
        UmodsApplication.RegisterMod<IArRobotUIMod>(this);
        _umodsEventListener = UmodsApplication.EventListenerManager.RegisterListener<IArRobotUIHandler>();
    }

    public override void OnDestroyListenersAndModUnregistration()
    {
        UmodsApplication.UnregisterMod<IArRobotUIMod>();
        UmodsApplication.EventListenerManager.UnregisterListener<IArRobotUIHandler>();
    }

    public void OnRoutinesListReceived(List<SavedRoutine> savedRoutines)
    {
        throw new System.NotImplementedException();
    }

    public void OnRoutineLoaded(bool success, SavedRoutine loadedRoutine, RobotAction.MotionAction[] motionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.EmotionAction[] emotionActions, RobotAction.SoundAction[] soundActions)
    {
        throw new System.NotImplementedException();
    }


    public void OnRoutineSaved(bool success, SavedRoutine savedRoutine)
    {
        throw new System.NotImplementedException();
    }
}