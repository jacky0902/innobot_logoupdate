﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILogicRoutinesStorageHandler {
    void OnRoutineLoaded(bool success, SavedRoutine loadedRoutine, RobotAction.MotionAction[] motionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.EmotionAction[] emotionActions, RobotAction.SoundAction[] soundActions);
    void OnRoutineSaved(bool success, SavedRoutine savedRoutine);
}
