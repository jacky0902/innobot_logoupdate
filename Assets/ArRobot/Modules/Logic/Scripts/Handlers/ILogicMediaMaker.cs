namespace ArRobot.Modules.Logic.Scripts.Handlers
{
    public interface ILogicMediaMaker
    {
        void OnPhotoSaved(bool success, string error);
        void OnVideoSaved(bool success, string error);
    }
}