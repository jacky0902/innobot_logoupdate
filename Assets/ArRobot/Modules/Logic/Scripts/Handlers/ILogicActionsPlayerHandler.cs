﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILogicActionsPlayerHandler
{
    void OnNewStepPlayed(int stepCounter, RobotAction.MotionAction motionAction, RobotAction.ArmsAction armsAction,
        RobotAction.EmotionAction emotionAction, RobotAction.SoundAction soundAction);
}