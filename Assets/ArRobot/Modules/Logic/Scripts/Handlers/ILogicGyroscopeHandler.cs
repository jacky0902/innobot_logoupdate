namespace ArRobot.Modules.Logic.Scripts.Handlers
{
    public interface ILogicGyroscopeHandler
    {
        void OnNewGyroscopeStatus(float horizontal, float vertical);
    }
}