﻿using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using UnityEngine;

public interface ILogicBluetoothConnectivityHandler
{
    void OnBluetoothDeviceListUpdated(List<LogicBluetoothDevice> bluetoothDevices);
    void OnBluetoothConnected(LogicBluetoothDevice logicBluetoothDevice, ArRobotLogicConnectivityVariables.ConnectionState state);
    void OnBluetoothDisconnected(LogicBluetoothDevice logicBluetoothDevice, ArRobotLogicConnectivityVariables.ConnectionState state);
}