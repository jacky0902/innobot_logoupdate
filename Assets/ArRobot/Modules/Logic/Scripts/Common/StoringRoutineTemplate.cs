using UnityEngine;

namespace ArRobot.Modules.Logic.Scripts.Common
{
    public class StoringRoutineTemplate: StorageTemplate
    {
        public string id;
        public string name;
        public RobotAction.MotionAction[] motionActions;
        public RobotAction.ArmsAction[] armsActions;
        public RobotAction.EmotionAction[] emotionActions;
        public RobotAction.SoundAction[] soundActions;

        public StoringRoutineTemplate(string id, string name, RobotAction.MotionAction[] motionActions, RobotAction.ArmsAction[] armsActions, RobotAction.EmotionAction[] emotionActions, RobotAction.SoundAction[] soundActions)
        {
            this.id = id;
            this.name = name;
            this.motionActions = motionActions;
            this.armsActions = armsActions;
            this.emotionActions = emotionActions;
            this.soundActions = soundActions;
        }
    }
}