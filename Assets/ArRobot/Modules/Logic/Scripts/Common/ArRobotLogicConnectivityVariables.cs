using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;

namespace ArRobot.Modules.Common
{
    public class ArRobotLogicConnectivityVariables
    {
        public enum ConnectionState
        {
            Connected,
            WrongAddress,
            ConnectionTimeout,
            ConnectionError,
            Disconnected,
            NotKnown
        }

        public static ConnectionState GetState(ConnectivityVariables.ConnectionState state)
        {
            switch (state)
            {
                case ConnectivityVariables.ConnectionState.Connected:
                    return ConnectionState.Connected;

                case ConnectivityVariables.ConnectionState.WrongAddress:
                    return ConnectionState.WrongAddress;

                case ConnectivityVariables.ConnectionState.ConnectionError:
                    return ConnectionState.ConnectionError;

                case ConnectivityVariables.ConnectionState.Disconnected:
                    return ConnectionState.Disconnected;
                
                case ConnectivityVariables.ConnectionState.ConnectionTimeout:
                    return ConnectionState.ConnectionTimeout;
                
                default:
                    return ConnectionState.NotKnown;
            }
        }
    }
}