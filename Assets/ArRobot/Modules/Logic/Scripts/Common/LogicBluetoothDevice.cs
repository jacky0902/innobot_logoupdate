﻿using System;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;

namespace ArRobot.Modules.Logic.Scripts.Common
{
    public class LogicBluetoothDevice : IEquatable<LogicBluetoothDevice>, IComparable<LogicBluetoothDevice>
    {
        public string Name { get; }
        public string Address { get; }

        public LogicBluetoothDevice(string address, string name)
        {
            Address = address;
            Name = name;
        }
        
        public LogicBluetoothDevice(ConnectivityBluetoothDevice bluetoothDevice)
        {
            Address = bluetoothDevice.DeviceAddress;
            Name = bluetoothDevice.DeviceName;
        }

        public bool Equals(LogicBluetoothDevice other)
        {
            return other != null && this.Name.Equals(other.Name) && this.Address.Equals(other.Address);
        }

        public int CompareTo(LogicBluetoothDevice other)
        {
            // A null value means that this object is greater.
            return other == null ? 1 : this.Name.CompareTo(other.Name);
        }
    }
}