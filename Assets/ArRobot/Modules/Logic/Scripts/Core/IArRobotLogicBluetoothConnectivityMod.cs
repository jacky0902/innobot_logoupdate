using ArRobot.Modules.Logic.Scripts.Common;

namespace ArRobot.Modules.Logic.Scripts.Core
{
    public interface IArRobotLogicBluetoothConnectivityMod
    {
        // Scanning devices
        void StartGettingBluetoothDeviceListAsync();
        void StopGettingBluetoothDeviceList();
        bool IsScanningBluetoothDevices();
        
        // Connecting to device
        void ConnectToBluetoothDeviceAsync(string address);
        void DisconnectFromBluetoothDeviceAsync();

        void ScanAndConnectToCompatibleDeviceAsync();
        void StopScanningAndConnectingToCompatibleDevice();

        // Others
        LogicBluetoothDevice GetBluetoothDevice(string address);
        LogicBluetoothDevice GetConnectedBluetoothDevice();
        bool IsBluetoothConnected();

        
        string ServiceUUID { get; set; }
        string CharacteristicUUID { get; set; }
        string DeviceName { get; set; }
    }
}