﻿using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using UnityEngine;
using UnityEngine.UI;

public interface IArRobotLogicMod
{
    // Programming mode
    SavedRoutine[] GetSavedRoutinesList();
    void LoadRoutineAsync(string routineID);
    void SaveRoutineAsync(string routineName, RobotAction.MotionAction[] motionActions,
        RobotAction.EmotionAction[] emotionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.SoundAction[] soundActions);

    // PM: Actions player
    bool PlayRoutine(RobotAction.MotionAction[] motionActions,
        RobotAction.EmotionAction[] emotionActions, RobotAction.ArmsAction[] armsActions,
        RobotAction.SoundAction[] soundActions, bool playOnConnectedRobot = true, bool turn90Degrees = true);
    void PauseRoutine();
    void UnpauseRoutine();
    void StopRoutine();

    // Realtime and gyroscope mode
    bool SendRealtimeAction(RobotAction.ArmsAction action);
    bool SendRealtimeAction(RobotAction.EmotionAction action);
    bool SendRealtimeAction(RobotAction.SoundAction action);

    // Realtime mode
    void StartSendingRealtimeAction(RobotAction.MotionAction action);
    void StopSendingRealtimeAction(RobotAction.MotionAction action = RobotAction.MotionAction.None);
    
    // Gyroscope mode
    bool StartMotionWithGyroscope(float updateInterval= 0.0167f); // 60 Hz
    void StopMotionWithGyroscope();
    void StabilizeGyroscospe();

    // Dancing mode
    void StartDancing(DancingMode.DancingMusic dancingMusic);
    void PauseDancing();
    void StopDancing();


    // Photos and video maker
    void ShowCameraOnTexture(RawImage rawImage, AspectRatioFitter aspectRatioFitter);
    void StopShowingCameraOnTexture();
    void TakePhotoAsync();
    void StartMakingVideo();
    void StopAndSaveVideoAsync();
}