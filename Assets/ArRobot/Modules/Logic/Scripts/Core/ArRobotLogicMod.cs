﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using ArRobot.Modules.Logic.Scripts.Core;
using ArRobot.Modules.Logic.Scripts.Handlers;
using DefaultNamespace;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Handlers;
using Umods.ExternalMods.Controllers.Gyroscope.Scripts.Core;
using Umods.ExternalMods.Media.Scripts.Core;
using Umods.ExternalMods.Storage.Mods.StorageMod.Common;
using Umods.ExternalMods.Storage.Mods.StorageMod.Core;
using Umods.ExternalMods.Storage.Mods.StorageMod.Handlers;
using Umods.Scripts.Core;
using Umods.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class ArRobotLogicMod : UmodsModBehaviour, IArRobotLogicMod, IArRobotLogicBluetoothConnectivityMod,
    IConnectivityBluetoothHandler, ILoadObjectTemplateHandler, IStorageCompletedHandler, IGyroscopeControllerHandler
{
    private enum ConnectionState
    {
        NotConnected,
        Connecting,
        Connected,
        Disconnecting
    };

    private enum RoutineState
    {
        PlayingRoutineOnRobot,
        PausedRoutineOnRobot,
        NotPlayingRoutine,
        PlayingRoutineSimulation,
        PausedRoutineSimulation
    }

    private enum RealActionsState
    {
        SendingMotionAction,
        NotSendingContinuousAction
    }

    private enum GyroscopeState
    {
        Enabled,
        NotEnabled
    }


    // Public variables
    public float timePerActionInRoutine = 3.0f;

    //public int longTimePerAction = 10;
    public int timePerActionInRealTime = 3;
    public float scanningAndConnectingTimeout = 5.0f;

    public string routineStoragePath = "saved_routines";

    [SerializeField] private string _serviceUuid;
    [SerializeField] private string _characteristicUuid;
    [SerializeField] private string _deviceName;


    public string ServiceUUID
    {
        get => _serviceUuid;
        set => _serviceUuid = value;
    }

    public string CharacteristicUUID
    {
        get => _characteristicUuid;
        set => _characteristicUuid = value;
    }

    public string DeviceName
    {
        get => _deviceName;
        set => _deviceName = value;
    }

    // Listeners
    private IUmodsEventListener _logicBluetoothConnectivityListener;
    private IUmodsEventListener _logicRoutinesStorageListener;
    private IUmodsEventListener _logicActionsPlayerListener;
    private IUmodsEventListener _logicActionsMediaMakerListener;
    private IUmodsEventListener _logicGyroscopeListener;

    // Actions
    private int _routineLength = 0;

    private RobotAction.MotionAction[] _motionActionsRoutine;
    private RobotAction.ArmsAction[] _armsActionsRoutine;
    private RobotAction.EmotionAction[] _emotionActionsRoutine;
    private RobotAction.SoundAction[] _soundActionsRoutine;

    private RoutineState _routineState = RoutineState.NotPlayingRoutine;
    private RealActionsState _realActionsState = RealActionsState.NotSendingContinuousAction;

    private Coroutine _playingRoutineCoroutine = null;

    private int _lastRoutineIndexPlayed = 0;
    private float _lastRoutineTimePlayed;
    private float _actionPauseTime;


    // Connectivity
    private LogicBluetoothDevice _connectedBluetoothDevice = null;
    private bool _scaningAndConnectingProcess = false;
    private Coroutine _scanningAndConnectingCoroutine = null;
    private ConnectionState _connectionState = ConnectionState.NotConnected;

    // Mods
    private IConnectivityBluetoothMod _connectivityBluetoothMod;
    private IStorageMod _storageMod;

    // Storage
    private Dictionary<string, SavedRoutine> _savedRoutinesDict = new Dictionary<string, SavedRoutine>();
    private SavedRoutine _savingOrLoadingRoutine;

    // Gyroscope
    private IGyroscopeControllerMod _gyroscopeControllerMod;
    public float maxGyroValueHorizontal = 0.2f;
    public float maxGyroValueVertical = 0.2f;
    public float centerVoidGyroValueHorizontal = 10f;
    public float centerVoidGyroValueVertical = 10f;

    private Quaternion _referenceRotation;
    private Quaternion _lastRotation;
    private bool _referenceRotationInitialized = false;

    private GameObject _referenceRotationGO;
    private GameObject _newRotationGO;

    // Media
    private ICameraMediaMakerMod _cameraMediaMakerMod;

    private void UStart()
    {
        _connectivityBluetoothMod = UmodsApplication.GetMod<IConnectivityBluetoothMod>();
        _storageMod = UmodsApplication.GetMod<IStorageMod>();
        _gyroscopeControllerMod = UmodsApplication.GetMod<IGyroscopeControllerMod>();
        _cameraMediaMakerMod = UmodsApplication.GetMod<ICameraMediaMakerMod>();

        _referenceRotationGO = new GameObject();
        _newRotationGO = new GameObject();
    }

    public override void OnListenersAndModRegistration()
    {
        UmodsApplication.RegisterMod<IArRobotLogicMod>(this);
        UmodsApplication.RegisterMod<IArRobotLogicBluetoothConnectivityMod>(this);

        _logicBluetoothConnectivityListener =
            UmodsApplication.EventListenerManager.RegisterListener<ILogicBluetoothConnectivityHandler>();
        _logicRoutinesStorageListener =
            UmodsApplication.EventListenerManager.RegisterListener<ILogicRoutinesStorageHandler>();
        _logicActionsPlayerListener =
            UmodsApplication.EventListenerManager.RegisterListener<ILogicActionsPlayerHandler>();
        _logicActionsMediaMakerListener =
            UmodsApplication.EventListenerManager.RegisterListener<ILogicMediaMaker>();
        _logicGyroscopeListener =
            UmodsApplication.EventListenerManager.RegisterListener<ILogicGyroscopeHandler>();
    }

    public override void OnDestroyListenersAndModUnregistration()
    {
        UmodsApplication.UnregisterMod<IArRobotLogicMod>();

        UmodsApplication.EventListenerManager.UnregisterListener<ILogicBluetoothConnectivityHandler>();
        UmodsApplication.EventListenerManager.UnregisterListener<ILogicRoutinesStorageHandler>();
        UmodsApplication.EventListenerManager.UnregisterListener<ILogicActionsPlayerHandler>();
        UmodsApplication.EventListenerManager.UnregisterListener<ILogicMediaMaker>();
        UmodsApplication.EventListenerManager.UnregisterListener<ILogicGyroscopeHandler>();
    }


    public void StartGettingBluetoothDeviceListAsync()
    {
        _connectivityBluetoothMod.StartScanningBluetoothDeviceListAsync();
    }


    public void StopGettingBluetoothDeviceList()
    {
        _connectivityBluetoothMod.StopScanningBluetoothDeviceList();
    }

    public void ConnectToBluetoothDeviceAsync(string address)
    {
        if (_connectionState != ConnectionState.NotConnected) return;
        _connectionState = ConnectionState.Connecting;
        _connectivityBluetoothMod.ConnectToBluetoothDeviceAsync(address);
    }

    public void DisconnectFromBluetoothDeviceAsync()
    {
        if (_connectionState != ConnectionState.Connected) return;
        _connectionState = ConnectionState.Disconnecting;
        _connectivityBluetoothMod.DisconnectFromBluetoothDeviceAsync();
    }

    public void ScanAndConnectToCompatibleDeviceAsync()
    {
        if (_scaningAndConnectingProcess) return;

        _scaningAndConnectingProcess = true;
        _connectivityBluetoothMod.StartScanningBluetoothDeviceListAsync();

        _scanningAndConnectingCoroutine = StartCoroutine(_ScanningAndConnectingTimeOut());
    }

    private IEnumerator _ScanningAndConnectingTimeOut()
    {
        yield return new WaitForSeconds(scanningAndConnectingTimeout);
        StopScanningAndConnectingToCompatibleDevice();
        _logicBluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
            new object[]
            {
                _connectedBluetoothDevice,
                ArRobotLogicConnectivityVariables.ConnectionState.ConnectionTimeout
            });
    }

    public void StopScanningAndConnectingToCompatibleDevice()
    {
        if (!_scaningAndConnectingProcess) return;

        _scaningAndConnectingProcess = false;
        StopCoroutine(_scanningAndConnectingCoroutine);
        StopGettingBluetoothDeviceList();
    }

    public LogicBluetoothDevice GetBluetoothDevice(string address)
    {
        var bluetoothDevice = _connectivityBluetoothMod.GetBluetoothDevice(address);
        return bluetoothDevice == null ? null : new LogicBluetoothDevice(bluetoothDevice);
    }

    public LogicBluetoothDevice GetConnectedBluetoothDevice()
    {
        var bluetoothDevice = _connectivityBluetoothMod.GetConnectedBluetoothDevice();
        return bluetoothDevice == null ? null : new LogicBluetoothDevice(bluetoothDevice);
    }

    public bool IsBluetoothConnected()
    {
        return _connectivityBluetoothMod.IsConnectedToAnyBluetoothDevice();
    }

    public bool IsScanningBluetoothDevices()
    {
        return _connectivityBluetoothMod.IsScanningBluetoothDevices();
    }

    private void _SendMessage(RobotAction.MotionAction motionAction, RobotAction.ArmsAction armsAction,
        RobotAction.EmotionAction emotionAction,
        RobotAction.SoundAction soundAction, int time)
    {
        if (_connectionState == ConnectionState.Connected)
        {
            _connectivityBluetoothMod.SendMessageToConnectedBluetooth(
                new ConnectivityBluetoothMod.WrittingOptions(ServiceUUID, CharacteristicUUID),
                _TransformActionsToMessage(motionAction, armsAction, emotionAction, soundAction, time));
            /*
            foreach (var message in _TransformActionsToMessage(emotionAction, time))
            {
                _connectivityBluetoothMod.SendMessageToConnectedBluetooth(
                    new ConnectivityBluetoothMod.WrittingOptions(ServiceUUID, CharacteristicUUID), message);
            }
            */
        }
    }

    public SavedRoutine[] GetSavedRoutinesList()
    {
        if (_savedRoutinesDict.Count != 0) return _savedRoutinesDict.Values.ToArray();

        var files = _storageMod.GetFilesInDirectory(Path.Combine(Application.persistentDataPath, routineStoragePath));
        SavedRoutine[] savedRoutines = new SavedRoutine[files.Length];
        for (int i = 0; i < files.Length; i++)
        {
            var splited = files[i].Split('-');
            savedRoutines[i] = new SavedRoutine(splited[0], splited[1]);
            _savedRoutinesDict[splited[0]] = savedRoutines[i];
        }

        return savedRoutines;
    }

    public void LoadRoutineAsync(string routineID)
    {
        if (!_savedRoutinesDict.ContainsKey(routineID))
            _logicRoutinesStorageListener.PullEvent("OnRoutineLoaded",
                new object[] {false, _savingOrLoadingRoutine, null, null, null, null});

        _savingOrLoadingRoutine = _savedRoutinesDict[routineID];
        string path = Path.Combine(Application.persistentDataPath, routineStoragePath,
            string.Format("{0}-{1}", _savingOrLoadingRoutine.Id, _savingOrLoadingRoutine.Name));
        _storageMod.LoadObjectTemplateAsync<StoringRoutineTemplate>(path);
    }

    public void SaveRoutineAsync(string routineName, RobotAction.MotionAction[] motionActions,
        RobotAction.EmotionAction[] emotionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.SoundAction[] soundActions)
    {
        string id = DateTime.Now.ToString("yyyyMMddHHmmss");
        string path = Path.Combine(Application.persistentDataPath, routineStoragePath,
            string.Format("{0}-{1}", id, routineName));
        _savingOrLoadingRoutine = new SavedRoutine(id, routineName);
        var template =
            new StoringRoutineTemplate(id, routineName, motionActions, armsActions, emotionActions, soundActions);
        _storageMod.StorageObjectTemplateAsync(template, path);
    }

    private static bool _turning90Degrees = false;
    public bool PlayRoutine(RobotAction.MotionAction[] motionActions,
        RobotAction.EmotionAction[] emotionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.SoundAction[] soundActions, bool playOnConnectedRobot = true, bool turn90Degrees = true)
    {
        _turning90Degrees = turn90Degrees;
        
        if (playOnConnectedRobot && _connectionState != ConnectionState.Connected) return false;

        if (_routineState != RoutineState.NotPlayingRoutine && _routineState != RoutineState.PausedRoutineSimulation &&
            _routineState != RoutineState.PausedRoutineOnRobot) return false;

        if (playOnConnectedRobot)
            _routineState = RoutineState.PlayingRoutineOnRobot;
        else
            _routineState = RoutineState.PlayingRoutineSimulation;

        _routineLength = Mathf.Min(motionActions.Length, armsActions.Length, emotionActions.Length,
            soundActions.Length);

        _motionActionsRoutine = motionActions;
        _armsActionsRoutine = armsActions;
        _emotionActionsRoutine = emotionActions;
        _soundActionsRoutine = soundActions;
        
        _playingRoutineCoroutine = StartCoroutine(_PlayingRoutineCoroutine());

        return true;
    }
    

    private IEnumerator _PlayingRoutineCoroutine()
    {
        int i;
        for (i = 0; i < _routineLength; i++)
        {
            if (_routineState == RoutineState.PlayingRoutineOnRobot)
                _PlayActionsOnRobot(
                    _motionActionsRoutine[i] == RobotAction.MotionAction.None ? RobotAction.MotionAction.Stop : _motionActionsRoutine[i], 
                    _armsActionsRoutine[i] == RobotAction.ArmsAction.None ? RobotAction.ArmsAction.Stop : _armsActionsRoutine[i], 
                    _emotionActionsRoutine[i] == RobotAction.EmotionAction.None ? RobotAction.EmotionAction.Stop : _emotionActionsRoutine[i], 
                    _soundActionsRoutine[i] == RobotAction.SoundAction.None ? RobotAction.SoundAction.Stop : _soundActionsRoutine[i], 
                    Mathf.CeilToInt(timePerActionInRoutine));

            Debug.Log(String.Format("Motion: {0}, Emotion: {1}, Arms:{2}, Sound:{3}", _motionActionsRoutine[i],
                _emotionActionsRoutine[i], _armsActionsRoutine[i], _soundActionsRoutine[i]));

            _logicActionsPlayerListener.PullEvent("OnNewStepPlayed", new object[]
            {
                i,
                _motionActionsRoutine[i], _armsActionsRoutine[i],
                _emotionActionsRoutine[i], _soundActionsRoutine[i],
            });

            _lastRoutineIndexPlayed = i;
            _lastRoutineTimePlayed = Time.time;
            yield return new WaitForSeconds(timePerActionInRoutine);
        }

        _logicActionsPlayerListener.PullEvent("OnNewStepPlayed", new object[]
        {
            i,
            RobotAction.MotionAction.Stop, RobotAction.ArmsAction.Stop,
            RobotAction.EmotionAction.Stop, RobotAction.SoundAction.Stop,
        });

        if (_routineState == RoutineState.PlayingRoutineOnRobot)
            _StopRobot(timePerActionInRoutine);
        _routineState = RoutineState.NotPlayingRoutine;
    }

    private IEnumerator _PlayingRoutineCoroutineWithPartial(int indexFirst, float timeFirst)
    {
        if (_routineState == RoutineState.PlayingRoutineOnRobot)
            _PlayActionsOnRobot(_motionActionsRoutine[indexFirst], _armsActionsRoutine[indexFirst],
                _emotionActionsRoutine[indexFirst], _soundActionsRoutine[indexFirst],
                Mathf.CeilToInt(timePerActionInRoutine - timeFirst));

        _logicActionsPlayerListener.PullEvent("OnNewStepPlayed", new object[]
        {
            indexFirst,
            _motionActionsRoutine[indexFirst], _armsActionsRoutine[indexFirst],
            _emotionActionsRoutine[indexFirst], _soundActionsRoutine[indexFirst],
        });

        _lastRoutineIndexPlayed = indexFirst;
        _lastRoutineTimePlayed = Time.time - timeFirst;
        yield return new WaitForSeconds(timePerActionInRoutine - timeFirst);

        int i;
        for (i = indexFirst + 1; i < _routineLength; i++)
        {
            if (_routineState == RoutineState.PlayingRoutineOnRobot)
                _PlayActionsOnRobot(_motionActionsRoutine[i], _armsActionsRoutine[i], _emotionActionsRoutine[i],
                    _soundActionsRoutine[i], Mathf.CeilToInt(timePerActionInRoutine));

            _logicActionsPlayerListener.PullEvent("OnNewStepPlayed", new object[]
            {
                i,
                _motionActionsRoutine[i], _armsActionsRoutine[i],
                _emotionActionsRoutine[i], _soundActionsRoutine[i],
            });

            _lastRoutineIndexPlayed = i;
            _lastRoutineTimePlayed = Time.time;
            yield return new WaitForSeconds(timePerActionInRoutine);
        }

        _logicActionsPlayerListener.PullEvent("OnNewStepPlayed", new object[]
        {
            i,
            RobotAction.MotionAction.Stop, RobotAction.ArmsAction.Stop,
            RobotAction.EmotionAction.Stop, RobotAction.SoundAction.Stop,
        });

        if (_routineState == RoutineState.PlayingRoutineOnRobot)
            _StopRobot(timePerActionInRoutine);
        _routineState = RoutineState.NotPlayingRoutine;
    }

    private void _PlayActionsOnRobot(RobotAction.MotionAction motionAction, RobotAction.ArmsAction armsAction,
        RobotAction.EmotionAction emotionAction, RobotAction.SoundAction soundAction, int time)
    {
        //_StopRobot();
        _connectivityBluetoothMod.SendMessageToConnectedBluetooth(
            new ConnectivityBluetoothMod.WrittingOptions(_serviceUuid, _characteristicUuid),
            _TransformActionsToMessage(motionAction, armsAction, emotionAction, soundAction, time));
        /*
        foreach (var msg in _TransformActionsToMessage(emotionAction, longTimePerAction))
        {
            _connectivityBluetoothMod.SendMessageToConnectedBluetooth(
                new ConnectivityBluetoothMod.WrittingOptions(_serviceUuid, _characteristicUuid),
                msg);
        }
        */
    }

    public void PauseRoutine()
    {
        if (_routineState != RoutineState.PlayingRoutineSimulation &&
            _routineState != RoutineState.PlayingRoutineOnRobot) return;

        if (_routineState == RoutineState.PlayingRoutineOnRobot)
        {
            _routineState = RoutineState.PausedRoutineOnRobot;
            _StopRobot(timePerActionInRoutine);
        }
        else
            _routineState = RoutineState.PausedRoutineSimulation;

        _actionPauseTime = Time.time - _lastRoutineTimePlayed;
        StopCoroutine(_playingRoutineCoroutine);
    }

    private void _StopRobot(float time)
    {
        _SendMessage(RobotAction.MotionAction.Stop, RobotAction.ArmsAction.Stop, RobotAction.EmotionAction.Stop,
            RobotAction.SoundAction.Stop, Mathf.CeilToInt(time));
    }

    private void _StopFace(float time)
    {
        _SendMessage(RobotAction.MotionAction.None, RobotAction.ArmsAction.None, RobotAction.EmotionAction.Stop,
            RobotAction.SoundAction.None, Mathf.CeilToInt(time));
    }

    public void UnpauseRoutine()
    {
        if (_routineState != RoutineState.PausedRoutineOnRobot &&
            _routineState != RoutineState.PausedRoutineSimulation) return;

        _routineState = _routineState == RoutineState.PausedRoutineSimulation
            ? RoutineState.PlayingRoutineSimulation
            : RoutineState.PlayingRoutineOnRobot;

        _playingRoutineCoroutine =
            StartCoroutine(_PlayingRoutineCoroutineWithPartial(_lastRoutineIndexPlayed, _actionPauseTime));
    }

    public void StopRoutine()
    {
        if (_routineState != RoutineState.PlayingRoutineSimulation &&
            _routineState != RoutineState.PlayingRoutineOnRobot &&
            _routineState != RoutineState.PausedRoutineSimulation &&
            _routineState != RoutineState.PausedRoutineOnRobot) return;


        if (_routineState == RoutineState.PlayingRoutineOnRobot)
            _StopRobot(timePerActionInRoutine);
        if (_playingRoutineCoroutine != null)
            StopCoroutine(_playingRoutineCoroutine);


        _routineState = RoutineState.NotPlayingRoutine;
    }

    public bool SendRealtimeAction(RobotAction.ArmsAction action)
    {
        return _SendRealTimeAction(action, RobotAction.EmotionAction.None, RobotAction.SoundAction.None);
    }

    public bool SendRealtimeAction(RobotAction.EmotionAction action)
    {
        return _SendRealTimeAction(RobotAction.ArmsAction.None, action, RobotAction.SoundAction.None);
    }

    public bool SendRealtimeAction(RobotAction.SoundAction action)
    {
        return _SendRealTimeAction(RobotAction.ArmsAction.None, RobotAction.EmotionAction.None, action);
    }

    private bool _SendRealTimeAction(RobotAction.ArmsAction armsAction,
        RobotAction.EmotionAction emotionAction, RobotAction.SoundAction soundAction)
    {
        if (_connectionState != ConnectionState.Connected) return false;


        _SendMessage(_pressedAction, armsAction, emotionAction, soundAction, timePerActionInRealTime);
        return true;
    }

    private RobotAction.MotionAction _pressedAction = RobotAction.MotionAction.None;

    public void StartSendingRealtimeAction(RobotAction.MotionAction action)
    {
        if (_realActionsState != RealActionsState.NotSendingContinuousAction) return;
        _realActionsState = RealActionsState.SendingMotionAction;

        _pressedAction = action;

        _sendContinuousRealTimeActionCoroutine = StartCoroutine(_SendContinuousRealTimeAction(action));
    }

    private Coroutine _sendContinuousRealTimeActionCoroutine;

    private IEnumerator _SendContinuousRealTimeAction(RobotAction.MotionAction action)
    {
        while (true)
        {
            _SendMessage(action, RobotAction.ArmsAction.None, RobotAction.EmotionAction.None,
                RobotAction.SoundAction.None, timePerActionInRealTime);
            yield return new WaitForSeconds(timePerActionInRealTime / 2.0f);
        }
    }

    public void StopSendingRealtimeAction(RobotAction.MotionAction action = RobotAction.MotionAction.None)
    {
        if (_realActionsState != RealActionsState.SendingMotionAction) return;

        if (_pressedAction != action && action != RobotAction.MotionAction.None) return;

        _pressedAction = RobotAction.MotionAction.None;

        _realActionsState = RealActionsState.NotSendingContinuousAction;

        _SendMessage(RobotAction.MotionAction.Stop, RobotAction.ArmsAction.None, RobotAction.EmotionAction.None,
            RobotAction.SoundAction.None, Mathf.CeilToInt(timePerActionInRoutine));

        StopCoroutine(_sendContinuousRealTimeActionCoroutine);
        _sendContinuousRealTimeActionCoroutine = null;
    }

    public bool StartMotionWithGyroscope(float updateInterval = 0.0167f)
    {
        return _gyroscopeControllerMod.StartReceivingGyroscopeStatus(updateInterval);
    }


    public void StopMotionWithGyroscope()
    {
        _gyroscopeControllerMod.StopReceivingGyroscopeStatus();
    }

    public void StartDancing(DancingMode.DancingMusic dancingMusic)
    {
        _dancingCoroutine = StartCoroutine(DancingCoroutine(dancingMusic));
    }

    private Coroutine _dancingCoroutine;

    private IEnumerator DancingCoroutine(DancingMode.DancingMusic dancingMusic)
    {
        while (true)
        {
            if (_routineState == RoutineState.NotPlayingRoutine)
            {
                var danceRoutine = _danceRoutines[dancingMusic];
                PlayRoutine(danceRoutine.motionActions, danceRoutine.emotionActions,
                    danceRoutine.armsActions, danceRoutine.soundActions, true, false);
            }

            yield return new WaitForSeconds(0.25f);
        }
    }

    public void PauseDancing()
    {
        if (_dancingCoroutine != null)
            StopCoroutine(_dancingCoroutine);
        PauseRoutine();
    }

    public void StopDancing()
    {
        if (_dancingCoroutine != null)
            StopCoroutine(_dancingCoroutine);
        StopRoutine();
    }

    public void ShowCameraOnTexture(RawImage rawImage, AspectRatioFitter aspectRatioFitter)
    {
        _cameraMediaMakerMod.ShowCameraOnTexture(rawImage, aspectRatioFitter);
    }

    public void StopShowingCameraOnTexture()
    {
        _cameraMediaMakerMod.StopShowingCameraOnTexture();
    }

    public void TakePhotoAsync()
    {
        string id = DateTime.Now.ToString("yyyy-MM-dd:HH:mm:ss");
        _cameraMediaMakerMod.TakePhotoAsync("INNOBOT", id);
    }

    public void StartMakingVideo()
    {
        _cameraMediaMakerMod.StartRecordingVideo();
    }

    public void StopAndSaveVideoAsync()
    {
        string id = DateTime.Now.ToString("yyyy-MM-dd:HH:mm:ss");
        _cameraMediaMakerMod.StopAndSaveRecordedVideoAsync("INNOBOT", id);
    }


    // HANDLERS

    public void OnBluetoothDeviceListUpdated(List<ConnectivityBluetoothDevice> bluetoothDevices)
    {
        List<LogicBluetoothDevice> robotBluetoothDevices = new List<LogicBluetoothDevice>();
        foreach (var bluetoothDevice in bluetoothDevices)
        {
            robotBluetoothDevices.Add(new LogicBluetoothDevice(bluetoothDevice));
        }

        _logicBluetoothConnectivityListener.PullEvent("OnBluetoothDeviceListUpdated",
            new object[] {robotBluetoothDevices});

        if (_scaningAndConnectingProcess)
        {
            foreach (var bluetoothDevice in bluetoothDevices)
            {
                if (DeviceName == bluetoothDevice.DeviceName)
                {
                    ConnectToBluetoothDeviceAsync(bluetoothDevice.DeviceAddress);
                    return;
                }
            }
        }
    }

    public void OnBluetoothConnected(ConnectivityBluetoothDevice bluetoothDevice,
        ConnectivityVariables.ConnectionState state)
    {
#if UNITY_EDITOR
        state = ConnectivityVariables.ConnectionState.Connected;
#endif
        if (_scaningAndConnectingProcess)
        {
            if (state == ConnectivityVariables.ConnectionState.Connected)
            {
                StopScanningAndConnectingToCompatibleDevice();
            }
            else
            {
                return;
            }
        }

        _connectedBluetoothDevice = bluetoothDevice == null ? null : new LogicBluetoothDevice(bluetoothDevice);

        if (state == ConnectivityVariables.ConnectionState.Connected)
            _connectionState = ConnectionState.Connected;
        else
            _connectionState = ConnectionState.NotConnected;

        _logicBluetoothConnectivityListener.PullEvent("OnBluetoothConnected",
            new object[]
            {
                _connectedBluetoothDevice,
                ArRobotLogicConnectivityVariables.GetState(state)
            });
    }

    public void OnBluetoothDisconnected(ConnectivityBluetoothDevice bluetoothDevice,
        ConnectivityVariables.ConnectionState state)
    {
        StopRoutine();

        _connectedBluetoothDevice = null;
        _connectionState = ConnectionState.NotConnected;

        _logicBluetoothConnectivityListener.PullEvent("OnBluetoothDisconnected",
            new object[]
            {
                bluetoothDevice == null ? null : new LogicBluetoothDevice(bluetoothDevice),
                ArRobotLogicConnectivityVariables.GetState(state)
            });
    }

    public void OnMessageSent(string response)
    {
        Debug.Log(string.Format("Response received: {0}", response));
    }

    public void OnLoadCompleted(object loadedObjec)
    {
        if (loadedObjec == null)
        {
            _logicRoutinesStorageListener.PullEvent("OnRoutineLoaded",
                new object[] {false, _savingOrLoadingRoutine, null, null, null, null});
            return;
        }

        StoringRoutineTemplate _storingRoutineTemplate = (StoringRoutineTemplate) loadedObjec;
        _logicRoutinesStorageListener.PullEvent("OnRoutineLoaded",
            new object[]
            {
                true, _savingOrLoadingRoutine, _storingRoutineTemplate.motionActions,
                _storingRoutineTemplate.armsActions,
                _storingRoutineTemplate.emotionActions, _storingRoutineTemplate.soundActions
            });
    }

    public void OnStorageCompleted(StorageVariables.StoragingState state)
    {
        if (state == StorageVariables.StoragingState.Storaged)
            _savedRoutinesDict[_savingOrLoadingRoutine.Id] = _savingOrLoadingRoutine;

        _logicRoutinesStorageListener.PullEvent("OnRoutineSaved",
            new object[] {state == StorageVariables.StoragingState.Storaged, _savingOrLoadingRoutine});
    }

    private static byte[] _TransformActionsToMessage(RobotAction.MotionAction motionAction,
        RobotAction.ArmsAction armsAction, RobotAction.EmotionAction emotionAction,
        RobotAction.SoundAction soundAction, int time)
    {
        byte[] msg = new byte[8];

        msg[0] = 0xAA;

        switch (motionAction)
        {
            case RobotAction.MotionAction.MoveForward:
                msg[1] = 0x03;
                break;
            case RobotAction.MotionAction.MoveBackward:
                msg[1] = 0x07;
                break;
            case RobotAction.MotionAction.TurnRight:
                if(_turning90Degrees)
                    msg[1] = 0x10;
                else
                    msg[1] = 0x06;
                break;
            case RobotAction.MotionAction.TurnLeft:
                if(_turning90Degrees)
                    msg[1] = 0x11;
                else
                    msg[1] = 0x04;
                break;
            case RobotAction.MotionAction.Stop:
                msg[1] = 0x05;
                break;
            case RobotAction.MotionAction.None:
                msg[1] = 0x00;
                break;
        }

        switch (armsAction)
        {
            case RobotAction.ArmsAction.OpenArms:
                msg[2] = 0x02;
                break;
            case RobotAction.ArmsAction.CloseArms:
                msg[2] = 0x01;
                break;
            case RobotAction.ArmsAction.Stop:
                msg[2] = 0x05;
                break;
            case RobotAction.ArmsAction.None:
                msg[2] = 0x00;
                break;
        }

        switch (emotionAction)
        {
            case RobotAction.EmotionAction.Stop:
                msg[3] = 0x0E;
                break;
            case RobotAction.EmotionAction.None:
                msg[3] = 0x00;
                break;
            default:
                msg[3] = (byte) (((int) emotionAction) - 1);
                break;
        }


        switch (soundAction)
        {
            case RobotAction.SoundAction.Sound1:
                msg[4] = 0x01;
                break;
            case RobotAction.SoundAction.Sound2:
                msg[4] = 0x02;
                break;
            case RobotAction.SoundAction.Sound3:
                msg[4] = 0x03;
                break;
            case RobotAction.SoundAction.Sound4:
                msg[4] = 0x04;
                break;
            case RobotAction.SoundAction.Sound5:
                msg[4] = 0x05;
                break;
            case RobotAction.SoundAction.Sound6:
                msg[4] = 0x06;
                break;
            case RobotAction.SoundAction.Stop:
                msg[4] = 0x08;
                break;
            case RobotAction.SoundAction.None:
                msg[4] = 0x00;
                break;
        }

        msg[5] = Convert.ToByte(time);

        msg[7] = 0xBB;

        msg[6] = (byte) (msg[0] ^ msg[1] ^ msg[2] ^ msg[3] ^ msg[4] ^ msg[5] ^ msg[7]);

        return msg;
    }

    /* private static byte[][] _TransformActionsToMessage(RobotAction.EmotionAction emotionAction, int time)
     {
         if (emotionAction == RobotAction.EmotionAction.Stop || emotionAction == RobotAction.EmotionAction.None)
             return new byte[0][];
 
         var faceActions = emotionToFaceActions[emotionAction];
 
         byte[][] msg = new byte[faceActions.Length][];
         for (int i = 0; i < msg.Length; i++)
         {
             msg[i] = new byte[8];
         }
 
         for (int i = 0; i < faceActions.Length; i++)
         {
             switch (faceActions[i])
             {
                 case RobotAction.FaceAction.UpperLeftEye:
                     msg[i][3] = 0x01;
                     break;
                 case RobotAction.FaceAction.UpperRightEye:
                     msg[i][3] = 0x02;
                     break;
                 case RobotAction.FaceAction.LowerLeftEye:
                     msg[i][3] = 0x04;
                     break;
                 case RobotAction.FaceAction.LowerRightEye:
                     msg[i][3] = 0x03;
                     break;
                 case RobotAction.FaceAction.LeftMouth:
                     msg[i][3] = 0x05;
                     break;
                 case RobotAction.FaceAction.CenterMouth:
                     msg[i][3] = 0x07;
                     break;
                 case RobotAction.FaceAction.RightMouth:
                     msg[i][3] = 0x06;
                     break;
                 case RobotAction.FaceAction.Stop:
                     msg[i][3] = 0x08;
                     break;
                 case RobotAction.FaceAction.None:
                     msg[i][3] = 0x00;
                     break;
             }
 
             msg[i][0] = 0xAA;
 
             msg[i][1] = 0x00;
 
             msg[i][2] = 0x00;
 
             msg[i][4] = 0x00;
 
             msg[i][5] = Convert.ToByte(time);
 
             msg[i][7] = 0xBB;
 
             msg[i][6] = (byte) (msg[i][0] & msg[i][1] & msg[i][2] & msg[i][3] & msg[i][4] & msg[i][5] & msg[i][7]);
         }
 
         return msg;
     }
 */

    public static Dictionary<RobotAction.EmotionAction, RobotAction.FaceAction[]> emotionToFaceActions =
        new Dictionary<RobotAction.EmotionAction, RobotAction.FaceAction[]>
        {
            {
                RobotAction.EmotionAction.Face1,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,

                    RobotAction.FaceAction.CenterMouth
                }
            },
            {
                RobotAction.EmotionAction.Face2,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,

                    RobotAction.FaceAction.LeftMouth, RobotAction.FaceAction.CenterMouth,
                    RobotAction.FaceAction.RightMouth
                }
            },
            {
                RobotAction.EmotionAction.Face3,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,

                    RobotAction.FaceAction.LeftMouth, RobotAction.FaceAction.CenterMouth,
                    RobotAction.FaceAction.RightMouth
                }
            },
            {
                RobotAction.EmotionAction.Face4,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,
                }
            },
            {
                RobotAction.EmotionAction.Face9,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,
                }
            },
            {
                RobotAction.EmotionAction.Face10,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,


                    RobotAction.FaceAction.LeftMouth, RobotAction.FaceAction.CenterMouth,
                    RobotAction.FaceAction.RightMouth
                }
            },
            {
                RobotAction.EmotionAction.Face11,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,
                }
            },
            {
                RobotAction.EmotionAction.Face12,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,

                    RobotAction.FaceAction.CenterMouth,
                }
            },
            {
                RobotAction.EmotionAction.Face5,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,

                    RobotAction.FaceAction.CenterMouth,
                }
            },
            {
                RobotAction.EmotionAction.Face6,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.LowerLeftEye,

                    RobotAction.FaceAction.RightMouth
                }
            },
            {
                RobotAction.EmotionAction.Face7,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.LowerLeftEye, RobotAction.FaceAction.LowerRightEye,

                    RobotAction.FaceAction.LeftMouth,
                }
            },
            {
                RobotAction.EmotionAction.Face8,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.UpperLeftEye, RobotAction.FaceAction.UpperRightEye,

                    RobotAction.FaceAction.LeftMouth
                }
            },
            {
                RobotAction.EmotionAction.Stop,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.Stop
                }
            },
            {
                RobotAction.EmotionAction.None,
                new RobotAction.FaceAction[]
                {
                    RobotAction.FaceAction.None
                }
            }
        };

    private static Dictionary<DancingMode.DancingMusic, DanceRoutine> _danceRoutines =
        new Dictionary<DancingMode.DancingMusic, DanceRoutine>
        {
            {
                DancingMode.DancingMusic.Classic,
                new DanceRoutine(
                    new[]
                    {
                        RobotAction.MotionAction.MoveForward, RobotAction.MotionAction.MoveBackward,
                        RobotAction.MotionAction.TurnRight, RobotAction.MotionAction.TurnRight,
                        RobotAction.MotionAction.TurnLeft, RobotAction.MotionAction.TurnLeft,
                    },
                    new[]
                    {
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                    }, new[]
                    {
                        RobotAction.EmotionAction.Face1, RobotAction.EmotionAction.Face5,
                        RobotAction.EmotionAction.Face10, RobotAction.EmotionAction.Face5,
                        RobotAction.EmotionAction.Face11, RobotAction.EmotionAction.Face4,
                    },
                    new[]
                    {
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                    }
                )
            },
            {
                DancingMode.DancingMusic.Pop,
                new DanceRoutine(
                    new[]
                    {
                        RobotAction.MotionAction.MoveForward, RobotAction.MotionAction.MoveForward,
                        RobotAction.MotionAction.MoveBackward, RobotAction.MotionAction.TurnLeft,
                        RobotAction.MotionAction.TurnRight, RobotAction.MotionAction.MoveBackward,
                    },
                    new[]
                    {
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                    }, new[]
                    {
                        RobotAction.EmotionAction.Face9, RobotAction.EmotionAction.Face12,
                        RobotAction.EmotionAction.Face8, RobotAction.EmotionAction.Face6,
                        RobotAction.EmotionAction.Face3, RobotAction.EmotionAction.Face9,
                    },
                    new[]
                    {
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                    }
                )
            },
            {
                DancingMode.DancingMusic.Rap,
                new DanceRoutine(
                    new[]
                    {
                        RobotAction.MotionAction.TurnLeft, RobotAction.MotionAction.TurnLeft,
                        RobotAction.MotionAction.MoveForward, RobotAction.MotionAction.MoveBackward,
                        RobotAction.MotionAction.TurnRight, RobotAction.MotionAction.TurnRight,
                    },
                    new[]
                    {
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.OpenArms,
                        RobotAction.ArmsAction.CloseArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                    }, new[]
                    {
                        RobotAction.EmotionAction.Face2, RobotAction.EmotionAction.Face4,
                        RobotAction.EmotionAction.Face7, RobotAction.EmotionAction.Face10,
                        RobotAction.EmotionAction.Face2, RobotAction.EmotionAction.Face8,
                    },
                    new[]
                    {
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                    }
                )
            },
            {
                DancingMode.DancingMusic.Rock,
                new DanceRoutine(
                    new[]
                    {
                        RobotAction.MotionAction.MoveForward, RobotAction.MotionAction.MoveForward,
                        RobotAction.MotionAction.TurnLeft, RobotAction.MotionAction.MoveBackward,
                        RobotAction.MotionAction.MoveBackward, RobotAction.MotionAction.TurnRight,
                    },
                    new[]
                    {
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                    }, new[]
                    {
                        RobotAction.EmotionAction.Face2, RobotAction.EmotionAction.Face6,
                        RobotAction.EmotionAction.Face8, RobotAction.EmotionAction.Face12,
                        RobotAction.EmotionAction.Face12, RobotAction.EmotionAction.Face11,
                    },
                    new[]
                    {
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                    }
                )
            },
            {
                DancingMode.DancingMusic.Techno,
                new DanceRoutine(
                    new[]
                    {
                        RobotAction.MotionAction.TurnLeft, RobotAction.MotionAction.TurnRight,
                        RobotAction.MotionAction.MoveForward, RobotAction.MotionAction.MoveBackward,
                        RobotAction.MotionAction.TurnRight, RobotAction.MotionAction.TurnLeft,
                    },
                    new[]
                    {
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.CloseArms, RobotAction.ArmsAction.OpenArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                    }, new[]
                    {
                        RobotAction.EmotionAction.Face9, RobotAction.EmotionAction.Face2,
                        RobotAction.EmotionAction.Face10, RobotAction.EmotionAction.Face3,
                        RobotAction.EmotionAction.Face9, RobotAction.EmotionAction.Face12,
                    },
                    new[]
                    {
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                    }
                )
            },
            {
                DancingMode.DancingMusic.HipHop,
                new DanceRoutine(
                    new[]
                    {
                        RobotAction.MotionAction.TurnRight, RobotAction.MotionAction.TurnLeft,
                        RobotAction.MotionAction.TurnRight, RobotAction.MotionAction.TurnLeft,
                        RobotAction.MotionAction.MoveForward, RobotAction.MotionAction.MoveBackward,
                    },
                    new[]
                    {
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                        RobotAction.ArmsAction.OpenArms, RobotAction.ArmsAction.CloseArms,
                    }, new[]
                    {
                        RobotAction.EmotionAction.Face7, RobotAction.EmotionAction.Face8,
                        RobotAction.EmotionAction.Face6, RobotAction.EmotionAction.Face2,
                        RobotAction.EmotionAction.Face4, RobotAction.EmotionAction.Face8,
                    },
                    new[]
                    {
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                        RobotAction.SoundAction.None, RobotAction.SoundAction.None,
                    }
                )
            },
        };

    private class DanceRoutine
    {
        public RobotAction.MotionAction[] motionActions;
        public RobotAction.ArmsAction[] armsActions;
        public RobotAction.EmotionAction[] emotionActions;
        public RobotAction.SoundAction[] soundActions;


        public DanceRoutine(RobotAction.MotionAction[] motionActions, RobotAction.ArmsAction[] armsActions,
            RobotAction.EmotionAction[] emotionActions, RobotAction.SoundAction[] soundActions)
        {
            this.motionActions = motionActions;
            this.armsActions = armsActions;
            this.emotionActions = emotionActions;
            this.soundActions = soundActions;
        }
    }

    public void OnNewGyroscopeStatus(Quaternion rotation)
    {
        if (!_referenceRotationInitialized)
        {
            _referenceRotation = rotation;
            _referenceRotationInitialized = true;
            _referenceRotationGO.transform.rotation = rotation;
        }

        _newRotationGO.transform.rotation = rotation;


        Vector2 gyro = new Vector2(
            Vector3.SignedAngle(
                Vector3.ProjectOnPlane(_newRotationGO.transform.forward, _referenceRotationGO.transform.up),
                _referenceRotationGO.transform.forward, _referenceRotationGO.transform.up),
            Vector3.SignedAngle(
                Vector3.ProjectOnPlane(_newRotationGO.transform.forward, _referenceRotationGO.transform.right),
                _referenceRotationGO.transform.forward, _referenceRotationGO.transform.right));

        //Vector3 rotationVector3 = rotation.eulerAngles;
        Debug.Log("rotationVecto3: " + gyro.ToString());
        gyro.x = Mathf.Abs(gyro.x) < centerVoidGyroValueHorizontal
            ? 0
            : (Mathf.Abs(gyro.x) - centerVoidGyroValueHorizontal) * Mathf.Sign(gyro.x);
        gyro.y = Mathf.Abs(gyro.y) < centerVoidGyroValueVertical
            ? 0
            : (Mathf.Abs(gyro.y) - centerVoidGyroValueVertical) * Mathf.Sign(gyro.y);

        Debug.Log("gyro: " + gyro.ToString());


        float horizontal = gyro.x > 0
            ? Mathf.Min(gyro.x / maxGyroValueHorizontal, 1)
            : Mathf.Max(gyro.x / maxGyroValueHorizontal, -1);

        float vertical = gyro.y > 0
            ? Mathf.Min(gyro.y / maxGyroValueVertical, 1)
            : Mathf.Max(gyro.y / maxGyroValueVertical, -1);

        _lastRotation = rotation;

        _logicGyroscopeListener.PullEvent("OnNewGyroscopeStatus", new object[] {horizontal, vertical * -1});
    }


    public void StabilizeGyroscospe()
    {
        _referenceRotation = _lastRotation;
        _referenceRotationGO.transform.rotation = _lastRotation;
    }
}