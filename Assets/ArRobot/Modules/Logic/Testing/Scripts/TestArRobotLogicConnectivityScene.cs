﻿using System;
using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class TestArRobotLogicConnectivityScene : UmodsSceneBehaviour, ILogicBluetoothConnectivityHandler
{
    public InputField idInput;
    public Button connectButton;
    public Button disconnectButton;
    public Button startScanningButton;
    public Button stopScanningButton;
    public Button sendButton;
    

    public string deviceAddress = "";

    private IArRobotLogicBluetoothConnectivityMod _logicConnectivityMod;
    private IArRobotLogicMod _logicMod;

    void UStart()
    {
        _logicConnectivityMod = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();

        connectButton.onClick.AddListener((_OnConnectButtonPressed));
        disconnectButton.onClick.AddListener((() => _logicConnectivityMod.DisconnectFromBluetoothDeviceAsync()));
        startScanningButton.onClick.AddListener((() => _logicConnectivityMod.StartGettingBluetoothDeviceListAsync()));
        stopScanningButton.onClick.AddListener((() => _logicConnectivityMod.StopGettingBluetoothDeviceList()));
        sendButton.onClick.AddListener((_SendMessage));
    }

    public void OnBluetoothDeviceListUpdated(List<LogicBluetoothDevice> bluetoothDevices)
    {
        var text = "";

        foreach (var bluetoothDevice in bluetoothDevices)
        {
            text += String.Format("Id: {0}, Name: {1}\n", bluetoothDevice.Address, bluetoothDevice.Name);
        }

        Debug.Log(text);
    }

    public void OnBluetoothConnected(LogicBluetoothDevice logicBluetoothDevice, ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        Debug.Log(String.Format("State received: {0}; Bluetooth device: {1}", state.ToString(),
            logicBluetoothDevice != null ? logicBluetoothDevice.Name : "Not Found"));
    }

    public void OnBluetoothDisconnected(LogicBluetoothDevice logicBluetoothDevice, ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        Debug.Log(String.Format("State received: {0}; Bluetooth device: {1}", state.ToString(),
            logicBluetoothDevice != null ? logicBluetoothDevice.Name : "Not Found"));
    }

    private void _OnConnectButtonPressed()
    {
        if (deviceAddress.Equals("") && idInput.text.Equals("")) return;
        _logicConnectivityMod.ConnectToBluetoothDeviceAsync(idInput.text.Equals("") ? deviceAddress : idInput.text);
    }

    private void _SendMessage()
    {
        //_logicMod.PlayRoutine();
    }
}