﻿using System;
using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;

public class StorageActionsTestingScene : UmodsSceneBehaviour, ILogicRoutinesStorageHandler
{
    private IArRobotLogicMod _arRobotLogicMod;
    private ILogicRoutinesStorageHandler _logicRoutinesStorageHandlerImplementation;

    void UStart()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
        _arRobotLogicMod.SaveRoutineAsync("First",
            new[] {RobotAction.MotionAction.MoveBackward, RobotAction.MotionAction.TurnLeft},
            new[] {RobotAction.EmotionAction.Face2, RobotAction.EmotionAction.Face4},
            new[] {RobotAction.ArmsAction.None, RobotAction.ArmsAction.CloseArms}, new[] {RobotAction.SoundAction.Sound2, RobotAction.SoundAction.Sound4});
    }

    public void OnRoutineLoaded(bool success, SavedRoutine loadedRoutine, RobotAction.MotionAction[] motionActions,
        RobotAction.ArmsAction[] armsActions,
        RobotAction.EmotionAction[] emotionActions, RobotAction.SoundAction[] soundActions)
    {
        if (success)
            Debug.Log(String.Format("Loaded {0} routine, id: {1}, motion {2}, arms: {3}, emotions: {4}, sounds: {5}",
                loadedRoutine.Name, loadedRoutine.Id, motionActions, armsActions, emotionActions, soundActions));
        else
            Debug.Log(String.Format(
                "Not loaded {0} routine, id: {1}, motion {2}, arms: {3}, emotions: {4}, sounds: {5}",
                loadedRoutine.Name, loadedRoutine.Id, motionActions, armsActions, emotionActions, soundActions));
    }

    public void OnRoutineSaved(bool success, SavedRoutine savedRoutine)
    {
        if (success)
        {
            Debug.Log(String.Format("Saved {0} routine, id: {1}", savedRoutine.Name, savedRoutine.Id));
            _arRobotLogicMod.LoadRoutineAsync(savedRoutine.Id);
        }
        else
            Debug.Log(String.Format("Not saved {0} routine, id: {1}", savedRoutine.Name, savedRoutine.Id));
    }
}