﻿using System;
using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Common;
using ArRobot.Modules.Logic.Scripts.Common;
using ArRobot.Modules.Logic.Scripts.Core;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Common;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Core;
using Umods.ExternalMods.Connectivity.Mods.BluetoothMod.Simulation.Core;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class AutomaticConnection : UmodsSceneBehaviour, ILogicBluetoothConnectivityHandler
{
    public string deviceName;
    public string service;
    public string characteristic;

    public int defaultTime;

    public Color colorButtonSelected;

    public Button forward;
    public Button backward;
    public Button left;
    public Button right;
    public Button stopMotion;

    public Button open;
    public Button close;
    public Button stopArms;

    public Button eyes;
    public Button mouth;
    public Button stopFace;

    public Button sound1;
    public Button sound2;
    public Button stopSound;

    public InputField time;

    public InputField nameInput;
    public InputField addressInput;
    public InputField serviceInput;
    public InputField characteristicInput;

    public Button connectBtn;
    public Button sendBtn;

    public GameObject warningPanel;
    public Text warningText;
    public Button warningBtn;


    private Color _colorButtonUnselected;

    private RobotAction.MotionAction _motionAction = RobotAction.MotionAction.None;
    private RobotAction.ArmsAction _armsAction = RobotAction.ArmsAction.None;
    private RobotAction.EmotionAction _emotionAction = RobotAction.EmotionAction.None;
    private RobotAction.SoundAction _soundAction = RobotAction.SoundAction.None;

    private Button _motionButton = null;
    private Button _armsButton = null;
    private Button _faceButton = null;
    private Button _soundButton = null;

    private IArRobotLogicBluetoothConnectivityMod _arRobotLogicBluetooth;
    private IArRobotLogicMod _arRobotLogicMod;

    private bool _isNameSearchingReference = true;

    private List<LogicBluetoothDevice> _bluetoothDevices = new List<LogicBluetoothDevice>();

    private bool _connectedToBluetoothDevice = false;


    void UStart()
    {
        _arRobotLogicBluetooth = UmodsApplication.GetMod<IArRobotLogicBluetoothConnectivityMod>();
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();

        nameInput.text = deviceName;
        _arRobotLogicBluetooth.DeviceName = deviceName;
        serviceInput.text = service;
        _arRobotLogicBluetooth.ServiceUUID = service;
        characteristicInput.text = characteristic;
        _arRobotLogicBluetooth.CharacteristicUUID = characteristic;
        time.text = defaultTime.ToString();

        _colorButtonUnselected = forward.colors.normalColor;

        forward.onClick.AddListener(() => _ButtonPressed(RobotAction.MotionAction.MoveForward, forward));
        backward.onClick.AddListener(() => _ButtonPressed(RobotAction.MotionAction.MoveBackward, backward));
        left.onClick.AddListener(() => _ButtonPressed(RobotAction.MotionAction.TurnLeft, left));
        right.onClick.AddListener(() => _ButtonPressed(RobotAction.MotionAction.TurnRight, right));
        stopMotion.onClick.AddListener(() => _ButtonPressed(RobotAction.MotionAction.Stop, stopMotion));

        open.onClick.AddListener(() => _ButtonPressed(RobotAction.ArmsAction.OpenArms, open));
        close.onClick.AddListener(() => _ButtonPressed(RobotAction.ArmsAction.CloseArms, close));
        stopArms.onClick.AddListener(() => _ButtonPressed(RobotAction.ArmsAction.Stop, stopArms));

        eyes.onClick.AddListener(() => _ButtonPressed(RobotAction.EmotionAction.Face1, eyes));
        mouth.onClick.AddListener(() => _ButtonPressed(RobotAction.EmotionAction.Face2, mouth));
        stopFace.onClick.AddListener(() => _ButtonPressed(RobotAction.EmotionAction.Stop, stopFace));

        sound1.onClick.AddListener(() => _ButtonPressed(RobotAction.SoundAction.Sound1, sound1));
        sound2.onClick.AddListener(() => _ButtonPressed(RobotAction.SoundAction.Sound2, sound2));
        stopSound.onClick.AddListener(() => _ButtonPressed(RobotAction.SoundAction.Stop, stopSound));



        nameInput.onValueChanged.AddListener((value) =>
        {
            _arRobotLogicBluetooth.DisconnectFromBluetoothDeviceAsync();
            _arRobotLogicBluetooth.DeviceName = deviceName;
            _isNameSearchingReference = true;
        });
        addressInput.onValueChanged.AddListener((value) =>
        {
            _arRobotLogicBluetooth.DisconnectFromBluetoothDeviceAsync();
            _isNameSearchingReference = false;
        });

        connectBtn.onClick.AddListener(_ConnectToAddress);

        sendBtn.onClick.AddListener(_SendActions);

        warningBtn.onClick.AddListener(HideWarning);

        ///BAD
        serviceInput.onValueChanged.AddListener((value) =>
        {
            _arRobotLogicBluetooth.DisconnectFromBluetoothDeviceAsync();
            _arRobotLogicBluetooth.ServiceUUID = value;
        });

        characteristicInput.onValueChanged.AddListener((value) =>
        {
            _arRobotLogicBluetooth.CharacteristicUUID = characteristic;
        });
    }


    private void _SendActions()
    {
        if (_arRobotLogicBluetooth.GetConnectedBluetoothDevice() == null)
        {
            ShowWarning("You are not connected to any bluetooth.");
            return;
        }

        _arRobotLogicMod.PlayRoutine(new []{_motionAction}, new []{_emotionAction}, new []{_armsAction},
            new []{_soundAction}, true);
    }


    private void _ConnectToAddress()
    {
        if (_connectedToBluetoothDevice)
        {
            _arRobotLogicBluetooth.DisconnectFromBluetoothDeviceAsync();
            return;
        }

        _arRobotLogicBluetooth.ScanAndConnectToCompatibleDeviceAsync();
    }

    private void ShowWarning(string text)
    {
        warningPanel.SetActive(true);
        warningText.text = text;
    }

    private void HideWarning()
    {
        warningPanel.SetActive(false);
    }

    private void _ButtonPressed(RobotAction.MotionAction action, Button button)
    {
        if (_motionButton != null)
        {
            var motionButtonColors = _motionButton.colors;
            motionButtonColors.normalColor = _colorButtonUnselected;
            motionButtonColors.selectedColor = _colorButtonUnselected;
            _motionButton.colors = motionButtonColors;
            _motionButton = null;
        }

        if (_motionAction != action)

        {
            _motionAction = action;
            _motionButton = button;

            var buttonColors = _motionButton.colors;
            buttonColors.normalColor = colorButtonSelected;
            buttonColors.selectedColor = colorButtonSelected;
            _motionButton.colors = buttonColors;
        }
        else
        {
            _motionAction = RobotAction.MotionAction.None;
        }
    }

    private void _ButtonPressed(RobotAction.ArmsAction action, Button button)
    {
        if (_armsButton != null)
        {
            var armsButtonColors = _armsButton.colors;
            armsButtonColors.normalColor = _colorButtonUnselected;
            armsButtonColors.selectedColor = _colorButtonUnselected;
            _armsButton.colors = armsButtonColors;
            _armsButton = null;
        }

        if (_armsAction != action)
        {
            _armsAction = action;
            _armsButton = button;

            var buttonColors = _armsButton.colors;
            buttonColors.normalColor = colorButtonSelected;
            buttonColors.selectedColor = colorButtonSelected;
            _armsButton.colors = buttonColors;
        }
        else
        {
            _armsAction = RobotAction.ArmsAction.None;
        }
    }

    private void _ButtonPressed(RobotAction.EmotionAction action, Button button)
    {
        if (_faceButton != null)
        {
            var faceButtonColors = _faceButton.colors;
            faceButtonColors.normalColor = _colorButtonUnselected;
            faceButtonColors.selectedColor = _colorButtonUnselected;
            _faceButton.colors = faceButtonColors;
            _faceButton = null;
        }

        if (_emotionAction != action)
        {
            _emotionAction = action;
            _faceButton = button;

            var buttonColors = _faceButton.colors;
            buttonColors.normalColor = colorButtonSelected;
            buttonColors.selectedColor = colorButtonSelected;
            _faceButton.colors = buttonColors;
        }
        else
        {
            _emotionAction = RobotAction.EmotionAction.None;
        }
    }

    private void _ButtonPressed(RobotAction.SoundAction action, Button button)
    {
        if (_soundButton != null)
        {
            var soundButtonColors = _soundButton.colors;
            soundButtonColors.normalColor = _colorButtonUnselected;
            soundButtonColors.selectedColor = _colorButtonUnselected;
            _soundButton.colors = soundButtonColors;
            _soundButton = null;
        }

        if (_soundAction != action)
        {
            _soundAction = action;
            _soundButton = button;

            var buttonColors = _soundButton.colors;
            buttonColors.normalColor = colorButtonSelected;
            buttonColors.selectedColor = colorButtonSelected;
            _soundButton.colors = buttonColors;
        }
        else
        {
            _soundAction = RobotAction.SoundAction.None;
        }
    }

    public void OnBluetoothDeviceListUpdated(List<LogicBluetoothDevice> bluetoothDevices)
    {
        
    }

    public void OnBluetoothConnected(LogicBluetoothDevice logicBluetoothDevice,
        ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        if (state == ArRobotLogicConnectivityVariables.ConnectionState.Connected)
        {
            ShowWarning(string.Format("You have successfully connected to the {0} Bluetooth device.",
                logicBluetoothDevice.Name));
            _connectedToBluetoothDevice = true;
            connectBtn.transform.GetComponentInChildren<Text>().text = "Disconnect";
        }
        else
        {
            ShowWarning("Bluetooth connection has failed: " + state.ToString());
        }
    }

    public void OnBluetoothDisconnected(LogicBluetoothDevice logicBluetoothDevice,
        ArRobotLogicConnectivityVariables.ConnectionState state)
    {
        ShowWarning(string.Format("Bluetooth {0} has been disconnected.",
            logicBluetoothDevice.Name));

        _connectedToBluetoothDevice = false;
        connectBtn.transform.GetComponentInChildren<Text>().text = "Connect";
    }
}