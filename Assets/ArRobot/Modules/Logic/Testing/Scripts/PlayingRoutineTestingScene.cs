﻿using System.Collections;
using System.Collections.Generic;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class PlayingRoutineTestingScene : UmodsSceneBehaviour, ILogicActionsPlayerHandler
{
   public Button play, pause, unpause, stop;

   private IArRobotLogicMod _arRobotLogicMod;

   void UStart()
   {
      _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();
      
      play.onClick.AddListener(_PlayButton);
      pause.onClick.AddListener(_arRobotLogicMod.PauseRoutine);
      unpause.onClick.AddListener(_arRobotLogicMod.UnpauseRoutine);
      stop.onClick.AddListener(_arRobotLogicMod.StopRoutine);
   }

   void _PlayButton()
   {
      _arRobotLogicMod.PlayRoutine(
         new[] {RobotAction.MotionAction.MoveBackward, RobotAction.MotionAction.TurnLeft, RobotAction.MotionAction.None, RobotAction.MotionAction.TurnLeft},
         new[] {RobotAction.EmotionAction.Face2, RobotAction.EmotionAction.Face4, RobotAction.EmotionAction.Face3, RobotAction.EmotionAction.None},
         new[] {RobotAction.ArmsAction.None, RobotAction.ArmsAction.CloseArms, RobotAction.ArmsAction.None, RobotAction.ArmsAction.CloseArms}, new[] {RobotAction.SoundAction.Sound2, RobotAction.SoundAction.Sound4, RobotAction.SoundAction.Sound1, RobotAction.SoundAction.None}, false);
   }

   public void OnNewStepPlayed(int stepCounter, RobotAction.MotionAction motionAction, RobotAction.ArmsAction armsAction, RobotAction.EmotionAction emotionAction,
      RobotAction.SoundAction soundAction)
   {
      Debug.Log(string.Format("Action number {0} played: {1}, {2}, {3}, {4}", stepCounter, motionAction, armsAction, emotionAction, soundAction));
   }
}
