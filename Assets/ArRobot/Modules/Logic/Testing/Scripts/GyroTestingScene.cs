﻿using System;
using System.Collections;
using System.Collections.Generic;
using ArRobot.Modules.Logic.Scripts.Handlers;
using Umods.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

public class GyroTestingScene : UmodsSceneBehaviour, ILogicGyroscopeHandler
{
    public Button play, stop;

    private IArRobotLogicMod _arRobotLogicMod;

    void UStart()
    {
        _arRobotLogicMod = UmodsApplication.GetMod<IArRobotLogicMod>();

        play.onClick.AddListener(_PlayButton);
        stop.onClick.AddListener(_StopButton);
    }

    void _PlayButton()
    {
        if(!_arRobotLogicMod.StartMotionWithGyroscope())
            Debug.Log("Gyroscope not supported");
    }

    void _StopButton()
    {
        _arRobotLogicMod.StopMotionWithGyroscope();
    }

    public void OnNewGyroscopeStatus(float horizontal, float vertical)
    {
      //  Debug.Log(String.Format("Horizontal: {0}, Vertical:{1}", horizontal, vertical));
    }
}