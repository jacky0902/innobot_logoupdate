﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class RobotAction
{
    public enum MotionAction
    {
        None,
        Stop,

        MoveForward,
        MoveBackward,
        TurnRight,
        TurnLeft,
    }

    public enum EmotionAction
    {
        None,
        Stop,

        Face1,
        Face2,
        Face3,
        Face4,
        Face5,
        Face6,
        Face7,
        Face8,
        Face9,
        Face10,
        Face11,
        Face12,
    }

    public enum FaceAction
    {
        None,
        Stop,

        UpperLeftEye,
        UpperRightEye,
        LowerLeftEye,
        LowerRightEye,
        LeftMouth,
        RightMouth,
        CenterMouth,
    }

    public enum ArmsAction
    {
        None,
        Stop,

        OpenArms,
        CloseArms,
    }

    public enum SoundAction
    {
        Sound1,
        Sound2,
        Sound3,
        Sound4,
        Sound5,
        Sound6,
        
        None,
        Stop,
    }

    public static FaceAction[] GetFaceActions(EmotionAction emotionAction)
    {
        switch (emotionAction)
        {
            case EmotionAction.Face1:
                return new[]
                {
                    FaceAction.UpperLeftEye, FaceAction.UpperRightEye, FaceAction.LowerLeftEye, FaceAction.LowerRightEye
                };
            case EmotionAction.Face2:
                return new[] {FaceAction.LeftMouth, FaceAction.CenterMouth, FaceAction.RightMouth};
            default:
                return new[] {FaceAction.LeftMouth, FaceAction.CenterMouth, FaceAction.RightMouth};
        }
    }
}