﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavedRoutine
{
    public string Name { get; }
    public string Id { get; }

    public SavedRoutine(string id, string name)
    {
        Name = name;
        Id = id;
    }
}