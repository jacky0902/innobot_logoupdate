namespace ArRobot.Modules.Common
{
    public class DancingMode
    {
        public enum DancingMusic
        {
            Rap,
            Pop,
            HipHop,
            Rock,
            Classic,
            Techno
        }
    }
}